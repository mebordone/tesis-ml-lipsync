=== Run information ===

Scheme:       weka.classifiers.trees.RandomForest -I 10 -K 0 -S 1 -num-slots 1
Relation:     I4-1ms-1gramasfeatures2
Instances:    183404
Attributes:   14
              0Int(dB)
              0Pitch(Hz)
              0RawPitch
              0SmPitch
              0Melogram(st)
              0ZCross
              0F1(Hz)
              0F2(Hz)
              0F3(Hz)
              0F4(Hz)
              0F2menosF1
              02F1menosF2
              0F2masF1
              class
Test mode:    10-fold cross-validation

=== Classifier model (full training set) ===

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1028



Time taken to build model: 28.07 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances      167645               91.4075 %
Incorrectly Classified Instances     15759                8.5925 %
Kappa statistic                          0.8869
Mean absolute error                      0.0272
Root mean squared error                  0.113 
Relative absolute error                 17.6577 %
Root relative squared error             40.7487 %
Coverage of cases (0.95 level)          98.4199 %
Mean rel. region size (0.95 level)      18.5816 %
Total Number of Instances           183404     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,899    0,000    0,968      0,899    0,933      0,933    0,978     0,927     C
                 0,940    0,003    0,960      0,940    0,950      0,946    0,991     0,970     E
                 0,707    0,001    0,952      0,707    0,811      0,818    0,981     0,815     FV
                 0,934    0,006    0,972      0,934    0,953      0,942    0,990     0,977     AI
                 0,873    0,037    0,814      0,873    0,842      0,813    0,984     0,934     CDGKNRSYZ
                 0,868    0,002    0,975      0,868    0,918      0,914    0,985     0,934     O
                 0,966    0,067    0,903      0,966    0,933      0,889    0,990     0,985     0
                 0,633    0,002    0,913      0,633    0,748      0,753    0,960     0,766     LT
                 0,954    0,000    0,979      0,954    0,966      0,965    0,992     0,979     U
                 0,777    0,001    0,962      0,777    0,860      0,861    0,977     0,873     MBP
Weighted Avg.    0,914    0,034    0,917      0,914    0,913      0,889    0,987     0,956     

=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   974    12     0    18    35     1    41     0     2     0 |     a = C
     2 12914    12    44   347    15   338    32    10    26 |     b = E
     1    24  1810    23   573    11   111     6     0     2 |     c = FV
    12    77    22 33279   859    41  1205    89    11    45 |     d = AI
     2   173    22   282 24852    81  2855   132    28    32 |     e = CDGKNRSYZ
     4    27     8    71   417 11491  1159    35     6    16 |     f = O
     9   109    14   226  1928    60 69673    60    12    26 |     g = 0
     0    53     6   156   952    48  1189  4188     7    20 |     h = LT
     2    15     2    29    64    13    40    13  3946    13 |     i = U
     0    43     5   101   519    27   560    31    10  4518 |     j = MBP

