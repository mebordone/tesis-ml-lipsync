=== Run information ===

Scheme:       weka.classifiers.bayes.BayesNet -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5
Relation:     I4-1ms-1gramas
Instances:    183404
Attributes:   11
              0Int(dB)
              0Pitch(Hz)
              0RawPitch
              0SmPitch
              0Melogram(st)
              0ZCross
              0F1(Hz)
              0F2(Hz)
              0F3(Hz)
              0F4(Hz)
              class
Test mode:    10-fold cross-validation

=== Classifier model (full training set) ===

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(33): class 
0Pitch(Hz)(93): class 
0RawPitch(72): class 
0SmPitch(93): class 
0Melogram(st)(91): class 
0ZCross(20): class 
0F1(Hz)(2623): class 
0F2(Hz)(3318): class 
0F3(Hz)(3383): class 
0F4(Hz)(3362): class 
class(10): 
LogScore Bayes: -5838130.03037705
LogScore BDeu: -7514570.559201219
LogScore MDL: -7099599.077718435
LogScore ENTROPY: -6307053.92384234
LogScore AIC: -6437842.923842339


Time taken to build model: 3.04 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances      148183               80.7959 %
Incorrectly Classified Instances     35221               19.2041 %
Kappa statistic                          0.7457
Mean absolute error                      0.0392
Root mean squared error                  0.1858
Relative absolute error                 25.4798 %
Root relative squared error             66.9712 %
Coverage of cases (0.95 level)          84.7855 %
Mean rel. region size (0.95 level)      11.8024 %
Total Number of Instances           183404     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,863    0,009    0,358      0,863    0,506      0,553    0,954     0,823     C
                 0,866    0,015    0,822      0,866    0,844      0,831    0,969     0,893     E
                 0,651    0,006    0,599      0,651    0,624      0,619    0,919     0,647     FV
                 0,863    0,020    0,912      0,863    0,887      0,861    0,964     0,933     AI
                 0,518    0,022    0,812      0,518    0,632      0,601    0,957     0,816     CDGKNRSYZ
                 0,809    0,011    0,852      0,809    0,830      0,817    0,930     0,857     O
                 0,915    0,162    0,785      0,915    0,845      0,738    0,972     0,963     0
                 0,540    0,007    0,738      0,540    0,623      0,620    0,926     0,620     LT
                 0,914    0,006    0,769      0,914    0,835      0,834    0,981     0,925     U
                 0,714    0,006    0,787      0,714    0,749      0,742    0,920     0,752     MBP
Weighted Avg.    0,808    0,074    0,814      0,808    0,803      0,748    0,961     0,896     

=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   935    20     1    29     8     4    78     2     4     2 |     a = C
    98 11905   109   238   435    71   580   107    76   121 |     b = E
    16    49  1666    47    73    20   673     6     8     3 |     c = FV
   401   377   190 30770   872   270  2023   354   115   268 |     d = AI
   517   927    76   933 14731   653  9992   185   304   141 |     e = CDGKNRSYZ
   149   102    49   213   231 10705  1492    93    65   135 |     f = O
   380   927   641  1168  1056   652 65965   462   481   385 |     g = 0
    38    98    17   181   493    86  2064  3571    30    41 |     h = LT
    39    19    12    32   104    25    75    25  3781    25 |     i = U
    37    56    18   144   148    81  1090    33    53  4154 |     j = MBP

