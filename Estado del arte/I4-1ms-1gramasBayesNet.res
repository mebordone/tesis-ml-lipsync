=== Run information ===

Scheme:       weka.classifiers.bayes.BayesNet -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5
Relation:     I4-1ms-1gramasfeatures2
Instances:    183404
Attributes:   14
              0Int(dB)
              0Pitch(Hz)
              0RawPitch
              0SmPitch
              0Melogram(st)
              0ZCross
              0F1(Hz)
              0F2(Hz)
              0F3(Hz)
              0F4(Hz)
              0F2menosF1
              02F1menosF2
              0F2masF1
              class
Test mode:    10-fold cross-validation

=== Classifier model (full training set) ===

Bayes Network Classifier
not using ADTree
#attributes=14 #classindex=13
Network structure (nodes followed by parents)
0Int(dB)(33): class 
0Pitch(Hz)(93): class 
0RawPitch(72): class 
0SmPitch(93): class 
0Melogram(st)(91): class 
0ZCross(20): class 
0F1(Hz)(2623): class 
0F2(Hz)(3318): class 
0F3(Hz)(3383): class 
0F4(Hz)(3362): class 
0F2menosF1(1697): class 
02F1menosF2(1733): class 
0F2masF1(1666): class 
class(10): 
LogScore Bayes: -7935717.195562549
LogScore BDeu: -1.022558543119603E7
LogScore MDL: -9666913.853614412
LogScore ENTROPY: -8565746.990826175
LogScore AIC: -8747465.990826175


Time taken to build model: 4.25 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances      150648               82.14   %
Incorrectly Classified Instances     32756               17.86   %
Kappa statistic                          0.7649
Mean absolute error                      0.0363
Root mean squared error                  0.182 
Relative absolute error                 23.6073 %
Root relative squared error             65.588  %
Coverage of cases (0.95 level)          84.5576 %
Mean rel. region size (0.95 level)      11.1501 %
Total Number of Instances           183404     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,858    0,006    0,451      0,858    0,591      0,619    0,954     0,834     C
                 0,880    0,021    0,770      0,880    0,821      0,808    0,967     0,889     E
                 0,643    0,005    0,653      0,643    0,648      0,643    0,922     0,650     FV
                 0,862    0,022    0,906      0,862    0,883      0,857    0,963     0,928     AI
                 0,610    0,024    0,826      0,610    0,702      0,667    0,956     0,822     CDGKNRSYZ
                 0,814    0,013    0,835      0,814    0,824      0,811    0,931     0,852     O
                 0,911    0,132    0,817      0,911    0,861      0,767    0,974     0,964     0
                 0,535    0,007    0,730      0,535    0,617      0,613    0,926     0,616     LT
                 0,911    0,006    0,793      0,911    0,848      0,846    0,981     0,923     U
                 0,710    0,007    0,780      0,710    0,744      0,737    0,923     0,748     MBP
Weighted Avg.    0,821    0,063    0,824      0,821    0,818      0,768    0,961     0,896     

=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   929    28     0    28     6     4    78     1     8     1 |     a = C
    69 12090    84   194   417    72   537   108    73    96 |     b = E
     5    65  1646    51    72    27   679     5     4     7 |     c = FV
   262   593   176 30721   911   310  1900   318   140   309 |     d = AI
   296  1283    59  1055 17372   755  6955   211   284   189 |     e = CDGKNRSYZ
    91   159    44   201   251 10773  1465   110    48    92 |     f = O
   321  1237   489  1291  1087   774 65678   490   330   420 |     g = 0
    24   135    12   186   643    93  1928  3539    36    23 |     h = LT
    35    35     4    31    91    22    84    39  3770    26 |     i = U
    30    72     8   151   194    78  1058    30    63  4130 |     j = MBP

