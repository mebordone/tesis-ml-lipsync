=== Run information ===

Scheme:       weka.classifiers.trees.RandomForest -I 10 -K 0 -S 1 -num-slots 1
Relation:     I4-1ms-1gramas
Instances:    183404
Attributes:   11
              0Int(dB)
              0Pitch(Hz)
              0RawPitch
              0SmPitch
              0Melogram(st)
              0ZCross
              0F1(Hz)
              0F2(Hz)
              0F3(Hz)
              0F4(Hz)
              class
Test mode:    10-fold cross-validation

=== Classifier model (full training set) ===

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1004



Time taken to build model: 26.51 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances      168001               91.6016 %
Incorrectly Classified Instances     15403                8.3984 %
Kappa statistic                          0.8895
Mean absolute error                      0.0272
Root mean squared error                  0.1124
Relative absolute error                 17.6856 %
Root relative squared error             40.5209 %
Coverage of cases (0.95 level)          98.4684 %
Mean rel. region size (0.95 level)      18.6797 %
Total Number of Instances           183404     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,898    0,000    0,975      0,898    0,935      0,935    0,978     0,933     C
                 0,942    0,003    0,964      0,942    0,953      0,949    0,992     0,972     E
                 0,713    0,000    0,963      0,713    0,819      0,826    0,981     0,815     FV
                 0,935    0,006    0,974      0,935    0,954      0,944    0,990     0,977     AI
                 0,877    0,036    0,816      0,877    0,845      0,816    0,984     0,936     CDGKNRSYZ
                 0,870    0,002    0,977      0,870    0,920      0,916    0,985     0,935     O
                 0,966    0,067    0,903      0,966    0,934      0,889    0,991     0,985     0
                 0,648    0,002    0,928      0,648    0,763      0,769    0,961     0,773     LT
                 0,959    0,000    0,979      0,959    0,969      0,968    0,992     0,980     U
                 0,779    0,001    0,967      0,779    0,863      0,865    0,977     0,873     MBP
Weighted Avg.    0,916    0,034    0,919      0,916    0,915      0,891    0,988     0,957     

=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   972    11     0    20    34     1    43     0     2     0 |     a = C
     3 12948    10    32   335    14   341    27     6    24 |     b = E
     0    23  1825    27   565     7   110     4     0     0 |     c = FV
    10    60    19 33326   853    38  1210    69    12    43 |     d = AI
     3   150    15   257 24945    76  2853   109    31    20 |     e = CDGKNRSYZ
     1    28     8    64   411 11513  1158    29     7    15 |     f = O
     6   116     9   232  1909    62 69687    58    13    25 |     g = 0
     0    50     4   120   921    36  1179  4287     4    18 |     h = LT
     2     8     2    26    61    11    41    11  3966     9 |     i = U
     0    37     3    97   524    25   559    27    10  4532 |     j = MBP

