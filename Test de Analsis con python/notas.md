#Notas PyVoPoRe

Estas notas pretenden dar cuenta de los avances que voy haciendo en el desarrollo de un sistema de Reconocimiento de posiciones Bucales en python, tomando como base el trabajo de tesis de Matas Eduardo Bordone Carranza - Famaf - UNC 2015

El sistema debera tomar un archivo de audio con el dialogo de un personaje de animacin y debera devolver un archivo de papagayo con las posiciones bucales. El mismo luego podra ser importado luego por Blender.

##26/04/2017

Analisis de audio con python. Probamos pyAudioAnalysis

pip install numpy matplotlib scipy sklearn hmmlearn simplejson eyed3 pydub
git clone https://github.com/tyiannak/pyAudioAnalysis.git

Ejemplo de extraccion de features tomado de 
https://github.com/tyiannak/pyAudioAnalysis/wiki/3.-Feature-Extraction

from pyAudioAnalysis import audioBasicIO
(tira warning de pydub, que no encuentra ffmpeg)
from pyAudioAnalysis import audioFeatureExtraction
No module named _tkinter, please install the python-tk package

prueba:
from pydub import AudioSegment
from pydub.playback import play
song = AudioSegment.from_wav("../Audios/Crudos/I02F1-MVI_9144.wav")
play(song)
Resultado
OSError: [Errno 2] No such file or directory

Ahora pruebo
sudo apt-get install ffmpeg
y si, no estaba instalado

Ahora si reprodujo el audio y no tiro errores.

Consejo, para hacer el banco de pruebas entonces
sudo apt-get install ffmpeg python-tk
pip install numpy matplotlib scipy sklearn hmmlearn simplejson eyed3 pydub

Pude extraer las features que propone la libreria de mis archivos.

Proximo sprint, extraer formantes y univicar los etiquetados para el aprendizaje.
Acercar el formato de salida de la libreria al formato que estaba utilizando (con segundos para identificar las instancias y utilizando las etiquetas)

## 27/04/2017
Experimentos con pydub libreria que usa PyAudioAnalisis para levantar y trabajar con los audios.
La idea es entender como trabaja AudioSegment, la abstraccion de audio que usan ambas librerias
Para eso estoy arendiendo debugging con **pdb**
https://pythonconquerstheuniverse.wordpress.com/2009/09/10/debugging-in-python/

No saque mucho, estoy todavia inconforme. Por un lado quiero hacer feature extraction, pero por el otro hay mucha abstraccion en esas librerias yodavia.

##28/04/2017

Encontre una pagina para hacer audio processing con scypy
http://samcarcagno.altervista.org/blog/basic-sound-processing-python/
Mejor quedar con las librerias básicas. Ir construyendo apra entender.
Aqui muestra como calcular fft.

No me resulta comodo la abstraccion

Tercera libreria de prueba Thinkdsp, hice un archivo de test. parece bastante piola para maniobrar los segmentos de audio.

Balance de las 3 opciones
Tareas que debería hacer:
- Abrir un audio
- Dividirlo en segmentos chiquitos (10ms o menos)
- Cada segmento sacar los formantes
	- calcularle el fft (me deberia dar un array con los coeficientes de cada frecuencia presente)
	- de ese segmento tomar los 5 picos y guardarlos las frecuencias en orden (no importan los coeficientes)

Scypy
http://samcarcagno.altervista.org/blog/basic-sound-processing-python/
permite manipulacion de audio y calculo de fft con la libreria directamente. No me queda muy comodo la manipulacion de audio.

thinkDsp
http://greenteapress.com/thinkdsp/html/thinkdsp002.html
esta un poco mas piola para manejar el sonido y graficar las cosas

pyAudioAnalisis
https://github.com/tyiannak/pyAudioAnalysis/wiki/3.-Feature-Extraction
para hacer aprendizaje automatico sobre audio es la papa, pero no me da los formantes sino que utiliza otros features.
la Astraccion se audio que usa es AudioSegment de pydub, no termino de entenderla.

