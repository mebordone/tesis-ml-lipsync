from thinkDSP import thinkdsp,thinkplot
import matplotlib

wave = thinkdsp.read_wave('../Audios/Crudos/I01F1-MVI_9143.wav')
start = 1.2
duration = 0.6
segment = wave.segment(start, duration)
segment.plot()
thinkplot.config(xlabel='Time (s)')
matplotlib.pyplot.show()


