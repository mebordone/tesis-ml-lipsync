from pylab import*
from scipy.io import wavfile

sampFreq, s1 = wavfile.read('../Audios/Crudos/I01F1-MVI_9143.wav')
s1 = s1 / (2.**15) #convert int -2^15 to (2^15)-1 to float -1,1
samples = s1.shape[0] #cantidad de muestras
time = (samples/sampFreq *1000) #tiempo de grabacion en ms

