Timestamp:2013-08-30-05:19:00
Inventario:I4
Intervalo:2ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I4-2ms-1gramas
Num Instances:  91703
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(22): class 
0Pitch(Hz)(47): class 
0RawPitch(34): class 
0SmPitch(39): class 
0Melogram(st)(45): class 
0ZCross(18): class 
0F1(Hz)(1497): class 
0F2(Hz)(1915): class 
0F3(Hz)(1983): class 
0F4(Hz)(2163): class 
class(10): 
LogScore Bayes: -2654284.8246939806
LogScore BDeu: -3589819.2908437685
LogScore MDL: -3355791.631732135
LogScore ENTROPY: -2912799.2917227917
LogScore AIC: -2990338.291722792




=== Stratified cross-validation ===

Correctly Classified Instances       68588               74.7936 %
Incorrectly Classified Instances     23115               25.2064 %
Kappa statistic                          0.6672
K&B Relative Info Score            6253311.2932 %
K&B Information Score               159386.2801 bits      1.7381 bits/instance
Class complexity | order 0          233717.1164 bits      2.5486 bits/instance
Class complexity | scheme           273275.636  bits      2.98   bits/instance
Complexity improvement     (Sf)     -39558.5196 bits     -0.4314 bits/instance
Mean absolute error                      0.0535
Root mean squared error                  0.2065
Relative absolute error                 34.6732 %
Root relative squared error             74.375  %
Coverage of cases (0.95 level)          82.8501 %
Mean rel. region size (0.95 level)      14.6795 %
Total Number of Instances            91703     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,761    0,010    0,303      0,761    0,434      0,476    0,944     0,652     C
                 0,807    0,039    0,630      0,807    0,708      0,687    0,951     0,794     E
                 0,512    0,006    0,566      0,512    0,538      0,532    0,887     0,534     FV
                 0,811    0,047    0,806      0,811    0,809      0,762    0,947     0,881     AI
                 0,364    0,026    0,717      0,364    0,483      0,453    0,914     0,664     CDGKNRSYZ
                 0,734    0,018    0,759      0,734    0,746      0,727    0,916     0,786     O
                 0,907    0,162    0,783      0,907    0,840      0,731    0,966     0,958     0
                 0,401    0,007    0,679      0,401    0,504      0,509    0,878     0,502     LT
                 0,852    0,009    0,687      0,852    0,761      0,759    0,973     0,863     U
                 0,597    0,009    0,679      0,597    0,635      0,626    0,887     0,644     MBP
Weighted Avg.    0,748    0,082    0,749      0,748    0,734      0,675    0,943     0,836     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   414    37     0    27    13     4    38     4     3     4 |     a = C
   109  5577    49   366   250    62   289    63    54    91 |     b = E
    12    86   658    83    40    30   333    12    11    21 |     c = FV
   171   832   110 14514   556   294  1010   138    91   178 |     d = AI
   343  1147    61  1375  5192   520  5000   115   304   215 |     e = CDGKNRSYZ
   100   193    29   389   140  4876   746    39    85    49 |     f = O
   119   603   233   798   589   416 32510   213   150   202 |     g = 0
    46   202     6   218   299    95  1032  1334    49    42 |     h = LT
    17    41     8    55    58    50    37    21  1773    20 |     i = U
    34   136     8   183   106    75   544    27    62  1740 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
76.1	6.8	0.0	4.96	2.39	0.74	6.99	0.74	0.55	0.74	 a = C
1.58	80.71	0.71	5.3	3.62	0.9	4.18	0.91	0.78	1.32	 b = E
0.93	6.69	51.17	6.45	3.11	2.33	25.89	0.93	0.86	1.63	 c = FV
0.96	4.65	0.61	81.11	3.11	1.64	5.64	0.77	0.51	0.99	 d = AI
2.4	8.04	0.43	9.63	36.38	3.64	35.03	0.81	2.13	1.51	 e = CDGKNRSYZ
1.5	2.9	0.44	5.85	2.11	73.37	11.22	0.59	1.28	0.74	 f = O
0.33	1.68	0.65	2.23	1.64	1.16	90.73	0.59	0.42	0.56	 g = 0
1.38	6.08	0.18	6.56	9.0	2.86	31.06	40.14	1.47	1.26	 h = LT
0.82	1.97	0.38	2.64	2.79	2.4	1.78	1.01	85.24	0.96	 i = U
1.17	4.67	0.27	6.28	3.64	2.57	18.66	0.93	2.13	59.69	 j = MBP
