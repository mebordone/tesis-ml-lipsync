Timestamp:2013-08-30-04:39:28
Inventario:I2
Intervalo:3ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I2-3ms-5gramas
Num Instances:  61131
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(19): class 
0Pitch(Hz)(28): class 
0RawPitch(22): class 
0SmPitch(23): class 
0Melogram(st)(31): class 
0ZCross(14): class 
0F1(Hz)(215): class 
0F2(Hz)(298): class 
0F3(Hz)(318): class 
0F4(Hz)(285): class 
1Int(dB)(20): class 
1Pitch(Hz)(26): class 
1RawPitch(22): class 
1SmPitch(24): class 
1Melogram(st)(30): class 
1ZCross(15): class 
1F1(Hz)(247): class 
1F2(Hz)(262): class 
1F3(Hz)(351): class 
1F4(Hz)(257): class 
2Int(dB)(20): class 
2Pitch(Hz)(28): class 
2RawPitch(22): class 
2SmPitch(23): class 
2Melogram(st)(32): class 
2ZCross(15): class 
2F1(Hz)(222): class 
2F2(Hz)(233): class 
2F3(Hz)(413): class 
2F4(Hz)(233): class 
3Int(dB)(21): class 
3Pitch(Hz)(28): class 
3RawPitch(19): class 
3SmPitch(23): class 
3Melogram(st)(35): class 
3ZCross(15): class 
3F1(Hz)(199): class 
3F2(Hz)(282): class 
3F3(Hz)(351): class 
3F4(Hz)(217): class 
4Int(dB)(19): class 
4Pitch(Hz)(27): class 
4RawPitch(21): class 
4SmPitch(24): class 
4Melogram(st)(36): class 
4ZCross(16): class 
4F1(Hz)(208): class 
4F2(Hz)(219): class 
4F3(Hz)(297): class 
4F4(Hz)(205): class 
class(12): 
LogScore Bayes: -6138481.854664361
LogScore BDeu: -6789167.8696730565
LogScore MDL: -6676080.309815192
LogScore ENTROPY: -6281916.803674763
LogScore AIC: -6353447.803674764




=== Stratified cross-validation ===

Correctly Classified Instances       38596               63.1365 %
Incorrectly Classified Instances     22535               36.8635 %
Kappa statistic                          0.5333
K&B Relative Info Score            3208095.7814 %
K&B Information Score                92079.122  bits      1.5063 bits/instance
Class complexity | order 0          175442.4274 bits      2.8699 bits/instance
Class complexity | scheme           999847.4905 bits     16.3558 bits/instance
Complexity improvement     (Sf)    -824405.0631 bits    -13.4859 bits/instance
Mean absolute error                      0.0617
Root mean squared error                  0.2381
Relative absolute error                 46.4425 %
Root relative squared error             92.3857 %
Coverage of cases (0.95 level)          67.1901 %
Mean rel. region size (0.95 level)      10.0192 %
Total Number of Instances            61131     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,634    0,040    0,745      0,634    0,685      0,635    0,919     0,765     A
                 0,085    0,012    0,274      0,085    0,130      0,130    0,793     0,178     LGJKC
                 0,640    0,057    0,478      0,640    0,547      0,511    0,910     0,572     E
                 0,252    0,014    0,204      0,252    0,225      0,214    0,855     0,178     FV
                 0,695    0,031    0,474      0,695    0,564      0,554    0,932     0,597     I
                 0,438    0,022    0,607      0,438    0,509      0,484    0,872     0,547     O
                 0,253    0,029    0,475      0,253    0,330      0,299    0,873     0,412     SNRTY
                 0,893    0,133    0,810      0,893    0,850      0,749    0,955     0,943     0
                 0,205    0,008    0,466      0,205    0,284      0,294    0,839     0,294     BMP
                 0,542    0,046    0,149      0,542    0,234      0,265    0,913     0,285     DZ
                 0,648    0,017    0,469      0,648    0,544      0,539    0,950     0,634     U
                 0,419    0,030    0,372      0,419    0,394      0,368    0,882     0,348     SNRTXY
Weighted Avg.    0,631    0,071    0,634      0,631    0,619      0,564    0,914     0,685     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  6076   132   730   142   203   459   253   482    42   615   122   329 |     a = A
   226   253   379    65   307   131   290   808    22   211    87   184 |     b = LGJKC
   298    67  2956    69   294    70   142   179    68   238    70   170 |     c = E
    55     6   105   218    53    23    60   190    19    95    15    25 |     d = FV
    26    35   241    26  1655     3   109   121    26    47    30    63 |     e = I
   540    67   339    45    93  1956   109   463    48   438   225   140 |     f = O
   310   150   349   171   195   111  1469  2130    83   368   130   342 |     g = SNRTY
   239   143   374   191   263   237   373 21185    87   297    61   272 |     h = 0
   121    28   193    75   223    58    98   338   399   153   147   115 |     i = BMP
    73     2    90    26    40    38    17    29    14   490    32    53 |     j = DZ
    49    25    74    21    13    90    34    30    24    67   898    61 |     k = U
   140    16   352    22   150    45   139   190    25   266    99  1041 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
63.39	1.38	7.62	1.48	2.12	4.79	2.64	5.03	0.44	6.42	1.27	3.43	 a = A
7.63	8.54	12.79	2.19	10.36	4.42	9.79	27.27	0.74	7.12	2.94	6.21	 b = LGJKC
6.45	1.45	63.97	1.49	6.36	1.51	3.07	3.87	1.47	5.15	1.51	3.68	 c = E
6.37	0.69	12.15	25.23	6.13	2.66	6.94	21.99	2.2	11.0	1.74	2.89	 d = FV
1.09	1.47	10.12	1.09	69.48	0.13	4.58	5.08	1.09	1.97	1.26	2.64	 e = I
12.1	1.5	7.6	1.01	2.08	43.83	2.44	10.37	1.08	9.81	5.04	3.14	 f = O
5.34	2.58	6.01	2.94	3.36	1.91	25.29	36.67	1.43	6.34	2.24	5.89	 g = SNRTY
1.01	0.6	1.58	0.81	1.11	1.0	1.57	89.31	0.37	1.25	0.26	1.15	 h = 0
6.21	1.44	9.91	3.85	11.45	2.98	5.03	17.35	20.48	7.85	7.55	5.9	 i = BMP
8.08	0.22	9.96	2.88	4.42	4.2	1.88	3.21	1.55	54.2	3.54	5.86	 j = DZ
3.54	1.8	5.34	1.52	0.94	6.49	2.45	2.16	1.73	4.83	64.79	4.4	 k = U
5.63	0.64	14.16	0.89	6.04	1.81	5.59	7.65	1.01	10.7	3.98	41.89	 l = SNRTXY
