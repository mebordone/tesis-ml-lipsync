import os

sftfiles = []
for file in os.listdir(os.getcwd()):
		if '.sft' in file:
			sftfiles.append(file)
inventario = {
	'ch':'s',
	'rr':'l',
	'0':'0',
	'hs':'a',
	'hg':'a',
	'a':'a',
	'c':'k',
	'b':'b',
	'e':'e',
	'd':'d',
	'g':'j',
	'f':'f',
	'i':'i',
	'j':'j',
	'm':'b',
	'l':'l',
	'o':'o',
	'n':'l',
	'q':'k',
	'p':'b',
	's':'s',
	'r':'l',
	'u':'u',
	't':'l',
	'v':'f',
	'y':'s'}

data=[]
for filename in sftfiles:
	file = open (filename,'r')
	for line in file:
		if line.startswith('\\') or line.startswith('Time'):
			continue
		lineitems = line.split('\t')
		for i in range(0,len(lineitems)):
			if lineitems [i] == '':
				lineitems [i] = '0'
		pos = lineitems[1]
		if ' ' in pos:
			poss = pos.split(' ')
			pos = poss [1]
		del lineitems[1]
		lineitems.insert(len(lineitems)-1,pos)
		lineitems[len(lineitems)-1] = '\n'
		data.append(lineitems)
	file.close()
datasec = []
#~ for i in (range(len(data)-2)):
for i in (range(128)):
	sec1 = data [i][:]
	sec2 = data [i+1][:]
	sec3 = data [i+2][:]
	del sec1[-1]
	del sec1[-1]
	del sec2[-1]
	del sec2[-1]
	pos = sec3[-2]
	del sec3[-1]
	del sec3[-1]
	sec = "\"{}\\n{}\\n{}\",{}\n".format(','.join(sec1),','.join(sec2), ','.join(sec3),pos)
	datasec.append(sec)
#~ print datasec[:1]

arff = open ('speechhmm.arff','w')
arff.write("""@relation speechhmm
@attribute bag relational
	@attribute Time numeric
	@attribute Int(dB) numeric
	@attribute Pitch(Hz) numeric
	@attribute RawPitch numeric
	@attribute SmPitch numeric
	@attribute Melogram(st) numeric
	@attribute ZCross numeric
	@attribute F1(Hz) numeric
	@attribute F2(Hz) numeric
	@attribute F3(Hz) numeric
	@attribute F4(Hz) numeric
@end bag
@attribute class {'ch', 'rr', '0', 'hs', 'hg', 'a', 'c', 'b', 'e', 'd', 'g', 'f', 'i', 'j', 'm', 'l', 'o', 'n', 'q', 'p', 's', 'r', 'u', 't', 'v', 'y'}
@data
""")
for item in datasec:
	arff.write(item)
arff.close()
