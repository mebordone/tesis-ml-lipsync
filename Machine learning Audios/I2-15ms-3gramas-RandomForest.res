Timestamp:2013-07-22-21:57:30
Inventario:I2
Intervalo:15ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I2-15ms-3gramas
Num Instances:  12187
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.35





=== Stratified cross-validation ===

Correctly Classified Instances        8620               70.7311 %
Incorrectly Classified Instances      3567               29.2689 %
Kappa statistic                          0.6367
K&B Relative Info Score             749752.0345 %
K&B Information Score                21959.7138 bits      1.8019 bits/instance
Class complexity | order 0           35677.675  bits      2.9275 bits/instance
Class complexity | scheme           958672.5537 bits     78.6635 bits/instance
Complexity improvement     (Sf)    -922994.8787 bits    -75.736  bits/instance
Mean absolute error                      0.0656
Root mean squared error                  0.1819
Relative absolute error                 48.5001 %
Root relative squared error             69.9461 %
Coverage of cases (0.95 level)          92.7546 %
Mean rel. region size (0.95 level)      25.9149 %
Total Number of Instances            12187     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,825    0,073    0,689      0,825    0,751      0,701    0,943     0,841     A
                 0,318    0,029    0,364      0,318    0,339      0,308    0,802     0,267     LGJKC
                 0,665    0,044    0,569      0,665    0,613      0,579    0,923     0,627     E
                 0,113    0,005    0,250      0,113    0,156      0,160    0,803     0,130     FV
                 0,626    0,017    0,610      0,626    0,618      0,602    0,927     0,624     I
                 0,601    0,032    0,609      0,601    0,605      0,573    0,896     0,611     O
                 0,513    0,055    0,509      0,513    0,511      0,457    0,873     0,510     SNRTY
                 0,915    0,054    0,906      0,915    0,911      0,859    0,975     0,956     0
                 0,361    0,008    0,590      0,361    0,448      0,448    0,857     0,386     BMP
                 0,170    0,004    0,378      0,170    0,235      0,246    0,866     0,235     DZ
                 0,596    0,005    0,752      0,596    0,665      0,663    0,936     0,683     U
                 0,456    0,013    0,604      0,456    0,520      0,507    0,908     0,513     SNRTXY
Weighted Avg.    0,707    0,046    0,698      0,707    0,699      0,659    0,927     0,726     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1640   44   79    2   17   55   70   50    7    5    2   18 |    a = A
  107  190   57    9   25   29   94   67    5    0    3   12 |    b = LGJKC
  101   50  646    4   45   31   36   13   10   10    5   20 |    c = E
   21   19   28   20    5   12   46   11    5    1    3    6 |    d = FV
   24   18   73    2  315    4   27   17   12    1    3    7 |    e = I
  115   28   51    5    5  565   42   64   18   15   16   16 |    f = O
  167   71   67   19   29   53  625  143   15    4    3   22 |    g = SNRTY
   64   37   20   11   26   34  160 4042    8    1    3   10 |    h = 0
   45   23   37    5   23   32   36   36  144    1    6   11 |    i = BMP
   30    7   19    0    6   42   18    2    3   31    3   21 |    j = DZ
   20    7   14    0    2   44   10    0    8    1  170    9 |    k = U
   47   28   44    3   18   26   64   17    9   12    9  232 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
82.45	2.21	3.97	0.1	0.85	2.77	3.52	2.51	0.35	0.25	0.1	0.9	 a = A
17.89	31.77	9.53	1.51	4.18	4.85	15.72	11.2	0.84	0.0	0.5	2.01	 b = LGJKC
10.4	5.15	66.53	0.41	4.63	3.19	3.71	1.34	1.03	1.03	0.51	2.06	 c = E
11.86	10.73	15.82	11.3	2.82	6.78	25.99	6.21	2.82	0.56	1.69	3.39	 d = FV
4.77	3.58	14.51	0.4	62.62	0.8	5.37	3.38	2.39	0.2	0.6	1.39	 e = I
12.23	2.98	5.43	0.53	0.53	60.11	4.47	6.81	1.91	1.6	1.7	1.7	 f = O
13.71	5.83	5.5	1.56	2.38	4.35	51.31	11.74	1.23	0.33	0.25	1.81	 g = SNRTY
1.45	0.84	0.45	0.25	0.59	0.77	3.62	91.53	0.18	0.02	0.07	0.23	 h = 0
11.28	5.76	9.27	1.25	5.76	8.02	9.02	9.02	36.09	0.25	1.5	2.76	 i = BMP
16.48	3.85	10.44	0.0	3.3	23.08	9.89	1.1	1.65	17.03	1.65	11.54	 j = DZ
7.02	2.46	4.91	0.0	0.7	15.44	3.51	0.0	2.81	0.35	59.65	3.16	 k = U
9.23	5.5	8.64	0.59	3.54	5.11	12.57	3.34	1.77	2.36	1.77	45.58	 l = SNRTXY
