Timestamp:2013-07-23-03:02:32
Inventario:I3
Intervalo:6ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I3-6ms-3gramas
Num Instances:  30451
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.2271





=== Stratified cross-validation ===

Correctly Classified Instances       25067               82.3191 %
Incorrectly Classified Instances      5384               17.6809 %
Kappa statistic                          0.7722
K&B Relative Info Score            2234162.0589 %
K&B Information Score                58411.497  bits      1.9182 bits/instance
Class complexity | order 0           79593.2678 bits      2.6138 bits/instance
Class complexity | scheme          1316896.2459 bits     43.2464 bits/instance
Complexity improvement     (Sf)    -1237302.9782 bits    -40.6326 bits/instance
Mean absolute error                      0.0511
Root mean squared error                  0.156 
Relative absolute error                 36.0473 %
Root relative squared error             58.5711 %
Coverage of cases (0.95 level)          96.0264 %
Mean rel. region size (0.95 level)      21.7592 %
Total Number of Instances            30451     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,863    0,034    0,825      0,863    0,844      0,814    0,964     0,906     A
                 0,798    0,022    0,753      0,798    0,775      0,756    0,962     0,828     E
                 0,785    0,081    0,688      0,785    0,733      0,670    0,934     0,778     CGJLNQSRT
                 0,381    0,002    0,756      0,381    0,507      0,533    0,902     0,476     FV
                 0,761    0,007    0,817      0,761    0,788      0,780    0,954     0,813     I
                 0,722    0,013    0,817      0,722    0,767      0,751    0,939     0,799     O
                 0,906    0,053    0,913      0,906    0,910      0,855    0,972     0,953     0
                 0,552    0,005    0,791      0,552    0,650      0,652    0,897     0,634     BMP
                 0,796    0,002    0,916      0,796    0,852      0,851    0,968     0,878     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,540     0,001     DZ
                 0,599    0,002    0,835      0,599    0,697      0,704    0,977     0,744     D
Weighted Avg.    0,823    0,044    0,827      0,823    0,822      0,782    0,956     0,864     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  4165    84   330     7    16    56   141    13     8     0     5 |     a = A
   115  1872   223    11    27    20    47    18     2     0    12 |     b = E
   340   173  4440    23    59    84   473    36    14     4     7 |     c = CGJLNQSRT
    25    39   146   164     6    11    26    11     2     0     0 |     d = FV
    24    76   119     2   918     6    43    16     1     0     1 |     e = I
   133    50   224     1     5  1626   174    13    10     0    15 |     f = O
   139   103   649     7    56    93 10522    22     8     1     9 |     g = 0
    58    41   198     1    26    32    78   541     4     0     1 |     h = BMP
    19    13    60     1     6    23     6    12   555     0     2 |     i = U
     1     0     9     0     0     0     1     1     0     0     0 |     j = DZ
    30    34    57     0     4    38    11     1     2     0   264 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
86.32	1.74	6.84	0.15	0.33	1.16	2.92	0.27	0.17	0.0	0.1	 a = A
4.9	79.76	9.5	0.47	1.15	0.85	2.0	0.77	0.09	0.0	0.51	 b = E
6.01	3.06	78.54	0.41	1.04	1.49	8.37	0.64	0.25	0.07	0.12	 c = CGJLNQSRT
5.81	9.07	33.95	38.14	1.4	2.56	6.05	2.56	0.47	0.0	0.0	 d = FV
1.99	6.3	9.87	0.17	76.12	0.5	3.57	1.33	0.08	0.0	0.08	 e = I
5.91	2.22	9.95	0.04	0.22	72.23	7.73	0.58	0.44	0.0	0.67	 f = O
1.2	0.89	5.59	0.06	0.48	0.8	90.64	0.19	0.07	0.01	0.08	 g = 0
5.92	4.18	20.2	0.1	2.65	3.27	7.96	55.2	0.41	0.0	0.1	 h = BMP
2.73	1.87	8.61	0.14	0.86	3.3	0.86	1.72	79.63	0.0	0.29	 i = U
8.33	0.0	75.0	0.0	0.0	0.0	8.33	8.33	0.0	0.0	0.0	 j = DZ
6.8	7.71	12.93	0.0	0.91	8.62	2.49	0.23	0.45	0.0	59.86	 k = D
