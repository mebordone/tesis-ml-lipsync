Timestamp:2013-08-30-04:41:39
Inventario:I2
Intervalo:10ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I2-10ms-1gramas
Num Instances:  18296
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(10): class 
0Pitch(Hz)(7): class 
0RawPitch(6): class 
0SmPitch(7): class 
0Melogram(st)(8): class 
0ZCross(12): class 
0F1(Hz)(12): class 
0F2(Hz)(11): class 
0F3(Hz)(10): class 
0F4(Hz)(8): class 
class(12): 
LogScore Bayes: -243445.46099177352
LogScore BDeu: -248021.1927892887
LogScore MDL: -247500.21612463563
LogScore ENTROPY: -242676.4199775378
LogScore AIC: -243659.4199775378




=== Stratified cross-validation ===

Correctly Classified Instances       11141               60.8931 %
Incorrectly Classified Instances      7155               39.1069 %
Kappa statistic                          0.503 
K&B Relative Info Score             911980.9448 %
K&B Information Score                26531.1421 bits      1.4501 bits/instance
Class complexity | order 0           53208.3385 bits      2.9082 bits/instance
Class complexity | scheme            70291.1466 bits      3.8419 bits/instance
Complexity improvement     (Sf)     -17082.8081 bits     -0.9337 bits/instance
Mean absolute error                      0.0712
Root mean squared error                  0.2202
Relative absolute error                 52.9273 %
Root relative squared error             84.9102 %
Coverage of cases (0.95 level)          78.3067 %
Mean rel. region size (0.95 level)      23.0533 %
Total Number of Instances            18296     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,733    0,084    0,625      0,733    0,675      0,609    0,911     0,741     A
                 0,019    0,006    0,143      0,019    0,034      0,035    0,734     0,104     LGJKC
                 0,636    0,078    0,409      0,636    0,497      0,458    0,900     0,513     E
                 0,050    0,006    0,105      0,050    0,068      0,063    0,778     0,046     FV
                 0,636    0,035    0,439      0,636    0,520      0,505    0,925     0,521     I
                 0,380    0,031    0,501      0,380    0,432      0,397    0,839     0,438     O
                 0,183    0,033    0,377      0,183    0,247      0,210    0,856     0,333     SNRTY
                 0,923    0,135    0,802      0,923    0,858      0,770    0,968     0,960     0
                 0,032    0,007    0,141      0,032    0,052      0,053    0,781     0,113     BMP
                 0,208    0,013    0,192      0,208    0,200      0,187    0,860     0,129     DZ
                 0,473    0,014    0,443      0,473    0,458      0,445    0,931     0,500     U
                 0,216    0,028    0,248      0,216    0,231      0,201    0,841     0,189     SNRTXY
Weighted Avg.    0,609    0,079    0,559      0,609    0,572      0,517    0,903     0,634     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 2155   12  254   20   51  112   93  137    4   25   12   65 |    a = A
  123   17  175   12   86   43  102  228    9   29   19   51 |    b = LGJKC
  171   18  914    7   93   33   42   53   14   16   21   56 |    c = E
   35    2   46   13   13   24   24   56   10   17    5   16 |    d = FV
   15    6  141    5  476    2   37   36    5    0    6   19 |    e = I
  316   10  137   10   22  523   35  143    5   51   83   42 |    f = O
  220   27  139   17   88   55  328  730   17   45   16  108 |    g = SNRTY
  124   10   90   28   61   45  102 6275    8   12    9   34 |    h = 0
   72   10  100    6   99   48   36   90   19   13   45   56 |    i = BMP
   53    0   55    1   12   37    6    9   10   57    7   27 |    j = DZ
   32    4   44    0    3   81   10    8    8   11  200   22 |    k = U
  133    3  142    5   80   40   54   63   26   21   28  164 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
73.3	0.41	8.64	0.68	1.73	3.81	3.16	4.66	0.14	0.85	0.41	2.21	 a = A
13.76	1.9	19.57	1.34	9.62	4.81	11.41	25.5	1.01	3.24	2.13	5.7	 b = LGJKC
11.89	1.25	63.56	0.49	6.47	2.29	2.92	3.69	0.97	1.11	1.46	3.89	 c = E
13.41	0.77	17.62	4.98	4.98	9.2	9.2	21.46	3.83	6.51	1.92	6.13	 d = FV
2.01	0.8	18.85	0.67	63.64	0.27	4.95	4.81	0.67	0.0	0.8	2.54	 e = I
22.95	0.73	9.95	0.73	1.6	37.98	2.54	10.38	0.36	3.7	6.03	3.05	 f = O
12.29	1.51	7.77	0.95	4.92	3.07	18.32	40.78	0.95	2.51	0.89	6.03	 g = SNRTY
1.82	0.15	1.32	0.41	0.9	0.66	1.5	92.31	0.12	0.18	0.13	0.5	 h = 0
12.12	1.68	16.84	1.01	16.67	8.08	6.06	15.15	3.2	2.19	7.58	9.43	 i = BMP
19.34	0.0	20.07	0.36	4.38	13.5	2.19	3.28	3.65	20.8	2.55	9.85	 j = DZ
7.57	0.95	10.4	0.0	0.71	19.15	2.36	1.89	1.89	2.6	47.28	5.2	 k = U
17.52	0.4	18.71	0.66	10.54	5.27	7.11	8.3	3.43	2.77	3.69	21.61	 l = SNRTXY
