Timestamp:2013-07-22-22:33:09
Inventario:I3
Intervalo:20ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I3-20ms-4gramas
Num Instances:  9126
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3376





=== Stratified cross-validation ===

Correctly Classified Instances        6571               72.0031 %
Incorrectly Classified Instances      2555               27.9969 %
Kappa statistic                          0.6428
K&B Relative Info Score             549927.3464 %
K&B Information Score                14690.3899 bits      1.6097 bits/instance
Class complexity | order 0           24347.7647 bits      2.668  bits/instance
Class complexity | scheme           593033.6494 bits     64.9829 bits/instance
Complexity improvement     (Sf)    -568685.8847 bits    -62.3149 bits/instance
Mean absolute error                      0.0706
Root mean squared error                  0.1882
Relative absolute error                 48.911  %
Root relative squared error             70.0455 %
Coverage of cases (0.95 level)          94.0281 %
Mean rel. region size (0.95 level)      26.8573 %
Total Number of Instances             9126     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,765    0,063    0,706      0,765    0,734      0,680    0,928     0,808     A
                 0,619    0,041    0,572      0,619    0,595      0,557    0,914     0,583     E
                 0,704    0,136    0,551      0,704    0,618      0,520    0,878     0,614     CGJLNQSRT
                 0,065    0,002    0,333      0,065    0,109      0,142    0,776     0,110     FV
                 0,545    0,012    0,675      0,545    0,603      0,592    0,916     0,570     I
                 0,537    0,028    0,619      0,537    0,575      0,543    0,888     0,573     O
                 0,913    0,045    0,918      0,913    0,915      0,869    0,977     0,957     0
                 0,194    0,006    0,527      0,194    0,284      0,307    0,840     0,290     BMP
                 0,459    0,004    0,752      0,459    0,570      0,580    0,912     0,580     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,798     0,066     DZ
                 0,105    0,002    0,412      0,105    0,168      0,203    0,785     0,138     D
Weighted Avg.    0,720    0,059    0,717      0,720    0,710      0,662    0,923     0,734     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1151   59  199    0   10   43   34    4    2    0    3 |    a = A
   74  462  140    1   23   22   11    9    3    0    1 |    b = E
  166  112 1233    6   26   53  127   11   11    1    5 |    c = CGJLNQSRT
   22   17   77    9    0    8    1    1    1    0    2 |    d = FV
   15   52   83    0  210    2   14    8    1    0    0 |    e = I
  103   36  108    2    4  382   55    8    6    0    8 |    f = O
   34   15  188    3   10   29 2952    3    0    0    0 |    g = 0
   27   32  109    5   22   19   19   58    8    0    0 |    h = BMP
   18    9   44    0    4   35    0    7  100    0    1 |    i = U
    0    0    3    0    0    0    2    0    0    0    0 |    j = DZ
   21   14   55    1    2   24    0    1    1    0   14 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
76.48	3.92	13.22	0.0	0.66	2.86	2.26	0.27	0.13	0.0	0.2	 a = A
9.92	61.93	18.77	0.13	3.08	2.95	1.47	1.21	0.4	0.0	0.13	 b = E
9.48	6.4	70.42	0.34	1.48	3.03	7.25	0.63	0.63	0.06	0.29	 c = CGJLNQSRT
15.94	12.32	55.8	6.52	0.0	5.8	0.72	0.72	0.72	0.0	1.45	 d = FV
3.9	13.51	21.56	0.0	54.55	0.52	3.64	2.08	0.26	0.0	0.0	 e = I
14.47	5.06	15.17	0.28	0.56	53.65	7.72	1.12	0.84	0.0	1.12	 f = O
1.05	0.46	5.81	0.09	0.31	0.9	91.28	0.09	0.0	0.0	0.0	 g = 0
9.03	10.7	36.45	1.67	7.36	6.35	6.35	19.4	2.68	0.0	0.0	 h = BMP
8.26	4.13	20.18	0.0	1.83	16.06	0.0	3.21	45.87	0.0	0.46	 i = U
0.0	0.0	60.0	0.0	0.0	0.0	40.0	0.0	0.0	0.0	0.0	 j = DZ
15.79	10.53	41.35	0.75	1.5	18.05	0.0	0.75	0.75	0.0	10.53	 k = D
