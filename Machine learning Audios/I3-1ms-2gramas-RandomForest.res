Timestamp:2013-07-22-22:01:35
Inventario:I3
Intervalo:1ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I3-1ms-2gramas
Num Instances:  183403
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1056





=== Stratified cross-validation ===

Correctly Classified Instances      167325               91.2335 %
Incorrectly Classified Instances     16078                8.7665 %
Kappa statistic                          0.886 
K&B Relative Info Score            16027049.8282 %
K&B Information Score               414976.4411 bits      2.2626 bits/instance
Class complexity | order 0          474844.9163 bits      2.5891 bits/instance
Class complexity | scheme          6115659.5048 bits     33.3455 bits/instance
Complexity improvement     (Sf)    -5640814.5885 bits    -30.7564 bits/instance
Mean absolute error                      0.0234
Root mean squared error                  0.1108
Relative absolute error                 16.6399 %
Root relative squared error             41.7638 %
Coverage of cases (0.95 level)          96.6647 %
Mean rel. region size (0.95 level)      13.8295 %
Total Number of Instances           183403     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,938    0,008    0,955      0,938    0,946      0,937    0,980     0,964     A
                 0,939    0,004    0,950      0,939    0,944      0,940    0,982     0,960     E
                 0,856    0,040    0,828      0,856    0,842      0,806    0,967     0,909     CGJLNQSRT
                 0,711    0,002    0,848      0,711    0,773      0,773    0,921     0,782     FV
                 0,919    0,002    0,955      0,919    0,937      0,934    0,977     0,947     I
                 0,871    0,004    0,944      0,871    0,906      0,900    0,962     0,918     O
                 0,945    0,055    0,918      0,945    0,931      0,886    0,983     0,969     0
                 0,783    0,003    0,889      0,783    0,833      0,829    0,934     0,842     BMP
                 0,955    0,001    0,977      0,955    0,966      0,965    0,992     0,979     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,545     0,001     DZ
                 0,943    0,000    0,972      0,943    0,958      0,957    0,996     0,982     D
Weighted Avg.    0,912    0,031    0,913      0,912    0,912      0,885    0,975     0,945     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 26780    51   801    23    25    47   748    62     8     5     7 |     a = A
    59 12905   410    18    22    21   250    35     6     2    12 |     b = E
   550   282 28672   168    75   218  3235   225    30    12    12 |     c = CGJLNQSRT
    27    24   520  1820    20    25   106    17     0     1     1 |     d = FV
    41    31   223    14  6512     8   224    19     9     0     2 |     e = I
    75    29   494    20    15 11527  1021    24     4     2    23 |     f = O
   385   182  2760    65   107   281 68141   163    19     2    11 |     g = 0
    90    52   562    13    33    40   458  4551    13     1     1 |     h = BMP
    12     9    92     2     7     8    39    18  3949     0     1 |     i = U
     2     2    41     2     1     2    15     0     0     0     0 |     j = DZ
    23    24    41     1     3    28    24     3     2     0  2468 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
93.78	0.18	2.8	0.08	0.09	0.16	2.62	0.22	0.03	0.02	0.02	 a = A
0.43	93.92	2.98	0.13	0.16	0.15	1.82	0.25	0.04	0.01	0.09	 b = E
1.64	0.84	85.64	0.5	0.22	0.65	9.66	0.67	0.09	0.04	0.04	 c = CGJLNQSRT
1.05	0.94	20.3	71.07	0.78	0.98	4.14	0.66	0.0	0.04	0.04	 d = FV
0.58	0.44	3.15	0.2	91.94	0.11	3.16	0.27	0.13	0.0	0.03	 e = I
0.57	0.22	3.73	0.15	0.11	87.1	7.71	0.18	0.03	0.02	0.17	 f = O
0.53	0.25	3.83	0.09	0.15	0.39	94.49	0.23	0.03	0.0	0.02	 g = 0
1.55	0.89	9.67	0.22	0.57	0.69	7.88	78.28	0.22	0.02	0.02	 h = BMP
0.29	0.22	2.22	0.05	0.17	0.19	0.94	0.44	95.46	0.0	0.02	 i = U
3.08	3.08	63.08	3.08	1.54	3.08	23.08	0.0	0.0	0.0	0.0	 j = DZ
0.88	0.92	1.57	0.04	0.11	1.07	0.92	0.11	0.08	0.0	94.31	 k = D
