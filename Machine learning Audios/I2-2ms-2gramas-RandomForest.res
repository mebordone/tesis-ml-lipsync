Timestamp:2013-07-22-21:42:11
Inventario:I2
Intervalo:2ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I2-2ms-2gramas
Num Instances:  91702
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1575





=== Stratified cross-validation ===

Correctly Classified Instances       79976               87.2129 %
Incorrectly Classified Instances     11726               12.7871 %
Kappa statistic                          0.8387
K&B Relative Info Score            7540940.9728 %
K&B Information Score               215922.5263 bits      2.3546 bits/instance
Class complexity | order 0          262555.2873 bits      2.8631 bits/instance
Class complexity | scheme          4996290.7578 bits     54.484  bits/instance
Complexity improvement     (Sf)    -4733735.4705 bits    -51.6209 bits/instance
Mean absolute error                      0.0317
Root mean squared error                  0.1288
Relative absolute error                 23.8706 %
Root relative squared error             50.0175 %
Coverage of cases (0.95 level)          94.6991 %
Mean rel. region size (0.95 level)      15.155  %
Total Number of Instances            91702     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,925    0,017    0,912      0,925    0,919      0,903    0,975     0,951     A
                 0,686    0,014    0,716      0,686    0,700      0,686    0,904     0,709     LGJKC
                 0,913    0,009    0,891      0,913    0,902      0,894    0,977     0,937     E
                 0,688    0,004    0,729      0,688    0,708      0,704    0,907     0,704     FV
                 0,897    0,004    0,900      0,897    0,898      0,894    0,971     0,922     I
                 0,850    0,009    0,886      0,850    0,868      0,858    0,951     0,888     O
                 0,700    0,033    0,687      0,700    0,694      0,661    0,928     0,721     SNRTY
                 0,927    0,058    0,911      0,927    0,919      0,867    0,975     0,954     0
                 0,740    0,005    0,816      0,740    0,776      0,770    0,923     0,785     BMP
                 0,872    0,001    0,920      0,872    0,895      0,894    0,981     0,921     DZ
                 0,928    0,001    0,962      0,928    0,945      0,944    0,987     0,961     U
                 0,835    0,005    0,886      0,835    0,860      0,854    0,955     0,879     SNRTXY
Weighted Avg.    0,872    0,031    0,872      0,872    0,872      0,843    0,962     0,900     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 13264   132    50    13    27    61   286   397    39    14     6    44 |     a = A
   171  3037    82    38    22    68   440   468    53     4    11    34 |     b = LGJKC
    90    72  6308    17    31    23   130   152    30    15     9    33 |     c = E
    32    43    30   885    10    21   181    63    10     2     0     9 |     d = FV
    27    46    50    12  3193     8    85   112    13     2     3    10 |     e = I
    95    68    50    25    15  5651   160   505    29    13     8    27 |     f = O
   337   359   149   132    75   147  6067  1122   126    14    15   120 |     g = SNRTY
   353   347   213    59   112   269  1001 33219   148    19    10    82 |     h = 0
    73    58    56    12    40    48   204   240  2158     4     7    15 |     i = BMP
    27     7    30     4     1    30    26    25     1  1174     3    18 |     j = DZ
    10    14    17     3    14    18    28    27    13     2  1930     4 |     k = U
    65    60    44    14     8    36   222   122    24    13     4  3090 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
92.54	0.92	0.35	0.09	0.19	0.43	2.0	2.77	0.27	0.1	0.04	0.31	 a = A
3.86	68.59	1.85	0.86	0.5	1.54	9.94	10.57	1.2	0.09	0.25	0.77	 b = LGJKC
1.3	1.04	91.29	0.25	0.45	0.33	1.88	2.2	0.43	0.22	0.13	0.48	 c = E
2.49	3.34	2.33	68.82	0.78	1.63	14.07	4.9	0.78	0.16	0.0	0.7	 d = FV
0.76	1.29	1.4	0.34	89.67	0.22	2.39	3.15	0.37	0.06	0.08	0.28	 e = I
1.43	1.02	0.75	0.38	0.23	85.03	2.41	7.6	0.44	0.2	0.12	0.41	 f = O
3.89	4.14	1.72	1.52	0.87	1.7	70.03	12.95	1.45	0.16	0.17	1.39	 g = SNRTY
0.99	0.97	0.59	0.16	0.31	0.75	2.79	92.71	0.41	0.05	0.03	0.23	 h = 0
2.5	1.99	1.92	0.41	1.37	1.65	7.0	8.23	74.03	0.14	0.24	0.51	 i = BMP
2.01	0.52	2.23	0.3	0.07	2.23	1.93	1.86	0.07	87.22	0.22	1.34	 j = DZ
0.48	0.67	0.82	0.14	0.67	0.87	1.35	1.3	0.63	0.1	92.79	0.19	 k = U
1.76	1.62	1.19	0.38	0.22	0.97	6.0	3.3	0.65	0.35	0.11	83.47	 l = SNRTXY
