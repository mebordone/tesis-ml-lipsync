Timestamp:2014-12-07-11:11:59
Inventario:I4
Intervalo:1ms
Ngramas:3
Clasificador:NaiveBayes
Relation Name:  I4-1ms-3gramas
Num Instances:  183402
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Naive Bayes Classifier

                    Class
Attribute               C         E        FV        AI CDGKNRSYZ         O         0        LT         U       MBP
                   (0.01)    (0.07)    (0.01)    (0.19)    (0.16)    (0.07)    (0.39)    (0.04)    (0.02)    (0.03)
====================================================================================================================
0Int(dB)
  mean             -7.6912   -5.1527  -15.0454   -5.1011  -15.9936   -8.1232  -48.0652  -17.0432   -5.1335  -12.4222
  std. dev.         8.0009    8.0415    8.8688     9.526    9.8598   12.1734   16.3662   11.9763    7.6759   10.6829
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039

0Pitch(Hz)
  mean            211.9162  190.0442  130.2645  206.8445  117.1622  178.3926   17.1388  120.5976  229.1151  159.0019
  std. dev.       101.6738   88.0362  107.3679  104.4536  114.7579  105.3465   56.9505   104.644   84.6861  111.4961
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184

0RawPitch
  mean            201.4818  185.3904  105.2473  196.9595   99.6352  172.5714   14.0524    99.641  220.3812  147.2724
  std. dev.       112.5636   95.8388  103.6816  111.0958  115.0822  113.2081   51.6498  103.2233   93.3578  116.8386
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214

0SmPitch
  mean            210.3612  188.5286  120.0376  204.7812  110.6896  176.1475   15.5182  114.5991  228.5834  154.4932
  std. dev.       103.4558   89.2681  104.6493  106.1621   115.003  107.6301    53.252  104.6978   85.0153  114.0328
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision          0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119

0Melogram(st)
  mean             51.1216   50.3338   36.5638     50.66   30.2784   46.4929    5.0368   33.3548   54.0407   41.2442
  std. dev.        17.8431    15.575    25.185   17.0168   27.4986   20.5401   15.7743   26.3745   14.2925   24.4436
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239

0ZCross
  mean              4.6038    5.2522    2.0494    6.6041    3.3098    4.3707    0.2663    2.0437    4.0724    2.4891
  std. dev.          2.893    3.3232    2.2106    4.7075     5.292    3.0828     1.162    2.2514    2.9865    2.2391
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175

0F1(Hz)
  mean            492.1557  463.9738   380.668    617.24  321.0756  484.0118   55.2552  286.2208  498.5025  370.2872
  std. dev.        194.247  146.1336  278.7586  282.3824  282.2229  226.7963  168.6102  253.2383  308.2875  237.6445
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611

0F2(Hz)
  mean           1683.9156 1786.3109 1161.6312 1546.7146  1007.408 1138.3786  173.0675 1003.6401 1052.4199 1302.7636
  std. dev.       599.3204  528.1199  781.5797  549.9976  846.4881  536.2083  522.6785  844.9572  425.1574  752.0998
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448

0F3(Hz)
  mean            2887.372 2620.1056 1979.1395 2652.4581 1745.5326 2496.9201  295.0862 1713.5942 2674.1003 2193.5767
  std. dev.       943.5462  787.0306 1320.5796  840.8201 1437.3296 1027.3116  876.1293 1437.8463  785.3599 1210.8731
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646

0F4(Hz)
  mean           2928.1402  3156.326 2367.2839 3091.9913 2015.7829 2872.8522  354.6228 1972.1122 3065.6415 2635.3487
  std. dev.       958.8293  913.7122 1544.4726  961.6184  1652.276 1164.6509 1050.4882 1642.0156  804.1571 1423.2744
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804

1Int(dB)
  mean             -7.7014   -5.1597   -15.033   -5.1103  -15.9758   -8.1449  -48.0905  -16.8068   -5.0726  -12.3149
  std. dev.         7.9943    8.0314    8.8639    9.5283    9.8397   12.1979   16.3155   11.9263    7.5754   10.6245
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037

1Pitch(Hz)
  mean            211.8357   190.258  129.9494  206.8242  117.2022  178.1206    16.961  122.1507  229.3502  159.4679
  std. dev.       101.9447   87.8928  107.2999  104.4571  114.8388   105.326   56.6733  104.6093   84.2123  111.1515
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184

1RawPitch
  mean            201.5101  185.5139  104.9895  196.9665   99.5064   172.363   13.9203  101.2918  220.5579  147.7852
  std. dev.       112.6818   95.7233   103.475  111.1115  115.1058  113.1234   51.4522  103.5218   92.9298  116.6168
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214

1SmPitch
  mean            210.4603  188.8112  119.7549  204.7914  110.6377  175.8934   15.3489  116.1154  228.8462  154.8892
  std. dev.       103.5242   89.0812  104.5511  106.1397  115.0686   107.593   52.9677  104.7641   84.5411  113.7583
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision          0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119

1Melogram(st)
  mean             51.0783    50.379   36.5095   50.6725   30.2666   46.4586    4.9859   33.7087     54.16   41.3724
  std. dev.        17.9128   15.5007   25.2094     16.99   27.5101   20.5573   15.7046   26.3027   14.0434   24.3658
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239

1ZCross
  mean              4.6029    5.2408    2.0498    6.6004    3.3147    4.3663    0.2635    2.0855    4.0771     2.508
  std. dev.         2.8824    3.3253    2.2131    4.7102    5.2928     3.087    1.1594     2.258    2.9819     2.237
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175

1F1(Hz)
  mean            491.6485  465.3474  378.3582  618.6325  319.2691  485.0896   54.6953  287.5771  499.8715  370.4332
  std. dev.       194.5674  144.1266  278.5939  281.4779  281.9122  226.1543  167.7972  252.7661  306.7882  237.1009
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611

1F2(Hz)
  mean           1681.1007 1793.0334 1159.1061 1549.7435  1002.967  1138.672   171.346 1009.1151 1056.4734 1303.2526
  std. dev.       601.4811  519.5885  783.4471  547.1516  846.7067  533.7441  520.3678  844.0227  419.7581  750.2659
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448

1F3(Hz)
  mean           2884.2338 2628.2257 1972.9814  2657.204 1738.2081 2500.7441  292.1102 1722.6744 2684.8467 2194.6698
  std. dev.       947.4571  775.3306  1322.667  834.8033 1438.3015 1023.7463   872.183 1436.6171  768.1272 1208.1856
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646

1F4(Hz)
  mean           2926.4358  3165.364 2361.3224 3096.9254  2007.983 2876.6923  351.1292 1983.0411 3077.7233 2638.4197
  std. dev.       963.4929  898.8907 1547.6942  954.3579 1654.0729 1160.0433 1045.9775 1640.9064  781.3188 1420.8135
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804

2Int(dB)
  mean             -7.7106   -5.1788  -15.0107   -5.1271  -15.9524   -8.1757   -48.118  -16.5577   -5.0188  -12.2049
  std. dev.         7.9841    8.0296     8.868    9.5372    9.8275   12.2243   16.2753   11.8777    7.4803   10.5691
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037

2Pitch(Hz)
  mean            211.7569  190.4015  129.6994  206.7814  117.2898  177.8211   16.7879  123.7462  229.5826  159.9332
  std. dev.        102.216   87.7758  107.2959   104.477  114.9223  105.3227   56.4054  104.5599   83.7347  110.8047
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184

2RawPitch
  mean            201.5228  185.6605    104.69  196.9665   99.4493  172.1393   13.7657  102.8896  220.7885  148.2899
  std. dev.       112.8537   95.6276  103.2429  111.1278  115.1427  113.0491   51.2115  103.7161   92.5273  116.3341
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214

2SmPitch
  mean            210.5612  189.0092  119.5928  204.7779  110.6566  175.6214   15.1795  117.6316  229.0387  155.3207
  std. dev.       103.5941   88.9353  104.4955  106.1403  115.1375  107.5646   52.6864  104.8077   84.1352  113.4664
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision          0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119

2Melogram(st)
  mean             51.0349   50.4242   36.4608    50.676   30.2668   46.3993    4.9363   34.0968   54.2789    41.496
  std. dev.        17.9822   15.4261   25.2383   16.9756   27.5209   20.5969   15.6359   26.2191   13.7888   24.2847
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239

2ZCross
  mean               4.602    5.2275    2.0522    6.5942    3.3206    4.3624    0.2613    2.1295    4.0827    2.5306
  std. dev.         2.8724     3.329    2.2158    4.7146    5.2929    3.0942     1.158    2.2623    2.9766    2.2366
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175

2F1(Hz)
  mean            491.6499   466.604  376.1761  619.9649  317.5819   485.985   54.1452  289.2213  501.0702  370.5749
  std. dev.       194.3911  142.2478  278.4193  280.6192  281.5459  225.6523   167.001  252.3268  305.5336   236.559
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611

2F2(Hz)
  mean           1680.0266 1799.2434 1156.6522 1552.5994  998.9077  1138.609  169.6356  1015.283 1060.1575 1303.9361
  std. dev.       601.5161  511.5475  785.3266  544.4403  846.8404   531.629  518.0391  842.8887  414.9703  748.5317
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448

2F3(Hz)
  mean           2884.1098 2635.6414 1966.8613 2661.7194 1731.5698 2503.6829  289.1632 1733.2514 2694.2989 2195.7732
  std. dev.       947.3313   764.297 1324.7402  829.0751 1439.1028 1021.0061  868.2206 1435.2739   752.613 1205.5074
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646

2F4(Hz)
  mean           2927.2908 3173.7603 2355.3511 3101.5827 2001.0209 2879.5448  347.6746 1995.3469 3088.1777 2641.4866
  std. dev.       964.0065  884.9534 1550.8738  947.4372  1655.676 1156.4562 1041.4777 1639.3547  760.6201 1418.3454
  weight sum          1083     13740      2561     35640     28459     13234     72115      6619      4137      5814
  precision         0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804





=== Stratified cross-validation ===

Correctly Classified Instances       94695               51.6325 %
Incorrectly Classified Instances     88707               48.3675 %
Kappa statistic                          0.3775
K&B Relative Info Score            6746640.4618 %
K&B Information Score               171614.7171 bits      0.9357 bits/instance
Class complexity | order 0          466505.2519 bits      2.5436 bits/instance
Class complexity | scheme          3171082.3926 bits     17.2903 bits/instance
Complexity improvement     (Sf)    -2704577.1407 bits    -14.7467 bits/instance
Mean absolute error                      0.097 
Root mean squared error                  0.287 
Relative absolute error                 63.0058 %
Root relative squared error            103.4425 %
Coverage of cases (0.95 level)          61.3221 %
Mean rel. region size (0.95 level)      16.1131 %
Total Number of Instances           183402     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,066    0,006    0,064      0,066    0,065      0,059    0,770     0,026     C
                 0,759    0,249    0,198      0,759    0,314      0,296    0,841     0,275     E
                 0,061    0,028    0,030      0,061    0,040      0,023    0,730     0,029     FV
                 0,368    0,038    0,701      0,368    0,483      0,432    0,850     0,605     AI
                 0,116    0,016    0,567      0,116    0,192      0,205    0,730     0,361     CDGKNRSYZ
                 0,036    0,007    0,290      0,036    0,064      0,080    0,769     0,177     O
                 0,893    0,147    0,797      0,893    0,842      0,733    0,944     0,924     0
                 0,058    0,013    0,142      0,058    0,082      0,069    0,716     0,078     LT
                 0,553    0,062    0,171      0,553    0,261      0,280    0,879     0,360     U
                 0,015    0,007    0,068      0,015    0,024      0,017    0,726     0,059     MBP
Weighted Avg.    0,516    0,090    0,585      0,516    0,493      0,442    0,851     0,584     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
    71   649    12   114    18     0    71     7   141     0 |     a = C
   333 10428   304  1186   246    29   549   191   363   111 |     b = E
     0  1193   156   187   111    49   661    70   121    13 |     c = FV
   122 14542   954 13131   649   242  1956   414  3463   167 |     d = AI
   409  9349  1673  1585  3290   366  8463   722  2188   414 |     e = CDGKNRSYZ
    25  5796   627  1127   330   475  1455   109  3240    50 |     f = O
    30  3823  1009   602   617   290 64388   455   679   222 |     g = 0
    41  2899   251   182   332    67  2086   382   241   138 |     h = LT
     9  1087    54   307    41    60    78   169  2289    43 |     i = U
    69  2962   221   304   173    59  1090   172   679    85 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
6.56	59.93	1.11	10.53	1.66	0.0	6.56	0.65	13.02	0.0	 a = C
2.42	75.9	2.21	8.63	1.79	0.21	4.0	1.39	2.64	0.81	 b = E
0.0	46.58	6.09	7.3	4.33	1.91	25.81	2.73	4.72	0.51	 c = FV
0.34	40.8	2.68	36.84	1.82	0.68	5.49	1.16	9.72	0.47	 d = AI
1.44	32.85	5.88	5.57	11.56	1.29	29.74	2.54	7.69	1.45	 e = CDGKNRSYZ
0.19	43.8	4.74	8.52	2.49	3.59	10.99	0.82	24.48	0.38	 f = O
0.04	5.3	1.4	0.83	0.86	0.4	89.29	0.63	0.94	0.31	 g = 0
0.62	43.8	3.79	2.75	5.02	1.01	31.52	5.77	3.64	2.08	 h = LT
0.22	26.28	1.31	7.42	0.99	1.45	1.89	4.09	55.33	1.04	 i = U
1.19	50.95	3.8	5.23	2.98	1.01	18.75	2.96	11.68	1.46	 j = MBP
