Timestamp:2013-07-22-21:43:47
Inventario:I2
Intervalo:2ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I2-2ms-3gramas
Num Instances:  91701
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.158





=== Stratified cross-validation ===

Correctly Classified Instances       80495               87.7798 %
Incorrectly Classified Instances     11206               12.2202 %
Kappa statistic                          0.8457
K&B Relative Info Score            7560728.5223 %
K&B Information Score               216490.1361 bits      2.3608 bits/instance
Class complexity | order 0          262553.9315 bits      2.8632 bits/instance
Class complexity | scheme          4126078.8795 bits     44.9949 bits/instance
Complexity improvement     (Sf)    -3863524.948 bits    -42.1318 bits/instance
Mean absolute error                      0.032 
Root mean squared error                  0.1249
Relative absolute error                 24.1361 %
Root relative squared error             48.5175 %
Coverage of cases (0.95 level)          95.8081 %
Mean rel. region size (0.95 level)      16.1434 %
Total Number of Instances            91701     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,927    0,016    0,915      0,927    0,921      0,906    0,977     0,953     A
                 0,693    0,013    0,725      0,693    0,708      0,694    0,917     0,734     LGJKC
                 0,917    0,008    0,905      0,917    0,911      0,903    0,977     0,943     E
                 0,672    0,003    0,755      0,672    0,711      0,708    0,912     0,706     FV
                 0,896    0,004    0,907      0,896    0,902      0,898    0,972     0,927     I
                 0,856    0,009    0,886      0,856    0,871      0,861    0,955     0,894     O
                 0,726    0,034    0,690      0,726    0,707      0,676    0,942     0,751     SNRTY
                 0,931    0,058    0,911      0,931    0,921      0,870    0,981     0,962     0
                 0,747    0,004    0,870      0,747    0,804      0,800    0,932     0,804     BMP
                 0,874    0,001    0,924      0,874    0,898      0,897    0,984     0,930     DZ
                 0,925    0,001    0,971      0,925    0,948      0,947    0,988     0,963     U
                 0,844    0,003    0,926      0,844    0,884      0,880    0,958     0,891     SNRTXY
Weighted Avg.    0,878    0,031    0,878      0,878    0,878      0,850    0,968     0,910     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 13287   123    58    16    28    59   280   409    34    10     4    25 |     a = A
   164  3069    50    25    23    52   482   502    27     3    10    21 |     b = LGJKC
    64    80  6336    18    27    40   139   139    24    13     4    26 |     c = E
    32    42    31   864    11    28   194    63     8     3     2     8 |     d = FV
    40    32    47    14  3192    13    82   119    13     1     1     7 |     e = I
    92    66    31    22    15  5686   151   516    22    23     4    18 |     f = O
   322   353   143   111    64   117  6286  1091    86    14     8    68 |     g = SNRTY
   355   339   187    46    98   285   981 33371    88    15    11    55 |     h = 0
    74    54    43    12    36    55   214   237  2177     0     9     4 |     i = BMP
    26     5    25     3     5    34    22    33     4  1176     1    12 |     j = DZ
    11    18    17     1    14    16    34    29     9     2  1925     4 |     k = U
    59    55    36    12     5    29   245   107    11    13     4  3126 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
92.7	0.86	0.4	0.11	0.2	0.41	1.95	2.85	0.24	0.07	0.03	0.17	 a = A
3.7	69.31	1.13	0.56	0.52	1.17	10.89	11.34	0.61	0.07	0.23	0.47	 b = LGJKC
0.93	1.16	91.69	0.26	0.39	0.58	2.01	2.01	0.35	0.19	0.06	0.38	 c = E
2.49	3.27	2.41	67.19	0.86	2.18	15.09	4.9	0.62	0.23	0.16	0.62	 d = FV
1.12	0.9	1.32	0.39	89.64	0.37	2.3	3.34	0.37	0.03	0.03	0.2	 e = I
1.38	0.99	0.47	0.33	0.23	85.56	2.27	7.76	0.33	0.35	0.06	0.27	 f = O
3.72	4.07	1.65	1.28	0.74	1.35	72.56	12.59	0.99	0.16	0.09	0.78	 g = SNRTY
0.99	0.95	0.52	0.13	0.27	0.8	2.74	93.13	0.25	0.04	0.03	0.15	 h = 0
2.54	1.85	1.48	0.41	1.23	1.89	7.34	8.13	74.68	0.0	0.31	0.14	 i = BMP
1.93	0.37	1.86	0.22	0.37	2.53	1.63	2.45	0.3	87.37	0.07	0.89	 j = DZ
0.53	0.87	0.82	0.05	0.67	0.77	1.63	1.39	0.43	0.1	92.55	0.19	 k = U
1.59	1.49	0.97	0.32	0.14	0.78	6.62	2.89	0.3	0.35	0.11	84.44	 l = SNRTXY
