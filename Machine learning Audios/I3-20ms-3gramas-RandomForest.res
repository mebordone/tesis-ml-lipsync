Timestamp:2013-07-22-22:32:58
Inventario:I3
Intervalo:20ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I3-20ms-3gramas
Num Instances:  9127
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3392





=== Stratified cross-validation ===

Correctly Classified Instances        6522               71.4583 %
Incorrectly Classified Instances      2605               28.5417 %
Kappa statistic                          0.6362
K&B Relative Info Score             544180.8541 %
K&B Information Score                14533.0214 bits      1.5923 bits/instance
Class complexity | order 0           24349.2613 bits      2.6678 bits/instance
Class complexity | scheme           659537.4926 bits     72.2622 bits/instance
Complexity improvement     (Sf)    -635188.2312 bits    -69.5944 bits/instance
Mean absolute error                      0.0712
Root mean squared error                  0.1905
Relative absolute error                 49.3341 %
Root relative squared error             70.9189 %
Coverage of cases (0.95 level)          93.3494 %
Mean rel. region size (0.95 level)      26.453  %
Total Number of Instances             9127     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,767    0,070    0,683      0,767    0,723      0,666    0,922     0,798     A
                 0,615    0,044    0,557      0,615    0,585      0,547    0,907     0,573     E
                 0,673    0,134    0,544      0,673    0,601      0,498    0,870     0,598     CGJLNQSRT
                 0,065    0,001    0,474      0,065    0,115      0,172    0,741     0,101     FV
                 0,556    0,014    0,643      0,556    0,596      0,581    0,916     0,556     I
                 0,510    0,029    0,602      0,510    0,552      0,520    0,882     0,539     O
                 0,915    0,041    0,924      0,915    0,920      0,876    0,976     0,955     0
                 0,231    0,007    0,543      0,231    0,324      0,341    0,822     0,281     BMP
                 0,468    0,004    0,756      0,468    0,578      0,587    0,893     0,570     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,798     0,046     DZ
                 0,090    0,001    0,480      0,090    0,152      0,204    0,801     0,136     D
Weighted Avg.    0,715    0,059    0,714      0,715    0,705      0,657    0,918     0,724     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1155   66  180    3   12   48   30    7    3    0    1 |    a = A
   65  459  159    1   22   26    8    4    1    0    1 |    b = E
  223  101 1178    3   45   50  122   18    7    1    3 |    c = CGJLNQSRT
   20   16   78    9    1    8    3    0    3    0    0 |    d = FV
    9   60   78    0  214    4   11    7    2    0    0 |    e = I
  110   46  115    1    3  363   50    9    9    0    6 |    f = O
   35   18  183    1   11   23 2961    2    1    0    0 |    g = 0
   32   30  103    1   19   23   16   69    6    0    0 |    h = BMP
   15   14   39    0    3   34    1    8  102    0    2 |    i = U
    0    0    5    0    0    0    0    0    0    0    0 |    j = DZ
   26   14   49    0    3   24    1    3    1    0   12 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
76.74	4.39	11.96	0.2	0.8	3.19	1.99	0.47	0.2	0.0	0.07	 a = A
8.71	61.53	21.31	0.13	2.95	3.49	1.07	0.54	0.13	0.0	0.13	 b = E
12.74	5.77	67.28	0.17	2.57	2.86	6.97	1.03	0.4	0.06	0.17	 c = CGJLNQSRT
14.49	11.59	56.52	6.52	0.72	5.8	2.17	0.0	2.17	0.0	0.0	 d = FV
2.34	15.58	20.26	0.0	55.58	1.04	2.86	1.82	0.52	0.0	0.0	 e = I
15.45	6.46	16.15	0.14	0.42	50.98	7.02	1.26	1.26	0.0	0.84	 f = O
1.08	0.56	5.66	0.03	0.34	0.71	91.53	0.06	0.03	0.0	0.0	 g = 0
10.7	10.03	34.45	0.33	6.35	7.69	5.35	23.08	2.01	0.0	0.0	 h = BMP
6.88	6.42	17.89	0.0	1.38	15.6	0.46	3.67	46.79	0.0	0.92	 i = U
0.0	0.0	100.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 j = DZ
19.55	10.53	36.84	0.0	2.26	18.05	0.75	2.26	0.75	0.0	9.02	 k = D
