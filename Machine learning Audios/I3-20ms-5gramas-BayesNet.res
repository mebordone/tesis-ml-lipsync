Timestamp:2013-08-30-05:06:30
Inventario:I3
Intervalo:20ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I3-20ms-5gramas
Num Instances:  9125
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 4Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  42 4Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  43 4RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  44 4SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  45 4Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  47 4F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  48 4F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  49 4F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  50 4F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(4): class 
0F1(Hz)(6): class 
0F2(Hz)(6): class 
0F3(Hz)(2): class 
0F4(Hz)(5): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(4): class 
1F1(Hz)(8): class 
1F2(Hz)(6): class 
1F3(Hz)(3): class 
1F4(Hz)(6): class 
2Int(dB)(8): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(5): class 
2ZCross(4): class 
2F1(Hz)(8): class 
2F2(Hz)(8): class 
2F3(Hz)(3): class 
2F4(Hz)(6): class 
3Int(dB)(8): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(4): class 
3F1(Hz)(9): class 
3F2(Hz)(8): class 
3F3(Hz)(5): class 
3F4(Hz)(3): class 
4Int(dB)(8): class 
4Pitch(Hz)(4): class 
4RawPitch(4): class 
4SmPitch(4): class 
4Melogram(st)(7): class 
4ZCross(8): class 
4F1(Hz)(9): class 
4F2(Hz)(9): class 
4F3(Hz)(5): class 
4F4(Hz)(6): class 
class(11): 
LogScore Bayes: -435424.89364436304
LogScore BDeu: -445299.7128710281
LogScore MDL: -444771.75331746344
LogScore ENTROPY: -433491.8308957197
LogScore AIC: -435965.8308957197




=== Stratified cross-validation ===

Correctly Classified Instances        4771               52.2849 %
Incorrectly Classified Instances      4354               47.7151 %
Kappa statistic                          0.4159
K&B Relative Info Score             377946.6965 %
K&B Information Score                10095.7213 bits      1.1064 bits/instance
Class complexity | order 0           24346.2677 bits      2.6681 bits/instance
Class complexity | scheme           116070.1704 bits     12.72   bits/instance
Complexity improvement     (Sf)     -91723.9027 bits    -10.0519 bits/instance
Mean absolute error                      0.0874
Root mean squared error                  0.276 
Relative absolute error                 60.5375 %
Root relative squared error            102.7516 %
Coverage of cases (0.95 level)          59.2658 %
Mean rel. region size (0.95 level)      13.4306 %
Total Number of Instances             9125     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,455    0,063    0,586      0,455    0,513      0,435    0,859     0,603     A
                 0,336    0,047    0,390      0,336    0,361      0,310    0,845     0,340     E
                 0,228    0,053    0,504      0,228    0,314      0,245    0,784     0,426     CGJLNQSRT
                 0,080    0,029    0,041      0,080    0,054      0,037    0,782     0,042     FV
                 0,551    0,047    0,338      0,551    0,419      0,400    0,899     0,418     I
                 0,209    0,024    0,429      0,209    0,281      0,260    0,787     0,286     O
                 0,889    0,081    0,858      0,889    0,873      0,802    0,953     0,915     0
                 0,077    0,011    0,190      0,077    0,110      0,102    0,750     0,098     BMP
                 0,427    0,022    0,322      0,427    0,367      0,353    0,890     0,403     U
                 0,400    0,020    0,011      0,400    0,021      0,063    0,926     0,036     DZ
                 0,549    0,140    0,055      0,549    0,099      0,139    0,844     0,073     D
Weighted Avg.    0,523    0,060    0,593      0,523    0,538      0,480    0,868     0,588     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
  685  111   80   35   59   99   78   13   30   17  298 |    a = A
   85  251   34    8  102   17   39    9   31   12  158 |    b = E
  187  117  399  118  126   28  220   39   54   77  386 |    c = CGJLNQSRT
   17   19   25   11    3    5    8    5    2    4   39 |    d = FV
    9   41   30    3  212    0   18    9    2    8   53 |    e = I
   99   45   34   10   33  149   80    8   53    7  194 |    f = O
   14   12  134   60   22    9 2873   12    5   54   38 |    g = 0
   31   27   39   20   51    9   18   23   12    5   64 |    h = BMP
   17   13    7    2   12   26   12    2   93    2   32 |    i = U
    0    0    1    0    0    0    2    0    0    2    0 |    j = DZ
   24    7    8    1    7    5    0    1    7    0   73 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
45.51	7.38	5.32	2.33	3.92	6.58	5.18	0.86	1.99	1.13	19.8	 a = A
11.39	33.65	4.56	1.07	13.67	2.28	5.23	1.21	4.16	1.61	21.18	 b = E
10.68	6.68	22.79	6.74	7.2	1.6	12.56	2.23	3.08	4.4	22.04	 c = CGJLNQSRT
12.32	13.77	18.12	7.97	2.17	3.62	5.8	3.62	1.45	2.9	28.26	 d = FV
2.34	10.65	7.79	0.78	55.06	0.0	4.68	2.34	0.52	2.08	13.77	 e = I
13.9	6.32	4.78	1.4	4.63	20.93	11.24	1.12	7.44	0.98	27.25	 f = O
0.43	0.37	4.14	1.86	0.68	0.28	88.86	0.37	0.15	1.67	1.18	 g = 0
10.37	9.03	13.04	6.69	17.06	3.01	6.02	7.69	4.01	1.67	21.4	 h = BMP
7.8	5.96	3.21	0.92	5.5	11.93	5.5	0.92	42.66	0.92	14.68	 i = U
0.0	0.0	20.0	0.0	0.0	0.0	40.0	0.0	0.0	40.0	0.0	 j = DZ
18.05	5.26	6.02	0.75	5.26	3.76	0.0	0.75	5.26	0.0	54.89	 k = D
