Timestamp:2013-07-23-02:55:54
Inventario:I1
Intervalo:6ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I1-6ms-2gramas
Num Instances:  30452
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.2568





=== Stratified cross-validation ===

Correctly Classified Instances       24193               79.4463 %
Incorrectly Classified Instances      6259               20.5537 %
Kappa statistic                          0.7427
K&B Relative Info Score            2183303.0696 %
K&B Information Score                63537.5    bits      2.0865 bits/instance
Class complexity | order 0           88598.1754 bits      2.9094 bits/instance
Class complexity | scheme          2191248.9725 bits     71.9575 bits/instance
Complexity improvement     (Sf)    -2102650.7971 bits    -69.048  bits/instance
Mean absolute error                      0.0463
Root mean squared error                  0.1529
Relative absolute error                 37.5879 %
Root relative squared error             61.5718 %
Coverage of cases (0.95 level)          93.2878 %
Mean rel. region size (0.95 level)      19.0176 %
Total Number of Instances            30452     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,871    0,040    0,802      0,871    0,835      0,804    0,959     0,898     a
                 0,586    0,011    0,638      0,586    0,611      0,599    0,892     0,595     b
                 0,813    0,024    0,738      0,813    0,774      0,755    0,957     0,824     e
                 0,594    0,003    0,764      0,594    0,668      0,669    0,957     0,657     d
                 0,440    0,005    0,574      0,440    0,498      0,496    0,881     0,428     f
                 0,778    0,010    0,770      0,778    0,774      0,765    0,958     0,814     i
                 0,297    0,011    0,357      0,297    0,324      0,313    0,793     0,205     k
                 0,570    0,002    0,760      0,570    0,651      0,655    0,905     0,620     j
                 0,659    0,039    0,643      0,659    0,651      0,614    0,905     0,668     l
                 0,735    0,017    0,778      0,735    0,756      0,737    0,931     0,783     o
                 0,901    0,063    0,898      0,901    0,899      0,837    0,966     0,944     0
                 0,641    0,019    0,679      0,641    0,660      0,639    0,914     0,669     s
                 0,788    0,002    0,891      0,788    0,836      0,834    0,976     0,877     u
Weighted Avg.    0,794    0,040    0,792      0,794    0,792      0,755    0,945     0,825     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  4202    30    87     7    15    21    21     7   172    50   153    53     7 |     a = a
    67   574    43     6    14    34    10     1    77    26    92    29     7 |     b = b
   112    27  1909     9     9    23    17     4   128    32    54    20     3 |     c = e
    38    11    28   269     2     4     1     1    44    41     9     4     1 |     d = d
    42    18    29     2   189    15     6     2    39    17    25    44     2 |     e = f
    31    21    79     2     7   938    13     0    47     6    43    17     2 |     f = i
    29    13    25     0    12    14   176     7    62    14   151    83     7 |     g = k
    30     8    27     0     6     5     7   200    15     5    15    30     3 |     h = j
   257    49   146    19    12    61    54    11  1929    74   230    72    13 |     i = l
   127    36    52    23    10     8    17     4   105  1654   180    21    14 |     j = o
   206    72   101     8    29    63   100    14   246   140 10462   163     6 |     k = 0
    84    26    37     6    24    29    63    11   102    28   228  1142     2 |     l = s
    12    14    25     1     0     3     8     1    32    39    10     3   549 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
87.09	0.62	1.8	0.15	0.31	0.44	0.44	0.15	3.56	1.04	3.17	1.1	0.15	 a = a
6.84	58.57	4.39	0.61	1.43	3.47	1.02	0.1	7.86	2.65	9.39	2.96	0.71	 b = b
4.77	1.15	81.34	0.38	0.38	0.98	0.72	0.17	5.45	1.36	2.3	0.85	0.13	 c = e
8.39	2.43	6.18	59.38	0.44	0.88	0.22	0.22	9.71	9.05	1.99	0.88	0.22	 d = d
9.77	4.19	6.74	0.47	43.95	3.49	1.4	0.47	9.07	3.95	5.81	10.23	0.47	 e = f
2.57	1.74	6.55	0.17	0.58	77.78	1.08	0.0	3.9	0.5	3.57	1.41	0.17	 f = i
4.89	2.19	4.22	0.0	2.02	2.36	29.68	1.18	10.46	2.36	25.46	14.0	1.18	 g = k
8.55	2.28	7.69	0.0	1.71	1.42	1.99	56.98	4.27	1.42	4.27	8.55	0.85	 h = j
8.78	1.67	4.99	0.65	0.41	2.08	1.84	0.38	65.9	2.53	7.86	2.46	0.44	 i = l
5.64	1.6	2.31	1.02	0.44	0.36	0.76	0.18	4.66	73.48	8.0	0.93	0.62	 j = o
1.77	0.62	0.87	0.07	0.25	0.54	0.86	0.12	2.12	1.21	90.11	1.4	0.05	 k = 0
4.71	1.46	2.08	0.34	1.35	1.63	3.54	0.62	5.72	1.57	12.79	64.09	0.11	 l = s
1.72	2.01	3.59	0.14	0.0	0.43	1.15	0.14	4.59	5.6	1.43	0.43	78.77	 m = u
