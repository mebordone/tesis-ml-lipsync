Timestamp:2013-08-30-04:17:29
Inventario:I1
Intervalo:10ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I1-10ms-1gramas
Num Instances:  18296
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(11): class 
0Pitch(Hz)(5): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(8): class 
0ZCross(11): class 
0F1(Hz)(12): class 
0F2(Hz)(10): class 
0F3(Hz)(9): class 
0F4(Hz)(8): class 
class(13): 
LogScore Bayes: -232814.19381740474
LogScore BDeu: -237235.18208459846
LogScore MDL: -236752.72151316542
LogScore ENTROPY: -232100.67802644242
LogScore AIC: -233048.67802644242




=== Stratified cross-validation ===

Correctly Classified Instances       11343               61.9972 %
Incorrectly Classified Instances      6953               38.0028 %
Kappa statistic                          0.5178
K&B Relative Info Score             925804.8424 %
K&B Information Score                27151.7256 bits      1.484  bits/instance
Class complexity | order 0           53635.8387 bits      2.9316 bits/instance
Class complexity | scheme            68206.157  bits      3.7279 bits/instance
Complexity improvement     (Sf)     -14570.3183 bits     -0.7964 bits/instance
Mean absolute error                      0.0648
Root mean squared error                  0.2075
Relative absolute error                 52.2087 %
Root relative squared error             83.3012 %
Coverage of cases (0.95 level)          81.843  %
Mean rel. region size (0.95 level)      21.1787 %
Total Number of Instances            18296     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,716    0,080    0,633      0,716    0,672      0,606    0,910     0,744     a
                 0,013    0,002    0,157      0,013    0,025      0,037    0,778     0,105     b
                 0,625    0,071    0,427      0,625    0,508      0,467    0,900     0,514     e
                 0,164    0,011    0,188      0,164    0,175      0,164    0,859     0,129     d
                 0,011    0,004    0,042      0,011    0,018      0,015    0,775     0,042     f
                 0,623    0,033    0,446      0,623    0,520      0,504    0,926     0,519     i
                 0,050    0,004    0,184      0,050    0,079      0,087    0,850     0,108     k
                 0,029    0,001    0,188      0,029    0,050      0,070    0,811     0,079     j
                 0,274    0,063    0,319      0,274    0,295      0,225    0,796     0,263     l
                 0,374    0,027    0,531      0,374    0,439      0,409    0,841     0,448     o
                 0,923    0,121    0,818      0,923    0,867      0,785    0,968     0,959     0
                 0,290    0,024    0,436      0,290    0,348      0,323    0,903     0,357     s
                 0,473    0,013    0,462      0,473    0,467      0,455    0,936     0,517     u
Weighted Avg.    0,620    0,075    0,579      0,620    0,591      0,535    0,906     0,639     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 2104    0  224   29   13   47   15    3  210  105  132   49    9 |    a = a
   67    8   93   12    4   94    7    6  115   42   91   19   36 |    b = b
  177    5  899   14    1   94    5    2  110   31   52   23   25 |    c = e
   54    5   52   45    2    9    1    0   50   38    9    1    8 |    d = d
   34    7   35   12    3   11    4    1   50   25   55   19    5 |    e = f
   10    2  152    0    2  466    4    2   41    3   36   24    6 |    f = i
   14    2   11    0    5   14   18    2   29    9  169   79    7 |    g = k
   58    1   51    6    0   19    4    6   21    3   24   13    2 |    h = j
  281   13  286   57    9  154    9    4  490   75  258   85   67 |    i = l
  325    3  132   40    4   19    8    0  115  515  143   15   58 |    j = o
  103    1   79    7   26   62    6    1  118   38 6274   74    9 |    k = 0
   60    1   51   11    3   54   14    5  140   14  419  315    1 |    l = s
   38    3   39    7    0    1    3    0   48   71    7    6  200 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
71.56	0.0	7.62	0.99	0.44	1.6	0.51	0.1	7.14	3.57	4.49	1.67	0.31	 a = a
11.28	1.35	15.66	2.02	0.67	15.82	1.18	1.01	19.36	7.07	15.32	3.2	6.06	 b = b
12.31	0.35	62.52	0.97	0.07	6.54	0.35	0.14	7.65	2.16	3.62	1.6	1.74	 c = e
19.71	1.82	18.98	16.42	0.73	3.28	0.36	0.0	18.25	13.87	3.28	0.36	2.92	 d = d
13.03	2.68	13.41	4.6	1.15	4.21	1.53	0.38	19.16	9.58	21.07	7.28	1.92	 e = f
1.34	0.27	20.32	0.0	0.27	62.3	0.53	0.27	5.48	0.4	4.81	3.21	0.8	 f = i
3.9	0.56	3.06	0.0	1.39	3.9	5.01	0.56	8.08	2.51	47.08	22.01	1.95	 g = k
27.88	0.48	24.52	2.88	0.0	9.13	1.92	2.88	10.1	1.44	11.54	6.25	0.96	 h = j
15.72	0.73	16.0	3.19	0.5	8.61	0.5	0.22	27.4	4.19	14.43	4.75	3.75	 i = l
23.6	0.22	9.59	2.9	0.29	1.38	0.58	0.0	8.35	37.4	10.38	1.09	4.21	 j = o
1.52	0.01	1.16	0.1	0.38	0.91	0.09	0.01	1.74	0.56	92.29	1.09	0.13	 k = 0
5.51	0.09	4.69	1.01	0.28	4.96	1.29	0.46	12.87	1.29	38.51	28.95	0.09	 l = s
8.98	0.71	9.22	1.65	0.0	0.24	0.71	0.0	11.35	16.78	1.65	1.42	47.28	 m = u
