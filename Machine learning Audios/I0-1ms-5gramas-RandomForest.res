Timestamp:2013-07-22-20:22:38
Inventario:I0
Intervalo:1ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I0-1ms-5gramas
Num Instances:  183400
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  49 4F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    27 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1249





=== Stratified cross-validation ===

Correctly Classified Instances      165430               90.2017 %
Incorrectly Classified Instances     17970                9.7983 %
Kappa statistic                          0.877 
K&B Relative Info Score            15662026.1816 %
K&B Information Score               514322.7125 bits      2.8044 bits/instance
Class complexity | order 0          602177.389  bits      3.2834 bits/instance
Class complexity | scheme          8767154.032  bits     47.8035 bits/instance
Complexity improvement     (Sf)    -8164976.6431 bits    -44.52   bits/instance
Mean absolute error                      0.0113
Root mean squared error                  0.0747
Relative absolute error                 18.8855 %
Root relative squared error             43.2065 %
Coverage of cases (0.95 level)          95.5605 %
Mean rel. region size (0.95 level)       6.5998 %
Total Number of Instances           183400     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,417    0,002    0,547      0,417    0,473      0,475    0,825     0,441     ch
                 0,968    0,000    0,977      0,968    0,972      0,972    1,000     0,994     rr
                 0,963    0,068    0,901      0,963    0,931      0,886    0,985     0,970     0
                 0,664    0,002    0,781      0,664    0,718      0,717    0,869     0,687     hs
                 0,970    0,000    0,988      0,970    0,978      0,978    1,000     0,997     hg
                 0,962    0,006    0,962      0,962    0,962      0,955    0,988     0,977     a
                 0,404    0,006    0,503      0,404    0,448      0,443    0,830     0,402     c
                 0,974    0,000    0,979      0,974    0,977      0,976    1,000     0,995     b
                 0,943    0,004    0,950      0,943    0,947      0,942    0,984     0,964     e
                 0,961    0,000    0,968      0,961    0,965      0,964    0,997     0,988     d
                 0,909    0,000    0,952      0,909    0,930      0,929    0,973     0,938     g
                 0,507    0,002    0,564      0,507    0,534      0,532    0,866     0,530     f
                 0,926    0,002    0,957      0,926    0,941      0,939    0,979     0,951     i
                 0,725    0,001    0,833      0,725    0,776      0,776    0,911     0,763     j
                 0,962    0,000    0,981      0,962    0,971      0,971    0,991     0,980     m
                 0,962    0,000    0,977      0,962    0,969      0,969    0,997     0,987     l
                 0,879    0,004    0,945      0,879    0,911      0,905    0,961     0,916     o
                 0,943    0,001    0,964      0,943    0,954      0,952    0,985     0,965     n
                 0,470    0,001    0,700      0,470    0,563      0,572    0,835     0,512     q
                 0,478    0,003    0,649      0,478    0,550      0,553    0,845     0,515     p
                 0,738    0,016    0,705      0,738    0,721      0,707    0,948     0,772     s
                 0,836    0,001    0,941      0,836    0,886      0,885    0,952     0,884     r
                 0,960    0,000    0,980      0,960    0,970      0,970    0,993     0,981     u
                 0,405    0,006    0,562      0,405    0,471      0,469    0,846     0,428     t
                 0,947    0,000    0,975      0,947    0,961      0,961    0,992     0,976     v
                 0,830    0,000    0,954      0,830    0,888      0,889    0,951     0,880     y
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,537     0,001     z
Weighted Avg.    0,902    0,030    0,898      0,902    0,899      0,878    0,971     0,920     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m     n     o     p     q     r     s     t     u     v     w     x     y     z    aa   <-- classified as
   356     0   127     0     0    19    26     0     3     0     0    10     0     1     0     0     9     2    15     8   236     3     1    38     0     0     0 |     a = ch
     0   210     1     0     0     1     0     0     4     0     0     0     0     0     0     0     1     0     0     0     0     0     0     0     0     0     0 |     b = rr
    47     0 69451   139     2   190   271     1   159     7    16    68    76    30     7    12   267    44    45   147   741    56    13   312     6     4     2 |     c = 0
     3     0   431  1295     0    15    14     0    14     0     1    11     6     1     0     1    19    12     4    13    78     8     3    20     0     1     0 |     d = hs
     0     0     2     0   477     4     0     0     4     0     0     0     3     0     0     0     0     1     0     0     0     0     1     0     0     0     0 |     e = hg
    16     2   482    14     2 25116    96     2    28     5     4    14     8     6     7    19    23    21     4    30   101    27     8    73     4     3     0 |     f = a
    37     0   820    26     0    85  1097     0    38     0     3    31    12    11     0     1    31     4    40    47   236    13     8   171     2     1     1 |     g = c
     0     0     5     0     0     7     0  1165     3     0     0     0    12     0     0     0     0     0     0     0     0     2     2     0     0     0     0 |     h = b
     3     2   345    16     1    33    41     4 12962    16     5    14    23     3     6     8    27    22     7    21    90    22     6    56     5     2     0 |     i = e
     0     0    15     0     0    23     1     0    21  2514     0     2     1     0     0     0    18     9     0     2     4     3     2     0     1     1     0 |     j = d
     1     0    36     2     0    15     1     0    11     0   984     1     4     0     0     0     2     0     0     2    15     2     2     5     0     0     0 |     k = g
    12     0   147     6     0    19    20     0    11     2     1   588     4    13     1     0    20     6     4    24   261     1     0    16     0     2     2 |     l = f
     0     0   253    13     0    30    27     5    32     1     4     3  6560     2     5     8     7     3     6    15    53     6     7    36     7     0     0 |     m = i
     5     0    58     1     0    10    14     0     8     0     1    13     1   710     0     0    12     4     2     1   124     2     1    11     0     1     0 |     n = j
     0     0    36     1     0     8     6     2     6     0     0     0    15     1  2513     1    12     0     0     1     5     1     2     3     0     0     0 |     o = m
     0     0    32     1     0    39     2     0    15     0     0     0     7     0     3  3087     7     3     0     0     3     3     4     3     0     1     0 |     p = l
    10     1  1103    20     1    60    33     1    38    30     3    28    14     9    10    11 11639    19     2    16   125     9     5    36     6     3     2 |     q = o
     2     0   134    10     0    39     3     0    34     8     1    13     5     0     1     1    25  5948     0     8    47     2     2    25     0     0     0 |     r = n
    13     0   201     3     0    10    78     0    20     0     0     6     2     8     0     0     3     3   395     6    66     1     1    24     0     0     0 |     s = q
     7     0   516    22     0    68    81     0    43     1     3    26    11     3     1     1    30     8     2   958   130    11     4    77     0     2     0 |     t = p
    94     0  1330    41     0    77   120     0    62     1     5   164    28    40     1     3    64    22    15    84  6516    21     1   118     1    12     6 |     u = s
     4     0   290    16     0    94    29     5    40     6     1    11    12     2     0     2    26     5     1    24    80  3469     1    29     0     1     0 |     v = r
     1     0    50     1     0    18    16     5    13     1     1     0     6     2     3     3     7     5     5     6     6     2  3973    11     1     1     0 |     w = u
    38     0  1132    26     0   110   193     0    57     1     0    31    34     8     3     0    51    23    17    56   223    18     4  1381     1     1     1 |     x = t
     0     0    17     0     0    14     4     0    12     0     0     0     8     0     1     0     7     0     0     3     3     0     0     5  1327     0     0 |     y = v
     2     0    22     3     0     6     4     0     4     3     0     4     2     2     0     3     3     0     0     3    79     4     2     5     0   739     0 |     z = y
     0     0    16     3     0     1     6     0     3     0     1     5     0     0     0     0     0     3     0     1    23     0     0     3     0     0     0 |    aa = z

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z	aa	
41.69	0.0	14.87	0.0	0.0	2.22	3.04	0.0	0.35	0.0	0.0	1.17	0.0	0.12	0.0	0.0	1.05	0.23	1.76	0.94	27.63	0.35	0.12	4.45	0.0	0.0	0.0	 a = ch
0.0	96.77	0.46	0.0	0.0	0.46	0.0	0.0	1.84	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.46	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 b = rr
0.07	0.0	96.31	0.19	0.0	0.26	0.38	0.0	0.22	0.01	0.02	0.09	0.11	0.04	0.01	0.02	0.37	0.06	0.06	0.2	1.03	0.08	0.02	0.43	0.01	0.01	0.0	 c = 0
0.15	0.0	22.1	66.41	0.0	0.77	0.72	0.0	0.72	0.0	0.05	0.56	0.31	0.05	0.0	0.05	0.97	0.62	0.21	0.67	4.0	0.41	0.15	1.03	0.0	0.05	0.0	 d = hs
0.0	0.0	0.41	0.0	96.95	0.81	0.0	0.0	0.81	0.0	0.0	0.0	0.61	0.0	0.0	0.0	0.0	0.2	0.0	0.0	0.0	0.0	0.2	0.0	0.0	0.0	0.0	 e = hg
0.06	0.01	1.85	0.05	0.01	96.17	0.37	0.01	0.11	0.02	0.02	0.05	0.03	0.02	0.03	0.07	0.09	0.08	0.02	0.11	0.39	0.1	0.03	0.28	0.02	0.01	0.0	 f = a
1.36	0.0	30.2	0.96	0.0	3.13	40.41	0.0	1.4	0.0	0.11	1.14	0.44	0.41	0.0	0.04	1.14	0.15	1.47	1.73	8.69	0.48	0.29	6.3	0.07	0.04	0.04	 g = c
0.0	0.0	0.42	0.0	0.0	0.59	0.0	97.41	0.25	0.0	0.0	0.0	1.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.17	0.17	0.0	0.0	0.0	0.0	 h = b
0.02	0.01	2.51	0.12	0.01	0.24	0.3	0.03	94.34	0.12	0.04	0.1	0.17	0.02	0.04	0.06	0.2	0.16	0.05	0.15	0.66	0.16	0.04	0.41	0.04	0.01	0.0	 i = e
0.0	0.0	0.57	0.0	0.0	0.88	0.04	0.0	0.8	96.06	0.0	0.08	0.04	0.0	0.0	0.0	0.69	0.34	0.0	0.08	0.15	0.11	0.08	0.0	0.04	0.04	0.0	 j = d
0.09	0.0	3.32	0.18	0.0	1.39	0.09	0.0	1.02	0.0	90.86	0.09	0.37	0.0	0.0	0.0	0.18	0.0	0.0	0.18	1.39	0.18	0.18	0.46	0.0	0.0	0.0	 k = g
1.03	0.0	12.67	0.52	0.0	1.64	1.72	0.0	0.95	0.17	0.09	50.69	0.34	1.12	0.09	0.0	1.72	0.52	0.34	2.07	22.5	0.09	0.0	1.38	0.0	0.17	0.17	 l = f
0.0	0.0	3.57	0.18	0.0	0.42	0.38	0.07	0.45	0.01	0.06	0.04	92.62	0.03	0.07	0.11	0.1	0.04	0.08	0.21	0.75	0.08	0.1	0.51	0.1	0.0	0.0	 m = i
0.51	0.0	5.92	0.1	0.0	1.02	1.43	0.0	0.82	0.0	0.1	1.33	0.1	72.52	0.0	0.0	1.23	0.41	0.2	0.1	12.67	0.2	0.1	1.12	0.0	0.1	0.0	 n = j
0.0	0.0	1.38	0.04	0.0	0.31	0.23	0.08	0.23	0.0	0.0	0.0	0.57	0.04	96.17	0.04	0.46	0.0	0.0	0.04	0.19	0.04	0.08	0.11	0.0	0.0	0.0	 o = m
0.0	0.0	1.0	0.03	0.0	1.21	0.06	0.0	0.47	0.0	0.0	0.0	0.22	0.0	0.09	96.17	0.22	0.09	0.0	0.0	0.09	0.09	0.12	0.09	0.0	0.03	0.0	 p = l
0.08	0.01	8.33	0.15	0.01	0.45	0.25	0.01	0.29	0.23	0.02	0.21	0.11	0.07	0.08	0.08	87.95	0.14	0.02	0.12	0.94	0.07	0.04	0.27	0.05	0.02	0.02	 q = o
0.03	0.0	2.12	0.16	0.0	0.62	0.05	0.0	0.54	0.13	0.02	0.21	0.08	0.0	0.02	0.02	0.4	94.29	0.0	0.13	0.75	0.03	0.03	0.4	0.0	0.0	0.0	 r = n
1.55	0.0	23.93	0.36	0.0	1.19	9.29	0.0	2.38	0.0	0.0	0.71	0.24	0.95	0.0	0.0	0.36	0.36	47.02	0.71	7.86	0.12	0.12	2.86	0.0	0.0	0.0	 s = q
0.35	0.0	25.74	1.1	0.0	3.39	4.04	0.0	2.14	0.05	0.15	1.3	0.55	0.15	0.05	0.05	1.5	0.4	0.1	47.78	6.48	0.55	0.2	3.84	0.0	0.1	0.0	 t = p
1.07	0.0	15.07	0.46	0.0	0.87	1.36	0.0	0.7	0.01	0.06	1.86	0.32	0.45	0.01	0.03	0.73	0.25	0.17	0.95	73.83	0.24	0.01	1.34	0.01	0.14	0.07	 u = s
0.1	0.0	6.99	0.39	0.0	2.27	0.7	0.12	0.96	0.14	0.02	0.27	0.29	0.05	0.0	0.05	0.63	0.12	0.02	0.58	1.93	83.63	0.02	0.7	0.0	0.02	0.0	 v = r
0.02	0.0	1.21	0.02	0.0	0.44	0.39	0.12	0.31	0.02	0.02	0.0	0.15	0.05	0.07	0.07	0.17	0.12	0.12	0.15	0.15	0.05	96.04	0.27	0.02	0.02	0.0	 w = u
1.11	0.0	33.21	0.76	0.0	3.23	5.66	0.0	1.67	0.03	0.0	0.91	1.0	0.23	0.09	0.0	1.5	0.67	0.5	1.64	6.54	0.53	0.12	40.51	0.03	0.03	0.03	 x = t
0.0	0.0	1.21	0.0	0.0	1.0	0.29	0.0	0.86	0.0	0.0	0.0	0.57	0.0	0.07	0.0	0.5	0.0	0.0	0.21	0.21	0.0	0.0	0.36	94.72	0.0	0.0	 y = v
0.22	0.0	2.47	0.34	0.0	0.67	0.45	0.0	0.45	0.34	0.0	0.45	0.22	0.22	0.0	0.34	0.34	0.0	0.0	0.34	8.88	0.45	0.22	0.56	0.0	83.03	0.0	 z = y
0.0	0.0	24.62	4.62	0.0	1.54	9.23	0.0	4.62	0.0	1.54	7.69	0.0	0.0	0.0	0.0	0.0	4.62	0.0	1.54	35.38	0.0	0.0	4.62	0.0	0.0	0.0	 aa = z
