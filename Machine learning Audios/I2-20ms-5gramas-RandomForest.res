Timestamp:2013-07-22-21:59:09
Inventario:I2
Intervalo:20ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I2-20ms-5gramas
Num Instances:  9125
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 4Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  42 4Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  43 4RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  44 4SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  45 4Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  47 4F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  48 4F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  49 4F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  50 4F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3774





=== Stratified cross-validation ===

Correctly Classified Instances        6200               67.9452 %
Incorrectly Classified Instances      2925               32.0548 %
Kappa statistic                          0.6029
K&B Relative Info Score             521476.3208 %
K&B Information Score                15380.6367 bits      1.6855 bits/instance
Class complexity | order 0           26896.5513 bits      2.9476 bits/instance
Class complexity | scheme           761620.7558 bits     83.4653 bits/instance
Complexity improvement     (Sf)    -734724.2045 bits    -80.5177 bits/instance
Mean absolute error                      0.0721
Root mean squared error                  0.19  
Relative absolute error                 52.9814 %
Root relative squared error             72.859  %
Coverage of cases (0.95 level)          92.3178 %
Mean rel. region size (0.95 level)      28.5096 %
Total Number of Instances             9125     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,816    0,091    0,639      0,816    0,717      0,660    0,928     0,804     A
                 0,262    0,032    0,299      0,262    0,279      0,245    0,794     0,209     LGJKC
                 0,609    0,049    0,527      0,609    0,565      0,525    0,908     0,549     E
                 0,116    0,004    0,333      0,116    0,172      0,190    0,778     0,121     FV
                 0,535    0,020    0,544      0,535    0,539      0,519    0,920     0,515     I
                 0,546    0,037    0,558      0,546    0,552      0,515    0,892     0,558     O
                 0,519    0,056    0,507      0,519    0,513      0,458    0,865     0,487     SNRTY
                 0,923    0,053    0,906      0,923    0,914      0,867    0,976     0,957     0
                 0,244    0,007    0,525      0,244    0,333      0,344    0,808     0,283     BMP
                 0,101    0,005    0,246      0,101    0,144      0,150    0,823     0,151     DZ
                 0,477    0,004    0,765      0,477    0,588      0,597    0,880     0,568     U
                 0,362    0,014    0,530      0,362    0,430      0,418    0,884     0,384     SNRTXY
Weighted Avg.    0,679    0,050    0,668      0,679    0,667      0,627    0,917     0,684     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1228   34   57    2   15   50   52   45    3    4    1   14 |    a = A
   89  118   46    2   23   23   78   48    6    3    2   12 |    b = LGJKC
   95   36  454    3   36   33   32   16    9    4    3   25 |    c = E
   23   18   21   16    6   10   37    1    3    0    0    3 |    d = FV
   26   18   78    1  206    5   29   13    5    0    0    4 |    e = I
  136   19   32    2   10  389   30   50    8   14    7   15 |    f = O
  141   52   56    7   23   30  474   98    7    4    5   17 |    g = SNRTY
   38   32    8    4   13   33  102 2984   12    0    2    5 |    h = 0
   40   24   28    7   32   30   34   19   73    3    3    6 |    i = BMP
   31    9   19    4    3   25    7    2    3   14    3   18 |    j = DZ
   22   13   12    0    3   39   11    3    5    1  104    5 |    k = U
   52   22   50    0    9   30   48   15    5   10    6  140 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
81.59	2.26	3.79	0.13	1.0	3.32	3.46	2.99	0.2	0.27	0.07	0.93	 a = A
19.78	26.22	10.22	0.44	5.11	5.11	17.33	10.67	1.33	0.67	0.44	2.67	 b = LGJKC
12.73	4.83	60.86	0.4	4.83	4.42	4.29	2.14	1.21	0.54	0.4	3.35	 c = E
16.67	13.04	15.22	11.59	4.35	7.25	26.81	0.72	2.17	0.0	0.0	2.17	 d = FV
6.75	4.68	20.26	0.26	53.51	1.3	7.53	3.38	1.3	0.0	0.0	1.04	 e = I
19.1	2.67	4.49	0.28	1.4	54.63	4.21	7.02	1.12	1.97	0.98	2.11	 f = O
15.43	5.69	6.13	0.77	2.52	3.28	51.86	10.72	0.77	0.44	0.55	1.86	 g = SNRTY
1.18	0.99	0.25	0.12	0.4	1.02	3.15	92.3	0.37	0.0	0.06	0.15	 h = 0
13.38	8.03	9.36	2.34	10.7	10.03	11.37	6.35	24.41	1.0	1.0	2.01	 i = BMP
22.46	6.52	13.77	2.9	2.17	18.12	5.07	1.45	2.17	10.14	2.17	13.04	 j = DZ
10.09	5.96	5.5	0.0	1.38	17.89	5.05	1.38	2.29	0.46	47.71	2.29	 k = U
13.44	5.68	12.92	0.0	2.33	7.75	12.4	3.88	1.29	2.58	1.55	36.18	 l = SNRTXY
