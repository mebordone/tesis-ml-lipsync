Timestamp:2013-08-30-04:13:49
Inventario:I1
Intervalo:3ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I1-3ms-3gramas
Num Instances:  61133
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(21): class 
0Pitch(Hz)(33): class 
0RawPitch(20): class 
0SmPitch(26): class 
0Melogram(st)(34): class 
0ZCross(15): class 
0F1(Hz)(300): class 
0F2(Hz)(408): class 
0F3(Hz)(511): class 
0F4(Hz)(389): class 
1Int(dB)(21): class 
1Pitch(Hz)(28): class 
1RawPitch(19): class 
1SmPitch(27): class 
1Melogram(st)(31): class 
1ZCross(15): class 
1F1(Hz)(280): class 
1F2(Hz)(355): class 
1F3(Hz)(427): class 
1F4(Hz)(299): class 
2Int(dB)(21): class 
2Pitch(Hz)(29): class 
2RawPitch(20): class 
2SmPitch(28): class 
2Melogram(st)(37): class 
2ZCross(15): class 
2F1(Hz)(301): class 
2F2(Hz)(250): class 
2F3(Hz)(382): class 
2F4(Hz)(354): class 
class(13): 
LogScore Bayes: -3826920.613555657
LogScore BDeu: -4412197.846896011
LogScore MDL: -4300228.443057211
LogScore ENTROPY: -3965912.2597527644
LogScore AIC: -4026582.2597527644




=== Stratified cross-validation ===

Correctly Classified Instances       40133               65.6487 %
Incorrectly Classified Instances     21000               34.3513 %
Kappa statistic                          0.5661
K&B Relative Info Score            3450969.3117 %
K&B Information Score                99859.8771 bits      1.6335 bits/instance
Class complexity | order 0          176877.2865 bits      2.8933 bits/instance
Class complexity | scheme           576656.7357 bits      9.4328 bits/instance
Complexity improvement     (Sf)    -399779.4491 bits     -6.5395 bits/instance
Mean absolute error                      0.0537
Root mean squared error                  0.2155
Relative absolute error                 43.7731 %
Root relative squared error             87.0206 %
Coverage of cases (0.95 level)          72.5386 %
Mean rel. region size (0.95 level)      10.3976 %
Total Number of Instances            61133     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,661    0,038    0,762      0,661    0,708      0,660    0,925     0,787     a
                 0,240    0,008    0,489      0,240    0,322      0,328    0,841     0,316     b
                 0,666    0,056    0,492      0,666    0,565      0,531    0,916     0,602     e
                 0,539    0,037    0,179      0,539    0,268      0,293    0,917     0,312     d
                 0,270    0,012    0,244      0,270    0,256      0,245    0,859     0,215     f
                 0,699    0,028    0,499      0,699    0,582      0,571    0,936     0,613     i
                 0,185    0,010    0,263      0,185    0,217      0,207    0,888     0,202     k
                 0,374    0,008    0,362      0,374    0,368      0,361    0,889     0,332     j
                 0,284    0,033    0,476      0,284    0,356      0,319    0,834     0,388     l
                 0,476    0,022    0,625      0,476    0,540      0,515    0,879     0,576     o
                 0,894    0,118    0,828      0,894    0,860      0,767    0,959     0,953     0
                 0,411    0,019    0,574      0,411    0,479      0,459    0,923     0,484     s
                 0,675    0,018    0,468      0,675    0,553      0,550    0,956     0,662     u
Weighted Avg.    0,656    0,065    0,663      0,656    0,650      0,597    0,921     0,710     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  6332    55   668   482   116   174   101    59   493   400   440   129   136 |     a = a
    98   468   193   160    68   202    33    37   112    63   323    52   139 |     b = b
   275    64  3076   189    65   251    62    68   216    71   167    41    76 |     c = e
    78    17    95   487    16    27     1     5    70    39    29     9    31 |     d = d
    48    25   111    74   233    46    22     4    33    26   191    33    18 |     e = f
    21    32   274    41    30  1665    43    26    59     5   108    52    26 |     f = i
    15    16    26     8    19    42   220    12    38    14   582   185    15 |     g = k
    77     4    88    14    15    50    10   259    23    19    77    49     8 |     h = j
   449    93   846   511    95   414   113    64  1653   224   832   225   300 |     i = l
   533    38   309   391    38    56    55    50   150  2124   459    33   227 |     j = o
   241    96   393   219   173   267   101    91   337   254 21221   265    66 |     k = 0
    99    28   109    99    69   134    54    40   218    43  1180  1459    20 |     l = s
    47    21    70    53    17     9    21     0    69   115    19     9   936 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
66.06	0.57	6.97	5.03	1.21	1.82	1.05	0.62	5.14	4.17	4.59	1.35	1.42	 a = a
5.03	24.02	9.91	8.21	3.49	10.37	1.69	1.9	5.75	3.23	16.58	2.67	7.14	 b = b
5.95	1.38	66.57	4.09	1.41	5.43	1.34	1.47	4.67	1.54	3.61	0.89	1.64	 c = e
8.63	1.88	10.51	53.87	1.77	2.99	0.11	0.55	7.74	4.31	3.21	1.0	3.43	 d = d
5.56	2.89	12.85	8.56	26.97	5.32	2.55	0.46	3.82	3.01	22.11	3.82	2.08	 e = f
0.88	1.34	11.5	1.72	1.26	69.9	1.81	1.09	2.48	0.21	4.53	2.18	1.09	 f = i
1.26	1.34	2.18	0.67	1.59	3.52	18.46	1.01	3.19	1.17	48.83	15.52	1.26	 g = k
11.11	0.58	12.7	2.02	2.16	7.22	1.44	37.37	3.32	2.74	11.11	7.07	1.15	 h = j
7.72	1.6	14.54	8.78	1.63	7.11	1.94	1.1	28.41	3.85	14.3	3.87	5.16	 i = l
11.94	0.85	6.92	8.76	0.85	1.25	1.23	1.12	3.36	47.59	10.28	0.74	5.09	 j = o
1.02	0.4	1.66	0.92	0.73	1.13	0.43	0.38	1.42	1.07	89.45	1.12	0.28	 k = 0
2.79	0.79	3.07	2.79	1.94	3.77	1.52	1.13	6.14	1.21	33.22	41.08	0.56	 l = s
3.39	1.52	5.05	3.82	1.23	0.65	1.52	0.0	4.98	8.3	1.37	0.65	67.53	 m = u
