Timestamp:2013-08-30-05:03:41
Inventario:I3
Intervalo:6ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I3-6ms-1gramas
Num Instances:  30453
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(16): class 
0Pitch(Hz)(12): class 
0RawPitch(13): class 
0SmPitch(11): class 
0Melogram(st)(19): class 
0ZCross(13): class 
0F1(Hz)(20): class 
0F2(Hz)(16): class 
0F3(Hz)(19): class 
0F4(Hz)(15): class 
class(11): 
LogScore Bayes: -510506.475441395
LogScore BDeu: -518911.55860266875
LogScore MDL: -518101.54991889343
LogScore ENTROPY: -509873.3699059029
LogScore AIC: -511467.3699059029




=== Stratified cross-validation ===

Correctly Classified Instances       18766               61.6228 %
Incorrectly Classified Instances     11687               38.3772 %
Kappa statistic                          0.5045
K&B Relative Info Score            1591358.9184 %
K&B Information Score                41607.5326 bits      1.3663 bits/instance
Class complexity | order 0           79596.05   bits      2.6137 bits/instance
Class complexity | scheme           115159.8128 bits      3.7816 bits/instance
Complexity improvement     (Sf)     -35563.7628 bits     -1.1678 bits/instance
Mean absolute error                      0.0746
Root mean squared error                  0.2333
Relative absolute error                 52.6154 %
Root relative squared error             87.6112 %
Coverage of cases (0.95 level)          77.109  %
Mean rel. region size (0.95 level)      20.387  %
Total Number of Instances            30453     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,709    0,069    0,660      0,709    0,684      0,622    0,911     0,737     A
                 0,633    0,078    0,403      0,633    0,492      0,453    0,897     0,525     E
                 0,178    0,045    0,477      0,178    0,260      0,204    0,819     0,443     CGJLNQSRT
                 0,051    0,003    0,195      0,051    0,081      0,093    0,806     0,072     FV
                 0,665    0,034    0,444      0,665    0,532      0,520    0,927     0,528     I
                 0,389    0,033    0,484      0,389    0,431      0,394    0,844     0,451     O
                 0,909    0,152    0,787      0,909    0,844      0,741    0,961     0,955     0
                 0,076    0,006    0,301      0,076    0,121      0,137    0,800     0,152     BMP
                 0,522    0,020    0,378      0,522    0,439      0,429    0,937     0,507     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,941     0,003     DZ
                 0,356    0,032    0,142      0,356    0,203      0,207    0,881     0,138     D
Weighted Avg.    0,616    0,088    0,601      0,616    0,587      0,522    0,903     0,677     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  3421   356   244    14    89   253   259     7    46     6   130 |     a = A
   258  1485   154     2   162    57    92    18    44     0    75 |     b = E
   607   820  1008    26   406   278  1911    75   191     0   331 |     c = CGJLNQSRT
    38    72    52    22    33    34   100    13    17     3    46 |     d = FV
    14   218    78     2   802     0    60    11    13     0     8 |     e = I
   427   217   124     7    40   875   245     9   130     0   177 |     f = O
   196   197   299    21   119   120 10558    11    33     2    55 |     g = 0
   104   157   100    12   130    56   169    74   106     0    72 |     h = BMP
    42    70    27     1     7   104    10    19   364     0    53 |     i = U
     0     0     0     0     0     0    12     0     0     0     0 |     j = DZ
    75    93    29     6    20    31     3     9    18     0   157 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
70.9	7.38	5.06	0.29	1.84	5.24	5.37	0.15	0.95	0.12	2.69	 a = A
10.99	63.27	6.56	0.09	6.9	2.43	3.92	0.77	1.87	0.0	3.2	 b = E
10.74	14.51	17.83	0.46	7.18	4.92	33.81	1.33	3.38	0.0	5.86	 c = CGJLNQSRT
8.84	16.74	12.09	5.12	7.67	7.91	23.26	3.02	3.95	0.7	10.7	 d = FV
1.16	18.08	6.47	0.17	66.5	0.0	4.98	0.91	1.08	0.0	0.66	 e = I
18.97	9.64	5.51	0.31	1.78	38.87	10.88	0.4	5.78	0.0	7.86	 f = O
1.69	1.7	2.58	0.18	1.02	1.03	90.93	0.09	0.28	0.02	0.47	 g = 0
10.61	16.02	10.2	1.22	13.27	5.71	17.24	7.55	10.82	0.0	7.35	 h = BMP
6.03	10.04	3.87	0.14	1.0	14.92	1.43	2.73	52.22	0.0	7.6	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
17.01	21.09	6.58	1.36	4.54	7.03	0.68	2.04	4.08	0.0	35.6	 k = D
