Timestamp:2013-07-22-20:47:10
Inventario:I0
Intervalo:20ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I0-20ms-3gramas
Num Instances:  9127
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    27 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3852





=== Stratified cross-validation ===

Correctly Classified Instances        6067               66.4731 %
Incorrectly Classified Instances      3060               33.5269 %
Kappa statistic                          0.5875
K&B Relative Info Score             510576.9681 %
K&B Information Score                17400.3552 bits      1.9065 bits/instance
Class complexity | order 0           31016.3645 bits      3.3983 bits/instance
Class complexity | scheme          1143333.1957 bits    125.2693 bits/instance
Complexity improvement     (Sf)    -1112316.8312 bits   -121.871  bits/instance
Mean absolute error                      0.0327
Root mean squared error                  0.1294
Relative absolute error                 53.0663 %
Root relative squared error             73.7788 %
Coverage of cases (0.95 level)          88.419  %
Mean rel. region size (0.95 level)      13.2553 %
Total Number of Instances             9127     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,025    0,002    0,063      0,025    0,036      0,037    0,686     0,035     ch
                 0,091    0,001    0,143      0,091    0,111      0,113    0,721     0,022     rr
                 0,939    0,082    0,863      0,939    0,899      0,842    0,975     0,955     0
                 0,164    0,005    0,269      0,164    0,203      0,202    0,769     0,134     hs
                 0,111    0,001    0,188      0,111    0,140      0,142    0,769     0,096     hg
                 0,836    0,074    0,665      0,836    0,741      0,695    0,942     0,833     a
                 0,152    0,008    0,219      0,152    0,179      0,172    0,745     0,095     c
                 0,140    0,004    0,200      0,140    0,165      0,163    0,796     0,097     b
                 0,655    0,055    0,515      0,655    0,577      0,539    0,907     0,568     e
                 0,180    0,007    0,273      0,180    0,217      0,213    0,806     0,135     d
                 0,217    0,002    0,448      0,217    0,292      0,309    0,892     0,201     g
                 0,049    0,003    0,107      0,049    0,067      0,068    0,714     0,051     f
                 0,556    0,022    0,532      0,556    0,544      0,523    0,897     0,531     i
                 0,143    0,001    0,583      0,143    0,230      0,287    0,846     0,292     j
                 0,305    0,006    0,453      0,305    0,364      0,364    0,870     0,320     m
                 0,200    0,008    0,305      0,200    0,242      0,236    0,797     0,177     l
                 0,570    0,038    0,558      0,570    0,564      0,527    0,890     0,564     o
                 0,435    0,016    0,502      0,435    0,466      0,448    0,911     0,414     n
                 0,093    0,002    0,211      0,093    0,129      0,137    0,707     0,056     q
                 0,069    0,004    0,175      0,069    0,099      0,104    0,685     0,054     p
                 0,544    0,024    0,543      0,544    0,544      0,519    0,899     0,534     s
                 0,150    0,006    0,374      0,150    0,215      0,225    0,764     0,174     r
                 0,477    0,006    0,667      0,477    0,556      0,555    0,898     0,563     u
                 0,215    0,007    0,362      0,215    0,270      0,268    0,795     0,140     t
                 0,117    0,001    0,429      0,117    0,184      0,221    0,757     0,106     v
                 0,167    0,001    0,421      0,167    0,239      0,263    0,810     0,214     y
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,698     0,020     z
Weighted Avg.    0,665    0,052    0,632      0,665    0,640      0,606    0,911     0,659     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m    n    o    p    q    r    s    t    u    v    w    x    y    z   aa   <-- classified as
    1    0    5    0    0    6    6    0    4    0    0    0    1    0    0    0    0    1    1    1   13    0    0    1    0    0    0 |    a = ch
    0    1    1    0    0    3    0    0    4    1    0    0    0    0    0    0    1    0    0    0    0    0    0    0    0    0    0 |    b = rr
    2    1 3039   11    0   21    6    0   17    3    1    4   10    2    2    0   34    3    1    6   55    2    3   11    0    1    0 |    c = 0
    0    0   24   18    0   17    3    0   11    0    0    1    5    0    1    2   11    3    0    3    6    3    1    1    0    0    0 |    d = hs
    0    0    1    1    3    3    0    0   10    0    1    0    3    0    1    0    1    1    0    1    0    0    1    0    0    0    0 |    e = hg
    0    0   38    5    0 1143    8    2   45    8    0    1    9    0    0   11   51   15    1    1   12   12    2    3    1    0    0 |    f = a
    4    0   44    4    0   10   21    0    3    0    0    2    3    0    1    0   12    1    9    2   10    1    3    8    0    0    0 |    g = c
    0    0    0    0    0    8    0    8    9    3    0    0    7    0    3    2   10    2    0    0    1    0    4    0    0    0    0 |    h = b
    1    0   28    2    3   77    3    0  489   10    3    0   36    1    7   10   29   16    0    1   14    3    4    6    2    1    0 |    i = e
    0    1    1    1    0   26    0    2   20   24    1    0    1    0    1    2   28   18    0    0    1    3    1    0    1    1    0 |    j = d
    0    0    1    0    2   17    0    0   16    0   13    0    5    0    0    0    3    1    0    0    2    0    0    0    0    0    0 |    k = g
    1    1   12    2    0    5    3    0    8    0    0    3    0    0    0    0    2    1    0    1   19    2    0    1    0    0    0 |    l = f
    0    0   25    1    2   15    1    3   72    1    1    0  214    0    7    5    5   13    0    0    7    2    1    8    0    2    0 |    m = i
    0    0    3    1    0   14    2    0    7    0    1    3    1    7    0    0    0    1    0    1    7    1    0    0    0    0    0 |    n = j
    0    0    3    2    0   15    1    4   14    5    1    0   18    0   43    7    8    9    0    0    1    1    7    1    1    0    0 |    o = m
    0    0    4    2    1   29    0    4   26    2    0    0   17    0    1   32   17    9    0    0    7    3    5    0    0    1    0 |    p = l
    0    0   65    3    1   91    5    3   43   14    1    0    9    0    9    8  406   18    0    1   10    7   10    6    1    0    1 |    q = o
    0    1   16    4    1   48    1    2   43    6    2    0   16    0    5    4   16  146    0    1    6    5    5    4    4    0    0 |    r = n
    0    0   12    0    0    4    6    0    2    0    0    1    2    0    0    0    2    1    4    0    9    0    0    0    0    0    0 |    s = q
    2    1   27    2    2   18    4    0    9    0    0    1    2    1    3    1    7    1    0    7    5    0    0    8    0    0    0 |    t = p
    3    1   75    4    1   34   10    1   20    0    1    7   16    1    2    5    9    4    0    5  252    5    0    6    0    1    0 |    u = s
    0    0   25    1    0   64    2    2   28    5    1    2    7    0    3    5   17   11    0    3   10   34    1    2    1    2    0 |    v = r
    1    0    4    1    0   19    3    7   22    2    2    0    6    0    4    2   33    5    0    0    1    1  104    1    0    0    0 |    w = u
    1    0   63    1    0   10   11    1    7    2    0    3    3    0    1    1    8    7    2    5    8    2    1   38    0    2    0 |    x = t
    0    0    3    1    0   19    0    0   16    0    0    0    5    0    0    5   13    2    1    0    0    2    1    0    9    0    0 |    y = v
    0    0    4    0    0    3    0    1    4    2    0    0    6    0    1    3    2    2    0    1    7    1    2    0    1    8    0 |    z = y
    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    3    0    0    0    1    1    0    0    0    0    0 |   aa = z

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z	aa	
2.5	0.0	12.5	0.0	0.0	15.0	15.0	0.0	10.0	0.0	0.0	0.0	2.5	0.0	0.0	0.0	0.0	2.5	2.5	2.5	32.5	0.0	0.0	2.5	0.0	0.0	0.0	 a = ch
0.0	9.09	9.09	0.0	0.0	27.27	0.0	0.0	36.36	9.09	0.0	0.0	0.0	0.0	0.0	0.0	9.09	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 b = rr
0.06	0.03	93.94	0.34	0.0	0.65	0.19	0.0	0.53	0.09	0.03	0.12	0.31	0.06	0.06	0.0	1.05	0.09	0.03	0.19	1.7	0.06	0.09	0.34	0.0	0.03	0.0	 c = 0
0.0	0.0	21.82	16.36	0.0	15.45	2.73	0.0	10.0	0.0	0.0	0.91	4.55	0.0	0.91	1.82	10.0	2.73	0.0	2.73	5.45	2.73	0.91	0.91	0.0	0.0	0.0	 d = hs
0.0	0.0	3.7	3.7	11.11	11.11	0.0	0.0	37.04	0.0	3.7	0.0	11.11	0.0	3.7	0.0	3.7	3.7	0.0	3.7	0.0	0.0	3.7	0.0	0.0	0.0	0.0	 e = hg
0.0	0.0	2.78	0.37	0.0	83.55	0.58	0.15	3.29	0.58	0.0	0.07	0.66	0.0	0.0	0.8	3.73	1.1	0.07	0.07	0.88	0.88	0.15	0.22	0.07	0.0	0.0	 f = a
2.9	0.0	31.88	2.9	0.0	7.25	15.22	0.0	2.17	0.0	0.0	1.45	2.17	0.0	0.72	0.0	8.7	0.72	6.52	1.45	7.25	0.72	2.17	5.8	0.0	0.0	0.0	 g = c
0.0	0.0	0.0	0.0	0.0	14.04	0.0	14.04	15.79	5.26	0.0	0.0	12.28	0.0	5.26	3.51	17.54	3.51	0.0	0.0	1.75	0.0	7.02	0.0	0.0	0.0	0.0	 h = b
0.13	0.0	3.75	0.27	0.4	10.32	0.4	0.0	65.55	1.34	0.4	0.0	4.83	0.13	0.94	1.34	3.89	2.14	0.0	0.13	1.88	0.4	0.54	0.8	0.27	0.13	0.0	 i = e
0.0	0.75	0.75	0.75	0.0	19.55	0.0	1.5	15.04	18.05	0.75	0.0	0.75	0.0	0.75	1.5	21.05	13.53	0.0	0.0	0.75	2.26	0.75	0.0	0.75	0.75	0.0	 j = d
0.0	0.0	1.67	0.0	3.33	28.33	0.0	0.0	26.67	0.0	21.67	0.0	8.33	0.0	0.0	0.0	5.0	1.67	0.0	0.0	3.33	0.0	0.0	0.0	0.0	0.0	0.0	 k = g
1.64	1.64	19.67	3.28	0.0	8.2	4.92	0.0	13.11	0.0	0.0	4.92	0.0	0.0	0.0	0.0	3.28	1.64	0.0	1.64	31.15	3.28	0.0	1.64	0.0	0.0	0.0	 l = f
0.0	0.0	6.49	0.26	0.52	3.9	0.26	0.78	18.7	0.26	0.26	0.0	55.58	0.0	1.82	1.3	1.3	3.38	0.0	0.0	1.82	0.52	0.26	2.08	0.0	0.52	0.0	 m = i
0.0	0.0	6.12	2.04	0.0	28.57	4.08	0.0	14.29	0.0	2.04	6.12	2.04	14.29	0.0	0.0	0.0	2.04	0.0	2.04	14.29	2.04	0.0	0.0	0.0	0.0	0.0	 n = j
0.0	0.0	2.13	1.42	0.0	10.64	0.71	2.84	9.93	3.55	0.71	0.0	12.77	0.0	30.5	4.96	5.67	6.38	0.0	0.0	0.71	0.71	4.96	0.71	0.71	0.0	0.0	 o = m
0.0	0.0	2.5	1.25	0.63	18.13	0.0	2.5	16.25	1.25	0.0	0.0	10.63	0.0	0.63	20.0	10.63	5.63	0.0	0.0	4.38	1.88	3.13	0.0	0.0	0.63	0.0	 p = l
0.0	0.0	9.13	0.42	0.14	12.78	0.7	0.42	6.04	1.97	0.14	0.0	1.26	0.0	1.26	1.12	57.02	2.53	0.0	0.14	1.4	0.98	1.4	0.84	0.14	0.0	0.14	 q = o
0.0	0.3	4.76	1.19	0.3	14.29	0.3	0.6	12.8	1.79	0.6	0.0	4.76	0.0	1.49	1.19	4.76	43.45	0.0	0.3	1.79	1.49	1.49	1.19	1.19	0.0	0.0	 r = n
0.0	0.0	27.91	0.0	0.0	9.3	13.95	0.0	4.65	0.0	0.0	2.33	4.65	0.0	0.0	0.0	4.65	2.33	9.3	0.0	20.93	0.0	0.0	0.0	0.0	0.0	0.0	 s = q
1.98	0.99	26.73	1.98	1.98	17.82	3.96	0.0	8.91	0.0	0.0	0.99	1.98	0.99	2.97	0.99	6.93	0.99	0.0	6.93	4.95	0.0	0.0	7.92	0.0	0.0	0.0	 t = p
0.65	0.22	16.2	0.86	0.22	7.34	2.16	0.22	4.32	0.0	0.22	1.51	3.46	0.22	0.43	1.08	1.94	0.86	0.0	1.08	54.43	1.08	0.0	1.3	0.0	0.22	0.0	 u = s
0.0	0.0	11.06	0.44	0.0	28.32	0.88	0.88	12.39	2.21	0.44	0.88	3.1	0.0	1.33	2.21	7.52	4.87	0.0	1.33	4.42	15.04	0.44	0.88	0.44	0.88	0.0	 v = r
0.46	0.0	1.83	0.46	0.0	8.72	1.38	3.21	10.09	0.92	0.92	0.0	2.75	0.0	1.83	0.92	15.14	2.29	0.0	0.0	0.46	0.46	47.71	0.46	0.0	0.0	0.0	 w = u
0.56	0.0	35.59	0.56	0.0	5.65	6.21	0.56	3.95	1.13	0.0	1.69	1.69	0.0	0.56	0.56	4.52	3.95	1.13	2.82	4.52	1.13	0.56	21.47	0.0	1.13	0.0	 x = t
0.0	0.0	3.9	1.3	0.0	24.68	0.0	0.0	20.78	0.0	0.0	0.0	6.49	0.0	0.0	6.49	16.88	2.6	1.3	0.0	0.0	2.6	1.3	0.0	11.69	0.0	0.0	 y = v
0.0	0.0	8.33	0.0	0.0	6.25	0.0	2.08	8.33	4.17	0.0	0.0	12.5	0.0	2.08	6.25	4.17	4.17	0.0	2.08	14.58	2.08	4.17	0.0	2.08	16.67	0.0	 z = y
0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	60.0	0.0	0.0	0.0	20.0	20.0	0.0	0.0	0.0	0.0	0.0	 aa = z
