Timestamp:2013-08-30-04:40:26
Inventario:I2
Intervalo:6ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I2-6ms-2gramas
Num Instances:  30452
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(15): class 
0Pitch(Hz)(11): class 
0RawPitch(11): class 
0SmPitch(11): class 
0Melogram(st)(19): class 
0ZCross(12): class 
0F1(Hz)(20): class 
0F2(Hz)(19): class 
0F3(Hz)(16): class 
0F4(Hz)(18): class 
1Int(dB)(14): class 
1Pitch(Hz)(12): class 
1RawPitch(13): class 
1SmPitch(11): class 
1Melogram(st)(18): class 
1ZCross(12): class 
1F1(Hz)(20): class 
1F2(Hz)(19): class 
1F3(Hz)(17): class 
1F4(Hz)(15): class 
class(12): 
LogScore Bayes: -948906.7423984145
LogScore BDeu: -967148.7455883758
LogScore MDL: -964745.5067367837
LogScore ENTROPY: -947158.7312433259
LogScore AIC: -950565.7312433259




=== Stratified cross-validation ===

Correctly Classified Instances       18240               59.8975 %
Incorrectly Classified Instances     12212               40.1025 %
Kappa statistic                          0.4928
K&B Relative Info Score            1501801.2919 %
K&B Information Score                43352.7243 bits      1.4236 bits/instance
Class complexity | order 0           87888.3762 bits      2.8861 bits/instance
Class complexity | scheme           215322.1849 bits      7.0709 bits/instance
Complexity improvement     (Sf)    -127433.8087 bits     -4.1847 bits/instance
Mean absolute error                      0.068 
Root mean squared error                  0.2335
Relative absolute error                 50.9455 %
Root relative squared error             90.3668 %
Coverage of cases (0.95 level)          70.8788 %
Mean rel. region size (0.95 level)      14.5557 %
Total Number of Instances            30452     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,642    0,052    0,698      0,642    0,669      0,611    0,910     0,735     A
                 0,033    0,010    0,145      0,033    0,053      0,047    0,745     0,108     LGJKC
                 0,595    0,065    0,432      0,595    0,500      0,458    0,895     0,519     E
                 0,109    0,012    0,118      0,109    0,114      0,102    0,809     0,068     FV
                 0,665    0,037    0,426      0,665    0,520      0,509    0,925     0,507     I
                 0,342    0,026    0,512      0,342    0,410      0,381    0,841     0,436     O
                 0,205    0,033    0,398      0,205    0,270      0,234    0,859     0,361     SNRTY
                 0,904    0,134    0,806      0,904    0,853      0,756    0,960     0,954     0
                 0,073    0,006    0,304      0,073    0,118      0,136    0,793     0,133     BMP
                 0,439    0,043    0,134      0,439    0,205      0,223    0,876     0,169     DZ
                 0,555    0,024    0,354      0,555    0,432      0,427    0,935     0,519     U
                 0,260    0,034    0,245      0,260    0,252      0,219    0,849     0,191     SNRTXY
Weighted Avg.    0,599    0,074    0,583      0,599    0,578      0,521    0,903     0,640     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  3100    44   400    55    94   235   142   236    15   243    92   169 |     a = A
   149    49   206    36   175    64   173   383    13    95    49   103 |     b = LGJKC
   211    41  1396    17   193    44    64    91    28    82    49   131 |     c = E
    29     0    62    47    26    26    46    91    12    56    15    20 |     d = FV
    16    16   182     8   802     0    53    59    10     8    14    38 |     e = I
   347    22   205    21    44   769    63   236    16   266   187    75 |     f = O
   183    67   189    68   121    74   596  1149    25   179    45   216 |     g = SNRTY
   132    52   168    78   125   109   196 10499    16    83    35   117 |     h = 0
    74    24   111    39   158    49    48   161    72    81   106    57 |     i = BMP
    56     3    55     9    17    25    10    14     6   199    19    40 |     j = DZ
    37     7    49     5     7    69    21    10    12    58   387    35 |     k = U
   105    14   210    14   119    38    87    91    12   137    95   324 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
64.25	0.91	8.29	1.14	1.95	4.87	2.94	4.89	0.31	5.04	1.91	3.5	 a = A
9.97	3.28	13.78	2.41	11.71	4.28	11.57	25.62	0.87	6.35	3.28	6.89	 b = LGJKC
8.99	1.75	59.48	0.72	8.22	1.87	2.73	3.88	1.19	3.49	2.09	5.58	 c = E
6.74	0.0	14.42	10.93	6.05	6.05	10.7	21.16	2.79	13.02	3.49	4.65	 d = FV
1.33	1.33	15.09	0.66	66.5	0.0	4.39	4.89	0.83	0.66	1.16	3.15	 e = I
15.42	0.98	9.11	0.93	1.95	34.16	2.8	10.48	0.71	11.82	8.31	3.33	 f = O
6.28	2.3	6.49	2.34	4.16	2.54	20.47	39.46	0.86	6.15	1.55	7.42	 g = SNRTY
1.14	0.45	1.45	0.67	1.08	0.94	1.69	90.43	0.14	0.71	0.3	1.01	 h = 0
7.55	2.45	11.33	3.98	16.12	5.0	4.9	16.43	7.35	8.27	10.82	5.82	 i = BMP
12.36	0.66	12.14	1.99	3.75	5.52	2.21	3.09	1.32	43.93	4.19	8.83	 j = DZ
5.31	1.0	7.03	0.72	1.0	9.9	3.01	1.43	1.72	8.32	55.52	5.02	 k = U
8.43	1.12	16.85	1.12	9.55	3.05	6.98	7.3	0.96	11.0	7.62	26.0	 l = SNRTXY
