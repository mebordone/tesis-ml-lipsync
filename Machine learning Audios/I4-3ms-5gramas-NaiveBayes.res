Timestamp:2014-12-07-11:17:34
Inventario:I4
Intervalo:3ms
Ngramas:5
Clasificador:NaiveBayes
Relation Name:  I4-3ms-5gramas
Num Instances:  61131
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Naive Bayes Classifier

                    Class
Attribute               C         E        FV        AI CDGKNRSYZ         O         0        LT         U       MBP
                   (0.01)    (0.08)    (0.01)     (0.2)    (0.16)    (0.07)    (0.39)    (0.04)    (0.02)    (0.03)
====================================================================================================================
0Int(dB)
  mean             -7.4592   -5.7241  -14.6126   -5.4509  -15.7734   -8.4226    -48.04  -18.5658   -6.2108  -13.1152
  std. dev.         7.8489    8.6737    9.2915    9.9348   10.4633   12.1005    16.606   12.7457    9.0317   11.3516
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044

0Pitch(Hz)
  mean            212.9144  185.9586  137.4946  204.9811  121.9376  178.4602   17.8444  113.4944  224.0701  158.8832
  std. dev.        97.7399   90.1831  108.1126  105.7923  113.8988  106.9431   58.0865   105.406   92.7797  112.4924
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303

0RawPitch
  mean            202.5047  181.3123  111.9267  194.8516  105.8347  171.3618   14.7579   94.2106  215.2186  146.1472
  std. dev.       108.8397    96.693  105.4368  111.3767  114.7905  113.5595   52.6837  102.8492   99.6003  116.9224
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision          0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131

0SmPitch
  mean            211.3648  183.6469  127.3698  202.6949  116.0144  176.0047   16.2046  107.3778  223.7517  155.5195
  std. dev.        99.5968   92.0569  105.9307  107.5864  114.5517  109.2519    54.355  104.6523   92.6385   114.372
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312

0Melogram(st)
  mean              51.584   49.4669   38.0803   50.1253   31.8394   46.3882    5.1991   31.9412   52.2659   41.4956
  std. dev.        17.0086   16.7812     24.59     17.75   27.2275   20.7426   15.9975   26.5775   17.2831   24.3973
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252

0ZCross
  mean                4.48    4.9169    2.0505    6.2678    3.1798    4.0022    0.2872    1.8038     3.713    2.3289
  std. dev.         2.8845    3.3248    2.1188    4.5993    4.9891     3.118    1.2752    2.2002    2.8833    2.1747
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633

0F1(Hz)
  mean            500.5366  448.4125  411.1787  596.9865  349.7918  467.7058   58.0026  296.8326  478.6311  378.8006
  std. dev.       192.0052   166.292  274.3057  295.1486  283.2185  236.8053  173.4388  258.2807    325.23  239.9247
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642

0F2(Hz)
  mean           1712.2384  1711.214  1217.474 1501.6016 1082.7531 1125.8145  180.0747 1020.5038 1005.5905 1326.7322
  std. dev.       572.2879  608.9906  757.4226  590.1372  836.6278  570.3838  532.2925  842.1986   487.338  756.1536
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431

0F3(Hz)
  mean           2922.0803 2531.3885  2080.908 2583.3178  1870.231 2433.4936  307.8348 1743.3713 2536.8144 2230.2399
  std. dev.       895.0597   899.016 1279.4794  925.6921  1411.119 1084.1339  893.0846 1426.4131  969.7241 1210.9903
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602

0F4(Hz)
  mean           2951.1202 3059.3156 2472.1615  3015.958 2155.4284 2805.3088  369.3315 2005.7339 2908.3713 2653.8723
  std. dev.       905.6261 1056.7202 1487.4598  1063.352 1616.6764 1235.5521 1069.3144 1629.0729 1040.3379 1411.1213
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775

1Int(dB)
  mean             -7.5492   -5.4866   -14.804   -5.3074  -15.8975   -8.2804  -48.1807  -18.3097   -5.8577  -13.0129
  std. dev.         7.9239    8.4392    9.1123    9.7674   10.2576   12.0927   16.4087    12.504    8.6173   11.1545
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044

1Pitch(Hz)
  mean            212.6535  187.5772  135.8817  205.9838  120.4325  179.1099   17.3506   114.616  226.2021  158.7755
  std. dev.        98.6194   89.3537  107.8471  104.9439  114.0782  106.1835   57.3369  105.0049   89.9667  112.1053
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303

1RawPitch
  mean             201.784  183.2465  109.9703  195.9835  103.9139  172.4006   14.2573   94.9649  217.6541  146.1731
  std. dev.        110.061   95.6687  104.6772  110.6213   114.699  112.8726   51.6702    102.52   96.8389  116.9415
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision          0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131

1SmPitch
  mean            211.1034  185.6219  125.7035  203.7581  114.3506  176.7887   15.6887  108.5613  225.9095  154.8711
  std. dev.       100.4567    90.965  105.4709  106.7382  114.6026  108.4257   53.4945  104.5124   89.8265  114.2773
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312

1Melogram(st)
  mean             51.4516   49.8961   37.8284   50.4327   31.3962   46.6075    5.0779   32.0012   52.9335   41.3347
  std. dev.        17.2291   16.2028   24.6828   17.3404   27.3039   20.5184   15.8345   26.5758   16.2835   24.4629
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252

1ZCross
  mean              4.4416    4.9388    2.0007     6.302    3.1761    4.0249    0.2622    1.8153     3.755    2.3235
  std. dev.         2.8337    3.2469    2.0822     4.582    5.0768    3.0126    1.1593     2.165    2.8542     2.129
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633

1F1(Hz)
  mean            497.8616  453.7041  403.0001  603.9955  341.8812  473.8855   56.1656   292.488  484.7897  374.9867
  std. dev.       192.5218  159.4091  276.2591  290.3548  283.1686  232.8436  170.5763  256.6749  320.1911  239.5169
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642

1F2(Hz)
  mean           1705.7219 1736.7393 1201.2387 1518.4156 1062.3882 1133.1742  174.9481 1011.3897 1019.5348 1317.4527
  std. dev.        579.874  582.7337  764.2508  575.4222  840.1812  558.0272   525.318  842.6537  469.8361  757.5471
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431

1F3(Hz)
  mean           2912.7207 2562.5981 2053.3107 2609.0607 1836.3653  2458.752  299.0333 1728.6555 2579.3441 2214.3535
  std. dev.       908.2088   862.118 1292.4335  894.9013 1419.5986 1061.5374  881.6893 1430.1786  919.5861 1214.8583
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602

1F4(Hz)
  mean           2945.4874 3093.8052  2442.436 3045.1246 2117.0015 2833.3702  358.9847 1989.6135 2956.7717 2641.6647
  std. dev.       920.2088 1009.5134 1504.0848 1026.6282  1627.545 1207.9571 1056.2377 1633.7892   976.944 1419.2169
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775

2Int(dB)
  mean             -7.6283   -5.3093   -14.929   -5.2102   -15.983    -8.186  -48.3098  -17.9339   -5.5524  -12.8391
  std. dev.         7.9844    8.2373    8.9669    9.6464   10.0795   12.1008   16.2247   12.2762    8.2298   10.9656
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044

2Pitch(Hz)
  mean            212.4122  188.9058  134.5242  206.6873  119.3567  179.2936   16.8275   117.087  228.1408  158.9902
  std. dev.        99.4933   88.6428  107.5224  104.3287  114.2582  105.6064   56.5405  104.7279   87.2319  111.5792
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303

2RawPitch
  mean            200.9562  184.5166  108.9501  196.8118  102.5504  173.0126   13.8002   96.7652  219.2103  146.3863
  std. dev.       111.3443   95.0659  104.2181  110.0777  114.5772  112.4306   50.8241  102.2822   94.8961  116.5641
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision          0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131

2SmPitch
  mean            210.8602  187.1933  124.4425  204.4955  113.1201  177.1572   15.1843  110.6942  227.7542  154.8208
  std. dev.       101.3118   90.0322  105.0137  106.1161  114.7002  107.7873    52.656   104.402   87.2767  113.9964
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312

2Melogram(st)
  mean             51.3216   50.2427   37.5734   50.6489   31.0416   46.6963    4.9554   32.5569   53.5545   41.2774
  std. dev.        17.4461   15.7114   24.7697   17.0322   27.3693    20.402   15.6692   26.4937    15.256   24.4581
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252

2ZCross
  mean              4.3935    4.9486    1.9576    6.3241    3.1738    4.0411    0.2433      1.84    3.7945    2.3403
  std. dev.         2.7825    3.2071    2.0438    4.5652    5.1371    2.9298    1.0804    2.1311    2.8295    2.1047
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633

2F1(Hz)
  mean            495.6713  458.5411  395.5357  610.0629  334.6439  479.3315   54.3918  289.4781  491.2079  373.5242
  std. dev.       192.9817  152.8966  277.2043  286.1205  282.9319  229.0643  167.8415  255.0441  314.7115  238.3426
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642

2F2(Hz)
  mean           1698.0593 1759.9732 1189.2807 1532.9358 1043.5831  1139.398  169.8362 1007.0508 1035.0423 1314.2158
  std. dev.       587.1878  557.1365  769.1361  562.4877  842.7299  546.4539  518.3893  843.0364  450.7523  754.2105
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431

2F3(Hz)
  mean           2903.3775 2590.6505 2031.3736 2631.1089 1805.4769 2481.3348  290.0758 1720.5729 2624.5033 2210.1004
  std. dev.       921.0225   826.473 1301.0849  867.5262 1426.5037 1040.3144  869.7877 1432.6985  861.9955 1211.2692
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602

2F4(Hz)
  mean           2939.9367 3124.7953 2420.8547 3069.5545 2082.1912 2858.4583  348.5398  1981.424 3006.9754 2643.1324
  std. dev.       934.5647  964.1286 1516.4414  993.8675 1636.7194 1181.9837 1042.8301 1637.3955  902.9144 1417.9475
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775

3Int(dB)
  mean             -7.6838   -5.2147  -14.9846   -5.1674  -16.0167   -8.1561  -48.4237  -17.4136   -5.2953  -12.5947
  std. dev.         8.0063    8.1108    8.8593    9.5835    9.9461   12.1343   16.0588   12.0707    7.8625   10.7736
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044

3Pitch(Hz)
  mean            212.1825  190.0809  133.2458   206.983  118.8709    179.07   16.2557  120.7679   229.415  159.7556
  std. dev.       100.3526   87.8538  107.2688  104.0507  114.4465  105.2101   55.6504  104.6086   85.2349  110.8403
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303

3RawPitch
  mean            201.2961  185.5537  108.1776  197.3055  101.6391  172.9408    13.331   99.6532  220.5485  147.2848
  std. dev.        111.593   94.6689  103.4633  109.7358  114.4784  112.1655   50.0238  102.4782   93.0837  115.9925
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision          0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131

3SmPitch
  mean              210.63  188.4788  123.2631  204.8418  112.4585   176.912   14.6616  114.3682  229.0404   155.285
  std. dev.       102.1531   89.1689  104.6396   105.801  114.8046  107.4207   51.7924  104.6114   85.2781  113.4942
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312

3Melogram(st)
  mean             51.1937   50.4808   37.3343   50.7688   30.8483    46.671    4.8142   33.3936   54.1306   41.4703
  std. dev.        17.6599   15.3557   24.8638   16.8412   27.4182   20.3864   15.4731   26.3501   14.2012   24.3086
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252

3ZCross
  mean              4.3679    4.9388    1.9347    6.3314     3.177    4.0484    0.2298    1.9023    3.8272    2.3719
  std. dev.         2.7571    3.1975    2.0238    4.5552    5.1616    2.8985    1.0465    2.1208    2.8121    2.0915
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633

3F1(Hz)
  mean            493.7188  462.8688  388.2589  615.2697  328.3257   483.672   52.7265  289.4672  496.5514  372.4682
  std. dev.       193.5409  146.9719  277.0199  282.5313  282.4216  226.1449  165.2548  253.6685  309.2602  237.0685
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642

3F2(Hz)
  mean           1690.1635 1781.0424 1180.6948 1544.8658 1027.2119 1142.8991  165.0589 1010.6667 1050.0402  1312.066
  std. dev.       594.0469  532.5896  774.0059  551.4583  844.2196  536.7971  511.9666  841.9232  432.2873  750.4882
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431

3F3(Hz)
  mean           2894.0835 2615.4812 2012.5832 2649.4886 1778.7915 2498.5172  281.5968 1726.9507 2665.1247  2207.248
  std. dev.        933.351  792.6591  1307.812  844.1952 1431.4769 1024.1918  858.2952 1432.7821  803.6971 1206.6787
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602

3F4(Hz)
  mean           2934.4812 3152.5297 2403.3745 3089.3267  2052.915 2876.7808  338.6077 1989.0321 3052.6473 2646.3634
  std. dev.       948.5863  921.2174 1526.7311  965.8544 1644.1525 1161.8705 1029.7278 1637.5802  827.1813 1415.4829
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775

4Int(dB)
  mean             -7.7144   -5.2154  -14.9663   -5.1844   -15.988    -8.205  -48.5189  -16.7493   -5.0964  -12.2908
  std. dev.         7.9877     8.061    8.8198    9.5792    9.8675   12.2015   15.9252   11.9026    7.5403   10.5869
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043

4Pitch(Hz)
  mean            211.9581  190.6854  132.7243  206.9409  118.9882  178.3171   15.7183  125.1058   230.256  161.0025
  std. dev.        101.191   87.4198  107.1636  104.0213  114.6785  105.1185   54.8033  104.5268   83.5916  109.8138
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303

4RawPitch
  mean            201.5263  186.0468  107.6938  197.3354  101.3949  172.3446   12.8482  104.3586   221.377  148.5952
  std. dev.       111.8555   94.2836  102.8608  109.7747   114.525   111.886   49.2604  103.1783   91.8259  115.2511
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision          0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131

4SmPitch
  mean            210.4051  189.2421  123.0119   204.867  112.3342  176.2127   14.1449   118.741   229.966  156.3345
  std. dev.       102.9728   88.6477  104.4081   105.724  114.9823  107.2818   50.9173  104.8018   83.6261  112.6809
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312

4Melogram(st)
  mean             51.0657   50.6147   37.2513   50.8126   30.7905   46.5702    4.6716   34.4319   54.4863   41.7583
  std. dev.        17.8701   15.1287    24.928    16.748   27.4606    20.439   15.2691   26.1315   13.4335   24.1126
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252

4ZCross
  mean              4.3582    4.9156    1.9401    6.3221    3.1839    4.0424    0.2221    2.0049    3.8515    2.4227
  std. dev.         2.7159    3.2034    2.0364     4.559    5.1652    2.9091    1.0405    2.1254    2.7932    2.0849
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633

4F1(Hz)
  mean            493.2121  466.9426  381.3156  619.6746  322.8712  486.8876   50.9791  292.6928  501.4313  372.4935
  std. dev.       192.4513  141.0764  276.5899  279.6222  281.5409  224.2029  162.6227  252.1069  304.2361  235.5015
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642

4F2(Hz)
  mean           1687.2659 1800.7234 1172.6362 1554.5292 1014.0562 1143.6165  159.7147 1024.8012 1063.4653 1312.5688
  std. dev.       594.1662  507.5736  779.2046  542.2833  845.1049  529.3781  504.5122   839.475  414.3404  745.4148
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431

4F3(Hz)
  mean           2893.5233  2638.722 1994.0357 2664.5767 1757.0647 2509.8227  272.3289 1750.6962  2701.015 2208.9124
  std. dev.       933.1946  758.2975 1314.2962  824.7762 1434.8075 1013.4676  845.5437 1429.9932  745.1651 1199.4463
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602

4F4(Hz)
  mean           2936.8721  3179.034 2385.6763 3105.1123  2029.654  2888.156   327.666 2016.9519 3093.8479 2654.3032
  std. dev.       950.2793  877.8457 1536.6667  942.4481 1649.8841 1148.0627  1014.973 1634.5631  750.1771 1409.2849
  weight sum           363      4621       864     11967      9576      4463     23722      2221      1386      1948
  precision         0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775





=== Stratified cross-validation ===

Correctly Classified Instances       30814               50.4065 %
Incorrectly Classified Instances     30317               49.5935 %
Kappa statistic                          0.3689
K&B Relative Info Score            2148922.8363 %
K&B Information Score                54881.2036 bits      0.8978 bits/instance
Class complexity | order 0          156103.513  bits      2.5536 bits/instance
Class complexity | scheme          1671795.5174 bits     27.3478 bits/instance
Complexity improvement     (Sf)    -1515692.0044 bits    -24.7942 bits/instance
Mean absolute error                      0.0992
Root mean squared error                  0.2979
Relative absolute error                 64.203  %
Root relative squared error            107.1744 %
Coverage of cases (0.95 level)          56.3331 %
Mean rel. region size (0.95 level)      13.9738 %
Total Number of Instances            61131     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,196    0,029    0,038      0,196    0,064      0,074    0,771     0,025     C
                 0,692    0,240    0,191      0,692    0,299      0,268    0,826     0,291     E
                 0,091    0,037    0,034      0,091    0,050      0,034    0,736     0,030     FV
                 0,356    0,032    0,730      0,356    0,478      0,437    0,845     0,606     AI
                 0,101    0,006    0,751      0,101    0,178      0,240    0,733     0,371     CDGKNRSYZ
                 0,028    0,006    0,272      0,028    0,050      0,066    0,768     0,176     O
                 0,893    0,139    0,803      0,893    0,846      0,742    0,907     0,814     0
                 0,086    0,030    0,098      0,086    0,092      0,060    0,714     0,074     LT
                 0,527    0,052    0,189      0,527    0,278      0,290    0,869     0,354     U
                 0,012    0,007    0,053      0,012    0,019      0,010    0,721     0,058     MBP
Weighted Avg.    0,504    0,083    0,617      0,504    0,487      0,446    0,834     0,540     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
    71   192     6    23     0     0    22     8    41     0 |     a = C
   403  3198   106   338    41    11   182   167   119    56 |     b = E
    27   391    79    52    12     9   202    54    32     6 |     c = FV
   394  4691   415  4255   136    83   626   342   933    92 |     d = AI
   568  2977   723   464   966    90  2581   492   611   104 |     e = CDGKNRSYZ
    81  1998   259   359    39   123   470   151   945    38 |     f = O
    91  1150   445   131    50   105 21177   347   165    61 |     g = 0
   103   889   122    34    32    14   720   192    78    37 |     h = LT
    33   347    46    92     1    13    30    80   730    14 |     i = U
    86   944   116    81    10     4   353   127   204    23 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
19.56	52.89	1.65	6.34	0.0	0.0	6.06	2.2	11.29	0.0	 a = C
8.72	69.21	2.29	7.31	0.89	0.24	3.94	3.61	2.58	1.21	 b = E
3.13	45.25	9.14	6.02	1.39	1.04	23.38	6.25	3.7	0.69	 c = FV
3.29	39.2	3.47	35.56	1.14	0.69	5.23	2.86	7.8	0.77	 d = AI
5.93	31.09	7.55	4.85	10.09	0.94	26.95	5.14	6.38	1.09	 e = CDGKNRSYZ
1.81	44.77	5.8	8.04	0.87	2.76	10.53	3.38	21.17	0.85	 f = O
0.38	4.85	1.88	0.55	0.21	0.44	89.27	1.46	0.7	0.26	 g = 0
4.64	40.03	5.49	1.53	1.44	0.63	32.42	8.64	3.51	1.67	 h = LT
2.38	25.04	3.32	6.64	0.07	0.94	2.16	5.77	52.67	1.01	 i = U
4.41	48.46	5.95	4.16	0.51	0.21	18.12	6.52	10.47	1.18	 j = MBP
