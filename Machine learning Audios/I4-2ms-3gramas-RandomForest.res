Timestamp:2013-07-22-22:53:12
Inventario:I4
Intervalo:2ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I4-2ms-3gramas
Num Instances:  91701
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1517





=== Stratified cross-validation ===

Correctly Classified Instances       81113               88.4538 %
Incorrectly Classified Instances     10588               11.5462 %
Kappa statistic                          0.8495
K&B Relative Info Score            7506191.8426 %
K&B Information Score               191322.4181 bits      2.0864 bits/instance
Class complexity | order 0          233714.4049 bits      2.5487 bits/instance
Class complexity | scheme          3670696.9841 bits     40.029  bits/instance
Complexity improvement     (Sf)    -3436982.5791 bits    -37.4803 bits/instance
Mean absolute error                      0.0367
Root mean squared error                  0.1336
Relative absolute error                 23.8295 %
Root relative squared error             48.1277 %
Coverage of cases (0.95 level)          96.2814 %
Mean rel. region size (0.95 level)      18.6865 %
Total Number of Instances            91701     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,869    0,000    0,924      0,869    0,896      0,896    0,969     0,909     C
                 0,917    0,009    0,897      0,917    0,907      0,900    0,979     0,943     E
                 0,681    0,003    0,761      0,681    0,719      0,716    0,913     0,713     FV
                 0,925    0,022    0,912      0,925    0,918      0,898    0,977     0,956     AI
                 0,829    0,041    0,790      0,829    0,809      0,773    0,961     0,870     CDGKNRSYZ
                 0,854    0,007    0,900      0,854    0,877      0,868    0,955     0,894     O
                 0,931    0,056    0,914      0,931    0,923      0,873    0,980     0,962     0
                 0,585    0,008    0,744      0,585    0,655      0,649    0,892     0,647     LT
                 0,926    0,001    0,970      0,926    0,948      0,947    0,986     0,960     U
                 0,731    0,004    0,866      0,731    0,793      0,789    0,930     0,800     MBP
Weighted Avg.    0,885    0,034    0,884      0,885    0,883      0,853    0,969     0,920     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   473     9     1    21    22     2    14     1     1     0 |     a = C
     7  6339    11    99   226    24   134    37     7    26 |     b = E
     0    36   876    40   241    15    59    11     2     6 |     c = FV
     9   118    31 16544   485    50   519    94     5    39 |     d = AI
     9   206   120   497 11830   137  1152   217    14    90 |     e = CDGKNRSYZ
     1    48    19    98   228  5679   505    36     2    30 |     f = O
     7   167    58   459  1184   276 33373   207    11    89 |     g = 0
     3    69    21   209   479    63   483  1943    11    42 |     h = LT
     1    12     1    31    43    15    27    16  1926     8 |     i = U
     2    59    13   141   234    47   234    49     6  2130 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
86.95	1.65	0.18	3.86	4.04	0.37	2.57	0.18	0.18	0.0	 a = C
0.1	91.74	0.16	1.43	3.27	0.35	1.94	0.54	0.1	0.38	 b = E
0.0	2.8	68.12	3.11	18.74	1.17	4.59	0.86	0.16	0.47	 c = FV
0.05	0.66	0.17	92.46	2.71	0.28	2.9	0.53	0.03	0.22	 d = AI
0.06	1.44	0.84	3.48	82.89	0.96	8.07	1.52	0.1	0.63	 e = CDGKNRSYZ
0.02	0.72	0.29	1.47	3.43	85.45	7.6	0.54	0.03	0.45	 f = O
0.02	0.47	0.16	1.28	3.3	0.77	93.14	0.58	0.03	0.25	 g = 0
0.09	2.08	0.63	6.29	14.41	1.9	14.54	58.47	0.33	1.26	 h = LT
0.05	0.58	0.05	1.49	2.07	0.72	1.3	0.77	92.6	0.38	 i = U
0.07	2.02	0.45	4.84	8.03	1.61	8.03	1.68	0.21	73.07	 j = MBP
