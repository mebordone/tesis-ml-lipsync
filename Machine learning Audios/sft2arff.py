import os
from Inventarios import inventarios

msop = 'Sft3ms'
inv_op='I2'
ngramas='3'

sftfiles = []
for file in os.listdir("{}/{}".format(os.getcwd(),msop)):
		if '.sft' in file:
			sftfiles.append("{}/{}/{}".format(os.getcwd(),msop,file))
inventario = inventarios[inv_op]
data=[]
for filename in sftfiles:
	file = open (filename,'r')
	for line in file:
		if line.startswith('\\') or line.startswith('Time'):
			continue
		lineitems = line.split('\t')
		for i in range(0,len(lineitems)):
			if lineitems [i] == '':
				lineitems [i] = '0'
		pos = lineitems[1]
		if ' ' in pos:
			poss = pos.split(' ')
			pos = poss [1]
		del lineitems[1]
		lineitems.insert(len(lineitems)-1,inventario[pos])
		lineitems = lineitems [1:]
		data.append(','.join(lineitems))
	file.close()

arff = open ('speech-{}-{}.arff'.format(inv_op,msop),'w')
arff.write("""@relation speech
@attribute Int(dB) numeric
@attribute Pitch(Hz) numeric
@attribute RawPitch numeric
@attribute SmPitch	numeric
@attribute Melogram(st) numeric
@attribute ZCross numeric
@attribute F1(Hz) numeric
@attribute F2(Hz) numeric
@attribute F3(Hz) numeric
@attribute F4(Hz) numeric
@attribute class {}{}{}
@data
""".format('{',','.join(list(set(inventario.values()))),'}'))
for item in data:
	arff.write(item)
arff.close()
