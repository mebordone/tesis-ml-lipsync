Timestamp:2013-07-22-22:24:16
Inventario:I3
Intervalo:3ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I3-3ms-1gramas
Num Instances:  61135
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1465





=== Stratified cross-validation ===

Correctly Classified Instances       53742               87.9071 %
Incorrectly Classified Instances      7393               12.0929 %
Kappa statistic                          0.8429
K&B Relative Info Score            4960775.6059 %
K&B Information Score               128993.2058 bits      2.11   bits/instance
Class complexity | order 0          158937.2465 bits      2.5998 bits/instance
Class complexity | scheme          1299058.6471 bits     21.249  bits/instance
Complexity improvement     (Sf)    -1140121.4006 bits    -18.6492 bits/instance
Mean absolute error                      0.0363
Root mean squared error                  0.1296
Relative absolute error                 25.7429 %
Root relative squared error             48.788  %
Coverage of cases (0.95 level)          97.4156 %
Mean rel. region size (0.95 level)      18.8688 %
Total Number of Instances            61135     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,906    0,015    0,920      0,906    0,913      0,897    0,980     0,950     A
                 0,885    0,010    0,880      0,885    0,882      0,873    0,978     0,922     E
                 0,830    0,058    0,764      0,830    0,795      0,748    0,965     0,880     CGJLNQSRT
                 0,590    0,001    0,890      0,590    0,710      0,722    0,947     0,708     FV
                 0,859    0,003    0,911      0,859    0,884      0,880    0,971     0,904     I
                 0,816    0,006    0,917      0,816    0,864      0,856    0,966     0,890     O
                 0,934    0,063    0,904      0,934    0,919      0,866    0,983     0,974     0
                 0,688    0,002    0,918      0,688    0,787      0,789    0,948     0,789     BMP
                 0,896    0,001    0,961      0,896    0,927      0,926    0,981     0,943     U
                 0,000    0,000    0,000      0,000    0,000      0,000    0,537     0,001     DZ
                 0,793    0,001    0,898      0,793    0,843      0,842    0,983     0,895     D
Weighted Avg.    0,879    0,039    0,881      0,879    0,879      0,846    0,975     0,928     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  8686    53   433     5    20    46   315    14     2     0    11 |     a = A
    69  4090   264    10    19    16   114    16     8     0    15 |     b = E
   318   189  9337    15    48    90  1210    22    17     0    10 |     c = CGJLNQSRT
    20    26   254   510     7    12    31     4     0     0     0 |     d = FV
    25    54   138     5  2046     8    93    10     3     0     0 |     e = I
    86    40   257     9     8  3643   383    14     4     0    19 |     f = O
   144   115  1128    13    59    63 22149    24     9     0    22 |     g = 0
    55    40   279     5    31    29   163  1340     5     0     1 |     h = BMP
     8    14    61     0     5    23    17    15  1242     0     1 |     i = U
     0     0    20     0     0     0     3     0     0     0     0 |     j = DZ
    33    28    51     1     4    41    21     0     3     0   699 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
90.62	0.55	4.52	0.05	0.21	0.48	3.29	0.15	0.02	0.0	0.11	 a = A
1.49	88.51	5.71	0.22	0.41	0.35	2.47	0.35	0.17	0.0	0.32	 b = E
2.83	1.68	82.95	0.13	0.43	0.8	10.75	0.2	0.15	0.0	0.09	 c = CGJLNQSRT
2.31	3.01	29.4	59.03	0.81	1.39	3.59	0.46	0.0	0.0	0.0	 d = FV
1.05	2.27	5.79	0.21	85.89	0.34	3.9	0.42	0.13	0.0	0.0	 e = I
1.93	0.9	5.76	0.2	0.18	81.63	8.58	0.31	0.09	0.0	0.43	 f = O
0.61	0.48	4.75	0.05	0.25	0.27	93.35	0.1	0.04	0.0	0.09	 g = 0
2.82	2.05	14.32	0.26	1.59	1.49	8.37	68.79	0.26	0.0	0.05	 h = BMP
0.58	1.01	4.4	0.0	0.36	1.66	1.23	1.08	89.61	0.0	0.07	 i = U
0.0	0.0	86.96	0.0	0.0	0.0	13.04	0.0	0.0	0.0	0.0	 j = DZ
3.75	3.18	5.79	0.11	0.45	4.65	2.38	0.0	0.34	0.0	79.34	 k = D
