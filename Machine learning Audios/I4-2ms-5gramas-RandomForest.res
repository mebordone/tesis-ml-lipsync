Timestamp:2013-07-22-22:56:45
Inventario:I4
Intervalo:2ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I4-2ms-5gramas
Num Instances:  91699
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1474





=== Stratified cross-validation ===

Correctly Classified Instances       81940               89.3576 %
Incorrectly Classified Instances      9759               10.6424 %
Kappa statistic                          0.861 
K&B Relative Info Score            7555141.9723 %
K&B Information Score               192573.2311 bits      2.1001 bits/instance
Class complexity | order 0          233711.6934 bits      2.5487 bits/instance
Class complexity | scheme          2987558.9391 bits     32.5801 bits/instance
Complexity improvement     (Sf)    -2753847.2456 bits    -30.0314 bits/instance
Mean absolute error                      0.0367
Root mean squared error                  0.1295
Relative absolute error                 23.7717 %
Root relative squared error             46.6396 %
Coverage of cases (0.95 level)          96.9989 %
Mean rel. region size (0.95 level)      19.4114 %
Total Number of Instances            91699     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,899    0,000    0,937      0,899    0,917      0,917    0,975     0,931     C
                 0,920    0,008    0,909      0,920    0,914      0,907    0,980     0,945     E
                 0,707    0,002    0,804      0,707    0,752      0,750    0,937     0,761     FV
                 0,928    0,019    0,921      0,928    0,924      0,906    0,979     0,960     AI
                 0,849    0,039    0,802      0,849    0,825      0,792    0,967     0,890     CDGKNRSYZ
                 0,852    0,005    0,924      0,852    0,887      0,879    0,959     0,900     O
                 0,942    0,056    0,916      0,942    0,929      0,882    0,984     0,968     0
                 0,593    0,006    0,774      0,593    0,671      0,667    0,911     0,681     LT
                 0,924    0,001    0,973      0,924    0,948      0,947    0,989     0,965     U
                 0,741    0,003    0,900      0,741    0,813      0,811    0,941     0,821     MBP
Weighted Avg.    0,894    0,033    0,893      0,894    0,892      0,864    0,974     0,929     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   489     7     1    10    19     1    10     2     3     2 |     a = C
     7  6355    13    95   214    22   146    32     6    20 |     b = E
     0    22   909    45   225    12    52    14     0     7 |     c = FV
     7    94    23 16611   467    47   506    97     4    38 |     d = AI
     5   165    96   392 12121   119  1116   181    10    67 |     e = CDGKNRSYZ
     1    52    20   109   219  5662   539    26     3    15 |     f = O
     9   164    39   427  1055   178 33741   155     9    52 |     g = 0
     1    66    17   206   499    37   489  1969     8    31 |     h = LT
     2    15     2    31    50    12    22    15  1922     9 |     i = U
     1    54    11   116   253    35   222    52    10  2161 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
89.89	1.29	0.18	1.84	3.49	0.18	1.84	0.37	0.55	0.37	 a = C
0.1	91.97	0.19	1.37	3.1	0.32	2.11	0.46	0.09	0.29	 b = E
0.0	1.71	70.68	3.5	17.5	0.93	4.04	1.09	0.0	0.54	 c = FV
0.04	0.53	0.13	92.83	2.61	0.26	2.83	0.54	0.02	0.21	 d = AI
0.04	1.16	0.67	2.75	84.93	0.83	7.82	1.27	0.07	0.47	 e = CDGKNRSYZ
0.02	0.78	0.3	1.64	3.3	85.19	8.11	0.39	0.05	0.23	 f = O
0.03	0.46	0.11	1.19	2.94	0.5	94.17	0.43	0.03	0.15	 g = 0
0.03	1.99	0.51	6.2	15.02	1.11	14.72	59.25	0.24	0.93	 h = LT
0.1	0.72	0.1	1.49	2.4	0.58	1.06	0.72	92.4	0.43	 i = U
0.03	1.85	0.38	3.98	8.68	1.2	7.62	1.78	0.34	74.13	 j = MBP
