Timestamp:2013-07-22-21:55:49
Inventario:I2
Intervalo:10ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I2-10ms-3gramas
Num Instances:  18294
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3086





=== Stratified cross-validation ===

Correctly Classified Instances       13798               75.4236 %
Incorrectly Classified Instances      4496               24.5764 %
Kappa statistic                          0.6934
K&B Relative Info Score            1211042.6292 %
K&B Information Score                35233.1192 bits      1.9259 bits/instance
Class complexity | order 0           53205.4817 bits      2.9084 bits/instance
Class complexity | scheme          1202762.6907 bits     65.7463 bits/instance
Complexity improvement     (Sf)    -1149557.209 bits    -62.8379 bits/instance
Mean absolute error                      0.0598
Root mean squared error                  0.1714
Relative absolute error                 44.4844 %
Root relative squared error             66.0908 %
Coverage of cases (0.95 level)          93.9488 %
Mean rel. region size (0.95 level)      24.3923 %
Total Number of Instances            18294     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,848    0,058    0,738      0,848    0,789      0,748    0,953     0,869     A
                 0,447    0,024    0,493      0,447    0,469      0,444    0,848     0,398     LGJKC
                 0,734    0,033    0,655      0,734    0,692      0,666    0,939     0,720     E
                 0,291    0,004    0,497      0,291    0,367      0,374    0,853     0,285     FV
                 0,703    0,013    0,690      0,703    0,697      0,684    0,948     0,727     I
                 0,675    0,026    0,683      0,675    0,679      0,653    0,915     0,698     O
                 0,584    0,049    0,564      0,584    0,574      0,527    0,898     0,565     SNRTY
                 0,915    0,058    0,904      0,915    0,909      0,855    0,974     0,955     0
                 0,429    0,009    0,628      0,429    0,510      0,506    0,886     0,490     BMP
                 0,347    0,002    0,699      0,347    0,463      0,487    0,920     0,470     DZ
                 0,655    0,003    0,824      0,655    0,730      0,729    0,949     0,762     U
                 0,565    0,010    0,708      0,565    0,629      0,619    0,928     0,629     SNRTXY
Weighted Avg.    0,754    0,043    0,751      0,754    0,749      0,712    0,941     0,779     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 2493   45   81    7   16   60  101   83   20    6    7   21 |    a = A
  127  400   51    7   24   41  121   92    7    3    6   15 |    b = LGJKC
   99   56 1056   10   50   36   54   35   11    8    2   21 |    c = E
   33   22   28   76   10   14   51   18    6    0    0    3 |    d = FV
   15   18   91    6  526    4   35   25   15    0    4    9 |    e = I
  117   32   55    5    9  929   53  108   21   11   11   26 |    f = O
  199   93   66   20   32   60 1046  217   24    4    8   21 |    g = SNRTY
  106   48   51    9   32   66  220 6216   22    0    4   22 |    h = 0
   57   28   44    7   36   42   60   43  255    1    9   12 |    i = BMP
   42   10   23    3    5   41   20    8    4   95    5   18 |    j = DZ
   21   19   21    0    3   41   12    3   13    4  277    9 |    k = U
   69   40   45    3   19   27   82   30    8    4    3  429 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
84.8	1.53	2.76	0.24	0.54	2.04	3.44	2.82	0.68	0.2	0.24	0.71	 a = A
14.21	44.74	5.7	0.78	2.68	4.59	13.53	10.29	0.78	0.34	0.67	1.68	 b = LGJKC
6.88	3.89	73.44	0.7	3.48	2.5	3.76	2.43	0.76	0.56	0.14	1.46	 c = E
12.64	8.43	10.73	29.12	3.83	5.36	19.54	6.9	2.3	0.0	0.0	1.15	 d = FV
2.01	2.41	12.17	0.8	70.32	0.53	4.68	3.34	2.01	0.0	0.53	1.2	 e = I
8.5	2.32	3.99	0.36	0.65	67.47	3.85	7.84	1.53	0.8	0.8	1.89	 f = O
11.12	5.2	3.69	1.12	1.79	3.35	58.44	12.12	1.34	0.22	0.45	1.17	 g = SNRTY
1.56	0.71	0.75	0.13	0.47	0.97	3.24	91.47	0.32	0.0	0.06	0.32	 h = 0
9.6	4.71	7.41	1.18	6.06	7.07	10.1	7.24	42.93	0.17	1.52	2.02	 i = BMP
15.33	3.65	8.39	1.09	1.82	14.96	7.3	2.92	1.46	34.67	1.82	6.57	 j = DZ
4.96	4.49	4.96	0.0	0.71	9.69	2.84	0.71	3.07	0.95	65.48	2.13	 k = U
9.09	5.27	5.93	0.4	2.5	3.56	10.8	3.95	1.05	0.53	0.4	56.52	 l = SNRTXY
