Timestamp:2013-07-23-02:58:54
Inventario:I2
Intervalo:6ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I2-6ms-2gramas
Num Instances:  30452
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.2528





=== Stratified cross-validation ===

Correctly Classified Instances       24235               79.5843 %
Incorrectly Classified Instances      6217               20.4157 %
Kappa statistic                          0.7445
K&B Relative Info Score            2193084.769  %
K&B Information Score                63308.1087 bits      2.0789 bits/instance
Class complexity | order 0           87888.3762 bits      2.8861 bits/instance
Class complexity | scheme          2023032.9692 bits     66.4335 bits/instance
Complexity improvement     (Sf)    -1935144.593 bits    -63.5474 bits/instance
Mean absolute error                      0.0504
Root mean squared error                  0.1588
Relative absolute error                 37.6969 %
Root relative squared error             61.4353 %
Coverage of cases (0.95 level)          93.8198 %
Mean rel. region size (0.95 level)      20.8093 %
Total Number of Instances            30452     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,869    0,041    0,801      0,869    0,834      0,802    0,961     0,901     A
                 0,553    0,021    0,575      0,553    0,564      0,542    0,876     0,517     LGJKC
                 0,815    0,023    0,746      0,815    0,779      0,760    0,959     0,828     E
                 0,465    0,004    0,621      0,465    0,532      0,532    0,863     0,420     FV
                 0,788    0,010    0,771      0,788    0,779      0,770    0,958     0,811     I
                 0,749    0,019    0,758      0,749    0,753      0,734    0,933     0,789     O
                 0,604    0,044    0,590      0,604    0,597      0,554    0,901     0,592     SNRTY
                 0,901    0,062    0,900      0,901    0,900      0,839    0,967     0,946     0
                 0,562    0,007    0,728      0,562    0,634      0,629    0,896     0,612     BMP
                 0,618    0,002    0,793      0,618    0,695      0,696    0,953     0,702     DZ
                 0,795    0,002    0,894      0,795    0,841      0,839    0,969     0,875     U
                 0,697    0,008    0,782      0,697    0,737      0,728    0,942     0,753     SNRTXY
Weighted Avg.    0,796    0,040    0,795      0,796    0,794      0,757    0,947     0,827     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  4193    63    84    10    24    82   135   165    21    14     4    30 |     a = A
   123   826    66     7    27    46   191   156    11     2    14    26 |     b = LGJKC
   103    75  1912    17    34    26    63    59    20     8     5    25 |     c = E
    34    16    36   200    10    20    74    23     8     2     3     4 |     d = FV
    26    28    78     8   950     3    33    44    20     1     2    13 |     e = I
   134    42    55     9    14  1687    65   183    22    10     8    22 |     f = O
   236   169    84    37    60    70  1758   380    42    14    15    47 |     g = SNRTY
   207   136    97    21    57   138   407 10456    42     8     6    35 |     h = 0
    59    26    49     5    40    34   112    83   551     2     7    12 |     i = BMP
    39     5    25     2     4    43    18    12     5   280     0    20 |     j = DZ
    12    18    21     3     4    42    18    11     6     0   554     8 |     k = U
    66    32    57     3     8    36   106    47     9    12     2   868 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
86.9	1.31	1.74	0.21	0.5	1.7	2.8	3.42	0.44	0.29	0.08	0.62	 a = A
8.23	55.25	4.41	0.47	1.81	3.08	12.78	10.43	0.74	0.13	0.94	1.74	 b = LGJKC
4.39	3.2	81.47	0.72	1.45	1.11	2.68	2.51	0.85	0.34	0.21	1.07	 c = E
7.91	3.72	8.37	46.51	2.33	4.65	17.21	5.35	1.86	0.47	0.7	0.93	 d = FV
2.16	2.32	6.47	0.66	78.77	0.25	2.74	3.65	1.66	0.08	0.17	1.08	 e = I
5.95	1.87	2.44	0.4	0.62	74.94	2.89	8.13	0.98	0.44	0.36	0.98	 f = O
8.1	5.8	2.88	1.27	2.06	2.4	60.37	13.05	1.44	0.48	0.52	1.61	 g = SNRTY
1.78	1.17	0.84	0.18	0.49	1.19	3.51	90.06	0.36	0.07	0.05	0.3	 h = 0
6.02	2.65	5.0	0.51	4.08	3.47	11.43	8.47	56.22	0.2	0.71	1.22	 i = BMP
8.61	1.1	5.52	0.44	0.88	9.49	3.97	2.65	1.1	61.81	0.0	4.42	 j = DZ
1.72	2.58	3.01	0.43	0.57	6.03	2.58	1.58	0.86	0.0	79.48	1.15	 k = U
5.3	2.57	4.57	0.24	0.64	2.89	8.51	3.77	0.72	0.96	0.16	69.66	 l = SNRTXY
