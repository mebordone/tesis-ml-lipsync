Timestamp:2013-07-22-23:06:57
Inventario:I4
Intervalo:15ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I4-15ms-5gramas
Num Instances:  12185
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  42 4Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  43 4RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  44 4SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  45 4Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  47 4F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  48 4F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  49 4F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  50 4F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3231





=== Stratified cross-validation ===

Correctly Classified Instances        8995               73.8203 %
Incorrectly Classified Instances      3190               26.1797 %
Kappa statistic                          0.6625
K&B Relative Info Score             751192.5533 %
K&B Information Score                19535.4419 bits      1.6032 bits/instance
Class complexity | order 0           31669.4139 bits      2.599  bits/instance
Class complexity | scheme           657686.4496 bits     53.9751 bits/instance
Complexity improvement     (Sf)    -626017.0357 bits    -51.376  bits/instance
Mean absolute error                      0.0749
Root mean squared error                  0.1916
Relative absolute error                 47.6972 %
Root relative squared error             68.3722 %
Coverage of cases (0.95 level)          95.0513 %
Mean rel. region size (0.95 level)      29.1883 %
Total Number of Instances            12185     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,319    0,002    0,500      0,319    0,390      0,397    0,885     0,338     C
                 0,673    0,041    0,586      0,673    0,626      0,593    0,926     0,649     E
                 0,164    0,003    0,439      0,164    0,239      0,262    0,806     0,190     FV
                 0,785    0,086    0,700      0,785    0,740      0,670    0,929     0,819     AI
                 0,678    0,097    0,574      0,678    0,622      0,544    0,897     0,631     CDGKNRSYZ
                 0,576    0,025    0,660      0,576    0,615      0,587    0,911     0,633     O
                 0,923    0,049    0,914      0,923    0,919      0,872    0,978     0,959     0
                 0,219    0,007    0,546      0,219    0,313      0,331    0,833     0,298     LT
                 0,530    0,003    0,795      0,530    0,636      0,642    0,937     0,642     U
                 0,318    0,005    0,694      0,318    0,436      0,459    0,876     0,429     MBP
Weighted Avg.    0,738    0,057    0,736      0,738    0,729      0,682    0,933     0,763     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   23    8    0   20   16    5    0    0    0    0 |    a = C
    7  653    5  138  107   24   20    7    0   10 |    b = E
    1   27   29   20   77   14    6    1    0    2 |    c = FV
    7  174    9 1955  202   46   68   17    4   10 |    d = AI
    2  115   14  252 1342   62  141   29    9   12 |    e = CDGKNRSYZ
    5   47    3  137  124  541   63    4    9    7 |    f = O
    0   21    1   77  188   30 4074   14    1    8 |    g = 0
    0   29    1   98  149   19   51  100    5    5 |    h = LT
    1   14    1   28   36   45    2    5  151    2 |    i = U
    0   27    3   66   95   34   30    6   11  127 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
31.94	11.11	0.0	27.78	22.22	6.94	0.0	0.0	0.0	0.0	 a = C
0.72	67.25	0.51	14.21	11.02	2.47	2.06	0.72	0.0	1.03	 b = E
0.56	15.25	16.38	11.3	43.5	7.91	3.39	0.56	0.0	1.13	 c = FV
0.28	6.98	0.36	78.45	8.11	1.85	2.73	0.68	0.16	0.4	 d = AI
0.1	5.81	0.71	12.74	67.85	3.13	7.13	1.47	0.46	0.61	 e = CDGKNRSYZ
0.53	5.0	0.32	14.57	13.19	57.55	6.7	0.43	0.96	0.74	 f = O
0.0	0.48	0.02	1.74	4.26	0.68	92.3	0.32	0.02	0.18	 g = 0
0.0	6.35	0.22	21.44	32.6	4.16	11.16	21.88	1.09	1.09	 h = LT
0.35	4.91	0.35	9.82	12.63	15.79	0.7	1.75	52.98	0.7	 i = U
0.0	6.77	0.75	16.54	23.81	8.52	7.52	1.5	2.76	31.83	 j = MBP
