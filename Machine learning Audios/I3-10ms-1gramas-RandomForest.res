Timestamp:2013-07-22-22:29:38
Inventario:I3
Intervalo:10ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I3-10ms-1gramas
Num Instances:  18296
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.2593





=== Stratified cross-validation ===

Correctly Classified Instances       14218               77.711  %
Incorrectly Classified Instances      4078               22.289  %
Kappa statistic                          0.7133
K&B Relative Info Score            1244081.5301 %
K&B Information Score                32773.5383 bits      1.7913 bits/instance
Class complexity | order 0           48176.7702 bits      2.6332 bits/instance
Class complexity | scheme          1180208.1257 bits     64.5063 bits/instance
Complexity improvement     (Sf)    -1132031.3555 bits    -61.8732 bits/instance
Mean absolute error                      0.0581
Root mean squared error                  0.1718
Relative absolute error                 40.6841 %
Root relative squared error             64.2945 %
Coverage of cases (0.95 level)          93.8402 %
Mean rel. region size (0.95 level)      22.5275 %
Total Number of Instances            18296     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,822    0,043    0,787      0,822    0,804      0,766    0,947     0,862     A
                 0,743    0,030    0,680      0,743    0,711      0,685    0,939     0,733     E
                 0,711    0,099    0,625      0,711    0,665      0,583    0,903     0,696     CGJLNQSRT
                 0,238    0,003    0,539      0,238    0,330      0,352    0,788     0,258     FV
                 0,699    0,009    0,766      0,699    0,731      0,721    0,938     0,739     I
                 0,634    0,019    0,735      0,634    0,681      0,659    0,912     0,709     O
                 0,912    0,063    0,895      0,912    0,904      0,846    0,970     0,957     0
                 0,401    0,005    0,719      0,401    0,515      0,526    0,864     0,489     BMP
                 0,697    0,003    0,836      0,697    0,760      0,758    0,935     0,774     U
                 0,000    0,000    0,000      0,000    0,000      0,000    0,498     0,000     DZ
                 0,342    0,002    0,679      0,342    0,455      0,477    0,905     0,447     D
Weighted Avg.    0,777    0,053    0,777      0,777    0,773      0,726    0,938     0,810     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 2417   83  260    4   17   55   90    7    2    0    5 |    a = A
   82 1069  180    7   21   24   33   13    3    0    6 |    b = E
  265  153 2447   15   54   71  383   23   18    0   14 |    c = CGJLNQSRT
   24   25  116   62    6   11   11    4    2    0    0 |    d = FV
   18   79   85    3  523    2   26    9    2    0    1 |    e = I
  106   53  171    9    6  873  121   12   14    0   12 |    f = O
   73   35  401    8   19   42 6203   13    2    0    2 |    g = 0
   45   38  136    4   31   32   57  238   11    0    2 |    h = BMP
   10   11   56    3    3   33    4    7  295    0    1 |    i = U
    0    0    7    0    0    0    1    0    0    0    0 |    j = DZ
   32   25   58    0    3   45    3    5    4    0   91 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
82.21	2.82	8.84	0.14	0.58	1.87	3.06	0.24	0.07	0.0	0.17	 a = A
5.7	74.34	12.52	0.49	1.46	1.67	2.29	0.9	0.21	0.0	0.42	 b = E
7.7	4.44	71.07	0.44	1.57	2.06	11.12	0.67	0.52	0.0	0.41	 c = CGJLNQSRT
9.2	9.58	44.44	23.75	2.3	4.21	4.21	1.53	0.77	0.0	0.0	 d = FV
2.41	10.56	11.36	0.4	69.92	0.27	3.48	1.2	0.27	0.0	0.13	 e = I
7.7	3.85	12.42	0.65	0.44	63.4	8.79	0.87	1.02	0.0	0.87	 f = O
1.07	0.51	5.9	0.12	0.28	0.62	91.25	0.19	0.03	0.0	0.03	 g = 0
7.58	6.4	22.9	0.67	5.22	5.39	9.6	40.07	1.85	0.0	0.34	 h = BMP
2.36	2.6	13.24	0.71	0.71	7.8	0.95	1.65	69.74	0.0	0.24	 i = U
0.0	0.0	87.5	0.0	0.0	0.0	12.5	0.0	0.0	0.0	0.0	 j = DZ
12.03	9.4	21.8	0.0	1.13	16.92	1.13	1.88	1.5	0.0	34.21	 k = D
