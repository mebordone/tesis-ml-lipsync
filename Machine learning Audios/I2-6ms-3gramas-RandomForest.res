Timestamp:2013-07-23-02:59:30
Inventario:I2
Intervalo:6ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I2-6ms-3gramas
Num Instances:  30451
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.2543





=== Stratified cross-validation ===

Correctly Classified Instances       24510               80.49   %
Incorrectly Classified Instances      5941               19.51   %
Kappa statistic                          0.7553
K&B Relative Info Score            2193865.5276 %
K&B Information Score                63331.1736 bits      2.0798 bits/instance
Class complexity | order 0           87886.9852 bits      2.8862 bits/instance
Class complexity | scheme          1693386.5793 bits     55.6102 bits/instance
Complexity improvement     (Sf)    -1605499.5941 bits    -52.724  bits/instance
Mean absolute error                      0.0512
Root mean squared error                  0.1562
Relative absolute error                 38.3617 %
Root relative squared error             60.4439 %
Coverage of cases (0.95 level)          94.8803 %
Mean rel. region size (0.95 level)      22.0847 %
Total Number of Instances            30451     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,887    0,040    0,806      0,887    0,844      0,815    0,966     0,910     A
                 0,548    0,019    0,598      0,548    0,572      0,552    0,888     0,559     LGJKC
                 0,814    0,024    0,741      0,814    0,776      0,757    0,956     0,821     E
                 0,484    0,004    0,654      0,484    0,556      0,557    0,911     0,487     FV
                 0,794    0,009    0,784      0,794    0,789      0,780    0,960     0,824     I
                 0,749    0,017    0,774      0,749    0,761      0,743    0,935     0,794     O
                 0,611    0,044    0,595      0,611    0,603      0,560    0,914     0,626     SNRTY
                 0,912    0,061    0,902      0,912    0,907      0,849    0,971     0,951     0
                 0,574    0,006    0,771      0,574    0,658      0,656    0,907     0,638     BMP
                 0,618    0,002    0,836      0,618    0,711      0,715    0,955     0,723     DZ
                 0,799    0,002    0,895      0,799    0,845      0,843    0,974     0,886     U
                 0,708    0,006    0,837      0,708    0,767      0,761    0,946     0,783     SNRTXY
Weighted Avg.    0,805    0,039    0,804      0,805    0,803      0,768    0,952     0,839     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  4279    65    77     7    12    70   124   142    15     9     2    23 |     a = A
   127   820    53    17    28    48   198   161    11     3     8    21 |     b = LGJKC
   119    52  1911    11    44    28    76    57    18     9     4    18 |     c = E
    36    14    35   208     6    17    72    22     9     0     2     9 |     d = FV
    27    33    81     4   957     6    38    43    12     1     1     3 |     e = I
   135    38    55     8     9  1686    76   185    21    10    11    17 |     f = O
   251   141   104    33    49    82  1780   393    31     7    10    31 |     g = SNRTY
   165   115   109    15    62   107   384 10587    29     4     8    24 |     h = 0
    66    36    46     5    33    35    94    85   563     2     8     7 |     i = BMP
    33     7    34     3     1    42    22    12     1   280     2    16 |     j = DZ
    23    11    24     1     4    27    24     9    12     2   557     3 |     k = U
    51    39    49     6    15    30   104    45     8     8     9   882 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
88.68	1.35	1.6	0.15	0.25	1.45	2.57	2.94	0.31	0.19	0.04	0.48	 a = A
8.49	54.85	3.55	1.14	1.87	3.21	13.24	10.77	0.74	0.2	0.54	1.4	 b = LGJKC
5.07	2.22	81.42	0.47	1.87	1.19	3.24	2.43	0.77	0.38	0.17	0.77	 c = E
8.37	3.26	8.14	48.37	1.4	3.95	16.74	5.12	2.09	0.0	0.47	2.09	 d = FV
2.24	2.74	6.72	0.33	79.35	0.5	3.15	3.57	1.0	0.08	0.08	0.25	 e = I
6.0	1.69	2.44	0.36	0.4	74.9	3.38	8.22	0.93	0.44	0.49	0.76	 f = O
8.62	4.84	3.57	1.13	1.68	2.82	61.13	13.5	1.06	0.24	0.34	1.06	 g = SNRTY
1.42	0.99	0.94	0.13	0.53	0.92	3.31	91.2	0.25	0.03	0.07	0.21	 h = 0
6.73	3.67	4.69	0.51	3.37	3.57	9.59	8.67	57.45	0.2	0.82	0.71	 i = BMP
7.28	1.55	7.51	0.66	0.22	9.27	4.86	2.65	0.22	61.81	0.44	3.53	 j = DZ
3.3	1.58	3.44	0.14	0.57	3.87	3.44	1.29	1.72	0.29	79.91	0.43	 k = U
4.09	3.13	3.93	0.48	1.2	2.41	8.35	3.61	0.64	0.64	0.72	70.79	 l = SNRTXY
