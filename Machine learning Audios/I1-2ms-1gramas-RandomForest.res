Timestamp:2013-07-22-21:04:55
Inventario:I1
Intervalo:2ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I1-2ms-1gramas
Num Instances:  91703
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1406





=== Stratified cross-validation ===

Correctly Classified Instances       81069               88.4039 %
Incorrectly Classified Instances     10634               11.5961 %
Kappa statistic                          0.8518
K&B Relative Info Score            7526077.146  %
K&B Information Score               217257.9224 bits      2.3691 bits/instance
Class complexity | order 0          264698.8661 bits      2.8865 bits/instance
Class complexity | scheme          1741950.5752 bits     18.9956 bits/instance
Complexity improvement     (Sf)    -1477251.7091 bits    -16.1091 bits/instance
Mean absolute error                      0.029 
Root mean squared error                  0.1161
Relative absolute error                 23.6939 %
Root relative squared error             46.9244 %
Coverage of cases (0.95 level)          97.348  %
Mean rel. region size (0.95 level)      17.1647 %
Total Number of Instances            91703     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,924    0,010    0,944      0,924    0,934      0,922    0,985     0,962     a
                 0,746    0,002    0,914      0,746    0,821      0,821    0,961     0,829     b
                 0,915    0,006    0,923      0,915    0,919      0,913    0,986     0,948     e
                 0,871    0,001    0,932      0,871    0,900      0,900    0,987     0,927     d
                 0,666    0,001    0,917      0,666    0,772      0,779    0,963     0,764     f
                 0,897    0,003    0,927      0,897    0,912      0,908    0,978     0,931     i
                 0,333    0,004    0,643      0,333    0,439      0,455    0,894     0,414     k
                 0,755    0,001    0,915      0,755    0,827      0,829    0,951     0,825     j
                 0,772    0,012    0,868      0,772    0,817      0,801    0,965     0,868     l
                 0,845    0,004    0,946      0,845    0,893      0,887    0,977     0,914     o
                 0,965    0,088    0,875      0,965    0,918      0,863    0,987     0,980     0
                 0,739    0,023    0,666      0,739    0,701      0,682    0,971     0,790     s
                 0,935    0,001    0,965      0,935    0,949      0,948    0,989     0,967     u
Weighted Avg.    0,884    0,040    0,885      0,884    0,881      0,855    0,979     0,925     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 13237    24    43    10     5    18    28     9   161    40   588   163     7 |     a = a
    50  2174    34     0     2    27    13     2    75    24   398   111     5 |     b = b
    65    26  6324    11     7    20    19     6   111    12   238    61    10 |     c = e
    31     2    26  1172     1     4     0     0    26    30    27    24     3 |     d = d
    16     8    18     4   857    10     7     1    33    13   109   209     1 |     e = f
    16    26    33     0     9  3194     7     3    53    10   169    37     4 |     f = i
    31     4    38     0     6     6   592    13    70    18   631   363     8 |     g = k
    30     3    20     0     4     8    11   781    15     1    67    91     4 |     h = j
   251    32   129    15     6    48    51     8  6699    68  1023   344     6 |     i = l
    67    16    27    19    11    14    22     1    92  5616   667    89     5 |     j = o
   169    44   124    22    17    65    53    13   229    66 34566   455    10 |     k = 0
    54     9    22     0    10    23   110    13   136    20   980  3913     8 |     l = s
     7    11    10     4     0     8     7     4    21    16    36    12  1944 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
92.35	0.17	0.3	0.07	0.03	0.13	0.2	0.06	1.12	0.28	4.1	1.14	0.05	 a = a
1.72	74.58	1.17	0.0	0.07	0.93	0.45	0.07	2.57	0.82	13.65	3.81	0.17	 b = b
0.94	0.38	91.52	0.16	0.1	0.29	0.27	0.09	1.61	0.17	3.44	0.88	0.14	 c = e
2.3	0.15	1.93	87.07	0.07	0.3	0.0	0.0	1.93	2.23	2.01	1.78	0.22	 d = d
1.24	0.62	1.4	0.31	66.64	0.78	0.54	0.08	2.57	1.01	8.48	16.25	0.08	 e = f
0.45	0.73	0.93	0.0	0.25	89.69	0.2	0.08	1.49	0.28	4.75	1.04	0.11	 f = i
1.74	0.22	2.13	0.0	0.34	0.34	33.26	0.73	3.93	1.01	35.45	20.39	0.45	 g = k
2.9	0.29	1.93	0.0	0.39	0.77	1.06	75.46	1.45	0.1	6.47	8.79	0.39	 h = j
2.89	0.37	1.49	0.17	0.07	0.55	0.59	0.09	77.18	0.78	11.79	3.96	0.07	 i = l
1.01	0.24	0.41	0.29	0.17	0.21	0.33	0.02	1.38	84.5	10.04	1.34	0.08	 j = o
0.47	0.12	0.35	0.06	0.05	0.18	0.15	0.04	0.64	0.18	96.46	1.27	0.03	 k = 0
1.02	0.17	0.42	0.0	0.19	0.43	2.08	0.25	2.57	0.38	18.5	73.86	0.15	 l = s
0.34	0.53	0.48	0.19	0.0	0.38	0.34	0.19	1.01	0.77	1.73	0.58	93.46	 m = u
