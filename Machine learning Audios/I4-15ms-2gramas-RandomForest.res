Timestamp:2013-07-22-23:06:12
Inventario:I4
Intervalo:15ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I4-15ms-2gramas
Num Instances:  12188
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3328





=== Stratified cross-validation ===

Correctly Classified Instances        8729               71.6196 %
Incorrectly Classified Instances      3459               28.3804 %
Kappa statistic                          0.6349
K&B Relative Info Score             736509.7514 %
K&B Information Score                19150.4447 bits      1.5713 bits/instance
Class complexity | order 0           31673.8078 bits      2.5988 bits/instance
Class complexity | scheme           975955.4126 bits     80.0751 bits/instance
Complexity improvement     (Sf)    -944281.6048 bits    -77.4763 bits/instance
Mean absolute error                      0.0745
Root mean squared error                  0.1975
Relative absolute error                 47.4174 %
Root relative squared error             70.5052 %
Coverage of cases (0.95 level)          92.5993 %
Mean rel. region size (0.95 level)      27.2284 %
Total Number of Instances            12188     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,306    0,002    0,512      0,306    0,383      0,393    0,844     0,323     C
                 0,645    0,041    0,574      0,645    0,607      0,572    0,914     0,605     E
                 0,153    0,006    0,278      0,153    0,197      0,198    0,758     0,100     FV
                 0,783    0,090    0,690      0,783    0,734      0,662    0,924     0,809     AI
                 0,632    0,102    0,546      0,632    0,586      0,500    0,874     0,595     CDGKNRSYZ
                 0,547    0,027    0,632      0,547    0,586      0,556    0,890     0,601     O
                 0,905    0,052    0,908      0,905    0,907      0,854    0,973     0,951     0
                 0,153    0,012    0,340      0,153    0,211      0,209    0,765     0,159     LT
                 0,572    0,004    0,758      0,572    0,652      0,652    0,936     0,697     U
                 0,273    0,008    0,524      0,273    0,359      0,364    0,821     0,295     MBP
Weighted Avg.    0,716    0,060    0,708      0,716    0,707      0,655    0,918     0,737     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   22   15    0   22    9    3    0    1    0    0 |    a = C
    7  626   10  148  117   30   19    5    0    9 |    b = E
    0   25   27   24   74   13    6    3    1    4 |    c = FV
    6  164    6 1951  205   45   74   22    7   12 |    d = AI
    2  111   22  305 1250   74  139   41   14   20 |    e = CDGKNRSYZ
    3   47    9  120  134  514   71   13   13   16 |    f = O
    0   14    9   85  211   55 3997   32    3   11 |    g = 0
    0   46    9   83  146   21   62   70    3   17 |    h = LT
    1   15    1   18   40   31    2    4  163   10 |    i = U
    2   27    4   70  104   27   30   15   11  109 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
30.56	20.83	0.0	30.56	12.5	4.17	0.0	1.39	0.0	0.0	 a = C
0.72	64.47	1.03	15.24	12.05	3.09	1.96	0.51	0.0	0.93	 b = E
0.0	14.12	15.25	13.56	41.81	7.34	3.39	1.69	0.56	2.26	 c = FV
0.24	6.58	0.24	78.29	8.23	1.81	2.97	0.88	0.28	0.48	 d = AI
0.1	5.61	1.11	15.42	63.2	3.74	7.03	2.07	0.71	1.01	 e = CDGKNRSYZ
0.32	5.0	0.96	12.77	14.26	54.68	7.55	1.38	1.38	1.7	 f = O
0.0	0.32	0.2	1.92	4.78	1.25	90.49	0.72	0.07	0.25	 g = 0
0.0	10.07	1.97	18.16	31.95	4.6	13.57	15.32	0.66	3.72	 h = LT
0.35	5.26	0.35	6.32	14.04	10.88	0.7	1.4	57.19	3.51	 i = U
0.5	6.77	1.0	17.54	26.07	6.77	7.52	3.76	2.76	27.32	 j = MBP
