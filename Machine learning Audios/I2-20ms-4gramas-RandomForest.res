Timestamp:2013-07-22-21:58:56
Inventario:I2
Intervalo:20ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I2-20ms-4gramas
Num Instances:  9126
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3809





=== Stratified cross-validation ===

Correctly Classified Instances        6241               68.387  %
Incorrectly Classified Instances      2885               31.613  %
Kappa statistic                          0.609 
K&B Relative Info Score             528724.183  %
K&B Information Score                15595.4106 bits      1.7089 bits/instance
Class complexity | order 0           26898.0483 bits      2.9474 bits/instance
Class complexity | scheme           794561.3384 bits     87.0657 bits/instance
Complexity improvement     (Sf)    -767663.2901 bits    -84.1183 bits/instance
Mean absolute error                      0.0705
Root mean squared error                  0.1887
Relative absolute error                 51.8104 %
Root relative squared error             72.3621 %
Coverage of cases (0.95 level)          91.979  %
Mean rel. region size (0.95 level)      27.7723 %
Total Number of Instances             9126     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,805    0,083    0,657      0,805    0,723      0,667    0,931     0,809     A
                 0,262    0,029    0,319      0,262    0,288      0,256    0,771     0,204     LGJKC
                 0,646    0,049    0,538      0,646    0,587      0,549    0,913     0,576     E
                 0,101    0,005    0,230      0,101    0,141      0,144    0,779     0,102     FV
                 0,540    0,020    0,545      0,540    0,542      0,522    0,912     0,527     I
                 0,565    0,036    0,567      0,565    0,566      0,529    0,893     0,563     O
                 0,539    0,061    0,497      0,539    0,518      0,462    0,868     0,502     SNRTY
                 0,922    0,050    0,910      0,922    0,916      0,869    0,978     0,958     0
                 0,227    0,009    0,469      0,227    0,306      0,311    0,826     0,282     BMP
                 0,123    0,003    0,386      0,123    0,187      0,212    0,819     0,149     DZ
                 0,450    0,005    0,690      0,450    0,544      0,549    0,897     0,571     U
                 0,382    0,014    0,556      0,382    0,453      0,442    0,870     0,384     SNRTXY
Weighted Avg.    0,684    0,048    0,672      0,684    0,672      0,633    0,917     0,690     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1211   24   54    6   15   50   76   40    9    1    5   14 |    a = A
   87  118   44    6   20   23   78   48    8    1    5   12 |    b = LGJKC
   89   35  482    3   35   33   30   11    9    3    1   15 |    c = E
   29   12   19   14    4    9   40    4    3    0    2    2 |    d = FV
   25   27   72    1  208    5   11   17    5    0    1   13 |    e = I
  114   21   41    3    7  402   35   48    6    9   15   11 |    f = O
  101   49   59   13   29   34  493   94   14    5    3   20 |    g = SNRTY
   38   20   20    4   16   28  114 2982    5    0    0    7 |    h = 0
   46   17   30    7   28   29   38   19   68    2    7    8 |    i = BMP
   27    8   18    3    2   29   18    0    3   17    2   11 |    j = DZ
   24    6   15    1    6   41   13    1    8    0   98    5 |    k = U
   52   33   42    0   12   26   45   13    7    6    3  148 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
80.47	1.59	3.59	0.4	1.0	3.32	5.05	2.66	0.6	0.07	0.33	0.93	 a = A
19.33	26.22	9.78	1.33	4.44	5.11	17.33	10.67	1.78	0.22	1.11	2.67	 b = LGJKC
11.93	4.69	64.61	0.4	4.69	4.42	4.02	1.47	1.21	0.4	0.13	2.01	 c = E
21.01	8.7	13.77	10.14	2.9	6.52	28.99	2.9	2.17	0.0	1.45	1.45	 d = FV
6.49	7.01	18.7	0.26	54.03	1.3	2.86	4.42	1.3	0.0	0.26	3.38	 e = I
16.01	2.95	5.76	0.42	0.98	56.46	4.92	6.74	0.84	1.26	2.11	1.54	 f = O
11.05	5.36	6.46	1.42	3.17	3.72	53.94	10.28	1.53	0.55	0.33	2.19	 g = SNRTY
1.18	0.62	0.62	0.12	0.49	0.87	3.53	92.21	0.15	0.0	0.0	0.22	 h = 0
15.38	5.69	10.03	2.34	9.36	9.7	12.71	6.35	22.74	0.67	2.34	2.68	 i = BMP
19.57	5.8	13.04	2.17	1.45	21.01	13.04	0.0	2.17	12.32	1.45	7.97	 j = DZ
11.01	2.75	6.88	0.46	2.75	18.81	5.96	0.46	3.67	0.0	44.95	2.29	 k = U
13.44	8.53	10.85	0.0	3.1	6.72	11.63	3.36	1.81	1.55	0.78	38.24	 l = SNRTXY
