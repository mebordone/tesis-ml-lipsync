Timestamp:2013-07-22-22:26:04
Inventario:I3
Intervalo:3ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I3-3ms-3gramas
Num Instances:  61133
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1644





=== Stratified cross-validation ===

Correctly Classified Instances       53559               87.6106 %
Incorrectly Classified Instances      7574               12.3894 %
Kappa statistic                          0.8397
K&B Relative Info Score            4931119.0263 %
K&B Information Score               128221.1597 bits      2.0974 bits/instance
Class complexity | order 0          158934.5154 bits      2.5998 bits/instance
Class complexity | scheme          2191533.9275 bits     35.8486 bits/instance
Complexity improvement     (Sf)    -2032599.4121 bits    -33.2488 bits/instance
Mean absolute error                      0.0378
Root mean squared error                  0.1333
Relative absolute error                 26.7764 %
Root relative squared error             50.1622 %
Coverage of cases (0.95 level)          96.6925 %
Mean rel. region size (0.95 level)      18.3063 %
Total Number of Instances            61133     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,913    0,019    0,900      0,913    0,906      0,889    0,974     0,944     A
                 0,889    0,011    0,868      0,889    0,878      0,868    0,976     0,922     E
                 0,826    0,056    0,770      0,826    0,797      0,750    0,955     0,853     CGJLNQSRT
                 0,588    0,002    0,829      0,588    0,688      0,695    0,906     0,669     FV
                 0,853    0,004    0,895      0,853    0,873      0,868    0,968     0,897     I
                 0,824    0,008    0,893      0,824    0,857      0,847    0,950     0,875     O
                 0,921    0,055    0,913      0,921    0,917      0,864    0,979     0,959     0
                 0,699    0,003    0,869      0,699    0,775      0,773    0,930     0,775     BMP
                 0,899    0,001    0,959      0,899    0,928      0,927    0,983     0,944     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,498     0,000     DZ
                 0,837    0,001    0,919      0,837    0,876      0,875    0,992     0,918     D
Weighted Avg.    0,876    0,036    0,877      0,876    0,876      0,842    0,968     0,915     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  8751    51   406     9    17    43   272    24     5     1     6 |     a = A
    78  4108   245    10    21    16   105    18     5     0    15 |     b = E
   388   218  9296    39    56   128  1042    59    12     7    11 |     c = CGJLNQSRT
    33    33   223   508    12    14    30     9     0     0     2 |     d = FV
    26    59   143    12  2031     9    88    13     0     0     1 |     e = I
   114    32   239    10    12  3676   337    18     3     0    22 |     f = O
   246   161  1135    20    78   162 21844    54    14     3     7 |     g = 0
    51    36   262     5    30    29   161  1362    12     0     0 |     h = BMP
    11    11    71     0     8     9    20     9  1246     0     1 |     i = U
     0     0    18     0     0     0     4     1     0     0     0 |     j = DZ
    26    23    42     0     5    30    15     1     2     0   737 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
91.3	0.53	4.24	0.09	0.18	0.45	2.84	0.25	0.05	0.01	0.06	 a = A
1.69	88.9	5.3	0.22	0.45	0.35	2.27	0.39	0.11	0.0	0.32	 b = E
3.45	1.94	82.59	0.35	0.5	1.14	9.26	0.52	0.11	0.06	0.1	 c = CGJLNQSRT
3.82	3.82	25.81	58.8	1.39	1.62	3.47	1.04	0.0	0.0	0.23	 d = FV
1.09	2.48	6.0	0.5	85.26	0.38	3.69	0.55	0.0	0.0	0.04	 e = I
2.55	0.72	5.36	0.22	0.27	82.37	7.55	0.4	0.07	0.0	0.49	 f = O
1.04	0.68	4.78	0.08	0.33	0.68	92.08	0.23	0.06	0.01	0.03	 g = 0
2.62	1.85	13.45	0.26	1.54	1.49	8.26	69.92	0.62	0.0	0.0	 h = BMP
0.79	0.79	5.12	0.0	0.58	0.65	1.44	0.65	89.9	0.0	0.07	 i = U
0.0	0.0	78.26	0.0	0.0	0.0	17.39	4.35	0.0	0.0	0.0	 j = DZ
2.95	2.61	4.77	0.0	0.57	3.41	1.7	0.11	0.23	0.0	83.65	 k = D
