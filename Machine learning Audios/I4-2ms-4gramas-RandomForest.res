Timestamp:2013-07-22-22:54:53
Inventario:I4
Intervalo:2ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I4-2ms-4gramas
Num Instances:  91700
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1486





=== Stratified cross-validation ===

Correctly Classified Instances       81571               88.9542 %
Incorrectly Classified Instances     10129               11.0458 %
Kappa statistic                          0.8558
K&B Relative Info Score            7530814.0483 %
K&B Information Score               191951.926  bits      2.0933 bits/instance
Class complexity | order 0          233713.0492 bits      2.5487 bits/instance
Class complexity | scheme          3273978.1817 bits     35.7031 bits/instance
Complexity improvement     (Sf)    -3040265.1326 bits    -33.1545 bits/instance
Mean absolute error                      0.0368
Root mean squared error                  0.1313
Relative absolute error                 23.8451 %
Root relative squared error             47.2891 %
Coverage of cases (0.95 level)          96.7056 %
Mean rel. region size (0.95 level)      19.1636 %
Total Number of Instances            91700     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,893    0,000    0,935      0,893    0,914      0,913    0,971     0,922     C
                 0,916    0,008    0,904      0,916    0,910      0,903    0,978     0,943     E
                 0,688    0,003    0,787      0,688    0,734      0,733    0,929     0,735     FV
                 0,926    0,019    0,920      0,926    0,923      0,905    0,978     0,958     AI
                 0,840    0,040    0,795      0,840    0,817      0,782    0,965     0,881     CDGKNRSYZ
                 0,850    0,005    0,924      0,850    0,886      0,878    0,956     0,897     O
                 0,939    0,057    0,913      0,939    0,926      0,877    0,983     0,966     0
                 0,587    0,007    0,758      0,587    0,661      0,656    0,905     0,668     LT
                 0,919    0,001    0,970      0,919    0,944      0,943    0,988     0,961     U
                 0,744    0,003    0,877      0,744    0,805      0,803    0,937     0,813     MBP
Weighted Avg.    0,890    0,034    0,889      0,890    0,888      0,859    0,972     0,925     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   486     7     1    11    14     1    19     3     2     0 |     a = C
     8  6330    12   105   210    20   156    38     4    27 |     b = E
     0    27   885    46   231    17    62    10     0     8 |     c = FV
     5   111    29 16569   449    46   537   100     6    42 |     d = AI
     5   187   107   441 11986   106  1159   191     9    81 |     e = CDGKNRSYZ
     2    34    19   112   240  5650   527    31     6    25 |     f = O
    12   167    46   394  1130   197 33633   173     6    72 |     g = 0
     0    68    14   179   517    35   508  1950    14    38 |     h = LT
     2    18     2    25    54    14    22    21  1912    10 |     i = U
     0    53     9   119   249    28   217    57    13  2170 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
89.34	1.29	0.18	2.02	2.57	0.18	3.49	0.55	0.37	0.0	 a = C
0.12	91.61	0.17	1.52	3.04	0.29	2.26	0.55	0.06	0.39	 b = E
0.0	2.1	68.82	3.58	17.96	1.32	4.82	0.78	0.0	0.62	 c = FV
0.03	0.62	0.16	92.6	2.51	0.26	3.0	0.56	0.03	0.23	 d = AI
0.04	1.31	0.75	3.09	83.98	0.74	8.12	1.34	0.06	0.57	 e = CDGKNRSYZ
0.03	0.51	0.29	1.69	3.61	85.01	7.93	0.47	0.09	0.38	 f = O
0.03	0.47	0.13	1.1	3.15	0.55	93.87	0.48	0.02	0.2	 g = 0
0.0	2.05	0.42	5.39	15.56	1.05	15.29	58.68	0.42	1.14	 h = LT
0.1	0.87	0.1	1.2	2.6	0.67	1.06	1.01	91.92	0.48	 i = U
0.0	1.82	0.31	4.08	8.54	0.96	7.44	1.96	0.45	74.44	 j = MBP
