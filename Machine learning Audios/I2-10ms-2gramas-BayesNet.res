Timestamp:2013-08-30-04:41:43
Inventario:I2
Intervalo:10ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I2-10ms-2gramas
Num Instances:  18295
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(10): class 
0Pitch(Hz)(5): class 
0RawPitch(6): class 
0SmPitch(6): class 
0Melogram(st)(9): class 
0ZCross(11): class 
0F1(Hz)(13): class 
0F2(Hz)(15): class 
0F3(Hz)(10): class 
0F4(Hz)(11): class 
1Int(dB)(10): class 
1Pitch(Hz)(7): class 
1RawPitch(6): class 
1SmPitch(7): class 
1Melogram(st)(8): class 
1ZCross(12): class 
1F1(Hz)(12): class 
1F2(Hz)(11): class 
1F3(Hz)(10): class 
1F4(Hz)(8): class 
class(12): 
LogScore Bayes: -456971.22328653897
LogScore BDeu: -466495.68136092706
LogScore MDL: -465410.5092483529
LogScore ENTROPY: -455522.51829781866
LogScore AIC: -457537.51829781866




=== Stratified cross-validation ===

Correctly Classified Instances       10861               59.3659 %
Incorrectly Classified Instances      7434               40.6341 %
Kappa statistic                          0.4892
K&B Relative Info Score             885541.7958 %
K&B Information Score                25762.7297 bits      1.4082 bits/instance
Class complexity | order 0           53206.9102 bits      2.9083 bits/instance
Class complexity | scheme           124673.7302 bits      6.8146 bits/instance
Complexity improvement     (Sf)     -71466.82   bits     -3.9064 bits/instance
Mean absolute error                      0.0695
Root mean squared error                  0.2334
Relative absolute error                 51.6952 %
Root relative squared error             89.9951 %
Coverage of cases (0.95 level)          71.3036 %
Mean rel. region size (0.95 level)      15.7894 %
Total Number of Instances            18295     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,659    0,065    0,660      0,659    0,659      0,594    0,906     0,732     A
                 0,023    0,009    0,115      0,023    0,039      0,031    0,729     0,101     LGJKC
                 0,592    0,065    0,437      0,592    0,503      0,460    0,893     0,498     E
                 0,092    0,017    0,072      0,092    0,081      0,066    0,782     0,048     FV
                 0,647    0,037    0,425      0,647    0,513      0,500    0,920     0,492     I
                 0,328    0,024    0,528      0,328    0,404      0,380    0,837     0,424     O
                 0,221    0,038    0,388      0,221    0,282      0,237    0,851     0,337     SNRTY
                 0,915    0,122    0,815      0,915    0,862      0,777    0,966     0,958     0
                 0,022    0,006    0,110      0,022    0,037      0,035    0,772     0,104     BMP
                 0,391    0,043    0,121      0,391    0,185      0,197    0,860     0,108     DZ
                 0,485    0,017    0,400      0,485    0,439      0,426    0,923     0,491     U
                 0,203    0,034    0,205      0,203    0,204      0,170    0,838     0,169     SNRTXY
Weighted Avg.    0,594    0,071    0,568      0,594    0,570      0,516    0,899     0,627     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1936   33  234   59   62  121  110  127    5  146   18   89 |    a = A
   92   21  123   25  100   34  116  219    7   64   25   68 |    b = LGJKC
  145   25  851   19   98   23   50   52   18   56   32   69 |    c = E
   26    1   35   24   17   16   30   47    8   35    5   17 |    d = FV
   10   13  120    7  484    3   36   38    5    1   11   20 |    e = I
  248   19  122   25   30  451   41  137    7  142  100   55 |    f = O
  160   26  117   64   83   39  396  633   11  125   19  117 |    g = SNRTY
   91   14   80   61   61   33  128 6219   13   35   10   52 |    h = 0
   57   16   81   22  102   32   41   86   13   44   43   57 |    i = BMP
   42    0   32    7   11   20    7    8    7  107    9   24 |    j = DZ
   26    8   36    1    6   55   14    8    5   31  205   28 |    k = U
   99    6  115   20   84   27   52   53   19   95   35  154 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
65.85	1.12	7.96	2.01	2.11	4.12	3.74	4.32	0.17	4.97	0.61	3.03	 a = A
10.29	2.35	13.76	2.8	11.19	3.8	12.98	24.5	0.78	7.16	2.8	7.61	 b = LGJKC
10.08	1.74	59.18	1.32	6.82	1.6	3.48	3.62	1.25	3.89	2.23	4.8	 c = E
9.96	0.38	13.41	9.2	6.51	6.13	11.49	18.01	3.07	13.41	1.92	6.51	 d = FV
1.34	1.74	16.04	0.94	64.71	0.4	4.81	5.08	0.67	0.13	1.47	2.67	 e = I
18.01	1.38	8.86	1.82	2.18	32.75	2.98	9.95	0.51	10.31	7.26	3.99	 f = O
8.94	1.45	6.54	3.58	4.64	2.18	22.12	35.36	0.61	6.98	1.06	6.54	 g = SNRTY
1.34	0.21	1.18	0.9	0.9	0.49	1.88	91.5	0.19	0.51	0.15	0.77	 h = 0
9.6	2.69	13.64	3.7	17.17	5.39	6.9	14.48	2.19	7.41	7.24	9.6	 i = BMP
15.33	0.0	11.68	2.55	4.01	7.3	2.55	2.92	2.55	39.05	3.28	8.76	 j = DZ
6.15	1.89	8.51	0.24	1.42	13.0	3.31	1.89	1.18	7.33	48.46	6.62	 k = U
13.04	0.79	15.15	2.64	11.07	3.56	6.85	6.98	2.5	12.52	4.61	20.29	 l = SNRTXY
