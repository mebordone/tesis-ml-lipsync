Timestamp:2013-07-22-21:53:49
Inventario:I2
Intervalo:3ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I2-3ms-5gramas
Num Instances:  61131
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1797





=== Stratified cross-validation ===

Correctly Classified Instances       53262               87.1276 %
Incorrectly Classified Instances      7869               12.8724 %
Kappa statistic                          0.8375
K&B Relative Info Score            4902460.6267 %
K&B Information Score               140710.9703 bits      2.3018 bits/instance
Class complexity | order 0          175442.4274 bits      2.8699 bits/instance
Class complexity | scheme          2207554.2391 bits     36.1119 bits/instance
Complexity improvement     (Sf)    -2032111.8117 bits    -33.2419 bits/instance
Mean absolute error                      0.0379
Root mean squared error                  0.1302
Relative absolute error                 28.5508 %
Root relative squared error             50.5171 %
Coverage of cases (0.95 level)          96.6793 %
Mean rel. region size (0.95 level)      18.8437 %
Total Number of Instances            61131     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,924    0,021    0,892      0,924    0,907      0,890    0,978     0,951     A
                 0,676    0,012    0,739      0,676    0,706      0,692    0,928     0,730     LGJKC
                 0,895    0,012    0,864      0,895    0,879      0,869    0,977     0,925     E
                 0,657    0,003    0,771      0,657    0,710      0,708    0,936     0,710     FV
                 0,872    0,004    0,890      0,872    0,881      0,876    0,972     0,908     I
                 0,837    0,008    0,891      0,837    0,863      0,853    0,956     0,884     O
                 0,724    0,034    0,691      0,724    0,707      0,676    0,948     0,759     SNRTY
                 0,937    0,058    0,911      0,937    0,924      0,875    0,983     0,966     0
                 0,711    0,003    0,874      0,711    0,785      0,783    0,938     0,798     BMP
                 0,836    0,001    0,925      0,836    0,879      0,878    0,984     0,915     DZ
                 0,904    0,001    0,952      0,904    0,927      0,926    0,985     0,948     U
                 0,827    0,003    0,923      0,827    0,872      0,869    0,970     0,899     SNRTXY
Weighted Avg.    0,871    0,032    0,871      0,871    0,870      0,843    0,971     0,908     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  8854    92    63     9    17    34   190   264    19    11     5    27 |     a = A
   146  2002    60    19    21    31   313   324    16     0     9    22 |     b = LGJKC
    86    55  4134     7    26    29   106   114    25    14     4    21 |     c = E
    31    31    28   568    13    15   135    29     9     0     3     2 |     d = FV
    28    31    60     9  2077     8    66    87    10     1     1     4 |     e = I
   112    34    43    13    14  3734    98   370    16    12     2    15 |     f = O
   257   199   123    61    52    95  4203   724    48     7     8    31 |     g = SNRTY
   240   166   140    25    80   133   607 22239    41     6    13    32 |     h = 0
    81    34    47    13    23    30   162   154  1386     0    11     7 |     i = BMP
    24     4    24     3     2    38    25    16     2   756     1     9 |     j = DZ
    19    22    15     2     4    22    18    20     8     1  1253     2 |     k = U
    51    40    48     8     4    20   161    77     5     9     6  2056 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
92.37	0.96	0.66	0.09	0.18	0.35	1.98	2.75	0.2	0.11	0.05	0.28	 a = A
4.93	67.57	2.02	0.64	0.71	1.05	10.56	10.93	0.54	0.0	0.3	0.74	 b = LGJKC
1.86	1.19	89.46	0.15	0.56	0.63	2.29	2.47	0.54	0.3	0.09	0.45	 c = E
3.59	3.59	3.24	65.74	1.5	1.74	15.63	3.36	1.04	0.0	0.35	0.23	 d = FV
1.18	1.3	2.52	0.38	87.2	0.34	2.77	3.65	0.42	0.04	0.04	0.17	 e = I
2.51	0.76	0.96	0.29	0.31	83.67	2.2	8.29	0.36	0.27	0.04	0.34	 f = O
4.42	3.43	2.12	1.05	0.9	1.64	72.37	12.47	0.83	0.12	0.14	0.53	 g = SNRTY
1.01	0.7	0.59	0.11	0.34	0.56	2.56	93.75	0.17	0.03	0.05	0.13	 h = 0
4.16	1.75	2.41	0.67	1.18	1.54	8.32	7.91	71.15	0.0	0.56	0.36	 i = BMP
2.65	0.44	2.65	0.33	0.22	4.2	2.77	1.77	0.22	83.63	0.11	1.0	 j = DZ
1.37	1.59	1.08	0.14	0.29	1.59	1.3	1.44	0.58	0.07	90.4	0.14	 k = U
2.05	1.61	1.93	0.32	0.16	0.8	6.48	3.1	0.2	0.36	0.24	82.74	 l = SNRTXY
