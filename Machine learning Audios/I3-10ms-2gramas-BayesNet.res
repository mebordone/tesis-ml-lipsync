Timestamp:2013-08-30-05:05:00
Inventario:I3
Intervalo:10ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I3-10ms-2gramas
Num Instances:  18295
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(11): class 
0Pitch(Hz)(7): class 
0RawPitch(6): class 
0SmPitch(8): class 
0Melogram(st)(9): class 
0ZCross(10): class 
0F1(Hz)(14): class 
0F2(Hz)(12): class 
0F3(Hz)(9): class 
0F4(Hz)(12): class 
1Int(dB)(12): class 
1Pitch(Hz)(7): class 
1RawPitch(7): class 
1SmPitch(9): class 
1Melogram(st)(9): class 
1ZCross(12): class 
1F1(Hz)(12): class 
1F2(Hz)(13): class 
1F3(Hz)(10): class 
1F4(Hz)(8): class 
class(11): 
LogScore Bayes: -482407.170542193
LogScore BDeu: -491577.6815780584
LogScore MDL: -490849.5498564145
LogScore ENTROPY: -481246.17601512634
LogScore AIC: -483203.17601512634




=== Stratified cross-validation ===

Correctly Classified Instances       10884               59.4917 %
Incorrectly Classified Instances      7411               40.5083 %
Kappa statistic                          0.4852
K&B Relative Info Score             914614.594  %
K&B Information Score                24096.8256 bits      1.3171 bits/instance
Class complexity | order 0           48175.3419 bits      2.6333 bits/instance
Class complexity | scheme           123754.8283 bits      6.7644 bits/instance
Complexity improvement     (Sf)     -75579.4864 bits     -4.1312 bits/instance
Mean absolute error                      0.0751
Root mean squared error                  0.2475
Relative absolute error                 52.5771 %
Root relative squared error             92.6252 %
Coverage of cases (0.95 level)          69.4179 %
Mean rel. region size (0.95 level)      15.2029 %
Total Number of Instances            18295     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,646    0,066    0,651      0,646    0,648      0,581    0,905     0,728     A
                 0,593    0,068    0,428      0,593    0,497      0,454    0,890     0,502     E
                 0,168    0,041    0,484      0,168    0,249      0,200    0,804     0,423     CGJLNQSRT
                 0,092    0,013    0,095      0,092    0,094      0,081    0,788     0,057     FV
                 0,652    0,038    0,420      0,652    0,511      0,498    0,921     0,494     I
                 0,312    0,023    0,530      0,312    0,393      0,371    0,837     0,430     O
                 0,918    0,136    0,799      0,918    0,854      0,764    0,965     0,957     0
                 0,039    0,005    0,192      0,039    0,064      0,073    0,777     0,109     BMP
                 0,513    0,021    0,367      0,513    0,428      0,418    0,925     0,488     U
                 0,000    0,004    0,000      0,000    0,000      -0,001   0,927     0,003     DZ
                 0,504    0,068    0,098      0,504    0,164      0,198    0,874     0,110     D
Weighted Avg.    0,595    0,080    0,601      0,595    0,574      0,515    0,897     0,661     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1899  238  158   45   61  122  142    7   16    4  248 |    a = A
  147  853   89    8  110   20   57   19   40    3   92 |    b = E
  355  396  577   72  260   98 1025   32  121   47  460 |    c = CGJLNQSRT
   27   34   24   24   20   20   56    4    5    2   45 |    d = FV
   11  122   50    6  488    1   41    7    7    3   12 |    e = I
  254  132   70   17   31  430  139    8   95    5  196 |    f = O
   90   78  131   56   65   37 6239   13   14   13   61 |    g = 0
   62   78   62   14  108   27   88   23   61    2   69 |    h = BMP
   30   35   18    3    7   45   10    4  217    3   51 |    i = U
    0    0    0    0    0    0    8    0    0    0    0 |    j = DZ
   42   27   13    7   13   11    1    3   15    0  134 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
64.59	8.1	5.37	1.53	2.07	4.15	4.83	0.24	0.54	0.14	8.44	 a = A
10.22	59.32	6.19	0.56	7.65	1.39	3.96	1.32	2.78	0.21	6.4	 b = E
10.31	11.5	16.76	2.09	7.55	2.85	29.77	0.93	3.51	1.37	13.36	 c = CGJLNQSRT
10.34	13.03	9.2	9.2	7.66	7.66	21.46	1.53	1.92	0.77	17.24	 d = FV
1.47	16.31	6.68	0.8	65.24	0.13	5.48	0.94	0.94	0.4	1.6	 e = I
18.45	9.59	5.08	1.23	2.25	31.23	10.09	0.58	6.9	0.36	14.23	 f = O
1.32	1.15	1.93	0.82	0.96	0.54	91.79	0.19	0.21	0.19	0.9	 g = 0
10.44	13.13	10.44	2.36	18.18	4.55	14.81	3.87	10.27	0.34	11.62	 h = BMP
7.09	8.27	4.26	0.71	1.65	10.64	2.36	0.95	51.3	0.71	12.06	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
15.79	10.15	4.89	2.63	4.89	4.14	0.38	1.13	5.64	0.0	50.38	 k = D
