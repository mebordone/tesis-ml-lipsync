Timestamp:2014-12-07-11:10:58
Inventario:I3
Intervalo:20ms
Ngramas:4
Clasificador:NaiveBayes
Relation Name:  I3-20ms-4gramas
Num Instances:  9126
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Naive Bayes Classifier

                    Class
Attribute               A         E CGJLNQSRT        FV         I         O         0       BMP         U        DZ         D
                   (0.16)    (0.08)    (0.19)    (0.02)    (0.04)    (0.08)    (0.35)    (0.03)    (0.02)       (0)    (0.01)
==============================================================================================================================
0Int(dB)
  mean             -8.6039  -11.7218  -12.2656   -8.8371  -14.1716  -13.1727  -48.3445  -10.1929  -12.1512  -22.3746   -8.5828
  std. dev.        13.8877   12.7888    12.693   12.1009   11.3435   13.9303   16.8499   12.1121   13.5815    4.8213    7.8675
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008

0Pitch(Hz)
  mean            194.5491  160.6546  164.5152  181.9044  134.2538  156.7441   19.4311  179.4782  185.7432  102.2504  185.2231
  std. dev.        122.101  102.2528  101.2728  100.9774   96.8851  116.9519    59.894  103.6256  130.6923   83.5284   85.6403
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034

0RawPitch
  mean             184.365  148.5522  153.3207  170.1709  121.7318  148.9971   16.1107  175.1905  173.5625         0  175.2889
  std. dev.       124.2233  106.1316  106.2499  103.5921   96.3139  120.0464   54.3875  105.5177  129.6098    0.0348   91.5053
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088

0SmPitch
  mean            192.5256  155.5873  161.2907  177.1758  131.2192  153.6272   17.6151   179.977  182.8643   69.8256  185.2493
  std. dev.       122.8826  104.7161  103.2911  100.5075   96.9443  117.9631   56.4219  103.1028  131.0566   85.5195      85.7
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051

0Melogram(st)
  mean             46.3494   43.0353   44.0343   48.8131   38.5277     40.72    5.6263   47.4023    41.677   41.9743   50.5341
  std. dev.        22.3412   22.8173   21.9203    17.183   23.9713   24.6779   16.5837   19.9946    26.535   20.9897   14.9915
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353

0ZCross
  mean              5.7987    3.7875     3.401    4.3439    3.3025    3.2283    0.3546    3.5458    3.5496    0.5091    3.5024
  std. dev.         5.2067    3.6027    3.2096    3.5198    4.4292    3.6497    1.5379    3.2035    3.8879    0.6235    2.4249
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727

0F1(Hz)
  mean            548.2115  393.9778  476.5649   516.388  337.2878   387.934   69.4574  434.0106  424.8479  404.4873  492.0247
  std. dev.       340.6042  237.3761  237.5945  185.7463  247.3025  267.0661  193.2324  222.5728  365.8257   331.872   180.435
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127

0F2(Hz)
  mean           1291.1129 1373.2113 1447.4624 1567.5638 1314.2846 1075.1192  205.6432 1481.8017  955.3932  716.5182 1399.3205
  std. dev.       669.3906  794.2677  668.1076  551.4753  896.4634  722.9702  567.5507  698.0192  693.4125  585.2602  487.2677
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985

0F3(Hz)
  mean           2332.9922 2174.5401 2470.9942 2627.5782 2014.1352 2131.1022  351.4258 2459.9895 2126.7769 2210.5911 2644.2835
  std. dev.      1159.2763 1212.1208   1078.92  842.6607 1344.3758 1290.7244  944.4587 1073.6058 1291.5323  1805.819  816.7753
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065

0F4(Hz)
  mean           2700.5548 2634.0278 2833.7774 3085.9541 2430.7791 2461.9478  424.3306 2871.1033 2454.4832  1790.029 3097.9087
  std. dev.      1332.9429 1448.9271 1220.3955   967.976 1598.0911 1484.5906  1137.485 1226.0673  1445.419 1461.5645  925.0033
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301

1Int(dB)
  mean             -7.2233   -9.6448  -13.7269  -10.0976  -12.8075  -11.6424  -49.2205  -10.9675  -10.4698  -24.1283    -7.149
  std. dev.        12.4964   11.2836   12.6639   11.5475   10.4841   13.0523   15.8188   11.7268   12.0458    4.1431     6.509
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008

1Pitch(Hz)
  mean            201.6038  168.4113  153.7893  174.9356  145.3172  164.5924   15.8268  181.5295   199.501   67.2315  198.0853
  std. dev.       116.7827   97.9387  105.5336   105.317    93.238  113.2428   54.5523  103.9694   120.686   82.3435   81.2489
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034

1RawPitch
  mean            192.0371  160.8464  140.5365  150.5271  134.7046  155.7527   12.9483  174.3366  187.6775         0  190.1691
  std. dev.       119.1688  102.2888  109.8955  108.0909   92.9403  117.0548   48.8007  107.6802   120.986    0.0348   89.6323
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088

1SmPitch
  mean            199.8945  165.6273  149.0924  164.0951  143.4528  162.0426   14.3604  180.2617  195.9784   34.7897  197.5786
  std. dev.       117.6128  100.1622  107.6426  102.8422   93.1972  114.4287   51.0284  105.3606  122.4742   69.5795   82.3924
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051

1Melogram(st)
  mean             48.2092   45.4706   41.1088   47.4754   41.3427   43.3948    4.6661   47.4768   44.4291   31.3679   52.3324
  std. dev.        20.5746   20.8835   23.8889   18.4939   22.4649   23.1016   15.2463   19.8068    24.856   25.6122    12.276
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353

1ZCross
  mean               6.115    4.2003    3.1996    3.7167    3.1471    3.4785    0.2554    3.0435    3.4504    0.2545    3.7321
  std. dev.         5.0271    3.4667    3.4705    3.2864    4.1866    3.6204    1.3162    2.9836    3.5108    0.5091    2.2138
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727

1F1(Hz)
  mean            581.7148  408.4223   451.723  522.7777  325.1292  411.5688   58.2974  443.7803   424.862  280.3215  515.0288
  std. dev.       326.0741  216.3841  252.7133  201.8334  214.6223  260.2244  178.1576  220.7378  361.7064  343.6322  134.1908
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127

1F2(Hz)
  mean           1332.7377 1477.3725 1383.4476 1525.1731 1473.7397 1085.7909  173.7875 1499.8925  924.4511  472.0442 1505.9489
  std. dev.       619.6166  754.7082  723.4465  570.7008  898.7946  674.5578  526.5016  665.9892  644.9976  578.1561  311.6051
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985

1F3(Hz)
  mean           2424.4511 2285.1335 2354.9132 2573.9367 2142.5805 2224.7554  296.2301 2515.1665 2177.1877 1491.5753 2862.2543
  std. dev.      1080.1353 1133.2536  1179.468  901.2684 1286.7452 1238.7361  875.0734 1019.8055 1282.0773 1827.0139  398.7846
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065

1F4(Hz)
  mean           2808.6736 2766.3747 2698.5673 3029.4131 2588.6317 2567.4184  360.2866 2942.8455 2475.9112 1195.4814 3332.5443
  std. dev.      1242.9542 1350.6568 1337.1008 1028.6692 1525.8539 1421.7151 1061.6517 1160.3327 1409.4738 1464.1601  408.6818
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301

2Int(dB)
  mean              -5.641   -7.2226  -15.4984  -12.3449  -10.9862   -9.7163  -50.1381   -12.305   -8.0296   -25.761    -7.265
  std. dev.        10.8727    9.4909   11.9491   10.3558    9.3463   12.1824   14.5049   11.5328    9.9998    2.8196    6.0708
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008

2Pitch(Hz)
  mean            211.1547   183.146  136.4752  161.6772  163.4233  177.1418   12.2342  174.4724  221.0527   33.4327  198.6388
  std. dev.       109.0219   90.4096  110.1606  107.4647   84.1925  106.6856   48.3753  105.6613   98.5341   66.8654   83.6049
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034

2RawPitch
  mean            200.9312  176.8718  122.3359  134.4189  153.6209  168.1349   10.0558  163.2284  210.2912         0  193.1658
  std. dev.       113.0094   96.4397  112.0284  106.6169   86.0466   111.735   42.5716  110.9161  103.8231    0.0348   90.7206
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088

2SmPitch
  mean            209.2749  181.1568  131.5679   150.738  161.2553  174.8902   10.6967  171.9345  219.9482         0   198.416
  std. dev.       110.3154   92.1927  111.5204  103.6325   84.9278  108.2193   43.9063  107.9136    99.473    0.0342   84.1478
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051

2Melogram(st)
  mean             50.6224   49.1776   36.6719   45.2162   45.9084   46.5218    3.7056   45.7201   50.6374   20.8602   52.5572
  std. dev.        17.7748   17.1261   25.8767   20.2304   18.9795   20.6378    13.742    21.266    19.473   25.5485   11.6326
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353    0.0353

2ZCross
  mean              6.4973    4.6951    2.9292    2.7115    3.0678    3.8039    0.1472    2.4688    3.4387         0    3.6746
  std. dev.         4.7305    3.4013    3.9332    2.5858     3.289    3.4966    0.8716    2.5057    3.0618    0.2121     2.155
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727

2F1(Hz)
  mean            632.5964  435.3843  402.9825  487.8092  340.7633  450.1528   45.7835  423.0991  462.9492  144.7747  507.6396
  std. dev.       296.8673  182.9634  270.5934    232.81  174.3406  244.2194   157.842  227.7907  332.7223  289.5494  140.7686
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127

2F2(Hz)
  mean           1400.8405 1633.7862  1258.076 1414.8511 1740.6907 1123.9385   138.821 1449.4723  1000.033  234.4156 1494.6889
  std. dev.       532.4998  662.2232  797.3903  640.3559  805.0767  603.6465  474.0493  686.1573   546.594  468.8312  292.6037
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985

2F3(Hz)
  mean           2566.7052 2466.1143 2131.4955 2406.0546 2427.5868 2381.9061  236.9164 2437.2044  2460.271  754.6445 2867.9147
  std. dev.       934.1062  983.2863 1312.6473 1054.0979 1119.2236 1133.1835  790.3143 1070.9844 1089.2339  1509.289  389.3036
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065

2F4(Hz)
  mean           2970.9678  2968.206 2448.7805 2858.1926 2906.6724 2739.0875  290.1716  2883.526  2778.374  598.0497 3355.4843
  std. dev.      1072.0689 1156.9806 1496.5293 1217.3342 1305.1093 1292.2218  965.7542 1232.2639 1167.5154 1196.0995   399.068
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301

3Int(dB)
  mean             -4.7838   -5.6493  -16.3879  -13.9655   -9.5022   -8.6644  -50.8607  -12.4527   -5.9413  -28.5024   -7.9076
  std. dev.          9.917    8.2505   10.4461    8.9902    8.6317   12.1183   13.3693   10.4872    8.0011    3.0818    6.2206
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008    0.1008

3Pitch(Hz)
  mean            215.4717  192.4898  131.5587  152.2312  174.3019  180.2959    8.6414  173.7208  232.3523         0  197.3055
  std. dev.       105.3209    85.636  110.6314   105.769   75.0715  103.4062   41.2262  104.0157   82.3117    0.0339   85.1182
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034    0.2034

3RawPitch
  mean            205.1279  188.8553  114.8527  123.0765  166.4766  174.0908    6.9092  156.9007  222.5693         0  192.4154
  std. dev.       110.0217   89.4789  111.3876  101.8456   78.7839  108.0935   35.5094  112.4322   89.8429    0.0348   92.3885
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088    0.2088

3SmPitch
  mean            213.4208  191.6241  125.4343  141.6009  171.8543   178.962    7.4185  169.8194  232.3227         0  196.7627
  std. dev.       106.8674   86.4181   112.086  100.3995   76.5988  104.3249   36.4788  107.6346   82.2366    0.0342   86.2961
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051    0.2051

3Melogram(st)
  mean             52.0827   51.3162   34.7221   42.7509   49.4307   47.7311     2.748   45.7719   54.9086   10.4257   52.3538
  std. dev.        15.5643   13.9659   26.6004   21.9334   14.5646   19.2227   12.0067   21.2519   12.8002   20.8513   11.7579
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.0352    0.0352    0.0352    0.0352    0.0352    0.0352    0.0352    0.0352    0.0352    0.0352    0.0352

3ZCross
  mean              6.7146    4.8248    2.7904    2.0843    3.1868    3.9665    0.0854    2.2092     3.573         0    3.6172
  std. dev.          4.535    3.1533    4.2824    2.0439    2.9527    3.0096    0.6292     2.095    2.8222    0.2121    2.1729
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727    1.2727

3F1(Hz)
  mean            680.6071  468.1216  353.3559  433.0067  362.9833  487.9887   33.6522  402.1692  498.1345         0  513.8878
  std. dev.       260.5161  135.4424  274.4304  254.6457  139.6622  219.6408  135.0197  226.7452  299.1257    0.0854  150.3522
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127    0.5127

3F2(Hz)
  mean           1463.4676 1802.3085 1136.6073 1302.5905 1971.4305 1165.7924  104.6514 1402.7174 1080.7158         0 1476.9797
  std. dev.       432.0523  487.8366  837.1102  701.6256  644.7659  525.3227   415.952  694.7863  431.8771    0.1164  305.2219
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985    0.6985

3F3(Hz)
  mean           2691.8093 2662.4498 1930.6527 2220.2677 2672.9489 2534.4259  178.5267 2362.4099 2714.6841         0 2860.5062
  std. dev.       762.9907   729.876 1393.9242 1179.8092  890.7818  989.9708  694.1704 1105.7815  768.9985    0.1677  458.2327
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065    1.0065

3F4(Hz)
  mean            3114.979 3199.5596 2217.4465 2671.4019 3176.0491 2913.9175  219.9788 2816.8308  3074.217         0 3346.2293
  std. dev.       871.0219  841.1377 1591.9677 1380.3083 1017.7226 1121.3472  853.2071 1281.9538  772.1011    0.1717  488.4351
  weight sum          1505       746      1751       138       385       712      3234       299       218         5       133
  precision         1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301    1.0301





=== Stratified cross-validation ===

Correctly Classified Instances        3568               39.0971 %
Incorrectly Classified Instances      5558               60.9029 %
Kappa statistic                          0.2889
K&B Relative Info Score             235536.1662 %
K&B Information Score                 6291.955  bits      0.6895 bits/instance
Class complexity | order 0           24347.7647 bits      2.668  bits/instance
Class complexity | scheme           221900.2727 bits     24.3152 bits/instance
Complexity improvement     (Sf)    -197552.508  bits    -21.6472 bits/instance
Mean absolute error                      0.1106
Root mean squared error                  0.32  
Relative absolute error                 76.6382 %
Root relative squared error            119.1159 %
Coverage of cases (0.95 level)          45.7813 %
Mean rel. region size (0.95 level)      12.2965 %
Total Number of Instances             9126     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,288    0,049    0,536      0,288    0,375      0,312    0,810     0,473     A
                 0,083    0,023    0,241      0,083    0,124      0,099    0,791     0,208     E
                 0,223    0,053    0,498      0,223    0,308      0,238    0,712     0,399     CGJLNQSRT
                 0,058    0,019    0,044      0,058    0,050      0,034    0,729     0,034     FV
                 0,255    0,036    0,239      0,255    0,247      0,212    0,837     0,233     I
                 0,034    0,014    0,173      0,034    0,056      0,044    0,672     0,139     O
                 0,748    0,033    0,926      0,748    0,828      0,757    0,953     0,918     0
                 0,000    0,003    0,000      0,000    0,000      -0,011   0,721     0,059     BMP
                 0,147    0,014    0,209      0,147    0,173      0,158    0,838     0,142     U
                 0,600    0,083    0,004      0,600    0,008      0,044    0,805     0,006     DZ
                 0,737    0,321    0,033      0,737    0,063      0,106    0,808     0,045     D
Weighted Avg.    0,391    0,040    0,562      0,391    0,445      0,391    0,827     0,524     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
  434   58   69   24   57   63   26    1   47   28  698 |    a = A
   78   62   42    5   77   21    7    4   17   14  419 |    b = E
  127   42  390   79   49   16  106   15   13  140  774 |    c = CGJLNQSRT
   12    2   21    8    5    1    5    0    0    4   80 |    d = FV
   22   51   34    4   98    3   13    1    2    5  152 |    e = I
   64   23   43   16   56   24   15    0   32   53  386 |    f = O
    8    3  129   32   14    0 2419    4    2  507  116 |    g = 0
   21    5   36   10   26    2   15    0    6    6  172 |    h = BMP
   29    3   13    2   27    8    4    5   32    1   94 |    i = U
    0    0    1    0    0    0    1    0    0    3    0 |    j = DZ
   14    8    5    3    1    1    1    0    2    0   98 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
28.84	3.85	4.58	1.59	3.79	4.19	1.73	0.07	3.12	1.86	46.38	 a = A
10.46	8.31	5.63	0.67	10.32	2.82	0.94	0.54	2.28	1.88	56.17	 b = E
7.25	2.4	22.27	4.51	2.8	0.91	6.05	0.86	0.74	8.0	44.2	 c = CGJLNQSRT
8.7	1.45	15.22	5.8	3.62	0.72	3.62	0.0	0.0	2.9	57.97	 d = FV
5.71	13.25	8.83	1.04	25.45	0.78	3.38	0.26	0.52	1.3	39.48	 e = I
8.99	3.23	6.04	2.25	7.87	3.37	2.11	0.0	4.49	7.44	54.21	 f = O
0.25	0.09	3.99	0.99	0.43	0.0	74.8	0.12	0.06	15.68	3.59	 g = 0
7.02	1.67	12.04	3.34	8.7	0.67	5.02	0.0	2.01	2.01	57.53	 h = BMP
13.3	1.38	5.96	0.92	12.39	3.67	1.83	2.29	14.68	0.46	43.12	 i = U
0.0	0.0	20.0	0.0	0.0	0.0	20.0	0.0	0.0	60.0	0.0	 j = DZ
10.53	6.02	3.76	2.26	0.75	0.75	0.75	0.0	1.5	0.0	73.68	 k = D
