Timestamp:2013-07-22-22:30:16
Inventario:I3
Intervalo:10ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I3-10ms-3gramas
Num Instances:  18294
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.2807





=== Stratified cross-validation ===

Correctly Classified Instances       14196               77.5992 %
Incorrectly Classified Instances      4098               22.4008 %
Kappa statistic                          0.7129
K&B Relative Info Score            1240766.0543 %
K&B Information Score                32688.4707 bits      1.7868 bits/instance
Class complexity | order 0           48173.9135 bits      2.6333 bits/instance
Class complexity | scheme           978053.4695 bits     53.4631 bits/instance
Complexity improvement     (Sf)    -929879.556  bits    -50.8298 bits/instance
Mean absolute error                      0.0599
Root mean squared error                  0.1715
Relative absolute error                 41.919  %
Root relative squared error             64.1736 %
Coverage of cases (0.95 level)          95.0804 %
Mean rel. region size (0.95 level)      23.7957 %
Total Number of Instances            18294     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,833    0,046    0,775      0,833    0,803      0,764    0,953     0,871     A
                 0,725    0,030    0,671      0,725    0,697      0,670    0,945     0,730     E
                 0,717    0,104    0,616      0,717    0,663      0,580    0,905     0,693     CGJLNQSRT
                 0,276    0,002    0,655      0,276    0,388      0,420    0,862     0,315     FV
                 0,686    0,011    0,730      0,686    0,707      0,695    0,946     0,731     I
                 0,655    0,021    0,722      0,655    0,687      0,663    0,918     0,715     O
                 0,902    0,050    0,914      0,902    0,908      0,854    0,971     0,951     0
                 0,423    0,006    0,713      0,423    0,531      0,538    0,879     0,486     BMP
                 0,648    0,003    0,854      0,648    0,737      0,738    0,949     0,760     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,561     0,018     DZ
                 0,357    0,002    0,754      0,357    0,485      0,514    0,926     0,509     D
Weighted Avg.    0,776    0,050    0,780      0,776    0,773      0,728    0,943     0,810     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 2448   76  243    5   16   60   74   10    3    1    4 |    a = A
   97 1042  200    5   30   20   22   15    2    0    5 |    b = E
  296  174 2469   11   62   76  307   22   17    0    9 |    c = CGJLNQSRT
   25   19  109   72    6   16    8    4    2    0    0 |    d = FV
   21   78   96    4  513    2   19   11    4    0    0 |    e = I
  104   53  169    4    8  902  106   16    6    0    9 |    f = O
   79   50  431    3   30   57 6130   11    4    0    1 |    g = 0
   46   27  153    2   32   38   37  251    5    0    3 |    h = BMP
   10   14   66    3    2   40    6    8  274    0    0 |    i = U
    1    0    7    0    0    0    0    0    0    0    0 |    j = DZ
   32   20   66    1    4   39    1    4    4    0   95 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
83.27	2.59	8.27	0.17	0.54	2.04	2.52	0.34	0.1	0.03	0.14	 a = A
6.75	72.46	13.91	0.35	2.09	1.39	1.53	1.04	0.14	0.0	0.35	 b = E
8.6	5.05	71.71	0.32	1.8	2.21	8.92	0.64	0.49	0.0	0.26	 c = CGJLNQSRT
9.58	7.28	41.76	27.59	2.3	6.13	3.07	1.53	0.77	0.0	0.0	 d = FV
2.81	10.43	12.83	0.53	68.58	0.27	2.54	1.47	0.53	0.0	0.0	 e = I
7.55	3.85	12.27	0.29	0.58	65.5	7.7	1.16	0.44	0.0	0.65	 f = O
1.16	0.74	6.34	0.04	0.44	0.84	90.2	0.16	0.06	0.0	0.01	 g = 0
7.74	4.55	25.76	0.34	5.39	6.4	6.23	42.26	0.84	0.0	0.51	 h = BMP
2.36	3.31	15.6	0.71	0.47	9.46	1.42	1.89	64.78	0.0	0.0	 i = U
12.5	0.0	87.5	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 j = DZ
12.03	7.52	24.81	0.38	1.5	14.66	0.38	1.5	1.5	0.0	35.71	 k = D
