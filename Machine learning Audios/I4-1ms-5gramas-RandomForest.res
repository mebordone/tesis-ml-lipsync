Timestamp:2013-07-22-22:46:19
Inventario:I4
Intervalo:1ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I4-1ms-5gramas
Num Instances:  183400
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  49 4F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1184





=== Stratified cross-validation ===

Correctly Classified Instances      166991               91.0529 %
Incorrectly Classified Instances     16409                8.9471 %
Kappa statistic                          0.883 
K&B Relative Info Score            15795555.9159 %
K&B Information Score               401797.4733 bits      2.1908 bits/instance
Class complexity | order 0          466502.5586 bits      2.5436 bits/instance
Class complexity | scheme          5986052.5609 bits     32.6393 bits/instance
Complexity improvement     (Sf)    -5519550.0022 bits    -30.0957 bits/instance
Mean absolute error                      0.0277
Root mean squared error                  0.1166
Relative absolute error                 17.983  %
Root relative squared error             42.0169 %
Coverage of cases (0.95 level)          96.9776 %
Mean rel. region size (0.95 level)      16.5149 %
Total Number of Instances           183400     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,915    0,000    0,943      0,915    0,929      0,928    0,972     0,937     C
                 0,943    0,005    0,944      0,943    0,943      0,939    0,984     0,964     E
                 0,744    0,002    0,814      0,744    0,778      0,775    0,931     0,793     FV
                 0,943    0,013    0,947      0,943    0,945      0,932    0,983     0,970     AI
                 0,865    0,034    0,824      0,865    0,844      0,814    0,972     0,912     CDGKNRSYZ
                 0,878    0,004    0,945      0,878    0,910      0,904    0,960     0,915     O
                 0,948    0,053    0,921      0,948    0,935      0,891    0,986     0,970     0
                 0,659    0,005    0,820      0,659    0,731      0,726    0,916     0,741     LT
                 0,956    0,000    0,984      0,956    0,970      0,969    0,993     0,982     U
                 0,788    0,003    0,910      0,788    0,845      0,842    0,946     0,852     MBP
Weighted Avg.    0,911    0,029    0,911      0,911    0,910      0,885    0,976     0,942     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   991    11     1    16    28     4    25     3     1     3 |     a = C
     7 12959    17    85   311    27   241    54     3    36 |     b = E
     2    30  1906    42   428    19   104    20     0    10 |     c = FV
    11   105    42 33610   728    50   890   131    11    62 |     d = AI
    16   242   205   603 24610   146  2166   338    22   111 |     e = CDGKNRSYZ
     4    21    32   122   352 11622  1011    37     0    33 |     f = O
    15   216    78   556  2093   333 68392   284    10   136 |     g = 0
     3    80    23   243   853    55   940  4362     6    54 |     h = LT
     2     9     3    30    57    13    40    16  3957    10 |     i = U
     0    58    34   166   423    31   431    77    12  4582 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
91.51	1.02	0.09	1.48	2.59	0.37	2.31	0.28	0.09	0.28	 a = C
0.05	94.32	0.12	0.62	2.26	0.2	1.75	0.39	0.02	0.26	 b = E
0.08	1.17	74.42	1.64	16.71	0.74	4.06	0.78	0.0	0.39	 c = FV
0.03	0.29	0.12	94.3	2.04	0.14	2.5	0.37	0.03	0.17	 d = AI
0.06	0.85	0.72	2.12	86.48	0.51	7.61	1.19	0.08	0.39	 e = CDGKNRSYZ
0.03	0.16	0.24	0.92	2.66	87.82	7.64	0.28	0.0	0.25	 f = O
0.02	0.3	0.11	0.77	2.9	0.46	94.84	0.39	0.01	0.19	 g = 0
0.05	1.21	0.35	3.67	12.89	0.83	14.2	65.9	0.09	0.82	 h = LT
0.05	0.22	0.07	0.73	1.38	0.31	0.97	0.39	95.65	0.24	 i = U
0.0	1.0	0.58	2.86	7.28	0.53	7.41	1.32	0.21	78.81	 j = MBP
