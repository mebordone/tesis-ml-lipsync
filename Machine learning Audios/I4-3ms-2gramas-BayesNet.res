Timestamp:2013-08-30-05:24:31
Inventario:I4
Intervalo:3ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I4-3ms-2gramas
Num Instances:  61134
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(20): class 
0Pitch(Hz)(33): class 
0RawPitch(21): class 
0SmPitch(24): class 
0Melogram(st)(29): class 
0ZCross(15): class 
0F1(Hz)(187): class 
0F2(Hz)(156): class 
0F3(Hz)(274): class 
0F4(Hz)(275): class 
1Int(dB)(20): class 
1Pitch(Hz)(30): class 
1RawPitch(22): class 
1SmPitch(23): class 
1Melogram(st)(30): class 
1ZCross(15): class 
1F1(Hz)(194): class 
1F2(Hz)(160): class 
1F3(Hz)(233): class 
1F4(Hz)(302): class 
class(10): 
LogScore Bayes: -2511351.235364391
LogScore BDeu: -2687626.341825722
LogScore MDL: -2660465.953486326
LogScore ENTROPY: -2547838.648185885
LogScore AIC: -2568277.648185885




=== Stratified cross-validation ===

Correctly Classified Instances       38425               62.8537 %
Incorrectly Classified Instances     22709               37.1463 %
Kappa statistic                          0.5146
K&B Relative Info Score            3225910.3512 %
K&B Information Score                82385.3966 bits      1.3476 bits/instance
Class complexity | order 0          156107.6099 bits      2.5535 bits/instance
Class complexity | scheme           414299.1992 bits      6.7769 bits/instance
Complexity improvement     (Sf)    -258191.5893 bits     -4.2234 bits/instance
Mean absolute error                      0.076 
Root mean squared error                  0.2532
Relative absolute error                 49.2146 %
Root relative squared error             91.11   %
Coverage of cases (0.95 level)          71.2533 %
Mean rel. region size (0.95 level)      14.7119 %
Total Number of Instances            61134     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,628    0,018    0,176      0,628    0,275      0,326    0,927     0,312     C
                 0,684    0,084    0,399      0,684    0,504      0,472    0,904     0,544     E
                 0,278    0,011    0,263      0,278    0,270      0,260    0,854     0,237     FV
                 0,637    0,070    0,688      0,637    0,661      0,583    0,899     0,741     AI
                 0,173    0,035    0,478      0,173    0,254      0,217    0,831     0,428     CDGKNRSYZ
                 0,505    0,033    0,545      0,505    0,524      0,488    0,872     0,547     O
                 0,901    0,157    0,784      0,901    0,839      0,729    0,959     0,953     0
                 0,124    0,011    0,297      0,124    0,175      0,173    0,814     0,167     LT
                 0,705    0,024    0,406      0,705    0,515      0,521    0,952     0,614     U
                 0,331    0,019    0,367      0,331    0,348      0,328    0,837     0,313     MBP
Weighted Avg.    0,629    0,091    0,620      0,629    0,606      0,537    0,906     0,698     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   228    63     0    21    11     8    26     5     1     0 |     a = C
   153  3159    76   422   216   110   182    96    95   112 |     b = E
    13   140   240    79    39    57   216    10    30    40 |     c = FV
   300  1598   103  7617   513   501   663   134   244   294 |     d = AI
   281  1363   205  1293  1660   556  3226   185   460   347 |     e = CDGKNRSYZ
   141   412    24   641   157  2252   488    40   252    56 |     f = O
    50   477   151   556   502   287 21373   106    94   129 |     g = 0
    49   356    55   192   237   166   695   275    93   103 |     h = LT
    22    80    17    64    30   116    24    27   977    29 |     i = U
    59   264    40   186   107    80   359    48   161   644 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
62.81	17.36	0.0	5.79	3.03	2.2	7.16	1.38	0.28	0.0	 a = C
3.31	68.36	1.64	9.13	4.67	2.38	3.94	2.08	2.06	2.42	 b = E
1.5	16.2	27.78	9.14	4.51	6.6	25.0	1.16	3.47	4.63	 c = FV
2.51	13.35	0.86	63.65	4.29	4.19	5.54	1.12	2.04	2.46	 d = AI
2.93	14.23	2.14	13.5	17.34	5.81	33.69	1.93	4.8	3.62	 e = CDGKNRSYZ
3.16	9.23	0.54	14.36	3.52	50.46	10.93	0.9	5.65	1.25	 f = O
0.21	2.01	0.64	2.34	2.12	1.21	90.09	0.45	0.4	0.54	 g = 0
2.21	16.03	2.48	8.64	10.67	7.47	31.29	12.38	4.19	4.64	 h = LT
1.59	5.77	1.23	4.62	2.16	8.37	1.73	1.95	70.49	2.09	 i = U
3.03	13.55	2.05	9.55	5.49	4.11	18.43	2.46	8.26	33.06	 j = MBP
