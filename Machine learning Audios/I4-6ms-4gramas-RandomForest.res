Timestamp:2013-07-23-03:06:01
Inventario:I4
Intervalo:6ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I4-6ms-4gramas
Num Instances:  30450
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.2353





=== Stratified cross-validation ===

Correctly Classified Instances       25062               82.3054 %
Incorrectly Classified Instances      5388               17.6946 %
Kappa statistic                          0.7704
K&B Relative Info Score            2194238.5419 %
K&B Information Score                56337.5913 bits      1.8502 bits/instance
Class complexity | order 0           78166.4262 bits      2.567  bits/instance
Class complexity | scheme          1301502.7223 bits     42.7423 bits/instance
Complexity improvement     (Sf)    -1223336.2962 bits    -40.1752 bits/instance
Mean absolute error                      0.0579
Root mean squared error                  0.1642
Relative absolute error                 37.2815 %
Root relative squared error             58.9414 %
Coverage of cases (0.95 level)          96.0755 %
Mean rel. region size (0.95 level)      25.0798 %
Total Number of Instances            30450     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,751    0,001    0,874      0,751    0,808      0,809    0,960     0,798     C
                 0,813    0,024    0,742      0,813    0,776      0,757    0,964     0,832     E
                 0,453    0,003    0,666      0,453    0,539      0,544    0,916     0,551     FV
                 0,868    0,049    0,814      0,868    0,840      0,800    0,963     0,907     AI
                 0,769    0,062    0,699      0,769    0,733      0,681    0,939     0,784     CDGKNRSYZ
                 0,738    0,012    0,830      0,738    0,781      0,766    0,941     0,808     O
                 0,917    0,057    0,908      0,917    0,912      0,858    0,973     0,955     0
                 0,416    0,008    0,676      0,416    0,515      0,517    0,884     0,516     LT
                 0,798    0,002    0,913      0,798    0,851      0,850    0,970     0,887     U
                 0,579    0,004    0,817      0,579    0,677      0,679    0,922     0,681     MBP
Weighted Avg.    0,823    0,044    0,823      0,823    0,820      0,781    0,957     0,865     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   139    13     1    18     5     2     5     1     1     0 |     a = C
     4  1908     6   158   164    19    49    19     5    15 |     b = E
     1    36   195    45   110    10    20     7     1     5 |     c = FV
     2   205    15  5236   278    57   168    44     6    20 |     d = AI
     5   162    34   329  3694    90   381    66     8    33 |     e = CDGKNRSYZ
     1    58     5   133   167  1661   177    20     8    21 |     f = O
     4    96    22   230   467    82 10640    43    10    14 |     g = 0
     0    43     6   145   232    30   181   466     4    12 |     h = LT
     1    21     2    30    37    26    11     6   556     7 |     i = U
     2    30     7   110   130    25    82    17    10   567 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
75.14	7.03	0.54	9.73	2.7	1.08	2.7	0.54	0.54	0.0	 a = C
0.17	81.3	0.26	6.73	6.99	0.81	2.09	0.81	0.21	0.64	 b = E
0.23	8.37	45.35	10.47	25.58	2.33	4.65	1.63	0.23	1.16	 c = FV
0.03	3.4	0.25	86.82	4.61	0.95	2.79	0.73	0.1	0.33	 d = AI
0.1	3.37	0.71	6.85	76.93	1.87	7.93	1.37	0.17	0.69	 e = CDGKNRSYZ
0.04	2.58	0.22	5.91	7.42	73.79	7.86	0.89	0.36	0.93	 f = O
0.03	0.83	0.19	1.98	4.02	0.71	91.66	0.37	0.09	0.12	 g = 0
0.0	3.84	0.54	12.96	20.73	2.68	16.18	41.64	0.36	1.07	 h = LT
0.14	3.01	0.29	4.3	5.31	3.73	1.58	0.86	79.77	1.0	 i = U
0.2	3.06	0.71	11.22	13.27	2.55	8.37	1.73	1.02	57.86	 j = MBP
