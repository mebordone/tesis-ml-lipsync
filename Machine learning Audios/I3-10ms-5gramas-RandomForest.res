Timestamp:2013-07-22-22:30:59
Inventario:I3
Intervalo:10ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I3-10ms-5gramas
Num Instances:  18292
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  43 4RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  44 4SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  45 4Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  47 4F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  49 4F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  50 4F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.2665





=== Stratified cross-validation ===

Correctly Classified Instances       14530               79.4336 %
Incorrectly Classified Instances      3762               20.5664 %
Kappa statistic                          0.7359
K&B Relative Info Score            1262419.7891 %
K&B Information Score                33259.4903 bits      1.8183 bits/instance
Class complexity | order 0           48171.0561 bits      2.6334 bits/instance
Class complexity | scheme           738783.8454 bits     40.3884 bits/instance
Complexity improvement     (Sf)    -690612.7893 bits    -37.7549 bits/instance
Mean absolute error                      0.0592
Root mean squared error                  0.1667
Relative absolute error                 41.4374 %
Root relative squared error             62.4002 %
Coverage of cases (0.95 level)          96.3044 %
Mean rel. region size (0.95 level)      24.4389 %
Total Number of Instances            18292     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,845    0,043    0,790      0,845    0,817      0,781    0,960     0,882     A
                 0,740    0,029    0,686      0,740    0,712      0,687    0,950     0,747     E
                 0,764    0,097    0,646      0,764    0,700      0,627    0,922     0,740     CGJLNQSRT
                 0,222    0,002    0,624      0,222    0,328      0,367    0,892     0,338     FV
                 0,689    0,009    0,770      0,689    0,727      0,717    0,959     0,752     I
                 0,649    0,017    0,752      0,649    0,697      0,676    0,930     0,720     O
                 0,918    0,046    0,921      0,918    0,919      0,872    0,976     0,959     0
                 0,451    0,004    0,779      0,451    0,571      0,583    0,902     0,571     BMP
                 0,667    0,003    0,844      0,667    0,745      0,745    0,954     0,778     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,749     0,083     DZ
                 0,383    0,002    0,779      0,383    0,514      0,542    0,950     0,554     D
Weighted Avg.    0,794    0,047    0,798      0,794    0,791      0,751    0,952     0,830     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 2483   80  240    3   14   43   66    6    4    0    1 |    a = A
   86 1064  189    1   32   24   23    6    3    1    9 |    b = E
  243  147 2630   11   42   69  262   23    9    1    6 |    c = CGJLNQSRT
   19   22  125   58    7   13    9    5    3    0    0 |    d = FV
   16   77   95    5  515    3   24   10    3    0    0 |    e = I
  127   44  171    3    7  894   95   12   14    0   10 |    f = O
   71   32  364    8   21   49 6234    7    6    0    2 |    g = 0
   48   38  137    4   24   23   42  268    9    0    1 |    h = BMP
   20   23   51    0    4   31    7    5  282    0    0 |    i = U
    0    1    5    0    0    0    2    0    0    0    0 |    j = DZ
   29   23   64    0    3   40    2    2    1    0  102 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
84.46	2.72	8.16	0.1	0.48	1.46	2.24	0.2	0.14	0.0	0.03	 a = A
5.98	73.99	13.14	0.07	2.23	1.67	1.6	0.42	0.21	0.07	0.63	 b = E
7.06	4.27	76.39	0.32	1.22	2.0	7.61	0.67	0.26	0.03	0.17	 c = CGJLNQSRT
7.28	8.43	47.89	22.22	2.68	4.98	3.45	1.92	1.15	0.0	0.0	 d = FV
2.14	10.29	12.7	0.67	68.85	0.4	3.21	1.34	0.4	0.0	0.0	 e = I
9.22	3.2	12.42	0.22	0.51	64.92	6.9	0.87	1.02	0.0	0.73	 f = O
1.05	0.47	5.36	0.12	0.31	0.72	91.76	0.1	0.09	0.0	0.03	 g = 0
8.08	6.4	23.06	0.67	4.04	3.87	7.07	45.12	1.52	0.0	0.17	 h = BMP
4.73	5.44	12.06	0.0	0.95	7.33	1.65	1.18	66.67	0.0	0.0	 i = U
0.0	12.5	62.5	0.0	0.0	0.0	25.0	0.0	0.0	0.0	0.0	 j = DZ
10.9	8.65	24.06	0.0	1.13	15.04	0.75	0.75	0.38	0.0	38.35	 k = D
