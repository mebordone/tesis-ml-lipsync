Timestamp:2013-07-22-21:58:44
Inventario:I2
Intervalo:20ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I2-20ms-3gramas
Num Instances:  9127
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3831





=== Stratified cross-validation ===

Correctly Classified Instances        6226               68.2152 %
Incorrectly Classified Instances      2901               31.7848 %
Kappa statistic                          0.6066
K&B Relative Info Score             526611.9934 %
K&B Information Score                15530.8086 bits      1.7016 bits/instance
Class complexity | order 0           26899.5449 bits      2.9472 bits/instance
Class complexity | scheme           851313.1589 bits     93.2741 bits/instance
Complexity improvement     (Sf)    -824413.614  bits    -90.3269 bits/instance
Mean absolute error                      0.0704
Root mean squared error                  0.1896
Relative absolute error                 51.7158 %
Root relative squared error             72.7073 %
Coverage of cases (0.95 level)          91.3991 %
Mean rel. region size (0.95 level)      27.2114 %
Total Number of Instances             9127     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,807    0,087    0,647      0,807    0,718      0,661    0,930     0,806     A
                 0,258    0,027    0,332      0,258    0,290      0,261    0,792     0,233     LGJKC
                 0,651    0,049    0,544      0,651    0,593      0,556    0,914     0,581     E
                 0,072    0,005    0,189      0,072    0,105      0,109    0,725     0,078     FV
                 0,584    0,019    0,571      0,584    0,578      0,559    0,909     0,560     I
                 0,546    0,038    0,552      0,546    0,549      0,511    0,894     0,559     O
                 0,525    0,060    0,492      0,525    0,508      0,451    0,858     0,480     SNRTY
                 0,919    0,052    0,906      0,919    0,913      0,864    0,975     0,954     0
                 0,231    0,009    0,473      0,231    0,310      0,315    0,794     0,245     BMP
                 0,138    0,003    0,432      0,138    0,209      0,238    0,780     0,158     DZ
                 0,463    0,004    0,732      0,463    0,567      0,575    0,907     0,578     U
                 0,372    0,014    0,533      0,372    0,438      0,425    0,872     0,388     SNRTXY
Weighted Avg.    0,682    0,049    0,670      0,682    0,670      0,629    0,914     0,687     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1214   38   66    2   10   55   55   45    7    0    1   12 |    a = A
   81  116   44    5   17   28   74   54    7    1    5   18 |    b = LGJKC
   92   20  486    3   38   26   44    9    8    1    5   14 |    c = E
   26   11   21   10    3   13   41    3    6    0    1    3 |    d = FV
   23   18   66    3  225    3   21   13    4    1    1    7 |    e = I
  117   21   39    4    7  389   32   54   14    9    9   17 |    f = O
  122   45   56   10   28   38  480   98   14    3    2   18 |    g = SNRTY
   53   30   13    4   11   27  109 2973    4    1    2    8 |    h = 0
   42   14   25    7   31   29   39   22   69    2    7   12 |    i = BMP
   29    7   15    1    4   32   14    1    1   19    2   13 |    j = DZ
   26    6   19    1    7   38    9    2    5    0  101    4 |    k = U
   52   23   44    3   13   27   58    7    7    7    2  144 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
80.66	2.52	4.39	0.13	0.66	3.65	3.65	2.99	0.47	0.0	0.07	0.8	 a = A
18.0	25.78	9.78	1.11	3.78	6.22	16.44	12.0	1.56	0.22	1.11	4.0	 b = LGJKC
12.33	2.68	65.15	0.4	5.09	3.49	5.9	1.21	1.07	0.13	0.67	1.88	 c = E
18.84	7.97	15.22	7.25	2.17	9.42	29.71	2.17	4.35	0.0	0.72	2.17	 d = FV
5.97	4.68	17.14	0.78	58.44	0.78	5.45	3.38	1.04	0.26	0.26	1.82	 e = I
16.43	2.95	5.48	0.56	0.98	54.63	4.49	7.58	1.97	1.26	1.26	2.39	 f = O
13.35	4.92	6.13	1.09	3.06	4.16	52.52	10.72	1.53	0.33	0.22	1.97	 g = SNRTY
1.64	0.93	0.4	0.12	0.34	0.83	3.37	91.9	0.12	0.03	0.06	0.25	 h = 0
14.05	4.68	8.36	2.34	10.37	9.7	13.04	7.36	23.08	0.67	2.34	4.01	 i = BMP
21.01	5.07	10.87	0.72	2.9	23.19	10.14	0.72	0.72	13.77	1.45	9.42	 j = DZ
11.93	2.75	8.72	0.46	3.21	17.43	4.13	0.92	2.29	0.0	46.33	1.83	 k = U
13.44	5.94	11.37	0.78	3.36	6.98	14.99	1.81	1.81	1.81	0.52	37.21	 l = SNRTXY
