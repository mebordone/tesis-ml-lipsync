Timestamp:2013-08-30-05:14:52
Inventario:I4
Intervalo:1ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I4-1ms-5gramas
Num Instances:  183400
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  49 4F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(33): class 
0Pitch(Hz)(99): class 
0RawPitch(79): class 
0SmPitch(93): class 
0Melogram(st)(86): class 
0ZCross(20): class 
0F1(Hz)(2645): class 
0F2(Hz)(3343): class 
0F3(Hz)(3362): class 
0F4(Hz)(3378): class 
1Int(dB)(34): class 
1Pitch(Hz)(96): class 
1RawPitch(77): class 
1SmPitch(100): class 
1Melogram(st)(87): class 
1ZCross(20): class 
1F1(Hz)(2660): class 
1F2(Hz)(3340): class 
1F3(Hz)(3375): class 
1F4(Hz)(3363): class 
2Int(dB)(33): class 
2Pitch(Hz)(95): class 
2RawPitch(74): class 
2SmPitch(95): class 
2Melogram(st)(92): class 
2ZCross(20): class 
2F1(Hz)(2627): class 
2F2(Hz)(3334): class 
2F3(Hz)(3371): class 
2F4(Hz)(3370): class 
3Int(dB)(33): class 
3Pitch(Hz)(97): class 
3RawPitch(72): class 
3SmPitch(95): class 
3Melogram(st)(91): class 
3ZCross(20): class 
3F1(Hz)(2635): class 
3F2(Hz)(3341): class 
3F3(Hz)(3378): class 
3F4(Hz)(3371): class 
4Int(dB)(33): class 
4Pitch(Hz)(93): class 
4RawPitch(72): class 
4SmPitch(93): class 
4Melogram(st)(91): class 
4ZCross(20): class 
4F1(Hz)(2623): class 
4F2(Hz)(3318): class 
4F3(Hz)(3383): class 
4F4(Hz)(3362): class 
class(10): 
LogScore Bayes: -2.8008262144385725E7
LogScore BDeu: -3.641477980993647E7
LogScore MDL: -3.433329475700139E7
LogScore ENTROPY: -3.0359765591939177E7
LogScore AIC: -3.1015494591939177E7




=== Stratified cross-validation ===

Correctly Classified Instances      148706               81.0829 %
Incorrectly Classified Instances     34694               18.9171 %
Kappa statistic                          0.7508
K&B Relative Info Score            14048466.5109 %
K&B Information Score               357356.1056 bits      1.9485 bits/instance
Class complexity | order 0          466502.5586 bits      2.5436 bits/instance
Class complexity | scheme          2109788.5049 bits     11.5038 bits/instance
Complexity improvement     (Sf)    -1643285.9463 bits     -8.9601 bits/instance
Mean absolute error                      0.0379
Root mean squared error                  0.1918
Relative absolute error                 24.6078 %
Root relative squared error             69.1476 %
Coverage of cases (0.95 level)          82.0453 %
Mean rel. region size (0.95 level)      10.3268 %
Total Number of Instances           183400     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,905    0,018    0,229      0,905    0,366      0,450    0,958     0,750     C
                 0,885    0,012    0,861      0,885    0,873      0,863    0,970     0,900     E
                 0,702    0,010    0,491      0,702    0,578      0,580    0,931     0,653     FV
                 0,852    0,013    0,941      0,852    0,894      0,872    0,967     0,938     AI
                 0,518    0,017    0,852      0,518    0,644      0,621    0,965     0,840     CDGKNRSYZ
                 0,838    0,009    0,884      0,838    0,860      0,850    0,936     0,867     O
                 0,909    0,158    0,788      0,909    0,844      0,736    0,963     0,925     0
                 0,574    0,009    0,701      0,574    0,632      0,622    0,936     0,631     LT
                 0,939    0,007    0,765      0,939    0,843      0,844    0,981     0,923     U
                 0,754    0,007    0,780      0,754    0,767      0,759    0,928     0,766     MBP
Weighted Avg.    0,811    0,070    0,829      0,811    0,809      0,758    0,960     0,888     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   980    11     0     4    13     1    74     0     0     0 |     a = C
   227 12164   108    53   300    20   576   123    45   124 |     b = E
    13    22  1798     7    40    10   646    23     2     0 |     c = FV
   904   184   442 30349   661   175  1991   506    99   329 |     d = AI
  1160   857    87   709 14737   516  9591   201   415   186 |     e = CDGKNRSYZ
   215    15    47    51   146 11087  1455    97     8   113 |     f = O
   633   764  1149   947   865   630 65523   599   549   454 |     g = 0
    74    73    15    75   346    57  2112  3802    42    23 |     h = LT
    23     4    10     4    81     0    81    41  3883    10 |     i = U
    45    27     5    41   109    46  1096    29    33  4383 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
90.49	1.02	0.0	0.37	1.2	0.09	6.83	0.0	0.0	0.0	 a = C
1.65	88.53	0.79	0.39	2.18	0.15	4.19	0.9	0.33	0.9	 b = E
0.51	0.86	70.21	0.27	1.56	0.39	25.22	0.9	0.08	0.0	 c = FV
2.54	0.52	1.24	85.15	1.85	0.49	5.59	1.42	0.28	0.92	 d = AI
4.08	3.01	0.31	2.49	51.78	1.81	33.7	0.71	1.46	0.65	 e = CDGKNRSYZ
1.62	0.11	0.36	0.39	1.1	83.78	10.99	0.73	0.06	0.85	 f = O
0.88	1.06	1.59	1.31	1.2	0.87	90.86	0.83	0.76	0.63	 g = 0
1.12	1.1	0.23	1.13	5.23	0.86	31.91	57.44	0.63	0.35	 h = LT
0.56	0.1	0.24	0.1	1.96	0.0	1.96	0.99	93.86	0.24	 i = U
0.77	0.46	0.09	0.71	1.87	0.79	18.85	0.5	0.57	75.39	 j = MBP
