Timestamp:2014-12-07-10:49:13
Inventario:I1
Intervalo:3ms
Ngramas:1
Clasificador:NaiveBayes
Relation Name:  I1-3ms-1gramas
Num Instances:  61135
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Naive Bayes Classifier

                    Class
Attribute               a         b         e         d         f         i         k         j         l         o         0         s         u
                   (0.16)    (0.03)    (0.08)    (0.01)    (0.01)    (0.04)    (0.02)    (0.01)     (0.1)    (0.07)    (0.39)    (0.06)    (0.02)
==================================================================================================================================================
0Int(dB)
  mean             -4.2995  -12.2908   -5.2154   -8.2151  -14.9663    -8.745  -23.3866  -11.5623  -12.8596    -8.205  -48.5184  -21.1024   -5.0964
  std. dev.         9.6425   10.5869     8.061    6.8402    8.8198    8.4284   10.3657    8.2124   10.4236   12.2015   15.9239    6.7883    7.5403
  weight sum          9585      1948      4621       904       864      2382      1192       693      5819      4463     23726      3552      1386
  precision         0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043

0Pitch(Hz)
  mean            215.2171  161.0025  190.6854  196.7149  132.7243  173.6379   67.3642  184.4111  158.7698  178.3171   15.7156   51.9214   230.256
  std. dev.       108.2936  109.8138   87.4198   89.5099  107.1636   76.0641  106.8951  119.6061  102.0568  105.1185    54.799   90.3009   83.5916
  weight sum          9585      1948      4621       904       864      2382      1192       693      5819      4463     23726      3552      1386
  precision         0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303

0RawPitch
  mean             205.011  148.5952  186.0468   190.776  107.6938  166.4491   44.7917  150.5995  144.2556  172.3446    12.846   29.9128   221.377
  std. dev.       114.2273  115.2511   94.2836   97.6689  102.8608   82.7406   91.3213  123.0117  108.6284   111.886   49.2565   71.5219   91.8259
  weight sum          9585      1948      4621       904       864      2382      1192       693      5819      4463     23726      3552      1386
  precision          0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131

0SmPitch
  mean             213.279  156.3345  189.2421   196.165  123.0119  171.0176    56.486   162.621   154.549  176.2127   14.1425   44.8005   229.966
  std. dev.       109.7994  112.6809   88.6477   90.4791  104.4081   78.8205  101.1877   130.198  103.8887  107.2818   50.9133   85.3576   83.6261
  weight sum          9585      1948      4621       904       864      2382      1192       693      5819      4463     23726      3552      1386
  precision         0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312

0Melogram(st)
  mean             51.3572   41.7583   50.6147   51.6188   37.2513   48.6212   16.7622   34.5176   42.3474   46.5702    4.6708   14.8861   54.4863
  std. dev.        16.8758   24.1126   15.1287   13.9964    24.928   16.0378   25.9926   28.3225    23.094    20.439    15.268   24.1901   13.4335
  weight sum          9585      1948      4621       904       864      2382      1192       693      5819      4463     23726      3552      1386
  precision         0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252

0ZCross
  mean              6.9994    2.4227    4.9156     3.706    1.9401    3.5967    2.2894    4.0504    2.4113    4.0424    0.2221    3.8307    3.8515
  std. dev.         4.6018    2.0849    3.2034    2.1752    2.0364    3.1513    4.7776    3.2828     2.055    2.9091    1.0404    7.5333    2.7932
  weight sum          9585      1948      4621       904       864      2382      1192       693      5819      4463     23726      3552      1386
  precision         1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633

0F1(Hz)
  mean             683.313  372.4935  466.9426  504.4024  381.3156  363.5983  121.9541   471.637  403.8074  486.8876   50.9705   181.017  501.4313
  std. dev.       268.9093  235.5015  141.0764  171.5549  276.5899  141.2908  225.5703  278.9021  233.6728  224.2029  162.6104  272.8673  304.2361
  weight sum          9585      1948      4621       904       864      2382      1192       693      5819      4463     23726      3552      1386
  precision         0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642

0F2(Hz)
  mean           1445.9618 1312.5688 1800.7234 1445.0542 1172.6362 1991.3969   401.452 1476.3315 1323.0179 1143.6165  159.6878   589.123 1063.4653
  std. dev.       449.7706  745.4148  507.5736   394.779  779.2046  651.9928  750.1028   792.788  707.8157  529.3781  504.4739  851.3754  414.3404
  weight sum          9585      1948      4621       904       864      2382      1192       693      5819      4463     23726      3552      1386
  precision         0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431

0F3(Hz)
  mean           2661.7097 2208.9124  2638.722 2793.5531 1994.0357 2676.1136  681.1953 2390.8763 2281.1469 2509.8227   272.283  984.2536  2701.015
  std. dev.       804.8708 1199.4463  758.2975  675.3818 1314.2962  900.3451 1228.6209 1256.7222  1195.795 1013.4676  845.4798 1410.2971  745.1651
  weight sum          9585      1948      4621       904       864      2382      1192       693      5819      4463     23726      3552      1386
  precision         0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602

0F4(Hz)
  mean           3086.6146 2654.3032  3179.034 3237.6627 2385.6763 3179.5458  803.4404 2636.0681 2626.9368  2888.156  327.6107 1121.6815 3093.8479
  std. dev.       919.3099 1409.2849  877.8457  758.6904 1536.6667  1026.953  1453.181 1372.4355 1360.5901 1148.0627 1014.8963 1604.3298  750.1771
  weight sum          9585      1948      4621       904       864      2382      1192       693      5819      4463     23726      3552      1386
  precision         0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775





=== Stratified cross-validation ===

Correctly Classified Instances       33335               54.5269 %
Incorrectly Classified Instances     27800               45.4731 %
Kappa statistic                          0.4152
K&B Relative Info Score            2435569.2442 %
K&B Information Score                70476.6693 bits      1.1528 bits/instance
Class complexity | order 0          176880.0177 bits      2.8933 bits/instance
Class complexity | scheme           389392.2903 bits      6.3694 bits/instance
Complexity improvement     (Sf)    -212512.2726 bits     -3.4761 bits/instance
Mean absolute error                      0.0759
Root mean squared error                  0.2225
Relative absolute error                 61.8825 %
Root relative squared error             89.8551 %
Coverage of cases (0.95 level)          75.8747 %
Mean rel. region size (0.95 level)      24.7548 %
Total Number of Instances            61135     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,625    0,073    0,615      0,625    0,620      0,549    0,884     0,618     a
                 0,001    0,000    0,033      0,001    0,001      0,000    0,726     0,059     b
                 0,733    0,210    0,222      0,733    0,341      0,319    0,868     0,390     e
                 0,039    0,009    0,063      0,039    0,048      0,038    0,811     0,046     d
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,731     0,030     f
                 0,233    0,021    0,314      0,233    0,267      0,245    0,871     0,325     i
                 0,061    0,008    0,133      0,061    0,084      0,078    0,830     0,091     k
                 0,016    0,001    0,250      0,016    0,030      0,061    0,745     0,070     j
                 0,105    0,052    0,176      0,105    0,132      0,068    0,750     0,180     l
                 0,007    0,001    0,320      0,007    0,014      0,038    0,782     0,204     o
                 0,900    0,143    0,800      0,900    0,847      0,744    0,953     0,951     0
                 0,227    0,017    0,451      0,227    0,302      0,292    0,885     0,385     s
                 0,348    0,011    0,428      0,348    0,384      0,373    0,875     0,351     u
Weighted Avg.    0,545    0,090    0,519      0,545    0,505      0,446    0,878     0,576     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  5991     3  2161   128     0    86    60    17   479    25   515   101    19 |     a = a
   197     1   816    36     0   177    35     0   180     3   354    43   106 |     b = b
   609     9  3388     4     0   132    43     6   195     4   177    43    11 |     c = e
   175     0   551    35     0    28     0     0    59     0    35    13     8 |     d = d
    67     0   330    26     0    60    25     0   102     0   216    30     8 |     e = f
   141     1  1369     0     0   554    35     0   115     0   129    38     0 |     f = i
    31     8    75     4     0    26    73     2   113     0   668   171    21 |     g = k
   152     1   250     0     0    22    18    11    90    18   112    19     0 |     h = j
   698     2  2802    94     0   288   121     2   611     2   940   181    78 |     i = l
   916     5  1959   125     0    23    24     2   452    31   488    84   354 |     j = o
   319     0   871    76     1   226    51     2   548     3 21352   244    33 |     k = 0
   189     0   277    11     0   120    51     2   397     4  1689   806     6 |     l = s
   253     0   430    18     0    24    14     0   123     7    22    13   482 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
62.5	0.03	22.55	1.34	0.0	0.9	0.63	0.18	5.0	0.26	5.37	1.05	0.2	 a = a
10.11	0.05	41.89	1.85	0.0	9.09	1.8	0.0	9.24	0.15	18.17	2.21	5.44	 b = b
13.18	0.19	73.32	0.09	0.0	2.86	0.93	0.13	4.22	0.09	3.83	0.93	0.24	 c = e
19.36	0.0	60.95	3.87	0.0	3.1	0.0	0.0	6.53	0.0	3.87	1.44	0.88	 d = d
7.75	0.0	38.19	3.01	0.0	6.94	2.89	0.0	11.81	0.0	25.0	3.47	0.93	 e = f
5.92	0.04	57.47	0.0	0.0	23.26	1.47	0.0	4.83	0.0	5.42	1.6	0.0	 f = i
2.6	0.67	6.29	0.34	0.0	2.18	6.12	0.17	9.48	0.0	56.04	14.35	1.76	 g = k
21.93	0.14	36.08	0.0	0.0	3.17	2.6	1.59	12.99	2.6	16.16	2.74	0.0	 h = j
12.0	0.03	48.15	1.62	0.0	4.95	2.08	0.03	10.5	0.03	16.15	3.11	1.34	 i = l
20.52	0.11	43.89	2.8	0.0	0.52	0.54	0.04	10.13	0.69	10.93	1.88	7.93	 j = o
1.34	0.0	3.67	0.32	0.0	0.95	0.21	0.01	2.31	0.01	89.99	1.03	0.14	 k = 0
5.32	0.0	7.8	0.31	0.0	3.38	1.44	0.06	11.18	0.11	47.55	22.69	0.17	 l = s
18.25	0.0	31.02	1.3	0.0	1.73	1.01	0.0	8.87	0.51	1.59	0.94	34.78	 m = u
