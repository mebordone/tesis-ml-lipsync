Timestamp:2013-07-22-22:58:43
Inventario:I4
Intervalo:3ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I4-3ms-1gramas
Num Instances:  61135
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1569





=== Stratified cross-validation ===

Correctly Classified Instances       53382               87.3182 %
Incorrectly Classified Instances      7753               12.6818 %
Kappa statistic                          0.8338
K&B Relative Info Score            4832769.5598 %
K&B Information Score               123419.6209 bits      2.0188 bits/instance
Class complexity | order 0          156108.9754 bits      2.5535 bits/instance
Class complexity | scheme          1402617.6616 bits     22.943  bits/instance
Complexity improvement     (Sf)    -1246508.6862 bits    -20.3894 bits/instance
Mean absolute error                      0.0424
Root mean squared error                  0.14  
Relative absolute error                 27.4158 %
Root relative squared error             50.3883 %
Coverage of cases (0.95 level)          97.0639 %
Mean rel. region size (0.95 level)      21.9192 %
Total Number of Instances            61135     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,813    0,000    0,916      0,813    0,861      0,862    0,959     0,877     C
                 0,891    0,011    0,870      0,891    0,881      0,871    0,979     0,920     E
                 0,606    0,001    0,878      0,606    0,717      0,727    0,941     0,703     FV
                 0,904    0,019    0,921      0,904    0,912      0,891    0,977     0,949     AI
                 0,813    0,049    0,757      0,813    0,784      0,743    0,964     0,868     CDGKNRSYZ
                 0,820    0,005    0,924      0,820    0,869      0,861    0,966     0,891     O
                 0,947    0,074    0,890      0,947    0,917      0,864    0,984     0,975     0
                 0,515    0,005    0,787      0,515    0,622      0,626    0,914     0,629     LT
                 0,906    0,001    0,947      0,906    0,926      0,925    0,984     0,947     U
                 0,680    0,003    0,898      0,680    0,774      0,775    0,943     0,781     MBP
Weighted Avg.    0,873    0,042    0,874      0,873    0,871      0,838    0,973     0,919     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   295    13     0    18    18     1    16     0     2     0 |     a = C
     3  4117     6    63   209    20   139    34    10    20 |     b = E
     0    32   524    27   216    12    41     6     3     3 |     c = FV
     6   137    15 10814   398    41   450    69     5    32 |     d = AI
     5   178    14   324  7788    72  1053   100    18    24 |     e = CDGKNRSYZ
     1    29    10    88   223  3660   412    22     5    13 |     f = O
     7   116    18   204   771    60 22461    52    10    27 |     g = 0
     0    56     4   105   422    42   426  1143     5    18 |     h = LT
     4    11     0    17    37    17    20    11  1256    13 |     i = U
     1    41     6    87   207    37   218    15    12  1324 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
81.27	3.58	0.0	4.96	4.96	0.28	4.41	0.0	0.55	0.0	 a = C
0.06	89.09	0.13	1.36	4.52	0.43	3.01	0.74	0.22	0.43	 b = E
0.0	3.7	60.65	3.13	25.0	1.39	4.75	0.69	0.35	0.35	 c = FV
0.05	1.14	0.13	90.37	3.33	0.34	3.76	0.58	0.04	0.27	 d = AI
0.05	1.86	0.15	3.38	81.33	0.75	11.0	1.04	0.19	0.25	 e = CDGKNRSYZ
0.02	0.65	0.22	1.97	5.0	82.01	9.23	0.49	0.11	0.29	 f = O
0.03	0.49	0.08	0.86	3.25	0.25	94.67	0.22	0.04	0.11	 g = 0
0.0	2.52	0.18	4.73	19.0	1.89	19.18	51.46	0.23	0.81	 h = LT
0.29	0.79	0.0	1.23	2.67	1.23	1.44	0.79	90.62	0.94	 i = U
0.05	2.1	0.31	4.47	10.63	1.9	11.19	0.77	0.62	67.97	 j = MBP
