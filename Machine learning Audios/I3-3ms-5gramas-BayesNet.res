Timestamp:2013-08-30-05:02:48
Inventario:I3
Intervalo:3ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I3-3ms-5gramas
Num Instances:  61131
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(20): class 
0Pitch(Hz)(24): class 
0RawPitch(24): class 
0SmPitch(18): class 
0Melogram(st)(31): class 
0ZCross(15): class 
0F1(Hz)(232): class 
0F2(Hz)(223): class 
0F3(Hz)(327): class 
0F4(Hz)(323): class 
1Int(dB)(21): class 
1Pitch(Hz)(30): class 
1RawPitch(25): class 
1SmPitch(23): class 
1Melogram(st)(33): class 
1ZCross(17): class 
1F1(Hz)(221): class 
1F2(Hz)(199): class 
1F3(Hz)(391): class 
1F4(Hz)(321): class 
2Int(dB)(22): class 
2Pitch(Hz)(31): class 
2RawPitch(22): class 
2SmPitch(25): class 
2Melogram(st)(33): class 
2ZCross(17): class 
2F1(Hz)(275): class 
2F2(Hz)(217): class 
2F3(Hz)(382): class 
2F4(Hz)(338): class 
3Int(dB)(21): class 
3Pitch(Hz)(29): class 
3RawPitch(22): class 
3SmPitch(24): class 
3Melogram(st)(32): class 
3ZCross(15): class 
3F1(Hz)(210): class 
3F2(Hz)(238): class 
3F3(Hz)(385): class 
3F4(Hz)(357): class 
4Int(dB)(20): class 
4Pitch(Hz)(35): class 
4RawPitch(22): class 
4SmPitch(25): class 
4Melogram(st)(32): class 
4ZCross(16): class 
4F1(Hz)(242): class 
4F2(Hz)(240): class 
4F3(Hz)(298): class 
4F4(Hz)(262): class 
class(11): 
LogScore Bayes: -6326556.328064244
LogScore BDeu: -6963941.489054118
LogScore MDL: -6854687.534095803
LogScore ENTROPY: -6469428.8136556
LogScore AIC: -6539343.813655602




=== Stratified cross-validation ===

Correctly Classified Instances       38442               62.8846 %
Incorrectly Classified Instances     22689               37.1154 %
Kappa statistic                          0.5236
K&B Relative Info Score            3312346.4551 %
K&B Information Score                86128.5309 bits      1.4089 bits/instance
Class complexity | order 0          158931.7841 bits      2.5999 bits/instance
Class complexity | scheme          1012603.7082 bits     16.5645 bits/instance
Complexity improvement     (Sf)    -853671.9241 bits    -13.9646 bits/instance
Mean absolute error                      0.0677
Root mean squared error                  0.2517
Relative absolute error                 47.926  %
Root relative squared error             94.7488 %
Coverage of cases (0.95 level)          66.1105 %
Mean rel. region size (0.95 level)      10.494  %
Total Number of Instances            61131     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,647    0,043    0,736      0,647    0,689      0,637    0,919     0,765     A
                 0,657    0,060    0,472      0,657    0,549      0,514    0,911     0,581     E
                 0,170    0,034    0,533      0,170    0,258      0,225    0,851     0,490     CGJLNQSRT
                 0,279    0,013    0,232      0,279    0,254      0,243    0,862     0,222     FV
                 0,710    0,034    0,459      0,710    0,557      0,550    0,933     0,599     I
                 0,477    0,026    0,595      0,477    0,529      0,500    0,874     0,549     O
                 0,895    0,154    0,786      0,895    0,837      0,727    0,949     0,920     0
                 0,257    0,010    0,452      0,257    0,328      0,325    0,842     0,312     BMP
                 0,681    0,020    0,446      0,681    0,539      0,538    0,951     0,644     U
                 0,000    0,002    0,000      0,000    0,000      -0,001   0,954     0,004     DZ
                 0,635    0,058    0,138      0,635    0,227      0,277    0,931     0,292     D
Weighted Avg.    0,629    0,082    0,646      0,629    0,612      0,550    0,913     0,706     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  6198   700   396   126   239   496   516    55   134    25   700 |     a = A
   291  3035   259    51   292    82   195    74    72     0   270 |     b = E
   820  1277  1913   266   788   409  3809   231   468    13  1262 |     c = CGJLNQSRT
    55   106    35   241    49    28   203    20    20     5   102 |     d = FV
    32   246   122    24  1692     3   143    26    28    10    56 |     e = I
   515   309   177    39    74  2128   476    48   228     5   464 |     f = O
   286   412   479   189   300   239 21231   111    64    54   357 |     g = 0
   113   192   123    61   208    64   360   501   136     3   187 |     h = BMP
    45    82    52    16    10    84    36    31   944     0    86 |     i = U
     0     0     0     0     0     0    23     0     0     0     0 |     j = DZ
    64    77    32    24    37    42     5    12    24     5   559 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
64.66	7.3	4.13	1.31	2.49	5.17	5.38	0.57	1.4	0.26	7.3	 a = A
6.3	65.68	5.6	1.1	6.32	1.77	4.22	1.6	1.56	0.0	5.84	 b = E
7.29	11.35	17.0	2.36	7.0	3.63	33.84	2.05	4.16	0.12	11.21	 c = CGJLNQSRT
6.37	12.27	4.05	27.89	5.67	3.24	23.5	2.31	2.31	0.58	11.81	 d = FV
1.34	10.33	5.12	1.01	71.03	0.13	6.0	1.09	1.18	0.42	2.35	 e = I
11.54	6.92	3.97	0.87	1.66	47.68	10.67	1.08	5.11	0.11	10.4	 f = O
1.21	1.74	2.02	0.8	1.26	1.01	89.5	0.47	0.27	0.23	1.5	 g = 0
5.8	9.86	6.31	3.13	10.68	3.29	18.48	25.72	6.98	0.15	9.6	 h = BMP
3.25	5.92	3.75	1.15	0.72	6.06	2.6	2.24	68.11	0.0	6.2	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
7.26	8.74	3.63	2.72	4.2	4.77	0.57	1.36	2.72	0.57	63.45	 k = D
