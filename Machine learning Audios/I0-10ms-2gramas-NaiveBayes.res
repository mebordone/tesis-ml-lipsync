Timestamp:2014-12-07-10:41:25
Inventario:I0
Intervalo:10ms
Ngramas:2
Clasificador:NaiveBayes
Relation Name:  I0-10ms-2gramas
Num Instances:  18295
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    27 


Naive Bayes Classifier

                    Class
Attribute              ch        rr         0        hs        hg         a         c         b         e         d         g         f         i         j         m         l         o         n         q         p         s         r         u         t         v         y         z
                      (0)       (0)    (0.37)    (0.01)       (0)    (0.15)    (0.02)    (0.01)    (0.08)    (0.01)    (0.01)    (0.01)    (0.04)    (0.01)    (0.01)    (0.02)    (0.08)    (0.04)       (0)    (0.01)    (0.05)    (0.02)    (0.02)    (0.02)    (0.01)    (0.01)       (0)
==============================================================================================================================================================================================================================================================================================
0Int(dB)
  mean            -20.5241   -8.9079  -49.4515  -18.3945  -10.9143   -3.4874  -27.1278   -5.9205   -5.9386   -7.8272   -7.3322  -20.2769   -9.6878  -14.5623   -7.7314   -8.0639    -8.538   -8.5421   -25.739  -24.0002  -20.4527  -10.9531    -6.523  -28.2288   -9.5797  -16.1719  -27.1778
  std. dev.         9.2709    3.9616   15.1903   11.2554    5.9465    9.0731   10.1168    4.4133    8.7524    5.9928    7.6223    6.8393    8.9009    6.8147    6.6716    5.7805   12.0283    7.1912   12.7573   10.6807    7.4939    9.5526    9.2325    9.0793     7.837    5.6953    1.8825
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005

0Pitch(Hz)
  mean             70.0807  248.8916   13.6877  102.0558  144.7583  224.2102   41.0014  215.5948  187.1289  201.6085  218.1275   99.9163   169.624  166.8513  191.7248  173.7241   180.298  195.2404   82.0202   96.6796   69.5972  162.7305  226.0414   66.2344  175.5686  117.2643         0
  std. dev.       108.4721  120.2835   51.1466   87.4688   35.9687  104.2012   82.5541    73.087   89.1848   83.5041   95.0512  122.3269   79.3475  129.3049   86.2093   66.2695  104.9788   87.1904  113.8397  124.0795  102.6853   92.1325   90.8899  108.7754   77.3773   68.4476    0.0271
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626

0RawPitch
  mean             47.6306  244.8591   11.1121   82.6437   134.424  214.0626   30.0392  212.5564  182.7671  195.8548  206.6453   65.8049  160.2257  118.2759  181.6908  160.8132  173.1119  183.4534   52.0184   74.6828    47.609  156.9347  217.7899   41.9111  161.9325   88.9685         0
  std. dev.        96.1387  116.4206   45.2351   88.6062   60.2341  109.3238     72.25   74.7057   93.8486   92.7432  107.5275  100.7144   83.7348   114.697   94.8138   74.6362   111.182   98.9471  104.5844   115.871   89.4023    98.019   96.3272   90.9775   87.1195   78.2522     0.028
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679

0SmPitch
  mean             64.2268  248.8941   12.1409   95.0621  146.6159  222.2798   35.2419  215.5965  185.0532  200.8491  216.3151   87.5723  167.3233  134.9539  189.8043  173.1633  177.9648  193.3881   68.1279   87.6433   62.7379  159.2636  225.0495   56.5674  172.2337  111.7801         0
  std. dev.       104.6392  120.3065   46.8868   85.3669    33.889  106.0002   79.1149   73.0933    91.154   85.1021   97.3174  111.7598   80.6951  140.9079   88.8931    66.872  106.8233   89.6175  108.6187  122.1285   99.0721   94.2653   91.3186  102.5507   80.5713   70.5142    0.0275
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647

0Melogram(st)
  mean             18.3304   57.0055    4.1821   30.8219   48.0494    53.038   13.1534   55.3978   50.1673   53.0952   52.3042   28.2039   47.7876   23.5686   51.0287   51.2117   46.7989   50.8912   18.1582   24.7793   19.2083   44.2001   52.9506    17.145   50.1795   42.2147     6.515
  std. dev.         26.881     8.789   14.5486   24.6142    8.1267   14.9413   23.5725    6.2667    15.729   10.7263   16.2008   28.0681   17.1488   28.3215   13.9559    9.3205   20.3195   15.4478   26.6522   28.5062   26.2584   21.4146   16.4082   26.2683   12.2902   16.9263   17.2371
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294

0ZCross
  mean              7.1098     3.878    0.1759    1.2425    2.8698    7.2369    1.2136    3.6499    4.8777     3.601    4.4338     0.793    3.3458    3.8141    2.6852    3.1269    4.0104    2.6192    2.9045    0.8851    3.5567    3.3052    3.6091    0.3841    3.0769    1.4968         0
  std. dev.        11.2288    2.2567    0.9112    1.7081     2.073    4.4828    3.0992    1.7468    3.2885    2.1368    2.8448    1.6257    3.3619    3.6444    1.5427    1.9806    3.2415    1.8369    5.6311     2.407    6.4805     2.615     2.929    1.0549    1.9145     1.606    0.2154
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927

0F1(Hz)
  mean            186.7484  596.3535   46.3495  450.4309  446.2853  685.3401  108.1119  475.9176  452.4055  513.7324  508.9472  355.4012  353.4925  484.4467  438.0606  481.5807  474.0843  493.4379  194.7788  268.8187  246.6757  507.4675  482.6221  139.1274  487.9085  352.7381         0
  std. dev.        285.582  131.8171  157.5663   272.391   80.9776  270.5143  219.3566  132.9536  159.6131  147.7122   182.517  334.8181  155.8578  317.1139  169.9994  150.0679  229.5879  153.6286  244.1212  304.6746  302.7801  228.0568  319.3509  224.9236  169.4416  180.1138    0.0615
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688

0F2(Hz)
  mean            557.5339 1677.2585  141.6776 1302.9435 1938.1096 1439.7475  320.9953 1592.7064 1730.4304 1489.1875 1730.5088   921.643 1886.1841 1434.0044 1655.2822 1661.6792 1149.4791 1631.3556   811.932  816.0242  762.1553  1399.565 1027.4385  470.7218   1535.49 1370.9709         0
  std. dev.       818.9804  129.6918  475.8914  764.3072  363.0502  439.1719   665.828  356.6616  583.9406  299.1535  532.3332  849.5675  727.5947  850.1505  474.4892  375.5223  557.6755   421.869  987.4242   884.377  905.8102  578.0643  482.4236  737.8472  446.1294  632.1899    0.0751
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504

0F3(Hz)
  mean            924.8809 2873.0419  243.8031 2127.9338 2538.6792 2680.7979  581.1487 2768.7739 2566.2209 2878.9701 2976.1527  1580.627 2569.2982 2076.9357 2750.6083 2779.0351 2474.5963 2811.0177 1205.7557 1349.2428 1269.2773 2442.2833 2578.9648  860.1903 2648.9603 2393.4621         0
  std. dev.      1360.7308  399.6352  803.7435 1159.3081  115.8264  800.8336 1171.3084  462.7451  864.4824  419.4367  827.9906 1446.2534 1001.9819 1233.4839   622.276  607.6561 1047.0894  585.9109 1422.8996 1454.3339 1485.3307 1011.6955   937.242 1328.7103  732.7104 1123.0861    0.1118
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705

0F4(Hz)
  mean           1101.2987 3243.3662  293.9019 2648.2481 3587.3982 3084.2343  657.8236 3307.9819 3090.2109 3351.4375 2973.4111 1825.2207 3069.7504 2591.3756 3287.1003 3209.8195 2853.8724   3216.71 1463.8958 1593.1889 1447.4735 2854.5371 2942.8419  970.9291 3192.5289 2739.5434         0
  std. dev.      1618.8558  423.2397  966.7162 1428.4275   92.6757  910.7211 1327.5663  436.4847 1010.4123  448.5867  831.3383 1655.5543 1158.3793 1520.7459  681.3686  637.0083 1192.6404    667.17 1741.3978 1696.1758 1691.2509 1121.0024  994.8814 1502.9051  797.8462 1271.8129    0.1142
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851

1Int(dB)
  mean            -19.0814   -10.564  -49.8199  -20.7518   -10.651   -3.1239   -24.237   -5.8918   -5.4216   -7.8401    -7.615  -21.5769   -9.1229  -15.4779   -7.6993   -8.3034   -8.3525   -9.3733  -22.3313  -22.5999  -21.5058  -11.9183   -5.5461  -25.6222   -9.2619  -16.1571  -28.0322
  std. dev.         8.3794    4.2228   14.6152   11.3686    6.2337    8.4027    9.8677     4.366    8.2134    6.1435    7.8549    5.6378    8.6417    6.1848    6.0498     5.792   12.1594    7.7206   10.9438   10.4858    6.7641    9.7379    7.9501    9.8486     6.325    5.0145    3.0174
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005

1Pitch(Hz)
  mean             77.6495  248.6632   11.8118   88.6305  146.2458  226.0088   68.8773  215.3605  190.3918  201.0018  217.1294   89.6626  173.2212  160.2725  193.0406  173.5217  179.1301  191.7203   92.8244  101.0594   58.4205     162.5  230.9859   83.9606  179.4156  119.7435         0
  std. dev.        114.169  122.8744   47.8985   86.8337   37.7558  102.2025  104.3432   72.1803   87.4257   84.8129     97.99  119.8396   75.5653   128.781   85.9993   65.1768   103.979   89.2387   116.884  120.1185   97.2112   94.0654   83.0165  115.3984   70.8481   68.2851    0.0271
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626

1RawPitch
  mean             52.1893  240.3952    9.4597   69.5402   135.745   216.219   52.1615  213.0008  186.9416  195.4717   203.988   53.3253  164.8509  112.0009  182.4391  159.1457  172.8881  179.3233   53.0661   75.9229   35.4666  156.5619  221.8351   61.2075  165.4778   86.7859         0
  std. dev.        99.7193  129.1598   41.8723   85.3335   62.4099  107.4348   93.6374   74.2022   92.1579   93.1794  111.7049   91.6829   80.9322  111.4484   95.6759   75.4546  110.0663  101.2616  100.9754  112.2847   78.8653   98.0512   90.5835  101.7704   80.6042   76.2217     0.028
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679

1SmPitch
  mean             71.8091  248.6628   10.2225   81.6346  148.1583   224.319   58.5754  215.3632  188.8901  200.6341  215.3063   77.2901   170.918  128.4736  190.7569  172.9529  177.1958  189.2119   75.7723   90.2963   51.3331   159.705  230.1655   75.1964  176.6789  114.2394         0
  std. dev.       110.8409  122.8761   43.0537   83.6012   35.5963  103.8158  101.5424   72.1765   88.8533   85.6348  100.1796  107.8428   77.6434  139.0808   89.2888   65.7996  105.5718   92.1796  111.1442  118.5691   92.1664   95.5864   83.5323   111.752   74.1419   70.5684    0.0274
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647

1Melogram(st)
  mean             19.7831   56.8115    3.6598   27.2818   49.2962   53.6675   18.2983   55.3973    50.757   53.0359   51.8536    26.048   48.7629   22.0537   51.1178   51.1759   46.9889   49.9973   22.4883   25.8102   16.3662   43.5626   54.7081   21.5794   51.3611   42.8327         0
  std. dev.        27.5706    9.0272   13.7158   24.9859    4.5337   13.6842   26.4593    6.2177   14.7786   10.7827   16.9829   28.1169   15.7624   28.1228   13.9765    9.3126   20.0024   16.6205   28.1066   28.2544   25.2647   22.0153      13.2   27.8404    9.7628   16.4263    0.0049
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294

1ZCross
  mean              8.0352    3.3856    0.1407     0.935    3.0249    7.3294    1.6275    3.8237    4.8831    3.7468    4.3613    0.6192    3.3285    3.6989    2.5711    2.9846    4.0329     2.295    4.0696    0.9233    3.5638    2.9761     3.713    0.7054    2.9586    1.4968         0
  std. dev.        12.1849    2.3424    0.7914    1.4833    2.2481    4.4255    3.3215    1.8002    3.1607    2.1448    2.7409    1.4786    3.1155    3.6519    1.3416    1.8611    2.9611    1.5017     6.427    2.2102    6.6592     2.381    2.8728    1.4341    1.7054     1.595    0.2154
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927

1F1(Hz)
  mean            160.6998  613.5628   40.0642   410.892  445.1347  710.3558  126.3501  477.7832  467.7497  516.3637  500.5551  293.0667  366.3373   470.795  432.8978  472.7167  488.3137    469.73  197.8336  262.9692  198.5962   493.922  499.4556  141.7193  489.0979  353.5416         0
  std. dev.       267.2971  138.1947  146.5236  293.3077   84.6291  245.4904  233.9364  132.4039  137.6849   149.061  184.0787  335.6295  136.8258  334.1598  164.9276  140.2743  220.1308   154.595  246.6861  296.9297  287.8838  231.1068   302.477  225.4522  159.5841   173.601    0.0615
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688

1F2(Hz)
  mean            507.3849 1646.3551  123.4695 1185.2173 1925.4093 1474.9501  353.8751 1576.1489 1804.3312  1487.044  1706.467  753.8431 1998.9223  1339.941 1667.1307 1670.6231 1157.6298 1607.4525   804.665  791.6004  620.4736 1384.7266 1072.0404  481.9616 1580.3836 1406.3078         0
  std. dev.       809.2374  124.3386  446.9566  819.2806  350.2846  373.1444  671.6031  352.2262  496.1344  307.7561  559.5245  846.5228  624.7892  881.0201  440.9842  341.4083  525.4581  447.1571   975.108  861.5481  872.6886  597.3845  424.8655   746.218  364.9676  626.0495    0.0751
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504

1F3(Hz)
  mean            815.6971 2878.1508  212.0373 1928.4643 2525.5501 2749.0319  668.7844 2760.3104 2653.2835 2875.6804 2943.5343 1292.8131 2693.2397 1959.5845 2751.0866 2808.2538 2526.8298 2775.5254 1207.9329 1334.5273 1028.4704 2410.6703 2703.6172  868.1084 2719.9525  2433.031         0
  std. dev.      1298.7799  422.0628  754.2349 1263.0595  111.9726  684.6052 1227.1559   469.379  742.6344  453.3058  876.6084 1442.3291  863.6174 1279.3581  576.3269  575.8686  995.0347  648.2804 1426.1474 1439.2326 1427.0793 1048.5757  758.4179 1330.4613  594.2459 1101.5516    0.1118
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705

1F4(Hz)
  mean            998.6249 3228.0327  256.0029 2406.8084   3591.92 3162.4058  773.8643  3303.497 3189.0151 3344.0454 2953.1522 1496.7859 3209.5559 2464.4123 3307.9787  3233.941 2910.5036 3182.3562 1443.9683  1591.677 1178.4883 2804.2139 3086.4504  985.6571   3287.48 2773.3087         0
  std. dev.      1588.9955  403.3343  908.3598 1563.9842   91.1897  774.8805 1420.2752  430.5134  856.4692  491.6785  883.5322 1658.5798  985.3806 1594.7315  625.5718  585.2383 1127.9374  747.5758 1718.2445 1694.5457 1633.2146 1165.5043  766.0819 1516.0453  587.4544 1242.1583    0.1142
  weight sum            88        21      6797       206        50      2684       278       119      1438       266       107       119       748       101       272       327      1377       650        81       203       905       440       423       350       142        95         8
  precision         0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851





=== Stratified cross-validation ===

Correctly Classified Instances        5642               30.839  %
Incorrectly Classified Instances     12653               69.161  %
Kappa statistic                          0.2323
K&B Relative Info Score             371351.8372 %
K&B Information Score                12453.4831 bits      0.6807 bits/instance
Class complexity | order 0           61257.4032 bits      3.3483 bits/instance
Class complexity | scheme           598046.558  bits     32.6891 bits/instance
Complexity improvement     (Sf)    -536789.1548 bits    -29.3408 bits/instance
Mean absolute error                      0.0516
Root mean squared error                  0.2079
Relative absolute error                 84.9083 %
Root relative squared error            119.2909 %
Coverage of cases (0.95 level)          43.0719 %
Mean rel. region size (0.95 level)       8.8282 %
Total Number of Instances            18295     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,170    0,003    0,203      0,170    0,185      0,182    0,713     0,107     ch
                 0,238    0,012    0,023      0,238    0,041      0,070    0,868     0,020     rr
                 0,521    0,016    0,952      0,521    0,673      0,607    0,916     0,884     0
                 0,141    0,023    0,065      0,141    0,089      0,081    0,713     0,044     hs
                 0,860    0,126    0,018      0,860    0,036      0,114    0,942     0,089     hg
                 0,502    0,050    0,635      0,502    0,561      0,500    0,880     0,617     a
                 0,112    0,009    0,157      0,112    0,131      0,121    0,544     0,073     c
                 0,311    0,107    0,019      0,311    0,035      0,053    0,825     0,023     b
                 0,082    0,006    0,549      0,082    0,143      0,191    0,757     0,241     e
                 0,034    0,019    0,025      0,034    0,029      0,013    0,776     0,033     d
                 0,047    0,003    0,089      0,047    0,061      0,061    0,756     0,024     g
                 0,034    0,006    0,038      0,034    0,036      0,030    0,591     0,021     f
                 0,076    0,003    0,538      0,076    0,133      0,192    0,827     0,245     i
                 0,178    0,001    0,400      0,178    0,247      0,264    0,745     0,151     j
                 0,000    0,001    0,000      0,000    0,000      -0,005   0,793     0,039     m
                 0,269    0,052    0,086      0,269    0,130      0,125    0,827     0,067     l
                 0,007    0,005    0,115      0,007    0,014      0,010    0,738     0,168     o
                 0,017    0,018    0,034      0,017    0,023      -0,001   0,775     0,077     n
                 0,000    0,000    0,000      0,000    0,000      -0,001   0,671     0,021     q
                 0,000    0,003    0,000      0,000    0,000      -0,006   0,597     0,032     p
                 0,152    0,006    0,563      0,152    0,240      0,276    0,639     0,266     s
                 0,011    0,002    0,128      0,011    0,021      0,031    0,695     0,046     r
                 0,173    0,005    0,471      0,173    0,253      0,275    0,848     0,255     u
                 0,097    0,014    0,123      0,097    0,108      0,094    0,550     0,059     t
                 0,000    0,001    0,000      0,000    0,000      -0,002   0,761     0,017     v
                 0,158    0,016    0,048      0,158    0,074      0,079    0,857     0,032     y
                 0,875    0,204    0,002      0,875    0,004      0,035    0,895     0,002     z
Weighted Avg.    0,308    0,018    0,575      0,308    0,381      0,354    0,823     0,491     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m    n    o    p    q    r    s    t    u    v    w    x    y    z   aa   <-- classified as
   15    2    5    2    2    5   11    2    1    2    0    3    0    1    0    2    2    1    0    0   10    0    0    2    0    0   20 |    a = ch
    0    5    0    0   10    3    0    2    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    1    0 |    b = rr
    2   13 3542   81  113   11    7   63    4   39    0   19    7    2    2   59   10   62    0    3   28    4    4   44    1   30 2647 |    c = 0
    0    3    5   29   61    7    0   10    0    1    0    2    0    1    0   15    0    6    0    0    6    3    0    8    0    9   40 |    d = hs
    0    0    0    0   43    0    0    0    1    2    0    0    1    0    0    2    0    0    0    0    0    0    0    0    0    1    0 |    e = hg
    1   34   18   35  341 1348   16  334   17  171    2    6    1    6    0  155   27   26    1   17    6    6   14   22    4   24   52 |    f = a
    6    0   22   11    8    0   31    3    0    0    0    8    2    1    0    4    4    8    1    5   12    0    6   21    0    4  121 |    g = c
    0    6    0    0   34   11    0   37    0    1    3    0    0    0    0   21    0    1    0    0    0    0    4    0    0    1    0 |    h = b
    1   20    5   26  518  185    8  278  118    9   19    3   11    1    1   91   14   21    1   13    4    0    8   18    0   20   45 |    i = e
    0   10    1    4   77   31    0   94    1    9    2    0    0    0    0   26    0    1    0    0    0    0    0    2    0    8    0 |    j = d
    0    6    0    2   29   17    0   26    0    0    5    2    2    0    0   11    0    2    0    0    1    0    0    0    0    0    4 |    k = g
    0    3    2   11   10    5    5    4    1    3    0    4    0    0    0    5    0    9    0    0    7    0    0    5    0    3   42 |    l = f
    2    2    5   21  174   37    4  155   43    0    8    2   57    1   17   97    1   16    1    4    3    0    2   24    0   46   26 |    m = i
    0    2    2    7    7   12    0    9    0    0    0    4    1   18    0    1    3    6    0    0    3    6    0    3    0    1   16 |    n = j
    0    3    0    9  102   19    0   73    0    5    0    0    1    0    0   39    0    2    0    0    0    0    5    3    0    7    4 |    o = m
    0   10    1    4  110   17    2   72    4    2    0    1    0    0    0   88    2    4    0    1    0    0    0    1    0    7    1 |    p = l
    2   25    8   47  230  219    4  336    9   68    1    6    3    3    1  144   10   22    1   10    2    2   31   12    6   49  126 |    q = o
    0   38    0   33  180   37    0  198    3    7    6    1    9    0    0   78    0   11    0    0    3    0    3    5    1   23   14 |    r = n
    6    0    9    4    6    6    6    1    0    2    0    1    0    0    1    0    0    8    0    0    8    0    0    2    0    0   21 |    s = q
    0    2    9   16   12   10    7    7    1    5    0    9    0    1    0    6    2   12    0    0    5    2    2   23    0    5   67 |    t = p
   39   18   57   51   43   34   61   11    7   11    1   18    7    7    1   29    0   51    0    2  138    9    0   17    0   16  277 |    u = s
    0   12    3   28   87   43    1   78    1    9    9    5    1    0    0   77    1   15    0    1    5    5    0   10    2    9   38 |    v = r
    0    3    2    4   28   57    4  147    0    4    0    1    0    0    2   46    7    4    0    2    0    2   73   14    0   19    4 |    w = u
    0    4   22   18   17    0   25    5    0    2    0    7    3    3    0    0    4   32    1    4    2    0    2   34    0    6  159 |    x = t
    0    0    1    0   66   10    0   25    4    4    0    0    0    0    1   17    0    2    0    0    0    0    0    3    0    8    1 |    y = v
    0    0    2    1   41    0    5    5    0    0    0    2    0    0    0    9    0    5    0    0    2    0    1    4    0   15    3 |    z = y
    0    0    1    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    7 |   aa = z

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z	aa	
17.05	2.27	5.68	2.27	2.27	5.68	12.5	2.27	1.14	2.27	0.0	3.41	0.0	1.14	0.0	2.27	2.27	1.14	0.0	0.0	11.36	0.0	0.0	2.27	0.0	0.0	22.73	 a = ch
0.0	23.81	0.0	0.0	47.62	14.29	0.0	9.52	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	4.76	0.0	 b = rr
0.03	0.19	52.11	1.19	1.66	0.16	0.1	0.93	0.06	0.57	0.0	0.28	0.1	0.03	0.03	0.87	0.15	0.91	0.0	0.04	0.41	0.06	0.06	0.65	0.01	0.44	38.94	 c = 0
0.0	1.46	2.43	14.08	29.61	3.4	0.0	4.85	0.0	0.49	0.0	0.97	0.0	0.49	0.0	7.28	0.0	2.91	0.0	0.0	2.91	1.46	0.0	3.88	0.0	4.37	19.42	 d = hs
0.0	0.0	0.0	0.0	86.0	0.0	0.0	0.0	2.0	4.0	0.0	0.0	2.0	0.0	0.0	4.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	2.0	0.0	 e = hg
0.04	1.27	0.67	1.3	12.7	50.22	0.6	12.44	0.63	6.37	0.07	0.22	0.04	0.22	0.0	5.77	1.01	0.97	0.04	0.63	0.22	0.22	0.52	0.82	0.15	0.89	1.94	 f = a
2.16	0.0	7.91	3.96	2.88	0.0	11.15	1.08	0.0	0.0	0.0	2.88	0.72	0.36	0.0	1.44	1.44	2.88	0.36	1.8	4.32	0.0	2.16	7.55	0.0	1.44	43.53	 g = c
0.0	5.04	0.0	0.0	28.57	9.24	0.0	31.09	0.0	0.84	2.52	0.0	0.0	0.0	0.0	17.65	0.0	0.84	0.0	0.0	0.0	0.0	3.36	0.0	0.0	0.84	0.0	 h = b
0.07	1.39	0.35	1.81	36.02	12.87	0.56	19.33	8.21	0.63	1.32	0.21	0.76	0.07	0.07	6.33	0.97	1.46	0.07	0.9	0.28	0.0	0.56	1.25	0.0	1.39	3.13	 i = e
0.0	3.76	0.38	1.5	28.95	11.65	0.0	35.34	0.38	3.38	0.75	0.0	0.0	0.0	0.0	9.77	0.0	0.38	0.0	0.0	0.0	0.0	0.0	0.75	0.0	3.01	0.0	 j = d
0.0	5.61	0.0	1.87	27.1	15.89	0.0	24.3	0.0	0.0	4.67	1.87	1.87	0.0	0.0	10.28	0.0	1.87	0.0	0.0	0.93	0.0	0.0	0.0	0.0	0.0	3.74	 k = g
0.0	2.52	1.68	9.24	8.4	4.2	4.2	3.36	0.84	2.52	0.0	3.36	0.0	0.0	0.0	4.2	0.0	7.56	0.0	0.0	5.88	0.0	0.0	4.2	0.0	2.52	35.29	 l = f
0.27	0.27	0.67	2.81	23.26	4.95	0.53	20.72	5.75	0.0	1.07	0.27	7.62	0.13	2.27	12.97	0.13	2.14	0.13	0.53	0.4	0.0	0.27	3.21	0.0	6.15	3.48	 m = i
0.0	1.98	1.98	6.93	6.93	11.88	0.0	8.91	0.0	0.0	0.0	3.96	0.99	17.82	0.0	0.99	2.97	5.94	0.0	0.0	2.97	5.94	0.0	2.97	0.0	0.99	15.84	 n = j
0.0	1.1	0.0	3.31	37.5	6.99	0.0	26.84	0.0	1.84	0.0	0.0	0.37	0.0	0.0	14.34	0.0	0.74	0.0	0.0	0.0	0.0	1.84	1.1	0.0	2.57	1.47	 o = m
0.0	3.06	0.31	1.22	33.64	5.2	0.61	22.02	1.22	0.61	0.0	0.31	0.0	0.0	0.0	26.91	0.61	1.22	0.0	0.31	0.0	0.0	0.0	0.31	0.0	2.14	0.31	 p = l
0.15	1.82	0.58	3.41	16.7	15.9	0.29	24.4	0.65	4.94	0.07	0.44	0.22	0.22	0.07	10.46	0.73	1.6	0.07	0.73	0.15	0.15	2.25	0.87	0.44	3.56	9.15	 q = o
0.0	5.85	0.0	5.08	27.69	5.69	0.0	30.46	0.46	1.08	0.92	0.15	1.38	0.0	0.0	12.0	0.0	1.69	0.0	0.0	0.46	0.0	0.46	0.77	0.15	3.54	2.15	 r = n
7.41	0.0	11.11	4.94	7.41	7.41	7.41	1.23	0.0	2.47	0.0	1.23	0.0	0.0	1.23	0.0	0.0	9.88	0.0	0.0	9.88	0.0	0.0	2.47	0.0	0.0	25.93	 s = q
0.0	0.99	4.43	7.88	5.91	4.93	3.45	3.45	0.49	2.46	0.0	4.43	0.0	0.49	0.0	2.96	0.99	5.91	0.0	0.0	2.46	0.99	0.99	11.33	0.0	2.46	33.0	 t = p
4.31	1.99	6.3	5.64	4.75	3.76	6.74	1.22	0.77	1.22	0.11	1.99	0.77	0.77	0.11	3.2	0.0	5.64	0.0	0.22	15.25	0.99	0.0	1.88	0.0	1.77	30.61	 u = s
0.0	2.73	0.68	6.36	19.77	9.77	0.23	17.73	0.23	2.05	2.05	1.14	0.23	0.0	0.0	17.5	0.23	3.41	0.0	0.23	1.14	1.14	0.0	2.27	0.45	2.05	8.64	 v = r
0.0	0.71	0.47	0.95	6.62	13.48	0.95	34.75	0.0	0.95	0.0	0.24	0.0	0.0	0.47	10.87	1.65	0.95	0.0	0.47	0.0	0.47	17.26	3.31	0.0	4.49	0.95	 w = u
0.0	1.14	6.29	5.14	4.86	0.0	7.14	1.43	0.0	0.57	0.0	2.0	0.86	0.86	0.0	0.0	1.14	9.14	0.29	1.14	0.57	0.0	0.57	9.71	0.0	1.71	45.43	 x = t
0.0	0.0	0.7	0.0	46.48	7.04	0.0	17.61	2.82	2.82	0.0	0.0	0.0	0.0	0.7	11.97	0.0	1.41	0.0	0.0	0.0	0.0	0.0	2.11	0.0	5.63	0.7	 y = v
0.0	0.0	2.11	1.05	43.16	0.0	5.26	5.26	0.0	0.0	0.0	2.11	0.0	0.0	0.0	9.47	0.0	5.26	0.0	0.0	2.11	0.0	1.05	4.21	0.0	15.79	3.16	 z = y
0.0	0.0	12.5	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	87.5	 aa = z
