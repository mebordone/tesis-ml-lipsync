Timestamp:2013-08-30-04:18:02
Inventario:I1
Intervalo:10ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I1-10ms-5gramas
Num Instances:  18292
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  43 4RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  44 4SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  45 4Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  47 4F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  49 4F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  50 4F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(6): class 
0RawPitch(4): class 
0SmPitch(6): class 
0Melogram(st)(8): class 
0ZCross(7): class 
0F1(Hz)(9): class 
0F2(Hz)(9): class 
0F3(Hz)(8): class 
0F4(Hz)(9): class 
1Int(dB)(10): class 
1Pitch(Hz)(6): class 
1RawPitch(4): class 
1SmPitch(6): class 
1Melogram(st)(7): class 
1ZCross(10): class 
1F1(Hz)(10): class 
1F2(Hz)(10): class 
1F3(Hz)(7): class 
1F4(Hz)(10): class 
2Int(dB)(12): class 
2Pitch(Hz)(5): class 
2RawPitch(4): class 
2SmPitch(5): class 
2Melogram(st)(10): class 
2ZCross(9): class 
2F1(Hz)(11): class 
2F2(Hz)(10): class 
2F3(Hz)(13): class 
2F4(Hz)(9): class 
3Int(dB)(10): class 
3Pitch(Hz)(6): class 
3RawPitch(6): class 
3SmPitch(5): class 
3Melogram(st)(9): class 
3ZCross(11): class 
3F1(Hz)(13): class 
3F2(Hz)(11): class 
3F3(Hz)(10): class 
3F4(Hz)(9): class 
4Int(dB)(11): class 
4Pitch(Hz)(5): class 
4RawPitch(4): class 
4SmPitch(4): class 
4Melogram(st)(8): class 
4ZCross(11): class 
4F1(Hz)(12): class 
4F2(Hz)(10): class 
4F3(Hz)(9): class 
4F4(Hz)(8): class 
class(13): 
LogScore Bayes: -1076188.2255167353
LogScore BDeu: -1098414.9894244096
LogScore MDL: -1095889.6809492528
LogScore ENTROPY: -1072546.5608561086
LogScore AIC: -1077303.5608561086




=== Stratified cross-validation ===

Correctly Classified Instances       10032               54.8436 %
Incorrectly Classified Instances      8260               45.1564 %
Kappa statistic                          0.448 
K&B Relative Info Score             783578.1908 %
K&B Information Score                22982.0696 bits      1.2564 bits/instance
Class complexity | order 0           53630.1246 bits      2.9319 bits/instance
Class complexity | scheme           282166.8395 bits     15.4257 bits/instance
Complexity improvement     (Sf)    -228536.7149 bits    -12.4938 bits/instance
Mean absolute error                      0.0696
Root mean squared error                  0.2497
Relative absolute error                 56.0297 %
Root relative squared error            100.202  %
Coverage of cases (0.95 level)          60.6276 %
Mean rel. region size (0.95 level)      10.3269 %
Total Number of Instances            18292     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,555    0,056    0,656      0,555    0,601      0,535    0,885     0,685     a
                 0,037    0,005    0,216      0,037    0,063      0,077    0,763     0,098     b
                 0,442    0,051    0,427      0,442    0,434      0,385    0,870     0,409     e
                 0,588    0,102    0,081      0,588    0,142      0,189    0,872     0,115     d
                 0,130    0,022    0,080      0,130    0,099      0,085    0,780     0,051     f
                 0,616    0,041    0,389      0,616    0,477      0,463    0,907     0,429     i
                 0,301    0,052    0,103      0,301    0,154      0,148    0,833     0,089     k
                 0,245    0,022    0,113      0,245    0,155      0,153    0,851     0,084     j
                 0,049    0,015    0,263      0,049    0,083      0,076    0,777     0,231     l
                 0,267    0,018    0,553      0,267    0,360      0,352    0,824     0,402     o
                 0,884    0,077    0,872      0,884    0,878      0,805    0,958     0,951     0
                 0,248    0,029    0,347      0,248    0,290      0,256    0,880     0,319     s
                 0,468    0,014    0,444      0,468    0,456      0,443    0,907     0,473     u
Weighted Avg.    0,548    0,051    0,590      0,548    0,550      0,505    0,890     0,604     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1632   10  208  388   66   81  143   51   54  134   91   62   20 |    a = a
   53   22   60  102   32  106   49   41   17   13   47   28   24 |    b = b
  143    2  635  184   21  137   73   82   26   19   48   30   38 |    c = e
   41    2   12  161    4    9    2    6   12    7    5    5    8 |    d = d
   21    1   30   54   34   13   28   16    7    9   18   26    4 |    e = f
    7    6   81   38    7  461   63   30    5    1   26   19    4 |    f = i
    5    3    6    6   15   15  108    5    7    7  151   31    0 |    g = k
   30    2   43   20   11   12    8   51    1    0    8   18    4 |    h = j
  226   13  171  508   82  164  132   78   88   32  146   99   49 |    i = l
  197    7  112  273   15   44   79   22   33  368  117   29   81 |    j = o
   64   16   74  102   90   71  128   15   32   28 6004  156   14 |    k = 0
   40   17   27  109   47   63  198   49   44    6  216  270    2 |    l = s
   30    1   29   52    1    9   36    4    8   42    9    4  198 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
55.51	0.34	7.07	13.2	2.24	2.76	4.86	1.73	1.84	4.56	3.1	2.11	0.68	 a = a
8.92	3.7	10.1	17.17	5.39	17.85	8.25	6.9	2.86	2.19	7.91	4.71	4.04	 b = b
9.94	0.14	44.16	12.8	1.46	9.53	5.08	5.7	1.81	1.32	3.34	2.09	2.64	 c = e
14.96	0.73	4.38	58.76	1.46	3.28	0.73	2.19	4.38	2.55	1.82	1.82	2.92	 d = d
8.05	0.38	11.49	20.69	13.03	4.98	10.73	6.13	2.68	3.45	6.9	9.96	1.53	 e = f
0.94	0.8	10.83	5.08	0.94	61.63	8.42	4.01	0.67	0.13	3.48	2.54	0.53	 f = i
1.39	0.84	1.67	1.67	4.18	4.18	30.08	1.39	1.95	1.95	42.06	8.64	0.0	 g = k
14.42	0.96	20.67	9.62	5.29	5.77	3.85	24.52	0.48	0.0	3.85	8.65	1.92	 h = j
12.64	0.73	9.56	28.41	4.59	9.17	7.38	4.36	4.92	1.79	8.17	5.54	2.74	 i = l
14.31	0.51	8.13	19.83	1.09	3.2	5.74	1.6	2.4	26.72	8.5	2.11	5.88	 j = o
0.94	0.24	1.09	1.5	1.32	1.05	1.88	0.22	0.47	0.41	88.37	2.3	0.21	 k = 0
3.68	1.56	2.48	10.02	4.32	5.79	18.2	4.5	4.04	0.55	19.85	24.82	0.18	 l = s
7.09	0.24	6.86	12.29	0.24	2.13	8.51	0.95	1.89	9.93	2.13	0.95	46.81	 m = u
