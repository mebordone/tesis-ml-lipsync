Timestamp:2013-07-23-03:03:44
Inventario:I3
Intervalo:6ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I3-6ms-5gramas
Num Instances:  30449
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  45 4Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.2166





=== Stratified cross-validation ===

Correctly Classified Instances       25430               83.5167 %
Incorrectly Classified Instances      5019               16.4833 %
Kappa statistic                          0.7876
K&B Relative Info Score            2260045.4193 %
K&B Information Score                59098.0616 bits      1.9409 bits/instance
Class complexity | order 0           79590.4853 bits      2.6139 bits/instance
Class complexity | scheme          1059327.3379 bits     34.7902 bits/instance
Complexity improvement     (Sf)    -979736.8527 bits    -32.1763 bits/instance
Mean absolute error                      0.0507
Root mean squared error                  0.1522
Relative absolute error                 35.721  %
Root relative squared error             57.1541 %
Coverage of cases (0.95 level)          96.8143 %
Mean rel. region size (0.95 level)      22.3378 %
Total Number of Instances            30449     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,881    0,034    0,831      0,881    0,856      0,828    0,971     0,916     A
                 0,808    0,019    0,778      0,808    0,793      0,776    0,963     0,836     E
                 0,804    0,077    0,704      0,804    0,750      0,691    0,942     0,805     CGJLNQSRT
                 0,447    0,002    0,738      0,447    0,557      0,570    0,922     0,571     FV
                 0,776    0,006    0,838      0,776    0,806      0,799    0,969     0,841     I
                 0,736    0,012    0,833      0,736    0,781      0,767    0,944     0,812     O
                 0,912    0,049    0,920      0,912    0,916      0,865    0,975     0,958     0
                 0,582    0,004    0,826      0,582    0,683      0,685    0,928     0,684     BMP
                 0,756    0,001    0,928      0,756    0,833      0,834    0,972     0,869     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,665     0,013     DZ
                 0,608    0,001    0,873      0,608    0,717      0,725    0,979     0,775     D
Weighted Avg.    0,835    0,041    0,839      0,835    0,834      0,797    0,962     0,879     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  4252    67   304     4    12    49   116     8     5     1     7 |     a = A
   107  1897   210     6    30    22    43    21     4     0     7 |     b = E
   304   150  4543    21    43    73   464    32    15     1     7 |     c = CGJLNQSRT
    35    32   141   192    11     6     7     4     0     0     2 |     d = FV
    25    81   107     7   936     3    38     9     0     0     0 |     e = I
   127    44   210     7     4  1656   168    16     8     0    11 |     f = O
   149    85   630    14    48    66 10589    18     4     0     4 |     g = 0
    59    30   181     7    26    33    69   570     4     0     1 |     h = BMP
    16    26    62     1     3    45     7    10   527     0     0 |     i = U
     1     0     9     0     0     1     1     0     0     0     0 |     j = DZ
    40    25    60     1     4    35     5     2     1     0   268 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
88.12	1.39	6.3	0.08	0.25	1.02	2.4	0.17	0.1	0.02	0.15	 a = A
4.56	80.83	8.95	0.26	1.28	0.94	1.83	0.89	0.17	0.0	0.3	 b = E
5.38	2.65	80.36	0.37	0.76	1.29	8.21	0.57	0.27	0.02	0.12	 c = CGJLNQSRT
8.14	7.44	32.79	44.65	2.56	1.4	1.63	0.93	0.0	0.0	0.47	 d = FV
2.07	6.72	8.87	0.58	77.61	0.25	3.15	0.75	0.0	0.0	0.0	 e = I
5.64	1.95	9.33	0.31	0.18	73.57	7.46	0.71	0.36	0.0	0.49	 f = O
1.28	0.73	5.43	0.12	0.41	0.57	91.23	0.16	0.03	0.0	0.03	 g = 0
6.02	3.06	18.47	0.71	2.65	3.37	7.04	58.16	0.41	0.0	0.1	 h = BMP
2.3	3.73	8.9	0.14	0.43	6.46	1.0	1.43	75.61	0.0	0.0	 i = U
8.33	0.0	75.0	0.0	0.0	8.33	8.33	0.0	0.0	0.0	0.0	 j = DZ
9.07	5.67	13.61	0.23	0.91	7.94	1.13	0.45	0.23	0.0	60.77	 k = D
