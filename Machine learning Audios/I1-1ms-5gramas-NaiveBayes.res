Timestamp:2014-12-07-10:45:55
Inventario:I1
Intervalo:1ms
Ngramas:5
Clasificador:NaiveBayes
Relation Name:  I1-1ms-5gramas
Num Instances:  183400
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  49 4F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Naive Bayes Classifier

                    Class
Attribute               a         b         e         d         f         i         k         j         l         o         0         s         u
                   (0.16)    (0.03)    (0.07)    (0.01)    (0.01)    (0.04)    (0.02)    (0.01)    (0.09)    (0.07)    (0.39)    (0.06)    (0.02)
==================================================================================================================================================
0Int(dB)
  mean             -4.1964   -12.615   -5.1696     -8.32  -15.0461   -8.7511  -24.5431  -11.4886  -12.9946   -8.1054  -47.9923  -20.9584   -5.2821
  std. dev.         9.5998   10.8018    8.0853    6.7333    8.9042    8.3577   10.3952    8.2249   10.5336   12.1344   16.4641    6.7998    7.8999
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision          0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104

0Pitch(Hz)
  mean            215.3149  158.4318  189.4232  197.3791  130.9209  172.2716   53.8407  183.3345  156.3798  178.7508   17.5051   51.4482  228.4759
  std. dev.       108.5757  112.0361   88.4732   88.9199  107.5421   77.2679   98.1135   120.816  102.9638  105.5185   57.5163   89.9388   85.8386
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184

0RawPitch
  mean            204.7929  146.4328  185.0572    190.74  105.6463  164.3931   33.0988  148.8954  141.7841  172.8748   14.3612   30.0184  219.6843
  std. dev.       115.4318  117.2396   96.0935   99.1565  104.0715   84.6206   80.3784  125.5321  110.0817  113.4727    52.147    72.303   94.3317
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214

0SmPitch
  mean            213.2799  154.1239  187.8291  196.6807   120.628  169.8395   44.2554  161.8616  152.3443  176.4732   15.8666   44.6346  227.9537
  std. dev.       110.2237  114.4166   89.7533   90.1595  104.8865   79.7321   91.3535  130.9459  104.6553  107.8241   53.8209    85.323   86.0974
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision          0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119

0Melogram(st)
  mean             51.1872   41.0599   50.2123   51.6959   36.6979   48.2326    13.886   34.0548   41.7238   46.5275    5.1312   14.8143   53.7531
  std. dev.        17.1944   24.5639    15.761   13.9284   25.1311   16.5942   24.4045   28.3874    23.465   20.5358   15.9025   24.1092   14.8416
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239

0ZCross
  mean              7.3236    2.4543    5.2651    3.9188    2.0478    3.7202    2.2412    4.2972    2.4914    4.3731     0.274    3.9419    4.0594
  std. dev.         4.7289    2.2427    3.3205    2.3249    2.2007    3.3047    4.9735    3.4714    2.2097    3.0841    1.1727    7.6418    2.9973
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175

0F1(Hz)
  mean            677.2424  370.5251   461.132  503.8882  385.4377  359.0082  109.2333  470.6224  403.3306  481.5603   56.3888  187.9087  495.1306
  std. dev.       274.8373  238.6588  150.0958  171.1258  279.0513  148.1386  216.5875   277.216  237.2995  228.3327   170.272  276.3926  311.7224
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611

0F2(Hz)
  mean           1436.7932 1303.0755 1772.6527 1448.0868 1167.0887 1952.5461   366.094 1484.2806 1313.2419 1137.1547   176.431  608.4085 1043.1641
  std. dev.       464.5841  755.1579  544.8328  390.4165  778.1683  691.2957  731.9253  791.8332  715.2172  541.7451  527.0815  858.2698  437.0492
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448

0F3(Hz)
  mean           2643.5312 2193.4003 2603.6609  2798.929 1991.6318 2630.5237  612.3396 2398.6961 2264.6823 2487.8665  301.0086 1017.6741 2649.1074
  std. dev.        829.899 1215.1125  809.9853  662.7702 1316.3866  952.1802 1183.3448 1254.5662 1207.5882 1035.9314  883.8958 1422.2254  823.2671
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646

0F4(Hz)
  mean           3067.2935 2631.0005 3138.0613 3242.6334 2379.2068 3129.2381  721.4585 2638.4948 2606.7378 2863.2558  361.5801 1158.3782 3037.3732
  std. dev.       949.2343 1426.5211  942.7734  744.0154 1537.9516 1090.9517 1399.4026 1366.3783 1372.9019  1175.482  1059.394 1615.9439  853.8923
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804

1Int(dB)
  mean             -4.2013  -12.5205    -5.156   -8.2838  -15.0484   -8.7128  -24.1869  -11.5145  -12.9556   -8.1103  -48.0284  -21.0073   -5.2051
  std. dev.         9.5927   10.7402    8.0597    6.7464    8.8828    8.3473   10.3767    8.2231   10.4883   12.1522   16.4116    6.7651    7.7835
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision          0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104

1Pitch(Hz)
  mean            215.3234  158.7298  189.7736  197.3296  130.5895  172.5882    56.543   183.123  156.7494   178.609   17.3159   50.5443   228.829
  std. dev.       108.5781  111.7609   88.2344   89.0154  107.4498   76.9615  100.1534  120.9144  102.8448  105.4051   57.2259   89.3706   85.2298
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184

1RawPitch
  mean            204.8522  146.8224  185.2743  190.7191  105.4364  164.8645   35.0221  148.6084  142.0759  172.7411   14.1972   29.1665  220.0766
  std. dev.       115.4214  117.0312   95.9379   99.1513   103.892   84.3754   82.3983  125.5889  110.0757  113.3437   51.8741    71.263   93.8332
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214

1SmPitch
  mean            213.3152  154.2915  188.2045  196.6643  120.3297  170.1487   46.6741  161.6517  152.7084  176.3386   15.6945   43.6795  228.2692
  std. dev.       110.1882  114.2373   89.4937   90.1903  104.7626   79.4942   93.6464  131.0006  104.5659  107.7071   53.5412   84.5599    85.559
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision          0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119

1Melogram(st)
  mean             51.2057   41.1472   50.2797   51.6924    36.619   48.3201   14.4496   33.9591   41.8047   46.5103    5.0851   14.5631   53.9044
  std. dev.        17.1608   24.5066   15.6601   13.9316   25.1611   16.4677   24.7579   28.4169   23.4219    20.538     15.84   23.9851   14.5585
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239

1ZCross
  mean              7.3216    2.4709    5.2602    3.9408    2.0498    3.7242    2.2858    4.2888    2.4937    4.3728    0.2697    3.9449    4.0672
  std. dev.         4.7305    2.2406    3.3214     2.323     2.208    3.2959    4.9724    3.4683     2.202    3.0812    1.1656    7.6506    2.9913
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175

1F1(Hz)
  mean            679.0141  370.2911  462.5388   504.234  383.0549  360.2928  111.2995  469.5987  402.4804  482.8394   55.8187  183.5562  496.8631
  std. dev.       273.4573  238.1729  148.1545  171.1321  278.9196  146.3199   218.155  278.2197  236.7453  227.5019  169.4384  274.3361  309.9663
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611

1F2(Hz)
  mean           1438.9064 1302.6268 1779.4393 1448.1183 1164.2707 1963.5585  371.6484 1478.1544 1313.1963 1137.9104  174.7317  595.5934 1047.9548
  std. dev.       460.8432  753.7966  536.6813  391.2488  779.7988  681.2366  734.7059  794.4483  714.9156  538.8449  524.8459  853.7624   431.069
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448

1F3(Hz)
  mean           2647.8865 2193.0228 2611.7843 2798.6728 1985.3528 2643.1271  623.2933 2391.2267 2264.7622 2492.6874  298.0271  995.5802 2662.0438
  std. dev.       823.8379 1213.2906  798.7858  665.1255 1318.4833  938.7899 1190.8091 1259.4677 1207.6983 1031.3021  879.9931 1414.1451  804.1633
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646

1F4(Hz)
  mean           3071.9596 2632.6489 3147.1235 3241.8211 2373.2424 3143.0541  734.4618 2631.2784 2606.7528 2868.4799  358.0783 1134.1654 3051.8664
  std. dev.       942.0101 1425.2945  928.5918  746.6094 1541.2239 1074.2428 1408.1112 1372.4616 1373.2037 1169.7236 1054.9195 1608.1267  828.8457
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804

2Int(dB)
  mean             -4.2125  -12.4222   -5.1527   -8.2397  -15.0454   -8.6835  -23.8179  -11.5322  -12.9091   -8.1232  -48.0653  -21.0525   -5.1335
  std. dev.         9.5923   10.6829    8.0415    6.7613    8.8688     8.344   10.3689    8.2208   10.4478   12.1734   16.3664    6.7431    7.6759
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039

2Pitch(Hz)
  mean            215.2862  159.0019  190.0442  197.3124  130.2645  172.8092   59.4252  182.9119  157.1975  178.3926   17.1392   49.7813  229.1151
  std. dev.       108.6186  111.4961   88.0362   89.0479  107.3679   76.7258  102.1778  121.0136  102.7246  105.3465   56.9512   88.8882   84.6861
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184

2RawPitch
  mean            204.8389  147.2724  185.3904  190.7376  105.2473  165.1915   37.2727  148.3685   142.425  172.5714   14.0528   28.4232  220.3812
  std. dev.       115.4661  116.8386   95.8388    99.018  103.6816   84.2064   84.7441  125.6143  110.0624  113.2081   51.6504   70.3493   93.3578
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214

2SmPitch
  mean            213.3047  154.4932  188.5286  196.6822  120.0376  170.4162   49.2977  161.5366  153.1279  176.1475   15.5187   42.8322  228.5834
  std. dev.       110.1891  114.0328   89.2681    90.157  104.6493   79.2706   95.9362    131.01  104.4742  107.6301   53.2527   83.8804   85.0153
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision          0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119

2Melogram(st)
  mean             51.2207   41.2442   50.3338   51.7211   36.5638   48.3994   15.0126   33.8636   41.8901   46.4929     5.037   14.3384   54.0407
  std. dev.        17.1322   24.4436    15.575   13.8647    25.185   16.3496   25.0926   28.4462   23.3773   20.5401   15.7745   23.8734   14.2925
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239

2ZCross
  mean              7.3168    2.4891    5.2522    3.9674    2.0494    3.7306    2.3299    4.2838    2.5002    4.3707    0.2663    3.9467    4.0724
  std. dev.          4.734    2.2391    3.3232     2.323    2.2106    3.2938    4.9682    3.4625    2.1974    3.0828     1.162    7.6555    2.9865
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175

2F1(Hz)
  mean              680.68  370.2872  463.9738  504.5848   380.668  361.4648   113.526  468.6451  401.7327  484.0118   55.2567  179.2811  498.5025
  std. dev.       272.1801  237.6445  146.1336  171.1324  278.7586  144.5804  219.7775  279.2685  236.1371  226.7963  168.6123  272.2007  308.2875
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611

2F2(Hz)
  mean           1440.7973 1302.7636 1786.3109 1448.0724 1161.6312 1973.7484  377.3839 1471.9763 1313.4915 1138.3786  173.0723  583.0802 1052.4199
  std. dev.       457.3774  752.0998  528.1199   392.072  781.5797  671.6296  737.2366  796.9819  714.4786  536.2083   522.685  849.1129  425.1574
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448

2F3(Hz)
  mean            2651.859 2193.5767 2620.1056 2798.3149 1979.1395 2654.8736  634.7581 2383.7334 2265.3607 2496.9201  295.0943  974.0677 2674.1003
  std. dev.        818.296 1210.8731  787.0306  667.4265 1320.5796  926.0878 1198.1399 1264.2791 1207.4875 1027.3116    876.14 1405.9163  785.3599
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646

2F4(Hz)
  mean           3076.1686 2635.3487  3156.326 3240.8949 2367.2839 3155.7849   748.882 2624.1464 2607.4217 2872.8522  354.6326 1110.5837 3065.6415
  std. dev.       935.4026 1423.2744  913.7122  749.1185 1544.4726 1058.3659 1418.1093 1378.5193 1373.1509 1164.6509 1050.5011 1600.1344  804.1571
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804

3Int(dB)
  mean             -4.2297  -12.3149   -5.1597   -8.1898   -15.033   -8.6607  -23.4365  -11.5452  -12.8541   -8.1449  -48.0906  -21.0861   -5.0726
  std. dev.         9.5985   10.6245    8.0314     6.778    8.8639    8.3453    10.376    8.2206   10.4125   12.1979   16.3157     6.733    7.5754
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037

3Pitch(Hz)
  mean            215.2062  159.4679   190.258  197.2987  129.9494  173.0302   62.3562  182.7014  157.6467  178.1206   16.9614   49.1772  229.3502
  std. dev.       108.6869  111.1515   87.8928   89.0771  107.2999   76.4927  104.0921  121.1126  102.5987   105.326   56.6741   88.5342   84.2123
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184

3RawPitch
  mean            204.7785  147.7852  185.5139  190.6234  104.9895  165.4701   39.8614  148.1788  142.7725   172.363   13.9206     27.74  220.5579
  std. dev.       115.5309  116.6168   95.7233   98.9787   103.475   84.0796   87.5666  125.5965  110.0182  113.1234   51.4529   69.5067   92.9298
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214

3SmPitch
  mean             213.264  154.8892  188.8112  196.7053  119.7549  170.6319   52.0187  161.4211   153.491  175.8934   15.3493   42.1596  228.8462
  std. dev.       110.2131  113.7583   89.0812   90.1193  104.5511   79.0959   98.1392  131.0196  104.4027   107.593   52.9684   83.3562   84.5411
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision          0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119

3Melogram(st)
  mean             51.2183   41.3724    50.379   51.7505   36.5095   48.4716   15.6069   33.8234   41.9866   46.4586    4.9861   14.1664     54.16
  std. dev.        17.1271   24.3658   15.5007   13.7974   25.2094   16.2405   25.4256   28.4675   23.3239   20.5573   15.7047   23.7903   14.0434
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239

3ZCross
  mean              7.3101     2.508    5.2408    3.9977    2.0498    3.7393    2.3748    4.2784    2.5113    4.3663    0.2635    3.9462    4.0771
  std. dev.         4.7386     2.237    3.3253    2.3251    2.2131    3.2999    4.9618    3.4575    2.1977     3.087    1.1594    7.6582    2.9819
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175

3F1(Hz)
  mean            682.1503  370.4332  465.3474  505.0486  378.3582  362.5437  116.1924  467.6817   401.309  485.0896   54.6968  175.0811  499.8715
  std. dev.        271.133  237.1009  144.1266  171.1736  278.5939  142.9356  221.6545   280.319  235.3747  226.1543  167.7993  269.9877  306.7882
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611

3F2(Hz)
  mean           1442.2136 1303.2526 1793.0334 1448.0931 1159.1061 1983.2786  384.4934 1465.8354 1314.8252  1138.672  171.3507  570.8828 1056.4734
  std. dev.       454.4964  750.2659  519.5885  392.9271  783.4471  662.5587  740.6138  799.4944  713.4479  533.7441  520.3742  844.3683  419.7581
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448

3F3(Hz)
  mean           2655.0761 2194.6698 2628.2257 2798.0476 1972.9814 2665.7831  648.3756 2376.2444 2267.5482 2500.7441  292.1183  953.0816 2684.8467
  std. dev.       813.9125 1208.1856  775.3306  669.7534  1322.667  914.1503 1206.4489 1269.0187 1206.1062 1023.7463  872.1937 1397.4736  768.1272
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646

3F4(Hz)
  mean           3079.4149 2638.4197  3165.364 3239.8651 2361.3224 3167.5238  766.2866 2617.0327 2610.1057 2876.6923  351.1389 1087.6567 3077.7233
  std. dev.       930.1153 1420.8135  898.8907  751.6295 1547.6942 1043.4405 1429.9774 1384.5147 1371.8413 1160.0433 1045.9904 1592.0178  781.3188
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804

4Int(dB)
  mean              -4.254  -12.2049   -5.1788   -8.1344  -15.0107    -8.647  -23.0454  -11.5529  -12.7927   -8.1757  -48.1181  -21.1123   -5.0188
  std. dev.         9.6114   10.5691    8.0296    6.7971     8.868    8.3528   10.3933    8.2207   10.3811   12.2243   16.2755    6.7379    7.4803
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037

4Pitch(Hz)
  mean            215.1081  159.9332  190.4015  197.2811  129.6994  173.2101   65.3742  182.4929  158.1051  177.8211   16.7884   48.6844  229.5826
  std. dev.       108.7696  110.8047   87.7758   89.0991  107.2959   76.2773  105.9656  121.2141   102.468  105.3227   56.4061    88.266   83.7347
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184    0.1184

4RawPitch
  mean            204.7122  148.2899  185.6605  190.4432    104.69  165.7379   42.5967  147.9941  143.1238  172.1393   13.7661   27.1754  220.7885
  std. dev.       115.5921  116.3341   95.6276   98.9727  103.2429   83.9735   90.3455  125.6089  109.9538  113.0491   51.2121   68.7975   92.5273
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214    0.1214

4SmPitch
  mean            213.2106  155.3207  189.0092  196.7284  119.5928  170.7795    54.878  161.3075  153.8621  175.6214   15.1799   41.6178  229.0387
  std. dev.       110.2529  113.4664   88.9353   90.0706  104.4955    78.963  100.3263  131.0323  104.3273  107.5646   52.6871   82.9328   84.1352
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision          0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119     0.119

4Melogram(st)
  mean             51.2091    41.496   50.4242   51.7797   36.4608   48.5268   16.2155   33.8146   42.0925   46.3993    4.9364   14.0219   54.2789
  std. dev.        17.1327   24.2847   15.4261   13.7295   25.2383   16.1496   25.7455   28.4871   23.2642   20.5969   15.6361   23.7231   13.7888
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239    0.0239

4ZCross
  mean              7.3004    2.5306    5.2275    4.0262    2.0522    3.7468    2.4163     4.274    2.5253    4.3624    0.2613    3.9463    4.0827
  std. dev.         4.7448    2.2366     3.329    2.3277    2.2158    3.3104    4.9515    3.4538    2.1995    3.0942    1.1581    7.6595    2.9766
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175    1.0175

4F1(Hz)
  mean            683.5492  370.5749   466.604  505.4969  376.1761  363.6076   119.273  466.9295    400.97   485.985   54.1467  171.1197  501.0702
  std. dev.       270.1486   236.559  142.2478  171.2455  278.4193  141.3453  223.7154  281.1013  234.5833  225.6523   167.003  267.7597  305.5336
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611    0.3611

4F2(Hz)
  mean           1443.5164 1303.9361 1799.2434  1448.146 1156.6522 1992.3968  392.6864 1460.7123 1316.3119  1138.609  169.6403  559.5041 1060.1575
  std. dev.       451.7609  748.5317  511.5475  393.8005  785.3266  653.6523  744.4085  801.4354  712.2901   531.629  518.0455  839.7476  414.9703
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448    0.4448

4F3(Hz)
  mean           2658.1212 2195.7732 2635.6414 2797.7777 1966.8613 2676.2267   664.142 2370.3101 2270.1065 2503.6829  289.1712  933.5574 2694.2989
  std. dev.       809.8058 1205.5074   764.297  672.0457 1324.7402   902.456 1215.6316 1272.7594 1204.5273 1021.0061  868.2313 1389.3791   752.613
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646    0.6646

4F4(Hz)
  mean           3082.4058 2641.4866 3173.7603 3238.8126 2355.3511 3178.8998  786.5555 2611.1612 2613.1847 2879.5448  347.6842 1066.2635 3088.1777
  std. dev.       925.1344 1418.3454  884.9534    754.11 1550.8738 1028.8621 1443.3441 1389.1675 1370.2678 1156.4562 1041.4905 1584.0832  760.6201
  weight sum         28557      5814     13740      2682      2561      7083      3555      2062     17292     13234     72113     10570      4137
  precision         0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804    0.6804





=== Stratified cross-validation ===

Correctly Classified Instances       93922               51.2116 %
Incorrectly Classified Instances     89478               48.7884 %
Kappa statistic                          0.3882
K&B Relative Info Score            6624045.532  %
K&B Information Score               190804.2541 bits      1.0404 bits/instance
Class complexity | order 0          528255.3218 bits      2.8803 bits/instance
Class complexity | scheme          5144146.698  bits     28.0488 bits/instance
Complexity improvement     (Sf)    -4615891.3761 bits    -25.1684 bits/instance
Mean absolute error                      0.0757
Root mean squared error                  0.2584
Relative absolute error                 61.9692 %
Root relative squared error            104.5287 %
Coverage of cases (0.95 level)          56.6494 %
Mean rel. region size (0.95 level)      10.8498 %
Total Number of Instances           183400     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,429    0,035    0,695      0,429    0,531      0,485    0,883     0,612     a
                 0,004    0,003    0,037      0,004    0,007      0,002    0,724     0,058     b
                 0,533    0,067    0,391      0,533    0,451      0,405    0,868     0,406     e
                 0,619    0,185    0,047      0,619    0,088      0,132    0,814     0,048     d
                 0,065    0,024    0,037      0,065    0,047      0,031    0,731     0,030     f
                 0,411    0,038    0,304      0,411    0,350      0,324    0,865     0,276     i
                 0,123    0,013    0,154      0,123    0,137      0,122    0,837     0,092     k
                 0,112    0,003    0,277      0,112    0,159      0,170    0,750     0,075     j
                 0,045    0,016    0,219      0,045    0,074      0,060    0,748     0,181     l
                 0,003    0,001    0,183      0,003    0,006      0,014    0,780     0,183     o
                 0,889    0,138    0,807      0,889    0,846      0,741    0,947     0,938     0
                 0,208    0,013    0,499      0,208    0,294      0,297    0,887     0,370     s
                 0,435    0,024    0,294      0,435    0,351      0,340    0,872     0,350     u
Weighted Avg.    0,512    0,072    0,544      0,512    0,501      0,448    0,876     0,570     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 12251    92  3399  7904   603   551   257   211   384    75  1482   187  1161 |     a = a
   344    21   385  2082   251   920   124    44   131    10  1054   121   327 |     b = b
  1157   104  7317  2736   286   939   189    69   181    15   523    91   133 |     c = e
   232     0   261  1659     7    87     0     0   133     0    98    40   165 |     d = d
   157     0   198   902   166   252    79    15    86     0   632    59    15 |     e = f
   313    77  2469   383   240  2913   146     3    91     1   363    82     2 |     f = i
    34    47    70   188   155   127   437    36    95     8  1920   406    32 |     g = k
   236     0   360   298   117   194    47   230    63     1   328    38   150 |     h = j
   811    70  1132  8121   684  1573   537    29   770    10  2735   393   427 |     i = l
  1302    81  2041  5440   445   130    89    46   395    37  1424   181  1623 |     j = o
   186    29   740  3182   873  1139   322    43   648    16 64120   583   232 |     k = 0
   258    16   154   871   610   647   546   102   382    17  4717  2203    47 |     l = s
   342    26   174  1299    73    98    62     3   150    12    66    34  1798 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
42.9	0.32	11.9	27.68	2.11	1.93	0.9	0.74	1.34	0.26	5.19	0.65	4.07	 a = a
5.92	0.36	6.62	35.81	4.32	15.82	2.13	0.76	2.25	0.17	18.13	2.08	5.62	 b = b
8.42	0.76	53.25	19.91	2.08	6.83	1.38	0.5	1.32	0.11	3.81	0.66	0.97	 c = e
8.65	0.0	9.73	61.86	0.26	3.24	0.0	0.0	4.96	0.0	3.65	1.49	6.15	 d = d
6.13	0.0	7.73	35.22	6.48	9.84	3.08	0.59	3.36	0.0	24.68	2.3	0.59	 e = f
4.42	1.09	34.86	5.41	3.39	41.13	2.06	0.04	1.28	0.01	5.12	1.16	0.03	 f = i
0.96	1.32	1.97	5.29	4.36	3.57	12.29	1.01	2.67	0.23	54.01	11.42	0.9	 g = k
11.45	0.0	17.46	14.45	5.67	9.41	2.28	11.15	3.06	0.05	15.91	1.84	7.27	 h = j
4.69	0.4	6.55	46.96	3.96	9.1	3.11	0.17	4.45	0.06	15.82	2.27	2.47	 i = l
9.84	0.61	15.42	41.11	3.36	0.98	0.67	0.35	2.98	0.28	10.76	1.37	12.26	 j = o
0.26	0.04	1.03	4.41	1.21	1.58	0.45	0.06	0.9	0.02	88.92	0.81	0.32	 k = 0
2.44	0.15	1.46	8.24	5.77	6.12	5.17	0.96	3.61	0.16	44.63	20.84	0.44	 l = s
8.27	0.63	4.21	31.4	1.76	2.37	1.5	0.07	3.63	0.29	1.6	0.82	43.46	 m = u
