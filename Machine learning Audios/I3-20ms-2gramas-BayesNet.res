Timestamp:2013-08-30-05:06:15
Inventario:I3
Intervalo:20ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I3-20ms-2gramas
Num Instances:  9128
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(4): class 
0F1(Hz)(9): class 
0F2(Hz)(8): class 
0F3(Hz)(5): class 
0F4(Hz)(3): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(8): class 
1F1(Hz)(9): class 
1F2(Hz)(9): class 
1F3(Hz)(5): class 
1F4(Hz)(6): class 
class(11): 
LogScore Bayes: -186716.84436010296
LogScore BDeu: -191262.914386848
LogScore MDL: -191029.38320428767
LogScore ENTROPY: -185968.2816544818
LogScore AIC: -187078.2816544818




=== Stratified cross-validation ===

Correctly Classified Instances        5258               57.603  %
Incorrectly Classified Instances      3870               42.397  %
Kappa statistic                          0.468 
K&B Relative Info Score             436813.8579 %
K&B Information Score                11665.5673 bits      1.278  bits/instance
Class complexity | order 0           24350.7576 bits      2.6677 bits/instance
Class complexity | scheme            56665.9174 bits      6.2079 bits/instance
Complexity improvement     (Sf)     -32315.1598 bits     -3.5402 bits/instance
Mean absolute error                      0.0792
Root mean squared error                  0.2496
Relative absolute error                 54.8693 %
Root relative squared error             92.9381 %
Coverage of cases (0.95 level)          69.9167 %
Mean rel. region size (0.95 level)      17.3552 %
Total Number of Instances             9128     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,636    0,079    0,612      0,636    0,624      0,548    0,887     0,686     A
                 0,535    0,071    0,400      0,535    0,458      0,407    0,877     0,444     E
                 0,189    0,056    0,444      0,189    0,265      0,191    0,769     0,386     CGJLNQSRT
                 0,065    0,014    0,065      0,065    0,065      0,051    0,759     0,040     FV
                 0,587    0,046    0,362      0,587    0,448      0,431    0,908     0,427     I
                 0,256    0,021    0,504      0,256    0,339      0,322    0,816     0,360     O
                 0,927    0,111    0,821      0,927    0,871      0,797    0,971     0,959     0
                 0,010    0,005    0,065      0,010    0,017      0,013    0,762     0,093     BMP
                 0,436    0,021    0,342      0,436    0,383      0,369    0,899     0,421     U
                 0,000    0,004    0,000      0,000    0,000      -0,001   0,925     0,004     DZ
                 0,414    0,069    0,081      0,414    0,135      0,157    0,852     0,077     D
Weighted Avg.    0,576    0,075    0,577      0,576    0,558      0,498    0,883     0,624     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
  957  124  106   27   43   53   67    3   14    3  108 |    a = A
   90  399   62    6   61   10   25    4   21    2   66 |    b = E
  239  199  331   47  167   47  422   19   59    8  213 |    c = CGJLNQSRT
   23   23   18    9    5    9   17    2    1    5   26 |    d = FV
    8   73   41    1  226    2   19    0    5    0   10 |    e = I
  125   73   55   10   23  182   65    6   52    4  117 |    f = O
   37   16   76   29   30   15 3001    4    3   10   15 |    g = 0
   36   56   33    7   58    8   33    3   19    2   44 |    h = BMP
   19   20   16    1    5   27    4    4   95    1   26 |    i = U
    0    0    1    0    0    0    4    0    0    0    0 |    j = DZ
   29   15    7    2    7    8    0    1    9    0   55 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
63.59	8.24	7.04	1.79	2.86	3.52	4.45	0.2	0.93	0.2	7.18	 a = A
12.06	53.49	8.31	0.8	8.18	1.34	3.35	0.54	2.82	0.27	8.85	 b = E
13.65	11.36	18.9	2.68	9.54	2.68	24.1	1.09	3.37	0.46	12.16	 c = CGJLNQSRT
16.67	16.67	13.04	6.52	3.62	6.52	12.32	1.45	0.72	3.62	18.84	 d = FV
2.08	18.96	10.65	0.26	58.7	0.52	4.94	0.0	1.3	0.0	2.6	 e = I
17.56	10.25	7.72	1.4	3.23	25.56	9.13	0.84	7.3	0.56	16.43	 f = O
1.14	0.49	2.35	0.9	0.93	0.46	92.74	0.12	0.09	0.31	0.46	 g = 0
12.04	18.73	11.04	2.34	19.4	2.68	11.04	1.0	6.35	0.67	14.72	 h = BMP
8.72	9.17	7.34	0.46	2.29	12.39	1.83	1.83	43.58	0.46	11.93	 i = U
0.0	0.0	20.0	0.0	0.0	0.0	80.0	0.0	0.0	0.0	0.0	 j = DZ
21.8	11.28	5.26	1.5	5.26	6.02	0.0	0.75	6.77	0.0	41.35	 k = D
