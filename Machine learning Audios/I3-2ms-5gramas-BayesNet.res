Timestamp:2013-08-30-04:58:52
Inventario:I3
Intervalo:2ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I3-2ms-5gramas
Num Instances:  91699
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(25): class 
0Pitch(Hz)(47): class 
0RawPitch(33): class 
0SmPitch(38): class 
0Melogram(st)(41): class 
0ZCross(18): class 
0F1(Hz)(1438): class 
0F2(Hz)(1756): class 
0F3(Hz)(2143): class 
0F4(Hz)(2115): class 
1Int(dB)(23): class 
1Pitch(Hz)(47): class 
1RawPitch(33): class 
1SmPitch(38): class 
1Melogram(st)(41): class 
1ZCross(17): class 
1F1(Hz)(1340): class 
1F2(Hz)(1805): class 
1F3(Hz)(2117): class 
1F4(Hz)(2165): class 
2Int(dB)(24): class 
2Pitch(Hz)(48): class 
2RawPitch(35): class 
2SmPitch(40): class 
2Melogram(st)(41): class 
2ZCross(17): class 
2F1(Hz)(1307): class 
2F2(Hz)(1893): class 
2F3(Hz)(2235): class 
2F4(Hz)(2060): class 
3Int(dB)(22): class 
3Pitch(Hz)(46): class 
3RawPitch(34): class 
3SmPitch(35): class 
3Melogram(st)(45): class 
3ZCross(17): class 
3F1(Hz)(1379): class 
3F2(Hz)(1939): class 
3F3(Hz)(2284): class 
3F4(Hz)(2125): class 
4Int(dB)(25): class 
4Pitch(Hz)(48): class 
4RawPitch(32): class 
4SmPitch(39): class 
4Melogram(st)(45): class 
4ZCross(17): class 
4F1(Hz)(1291): class 
4F2(Hz)(1920): class 
4F3(Hz)(2248): class 
4F4(Hz)(2101): class 
class(11): 
LogScore Bayes: -1.2404642415142782E7
LogScore BDeu: -1.7574890959453747E7
LogScore MDL: -1.6266804150955787E7
LogScore ENTROPY: -1.3839568009670336E7
LogScore AIC: -1.4264420009670336E7




=== Stratified cross-validation ===

Correctly Classified Instances       69223               75.4894 %
Incorrectly Classified Instances     22476               24.5106 %
Kappa statistic                          0.6829
K&B Relative Info Score            6562745.6349 %
K&B Information Score               170290.8763 bits      1.8571 bits/instance
Class complexity | order 0          237910.9549 bits      2.5945 bits/instance
Class complexity | scheme          1175948.8007 bits     12.824  bits/instance
Complexity improvement     (Sf)    -938037.8458 bits    -10.2295 bits/instance
Mean absolute error                      0.0447
Root mean squared error                  0.2063
Relative absolute error                 31.6956 %
Root relative squared error             77.7134 %
Coverage of cases (0.95 level)          77.4436 %
Mean rel. region size (0.95 level)       9.7728 %
Total Number of Instances            91699     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,814    0,021    0,878      0,814    0,845      0,818    0,957     0,902     A
                 0,844    0,030    0,696      0,844    0,763      0,746    0,959     0,848     E
                 0,328    0,020    0,788      0,328    0,464      0,450    0,936     0,737     CGJLNQSRT
                 0,626    0,009    0,505      0,626    0,559      0,556    0,909     0,613     FV
                 0,854    0,017    0,671      0,854    0,752      0,746    0,957     0,836     I
                 0,763    0,012    0,827      0,763    0,794      0,779    0,925     0,827     O
                 0,899    0,155    0,788      0,899    0,840      0,730    0,959     0,929     0
                 0,677    0,008    0,743      0,677    0,708      0,700    0,902     0,718     BMP
                 0,913    0,010    0,681      0,913    0,780      0,783    0,976     0,911     U
                 0,000    0,005    0,000      0,000    0,000      -0,001   0,858     0,001     DZ
                 0,921    0,031    0,303      0,921    0,456      0,518    0,983     0,793     D
Weighted Avg.    0,755    0,072    0,778      0,755    0,744      0,692    0,950     0,858     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 11666   316   378   152   158   235   784    92    79    86   387 |     a = A
    96  5835   240    66    99    20   292    64    24    34   140 |     b = E
   840  1374  5516   123   733   344  5724   252   544    26  1317 |     c = CGJLNQSRT
    19    34    28   805    28    12   315     7     6     0    32 |     d = FV
    13    69   122    20  3041     4   199    24     2    43    24 |     e = I
   144   159   114    14    34  5068   723    44    29    41   276 |     f = O
   435   488   440   385   379   396 32211   182   159   211   543 |     g = 0
    48    78    83     6    54    30   547  1972    48     3    46 |     h = BMP
     3    11    65    11     4     3    48    16  1900     3    16 |     i = U
     0     0     0     0     0     0    33     0     0     0     0 |     j = DZ
    17    22    10    11     2    14    13     2     0    13  1209 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
81.39	2.2	2.64	1.06	1.1	1.64	5.47	0.64	0.55	0.6	2.7	 a = A
1.39	84.44	3.47	0.96	1.43	0.29	4.23	0.93	0.35	0.49	2.03	 b = E
5.0	8.18	32.85	0.73	4.36	2.05	34.09	1.5	3.24	0.15	7.84	 c = CGJLNQSRT
1.48	2.64	2.18	62.6	2.18	0.93	24.49	0.54	0.47	0.0	2.49	 d = FV
0.37	1.94	3.43	0.56	85.4	0.11	5.59	0.67	0.06	1.21	0.67	 e = I
2.17	2.39	1.72	0.21	0.51	76.26	10.88	0.66	0.44	0.62	4.15	 f = O
1.21	1.36	1.23	1.07	1.06	1.11	89.9	0.51	0.44	0.59	1.52	 g = 0
1.65	2.68	2.85	0.21	1.85	1.03	18.77	67.65	1.65	0.1	1.58	 h = BMP
0.14	0.53	3.13	0.53	0.19	0.14	2.31	0.77	91.35	0.14	0.77	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
1.29	1.68	0.76	0.84	0.15	1.07	0.99	0.15	0.0	0.99	92.08	 k = D
