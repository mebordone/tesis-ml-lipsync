Timestamp:2013-07-22-21:58:03
Inventario:I2
Intervalo:15ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I2-15ms-5gramas
Num Instances:  12185
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  42 4Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  43 4RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  44 4SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  45 4Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  47 4F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  48 4F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  49 4F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  50 4F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3474





=== Stratified cross-validation ===

Correctly Classified Instances        8829               72.4579 %
Incorrectly Classified Instances      3356               27.5421 %
Kappa statistic                          0.6574
K&B Relative Info Score             758752.8528 %
K&B Information Score                22225.3638 bits      1.824  bits/instance
Class complexity | order 0           35674.7455 bits      2.9278 bits/instance
Class complexity | scheme           798954.8354 bits     65.5687 bits/instance
Complexity improvement     (Sf)    -763280.0899 bits    -62.641  bits/instance
Mean absolute error                      0.0658
Root mean squared error                  0.1793
Relative absolute error                 48.5916 %
Root relative squared error             68.9288 %
Coverage of cases (0.95 level)          93.9762 %
Mean rel. region size (0.95 level)      26.7911 %
Total Number of Instances            12185     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,829    0,068    0,703      0,829    0,761      0,713    0,945     0,845     A
                 0,348    0,027    0,400      0,348    0,372      0,343    0,832     0,299     LGJKC
                 0,692    0,043    0,582      0,692    0,632      0,600    0,927     0,648     E
                 0,192    0,005    0,362      0,192    0,251      0,256    0,830     0,218     FV
                 0,618    0,015    0,641      0,618    0,630      0,614    0,940     0,645     I
                 0,584    0,029    0,625      0,584    0,604      0,572    0,907     0,622     O
                 0,577    0,054    0,542      0,577    0,559      0,509    0,888     0,548     SNRTY
                 0,931    0,054    0,907      0,931    0,919      0,872    0,978     0,959     0
                 0,336    0,007    0,629      0,336    0,438      0,447    0,878     0,393     BMP
                 0,236    0,003    0,581      0,236    0,336      0,365    0,881     0,289     DZ
                 0,561    0,004    0,777      0,561    0,652      0,654    0,928     0,662     U
                 0,503    0,011    0,658      0,503    0,570      0,559    0,919     0,527     SNRTXY
Weighted Avg.    0,725    0,045    0,719      0,725    0,716      0,680    0,935     0,739     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1648   34   76    4   13   58   65   56    9    5    2   19 |    a = A
   91  208   60    7   25   26   90   67    4    2    3   15 |    b = LGJKC
  101   42  672    2   29   35   46   16    6    4    3   15 |    c = E
   26   13   25   34    5   12   43    7    7    0    0    5 |    d = FV
   20   17   83    4  311    6   28   16    9    0    2    7 |    e = I
  133   27   50    3   11  549   42   71   15   11   11   17 |    f = O
  128   69   64   13   27   49  703  134   10    1    5   15 |    g = SNRTY
   50   36   21    6   18   26  135 4111    4    0    1    6 |    h = 0
   35   23   27    9   30   29   55   32  134    1   11   13 |    i = BMP
   34    9   21    6    2   32   13    3    1   43    2   16 |    j = DZ
   25   11   12    0    7   43   15    1    5    1  160    5 |    k = U
   53   31   44    6    7   13   61   17    9    6    6  256 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
82.86	1.71	3.82	0.2	0.65	2.92	3.27	2.82	0.45	0.25	0.1	0.96	 a = A
15.22	34.78	10.03	1.17	4.18	4.35	15.05	11.2	0.67	0.33	0.5	2.51	 b = LGJKC
10.4	4.33	69.21	0.21	2.99	3.6	4.74	1.65	0.62	0.41	0.31	1.54	 c = E
14.69	7.34	14.12	19.21	2.82	6.78	24.29	3.95	3.95	0.0	0.0	2.82	 d = FV
3.98	3.38	16.5	0.8	61.83	1.19	5.57	3.18	1.79	0.0	0.4	1.39	 e = I
14.15	2.87	5.32	0.32	1.17	58.4	4.47	7.55	1.6	1.17	1.17	1.81	 f = O
10.51	5.67	5.25	1.07	2.22	4.02	57.72	11.0	0.82	0.08	0.41	1.23	 g = SNRTY
1.13	0.82	0.48	0.14	0.41	0.59	3.06	93.14	0.09	0.0	0.02	0.14	 h = 0
8.77	5.76	6.77	2.26	7.52	7.27	13.78	8.02	33.58	0.25	2.76	3.26	 i = BMP
18.68	4.95	11.54	3.3	1.1	17.58	7.14	1.65	0.55	23.63	1.1	8.79	 j = DZ
8.77	3.86	4.21	0.0	2.46	15.09	5.26	0.35	1.75	0.35	56.14	1.75	 k = U
10.41	6.09	8.64	1.18	1.38	2.55	11.98	3.34	1.77	1.18	1.18	50.29	 l = SNRTXY
