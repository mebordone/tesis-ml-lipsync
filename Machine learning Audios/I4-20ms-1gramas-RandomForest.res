Timestamp:2013-07-22-23:07:14
Inventario:I4
Intervalo:20ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I4-20ms-1gramas
Num Instances:  9129
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.3604





=== Stratified cross-validation ===

Correctly Classified Instances        6199               67.9045 %
Incorrectly Classified Instances      2930               32.0955 %
Kappa statistic                          0.5879
K&B Relative Info Score             515827.6675 %
K&B Information Score                13499.0886 bits      1.4787 bits/instance
Class complexity | order 0           23873.9636 bits      2.6152 bits/instance
Class complexity | scheme           948080.3982 bits    103.8537 bits/instance
Complexity improvement     (Sf)    -924206.4346 bits   -101.2385 bits/instance
Mean absolute error                      0.0795
Root mean squared error                  0.2085
Relative absolute error                 50.3952 %
Root relative squared error             74.2526 %
Coverage of cases (0.95 level)          90.2508 %
Mean rel. region size (0.95 level)      27.2034 %
Total Number of Instances             9129     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,217    0,003    0,302      0,217    0,252      0,252    0,843     0,174     C
                 0,588    0,047    0,529      0,588    0,557      0,516    0,896     0,522     E
                 0,094    0,007    0,173      0,094    0,122      0,118    0,687     0,059     FV
                 0,738    0,102    0,653      0,738    0,693      0,609    0,899     0,762     AI
                 0,546    0,112    0,488      0,546    0,516      0,416    0,841     0,539     CDGKNRSYZ
                 0,503    0,031    0,580      0,503    0,539      0,504    0,852     0,532     O
                 0,914    0,059    0,895      0,914    0,904      0,851    0,970     0,947     0
                 0,110    0,013    0,242      0,110    0,151      0,142    0,727     0,121     LT
                 0,495    0,005    0,711      0,495    0,584      0,585    0,915     0,602     U
                 0,207    0,010    0,416      0,207    0,277      0,277    0,765     0,220     MBP
Weighted Avg.    0,679    0,068    0,665      0,679    0,668      0,609    0,897     0,694     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   13   14    1   18    8    5    0    0    1    0 |    a = C
    5  439    6  122  116   27   13    8    1    9 |    b = E
    1   16   13   29   54   10    7    4    1    3 |    c = FV
    6  144    8 1395  174   50   65   24    6   18 |    d = AI
   11  104   25  264  815   69  137   34   11   22 |    e = CDGKNRSYZ
    3   40    4  103  119  358   51    9   16    9 |    f = O
    0   17    7   59  143   27 2959   15    0   10 |    g = 0
    1   30    6   71  124   16   46   37    2    4 |    h = LT
    3    8    1   16   35   29    1    5  108   12 |    i = U
    0   18    4   58   81   26   27   17    6   62 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
21.67	23.33	1.67	30.0	13.33	8.33	0.0	0.0	1.67	0.0	 a = C
0.67	58.85	0.8	16.35	15.55	3.62	1.74	1.07	0.13	1.21	 b = E
0.72	11.59	9.42	21.01	39.13	7.25	5.07	2.9	0.72	2.17	 c = FV
0.32	7.62	0.42	73.81	9.21	2.65	3.44	1.27	0.32	0.95	 d = AI
0.74	6.97	1.68	17.69	54.62	4.62	9.18	2.28	0.74	1.47	 e = CDGKNRSYZ
0.42	5.62	0.56	14.47	16.71	50.28	7.16	1.26	2.25	1.26	 f = O
0.0	0.53	0.22	1.82	4.42	0.83	91.41	0.46	0.0	0.31	 g = 0
0.3	8.9	1.78	21.07	36.8	4.75	13.65	10.98	0.59	1.19	 h = LT
1.38	3.67	0.46	7.34	16.06	13.3	0.46	2.29	49.54	5.5	 i = U
0.0	6.02	1.34	19.4	27.09	8.7	9.03	5.69	2.01	20.74	 j = MBP
