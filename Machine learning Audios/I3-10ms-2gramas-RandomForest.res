Timestamp:2013-07-22-22:29:55
Inventario:I3
Intervalo:10ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I3-10ms-2gramas
Num Instances:  18295
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.287





=== Stratified cross-validation ===

Correctly Classified Instances       14003               76.54   %
Incorrectly Classified Instances      4292               23.46   %
Kappa statistic                          0.6994
K&B Relative Info Score            1233786.0251 %
K&B Information Score                32505.852  bits      1.7768 bits/instance
Class complexity | order 0           48175.3419 bits      2.6333 bits/instance
Class complexity | scheme          1128133.8093 bits     61.6635 bits/instance
Complexity improvement     (Sf)    -1079958.4674 bits    -59.0303 bits/instance
Mean absolute error                      0.0596
Root mean squared error                  0.1748
Relative absolute error                 41.7084 %
Root relative squared error             65.4287 %
Coverage of cases (0.95 level)          94.2881 %
Mean rel. region size (0.95 level)      22.7881 %
Total Number of Instances            18295     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,831    0,048    0,769      0,831    0,799      0,759    0,950     0,863     A
                 0,723    0,033    0,654      0,723    0,687      0,660    0,939     0,712     E
                 0,696    0,103    0,609      0,696    0,650      0,564    0,893     0,664     CGJLNQSRT
                 0,226    0,002    0,573      0,226    0,324      0,354    0,832     0,263     FV
                 0,647    0,010    0,726      0,647    0,684      0,673    0,942     0,711     I
                 0,651    0,023    0,693      0,651    0,671      0,646    0,916     0,697     O
                 0,894    0,055    0,906      0,894    0,900      0,841    0,968     0,944     0
                 0,396    0,008    0,639      0,396    0,489      0,490    0,863     0,438     BMP
                 0,657    0,002    0,874      0,657    0,750      0,753    0,949     0,776     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,624     0,010     DZ
                 0,353    0,002    0,691      0,353    0,468      0,489    0,908     0,472     D
Weighted Avg.    0,765    0,053    0,767      0,765    0,762      0,714    0,937     0,795     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 2444   89  248    2   13   55   77    5    4    0    3 |    a = A
   93 1039  190    7   34   29   23   14    3    1    5 |    b = E
  297  176 2397   17   58  109  328   43    8    2    8 |    c = CGJLNQSRT
   22   30  110   59    4   11   14    6    5    0    0 |    d = FV
   19   87  106    4  484    2   29   15    2    0    0 |    e = I
  109   49  167    1    6  896  108   18    6    0   17 |    f = O
  100   49  429    7   33   76 6077   20    2    0    4 |    g = 0
   45   43  150    6   30   32   44  235    6    0    3 |    h = BMP
    9   10   64    0    2   42    5   11  278    0    2 |    i = U
    1    0    5    0    0    0    2    0    0    0    0 |    j = DZ
   39   16   67    0    3   40    2    1    4    0   94 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
83.13	3.03	8.44	0.07	0.44	1.87	2.62	0.17	0.14	0.0	0.1	 a = A
6.47	72.25	13.21	0.49	2.36	2.02	1.6	0.97	0.21	0.07	0.35	 b = E
8.63	5.11	69.62	0.49	1.68	3.17	9.53	1.25	0.23	0.06	0.23	 c = CGJLNQSRT
8.43	11.49	42.15	22.61	1.53	4.21	5.36	2.3	1.92	0.0	0.0	 d = FV
2.54	11.63	14.17	0.53	64.71	0.27	3.88	2.01	0.27	0.0	0.0	 e = I
7.92	3.56	12.13	0.07	0.44	65.07	7.84	1.31	0.44	0.0	1.23	 f = O
1.47	0.72	6.31	0.1	0.49	1.12	89.41	0.29	0.03	0.0	0.06	 g = 0
7.58	7.24	25.25	1.01	5.05	5.39	7.41	39.56	1.01	0.0	0.51	 h = BMP
2.13	2.36	15.13	0.0	0.47	9.93	1.18	2.6	65.72	0.0	0.47	 i = U
12.5	0.0	62.5	0.0	0.0	0.0	25.0	0.0	0.0	0.0	0.0	 j = DZ
14.66	6.02	25.19	0.0	1.13	15.04	0.75	0.38	1.5	0.0	35.34	 k = D
