Timestamp:2013-08-30-05:27:25
Inventario:I4
Intervalo:6ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I4-6ms-3gramas
Num Instances:  30451
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(14): class 
0Pitch(Hz)(10): class 
0RawPitch(11): class 
0SmPitch(10): class 
0Melogram(st)(14): class 
0ZCross(12): class 
0F1(Hz)(21): class 
0F2(Hz)(15): class 
0F3(Hz)(16): class 
0F4(Hz)(14): class 
1Int(dB)(14): class 
1Pitch(Hz)(10): class 
1RawPitch(10): class 
1SmPitch(10): class 
1Melogram(st)(11): class 
1ZCross(13): class 
1F1(Hz)(20): class 
1F2(Hz)(17): class 
1F3(Hz)(14): class 
1F4(Hz)(16): class 
2Int(dB)(16): class 
2Pitch(Hz)(11): class 
2RawPitch(12): class 
2SmPitch(11): class 
2Melogram(st)(14): class 
2ZCross(13): class 
2F1(Hz)(23): class 
2F2(Hz)(18): class 
2F3(Hz)(13): class 
2F4(Hz)(12): class 
class(10): 
LogScore Bayes: -1398618.6960149624
LogScore BDeu: -1418131.2959758476
LogScore MDL: -1416031.4096274585
LogScore ENTROPY: -1396111.4945258053
LogScore AIC: -1399970.494525805




=== Stratified cross-validation ===

Correctly Classified Instances       17695               58.1098 %
Incorrectly Classified Instances     12756               41.8902 %
Kappa statistic                          0.4583
K&B Relative Info Score            1402687.4691 %
K&B Information Score                36014.9271 bits      1.1827 bits/instance
Class complexity | order 0           78167.8173 bits      2.567  bits/instance
Class complexity | scheme           310721.6537 bits     10.204  bits/instance
Complexity improvement     (Sf)    -232553.8364 bits     -7.637  bits/instance
Mean absolute error                      0.0847
Root mean squared error                  0.2713
Relative absolute error                 54.5444 %
Root relative squared error             97.3797 %
Coverage of cases (0.95 level)          65.1407 %
Mean rel. region size (0.95 level)      14.258  %
Total Number of Instances            30451     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,476    0,033    0,081      0,476    0,138      0,186    0,896     0,134     C
                 0,635    0,097    0,353      0,635    0,454      0,416    0,879     0,447     E
                 0,158    0,018    0,113      0,158    0,132      0,119    0,800     0,078     FV
                 0,554    0,067    0,672      0,554    0,607      0,525    0,878     0,698     AI
                 0,142    0,040    0,397      0,142    0,209      0,160    0,794     0,365     CDGKNRSYZ
                 0,408    0,041    0,444      0,408    0,425      0,382    0,841     0,417     O
                 0,904    0,151    0,786      0,904    0,841      0,736    0,958     0,952     0
                 0,045    0,012    0,120      0,045    0,065      0,052    0,767     0,092     LT
                 0,557    0,029    0,312      0,557    0,400      0,399    0,926     0,487     U
                 0,186    0,021    0,229      0,186    0,205      0,183    0,793     0,144     MBP
Weighted Avg.    0,581    0,090    0,577      0,581    0,561      0,490    0,886     0,645     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
    88    53     0    21     8     3     9     0     0     3 |     a = C
   166  1491    35   216   111    76   103    45    52    52 |     b = E
    32    84    68    35    32    39    98    15    10    17 |     c = FV
   256   954    91  3339   322   329   324    87   142   187 |     d = AI
   267   717   169   614   681   310  1539   107   216   182 |     e = CDGKNRSYZ
   103   259    20   335   115   918   238    28   215    20 |     f = O
    32   227   113   217   247   149 10490    36    42    56 |     g = 0
    29   214    38    73   117   119   357    50    60    62 |     h = LT
    37    64    16    35    17    73    17    16   388    34 |     i = U
    76   155    51    84    65    51   165    31   120   182 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
47.57	28.65	0.0	11.35	4.32	1.62	4.86	0.0	0.0	1.62	 a = C
7.07	63.53	1.49	9.2	4.73	3.24	4.39	1.92	2.22	2.22	 b = E
7.44	19.53	15.81	8.14	7.44	9.07	22.79	3.49	2.33	3.95	 c = FV
4.24	15.82	1.51	55.36	5.34	5.46	5.37	1.44	2.35	3.1	 d = AI
5.56	14.93	3.52	12.79	14.18	6.46	32.05	2.23	4.5	3.79	 e = CDGKNRSYZ
4.58	11.51	0.89	14.88	5.11	40.78	10.57	1.24	9.55	0.89	 f = O
0.28	1.96	0.97	1.87	2.13	1.28	90.36	0.31	0.36	0.48	 g = 0
2.59	19.12	3.4	6.52	10.46	10.63	31.9	4.47	5.36	5.54	 h = LT
5.31	9.18	2.3	5.02	2.44	10.47	2.44	2.3	55.67	4.88	 i = U
7.76	15.82	5.2	8.57	6.63	5.2	16.84	3.16	12.24	18.57	 j = MBP
