Timestamp:2013-08-30-04:18:26
Inventario:I1
Intervalo:15ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I1-15ms-3gramas
Num Instances:  12187
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(5): class 
0F1(Hz)(9): class 
0F2(Hz)(8): class 
0F3(Hz)(7): class 
0F4(Hz)(6): class 
1Int(dB)(9): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(8): class 
1F1(Hz)(9): class 
1F2(Hz)(9): class 
1F3(Hz)(7): class 
1F4(Hz)(6): class 
2Int(dB)(9): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(9): class 
2F1(Hz)(11): class 
2F2(Hz)(10): class 
2F3(Hz)(7): class 
2F4(Hz)(7): class 
class(13): 
LogScore Bayes: -391435.3412584836
LogScore BDeu: -401337.46921854815
LogScore MDL: -400264.117275692
LogScore ENTROPY: -389689.3846758544
LogScore AIC: -391937.38467585447




=== Stratified cross-validation ===

Correctly Classified Instances        6832               56.0597 %
Incorrectly Classified Instances      5355               43.9403 %
Kappa statistic                          0.4615
K&B Relative Info Score             546221.8246 %
K&B Information Score                16126.0282 bits      1.3232 bits/instance
Class complexity | order 0           35955.7145 bits      2.9503 bits/instance
Class complexity | scheme           110825.6059 bits      9.0938 bits/instance
Complexity improvement     (Sf)     -74869.8914 bits     -6.1434 bits/instance
Mean absolute error                      0.0683
Root mean squared error                  0.2367
Relative absolute error                 54.6829 %
Root relative squared error             94.7016 %
Coverage of cases (0.95 level)          67.2848 %
Mean rel. region size (0.95 level)      13.0688 %
Total Number of Instances            12187     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,589    0,063    0,644      0,589    0,616      0,545    0,888     0,695     a
                 0,018    0,004    0,127      0,018    0,031      0,036    0,761     0,094     b
                 0,488    0,059    0,416      0,488    0,449      0,399    0,872     0,425     e
                 0,484    0,082    0,082      0,484    0,140      0,172    0,850     0,102     d
                 0,090    0,016    0,077      0,090    0,083      0,069    0,770     0,045     f
                 0,620    0,041    0,392      0,620    0,481      0,466    0,910     0,430     i
                 0,257    0,045    0,103      0,257    0,147      0,136    0,840     0,098     k
                 0,159    0,016    0,102      0,159    0,124      0,115    0,832     0,069     j
                 0,092    0,025    0,292      0,092    0,140      0,116    0,780     0,240     l
                 0,268    0,017    0,568      0,268    0,364      0,357    0,823     0,407     o
                 0,908    0,079    0,867      0,908    0,887      0,821    0,968     0,960     0
                 0,238    0,030    0,337      0,238    0,279      0,245    0,880     0,321     s
                 0,463    0,015    0,418      0,463    0,439      0,426    0,911     0,476     u
Weighted Avg.    0,561    0,054    0,581      0,561    0,555      0,510    0,893     0,607     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1172    5  152  207   42   51   86   19   57   69   67   48   14 |    a = a
   38    7   48   61   11   74   30   24   26    7   31   21   21 |    b = b
  100    2  474  103   13   77   41   39   27   17   29   25   24 |    c = e
   28    4   14   88    2    8    2    6   12    7    4    1    6 |    d = d
   21    1   26   32   16    6   17    8    9    9   14   17    1 |    e = f
    6    5   73   15    4  312   35   12    8    0   14   14    5 |    f = i
    4    2    3    3    6   13   61    8    5    4  104   24    0 |    g = k
   30    3   33    5    5   11    4   22    6    0    6   12    1 |    h = j
  178    8  147  269   31  119   83   30  112   25  100   69   45 |    i = l
  149    3   77  170   12   31   43   11   31  252   79   23   59 |    j = o
   39    8   38   36   36   42   59    6   34   17 4009   87    5 |    k = 0
   35    5   32   50   28   46  115   30   45    8  162  175    3 |    l = s
   19    2   22   35    1    5   19    1   11   29    5    4  132 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
58.92	0.25	7.64	10.41	2.11	2.56	4.32	0.96	2.87	3.47	3.37	2.41	0.7	 a = a
9.52	1.75	12.03	15.29	2.76	18.55	7.52	6.02	6.52	1.75	7.77	5.26	5.26	 b = b
10.3	0.21	48.82	10.61	1.34	7.93	4.22	4.02	2.78	1.75	2.99	2.57	2.47	 c = e
15.38	2.2	7.69	48.35	1.1	4.4	1.1	3.3	6.59	3.85	2.2	0.55	3.3	 d = d
11.86	0.56	14.69	18.08	9.04	3.39	9.6	4.52	5.08	5.08	7.91	9.6	0.56	 e = f
1.19	0.99	14.51	2.98	0.8	62.03	6.96	2.39	1.59	0.0	2.78	2.78	0.99	 f = i
1.69	0.84	1.27	1.27	2.53	5.49	25.74	3.38	2.11	1.69	43.88	10.13	0.0	 g = k
21.74	2.17	23.91	3.62	3.62	7.97	2.9	15.94	4.35	0.0	4.35	8.7	0.72	 h = j
14.64	0.66	12.09	22.12	2.55	9.79	6.83	2.47	9.21	2.06	8.22	5.67	3.7	 i = l
15.85	0.32	8.19	18.09	1.28	3.3	4.57	1.17	3.3	26.81	8.4	2.45	6.28	 j = o
0.88	0.18	0.86	0.82	0.82	0.95	1.34	0.14	0.77	0.38	90.78	1.97	0.11	 k = 0
4.77	0.68	4.36	6.81	3.81	6.27	15.67	4.09	6.13	1.09	22.07	23.84	0.41	 l = s
6.67	0.7	7.72	12.28	0.35	1.75	6.67	0.35	3.86	10.18	1.75	1.4	46.32	 m = u
