Timestamp:2013-08-30-04:44:11
Inventario:I3
Intervalo:1ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I3-1ms-2gramas
Num Instances:  183403
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(35): class 
0Pitch(Hz)(96): class 
0RawPitch(78): class 
0SmPitch(93): class 
0Melogram(st)(83): class 
0ZCross(20): class 
0F1(Hz)(2607): class 
0F2(Hz)(3303): class 
0F3(Hz)(3508): class 
0F4(Hz)(3434): class 
1Int(dB)(32): class 
1Pitch(Hz)(96): class 
1RawPitch(78): class 
1SmPitch(90): class 
1Melogram(st)(87): class 
1ZCross(20): class 
1F1(Hz)(2598): class 
1F2(Hz)(3309): class 
1F3(Hz)(3521): class 
1F4(Hz)(3448): class 
class(11): 
LogScore Bayes: -1.125426147652181E7
LogScore BDeu: -1.5027919604417348E7
LogScore MDL: -1.4080082284636296E7
LogScore ENTROPY: -1.2312546622235736E7
LogScore AIC: -1.2604232622235736E7




=== Stratified cross-validation ===

Correctly Classified Instances      148492               80.9649 %
Incorrectly Classified Instances     34911               19.0351 %
Kappa statistic                          0.7511
K&B Relative Info Score            14282236.4693 %
K&B Information Score               369799.2909 bits      2.0163 bits/instance
Class complexity | order 0          474844.9163 bits      2.5891 bits/instance
Class complexity | scheme           873273.1573 bits      4.7615 bits/instance
Complexity improvement     (Sf)    -398428.2411 bits     -2.1724 bits/instance
Mean absolute error                      0.0348
Root mean squared error                  0.1806
Relative absolute error                 24.7533 %
Root relative squared error             68.0867 %
Coverage of cases (0.95 level)          83.1393 %
Mean rel. region size (0.95 level)       9.8344 %
Total Number of Instances           183403     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,870    0,013    0,927      0,870    0,898      0,880    0,966     0,935     A
                 0,881    0,013    0,846      0,881    0,863      0,852    0,970     0,902     E
                 0,499    0,018    0,858      0,499    0,631      0,602    0,961     0,839     CGJLNQSRT
                 0,674    0,007    0,562      0,674    0,613      0,609    0,928     0,666     FV
                 0,876    0,008    0,813      0,876    0,843      0,837    0,965     0,883     I
                 0,814    0,009    0,876      0,814    0,844      0,833    0,934     0,863     O
                 0,909    0,159    0,787      0,909    0,844      0,735    0,972     0,962     0
                 0,731    0,006    0,791      0,731    0,760      0,753    0,927     0,761     BMP
                 0,937    0,007    0,756      0,937    0,837      0,838    0,983     0,936     U
                 0,000    0,005    0,000      0,000    0,000      -0,001   0,807     0,001     DZ
                 0,947    0,014    0,486      0,947    0,643      0,673    0,990     0,864     D
Weighted Avg.    0,810    0,071    0,825      0,810    0,805      0,753    0,964     0,908     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 24853   186   522   178   112   182  1605   161    80   310   368 |     a = A
    67 12103   404   105    61    22   545   109    52   104   168 |     b = E
   950  1046 16691   149   520   606 11804   252   616    61   784 |     c = CGJLNQSRT
    25    38    67  1725    28    12   661     0     3     0     2 |     d = FV
    15    23   238    59  6202    11   381    56     3    67    28 |     e = I
    85    22   198    34    31 10776  1476   126    35   105   346 |     f = O
   740   828  1052   805   565   598 65536   402   401   293   896 |     g = 0
    52    28   160     6   105    68  1077  4251    55     5     7 |     h = BMP
     8     6   109     6     1     0    75    16  3876    20    20 |     i = U
     0     0     0     0     0     0    65     0     0     0     0 |     j = DZ
    12    26    11     1     4    20    37     0     7    20  2479 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
87.03	0.65	1.83	0.62	0.39	0.64	5.62	0.56	0.28	1.09	1.29	 a = A
0.49	88.09	2.94	0.76	0.44	0.16	3.97	0.79	0.38	0.76	1.22	 b = E
2.84	3.12	49.86	0.45	1.55	1.81	35.26	0.75	1.84	0.18	2.34	 c = CGJLNQSRT
0.98	1.48	2.62	67.36	1.09	0.47	25.81	0.0	0.12	0.0	0.08	 d = FV
0.21	0.32	3.36	0.83	87.56	0.16	5.38	0.79	0.04	0.95	0.4	 e = I
0.64	0.17	1.5	0.26	0.23	81.43	11.15	0.95	0.26	0.79	2.61	 f = O
1.03	1.15	1.46	1.12	0.78	0.83	90.88	0.56	0.56	0.41	1.24	 g = 0
0.89	0.48	2.75	0.1	1.81	1.17	18.52	73.12	0.95	0.09	0.12	 h = BMP
0.19	0.15	2.63	0.15	0.02	0.0	1.81	0.39	93.69	0.48	0.48	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
0.46	0.99	0.42	0.04	0.15	0.76	1.41	0.0	0.27	0.76	94.73	 k = D
