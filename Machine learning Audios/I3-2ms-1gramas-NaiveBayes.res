Timestamp:2014-12-07-11:05:46
Inventario:I3
Intervalo:2ms
Ngramas:1
Clasificador:NaiveBayes
Relation Name:  I3-2ms-1gramas
Num Instances:  91703
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Naive Bayes Classifier

                    Class
Attribute               A         E CGJLNQSRT        FV         I         O         0       BMP         U        DZ         D
                   (0.16)    (0.08)    (0.18)    (0.01)    (0.04)    (0.07)    (0.39)    (0.03)    (0.02)       (0)    (0.01)
==============================================================================================================================
0Int(dB)
  mean             -4.2733    -5.199  -16.4813  -15.0003    -8.676   -8.1936  -48.3069  -12.2641   -5.0927  -26.9132   -7.7016
  std. dev.         9.6287    8.0531   10.2875    8.8607    8.3577   12.2208   16.1112   10.5968    7.5769    1.9983     6.187
  weight sum         14333      6910     16793      1286      3561      6646     35833      2915      2080        33      1313
  precision         0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037

0Pitch(Hz)
  mean            215.1446  190.3791  115.9271  130.8258  173.4856  178.0537   16.3378  160.3717  229.8416         0  202.2426
  std. dev.       108.5552   87.7367  113.3329  107.1381   76.0683  105.2226    55.727  110.4325   83.5753    0.0207   84.5819
  weight sum         14333      6910     16793      1286      3561      6646     35833      2915      2080        33      1313
  precision         0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242

0RawPitch
  mean            204.7828   185.837   97.0368  105.9763  166.2787  172.2852   13.3724  148.5095  220.9278         0  195.8324
  std. dev.       114.9802   95.0002  112.4475  102.9581   83.0617  112.5036   50.3479  115.8451   92.1417    0.0207    94.326
  weight sum         14333      6910     16793      1286      3561      6646     35833      2915      2080        33      1313
  precision         0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243

0SmPitch
  mean            213.2239  188.9642  109.1119  120.6473  171.0525  175.9682   14.7403  155.6124  229.2536         0  201.7614
  std. dev.       110.0447   88.9263  113.4077  104.2291   78.7886  107.3853   51.9288  113.2741   84.0032    0.0208   85.5097
  weight sum         14333      6910     16793      1286      3561      6646     35833      2915      2080        33      1313
  precision         0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248

0Melogram(st)
  mean             51.2983   50.4842   30.1853   37.0069   48.6341   46.4749    4.8253   41.6893   54.3072         0   53.0765
  std. dev.        16.9875   15.3213   27.4518   25.0249   15.9951   20.5262   15.4829   24.1614   13.7628    0.0041   11.1742
  weight sum         14333      6910     16793      1286      3561      6646     35833      2915      2080        33      1313
  precision         0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246

0ZCross
  mean               7.111    5.2438    3.0478    2.0698    3.7483    4.4237    0.2484    2.5583    4.1111         0    4.1913
  std. dev.         4.6004    3.2168     4.934    2.2498    3.2275    3.0692    1.1311    2.2519    2.9164    0.1792    2.3027
  weight sum         14333      6910     16793      1286      3561      6646     35833      2915      2080        33      1313
  precision         1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755

0F1(Hz)
  mean            683.4195  466.4894  305.1041  378.5922    363.75  486.2046   52.7313   372.086  500.6341         0  518.4309
  std. dev.        269.455  142.2388   277.473  277.5826  140.8431  225.0865  165.0978  236.0116  305.2064    0.0608  153.3851
  weight sum         14333      6910     16793      1286      3561      6646     35833      2915      2080        33      1313
  precision         0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648

0F2(Hz)
  mean           1444.7727 1798.4442  995.4145 1163.3139  1993.047 1140.7371  165.1112 1309.1173 1061.2779         0 1484.1075
  std. dev.       450.4532  511.8666  864.2114   782.141  650.7685  531.1147  511.8582    746.06  416.4942    0.0738  323.4684
  weight sum         14333      6910     16793      1286      3561      6646     35833      2915      2080        33      1313
  precision         0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429

0F3(Hz)
  mean           2659.8447 2634.9785 1696.3721 1978.2255 2677.3565 2506.4348  281.5167 2204.7048 2694.0925         0 2868.2959
  std. dev.       806.7783  764.1593 1451.3307 1318.9077  898.3116 1018.6433  858.0435 1201.2074  755.1652    0.1103  510.0445
  weight sum         14333      6910     16793      1286      3561      6646     35833      2915      2080        33      1313
  precision          0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662

0F4(Hz)
  mean           3084.7647   3174.33 1944.3398 2370.2376 3182.3609 2882.7851  338.5076 2650.5175 3086.3027         0 3319.7801
  std. dev.       921.7526  885.5429 1654.8525 1544.5093 1024.5862 1153.5603 1029.3862 1411.8243   763.546    0.1138  557.8761
  weight sum         14333      6910     16793      1286      3561      6646     35833      2915      2080        33      1313
  precision         0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829





=== Stratified cross-validation ===

Correctly Classified Instances       36742               40.0663 %
Incorrectly Classified Instances     54961               59.9337 %
Kappa statistic                          0.3063
K&B Relative Info Score            2717386.9176 %
K&B Information Score                70509.4524 bits      0.7689 bits/instance
Class complexity | order 0          237916.3779 bits      2.5944 bits/instance
Class complexity | scheme          1588881.8667 bits     17.3264 bits/instance
Complexity improvement     (Sf)    -1350965.4888 bits    -14.732  bits/instance
Mean absolute error                      0.116 
Root mean squared error                  0.2939
Relative absolute error                 82.3208 %
Root relative squared error            110.7085 %
Coverage of cases (0.95 level)          58.1813 %
Mean rel. region size (0.95 level)      26.3042 %
Total Number of Instances            91703     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,562    0,069    0,602      0,562    0,581      0,507    0,866     0,595     A
                 0,677    0,137    0,287      0,677    0,403      0,373    0,859     0,390     E
                 0,194    0,044    0,494      0,194    0,278      0,224    0,618     0,370     CGJLNQSRT
                 0,000    0,000    0,000      0,000    0,000      0,000    0,624     0,026     FV
                 0,254    0,029    0,261      0,254    0,258      0,228    0,857     0,307     I
                 0,040    0,007    0,323      0,040    0,072      0,092    0,748     0,186     O
                 0,513    0,018    0,947      0,513    0,666      0,591    0,891     0,874     0
                 0,001    0,000    0,130      0,001    0,002      0,009    0,666     0,055     BMP
                 0,317    0,006    0,538      0,317    0,399      0,403    0,874     0,374     U
                 1,000    0,227    0,002      1,000    0,003      0,035    0,948     0,003     DZ
                 0,379    0,101    0,052      0,379    0,091      0,108    0,821     0,048     D
Weighted Avg.    0,401    0,040    0,627      0,401    0,458      0,406    0,811     0,568     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  8050  2035   673     0   176    86   126     0     5   655  2527 |     a = A
   903  4679   375     0   270    11    23     2     1   252   394 |     b = E
  1482  2910  3255     0   978   228   748    11    60  4469  2652 |     c = CGJLNQSRT
   104   287   157     0   122    31    16     0     0   311   258 |     d = FV
   205  1937   265     0   906     0    30     1     0   167    50 |     e = I
  1315  1986   465     0    55   269    17     0   398   714  1427 |     f = O
   439   848   928     0   473   123 18389     6    27 13754   846 |     g = 0
   268   811   267     0   347    32    50     3    76   489   572 |     h = BMP
   387   367   129     0    73    42    12     0   660    26   384 |     i = U
     0     0     0     0     0     0     0     0     0    33     0 |     j = DZ
   219   428    71     0    67    12     8     0     0    10   498 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
56.16	14.2	4.7	0.0	1.23	0.6	0.88	0.0	0.03	4.57	17.63	 a = A
13.07	67.71	5.43	0.0	3.91	0.16	0.33	0.03	0.01	3.65	5.7	 b = E
8.83	17.33	19.38	0.0	5.82	1.36	4.45	0.07	0.36	26.61	15.79	 c = CGJLNQSRT
8.09	22.32	12.21	0.0	9.49	2.41	1.24	0.0	0.0	24.18	20.06	 d = FV
5.76	54.39	7.44	0.0	25.44	0.0	0.84	0.03	0.0	4.69	1.4	 e = I
19.79	29.88	7.0	0.0	0.83	4.05	0.26	0.0	5.99	10.74	21.47	 f = O
1.23	2.37	2.59	0.0	1.32	0.34	51.32	0.02	0.08	38.38	2.36	 g = 0
9.19	27.82	9.16	0.0	11.9	1.1	1.72	0.1	2.61	16.78	19.62	 h = BMP
18.61	17.64	6.2	0.0	3.51	2.02	0.58	0.0	31.73	1.25	18.46	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	 j = DZ
16.68	32.6	5.41	0.0	5.1	0.91	0.61	0.0	0.0	0.76	37.93	 k = D
