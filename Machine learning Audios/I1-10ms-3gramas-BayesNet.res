Timestamp:2013-08-30-04:17:40
Inventario:I1
Intervalo:10ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I1-10ms-3gramas
Num Instances:  18294
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(12): class 
0Pitch(Hz)(5): class 
0RawPitch(4): class 
0SmPitch(5): class 
0Melogram(st)(10): class 
0ZCross(9): class 
0F1(Hz)(11): class 
0F2(Hz)(10): class 
0F3(Hz)(13): class 
0F4(Hz)(9): class 
1Int(dB)(10): class 
1Pitch(Hz)(6): class 
1RawPitch(6): class 
1SmPitch(5): class 
1Melogram(st)(9): class 
1ZCross(11): class 
1F1(Hz)(13): class 
1F2(Hz)(11): class 
1F3(Hz)(10): class 
1F4(Hz)(9): class 
2Int(dB)(11): class 
2Pitch(Hz)(5): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(8): class 
2ZCross(11): class 
2F1(Hz)(12): class 
2F2(Hz)(10): class 
2F3(Hz)(9): class 
2F4(Hz)(8): class 
class(13): 
LogScore Bayes: -658632.2632112111
LogScore BDeu: -672856.3236476583
LogScore MDL: -671190.3403392873
LogScore ENTROPY: -656459.0333865463
LogScore AIC: -659461.0333865464




=== Stratified cross-validation ===

Correctly Classified Instances       10537               57.5981 %
Incorrectly Classified Instances      7757               42.4019 %
Kappa statistic                          0.4749
K&B Relative Info Score             853575.8266 %
K&B Information Score                25034.9725 bits      1.3685 bits/instance
Class complexity | order 0           53632.982  bits      2.9317 bits/instance
Class complexity | scheme           174825.812  bits      9.5565 bits/instance
Complexity improvement     (Sf)    -121192.8301 bits     -6.6247 bits/instance
Mean absolute error                      0.0656
Root mean squared error                  0.2333
Relative absolute error                 52.8272 %
Root relative squared error             93.6487 %
Coverage of cases (0.95 level)          67.9075 %
Mean rel. region size (0.95 level)      12.3357 %
Total Number of Instances            18294     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,619    0,061    0,661      0,619    0,639      0,573    0,900     0,723     a
                 0,025    0,004    0,161      0,025    0,044      0,052    0,763     0,089     b
                 0,558    0,061    0,438      0,558    0,491      0,445    0,886     0,471     e
                 0,500    0,069    0,099      0,500    0,165      0,198    0,868     0,117     d
                 0,100    0,021    0,065      0,100    0,078      0,064    0,773     0,048     f
                 0,636    0,039    0,410      0,636    0,498      0,485    0,916     0,459     i
                 0,231    0,035    0,117      0,231    0,155      0,141    0,857     0,107     k
                 0,159    0,010    0,154      0,159    0,156      0,147    0,833     0,084     j
                 0,081    0,029    0,233      0,081    0,120      0,085    0,779     0,228     l
                 0,301    0,018    0,575      0,301    0,395      0,384    0,834     0,429     o
                 0,902    0,093    0,852      0,902    0,876      0,800    0,963     0,956     0
                 0,226    0,026    0,358      0,226    0,277      0,249    0,892     0,328     s
                 0,494    0,017    0,412      0,494    0,449      0,437    0,921     0,488     u
Weighted Avg.    0,576    0,059    0,582      0,576    0,565      0,516    0,898     0,621     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1820    8  230  252   67   70   77   17  115  105   99   64   16 |    a = a
   53   15   74   68   30  109   35   19   36   21   67   24   43 |    b = b
  138    6  802  109   23  106   38   28   61   21   46   31   29 |    c = e
   43    2   23  137    2   12    1    2   14   16    7    4   11 |    d = d
   20    2   33   43   26   15   20   14   17   13   35   18    5 |    e = f
    9    7  120   12    6  476   34   10   12    0   29   28    5 |    f = i
    7    3    6    1   15   16   83    8    9    5  168   37    1 |    g = k
   44    1   51    9    9   13    8   33    5    1   13   17    4 |    h = j
  241   20  221  380   80  174   97   27  144   39  199   80   86 |    i = l
  230    7  121  200   24   33   39   14   63  415  123   28   80 |    j = o
   74   11   81   58   76   69   73    8   62   33 6131  107   13 |    k = 0
   47    6   37   75   42   65  185   30   63   10  277  246    5 |    l = s
   29    5   33   44    2    4   22    4   18   43    6    4  209 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
61.9	0.27	7.82	8.57	2.28	2.38	2.62	0.58	3.91	3.57	3.37	2.18	0.54	 a = a
8.92	2.53	12.46	11.45	5.05	18.35	5.89	3.2	6.06	3.54	11.28	4.04	7.24	 b = b
9.6	0.42	55.77	7.58	1.6	7.37	2.64	1.95	4.24	1.46	3.2	2.16	2.02	 c = e
15.69	0.73	8.39	50.0	0.73	4.38	0.36	0.73	5.11	5.84	2.55	1.46	4.01	 d = d
7.66	0.77	12.64	16.48	9.96	5.75	7.66	5.36	6.51	4.98	13.41	6.9	1.92	 e = f
1.2	0.94	16.04	1.6	0.8	63.64	4.55	1.34	1.6	0.0	3.88	3.74	0.67	 f = i
1.95	0.84	1.67	0.28	4.18	4.46	23.12	2.23	2.51	1.39	46.8	10.31	0.28	 g = k
21.15	0.48	24.52	4.33	4.33	6.25	3.85	15.87	2.4	0.48	6.25	8.17	1.92	 h = j
13.48	1.12	12.36	21.25	4.47	9.73	5.43	1.51	8.05	2.18	11.13	4.47	4.81	 i = l
16.7	0.51	8.79	14.52	1.74	2.4	2.83	1.02	4.58	30.14	8.93	2.03	5.81	 j = o
1.09	0.16	1.19	0.85	1.12	1.02	1.07	0.12	0.91	0.49	90.21	1.57	0.19	 k = 0
4.32	0.55	3.4	6.89	3.86	5.97	17.0	2.76	5.79	0.92	25.46	22.61	0.46	 l = s
6.86	1.18	7.8	10.4	0.47	0.95	5.2	0.95	4.26	10.17	1.42	0.95	49.41	 m = u
