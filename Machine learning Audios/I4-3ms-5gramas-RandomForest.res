Timestamp:2013-07-22-23:02:55
Inventario:I4
Intervalo:3ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I4-3ms-5gramas
Num Instances:  61131
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1697





=== Stratified cross-validation ===

Correctly Classified Instances       53710               87.8605 %
Incorrectly Classified Instances      7421               12.1395 %
Kappa statistic                          0.8417
K&B Relative Info Score            4853463.8883 %
K&B Information Score               123952.3055 bits      2.0277 bits/instance
Class complexity | order 0          156103.513  bits      2.5536 bits/instance
Class complexity | scheme          1975500.7242 bits     32.3159 bits/instance
Complexity improvement     (Sf)    -1819397.2112 bits    -29.7623 bits/instance
Mean absolute error                      0.0437
Root mean squared error                  0.1396
Relative absolute error                 28.2679 %
Root relative squared error             50.2114 %
Coverage of cases (0.95 level)          97.031  %
Mean rel. region size (0.95 level)      21.6234 %
Total Number of Instances            61131     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,873    0,000    0,919      0,873    0,895      0,895    0,976     0,919     C
                 0,899    0,011    0,872      0,899    0,885      0,876    0,978     0,926     E
                 0,639    0,003    0,777      0,639    0,701      0,701    0,939     0,701     FV
                 0,916    0,026    0,895      0,916    0,905      0,882    0,977     0,950     AI
                 0,834    0,044    0,778      0,834    0,805      0,768    0,962     0,869     CDGKNRSYZ
                 0,826    0,006    0,911      0,826    0,866      0,858    0,954     0,883     O
                 0,936    0,056    0,913      0,936    0,924      0,875    0,982     0,966     0
                 0,549    0,006    0,779      0,549    0,644      0,643    0,907     0,643     LT
                 0,901    0,001    0,953      0,901    0,926      0,925    0,985     0,946     U
                 0,717    0,003    0,886      0,717    0,793      0,791    0,944     0,800     MBP
Weighted Avg.    0,879    0,036    0,878      0,879    0,877      0,846    0,971     0,917     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   317     9     0    12     9     1    12     1     1     1 |     a = C
     8  4152    11   114   161    25   107    22     7    14 |     b = E
     0    30   552    41   175    18    32    10     0     6 |     c = FV
     8   135    23 10956   352    45   336    76     5    31 |     d = AI
     3   154    57   383  7989    97   739   108    10    36 |     e = CDGKNRSYZ
     0    44    10   111   201  3687   370    20     7    13 |     f = O
     5   130    28   315   829    98 22192    76     9    40 |     g = 0
     2    45    13   178   350    34   339  1219    11    30 |     h = LT
     2    21     1    21    37    17    20     9  1249     9 |     i = U
     0    42    15   114   166    26   152    24    12  1397 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
87.33	2.48	0.0	3.31	2.48	0.28	3.31	0.28	0.28	0.28	 a = C
0.17	89.85	0.24	2.47	3.48	0.54	2.32	0.48	0.15	0.3	 b = E
0.0	3.47	63.89	4.75	20.25	2.08	3.7	1.16	0.0	0.69	 c = FV
0.07	1.13	0.19	91.55	2.94	0.38	2.81	0.64	0.04	0.26	 d = AI
0.03	1.61	0.6	4.0	83.43	1.01	7.72	1.13	0.1	0.38	 e = CDGKNRSYZ
0.0	0.99	0.22	2.49	4.5	82.61	8.29	0.45	0.16	0.29	 f = O
0.02	0.55	0.12	1.33	3.49	0.41	93.55	0.32	0.04	0.17	 g = 0
0.09	2.03	0.59	8.01	15.76	1.53	15.26	54.89	0.5	1.35	 h = LT
0.14	1.52	0.07	1.52	2.67	1.23	1.44	0.65	90.12	0.65	 i = U
0.0	2.16	0.77	5.85	8.52	1.33	7.8	1.23	0.62	71.71	 j = MBP
