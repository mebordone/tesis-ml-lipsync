Timestamp:2013-08-30-04:21:46
Inventario:I2
Intervalo:1ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I2-1ms-3gramas
Num Instances:  183402
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(36): class 
0Pitch(Hz)(99): class 
0RawPitch(73): class 
0SmPitch(87): class 
0Melogram(st)(83): class 
0ZCross(21): class 
0F1(Hz)(2745): class 
0F2(Hz)(3494): class 
0F3(Hz)(3656): class 
0F4(Hz)(3597): class 
1Int(dB)(34): class 
1Pitch(Hz)(100): class 
1RawPitch(73): class 
1SmPitch(89): class 
1Melogram(st)(83): class 
1ZCross(21): class 
1F1(Hz)(2722): class 
1F2(Hz)(3506): class 
1F3(Hz)(3673): class 
1F4(Hz)(3598): class 
2Int(dB)(33): class 
2Pitch(Hz)(100): class 
2RawPitch(71): class 
2SmPitch(95): class 
2Melogram(st)(83): class 
2ZCross(21): class 
2F1(Hz)(2725): class 
2F2(Hz)(3494): class 
2F3(Hz)(3680): class 
2F4(Hz)(3605): class 
class(12): 
LogScore Bayes: -1.664770391840645E7
LogScore BDeu: -2.3199349642322306E7
LogScore MDL: -2.1517021809616003E7
LogScore ENTROPY: -1.8487071977880307E7
LogScore AIC: -1.8987086977880307E7




=== Stratified cross-validation ===

Correctly Classified Instances      151442               82.5738 %
Incorrectly Classified Instances     31960               17.4262 %
Kappa statistic                          0.7768
K&B Relative Info Score            14440859.9251 %
K&B Information Score               412580.3692 bits      2.2496 bits/instance
Class complexity | order 0          523969.0404 bits      2.8569 bits/instance
Class complexity | scheme          1263961.3607 bits      6.8918 bits/instance
Complexity improvement     (Sf)    -739992.3203 bits     -4.0348 bits/instance
Mean absolute error                      0.0292
Root mean squared error                  0.166 
Relative absolute error                 22.0711 %
Root relative squared error             64.5369 %
Coverage of cases (0.95 level)          84.0503 %
Mean rel. region size (0.95 level)       8.8334 %
Total Number of Instances           183402     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,868    0,009    0,944      0,868    0,905      0,889    0,968     0,941     A
                 0,578    0,009    0,759      0,578    0,656      0,647    0,950     0,702     LGJKC
                 0,877    0,010    0,881      0,877    0,879      0,869    0,971     0,911     E
                 0,686    0,008    0,551      0,686    0,611      0,609    0,939     0,673     FV
                 0,885    0,006    0,858      0,885    0,871      0,866    0,966     0,894     I
                 0,821    0,007    0,896      0,821    0,856      0,847    0,937     0,870     O
                 0,480    0,017    0,745      0,480    0,584      0,567    0,959     0,729     SNRTY
                 0,909    0,140    0,808      0,909    0,856      0,756    0,973     0,964     0
                 0,736    0,006    0,800      0,736    0,767      0,760    0,935     0,772     BMP
                 0,930    0,012    0,545      0,930    0,687      0,707    0,978     0,858     DZ
                 0,939    0,005    0,826      0,939    0,879      0,878    0,984     0,942     U
                 0,832    0,007    0,842      0,832    0,837      0,830    0,953     0,848     SNRTXY
Weighted Avg.    0,826    0,061    0,829      0,826    0,821      0,776    0,965     0,895     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 24793   353   155   268    99   170   412  1534   162   346    60   205 |     a = A
   114  5098   147    22    62    73   724  2344    44    40   120    39 |     b = LGJKC
    54   162 12046   111    44    28   228   545   135   149    45   193 |     c = E
    16     9    29  1756    24    11    76   635     0     1     4     0 |     d = FV
    14   101    18    44  6265    13   145   361    84    18     0    20 |     e = I
    61   118    11    39    34 10859    96  1453   121   311    22   109 |     f = O
   420   240   375    52   155   278  8292  6807   123   295   139    97 |     g = SNRTY
   668   541   769   868   491   583   626 65536   385   856   356   436 |     h = 0
    51    47    26     9   101    60   142  1052  4280     4    42     0 |     i = BMP
     9     0    18     0     1    13     8    95     0  2495     7    36 |     j = DZ
     7    44     4    13     0     1    67    69    19     9  3886    18 |     k = U
    54     7    79     5    27    37   309   644     0    57    24  6136 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
86.82	1.24	0.54	0.94	0.35	0.6	1.44	5.37	0.57	1.21	0.21	0.72	 a = A
1.29	57.75	1.67	0.25	0.7	0.83	8.2	26.55	0.5	0.45	1.36	0.44	 b = LGJKC
0.39	1.18	87.67	0.81	0.32	0.2	1.66	3.97	0.98	1.08	0.33	1.4	 c = E
0.62	0.35	1.13	68.57	0.94	0.43	2.97	24.8	0.0	0.04	0.16	0.0	 d = FV
0.2	1.43	0.25	0.62	88.45	0.18	2.05	5.1	1.19	0.25	0.0	0.28	 e = I
0.46	0.89	0.08	0.29	0.26	82.05	0.73	10.98	0.91	2.35	0.17	0.82	 f = O
2.43	1.39	2.17	0.3	0.9	1.61	48.01	39.41	0.71	1.71	0.8	0.56	 g = SNRTY
0.93	0.75	1.07	1.2	0.68	0.81	0.87	90.88	0.53	1.19	0.49	0.6	 h = 0
0.88	0.81	0.45	0.15	1.74	1.03	2.44	18.09	73.62	0.07	0.72	0.0	 i = BMP
0.34	0.0	0.67	0.0	0.04	0.48	0.3	3.54	0.0	93.03	0.26	1.34	 j = DZ
0.17	1.06	0.1	0.31	0.0	0.02	1.62	1.67	0.46	0.22	93.93	0.44	 k = U
0.73	0.09	1.07	0.07	0.37	0.5	4.19	8.73	0.0	0.77	0.33	83.15	 l = SNRTXY
