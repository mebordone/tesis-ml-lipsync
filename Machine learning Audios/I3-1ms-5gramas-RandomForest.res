Timestamp:2013-07-22-22:11:56
Inventario:I3
Intervalo:1ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I3-1ms-5gramas
Num Instances:  183400
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  49 4F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1093





=== Stratified cross-validation ===

Correctly Classified Instances      168219               91.7225 %
Incorrectly Classified Instances     15181                8.2775 %
Kappa statistic                          0.8925
K&B Relative Info Score            16060283.9306 %
K&B Information Score               415835.0875 bits      2.2674 bits/instance
Class complexity | order 0          474840.8764 bits      2.5891 bits/instance
Class complexity | scheme          5211119.4714 bits     28.414  bits/instance
Complexity improvement     (Sf)    -4736278.595 bits    -25.8249 bits/instance
Mean absolute error                      0.0237
Root mean squared error                  0.1077
Relative absolute error                 16.8204 %
Root relative squared error             40.6245 %
Coverage of cases (0.95 level)          97.3637 %
Mean rel. region size (0.95 level)      14.4745 %
Total Number of Instances           183400     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,941    0,008    0,955      0,941    0,948      0,939    0,981     0,966     A
                 0,943    0,004    0,954      0,943    0,948      0,944    0,984     0,964     E
                 0,875    0,041    0,826      0,875    0,850      0,816    0,973     0,917     CGJLNQSRT
                 0,727    0,002    0,872      0,727    0,793      0,794    0,931     0,793     FV
                 0,924    0,002    0,960      0,924    0,942      0,940    0,978     0,951     I
                 0,877    0,004    0,948      0,877    0,911      0,905    0,960     0,915     O
                 0,943    0,048    0,927      0,943    0,935      0,892    0,986     0,970     0
                 0,790    0,002    0,915      0,790    0,848      0,846    0,946     0,853     BMP
                 0,958    0,000    0,983      0,958    0,970      0,969    0,993     0,981     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,522     0,001     DZ
                 0,955    0,000    0,980      0,955    0,967      0,967    0,997     0,988     D
Weighted Avg.    0,917    0,029    0,918      0,917    0,917      0,892    0,978     0,949     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 26884    43   845    22    19    39   651    42     7     0     5 |     a = A
    60 12953   415    15    19    22   215    26     7     1     7 |     b = E
   525   213 29310   130    77   169  2849   160    22     7    17 |     c = CGJLNQSRT
    36    25   519  1863     6    23    73    12     1     2     1 |     d = FV
    38    24   252     9  6546     7   186    15     5     0     1 |     e = I
    84    35   453    26    13 11603   971    30     4     1    14 |     f = O
   404   191  2909    50   106   305 68006   126     9     2     5 |     g = 0
    82    59   611    18    24    34   382  4592    12     0     0 |     h = BMP
    10    14    97     0     5     4    32    11  3962     0     2 |     i = U
     2     2    47     3     0     1     7     3     0     0     0 |     j = DZ
    18    17    35     1     3    26    13     1     3     0  2500 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
94.14	0.15	2.96	0.08	0.07	0.14	2.28	0.15	0.02	0.0	0.02	 a = A
0.44	94.27	3.02	0.11	0.14	0.16	1.56	0.19	0.05	0.01	0.05	 b = E
1.57	0.64	87.55	0.39	0.23	0.5	8.51	0.48	0.07	0.02	0.05	 c = CGJLNQSRT
1.41	0.98	20.27	72.75	0.23	0.9	2.85	0.47	0.04	0.08	0.04	 d = FV
0.54	0.34	3.56	0.13	92.42	0.1	2.63	0.21	0.07	0.0	0.01	 e = I
0.63	0.26	3.42	0.2	0.1	87.68	7.34	0.23	0.03	0.01	0.11	 f = O
0.56	0.26	4.03	0.07	0.15	0.42	94.3	0.17	0.01	0.0	0.01	 g = 0
1.41	1.01	10.51	0.31	0.41	0.58	6.57	78.98	0.21	0.0	0.0	 h = BMP
0.24	0.34	2.34	0.0	0.12	0.1	0.77	0.27	95.77	0.0	0.05	 i = U
3.08	3.08	72.31	4.62	0.0	1.54	10.77	4.62	0.0	0.0	0.0	 j = DZ
0.69	0.65	1.34	0.04	0.11	0.99	0.5	0.04	0.11	0.0	95.53	 k = D
