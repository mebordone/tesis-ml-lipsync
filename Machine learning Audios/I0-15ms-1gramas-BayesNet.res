Timestamp:2013-08-30-03:54:23
Inventario:I0
Intervalo:15ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I0-15ms-1gramas
Num Instances:  12189
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    27 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(8): class 
0F1(Hz)(8): class 
0F2(Hz)(9): class 
0F3(Hz)(6): class 
0F4(Hz)(5): class 
class(27): 
LogScore Bayes: -138444.6576461913
LogScore BDeu: -145757.49803637373
LogScore MDL: -144883.29958381382
LogScore ENTROPY: -138029.36091279206
LogScore AIC: -139486.36091279204




=== Stratified cross-validation ===

Correctly Classified Instances        7130               58.4954 %
Incorrectly Classified Instances      5059               41.5046 %
Kappa statistic                          0.4853
K&B Relative Info Score             558523.4995 %
K&B Information Score                18877.6211 bits      1.5487 bits/instance
Class complexity | order 0           41121.1307 bits      3.3736 bits/instance
Class complexity | scheme            46856.0593 bits      3.8441 bits/instance
Complexity improvement     (Sf)      -5734.9286 bits     -0.4705 bits/instance
Mean absolute error                      0.0344
Root mean squared error                  0.1465
Relative absolute error                 56.177  %
Root relative squared error             83.7611 %
Coverage of cases (0.95 level)          81.1962 %
Mean rel. region size (0.95 level)      15.342  %
Total Number of Instances            12189     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,086    0,001    0,333      0,086    0,137      0,168    0,876     0,112     ch
                 0,000    0,000    0,000      0,000    0,000      -0,001   0,858     0,012     rr
                 0,935    0,113    0,825      0,935    0,877      0,804    0,974     0,964     0
                 0,071    0,013    0,061      0,071    0,066      0,054    0,771     0,045     hs
                 0,235    0,007    0,089      0,235    0,129      0,141    0,944     0,070     hg
                 0,747    0,080    0,620      0,747    0,677      0,618    0,926     0,750     a
                 0,011    0,001    0,154      0,011    0,021      0,037    0,855     0,088     c
                 0,000    0,000    0,000      0,000    0,000      -0,001   0,855     0,031     b
                 0,530    0,053    0,465      0,530    0,495      0,450    0,892     0,487     e
                 0,068    0,005    0,158      0,068    0,095      0,095    0,841     0,086     d
                 0,042    0,004    0,059      0,042    0,049      0,045    0,793     0,029     g
                 0,051    0,002    0,143      0,051    0,075      0,082    0,887     0,048     f
                 0,567    0,029    0,459      0,567    0,507      0,487    0,925     0,476     i
                 0,106    0,001    0,318      0,106    0,159      0,181    0,858     0,101     j
                 0,076    0,007    0,140      0,076    0,099      0,093    0,872     0,086     m
                 0,368    0,055    0,111      0,368    0,171      0,176    0,870     0,103     l
                 0,299    0,024    0,508      0,299    0,376      0,352    0,830     0,389     o
                 0,142    0,019    0,221      0,142    0,173      0,153    0,861     0,170     n
                 0,018    0,001    0,100      0,018    0,031      0,041    0,820     0,026     q
                 0,008    0,001    0,056      0,008    0,013      0,017    0,830     0,048     p
                 0,291    0,023    0,404      0,291    0,338      0,314    0,894     0,362     s
                 0,032    0,007    0,108      0,032    0,050      0,046    0,779     0,082     r
                 0,428    0,012    0,462      0,428    0,444      0,432    0,923     0,486     u
                 0,064    0,005    0,208      0,064    0,098      0,106    0,862     0,116     t
                 0,121    0,008    0,107      0,121    0,114      0,106    0,832     0,046     v
                 0,219    0,008    0,121      0,219    0,156      0,157    0,888     0,127     y
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,940     0,004     z
Weighted Avg.    0,585    0,064    0,549      0,585    0,556      0,513    0,914     0,599     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m    n    o    p    q    r    s    t    u    v    w    x    y    z   aa   <-- classified as
    5    0   17    0    0    4    1    0    2    0    0    0    1    0    0    2    0    2    0    0   22    1    0    0    0    1    0 |    a = ch
    0    0    0    0    0    5    0    0    2    0    0    0    0    0    0    5    1    0    0    0    0    1    0    0    0    1    0 |    b = rr
    1    0 4132   26    7   51    0    0   20    1    0    5   30    3    2   24   27   14    4    3   36   13    2    2    3   12    0 |    c = 0
    0    0   41   10    6   11    0    0    8    0    0    0    9    1    3   19   12    1    0    1    8    5    1    1    1    2    0 |    d = hs
    0    0    0    1    8    3    0    0    5    2    0    0   10    0    2    2    1    0    0    0    0    0    0    0    0    0    0 |    e = hg
    1    0   53   11    1 1355    2    0   87    8    1    2   12    2    2   99   56   28    1    1   23   21    6    9   22   10    2 |    f = a
    2    0   90    6    1    2    2    0    8    0    1    1    4    0    0    1    8    4    0    1   42    2    1    6    0    0    0 |    g = c
    0    0    0    0    2   13    0    0   17    1    1    0    2    0    6   15    7    4    0    0    0    0   10    0    3    1    0 |    h = b
    0    1   34   16   20  124    2    0  515    9   16    0   61    1    1   83   17   28    1    1   15    0    9    5    8    4    0 |    i = e
    0    0    1    1    4   41    0    0   26   12    2    0    4    0    4   24   21   11    0    0    0    0    6    3    9    8    0 |    j = d
    0    0    3    0    1   20    0    0   18    0    3    0   10    0    0    4    3    3    0    0    3    1    1    0    2    0    0 |    k = g
    0    0   31    6    3   10    0    0    2    0    0    4    0    1    0    4    2    3    0    1    8    1    0    0    0    2    0 |    l = f
    0    0   19    6    5   10    1    0   98    2    2    0  285    0   10   24    2    8    2    0   16    0    2    4    2    5    0 |    m = i
    0    0   10    4    1   18    0    0    8    0    0    1    4    7    0    3    0    0    0    2    4    4    0    0    0    0    0 |    n = j
    0    0    2    2    1   16    0    0   28    1    4    0   40    0   14   41    2   13    0    0    0    0   12    1    1    6    0 |    o = m
    0    0    1    2    1   29    0    0   33    4    1    0   21    0    6   82    9   12    0    0    2    1    2    1    7    9    0 |    p = l
    1    0   94   17    9  227    2    0   73   23    7    2   13    0   13   67  281   17    0    4    9    5   55    5   10    6    0 |    q = o
    0    0   15   11    4   74    0    3   53    4    2    1   47    0   18   69   12   62    0    0    6    2   21    2   16   14    0 |    r = n
    0    0   19    1    0    5    0    0    2    0    1    0    8    0    0    0    0    1    1    0   15    0    0    0    0    2    0 |    s = q
    0    0   53   11    1    9    2    0    4    2    0    2    1    3    1    8    6    7    0    1   11    6    0    4    0    1    0 |    t = p
    3    0  230   12    3   36    0    0   18    0    0    2   36    3    2   26   13   18    1    2  178   12    2    8    0    7    0 |    u = s
    0    0   36   14    0   75    0    0   35    3    2    4   14    1    0   59   17   13    0    0    5   10    5    2   11    2    0 |    v = r
    0    1    5    1    3   32    0    0   22    1    4    1    0    0   11   25   42    5    0    1    3    0  122    1    1    3    1 |    w = u
    2    1  114    4    1    8    1    0    2    1    3    3    4    0    2    4    6   17    0    0   29    3    5   15    4    5    0 |    x = t
    0    1    1    1    8    9    0    1   20    2    1    0    3    0    3   19    7    4    0    0    1    3    1    1   12    1    0 |    y = v
    0    0    3    0    0    0    0    0    2    0    0    0    2    0    0   27    1    5    0    0    5    2    1    2    0   14    0 |    z = y
    0    0    5    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0 |   aa = z

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z	aa	
8.62	0.0	29.31	0.0	0.0	6.9	1.72	0.0	3.45	0.0	0.0	0.0	1.72	0.0	0.0	3.45	0.0	3.45	0.0	0.0	37.93	1.72	0.0	0.0	0.0	1.72	0.0	 a = ch
0.0	0.0	0.0	0.0	0.0	33.33	0.0	0.0	13.33	0.0	0.0	0.0	0.0	0.0	0.0	33.33	6.67	0.0	0.0	0.0	0.0	6.67	0.0	0.0	0.0	6.67	0.0	 b = rr
0.02	0.0	93.53	0.59	0.16	1.15	0.0	0.0	0.45	0.02	0.0	0.11	0.68	0.07	0.05	0.54	0.61	0.32	0.09	0.07	0.81	0.29	0.05	0.05	0.07	0.27	0.0	 c = 0
0.0	0.0	29.29	7.14	4.29	7.86	0.0	0.0	5.71	0.0	0.0	0.0	6.43	0.71	2.14	13.57	8.57	0.71	0.0	0.71	5.71	3.57	0.71	0.71	0.71	1.43	0.0	 d = hs
0.0	0.0	0.0	2.94	23.53	8.82	0.0	0.0	14.71	5.88	0.0	0.0	29.41	0.0	5.88	5.88	2.94	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 e = hg
0.06	0.0	2.92	0.61	0.06	74.66	0.11	0.0	4.79	0.44	0.06	0.11	0.66	0.11	0.11	5.45	3.09	1.54	0.06	0.06	1.27	1.16	0.33	0.5	1.21	0.55	0.11	 f = a
1.1	0.0	49.45	3.3	0.55	1.1	1.1	0.0	4.4	0.0	0.55	0.55	2.2	0.0	0.0	0.55	4.4	2.2	0.0	0.55	23.08	1.1	0.55	3.3	0.0	0.0	0.0	 g = c
0.0	0.0	0.0	0.0	2.44	15.85	0.0	0.0	20.73	1.22	1.22	0.0	2.44	0.0	7.32	18.29	8.54	4.88	0.0	0.0	0.0	0.0	12.2	0.0	3.66	1.22	0.0	 h = b
0.0	0.1	3.5	1.65	2.06	12.77	0.21	0.0	53.04	0.93	1.65	0.0	6.28	0.1	0.1	8.55	1.75	2.88	0.1	0.1	1.54	0.0	0.93	0.51	0.82	0.41	0.0	 i = e
0.0	0.0	0.56	0.56	2.26	23.16	0.0	0.0	14.69	6.78	1.13	0.0	2.26	0.0	2.26	13.56	11.86	6.21	0.0	0.0	0.0	0.0	3.39	1.69	5.08	4.52	0.0	 j = d
0.0	0.0	4.17	0.0	1.39	27.78	0.0	0.0	25.0	0.0	4.17	0.0	13.89	0.0	0.0	5.56	4.17	4.17	0.0	0.0	4.17	1.39	1.39	0.0	2.78	0.0	0.0	 k = g
0.0	0.0	39.74	7.69	3.85	12.82	0.0	0.0	2.56	0.0	0.0	5.13	0.0	1.28	0.0	5.13	2.56	3.85	0.0	1.28	10.26	1.28	0.0	0.0	0.0	2.56	0.0	 l = f
0.0	0.0	3.78	1.19	0.99	1.99	0.2	0.0	19.48	0.4	0.4	0.0	56.66	0.0	1.99	4.77	0.4	1.59	0.4	0.0	3.18	0.0	0.4	0.8	0.4	0.99	0.0	 m = i
0.0	0.0	15.15	6.06	1.52	27.27	0.0	0.0	12.12	0.0	0.0	1.52	6.06	10.61	0.0	4.55	0.0	0.0	0.0	3.03	6.06	6.06	0.0	0.0	0.0	0.0	0.0	 n = j
0.0	0.0	1.09	1.09	0.54	8.7	0.0	0.0	15.22	0.54	2.17	0.0	21.74	0.0	7.61	22.28	1.09	7.07	0.0	0.0	0.0	0.0	6.52	0.54	0.54	3.26	0.0	 o = m
0.0	0.0	0.45	0.9	0.45	13.0	0.0	0.0	14.8	1.79	0.45	0.0	9.42	0.0	2.69	36.77	4.04	5.38	0.0	0.0	0.9	0.45	0.9	0.45	3.14	4.04	0.0	 p = l
0.11	0.0	10.0	1.81	0.96	24.15	0.21	0.0	7.77	2.45	0.74	0.21	1.38	0.0	1.38	7.13	29.89	1.81	0.0	0.43	0.96	0.53	5.85	0.53	1.06	0.64	0.0	 q = o
0.0	0.0	3.44	2.52	0.92	16.97	0.0	0.69	12.16	0.92	0.46	0.23	10.78	0.0	4.13	15.83	2.75	14.22	0.0	0.0	1.38	0.46	4.82	0.46	3.67	3.21	0.0	 r = n
0.0	0.0	34.55	1.82	0.0	9.09	0.0	0.0	3.64	0.0	1.82	0.0	14.55	0.0	0.0	0.0	0.0	1.82	1.82	0.0	27.27	0.0	0.0	0.0	0.0	3.64	0.0	 s = q
0.0	0.0	39.85	8.27	0.75	6.77	1.5	0.0	3.01	1.5	0.0	1.5	0.75	2.26	0.75	6.02	4.51	5.26	0.0	0.75	8.27	4.51	0.0	3.01	0.0	0.75	0.0	 t = p
0.49	0.0	37.58	1.96	0.49	5.88	0.0	0.0	2.94	0.0	0.0	0.33	5.88	0.49	0.33	4.25	2.12	2.94	0.16	0.33	29.08	1.96	0.33	1.31	0.0	1.14	0.0	 u = s
0.0	0.0	11.69	4.55	0.0	24.35	0.0	0.0	11.36	0.97	0.65	1.3	4.55	0.32	0.0	19.16	5.52	4.22	0.0	0.0	1.62	3.25	1.62	0.65	3.57	0.65	0.0	 v = r
0.0	0.35	1.75	0.35	1.05	11.23	0.0	0.0	7.72	0.35	1.4	0.35	0.0	0.0	3.86	8.77	14.74	1.75	0.0	0.35	1.05	0.0	42.81	0.35	0.35	1.05	0.35	 w = u
0.85	0.43	48.72	1.71	0.43	3.42	0.43	0.0	0.85	0.43	1.28	1.28	1.71	0.0	0.85	1.71	2.56	7.26	0.0	0.0	12.39	1.28	2.14	6.41	1.71	2.14	0.0	 x = t
0.0	1.01	1.01	1.01	8.08	9.09	0.0	1.01	20.2	2.02	1.01	0.0	3.03	0.0	3.03	19.19	7.07	4.04	0.0	0.0	1.01	3.03	1.01	1.01	12.12	1.01	0.0	 y = v
0.0	0.0	4.69	0.0	0.0	0.0	0.0	0.0	3.13	0.0	0.0	0.0	3.13	0.0	0.0	42.19	1.56	7.81	0.0	0.0	7.81	3.13	1.56	3.13	0.0	21.88	0.0	 z = y
0.0	0.0	100.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 aa = z
