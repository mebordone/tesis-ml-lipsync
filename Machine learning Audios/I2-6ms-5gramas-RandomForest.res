Timestamp:2013-07-23-03:00:54
Inventario:I2
Intervalo:6ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I2-6ms-5gramas
Num Instances:  30449
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  45 4Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.2441





=== Stratified cross-validation ===

Correctly Classified Instances       25070               82.3344 %
Incorrectly Classified Instances      5379               17.6656 %
Kappa statistic                          0.7782
K&B Relative Info Score            2241103.7172 %
K&B Information Score                64700.7602 bits      2.1249 bits/instance
Class complexity | order 0           87884.2027 bits      2.8863 bits/instance
Class complexity | scheme          1319581.5982 bits     43.3374 bits/instance
Complexity improvement     (Sf)    -1231697.3954 bits    -40.4512 bits/instance
Mean absolute error                      0.0499
Root mean squared error                  0.1511
Relative absolute error                 37.3853 %
Root relative squared error             58.4724 %
Coverage of cases (0.95 level)          96.0229 %
Mean rel. region size (0.95 level)      22.4271 %
Total Number of Instances            30449     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,898    0,037    0,819      0,898    0,856      0,829    0,971     0,918     A
                 0,574    0,017    0,638      0,574    0,604      0,586    0,905     0,607     LGJKC
                 0,831    0,020    0,774      0,831    0,801      0,784    0,967     0,846     E
                 0,512    0,003    0,698      0,512    0,591      0,593    0,934     0,593     FV
                 0,803    0,009    0,791      0,803    0,797      0,789    0,966     0,837     I
                 0,756    0,015    0,804      0,756    0,779      0,762    0,943     0,814     O
                 0,682    0,039    0,651      0,682    0,666      0,630    0,933     0,693     SNRTY
                 0,923    0,059    0,906      0,923    0,915      0,861    0,975     0,957     0
                 0,610    0,005    0,801      0,610    0,693      0,690    0,927     0,699     BMP
                 0,609    0,002    0,836      0,609    0,705      0,710    0,965     0,737     DZ
                 0,792    0,002    0,898      0,792    0,841      0,840    0,975     0,874     U
                 0,734    0,005    0,851      0,734    0,788      0,782    0,964     0,818     SNRTXY
Weighted Avg.    0,823    0,036    0,823      0,823    0,821      0,789    0,961     0,860     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  4332    56    68     5    21    45   113   140    14     7     3    21 |     a = A
   137   858    50    11    31    28   171   162    15     0    13    19 |     b = LGJKC
   102    50  1950    10    33    25    74    57    13    10     5    18 |     c = E
    29    17    32   220    12    10    73    22     9     1     0     5 |     d = FV
    30    21    76     8   969     8    36    35    13     1     2     7 |     e = I
   131    33    48     5    11  1701    70   187    17    17     5    26 |     f = O
   203   105    79    23    37    78  1986   342    25     6    10    18 |     g = SNRTY
   168   113    79    17    53    82   317 10714    22     5    10    27 |     h = 0
    56    40    33     7    35    37    68    93   598     0     8     5 |     i = BMP
    32     6    37     3     6    39    23    12     4   276     3    12 |     j = DZ
    20    14    20     3     7    39    17    12    11     0   552     2 |     k = U
    51    31    49     3    10    24   103    44     6     7     4   914 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
89.78	1.16	1.41	0.1	0.44	0.93	2.34	2.9	0.29	0.15	0.06	0.44	 a = A
9.16	57.39	3.34	0.74	2.07	1.87	11.44	10.84	1.0	0.0	0.87	1.27	 b = LGJKC
4.35	2.13	83.08	0.43	1.41	1.07	3.15	2.43	0.55	0.43	0.21	0.77	 c = E
6.74	3.95	7.44	51.16	2.79	2.33	16.98	5.12	2.09	0.23	0.0	1.16	 d = FV
2.49	1.74	6.3	0.66	80.35	0.66	2.99	2.9	1.08	0.08	0.17	0.58	 e = I
5.82	1.47	2.13	0.22	0.49	75.57	3.11	8.31	0.76	0.76	0.22	1.16	 f = O
6.97	3.61	2.71	0.79	1.27	2.68	68.2	11.74	0.86	0.21	0.34	0.62	 g = SNRTY
1.45	0.97	0.68	0.15	0.46	0.71	2.73	92.31	0.19	0.04	0.09	0.23	 h = 0
5.71	4.08	3.37	0.71	3.57	3.78	6.94	9.49	61.02	0.0	0.82	0.51	 i = BMP
7.06	1.32	8.17	0.66	1.32	8.61	5.08	2.65	0.88	60.93	0.66	2.65	 j = DZ
2.87	2.01	2.87	0.43	1.0	5.6	2.44	1.72	1.58	0.0	79.2	0.29	 k = U
4.09	2.49	3.93	0.24	0.8	1.93	8.27	3.53	0.48	0.56	0.32	73.35	 l = SNRTXY
