Timestamp:2013-08-30-04:00:37
Inventario:I1
Intervalo:1ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I1-1ms-4gramas
Num Instances:  183401
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(34): class 
0Pitch(Hz)(97): class 
0RawPitch(80): class 
0SmPitch(85): class 
0Melogram(st)(88): class 
0ZCross(21): class 
0F1(Hz)(2728): class 
0F2(Hz)(3468): class 
0F3(Hz)(3630): class 
0F4(Hz)(3539): class 
1Int(dB)(34): class 
1Pitch(Hz)(99): class 
1RawPitch(67): class 
1SmPitch(82): class 
1Melogram(st)(86): class 
1ZCross(21): class 
1F1(Hz)(2720): class 
1F2(Hz)(3451): class 
1F3(Hz)(3623): class 
1F4(Hz)(3519): class 
2Int(dB)(32): class 
2Pitch(Hz)(96): class 
2RawPitch(64): class 
2SmPitch(88): class 
2Melogram(st)(89): class 
2ZCross(21): class 
2F1(Hz)(2707): class 
2F2(Hz)(3429): class 
2F3(Hz)(3620): class 
2F4(Hz)(3530): class 
3Int(dB)(33): class 
3Pitch(Hz)(95): class 
3RawPitch(63): class 
3SmPitch(93): class 
3Melogram(st)(85): class 
3ZCross(21): class 
3F1(Hz)(2711): class 
3F2(Hz)(3422): class 
3F3(Hz)(3617): class 
3F4(Hz)(3544): class 
class(13): 
LogScore Bayes: -2.198818786784448E7
LogScore BDeu: -3.1363891558713887E7
LogScore MDL: -2.893805772410485E7
LogScore ENTROPY: -2.462167414811855E7
LogScore AIC: -2.5333982148118544E7




=== Stratified cross-validation ===

Correctly Classified Instances      152653               83.2346 %
Incorrectly Classified Instances     30748               16.7654 %
Kappa statistic                          0.7862
K&B Relative Info Score            14449611.7805 %
K&B Information Score               416214.0466 bits      2.2694 bits/instance
Class complexity | order 0          528256.6685 bits      2.8803 bits/instance
Class complexity | scheme          1648508.251  bits      8.9885 bits/instance
Complexity improvement     (Sf)    -1120251.5826 bits     -6.1082 bits/instance
Mean absolute error                      0.0258
Root mean squared error                  0.1572
Relative absolute error                 21.1463 %
Root relative squared error             63.6157 %
Coverage of cases (0.95 level)          84.3332 %
Mean rel. region size (0.95 level)       8.0465 %
Total Number of Instances           183401     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,864    0,008    0,951      0,864    0,905      0,890    0,968     0,941     a
                 0,735    0,006    0,800      0,735    0,766      0,759    0,936     0,773     b
                 0,876    0,009    0,891      0,876    0,883      0,874    0,972     0,913     e
                 0,935    0,011    0,557      0,935    0,698      0,717    0,978     0,847     d
                 0,686    0,007    0,576      0,686    0,626      0,623    0,942     0,669     f
                 0,883    0,006    0,853      0,883    0,868      0,862    0,966     0,888     i
                 0,257    0,007    0,405      0,257    0,315      0,312    0,935     0,350     k
                 0,782    0,006    0,605      0,782    0,682      0,684    0,940     0,751     j
                 0,712    0,012    0,859      0,712    0,779      0,762    0,950     0,822     l
                 0,826    0,007    0,897      0,826    0,860      0,850    0,938     0,873     o
                 0,905    0,123    0,827      0,905    0,864      0,772    0,973     0,964     0
                 0,592    0,015    0,714      0,592    0,648      0,631    0,974     0,746     s
                 0,929    0,004    0,842      0,929    0,884      0,882    0,984     0,939     u
Weighted Avg.    0,832    0,054    0,835      0,832    0,830      0,787    0,965     0,895     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 24679   165   117   336   177    95   420   221   436   181  1391   278    61 |     a = a
    45  4272    27     1    12   105    62     9    52    66  1013   109    41 |     b = b
    57   122 12035   150   111    41   140    78   283    42   532   105    44 |     c = e
    16     1    15  2507     0     0     5     0    18    16    89     8     7 |     d = d
    13     0    24     0  1757    32    28     0    17     9   602    76     3 |     e = f
    10    86    19    17    42  6255    67    14   103    23   328   114     5 |     f = i
     8    19    56     5     3    16   915     4    64    37  1718   665    45 |     g = k
    11     0    23     2     0     5    31  1612     2     3   255   114     4 |     h = j
   350   113   397   171    46   153   171    74 12319   239  2569   555   135 |     i = l
    62   124     7   301    38    28    85    24   152 10931  1411    55    16 |     j = o
   612   396   725   886   830   545   254   560   762   573 65266   392   313 |     k = 0
    91    14    55   115    25    56    40    44    70    62  3692  6261    45 |     l = s
     0    30     7     6     9     4    41    25    70     6    59    36  3844 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
86.42	0.58	0.41	1.18	0.62	0.33	1.47	0.77	1.53	0.63	4.87	0.97	0.21	 a = a
0.77	73.48	0.46	0.02	0.21	1.81	1.07	0.15	0.89	1.14	17.42	1.87	0.71	 b = b
0.41	0.89	87.59	1.09	0.81	0.3	1.02	0.57	2.06	0.31	3.87	0.76	0.32	 c = e
0.6	0.04	0.56	93.48	0.0	0.0	0.19	0.0	0.67	0.6	3.32	0.3	0.26	 d = d
0.51	0.0	0.94	0.0	68.61	1.25	1.09	0.0	0.66	0.35	23.51	2.97	0.12	 e = f
0.14	1.21	0.27	0.24	0.59	88.31	0.95	0.2	1.45	0.32	4.63	1.61	0.07	 f = i
0.23	0.53	1.58	0.14	0.08	0.45	25.74	0.11	1.8	1.04	48.33	18.71	1.27	 g = k
0.53	0.0	1.12	0.1	0.0	0.24	1.5	78.18	0.1	0.15	12.37	5.53	0.19	 h = j
2.02	0.65	2.3	0.99	0.27	0.88	0.99	0.43	71.24	1.38	14.86	3.21	0.78	 i = l
0.47	0.94	0.05	2.27	0.29	0.21	0.64	0.18	1.15	82.6	10.66	0.42	0.12	 j = o
0.85	0.55	1.01	1.23	1.15	0.76	0.35	0.78	1.06	0.79	90.5	0.54	0.43	 k = 0
0.86	0.13	0.52	1.09	0.24	0.53	0.38	0.42	0.66	0.59	34.93	59.23	0.43	 l = s
0.0	0.73	0.17	0.15	0.22	0.1	0.99	0.6	1.69	0.15	1.43	0.87	92.92	 m = u
