Timestamp:2013-07-22-23:06:41
Inventario:I4
Intervalo:15ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I4-15ms-4gramas
Num Instances:  12186
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3251





=== Stratified cross-validation ===

Correctly Classified Instances        8921               73.207  %
Incorrectly Classified Instances      3265               26.793  %
Kappa statistic                          0.6546
K&B Relative Info Score             751620.5638 %
K&B Information Score                19545.3287 bits      1.6039 bits/instance
Class complexity | order 0           31670.8788 bits      2.599  bits/instance
Class complexity | scheme           712269.0382 bits     58.4498 bits/instance
Complexity improvement     (Sf)    -680598.1593 bits    -55.8508 bits/instance
Mean absolute error                      0.0745
Root mean squared error                  0.1925
Relative absolute error                 47.4721 %
Root relative squared error             68.6884 %
Coverage of cases (0.95 level)          94.6332 %
Mean rel. region size (0.95 level)      28.6673 %
Total Number of Instances            12186     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,319    0,001    0,657      0,319    0,430      0,456    0,887     0,416     C
                 0,656    0,039    0,595      0,656    0,624      0,590    0,922     0,632     E
                 0,192    0,004    0,391      0,192    0,258      0,267    0,825     0,180     FV
                 0,791    0,089    0,696      0,791    0,740      0,670    0,932     0,820     AI
                 0,655    0,100    0,560      0,655    0,604      0,522    0,885     0,621     CDGKNRSYZ
                 0,563    0,024    0,659      0,563    0,607      0,579    0,910     0,632     O
                 0,920    0,050    0,913      0,920    0,916      0,869    0,978     0,959     0
                 0,206    0,011    0,425      0,206    0,277      0,277    0,824     0,242     LT
                 0,551    0,003    0,831      0,551    0,662      0,670    0,923     0,690     U
                 0,298    0,005    0,650      0,298    0,409      0,428    0,866     0,396     MBP
Weighted Avg.    0,732    0,058    0,728      0,732    0,723      0,675    0,930     0,758     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   23   13    0   16   13    6    1    0    0    0 |    a = C
    6  637    6  149  108   27   16   13    0    9 |    b = E
    0   17   34   26   69   13   10    5    0    3 |    c = FV
    2  139   11 1971  216   53   65   20    4   11 |    d = AI
    1  113   16  273 1296   60  146   52    8   13 |    e = CDGKNRSYZ
    2   51    4  127  134  529   68    8    9    8 |    f = O
    0   26    8   72  194   27 4061   18    2    7 |    g = 0
    0   35    3   92  150   17   56   94    2    8 |    h = LT
    0   13    2   28   29   47    0    4  157    5 |    i = U
    1   27    3   79  107   24   25    7    7  119 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
31.94	18.06	0.0	22.22	18.06	8.33	1.39	0.0	0.0	0.0	 a = C
0.62	65.6	0.62	15.35	11.12	2.78	1.65	1.34	0.0	0.93	 b = E
0.0	9.6	19.21	14.69	38.98	7.34	5.65	2.82	0.0	1.69	 c = FV
0.08	5.58	0.44	79.09	8.67	2.13	2.61	0.8	0.16	0.44	 d = AI
0.05	5.71	0.81	13.8	65.52	3.03	7.38	2.63	0.4	0.66	 e = CDGKNRSYZ
0.21	5.43	0.43	13.51	14.26	56.28	7.23	0.85	0.96	0.85	 f = O
0.0	0.59	0.18	1.63	4.39	0.61	91.98	0.41	0.05	0.16	 g = 0
0.0	7.66	0.66	20.13	32.82	3.72	12.25	20.57	0.44	1.75	 h = LT
0.0	4.56	0.7	9.82	10.18	16.49	0.0	1.4	55.09	1.75	 i = U
0.25	6.77	0.75	19.8	26.82	6.02	6.27	1.75	1.75	29.82	 j = MBP
