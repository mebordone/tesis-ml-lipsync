Timestamp:2013-07-22-22:33:21
Inventario:I3
Intervalo:20ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I3-20ms-5gramas
Num Instances:  9125
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 4Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  42 4Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  43 4RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  44 4SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  45 4Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  47 4F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  48 4F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  49 4F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  50 4F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3499





=== Stratified cross-validation ===

Correctly Classified Instances        6557               71.8575 %
Incorrectly Classified Instances      2568               28.1425 %
Kappa statistic                          0.641 
K&B Relative Info Score             546394.2268 %
K&B Information Score                14595.2958 bits      1.5995 bits/instance
Class complexity | order 0           24346.2677 bits      2.6681 bits/instance
Class complexity | scheme           557771.5705 bits     61.1257 bits/instance
Complexity improvement     (Sf)    -533425.3028 bits    -58.4576 bits/instance
Mean absolute error                      0.0715
Root mean squared error                  0.1883
Relative absolute error                 49.5479 %
Root relative squared error             70.1013 %
Coverage of cases (0.95 level)          94.389  %
Mean rel. region size (0.95 level)      27.4192 %
Total Number of Instances             9125     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,773    0,070    0,686      0,773    0,727      0,671    0,933     0,810     A
                 0,614    0,044    0,557      0,614    0,584      0,546    0,915     0,583     E
                 0,700    0,134    0,553      0,700    0,618      0,519    0,879     0,613     CGJLNQSRT
                 0,051    0,001    0,412      0,051    0,090      0,140    0,761     0,136     FV
                 0,517    0,012    0,648      0,517    0,575      0,562    0,916     0,539     I
                 0,510    0,026    0,623      0,510    0,561      0,530    0,893     0,579     O
                 0,914    0,041    0,924      0,914    0,919      0,875    0,978     0,958     0
                 0,214    0,006    0,542      0,214    0,307      0,328    0,818     0,276     BMP
                 0,500    0,003    0,779      0,500    0,609      0,617    0,915     0,606     U
                 0,200    0,000    1,000      0,200    0,333      0,447    0,699     0,280     DZ
                 0,090    0,002    0,444      0,090    0,150      0,195    0,818     0,159     D
Weighted Avg.    0,719    0,058    0,718      0,719    0,708      0,661    0,924     0,734     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1163   58  190    1    7   45   36    3    2    0    0 |    a = A
   75  458  140    2   26   26    9    5    4    0    1 |    b = E
  190  118 1225    2   35   40  115   14    8    0    4 |    c = CGJLNQSRT
   22   20   73    7    1    5    4    3    3    0    0 |    d = FV
   16   64   83    3  199    3    7    9    0    0    1 |    e = I
  114   41  118    0    4  363   53    8    4    0    7 |    f = O
   31   14  184    2   10   28 2956    6    2    0    0 |    g = 0
   38   19  108    0   21   24   18   64    7    0    0 |    h = BMP
   19   14   39    0    3   27    1    4  109    0    2 |    i = U
    0    1    3    0    0    0    0    0    0    1    0 |    j = DZ
   27   16   52    0    1   22    0    2    1    0   12 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
77.28	3.85	12.62	0.07	0.47	2.99	2.39	0.2	0.13	0.0	0.0	 a = A
10.05	61.39	18.77	0.27	3.49	3.49	1.21	0.67	0.54	0.0	0.13	 b = E
10.85	6.74	69.96	0.11	2.0	2.28	6.57	0.8	0.46	0.0	0.23	 c = CGJLNQSRT
15.94	14.49	52.9	5.07	0.72	3.62	2.9	2.17	2.17	0.0	0.0	 d = FV
4.16	16.62	21.56	0.78	51.69	0.78	1.82	2.34	0.0	0.0	0.26	 e = I
16.01	5.76	16.57	0.0	0.56	50.98	7.44	1.12	0.56	0.0	0.98	 f = O
0.96	0.43	5.69	0.06	0.31	0.87	91.43	0.19	0.06	0.0	0.0	 g = 0
12.71	6.35	36.12	0.0	7.02	8.03	6.02	21.4	2.34	0.0	0.0	 h = BMP
8.72	6.42	17.89	0.0	1.38	12.39	0.46	1.83	50.0	0.0	0.92	 i = U
0.0	20.0	60.0	0.0	0.0	0.0	0.0	0.0	0.0	20.0	0.0	 j = DZ
20.3	12.03	39.1	0.0	0.75	16.54	0.0	1.5	0.75	0.0	9.02	 k = D
