Timestamp:2013-08-30-05:28:30
Inventario:I4
Intervalo:10ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I4-10ms-2gramas
Num Instances:  18295
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(10): class 
0Pitch(Hz)(7): class 
0RawPitch(5): class 
0SmPitch(7): class 
0Melogram(st)(9): class 
0ZCross(10): class 
0F1(Hz)(13): class 
0F2(Hz)(10): class 
0F3(Hz)(9): class 
0F4(Hz)(9): class 
1Int(dB)(11): class 
1Pitch(Hz)(7): class 
1RawPitch(5): class 
1SmPitch(5): class 
1Melogram(st)(9): class 
1ZCross(11): class 
1F1(Hz)(13): class 
1F2(Hz)(13): class 
1F3(Hz)(9): class 
1F4(Hz)(8): class 
class(10): 
LogScore Bayes: -461795.87983257725
LogScore BDeu: -469021.72349913546
LogScore MDL: -468468.02869013
LogScore ENTROPY: -460572.3575043188
LogScore AIC: -462181.3575043188




=== Stratified cross-validation ===

Correctly Classified Instances       10714               58.5624 %
Incorrectly Classified Instances      7581               41.4376 %
Kappa statistic                          0.4633
K&B Relative Info Score             855129.366  %
K&B Information Score                22089.844  bits      1.2074 bits/instance
Class complexity | order 0           47241.9618 bits      2.5822 bits/instance
Class complexity | scheme           121353.9894 bits      6.6332 bits/instance
Complexity improvement     (Sf)     -74112.0276 bits     -4.0509 bits/instance
Mean absolute error                      0.0848
Root mean squared error                  0.2607
Relative absolute error                 54.3079 %
Root relative squared error             93.3147 %
Coverage of cases (0.95 level)          69.7568 %
Mean rel. region size (0.95 level)      17.6179 %
Total Number of Instances            18295     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,290    0,024    0,066      0,290    0,107      0,128    0,851     0,111     C
                 0,617    0,102    0,341      0,617    0,439      0,397    0,879     0,452     E
                 0,096    0,010    0,121      0,096    0,107      0,096    0,782     0,065     FV
                 0,590    0,096    0,609      0,590    0,599      0,500    0,874     0,695     AI
                 0,159    0,046    0,395      0,159    0,227      0,168    0,783     0,362     CDGKNRSYZ
                 0,403    0,040    0,449      0,403    0,425      0,381    0,839     0,424     O
                 0,919    0,141    0,794      0,919    0,852      0,759    0,965     0,957     0
                 0,030    0,008    0,120      0,030    0,047      0,042    0,758     0,092     LT
                 0,511    0,019    0,385      0,511    0,439      0,428    0,923     0,492     U
                 0,155    0,019    0,212      0,155    0,179      0,158    0,775     0,133     MBP
Weighted Avg.    0,586    0,092    0,564      0,586    0,560      0,488    0,883     0,642     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   31   41    0   15    4    4    5    0    5    2 |    a = C
   84  887   13  213   79   32   59   20   24   27 |    b = E
    9   54   25   32   23   31   56    3    6   22 |    c = FV
  108  600   34 2175  223  181  183   33   40  111 |    d = AI
  128  443   55  523  466  212  877   39   89  101 |    e = CDGKNRSYZ
   41  154   10  283   67  555  142   15   98   12 |    f = O
   17  114   33  135  151   56 6247    8   12   24 |    g = 0
   10  144   17   74  101   57  200   20   24   30 |    h = LT
   15   41    5   37   13   62    9   13  216   12 |    i = U
   28  121   14   85   52   47   92   16   47   92 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
28.97	38.32	0.0	14.02	3.74	3.74	4.67	0.0	4.67	1.87	 a = C
5.84	61.68	0.9	14.81	5.49	2.23	4.1	1.39	1.67	1.88	 b = E
3.45	20.69	9.58	12.26	8.81	11.88	21.46	1.15	2.3	8.43	 c = FV
2.93	16.27	0.92	58.98	6.05	4.91	4.96	0.89	1.08	3.01	 d = AI
4.36	15.1	1.88	17.83	15.89	7.23	29.9	1.33	3.03	3.44	 e = CDGKNRSYZ
2.98	11.18	0.73	20.55	4.87	40.31	10.31	1.09	7.12	0.87	 f = O
0.25	1.68	0.49	1.99	2.22	0.82	91.91	0.12	0.18	0.35	 g = 0
1.48	21.27	2.51	10.93	14.92	8.42	29.54	2.95	3.55	4.43	 h = LT
3.55	9.69	1.18	8.75	3.07	14.66	2.13	3.07	51.06	2.84	 i = U
4.71	20.37	2.36	14.31	8.75	7.91	15.49	2.69	7.91	15.49	 j = MBP
