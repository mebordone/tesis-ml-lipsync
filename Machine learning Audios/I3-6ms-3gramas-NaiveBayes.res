Timestamp:2014-12-07-11:09:24
Inventario:I3
Intervalo:6ms
Ngramas:3
Clasificador:NaiveBayes
Relation Name:  I3-6ms-3gramas
Num Instances:  30451
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Naive Bayes Classifier

                    Class
Attribute               A         E CGJLNQSRT        FV         I         O         0       BMP         U        DZ         D
                   (0.16)    (0.08)    (0.19)    (0.01)    (0.04)    (0.07)    (0.38)    (0.03)    (0.02)       (0)    (0.01)
==============================================================================================================================
0Int(dB)
  mean             -4.5987   -5.8995  -16.6052  -14.5611   -9.5588   -8.5713   -48.557  -12.9966    -6.419  -27.1266   -7.7601
  std. dev.        10.0572     8.786   11.1368    9.4214    8.7617   12.1108   16.0879   11.3571    9.2206    2.0567    5.9525
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001

0Pitch(Hz)
  mean            213.9858  185.9675  119.7909  138.3865   168.922  178.5044   16.3299  162.1914  225.2933         0  202.6766
  std. dev.       108.6894   90.3606  112.5363  108.2488   80.1793  106.4846   55.6006  111.4338   91.5415    0.0243   83.2781
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457

0RawPitch
  mean            203.4289  181.6948  102.6949  114.6567   160.411  171.1249   13.5133  148.0422  215.3066         0  197.4072
  std. dev.       114.2033   96.2139   111.998  105.4998   84.8193  112.4932   50.0924  115.6256   98.6174    0.0248   92.3001
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486

0SmPitch
  mean            211.6839  183.5596  113.6264  128.8353  166.5988  175.9059   14.8061  158.6549  223.7844         0   201.666
  std. dev.       110.6731   92.3488  112.7868  105.8213   81.8868  108.6981   51.7803   113.938   92.4136    0.0246   85.2848
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475

0Melogram(st)
  mean             51.0457   49.6417   31.6229   39.2686   47.3894   46.4994    4.8244   42.2976   52.9038    4.3435   53.1202
  std. dev.        17.3442   16.5413   27.1587   24.0506   17.6115   20.6431   15.4707   23.9194    16.369   14.4057   10.9727
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271

0ZCross
  mean              6.8934    4.8687    2.9077    2.0038    3.5256    3.9353    0.2426    2.3634    3.6666         0    3.6096
  std. dev.         4.5993    3.3612    4.6136    2.0839    3.3498     3.141    1.1445    2.1945    2.8883    0.1957    2.0875
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739

0F1(Hz)
  mean            658.1625  448.1303  340.5125  421.3901  348.8664  467.9136   53.9321  387.5882  478.0605         0  513.8187
  std. dev.       286.2026  167.3815  280.8604  272.3544  162.2662  235.6843  168.2103  237.6824   324.529    0.0605  149.6379
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628

0F2(Hz)
  mean           1419.2622 1706.5198 1086.9141 1239.1028 1853.8785 1129.6245  166.3315 1348.6957 1009.2259         0 1486.3363
  std. dev.       497.7941  612.0386   853.308  748.4208  761.7664  569.4748  512.7304  740.2835  490.7397    0.0742  303.8381
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452

0F3(Hz)
  mean           2605.2622 2528.8267 1846.3738 2115.5779 2527.1222  2440.143  285.4193  2271.328 2534.3382         0 2871.3859
  std. dev.       881.9027   904.012 1422.3879 1263.4416 1049.8927 1080.4541  863.1325 1184.8692  971.1663    0.1098  449.1931
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587

0F4(Hz)
  mean           3024.9811  3055.702 2112.2946 2508.5338 3015.9411 2810.7638  343.0735 2699.0169 2907.9441         0 3342.0954
  std. dev.      1010.9519 1062.5316  1616.999 1464.9089 1214.2293 1230.5926 1035.4709 1377.3282 1043.9573    0.1133  484.4215
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801

1Int(dB)
  mean             -4.3897   -5.4538  -16.7094  -14.9101   -9.0975     -8.32  -48.8223   -12.808   -5.7217  -27.2184    -7.865
  std. dev.         9.7701    8.3541   10.6707     9.056    8.4757   12.1084   15.7034   10.9801    8.3788    2.0706     6.066
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001

1Pitch(Hz)
  mean            215.1858  188.9309  118.3414  135.5009  172.4627  179.6002   15.2638  162.0524  229.1165         0  202.2266
  std. dev.       107.7136   88.8484  112.5858  107.5102   77.3033  105.0982   53.8922  110.5525    86.113    0.0243    84.159
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457

1RawPitch
  mean            204.8304  185.2813  100.2234   111.142  164.6635  173.0353   12.5253  147.7901  219.1052         0  197.2802
  std. dev.       113.2593   94.3657  111.6138   103.983   82.3462  111.2194   48.0516  115.4056   94.0431    0.0248   92.7599
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486

1SmPitch
  mean            213.0105  187.4191  111.7144  126.3464   169.864  177.2873   13.7642  157.4726  227.6752         0   201.473
  std. dev.       109.5872   90.1622  112.7946  104.6501   79.4786  107.1683   49.9351  113.6823   87.0875    0.0246   85.6758
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475

1Melogram(st)
  mean             51.4062    50.443   31.0579    38.768   48.4372   46.7991    4.5871   42.0662   54.1423         0   53.1699
  std. dev.        16.8175   15.4134   27.2649   24.2654   16.3134   20.2997   15.1428   23.9804    14.241    0.0045   10.7286
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271

1ZCross
  mean              6.9598    4.9057    2.8938    1.9165    3.5597    3.9697    0.2049    2.3311    3.7441         0    3.6229
  std. dev.         4.5644    3.2087    4.7395    2.0253    3.2377    2.9459    0.9759    2.0986    2.8259    0.1957    2.1011
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739

1F1(Hz)
  mean            671.8022  458.4701  325.7263  405.4793  358.3341  479.3066   50.4031  382.1121  490.8774         0  515.3751
  std. dev.       275.7695  153.5049  279.5075  275.9084  149.9822  228.1739   162.571  236.5551  313.8193    0.0605  150.3453
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628

1F2(Hz)
  mean           1436.7855 1756.5682 1050.4025  1210.404 1934.2548 1142.3274  156.3707 1334.4278 1039.7614         0 1485.2782
  std. dev.       469.7671  558.7593  858.7423  760.5497    698.11  545.8877  498.6315  740.0834  453.9752    0.0742  309.9774
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452

1F3(Hz)
  mean           2640.8558 2589.9599 1785.9434 2065.6755  2617.135   2486.42  267.8531 2248.5273 2624.1187         0 2868.7121
  std. dev.       835.2949  828.9033 1435.4338 1286.9009  961.8845 1037.8263  838.7348 1187.9068   862.036    0.1098  468.1374
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587

1F4(Hz)
  mean           3063.4806 3123.7667 2045.6167 2456.6943 3116.8532 2862.0839  322.5326 2685.0292 3008.5041         0 3338.8508
  std. dev.       955.4282   967.012 1634.6612 1495.5033 1104.1679 1178.6146 1007.8515 1387.1463  904.6428    0.1133  509.6049
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801

2Int(dB)
  mean             -4.3746   -5.3083  -16.5509  -14.9621   -8.8383   -8.2927  -49.0435  -12.3369   -5.2124  -27.3626   -7.7311
  std. dev.         9.6808    8.1563   10.3341    8.8347    8.4087   12.1905   15.4144    10.602    7.6533    2.3916    6.1548
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1027    0.1027    0.1027    0.1027    0.1027    0.1027    0.1027    0.1027    0.1027    0.1027    0.1027

2Pitch(Hz)
  mean            215.2971  191.0581  119.4974  133.3692  173.8339  178.5747   14.1324  163.7124  230.7901         0   201.883
  std. dev.       107.7031   87.4612  112.8369  106.8281   75.6506  104.7438    52.013  108.8774   82.8574    0.0243    84.869
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457

2RawPitch
  mean            204.9722  186.7023   100.606  109.9149  166.6698  172.5426   11.6302  150.0446  221.3767         0  196.4472
  std. dev.       113.3158   93.4491  111.6521  102.8897   81.1132  110.6448   46.5627   114.114   91.1707    0.0248    92.771
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486

2SmPitch
  mean            213.2402  189.6338  112.4515   124.407  171.4037  176.5059    12.707  158.6198  229.7721         0  201.3593
  std. dev.       109.3851   88.7774  113.0799   103.769   78.1033   106.636   48.0587  112.4568   83.4441    0.0246    85.928
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475

2Melogram(st)
  mean              51.533   50.8127   31.2207   38.4205   48.9436   46.6912     4.292    42.399   54.8522         0     53.14
  std. dev.        16.5965   14.8245   27.3134   24.4262   15.5254   20.3122   14.7028   23.6994   12.6632    0.0045   10.7598
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271

2ZCross
  mean              6.9593    4.8852     2.911    1.9001    3.5568    3.9781    0.1869    2.4041    3.8114         0    3.7028
  std. dev.         4.5717    3.1792    4.7748    2.0075    3.1167    2.8849     0.942    2.0753    2.7838    0.1957    2.0838
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739

2F1(Hz)
  mean            682.1071   466.462  316.1813  391.2913  366.1781  486.7294   46.8644  381.3506  499.9719         0  517.4169
  std. dev.       268.0102  141.9729  277.1652  276.1702  138.5016  223.3891  156.9447   233.551  304.4022    0.0605  150.5159
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628

2F2(Hz)
  mean           1448.2374 1796.4609 1028.9839 1193.1191 2002.2125  1145.619  145.6726 1332.6509 1063.4579         0 1484.5553
  std. dev.       448.6029  511.4411  859.8842  771.0706  633.8216   528.768  482.7197  730.2324  420.6329    0.0742  315.1872
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452

2F3(Hz)
  mean           2665.0519 2636.0758 1751.4014 2027.9477 2693.6179  2513.778  249.4006 2248.6819 2692.0225         0 2867.4021
  std. dev.       801.5039  763.9923   1440.86 1301.1049  876.6153 1011.4708  812.1111 1174.9913   759.659    0.1098  487.3711
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587

2F4(Hz)
  mean            3089.112 3175.4825 2009.2839 2421.4135 3200.0296 2890.9482  300.6818 2697.4569 3085.5469         0 3334.3495
  std. dev.       915.0775  884.6328 1644.2265 1516.9857  997.3182 1145.4649  976.9137 1376.7352  770.8999    0.1133  533.4148
  weight sum          4825      2347      5653       430      1206      2251     11609       980       697        12       441
  precision         0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801





=== Stratified cross-validation ===

Correctly Classified Instances       10434               34.2649 %
Incorrectly Classified Instances     20017               65.7351 %
Kappa statistic                          0.2546
K&B Relative Info Score             722447.7475 %
K&B Information Score                18888.1797 bits      0.6203 bits/instance
Class complexity | order 0           79593.2678 bits      2.6138 bits/instance
Class complexity | scheme          1494563.9482 bits     49.0809 bits/instance
Complexity improvement     (Sf)    -1414970.6804 bits    -46.4671 bits/instance
Mean absolute error                      0.1198
Root mean squared error                  0.3298
Relative absolute error                 84.4838 %
Root relative squared error            123.8359 %
Coverage of cases (0.95 level)          42.2876 %
Mean rel. region size (0.95 level)      13.5237 %
Total Number of Instances            30451     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,417    0,042    0,652      0,417    0,509      0,454    0,852     0,573     A
                 0,277    0,029    0,440      0,277    0,340      0,307    0,835     0,341     E
                 0,166    0,036    0,511      0,166    0,251      0,212    0,650     0,377     CGJLNQSRT
                 0,065    0,021    0,043      0,065    0,052      0,036    0,668     0,029     FV
                 0,342    0,029    0,324      0,342    0,333      0,305    0,856     0,277     I
                 0,009    0,003    0,185      0,009    0,017      0,025    0,726     0,162     O
                 0,501    0,032    0,906      0,501    0,645      0,559    0,882     0,850     0
                 0,010    0,007    0,050      0,010    0,017      0,008    0,678     0,055     BMP
                 0,291    0,009    0,426      0,291    0,346      0,340    0,859     0,310     U
                 1,000    0,214    0,002      1,000    0,004      0,038    0,895     0,002     DZ
                 0,760    0,269    0,040      0,760    0,076      0,131    0,829     0,051     D
Weighted Avg.    0,343    0,034    0,617      0,343    0,424      0,372    0,807     0,544     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 2012  169  187   99   90   31   74   19   42  166 1936 |    a = A
  228  649  124   29  132    0   13   24   29   77 1042 |    b = E
  304  193  938  218  300   32  408   71   44 1147 1998 |    c = CGJLNQSRT
   29   20   39   28   23    0   24    3    2   70  192 |    d = FV
   65  291   91   17  412    0   17   16    4   45  248 |    e = I
  244   66  117   88   29   20   21   16  118  214 1318 |    f = O
   25   45  210  121  137   16 5815   30   11 4646  553 |    g = 0
   59   25   83   25  115    4   35   10   20  129  475 |    h = BMP
   67    6   39   16   21    5    5    8  203    8  319 |    i = U
    0    0    0    0    0    0    0    0    0   12    0 |    j = DZ
   52   11    8   11   11    0    3    5    4    1  335 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
41.7	3.5	3.88	2.05	1.87	0.64	1.53	0.39	0.87	3.44	40.12	 a = A
9.71	27.65	5.28	1.24	5.62	0.0	0.55	1.02	1.24	3.28	44.4	 b = E
5.38	3.41	16.59	3.86	5.31	0.57	7.22	1.26	0.78	20.29	35.34	 c = CGJLNQSRT
6.74	4.65	9.07	6.51	5.35	0.0	5.58	0.7	0.47	16.28	44.65	 d = FV
5.39	24.13	7.55	1.41	34.16	0.0	1.41	1.33	0.33	3.73	20.56	 e = I
10.84	2.93	5.2	3.91	1.29	0.89	0.93	0.71	5.24	9.51	58.55	 f = O
0.22	0.39	1.81	1.04	1.18	0.14	50.09	0.26	0.09	40.02	4.76	 g = 0
6.02	2.55	8.47	2.55	11.73	0.41	3.57	1.02	2.04	13.16	48.47	 h = BMP
9.61	0.86	5.6	2.3	3.01	0.72	0.72	1.15	29.12	1.15	45.77	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	 j = DZ
11.79	2.49	1.81	2.49	2.49	0.0	0.68	1.13	0.91	0.23	75.96	 k = D
