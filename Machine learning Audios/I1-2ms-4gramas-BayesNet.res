Timestamp:2013-08-30-04:09:56
Inventario:I1
Intervalo:2ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I1-2ms-4gramas
Num Instances:  91700
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(25): class 
0Pitch(Hz)(41): class 
0RawPitch(30): class 
0SmPitch(35): class 
0Melogram(st)(47): class 
0ZCross(17): class 
0F1(Hz)(1587): class 
0F2(Hz)(2331): class 
0F3(Hz)(2217): class 
0F4(Hz)(2488): class 
1Int(dB)(24): class 
1Pitch(Hz)(43): class 
1RawPitch(29): class 
1SmPitch(37): class 
1Melogram(st)(46): class 
1ZCross(17): class 
1F1(Hz)(1647): class 
1F2(Hz)(2312): class 
1F3(Hz)(2304): class 
1F4(Hz)(2524): class 
2Int(dB)(22): class 
2Pitch(Hz)(46): class 
2RawPitch(29): class 
2SmPitch(36): class 
2Melogram(st)(45): class 
2ZCross(16): class 
2F1(Hz)(1612): class 
2F2(Hz)(2243): class 
2F3(Hz)(2379): class 
2F4(Hz)(2445): class 
3Int(dB)(25): class 
3Pitch(Hz)(45): class 
3RawPitch(31): class 
3SmPitch(39): class 
3Melogram(st)(44): class 
3ZCross(17): class 
3F1(Hz)(1439): class 
3F2(Hz)(2328): class 
3F3(Hz)(2303): class 
3F4(Hz)(2522): class 
class(13): 
LogScore Bayes: -1.001519448006355E7
LogScore BDeu: -1.5800452346271846E7
LogScore MDL: -1.4263740342691539E7
LogScore ENTROPY: -1.1632479984134486E7
LogScore AIC: -1.2093042984134488E7




=== Stratified cross-validation ===

Correctly Classified Instances       73487               80.1385 %
Incorrectly Classified Instances     18213               19.8615 %
Kappa statistic                          0.7476
K&B Relative Info Score            6877324.3141 %
K&B Information Score               198533.1032 bits      2.165  bits/instance
Class complexity | order 0          264694.7989 bits      2.8865 bits/instance
Class complexity | scheme           889263.417  bits      9.6975 bits/instance
Complexity improvement     (Sf)    -624568.6181 bits     -6.811  bits/instance
Mean absolute error                      0.0308
Root mean squared error                  0.1692
Relative absolute error                 25.1311 %
Root relative squared error             68.3797 %
Coverage of cases (0.95 level)          82.1614 %
Mean rel. region size (0.95 level)       8.3949 %
Total Number of Instances            91700     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,820    0,014    0,917      0,820    0,866      0,845    0,961     0,919     a
                 0,684    0,006    0,777      0,684    0,728      0,721    0,905     0,728     b
                 0,847    0,017    0,802      0,847    0,824      0,810    0,963     0,876     e
                 0,884    0,016    0,450      0,884    0,596      0,624    0,972     0,807     d
                 0,631    0,008    0,537      0,631    0,580      0,576    0,910     0,626     f
                 0,857    0,011    0,767      0,857    0,809      0,802    0,960     0,857     i
                 0,213    0,008    0,341      0,213    0,262      0,258    0,923     0,298     k
                 0,725    0,007    0,558      0,725    0,630      0,631    0,919     0,714     j
                 0,643    0,015    0,822      0,643    0,721      0,702    0,912     0,761     l
                 0,775    0,010    0,863      0,775    0,817      0,805    0,927     0,840     o
                 0,901    0,123    0,824      0,901    0,861      0,767    0,968     0,960     0
                 0,531    0,015    0,682      0,531    0,597      0,581    0,962     0,677     s
                 0,910    0,006    0,789      0,910    0,845      0,843    0,978     0,920     u
Weighted Avg.    0,801    0,056    0,805      0,801    0,799      0,754    0,954     0,868     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 11759   122   227   306   149   106   144    92   297   178   706   182    65 |     a = a
    37  1994    65    15    11    59    34    13    28    42   508    62    47 |     b = b
    68    56  5851   121    52    67    76    53   165    28   272    75    26 |     c = e
    27     2    27  1190     7     4    13     5     8    10    42     7     4 |     d = d
    18     8    33    17   811    25    26     3     6    10   293    33     3 |     e = f
    10    25    52    20    23  3050    62    20    57    11   163    65     3 |     f = i
    12     9    34    12     6    18   379    12    31    22   907   315    23 |     g = k
    10     1    41    14     1    13    13   750     2     0   130    56     4 |     h = j
   312    71   360   192    23   151   107    58  5577   112  1322   263   132 |     i = l
   130    62    92   203    25    23    67    43    87  5153   700    33    28 |     j = o
   356   172   443   408   341   340   142   252   431   357 32269   200   119 |     k = 0
    82    31    59   132    57   115    24    36    53    40  1805  2812    52 |     l = s
     2    12     7    16     3     6    24     8    46     9    36    19  1892 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
82.04	0.85	1.58	2.13	1.04	0.74	1.0	0.64	2.07	1.24	4.93	1.27	0.45	 a = a
1.27	68.4	2.23	0.51	0.38	2.02	1.17	0.45	0.96	1.44	17.43	2.13	1.61	 b = b
0.98	0.81	84.67	1.75	0.75	0.97	1.1	0.77	2.39	0.41	3.94	1.09	0.38	 c = e
2.01	0.15	2.01	88.41	0.52	0.3	0.97	0.37	0.59	0.74	3.12	0.52	0.3	 d = d
1.4	0.62	2.57	1.32	63.06	1.94	2.02	0.23	0.47	0.78	22.78	2.57	0.23	 e = f
0.28	0.7	1.46	0.56	0.65	85.65	1.74	0.56	1.6	0.31	4.58	1.83	0.08	 f = i
0.67	0.51	1.91	0.67	0.34	1.01	21.29	0.67	1.74	1.24	50.96	17.7	1.29	 g = k
0.97	0.1	3.96	1.35	0.1	1.26	1.26	72.46	0.19	0.0	12.56	5.41	0.39	 h = j
3.59	0.82	4.15	2.21	0.26	1.74	1.23	0.67	64.25	1.29	15.23	3.03	1.52	 i = l
1.96	0.93	1.38	3.05	0.38	0.35	1.01	0.65	1.31	77.54	10.53	0.5	0.42	 j = o
0.99	0.48	1.24	1.14	0.95	0.95	0.4	0.7	1.2	1.0	90.06	0.56	0.33	 k = 0
1.55	0.59	1.11	2.49	1.08	2.17	0.45	0.68	1.0	0.76	34.07	53.08	0.98	 l = s
0.1	0.58	0.34	0.77	0.14	0.29	1.15	0.38	2.21	0.43	1.73	0.91	90.96	 m = u
