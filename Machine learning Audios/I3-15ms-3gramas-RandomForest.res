Timestamp:2013-07-22-22:31:50
Inventario:I3
Intervalo:15ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I3-15ms-3gramas
Num Instances:  12187
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.318





=== Stratified cross-validation ===

Correctly Classified Instances        9024               74.0461 %
Incorrectly Classified Instances      3163               25.9539 %
Kappa statistic                          0.6682
K&B Relative Info Score             772078.3092 %
K&B Information Score                20471.9529 bits      1.6798 bits/instance
Class complexity | order 0           32288.5474 bits      2.6494 bits/instance
Class complexity | scheme           750455.3315 bits     61.5783 bits/instance
Complexity improvement     (Sf)    -718166.7841 bits    -58.9289 bits/instance
Mean absolute error                      0.0659
Root mean squared error                  0.1818
Relative absolute error                 45.8656 %
Root relative squared error             67.8502 %
Coverage of cases (0.95 level)          94.3382 %
Mean rel. region size (0.95 level)      25.2631 %
Total Number of Instances            12187     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,797    0,058    0,730      0,797    0,762      0,714    0,941     0,841     A
                 0,662    0,037    0,609      0,662    0,635      0,602    0,926     0,648     E
                 0,695    0,121    0,575      0,695    0,629      0,535    0,887     0,638     CGJLNQSRT
                 0,119    0,002    0,412      0,119    0,184      0,215    0,773     0,141     FV
                 0,588    0,011    0,696      0,588    0,638      0,626    0,924     0,625     I
                 0,562    0,027    0,635      0,562    0,596      0,566    0,897     0,622     O
                 0,909    0,046    0,918      0,909    0,914      0,865    0,975     0,954     0
                 0,323    0,007    0,594      0,323    0,419      0,425    0,850     0,380     BMP
                 0,537    0,003    0,836      0,537    0,654      0,664    0,930     0,668     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,498     0,000     DZ
                 0,203    0,002    0,571      0,203    0,300      0,336    0,875     0,304     D
Weighted Avg.    0,740    0,055    0,741      0,740    0,734      0,687    0,931     0,765     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1586   68  196    0   13   53   59    8    2    0    4 |    a = A
   84  643  157    2   29   30   11   11    1    1    2 |    b = E
  233  137 1616   11   42   81  171   19    6    2    7 |    c = CGJLNQSRT
   15   27   89   21    2    7    9    3    2    0    2 |    d = FV
   15   72   88    2  296    1   18    9    1    0    1 |    e = I
  114   38  151    1    4  528   66   17   12    0    9 |    f = O
   50   15  271    7   11   34 4016   11    1    0    0 |    g = 0
   33   22  135    7   23   23   21  129    4    0    2 |    h = BMP
   14   16   47    0    5   43    1    6  153    0    0 |    i = U
    0    0    5    0    0    0    0    0    0    0    0 |    j = DZ
   30   17   56    0    0   32    1    4    1    0   36 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
79.74	3.42	9.85	0.0	0.65	2.66	2.97	0.4	0.1	0.0	0.2	 a = A
8.65	66.22	16.17	0.21	2.99	3.09	1.13	1.13	0.1	0.1	0.21	 b = E
10.02	5.89	69.51	0.47	1.81	3.48	7.35	0.82	0.26	0.09	0.3	 c = CGJLNQSRT
8.47	15.25	50.28	11.86	1.13	3.95	5.08	1.69	1.13	0.0	1.13	 d = FV
2.98	14.31	17.5	0.4	58.85	0.2	3.58	1.79	0.2	0.0	0.2	 e = I
12.13	4.04	16.06	0.11	0.43	56.17	7.02	1.81	1.28	0.0	0.96	 f = O
1.13	0.34	6.14	0.16	0.25	0.77	90.94	0.25	0.02	0.0	0.0	 g = 0
8.27	5.51	33.83	1.75	5.76	5.76	5.26	32.33	1.0	0.0	0.5	 h = BMP
4.91	5.61	16.49	0.0	1.75	15.09	0.35	2.11	53.68	0.0	0.0	 i = U
0.0	0.0	100.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 j = DZ
16.95	9.6	31.64	0.0	0.0	18.08	0.56	2.26	0.56	0.0	20.34	 k = D
