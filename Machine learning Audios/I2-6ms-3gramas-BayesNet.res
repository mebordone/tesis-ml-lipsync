Timestamp:2013-08-30-04:40:36
Inventario:I2
Intervalo:6ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I2-6ms-3gramas
Num Instances:  30451
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(15): class 
0Pitch(Hz)(11): class 
0RawPitch(11): class 
0SmPitch(10): class 
0Melogram(st)(17): class 
0ZCross(12): class 
0F1(Hz)(21): class 
0F2(Hz)(18): class 
0F3(Hz)(17): class 
0F4(Hz)(14): class 
1Int(dB)(15): class 
1Pitch(Hz)(11): class 
1RawPitch(11): class 
1SmPitch(11): class 
1Melogram(st)(19): class 
1ZCross(12): class 
1F1(Hz)(20): class 
1F2(Hz)(19): class 
1F3(Hz)(16): class 
1F4(Hz)(18): class 
2Int(dB)(14): class 
2Pitch(Hz)(12): class 
2RawPitch(13): class 
2SmPitch(11): class 
2Melogram(st)(18): class 
2ZCross(12): class 
2F1(Hz)(20): class 
2F2(Hz)(19): class 
2F3(Hz)(17): class 
2F4(Hz)(15): class 
class(12): 
LogScore Bayes: -1386773.67582264
LogScore BDeu: -1413696.799128569
LogScore MDL: -1410196.3625530978
LogScore ENTROPY: -1384185.3617245848
LogScore AIC: -1389224.3617245846




=== Stratified cross-validation ===

Correctly Classified Instances       17964               58.9931 %
Incorrectly Classified Instances     12487               41.0069 %
Kappa statistic                          0.483 
K&B Relative Info Score            1453368.1878 %
K&B Information Score                41954.9475 bits      1.3778 bits/instance
Class complexity | order 0           87886.9852 bits      2.8862 bits/instance
Class complexity | scheme           314732.3166 bits     10.3357 bits/instance
Complexity improvement     (Sf)    -226845.3314 bits     -7.4495 bits/instance
Mean absolute error                      0.0686
Root mean squared error                  0.2425
Relative absolute error                 51.3895 %
Root relative squared error             93.824  %
Coverage of cases (0.95 level)          66.8352 %
Mean rel. region size (0.95 level)      12.279  %
Total Number of Instances            30451     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,615    0,047    0,709      0,615    0,658      0,602    0,907     0,727     A
                 0,029    0,011    0,124      0,029    0,047      0,037    0,741     0,104     LGJKC
                 0,580    0,062    0,439      0,580    0,500      0,457    0,891     0,506     E
                 0,123    0,017    0,097      0,123    0,108      0,095    0,811     0,073     FV
                 0,657    0,038    0,418      0,657    0,510      0,499    0,922     0,496     I
                 0,322    0,023    0,528      0,322    0,400      0,377    0,840     0,435     O
                 0,214    0,034    0,400      0,214    0,279      0,240    0,855     0,351     SNRTY
                 0,901    0,132    0,808      0,901    0,852      0,756    0,959     0,953     0
                 0,056    0,005    0,272      0,056    0,093      0,111    0,793     0,135     BMP
                 0,506    0,057    0,119      0,506    0,193      0,223    0,877     0,155     DZ
                 0,535    0,026    0,328      0,535    0,407      0,402    0,931     0,510     U
                 0,230    0,033    0,232      0,230    0,231      0,198    0,848     0,189     SNRTXY
Weighted Avg.    0,590    0,073    0,583      0,590    0,573      0,517    0,901     0,635     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  2965    61   384    80   101   218   151   240    11   361    95   158 |     a = A
   141    43   191    46   172    57   162   402    10   111    62    98 |     b = LGJKC
   194    41  1361    36   176    44    73    93    19   134    56   120 |     c = E
    23     3    59    53    31    21    41    90     9    63    17    20 |     d = FV
    17    26   173     9   792     0    59    58    11    15    15    31 |     e = I
   313    26   200    24    51   725    78   229    13   307   208    77 |     f = O
   167    71   173    94   116    53   623  1094    26   232    52   211 |     g = SNRTY
   117    41   162   114   135   100   203 10458    18   114    37   110 |     h = 0
    65    17   102    53   170    32    57   156    55   103   112    58 |     i = BMP
    51     0    42    14    20    16    11    14     7   229    20    29 |     j = DZ
    32    11    50    11     3    69    21    14    10    63   373    40 |     k = U
    97     8   202    15   130    39    78    94    13   193    90   287 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
61.45	1.26	7.96	1.66	2.09	4.52	3.13	4.97	0.23	7.48	1.97	3.27	 a = A
9.43	2.88	12.78	3.08	11.51	3.81	10.84	26.89	0.67	7.42	4.15	6.56	 b = LGJKC
8.27	1.75	57.99	1.53	7.5	1.87	3.11	3.96	0.81	5.71	2.39	5.11	 c = E
5.35	0.7	13.72	12.33	7.21	4.88	9.53	20.93	2.09	14.65	3.95	4.65	 d = FV
1.41	2.16	14.34	0.75	65.67	0.0	4.89	4.81	0.91	1.24	1.24	2.57	 e = I
13.9	1.16	8.88	1.07	2.27	32.21	3.47	10.17	0.58	13.64	9.24	3.42	 f = O
5.73	2.44	5.94	3.23	3.98	1.82	21.39	37.57	0.89	7.97	1.79	7.25	 g = SNRTY
1.01	0.35	1.4	0.98	1.16	0.86	1.75	90.09	0.16	0.98	0.32	0.95	 h = 0
6.63	1.73	10.41	5.41	17.35	3.27	5.82	15.92	5.61	10.51	11.43	5.92	 i = BMP
11.26	0.0	9.27	3.09	4.42	3.53	2.43	3.09	1.55	50.55	4.42	6.4	 j = DZ
4.59	1.58	7.17	1.58	0.43	9.9	3.01	2.01	1.43	9.04	53.52	5.74	 k = U
7.78	0.64	16.21	1.2	10.43	3.13	6.26	7.54	1.04	15.49	7.22	23.03	 l = SNRTXY
