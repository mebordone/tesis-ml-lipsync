Timestamp:2013-07-22-21:57:45
Inventario:I2
Intervalo:15ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I2-15ms-4gramas
Num Instances:  12186
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3446





=== Stratified cross-validation ===

Correctly Classified Instances        8713               71.5001 %
Incorrectly Classified Instances      3473               28.4999 %
Kappa statistic                          0.6461
K&B Relative Info Score             752564.9748 %
K&B Information Score                22043.1057 bits      1.8089 bits/instance
Class complexity | order 0           35676.2104 bits      2.9276 bits/instance
Class complexity | scheme           861148.5947 bits     70.667  bits/instance
Complexity improvement     (Sf)    -825472.3843 bits    -67.7394 bits/instance
Mean absolute error                      0.0657
Root mean squared error                  0.1804
Relative absolute error                 48.564  %
Root relative squared error             69.3578 %
Coverage of cases (0.95 level)          93.5007 %
Mean rel. region size (0.95 level)      26.3916 %
Total Number of Instances            12186     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,824    0,069    0,701      0,824    0,757      0,709    0,945     0,842     A
                 0,328    0,029    0,366      0,328    0,346      0,315    0,816     0,290     LGJKC
                 0,693    0,041    0,595      0,693    0,640      0,609    0,929     0,646     E
                 0,141    0,004    0,321      0,141    0,196      0,205    0,782     0,127     FV
                 0,626    0,016    0,621      0,626    0,624      0,608    0,943     0,653     I
                 0,593    0,032    0,607      0,593    0,600      0,567    0,899     0,615     O
                 0,543    0,056    0,518      0,543    0,530      0,476    0,884     0,524     SNRTY
                 0,922    0,053    0,909      0,922    0,915      0,867    0,978     0,960     0
                 0,348    0,009    0,579      0,348    0,435      0,435    0,869     0,396     BMP
                 0,220    0,004    0,465      0,220    0,299      0,313    0,861     0,279     DZ
                 0,568    0,004    0,771      0,568    0,655      0,655    0,936     0,677     U
                 0,462    0,013    0,610      0,462    0,526      0,513    0,906     0,508     SNRTXY
Weighted Avg.    0,715    0,045    0,707      0,715    0,707      0,669    0,932     0,734     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1639   33   70    5   14   47   79   61   15    7    1   18 |    a = A
   91  196   54    6   24   32   97   59   10    2    6   21 |    b = LGJKC
   86   38  673    7   37   28   47   10    9   10    5   21 |    c = E
   29   21   19   25    6   17   47    3    3    0    1    6 |    d = FV
   14   22   84    5  315    6   19   17    9    0    3    9 |    e = I
  119   24   43    2    8  557   51   69   18   14   13   22 |    f = O
  139   88   60   14   32   45  661  134   12    4    4   25 |    g = SNRTY
   72   36   22    6   22   39  138 4071    4    0    1    4 |    h = 0
   35   25   31    3   32   32   51   31  139    4    5   11 |    i = BMP
   34   13   17    1    5   41   14    2    2   40    2   11 |    j = DZ
   16   11   16    0    3   48   17    2    8    0  162    2 |    k = U
   65   28   42    4    9   25   56   22   11    5    7  235 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
82.4	1.66	3.52	0.25	0.7	2.36	3.97	3.07	0.75	0.35	0.05	0.9	 a = A
15.22	32.78	9.03	1.0	4.01	5.35	16.22	9.87	1.67	0.33	1.0	3.51	 b = LGJKC
8.86	3.91	69.31	0.72	3.81	2.88	4.84	1.03	0.93	1.03	0.51	2.16	 c = E
16.38	11.86	10.73	14.12	3.39	9.6	26.55	1.69	1.69	0.0	0.56	3.39	 d = FV
2.78	4.37	16.7	0.99	62.62	1.19	3.78	3.38	1.79	0.0	0.6	1.79	 e = I
12.66	2.55	4.57	0.21	0.85	59.26	5.43	7.34	1.91	1.49	1.38	2.34	 f = O
11.41	7.22	4.93	1.15	2.63	3.69	54.27	11.0	0.99	0.33	0.33	2.05	 g = SNRTY
1.63	0.82	0.5	0.14	0.5	0.88	3.13	92.21	0.09	0.0	0.02	0.09	 h = 0
8.77	6.27	7.77	0.75	8.02	8.02	12.78	7.77	34.84	1.0	1.25	2.76	 i = BMP
18.68	7.14	9.34	0.55	2.75	22.53	7.69	1.1	1.1	21.98	1.1	6.04	 j = DZ
5.61	3.86	5.61	0.0	1.05	16.84	5.96	0.7	2.81	0.0	56.84	0.7	 k = U
12.77	5.5	8.25	0.79	1.77	4.91	11.0	4.32	2.16	0.98	1.38	46.17	 l = SNRTXY
