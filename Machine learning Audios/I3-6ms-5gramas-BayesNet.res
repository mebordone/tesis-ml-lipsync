Timestamp:2013-08-30-05:04:32
Inventario:I3
Intervalo:6ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I3-6ms-5gramas
Num Instances:  30449
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  45 4Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(14): class 
0Pitch(Hz)(13): class 
0RawPitch(15): class 
0SmPitch(12): class 
0Melogram(st)(17): class 
0ZCross(11): class 
0F1(Hz)(19): class 
0F2(Hz)(19): class 
0F3(Hz)(20): class 
0F4(Hz)(18): class 
1Int(dB)(15): class 
1Pitch(Hz)(12): class 
1RawPitch(12): class 
1SmPitch(10): class 
1Melogram(st)(16): class 
1ZCross(12): class 
1F1(Hz)(17): class 
1F2(Hz)(20): class 
1F3(Hz)(16): class 
1F4(Hz)(15): class 
2Int(dB)(16): class 
2Pitch(Hz)(12): class 
2RawPitch(12): class 
2SmPitch(11): class 
2Melogram(st)(15): class 
2ZCross(13): class 
2F1(Hz)(17): class 
2F2(Hz)(19): class 
2F3(Hz)(15): class 
2F4(Hz)(14): class 
3Int(dB)(14): class 
3Pitch(Hz)(12): class 
3RawPitch(13): class 
3SmPitch(11): class 
3Melogram(st)(14): class 
3ZCross(13): class 
3F1(Hz)(18): class 
3F2(Hz)(20): class 
3F3(Hz)(20): class 
3F4(Hz)(14): class 
4Int(dB)(16): class 
4Pitch(Hz)(12): class 
4RawPitch(13): class 
4SmPitch(11): class 
4Melogram(st)(19): class 
4ZCross(13): class 
4F1(Hz)(20): class 
4F2(Hz)(16): class 
4F3(Hz)(19): class 
4F4(Hz)(15): class 
class(11): 
LogScore Bayes: -2322720.6928183204
LogScore BDeu: -2363208.1064885044
LogScore MDL: -2359408.9798947284
LogScore ENTROPY: -2319610.6983898454
LogScore AIC: -2327320.6983898454




=== Stratified cross-validation ===

Correctly Classified Instances       17481               57.4108 %
Incorrectly Classified Instances     12968               42.5892 %
Kappa statistic                          0.4607
K&B Relative Info Score            1412661.1832 %
K&B Information Score                36939.761  bits      1.2132 bits/instance
Class complexity | order 0           79590.4853 bits      2.6139 bits/instance
Class complexity | scheme           523837.3666 bits     17.2038 bits/instance
Complexity improvement     (Sf)    -444246.8813 bits    -14.5899 bits/instance
Mean absolute error                      0.0778
Root mean squared error                  0.2683
Relative absolute error                 54.8206 %
Root relative squared error            100.7362 %
Coverage of cases (0.95 level)          61.0825 %
Mean rel. region size (0.95 level)      11.0441 %
Total Number of Instances            30449     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,577    0,047    0,700      0,577    0,632      0,575    0,898     0,703     A
                 0,546    0,057    0,447      0,546    0,491      0,447    0,881     0,466     E
                 0,139    0,042    0,428      0,139    0,210      0,158    0,795     0,399     CGJLNQSRT
                 0,149    0,019    0,100      0,149    0,120      0,107    0,810     0,091     FV
                 0,659    0,041    0,398      0,659    0,496      0,487    0,918     0,456     I
                 0,306    0,022    0,528      0,306    0,388      0,367    0,837     0,407     O
                 0,894    0,138    0,799      0,894    0,844      0,742    0,954     0,947     0
                 0,071    0,006    0,300      0,071    0,115      0,133    0,792     0,131     BMP
                 0,492    0,023    0,331      0,492    0,396      0,387    0,920     0,490     U
                 0,167    0,011    0,006      0,167    0,011      0,029    0,939     0,004     DZ
                 0,655    0,098    0,090      0,655    0,158      0,216    0,895     0,144     D
Weighted Avg.    0,574    0,078    0,604      0,574    0,564      0,502    0,891     0,649     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  2783   354   267    91   134   201   255    14    89    33   604 |     a = A
   185  1282   157    41   174    35   102    20    70    22   259 |     b = E
   413   541   788   203   451   153  1678    52   187   154  1033 |     c = CGJLNQSRT
    28    48    37    64    27    22    80     8     3    12   101 |     d = FV
    21   148    88     8   795     2    71    10    16    12    35 |     e = I
   285   173   136    22    58   689   233     9   200    27   419 |     f = O
   120   161   254   132   150   103 10376    31    33    58   189 |     g = 0
    73    85    59    58   170    22   153    70    84    16   190 |     h = BMP
    29    53    36     7    16    62    24    13   343     8   106 |     i = U
     0     0     0     0     0     0    10     0     0     2     0 |     j = DZ
    39    26    17    12    24    15     0     6    11     2   289 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
57.68	7.34	5.53	1.89	2.78	4.17	5.28	0.29	1.84	0.68	12.52	 a = A
7.88	54.62	6.69	1.75	7.41	1.49	4.35	0.85	2.98	0.94	11.04	 b = E
7.31	9.57	13.94	3.59	7.98	2.71	29.68	0.92	3.31	2.72	18.27	 c = CGJLNQSRT
6.51	11.16	8.6	14.88	6.28	5.12	18.6	1.86	0.7	2.79	23.49	 d = FV
1.74	12.27	7.3	0.66	65.92	0.17	5.89	0.83	1.33	1.0	2.9	 e = I
12.66	7.69	6.04	0.98	2.58	30.61	10.35	0.4	8.88	1.2	18.61	 f = O
1.03	1.39	2.19	1.14	1.29	0.89	89.39	0.27	0.28	0.5	1.63	 g = 0
7.45	8.67	6.02	5.92	17.35	2.24	15.61	7.14	8.57	1.63	19.39	 h = BMP
4.16	7.6	5.16	1.0	2.3	8.9	3.44	1.87	49.21	1.15	15.21	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	83.33	0.0	0.0	16.67	0.0	 j = DZ
8.84	5.9	3.85	2.72	5.44	3.4	0.0	1.36	2.49	0.45	65.53	 k = D
