Timestamp:2013-08-30-03:42:00
Inventario:I0
Intervalo:2ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I0-2ms-2gramas
Num Instances:  91702
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    27 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(22): class 
0Pitch(Hz)(41): class 
0RawPitch(26): class 
0SmPitch(39): class 
0Melogram(st)(44): class 
0ZCross(15): class 
0F1(Hz)(1963): class 
0F2(Hz)(2684): class 
0F3(Hz)(2854): class 
0F4(Hz)(2912): class 
1Int(dB)(23): class 
1Pitch(Hz)(44): class 
1RawPitch(31): class 
1SmPitch(37): class 
1Melogram(st)(44): class 
1ZCross(15): class 
1F1(Hz)(1919): class 
1F2(Hz)(2688): class 
1F3(Hz)(3038): class 
1F4(Hz)(2726): class 
class(27): 
LogScore Bayes: -5115127.386815128
LogScore BDeu: -1.2868167636238316E7
LogScore MDL: -1.0523563398263937E7
LogScore ENTROPY: -7261691.975910729
LogScore AIC: -7832632.975910729




=== Stratified cross-validation ===

Correctly Classified Instances       71598               78.0768 %
Incorrectly Classified Instances     20104               21.9232 %
Kappa statistic                          0.7252
K&B Relative Info Score            6590092.2064 %
K&B Information Score               216943.7804 bits      2.3657 bits/instance
Class complexity | order 0          301796.917  bits      3.2911 bits/instance
Class complexity | scheme           470101.2712 bits      5.1264 bits/instance
Complexity improvement     (Sf)    -168304.3543 bits     -1.8353 bits/instance
Mean absolute error                      0.0167
Root mean squared error                  0.12  
Relative absolute error                 27.8405 %
Root relative squared error             69.3641 %
Coverage of cases (0.95 level)          81.9437 %
Mean rel. region size (0.95 level)       4.6261 %
Total Number of Instances            91702     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,198    0,001    0,464      0,198    0,278      0,301    0,947     0,255     ch
                 0,927    0,005    0,184      0,927    0,307      0,411    0,996     0,817     rr
                 0,902    0,128    0,819      0,902    0,858      0,763    0,969     0,960     0
                 0,544    0,006    0,492      0,544    0,516      0,512    0,917     0,526     hs
                 0,935    0,009    0,219      0,935    0,355      0,450    0,996     0,851     hg
                 0,803    0,009    0,937      0,803    0,865      0,848    0,972     0,932     a
                 0,143    0,002    0,485      0,143    0,220      0,257    0,922     0,246     c
                 0,928    0,005    0,542      0,928    0,684      0,707    0,996     0,862     b
                 0,758    0,010    0,858      0,758    0,805      0,792    0,963     0,863     e
                 0,797    0,008    0,602      0,797    0,686      0,688    0,982     0,760     d
                 0,813    0,006    0,453      0,813    0,582      0,604    0,952     0,746     g
                 0,351    0,002    0,576      0,351    0,436      0,447    0,955     0,380     f
                 0,804    0,006    0,852      0,804    0,827      0,821    0,962     0,861     i
                 0,595    0,001    0,707      0,595    0,646      0,647    0,933     0,621     j
                 0,867    0,004    0,740      0,867    0,798      0,797    0,983     0,874     m
                 0,878    0,009    0,649      0,878    0,746      0,750    0,986     0,841     l
                 0,715    0,008    0,869      0,715    0,785      0,774    0,926     0,827     o
                 0,812    0,005    0,845      0,812    0,828      0,822    0,971     0,866     n
                 0,325    0,001    0,507      0,325    0,396      0,404    0,923     0,343     q
                 0,290    0,002    0,591      0,290    0,389      0,410    0,934     0,354     p
                 0,488    0,014    0,636      0,488    0,552      0,538    0,960     0,634     s
                 0,677    0,006    0,731      0,677    0,703      0,697    0,920     0,708     r
                 0,874    0,003    0,864      0,874    0,869      0,866    0,979     0,909     u
                 0,199    0,004    0,518      0,199    0,287      0,314    0,922     0,295     t
                 0,857    0,006    0,538      0,857    0,661      0,676    0,978     0,804     v
                 0,735    0,012    0,231      0,735    0,351      0,407    0,940     0,673     y
                 0,000    0,002    0,000      0,000    0,000      -0,001   0,824     0,001     z
Weighted Avg.    0,781    0,055    0,794      0,781    0,778      0,737    0,962     0,852     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m     n     o     p     q     r     s     t     u     v     w     x     y     z    aa   <-- classified as
    85     0   149     0     0     5     2     1     0     0     1     0     0     0     0     1     4     1     0     0   170     2     0     4     4     0     0 |     a = ch
     0   101     0     0     1     1     0     0     3     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     3     0     0 |     b = rr
    47    68 32320   174   282   202    38    64   285   187   105    66   200    98    67   143   242   143    76    89   116   146    75    56   128   368    47 |     c = 0
     0     4   332   533     8     6     0     3    20     4     0     0     5     0     3    16    18     1     0     0     8     0     3     0     7     9     0 |     d = hs
     0     1     0     0   231     2     0     1     1     0     2     0     2     0     3     0     0     0     0     0     0     0     0     0     4     0     0 |     e = hg
    32   103   406   117   212 10528    31   118   127   140   118    16    27    10    77   220   100    90    27    22   101    96    30    44   117   193     4 |     f = a
     0     2   752     2     5     9   194     0    18     2     0     1    13     0     4     1    31     0     0     4   250    11    28    21     7     6     0 |     g = c
     0     2     0     0     1     6     0   556     3     0     2     0    12     0     4     2     0     0     0     0     0     2     4     0     2     3     0 |     h = b
     6    59   276    91    71    93    19    56  5241    88    87    11    55     4    55   120    49    92    11    33    47    85    36    39    68   110     8 |     i = e
     0    21    12     4    12    30     1     5    28  1047    12     2     5     0     5    11    27    22     0     2     3     7     3     2     9    35     8 |     j = d
     0     5    33     0     8     8     2     1    16     2   442     2     4     0     0     2     2     2     1     1     3     4     2     2     0     1     1 |     k = g
     0     0   299     2     1    10     0     1    15     0     2   204     5     0     0     0     4     0     0     0    21     0     2    10     2     4     0 |     l = f
     1     4   171    22    26    11    21    31    38    10    32    11  2863     0    30    25    38    20     1     3    49    24     4    23    28    56    19 |     m = i
     0     0   102     0     2     0     3     4    11     4     0     0     1   292     6     0     2     0     1     4    49     1     4     3     0     2     0 |     n = j
     0     6    21    19     2     9     1    14     9     3     7     7    15     0  1136     7    15     3     0     0     0     5     7     1     3    15     6 |     o = m
     0     3    11    19     6    10     1    15    31     4    12     1    13     1     6  1416     6     8     2     1    14    10     5     1     3    14     0 |     p = l
     5    67   731    44    68   100    24    60    46   167    86    13    21     0    55    94  4754    43     7     8    15    34    30    34    46    70    24 |     q = o
     4    37   121    15    28    29     4    19    56    28    16     7    19     1    25    30     6  2569     1     9     3     8    22    22     9    52    24 |     r = n
     1     1   156     1     4     4     0     0     8     0     0     0     3     0     0     1     2     2   136     5    86     4     0     1     0     4     0 |     s = q
     0     2   501     1     2    15     8     2    24     2     1     0     0     0     4    13    27     0     0   291    42    14    11     8    20    17     0 |     t = p
     0    29  1758    10    42    42    12    11    21    17     7     4    51     3    19    24    29     5     3     0  2160    24    14    14    44    79     1 |     u = s
     2     9   244    18    17    74    10    33    73    23    17     7    26     0     6    18    22     9     0     6    30  1411     1     6     7    13     2 |     v = r
     0     9    32     2     6     2     8    13     5     8    13     1     0     4    19    23    19    11     2    10    18    12  1817    16     5    25     0 |     w = u
     0     5   958     7     5    25    16     9    14     4     3     0     9     0     9     6    75    19     0     1   163    25     1   340     1    15     0 |     x = t
     0    12    10     3     1     9     1     8    12     0    10     1    11     0     1     1     1     1     0     2     6     1     1     6   603     3     0 |     y = v
     0     0    36     0    13     0     4     1     0     0     0     0     0     0     2     8     0     0     0     1    43     5     2     3     0   328     0 |     z = y
     0     0    33     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0 |    aa = z

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z	aa	
19.81	0.0	34.73	0.0	0.0	1.17	0.47	0.23	0.0	0.0	0.23	0.0	0.0	0.0	0.0	0.23	0.93	0.23	0.0	0.0	39.63	0.47	0.0	0.93	0.93	0.0	0.0	 a = ch
0.0	92.66	0.0	0.0	0.92	0.92	0.0	0.0	2.75	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	2.75	0.0	0.0	 b = rr
0.13	0.19	90.2	0.49	0.79	0.56	0.11	0.18	0.8	0.52	0.29	0.18	0.56	0.27	0.19	0.4	0.68	0.4	0.21	0.25	0.32	0.41	0.21	0.16	0.36	1.03	0.13	 c = 0
0.0	0.41	33.88	54.39	0.82	0.61	0.0	0.31	2.04	0.41	0.0	0.0	0.51	0.0	0.31	1.63	1.84	0.1	0.0	0.0	0.82	0.0	0.31	0.0	0.71	0.92	0.0	 d = hs
0.0	0.4	0.0	0.0	93.52	0.81	0.0	0.4	0.4	0.0	0.81	0.0	0.81	0.0	1.21	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	1.62	0.0	0.0	 e = hg
0.24	0.79	3.1	0.89	1.62	80.33	0.24	0.9	0.97	1.07	0.9	0.12	0.21	0.08	0.59	1.68	0.76	0.69	0.21	0.17	0.77	0.73	0.23	0.34	0.89	1.47	0.03	 f = a
0.0	0.15	55.25	0.15	0.37	0.66	14.25	0.0	1.32	0.15	0.0	0.07	0.96	0.0	0.29	0.07	2.28	0.0	0.0	0.29	18.37	0.81	2.06	1.54	0.51	0.44	0.0	 g = c
0.0	0.33	0.0	0.0	0.17	1.0	0.0	92.82	0.5	0.0	0.33	0.0	2.0	0.0	0.67	0.33	0.0	0.0	0.0	0.0	0.0	0.33	0.67	0.0	0.33	0.5	0.0	 h = b
0.09	0.85	3.99	1.32	1.03	1.35	0.27	0.81	75.85	1.27	1.26	0.16	0.8	0.06	0.8	1.74	0.71	1.33	0.16	0.48	0.68	1.23	0.52	0.56	0.98	1.59	0.12	 i = e
0.0	1.6	0.91	0.3	0.91	2.28	0.08	0.38	2.13	79.74	0.91	0.15	0.38	0.0	0.38	0.84	2.06	1.68	0.0	0.15	0.23	0.53	0.23	0.15	0.69	2.67	0.61	 j = d
0.0	0.92	6.07	0.0	1.47	1.47	0.37	0.18	2.94	0.37	81.25	0.37	0.74	0.0	0.0	0.37	0.37	0.37	0.18	0.18	0.55	0.74	0.37	0.37	0.0	0.18	0.18	 k = g
0.0	0.0	51.37	0.34	0.17	1.72	0.0	0.17	2.58	0.0	0.34	35.05	0.86	0.0	0.0	0.0	0.69	0.0	0.0	0.0	3.61	0.0	0.34	1.72	0.34	0.69	0.0	 l = f
0.03	0.11	4.8	0.62	0.73	0.31	0.59	0.87	1.07	0.28	0.9	0.31	80.4	0.0	0.84	0.7	1.07	0.56	0.03	0.08	1.38	0.67	0.11	0.65	0.79	1.57	0.53	 m = i
0.0	0.0	20.77	0.0	0.41	0.0	0.61	0.81	2.24	0.81	0.0	0.0	0.2	59.47	1.22	0.0	0.41	0.0	0.2	0.81	9.98	0.2	0.81	0.61	0.0	0.41	0.0	 n = j
0.0	0.46	1.6	1.45	0.15	0.69	0.08	1.07	0.69	0.23	0.53	0.53	1.14	0.0	86.65	0.53	1.14	0.23	0.0	0.0	0.0	0.38	0.53	0.08	0.23	1.14	0.46	 o = m
0.0	0.19	0.68	1.18	0.37	0.62	0.06	0.93	1.92	0.25	0.74	0.06	0.81	0.06	0.37	87.79	0.37	0.5	0.12	0.06	0.87	0.62	0.31	0.06	0.19	0.87	0.0	 p = l
0.08	1.01	11.0	0.66	1.02	1.5	0.36	0.9	0.69	2.51	1.29	0.2	0.32	0.0	0.83	1.41	71.53	0.65	0.11	0.12	0.23	0.51	0.45	0.51	0.69	1.05	0.36	 q = o
0.13	1.17	3.82	0.47	0.88	0.92	0.13	0.6	1.77	0.88	0.51	0.22	0.6	0.03	0.79	0.95	0.19	81.19	0.03	0.28	0.09	0.25	0.7	0.7	0.28	1.64	0.76	 r = n
0.24	0.24	37.23	0.24	0.95	0.95	0.0	0.0	1.91	0.0	0.0	0.0	0.72	0.0	0.0	0.24	0.48	0.48	32.46	1.19	20.53	0.95	0.0	0.24	0.0	0.95	0.0	 s = q
0.0	0.2	49.85	0.1	0.2	1.49	0.8	0.2	2.39	0.2	0.1	0.0	0.0	0.0	0.4	1.29	2.69	0.0	0.0	28.96	4.18	1.39	1.09	0.8	1.99	1.69	0.0	 t = p
0.0	0.66	39.75	0.23	0.95	0.95	0.27	0.25	0.47	0.38	0.16	0.09	1.15	0.07	0.43	0.54	0.66	0.11	0.07	0.0	48.84	0.54	0.32	0.32	0.99	1.79	0.02	 u = s
0.1	0.43	11.71	0.86	0.82	3.55	0.48	1.58	3.5	1.1	0.82	0.34	1.25	0.0	0.29	0.86	1.06	0.43	0.0	0.29	1.44	67.71	0.05	0.29	0.34	0.62	0.1	 v = r
0.0	0.43	1.54	0.1	0.29	0.1	0.38	0.63	0.24	0.38	0.63	0.05	0.0	0.19	0.91	1.11	0.91	0.53	0.1	0.48	0.87	0.58	87.36	0.77	0.24	1.2	0.0	 w = u
0.0	0.29	56.02	0.41	0.29	1.46	0.94	0.53	0.82	0.23	0.18	0.0	0.53	0.0	0.53	0.35	4.39	1.11	0.0	0.06	9.53	1.46	0.06	19.88	0.06	0.88	0.0	 x = t
0.0	1.7	1.42	0.43	0.14	1.28	0.14	1.14	1.7	0.0	1.42	0.14	1.56	0.0	0.14	0.14	0.14	0.14	0.0	0.28	0.85	0.14	0.14	0.85	85.65	0.43	0.0	 y = v
0.0	0.0	8.07	0.0	2.91	0.0	0.9	0.22	0.0	0.0	0.0	0.0	0.0	0.0	0.45	1.79	0.0	0.0	0.0	0.22	9.64	1.12	0.45	0.67	0.0	73.54	0.0	 z = y
0.0	0.0	100.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 aa = z
