#!/usr/bin/python
# -*- coding: utf8 -*-
import sys,os
import time,datetime

#~ inv_op = sys.argv[1]
#~ msop = sys.argv[2]
#~ ngramas = int(sys.argv[3])

for inv_op in range(0,5):
	for msop in [1,2,3,6,10,15,20]:
		for ngramas in range(1,6):

			timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H:%M:%S')
			classifier = "BayesNet"
			os.system("export CLASSPATH=~/weka-3-7-9/weka.jar")

			#~ print "python sft2arffngrams.py {} {} {}".format(inv_op,msop,ngramas)
			os.system("python sft2arffngrams.py {} {} {}".format(inv_op,msop,ngramas))

			os.system("echo 'Timestamp:{0}\nInventario:I{1}\nIntervalo:{2}ms\nNgramas:{3}\nClasificador:{4}' > \
I{1}-{2}ms-{3}gramas-{4}.res".format(timestamp,inv_op,msop,ngramas,classifier))

			#~ print "java weka.core.Instances speech-I{1}-{2}ms-{3}gramas.arff > res-{0}-I{1}-{2}ms-{3}gramas-{4}.txt".format(timestamp,inv_op,msop,ngramas,classifier)
			os.system("java weka.core.Instances I{1}-{2}ms-{3}gramas.arff >> \
I{1}-{2}ms-{3}gramas-{4}.res".format(timestamp,inv_op,msop,ngramas,classifier))

			#~ RandomTree
			#~ os.system("\
			#~ java weka.classifiers.trees.RandomForest -i -v -k -I 10 -K 0 -S 1 -num-slots 4 \
			#~ -t I{1}-{2}ms-{3}gramas.arff \
			#~ >> I{1}-{2}ms-{3}gramas-{4}.res".format(timestamp,inv_op,msop,ngramas,classifier))
			
			#~ BayesNet
			#~ print("\
            #~ java weka.classifiers.bayes.BayesNet -i -v -k \
            #~ -t I{1}-{2}ms-{3}gramas.arff -D -Q weka.classifiers.bayes.net.search.local.K2 \
            #~ -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 \
            #~ >> I{1}-{2}ms-{3}gramas-{4}.res".format(timestamp,inv_op,msop,ngramas,classifier))
			os.system("\
			java weka.classifiers.bayes.BayesNet -i -v -k \
			-t I{1}-{2}ms-{3}gramas.arff -D -Q weka.classifiers.bayes.net.search.local.K2 \
			-- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 \
			>> I{1}-{2}ms-{3}gramas-{4}.res".format(timestamp,inv_op,msop,ngramas,classifier))

			os.system("python exp.py I{1}-{2}ms-{3}gramas-{4}.res\
			>> I{1}-{2}ms-{3}gramas-{4}.res".format(timestamp,inv_op,msop,ngramas,classifier))

			#~ NahiveBayes
			print "java -Xmx2500M -cp /home/alumno/weka-3-7-10/weka.jar weka.classifiers.bayes.NaiveBayes -i -v -k -t \
			/home/alumno/Documentos/Facu/Tesis\ Matías\ Bordone/Audios/I{1}-{2}ms-{3}gramas.arff \
			>> I{1}-{2}ms-{3}gramas-{4}.res".format(timestamp,inv_op,msop,ngramas,classifier)
			os.system("java -cp /home/alumno/weka-3-7-10/weka.jar weka.classifiers.bayes.NaiveBayes -i -v -k -t \
			/home/alumno/Documentos/Facu/Tesis\ Matías\ Bordone/Audios/I{1}-{2}ms-{3}gramas.arff \
			>> I{1}-{2}ms-{3}gramas-{4}.res".format(timestamp,inv_op,msop,ngramas,classifier))
			
			os.system("python exp.py I{1}-{2}ms-{3}gramas-{4}.res\
			>> I{1}-{2}ms-{3}gramas-{4}.res".format(timestamp,inv_op,msop,ngramas,classifier))


