Timestamp:2013-07-22-21:21:53
Inventario:I1
Intervalo:15ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I1-15ms-4gramas
Num Instances:  12186
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3416





=== Stratified cross-validation ===

Correctly Classified Instances        8796               72.1812 %
Incorrectly Classified Instances      3390               27.8188 %
Kappa statistic                          0.6541
K&B Relative Info Score             759314.443  %
K&B Information Score                22416.3181 bits      1.8395 bits/instance
Class complexity | order 0           35954.2498 bits      2.9505 bits/instance
Class complexity | scheme           869505.5477 bits     71.3528 bits/instance
Complexity improvement     (Sf)    -833551.2979 bits    -68.4024 bits/instance
Mean absolute error                      0.06  
Root mean squared error                  0.1725
Relative absolute error                 48.0203 %
Root relative squared error             69.0129 %
Coverage of cases (0.95 level)          93.4351 %
Mean rel. region size (0.95 level)      23.9411 %
Total Number of Instances            12186     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,824    0,069    0,700      0,824    0,757      0,708    0,946     0,842     a
                 0,416    0,013    0,522      0,416    0,463      0,450    0,853     0,377     b
                 0,678    0,039    0,599      0,678    0,636      0,604    0,931     0,647     e
                 0,231    0,004    0,452      0,231    0,305      0,316    0,857     0,235     d
                 0,181    0,005    0,348      0,181    0,238      0,243    0,789     0,141     f
                 0,650    0,017    0,616      0,650    0,632      0,616    0,940     0,652     i
                 0,194    0,009    0,301      0,194    0,236      0,230    0,794     0,191     k
                 0,268    0,003    0,544      0,268    0,359      0,377    0,893     0,343     j
                 0,530    0,055    0,518      0,530    0,524      0,471    0,875     0,492     l
                 0,577    0,026    0,654      0,577    0,613      0,584    0,901     0,622     o
                 0,926    0,057    0,902      0,926    0,914      0,864    0,977     0,958     0
                 0,569    0,023    0,614      0,569    0,591      0,566    0,916     0,592     s
                 0,551    0,004    0,770      0,551    0,642      0,644    0,938     0,687     u
Weighted Avg.    0,722    0,046    0,712      0,722    0,713      0,675    0,933     0,737     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1638   13   64    7    3   19   15    8   91   40   63   25    3 |    a = a
   32  166   24    4    8   24   10    1   54   24   33   14    5 |    b = b
   80   19  658    8    6   44    8    7   76   30   16   16    3 |    c = e
   35    7   17   42    1    2    0    1   42   30    0    2    3 |    d = d
   21   10   25    1   32    4    2    2   31   14    8   25    2 |    e = f
   20   10   79    1    3  327    1    0   27    3   17   12    3 |    f = i
   27    6   13    0    5    8   46    1   27   11   60   32    1 |    g = k
   38    4   23    1    2    6    1   37   11    5    0   10    0 |    h = j
  183   33   87   11   11   43   26    2  645   40   89   43    3 |    i = l
  125   21   43   13    1    7    9    3   69  542   69   18   20 |    j = o
   70   13   23    2    7   21   20    3   75   31 4088   59    3 |    k = 0
   55    7   26    2   11   20   13    3   76   14   88  418    1 |    l = s
   16    9   16    1    2    6    2    0   22   45    2    7  157 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
82.35	0.65	3.22	0.35	0.15	0.96	0.75	0.4	4.58	2.01	3.17	1.26	0.15	 a = a
8.02	41.6	6.02	1.0	2.01	6.02	2.51	0.25	13.53	6.02	8.27	3.51	1.25	 b = b
8.24	1.96	67.77	0.82	0.62	4.53	0.82	0.72	7.83	3.09	1.65	1.65	0.31	 c = e
19.23	3.85	9.34	23.08	0.55	1.1	0.0	0.55	23.08	16.48	0.0	1.1	1.65	 d = d
11.86	5.65	14.12	0.56	18.08	2.26	1.13	1.13	17.51	7.91	4.52	14.12	1.13	 e = f
3.98	1.99	15.71	0.2	0.6	65.01	0.2	0.0	5.37	0.6	3.38	2.39	0.6	 f = i
11.39	2.53	5.49	0.0	2.11	3.38	19.41	0.42	11.39	4.64	25.32	13.5	0.42	 g = k
27.54	2.9	16.67	0.72	1.45	4.35	0.72	26.81	7.97	3.62	0.0	7.25	0.0	 h = j
15.05	2.71	7.15	0.9	0.9	3.54	2.14	0.16	53.04	3.29	7.32	3.54	0.25	 i = l
13.3	2.23	4.57	1.38	0.11	0.74	0.96	0.32	7.34	57.66	7.34	1.91	2.13	 j = o
1.59	0.29	0.52	0.05	0.16	0.48	0.45	0.07	1.7	0.7	92.59	1.34	0.07	 k = 0
7.49	0.95	3.54	0.27	1.5	2.72	1.77	0.41	10.35	1.91	11.99	56.95	0.14	 l = s
5.61	3.16	5.61	0.35	0.7	2.11	0.7	0.0	7.72	15.79	0.7	2.46	55.09	 m = u
