Timestamp:2013-07-22-21:21:09
Inventario:I1
Intervalo:15ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I1-15ms-1gramas
Num Instances:  12189
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.3421





=== Stratified cross-validation ===

Correctly Classified Instances        8503               69.7596 %
Incorrectly Classified Instances      3686               30.2404 %
Kappa statistic                          0.6241
K&B Relative Info Score             736589.0722 %
K&B Information Score                21744.0421 bits      1.7839 bits/instance
Class complexity | order 0           35958.643  bits      2.9501 bits/instance
Class complexity | scheme          1356997.316  bits    111.3297 bits/instance
Complexity improvement     (Sf)    -1321038.673 bits   -108.3796 bits/instance
Mean absolute error                      0.0597
Root mean squared error                  0.1789
Relative absolute error                 47.7922 %
Root relative squared error             71.5896 %
Coverage of cases (0.95 level)          89.5069 %
Mean rel. region size (0.95 level)      21.5977 %
Total Number of Instances            12189     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,797    0,064    0,710      0,797    0,751      0,701    0,927     0,820     a
                 0,346    0,017    0,413      0,346    0,377      0,359    0,811     0,319     b
                 0,654    0,042    0,574      0,654    0,611      0,577    0,914     0,610     e
                 0,192    0,006    0,318      0,192    0,240      0,239    0,796     0,186     d
                 0,169    0,005    0,323      0,169    0,222      0,226    0,746     0,128     f
                 0,636    0,018    0,602      0,636    0,618      0,602    0,913     0,622     i
                 0,165    0,011    0,223      0,165    0,189      0,178    0,745     0,117     k
                 0,225    0,003    0,463      0,225    0,302      0,317    0,798     0,207     j
                 0,452    0,062    0,449      0,452    0,451      0,390    0,836     0,413     l
                 0,557    0,030    0,612      0,557    0,584      0,551    0,882     0,604     o
                 0,925    0,062    0,895      0,925    0,910      0,857    0,971     0,955     0
                 0,495    0,027    0,543      0,495    0,517      0,489    0,879     0,531     s
                 0,579    0,004    0,760      0,579    0,657      0,657    0,920     0,673     u
Weighted Avg.    0,698    0,048    0,686      0,698    0,689      0,648    0,913     0,709     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1586   13   76    6    4   20   11    6  110   50   70   34    3 |    a = a
   38  138   27    6    5   30   10    1   73   22   30   12    7 |    b = b
   81   25  635    7    3   42    7    9   87   26   24   21    4 |    c = e
   29    6   17   35    2    6    1    1   39   37    2    5    2 |    d = d
   16    9   24    3   30    5    5    2   31   17   11   22    2 |    e = f
   22   17   71    0    3  320    1    1   32    4   22    9    1 |    f = i
   12    6   10    2    8   12   39    2   28   21   55   41    1 |    g = k
   38    2   20    2    1    7    4   31    8    6    7   10    2 |    h = j
  180   49  118   14    9   45   23    3  550   58  102   52   13 |    i = l
  114   22   39   25    4    7   10    4   96  524   63   20   12 |    j = o
   47   20   23    3   11   17   20    4   78   30 4087   77    1 |    k = 0
   61   13   34    7   12   20   41    1   65   19   94  363    4 |    l = s
   11   14   13    0    1    1    3    2   28   42    2    3  165 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
79.74	0.65	3.82	0.3	0.2	1.01	0.55	0.3	5.53	2.51	3.52	1.71	0.15	 a = a
9.52	34.59	6.77	1.5	1.25	7.52	2.51	0.25	18.3	5.51	7.52	3.01	1.75	 b = b
8.34	2.57	65.4	0.72	0.31	4.33	0.72	0.93	8.96	2.68	2.47	2.16	0.41	 c = e
15.93	3.3	9.34	19.23	1.1	3.3	0.55	0.55	21.43	20.33	1.1	2.75	1.1	 d = d
9.04	5.08	13.56	1.69	16.95	2.82	2.82	1.13	17.51	9.6	6.21	12.43	1.13	 e = f
4.37	3.38	14.12	0.0	0.6	63.62	0.2	0.2	6.36	0.8	4.37	1.79	0.2	 f = i
5.06	2.53	4.22	0.84	3.38	5.06	16.46	0.84	11.81	8.86	23.21	17.3	0.42	 g = k
27.54	1.45	14.49	1.45	0.72	5.07	2.9	22.46	5.8	4.35	5.07	7.25	1.45	 h = j
14.8	4.03	9.7	1.15	0.74	3.7	1.89	0.25	45.23	4.77	8.39	4.28	1.07	 i = l
12.13	2.34	4.15	2.66	0.43	0.74	1.06	0.43	10.21	55.74	6.7	2.13	1.28	 j = o
1.06	0.45	0.52	0.07	0.25	0.38	0.45	0.09	1.77	0.68	92.51	1.74	0.02	 k = 0
8.31	1.77	4.63	0.95	1.63	2.72	5.59	0.14	8.86	2.59	12.81	49.46	0.54	 l = s
3.86	4.91	4.56	0.0	0.35	0.35	1.05	0.7	9.82	14.74	0.7	1.05	57.89	 m = u
