Timestamp:2013-08-30-04:32:58
Inventario:I2
Intervalo:2ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I2-2ms-3gramas
Num Instances:  91701
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(23): class 
0Pitch(Hz)(44): class 
0RawPitch(34): class 
0SmPitch(39): class 
0Melogram(st)(46): class 
0ZCross(16): class 
0F1(Hz)(1715): class 
0F2(Hz)(2221): class 
0F3(Hz)(2405): class 
0F4(Hz)(2304): class 
1Int(dB)(25): class 
1Pitch(Hz)(46): class 
1RawPitch(33): class 
1SmPitch(36): class 
1Melogram(st)(46): class 
1ZCross(16): class 
1F1(Hz)(1679): class 
1F2(Hz)(2163): class 
1F3(Hz)(2662): class 
1F4(Hz)(2583): class 
2Int(dB)(23): class 
2Pitch(Hz)(46): class 
2RawPitch(35): class 
2SmPitch(39): class 
2Melogram(st)(47): class 
2ZCross(16): class 
2F1(Hz)(1632): class 
2F2(Hz)(2133): class 
2F3(Hz)(2518): class 
2F4(Hz)(2413): class 
class(12): 
LogScore Bayes: -7539850.59917045
LogScore BDeu: -1.1591210117041325E7
LogScore MDL: -1.0524698620323092E7
LogScore ENTROPY: -8673028.56662865
LogScore AIC: -8997135.56662865




=== Stratified cross-validation ===

Correctly Classified Instances       71509               77.9806 %
Incorrectly Classified Instances     20192               22.0194 %
Kappa statistic                          0.7189
K&B Relative Info Score            6716318.226  %
K&B Information Score               192311.7121 bits      2.0972 bits/instance
Class complexity | order 0          262553.9315 bits      2.8632 bits/instance
Class complexity | scheme           703460.1127 bits      7.6712 bits/instance
Complexity improvement     (Sf)    -440906.1812 bits     -4.8081 bits/instance
Mean absolute error                      0.0369
Root mean squared error                  0.1833
Relative absolute error                 27.816  %
Root relative squared error             71.1905 %
Coverage of cases (0.95 level)          81.0515 %
Mean rel. region size (0.95 level)       9.4443 %
Total Number of Instances            91701     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,818    0,017    0,900      0,818    0,857      0,833    0,959     0,911     A
                 0,445    0,009    0,710      0,445    0,547      0,545    0,906     0,579     LGJKC
                 0,835    0,023    0,748      0,835    0,789      0,772    0,961     0,857     E
                 0,602    0,008    0,513      0,602    0,554      0,549    0,902     0,589     FV
                 0,851    0,012    0,734      0,851    0,788      0,781    0,959     0,850     I
                 0,759    0,011    0,849      0,759    0,801      0,788    0,925     0,830     O
                 0,343    0,018    0,663      0,343    0,452      0,441    0,939     0,625     SNRTY
                 0,903    0,142    0,803      0,903    0,850      0,748    0,968     0,959     0
                 0,661    0,007    0,757      0,661    0,706      0,698    0,901     0,708     BMP
                 0,855    0,019    0,400      0,855    0,545      0,576    0,969     0,749     DZ
                 0,902    0,007    0,744      0,902    0,816      0,815    0,978     0,915     U
                 0,787    0,011    0,756      0,787    0,771      0,762    0,935     0,800     SNRTXY
Weighted Avg.    0,780    0,065    0,782      0,780    0,771      0,724    0,953     0,857     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 11722   126   298   160   149   202   267   761    81   299    65   203 |     a = A
   106  1970   257    10   152    66   338  1203    34   131   107    54 |     b = LGJKC
   108    92  5770    66    99    28   138   276    62   128    46    97 |     c = E
    23     5    36   774    30    15    39   316    17    20     8     3 |     d = FV
    16    51    68    21  3031    14    89   181    36    19     8    27 |     e = I
   157    56   138    26    29  5043    69   720    67   223    56    62 |     f = O
   366   136   414    70   185   151  2975  3568   124   370   157   147 |     g = SNRTY
   393   276   505   349   318   328   291 32355   162   438   109   307 |     h = 0
    61    31    82     6    76    40    76   521  1927    32    58     5 |     i = BMP
    26     2    39    12     6    28    11    42     4  1151     4    21 |     j = DZ
     5    29    15    12     5     9    36    39    22    17  1877    14 |     k = U
    46     2    94     4    52    15   160   324    10    53    28  2914 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
81.78	0.88	2.08	1.12	1.04	1.41	1.86	5.31	0.57	2.09	0.45	1.42	 a = A
2.39	44.49	5.8	0.23	3.43	1.49	7.63	27.17	0.77	2.96	2.42	1.22	 b = LGJKC
1.56	1.33	83.5	0.96	1.43	0.41	2.0	3.99	0.9	1.85	0.67	1.4	 c = E
1.79	0.39	2.8	60.19	2.33	1.17	3.03	24.57	1.32	1.56	0.62	0.23	 d = FV
0.45	1.43	1.91	0.59	85.12	0.39	2.5	5.08	1.01	0.53	0.22	0.76	 e = I
2.36	0.84	2.08	0.39	0.44	75.88	1.04	10.83	1.01	3.36	0.84	0.93	 f = O
4.22	1.57	4.78	0.81	2.14	1.74	34.34	41.19	1.43	4.27	1.81	1.7	 g = SNRTY
1.1	0.77	1.41	0.97	0.89	0.92	0.81	90.3	0.45	1.22	0.3	0.86	 h = 0
2.09	1.06	2.81	0.21	2.61	1.37	2.61	17.87	66.11	1.1	1.99	0.17	 i = BMP
1.93	0.15	2.9	0.89	0.45	2.08	0.82	3.12	0.3	85.51	0.3	1.56	 j = DZ
0.24	1.39	0.72	0.58	0.24	0.43	1.73	1.88	1.06	0.82	90.24	0.67	 k = U
1.24	0.05	2.54	0.11	1.4	0.41	4.32	8.75	0.27	1.43	0.76	78.71	 l = SNRTXY
