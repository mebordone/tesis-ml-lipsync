Timestamp:2013-08-30-05:04:56
Inventario:I3
Intervalo:10ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I3-10ms-1gramas
Num Instances:  18296
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(12): class 
0Pitch(Hz)(7): class 
0RawPitch(7): class 
0SmPitch(9): class 
0Melogram(st)(9): class 
0ZCross(12): class 
0F1(Hz)(12): class 
0F2(Hz)(13): class 
0F3(Hz)(10): class 
0F4(Hz)(8): class 
class(11): 
LogScore Bayes: -258650.99462276167
LogScore BDeu: -263275.2777431609
LogScore MDL: -262898.6363852825
LogScore ENTROPY: -258045.39692497754
LogScore AIC: -259034.39692497754




=== Stratified cross-validation ===

Correctly Classified Instances       11242               61.4451 %
Incorrectly Classified Instances      7054               38.5549 %
Kappa statistic                          0.5041
K&B Relative Info Score             948772.5802 %
K&B Information Score                24994.0488 bits      1.3661 bits/instance
Class complexity | order 0           48176.7702 bits      2.6332 bits/instance
Class complexity | scheme            67291.3009 bits      3.6779 bits/instance
Complexity improvement     (Sf)     -19114.5306 bits     -1.0447 bits/instance
Mean absolute error                      0.0756
Root mean squared error                  0.2321
Relative absolute error                 52.9347 %
Root relative squared error             86.8717 %
Coverage of cases (0.95 level)          78.5363 %
Mean rel. region size (0.95 level)      21.9914 %
Total Number of Instances            18296     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,730    0,085    0,622      0,730    0,672      0,606    0,909     0,739     A
                 0,640    0,082    0,399      0,640    0,492      0,452    0,897     0,521     E
                 0,179    0,045    0,482      0,179    0,261      0,206    0,808     0,438     CGJLNQSRT
                 0,023    0,003    0,100      0,023    0,037      0,041    0,786     0,049     FV
                 0,643    0,035    0,440      0,643    0,523      0,508    0,925     0,519     I
                 0,361    0,030    0,494      0,361    0,417      0,383    0,839     0,437     O
                 0,924    0,145    0,791      0,924    0,852      0,759    0,968     0,960     0
                 0,037    0,005    0,196      0,037    0,062      0,073    0,787     0,124     BMP
                 0,489    0,018    0,390      0,489    0,434      0,422    0,930     0,490     U
                 0,000    0,001    0,000      0,000    0,000      -0,000   0,918     0,003     DZ
                 0,259    0,025    0,135      0,259    0,177      0,170    0,872     0,123     D
Weighted Avg.    0,614    0,087    0,590      0,614    0,580      0,519    0,900     0,671     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 2147  245  160    9   45  125  139    1   13    4   52 |    a = A
  180  920   85    0   98   33   55    6   32    0   29 |    b = E
  466  492  615   17  257  146 1120   39  108    1  182 |    c = CGJLNQSRT
   33   48   38    6   16   21   56   11    5    2   25 |    d = FV
   13  145   54    1  481    0   37    6    9    0    2 |    e = I
  318  156   73    4   22  497  144    6   77    1   79 |    f = O
  119   90  157   17   60   34 6278    3   17    2   21 |    g = 0
   74  101   63    4  100   48   95   22   52    0   35 |    h = BMP
   40   48   17    1    3   71    8    9  207    0   19 |    i = U
    0    0    0    0    0    0    8    0    0    0    0 |    j = DZ
   61   58   15    1   10   31    1    9   11    0   69 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
73.03	8.33	5.44	0.31	1.53	4.25	4.73	0.03	0.44	0.14	1.77	 a = A
12.52	63.98	5.91	0.0	6.82	2.29	3.82	0.42	2.23	0.0	2.02	 b = E
13.53	14.29	17.86	0.49	7.46	4.24	32.53	1.13	3.14	0.03	5.29	 c = CGJLNQSRT
12.64	18.39	14.56	2.3	6.13	8.05	21.46	4.21	1.92	0.77	9.58	 d = FV
1.74	19.39	7.22	0.13	64.3	0.0	4.95	0.8	1.2	0.0	0.27	 e = I
23.09	11.33	5.3	0.29	1.6	36.09	10.46	0.44	5.59	0.07	5.74	 f = O
1.75	1.32	2.31	0.25	0.88	0.5	92.35	0.04	0.25	0.03	0.31	 g = 0
12.46	17.0	10.61	0.67	16.84	8.08	15.99	3.7	8.75	0.0	5.89	 h = BMP
9.46	11.35	4.02	0.24	0.71	16.78	1.89	2.13	48.94	0.0	4.49	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
22.93	21.8	5.64	0.38	3.76	11.65	0.38	3.38	4.14	0.0	25.94	 k = D
