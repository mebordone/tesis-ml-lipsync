Timestamp:2013-08-30-04:18:58
Inventario:I1
Intervalo:20ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I1-20ms-3gramas
Num Instances:  9127
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(5): class 
0F1(Hz)(8): class 
0F2(Hz)(7): class 
0F3(Hz)(3): class 
0F4(Hz)(3): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(7): class 
1F1(Hz)(9): class 
1F2(Hz)(8): class 
1F3(Hz)(3): class 
1F4(Hz)(3): class 
2Int(dB)(9): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(8): class 
2F1(Hz)(10): class 
2F2(Hz)(9): class 
2F3(Hz)(6): class 
2F4(Hz)(5): class 
class(13): 
LogScore Bayes: -269833.50494093157
LogScore BDeu: -277833.7033455248
LogScore MDL: -277012.45164439105
LogScore ENTROPY: -268422.36086716026
LogScore AIC: -270306.36086716026




=== Stratified cross-validation ===

Correctly Classified Instances        4943               54.158  %
Incorrectly Classified Instances      4184               45.842  %
Kappa statistic                          0.4421
K&B Relative Info Score             394546.4748 %
K&B Information Score                11734.5226 bits      1.2857 bits/instance
Class complexity | order 0           27125.0337 bits      2.972  bits/instance
Class complexity | scheme            79276.0039 bits      8.6859 bits/instance
Complexity improvement     (Sf)     -52150.9702 bits     -5.7139 bits/instance
Mean absolute error                      0.0715
Root mean squared error                  0.2391
Relative absolute error                 56.9227 %
Root relative squared error             95.4105 %
Coverage of cases (0.95 level)          67.5468 %
Mean rel. region size (0.95 level)      14.3142 %
Total Number of Instances             9127     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,557    0,068    0,620      0,557    0,587      0,511    0,876     0,666     a
                 0,013    0,004    0,105      0,013    0,024      0,026    0,749     0,081     b
                 0,446    0,059    0,403      0,446    0,423      0,370    0,863     0,399     e
                 0,428    0,079    0,077      0,428    0,130      0,153    0,844     0,085     d
                 0,043    0,012    0,053      0,043    0,048      0,035    0,759     0,042     f
                 0,574    0,049    0,343      0,574    0,429      0,412    0,896     0,403     i
                 0,243    0,046    0,097      0,243    0,138      0,126    0,813     0,077     k
                 0,083    0,015    0,062      0,083    0,071      0,059    0,828     0,049     j
                 0,111    0,036    0,256      0,111    0,155      0,111    0,774     0,229     l
                 0,222    0,022    0,461      0,222    0,300      0,282    0,809     0,332     o
                 0,905    0,071    0,875      0,905    0,890      0,828    0,968     0,957     0
                 0,263    0,034    0,335      0,263    0,295      0,257    0,851     0,306     s
                 0,445    0,019    0,369      0,445    0,403      0,389    0,893     0,435     u
Weighted Avg.    0,542    0,054    0,558      0,542    0,538      0,490    0,884     0,583     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
  839    3  120  148   22   51   61   13   68   83   45   40   12 |    a = a
   27    4   41   42    5   61   23   11   27    9   18   17   14 |    b = b
   79    1  333   82    2   75   37   37   19   16   25   14   26 |    c = e
   25    3   14   59    0    4    2    6   11    4    3    1    6 |    d = d
   18    2   20   23    6    5   15    6   15    6    6   14    2 |    e = f
    4    2   66   15    1  221   29    9    7    0   12   15    4 |    f = i
    3    1    1    2    4   11   44    5   10    4   74   21    1 |    g = k
   24    0   27    6    7   10    2    9    9    0    2    9    4 |    h = j
  146    6   97  196   14   96   61   20  101   18   59   64   32 |    i = l
  111    1   56  134    7   30   43    6   35  158   59   14   58 |    j = o
   23    8   16   14   29   32   53    3   33   15 2927   78    4 |    k = 0
   37    6   18   27   16   42   70   20   52    3  112  145    3 |    l = s
   18    1   18   23    0    7   15    0    7   27    4    1   97 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
55.75	0.2	7.97	9.83	1.46	3.39	4.05	0.86	4.52	5.51	2.99	2.66	0.8	 a = a
9.03	1.34	13.71	14.05	1.67	20.4	7.69	3.68	9.03	3.01	6.02	5.69	4.68	 b = b
10.59	0.13	44.64	10.99	0.27	10.05	4.96	4.96	2.55	2.14	3.35	1.88	3.49	 c = e
18.12	2.17	10.14	42.75	0.0	2.9	1.45	4.35	7.97	2.9	2.17	0.72	4.35	 d = d
13.04	1.45	14.49	16.67	4.35	3.62	10.87	4.35	10.87	4.35	4.35	10.14	1.45	 e = f
1.04	0.52	17.14	3.9	0.26	57.4	7.53	2.34	1.82	0.0	3.12	3.9	1.04	 f = i
1.66	0.55	0.55	1.1	2.21	6.08	24.31	2.76	5.52	2.21	40.88	11.6	0.55	 g = k
22.02	0.0	24.77	5.5	6.42	9.17	1.83	8.26	8.26	0.0	1.83	8.26	3.67	 h = j
16.04	0.66	10.66	21.54	1.54	10.55	6.7	2.2	11.1	1.98	6.48	7.03	3.52	 i = l
15.59	0.14	7.87	18.82	0.98	4.21	6.04	0.84	4.92	22.19	8.29	1.97	8.15	 j = o
0.71	0.25	0.49	0.43	0.9	0.99	1.64	0.09	1.02	0.46	90.48	2.41	0.12	 k = 0
6.72	1.09	3.27	4.9	2.9	7.62	12.7	3.63	9.44	0.54	20.33	26.32	0.54	 l = s
8.26	0.46	8.26	10.55	0.0	3.21	6.88	0.0	3.21	12.39	1.83	0.46	44.5	 m = u
