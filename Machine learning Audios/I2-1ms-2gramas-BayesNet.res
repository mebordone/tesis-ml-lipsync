Timestamp:2013-08-30-04:20:08
Inventario:I2
Intervalo:1ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I2-1ms-2gramas
Num Instances:  183403
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(34): class 
0Pitch(Hz)(100): class 
0RawPitch(73): class 
0SmPitch(89): class 
0Melogram(st)(83): class 
0ZCross(21): class 
0F1(Hz)(2722): class 
0F2(Hz)(3506): class 
0F3(Hz)(3673): class 
0F4(Hz)(3598): class 
1Int(dB)(33): class 
1Pitch(Hz)(100): class 
1RawPitch(71): class 
1SmPitch(95): class 
1Melogram(st)(83): class 
1ZCross(21): class 
1F1(Hz)(2725): class 
1F2(Hz)(3494): class 
1F3(Hz)(3680): class 
1F4(Hz)(3605): class 
class(12): 
LogScore Bayes: -1.1201887523886677E7
LogScore BDeu: -1.557108152406602E7
LogScore MDL: -1.4449183030826626E7
LogScore ENTROPY: -1.2428611615407893E7
LogScore AIC: -1.2762054615407892E7




=== Stratified cross-validation ===

Correctly Classified Instances      150500               82.0597 %
Incorrectly Classified Instances     32903               17.9403 %
Kappa statistic                          0.7697
K&B Relative Info Score            14386588.5648 %
K&B Information Score               411030.2111 bits      2.2411 bits/instance
Class complexity | order 0          523970.387  bits      2.8569 bits/instance
Class complexity | scheme           864411.4416 bits      4.7132 bits/instance
Complexity improvement     (Sf)    -340441.0545 bits     -1.8562 bits/instance
Mean absolute error                      0.0298
Root mean squared error                  0.1658
Relative absolute error                 22.5394 %
Root relative squared error             64.4458 %
Coverage of cases (0.95 level)          84.413  %
Mean rel. region size (0.95 level)       9.0929 %
Total Number of Instances           183403     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,868    0,010    0,939      0,868    0,902      0,886    0,967     0,939     A
                 0,571    0,009    0,755      0,571    0,650      0,642    0,948     0,697     LGJKC
                 0,872    0,010    0,871      0,872    0,872      0,861    0,971     0,907     E
                 0,677    0,007    0,570      0,677    0,619      0,615    0,936     0,668     FV
                 0,881    0,006    0,848      0,881    0,864      0,859    0,966     0,890     I
                 0,816    0,008    0,889      0,816    0,851      0,841    0,936     0,867     O
                 0,441    0,016    0,741      0,441    0,553      0,540    0,958     0,725     SNRTY
                 0,910    0,147    0,801      0,910    0,852      0,750    0,973     0,964     0
                 0,731    0,006    0,798      0,731    0,763      0,756    0,933     0,768     BMP
                 0,922    0,011    0,555      0,922    0,693      0,710    0,977     0,851     DZ
                 0,936    0,005    0,816      0,936    0,872      0,871    0,984     0,943     U
                 0,821    0,007    0,837      0,821    0,829      0,822    0,952     0,843     SNRTXY
Weighted Avg.    0,821    0,064    0,823      0,821    0,815      0,769    0,964     0,893     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 24788   344   192   217   106   188   384  1567   166   321    67   217 |     a = A
   115  5038   171    20    66    81   647  2431    36    48   127    47 |     b = LGJKC
    66   162 11987   127    52    43   220   548   134   161    48   192 |     c = E
    21    10    32  1734    28    12    71   646     1     3     3     0 |     d = FV
    14   106    27    56  6237    16   143   358    82    20     7    17 |     e = I
    80   116    21    41    36 10793    94  1464   129   323    37   100 |     f = O
   440   249   386    49   172   303  7622  7435   117   254   145   101 |     g = SNRTY
   710   541   802   772   496   578   616 65645   384   768   357   447 |     h = 0
    54    45    31     8   115    70   140  1050  4251     6    44     0 |     i = BMP
    16     0    23     1     7    15     9    93     0  2473     7    38 |     j = DZ
     8    54     2    13     1     0    62    67    24    14  3874    18 |     k = U
    74     9    91     5    36    35   279   689     6    65    32  6058 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
86.8	1.2	0.67	0.76	0.37	0.66	1.34	5.49	0.58	1.12	0.23	0.76	 a = A
1.3	57.07	1.94	0.23	0.75	0.92	7.33	27.54	0.41	0.54	1.44	0.53	 b = LGJKC
0.48	1.18	87.24	0.92	0.38	0.31	1.6	3.99	0.98	1.17	0.35	1.4	 c = E
0.82	0.39	1.25	67.71	1.09	0.47	2.77	25.22	0.04	0.12	0.12	0.0	 d = FV
0.2	1.5	0.38	0.79	88.06	0.23	2.02	5.05	1.16	0.28	0.1	0.24	 e = I
0.6	0.88	0.16	0.31	0.27	81.56	0.71	11.06	0.97	2.44	0.28	0.76	 f = O
2.55	1.44	2.23	0.28	1.0	1.75	44.13	43.04	0.68	1.47	0.84	0.58	 g = SNRTY
0.98	0.75	1.11	1.07	0.69	0.8	0.85	91.03	0.53	1.06	0.5	0.62	 h = 0
0.93	0.77	0.53	0.14	1.98	1.2	2.41	18.06	73.12	0.1	0.76	0.0	 i = BMP
0.6	0.0	0.86	0.04	0.26	0.56	0.34	3.47	0.0	92.21	0.26	1.42	 j = DZ
0.19	1.31	0.05	0.31	0.02	0.0	1.5	1.62	0.58	0.34	93.64	0.44	 k = U
1.0	0.12	1.23	0.07	0.49	0.47	3.78	9.34	0.08	0.88	0.43	82.1	 l = SNRTXY
