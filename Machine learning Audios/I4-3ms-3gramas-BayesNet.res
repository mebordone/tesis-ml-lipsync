Timestamp:2013-08-30-05:24:55
Inventario:I4
Intervalo:3ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I4-3ms-3gramas
Num Instances:  61133
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(21): class 
0Pitch(Hz)(28): class 
0RawPitch(19): class 
0SmPitch(24): class 
0Melogram(st)(27): class 
0ZCross(15): class 
0F1(Hz)(221): class 
0F2(Hz)(270): class 
0F3(Hz)(243): class 
0F4(Hz)(293): class 
1Int(dB)(20): class 
1Pitch(Hz)(33): class 
1RawPitch(21): class 
1SmPitch(24): class 
1Melogram(st)(29): class 
1ZCross(15): class 
1F1(Hz)(187): class 
1F2(Hz)(156): class 
1F3(Hz)(274): class 
1F4(Hz)(275): class 
2Int(dB)(20): class 
2Pitch(Hz)(30): class 
2RawPitch(22): class 
2SmPitch(23): class 
2Melogram(st)(30): class 
2ZCross(15): class 
2F1(Hz)(194): class 
2F2(Hz)(160): class 
2F3(Hz)(233): class 
2F4(Hz)(302): class 
class(10): 
LogScore Bayes: -3730648.8662431175
LogScore BDeu: -4008716.7978431005
LogScore MDL: -3965213.912550623
LogScore ENTROPY: -3789162.0295706694
LogScore AIC: -3821111.0295706694




=== Stratified cross-validation ===

Correctly Classified Instances       38255               62.5767 %
Incorrectly Classified Instances     22878               37.4233 %
Kappa statistic                          0.5124
K&B Relative Info Score            3197529.1632 %
K&B Information Score                81660.044  bits      1.3358 bits/instance
Class complexity | order 0          156106.2443 bits      2.5536 bits/instance
Class complexity | scheme           607094.2518 bits      9.9307 bits/instance
Complexity improvement     (Sf)    -450988.0075 bits     -7.3772 bits/instance
Mean absolute error                      0.0758
Root mean squared error                  0.2593
Relative absolute error                 49.0435 %
Root relative squared error             93.3004 %
Coverage of cases (0.95 level)          68.351  %
Mean rel. region size (0.95 level)      13.0625 %
Total Number of Instances            61133     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,669    0,020    0,166      0,669    0,265      0,326    0,928     0,313     C
                 0,688    0,085    0,399      0,688    0,505      0,474    0,903     0,542     E
                 0,329    0,013    0,266      0,329    0,294      0,285    0,858     0,262     FV
                 0,617    0,062    0,706      0,617    0,658      0,584    0,898     0,740     AI
                 0,168    0,034    0,475      0,168    0,248      0,212    0,832     0,432     CDGKNRSYZ
                 0,518    0,034    0,546      0,518    0,532      0,496    0,873     0,551     O
                 0,899    0,156    0,785      0,899    0,838      0,728    0,958     0,952     0
                 0,126    0,012    0,283      0,126    0,174      0,169    0,816     0,159     LT
                 0,711    0,026    0,390      0,711    0,503      0,512    0,951     0,625     U
                 0,340    0,020    0,355      0,340    0,347      0,327    0,837     0,306     MBP
Weighted Avg.    0,626    0,089    0,622      0,626    0,604      0,536    0,905     0,699     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   243    55     1    14    12     6    24     4     2     2 |     a = C
   180  3181    77   365   208   120   184    94    98   114 |     b = E
    18   123   284    61    29    53   214    10    29    43 |     c = FV
   373  1630   136  7379   516   526   654   150   259   344 |     d = AI
   310  1389   243  1169  1607   565  3194   202   529   368 |     e = CDGKNRSYZ
   151   428    25   542   173  2311   483    45   253    52 |     f = O
    55   479   174   531   498   300 21322   122   107   136 |     g = 0
    58   351    55   181   223   160   709   280    94   110 |     h = LT
    19    88    16    49    30   106    29    28   985    36 |     i = U
    61   248    56   159    89    83   361    56   172   663 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
66.94	15.15	0.28	3.86	3.31	1.65	6.61	1.1	0.55	0.55	 a = C
3.9	68.84	1.67	7.9	4.5	2.6	3.98	2.03	2.12	2.47	 b = E
2.08	14.24	32.87	7.06	3.36	6.13	24.77	1.16	3.36	4.98	 c = FV
3.12	13.62	1.14	61.66	4.31	4.4	5.47	1.25	2.16	2.87	 d = AI
3.24	14.51	2.54	12.21	16.78	5.9	33.35	2.11	5.52	3.84	 e = CDGKNRSYZ
3.38	9.59	0.56	12.14	3.88	51.78	10.82	1.01	5.67	1.17	 f = O
0.23	2.02	0.73	2.24	2.1	1.26	89.88	0.51	0.45	0.57	 g = 0
2.61	15.8	2.48	8.15	10.04	7.2	31.92	12.61	4.23	4.95	 h = LT
1.37	6.35	1.15	3.54	2.16	7.65	2.09	2.02	71.07	2.6	 i = U
3.13	12.73	2.87	8.16	4.57	4.26	18.53	2.87	8.83	34.03	 j = MBP
