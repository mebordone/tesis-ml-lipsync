Timestamp:2013-08-30-04:42:58
Inventario:I2
Intervalo:20ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I2-20ms-1gramas
Num Instances:  9129
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(8): class 
0F1(Hz)(9): class 
0F2(Hz)(9): class 
0F3(Hz)(5): class 
0F4(Hz)(5): class 
class(12): 
LogScore Bayes: -104933.69528369083
LogScore BDeu: -107729.84749225283
LogScore MDL: -107439.6057069011
LogScore ENTROPY: -104434.82553789392
LogScore AIC: -105093.82553789392




=== Stratified cross-validation ===

Correctly Classified Instances        5414               59.3055 %
Incorrectly Classified Instances      3715               40.6945 %
Kappa statistic                          0.4888
K&B Relative Info Score             438950.0269 %
K&B Information Score                12944.3462 bits      1.4179 bits/instance
Class complexity | order 0           26902.5371 bits      2.9469 bits/instance
Class complexity | scheme            34350.7456 bits      3.7628 bits/instance
Complexity improvement     (Sf)      -7448.2085 bits     -0.8159 bits/instance
Mean absolute error                      0.0743
Root mean squared error                  0.2226
Relative absolute error                 54.5827 %
Root relative squared error             85.364  %
Coverage of cases (0.95 level)          78.9572 %
Mean rel. region size (0.95 level)      25.5787 %
Total Number of Instances             9129     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,718    0,098    0,591      0,718    0,649      0,575    0,900     0,719     A
                 0,009    0,005    0,089      0,009    0,016      0,013    0,714     0,094     LGJKC
                 0,627    0,087    0,392      0,627    0,482      0,439    0,895     0,510     E
                 0,036    0,005    0,104      0,036    0,054      0,053    0,762     0,046     FV
                 0,605    0,038    0,413      0,605    0,491      0,474    0,916     0,484     I
                 0,347    0,034    0,464      0,347    0,397      0,358    0,828     0,400     O
                 0,194    0,041    0,345      0,194    0,248      0,199    0,837     0,313     SNRTY
                 0,936    0,118    0,813      0,936    0,870      0,796    0,976     0,964     0
                 0,020    0,006    0,109      0,020    0,034      0,033    0,769     0,097     BMP
                 0,094    0,008    0,148      0,094    0,115      0,107    0,822     0,087     DZ
                 0,427    0,013    0,449      0,427    0,438      0,424    0,911     0,443     U
                 0,150    0,031    0,176      0,150    0,162      0,128    0,817     0,140     SNRTXY
Weighted Avg.    0,593    0,076    0,536      0,593    0,553      0,500    0,895     0,611     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1081    9  149    9   25   59   59   65    3    7    7   32 |    a = A
   65    4   87    4   45   27   64  101    4    9    9   31 |    b = LGJKC
  104    7  468    5   52   15   25   25    3    4   15   23 |    c = E
   24    0   34    5    5   11   16   22    6    4    1   10 |    d = FV
    7    4   86    0  233    1   22   18    1    2    1   10 |    e = I
  176    3   91    0   12  247   25   71    4   16   37   30 |    f = O
  143   12   68    9   63   31  177  318    8    9   15   61 |    g = SNRTY
   43    3   23    7   23   18   63 3029    5    3    3   17 |    h = 0
   42    0   62    3   52   25   23   40    6    6   12   28 |    i = BMP
   34    0   30    1    5   23    6    6    4   13    4   12 |    j = DZ
   25    2   21    0    2   42    6    3    1    5   93   18 |    k = U
   84    1   75    5   47   33   27   27   10   10   10   58 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
71.83	0.6	9.9	0.6	1.66	3.92	3.92	4.32	0.2	0.47	0.47	2.13	 a = A
14.44	0.89	19.33	0.89	10.0	6.0	14.22	22.44	0.89	2.0	2.0	6.89	 b = LGJKC
13.94	0.94	62.73	0.67	6.97	2.01	3.35	3.35	0.4	0.54	2.01	3.08	 c = E
17.39	0.0	24.64	3.62	3.62	7.97	11.59	15.94	4.35	2.9	0.72	7.25	 d = FV
1.82	1.04	22.34	0.0	60.52	0.26	5.71	4.68	0.26	0.52	0.26	2.6	 e = I
24.72	0.42	12.78	0.0	1.69	34.69	3.51	9.97	0.56	2.25	5.2	4.21	 f = O
15.65	1.31	7.44	0.98	6.89	3.39	19.37	34.79	0.88	0.98	1.64	6.67	 g = SNRTY
1.33	0.09	0.71	0.22	0.71	0.56	1.95	93.57	0.15	0.09	0.09	0.53	 h = 0
14.05	0.0	20.74	1.0	17.39	8.36	7.69	13.38	2.01	2.01	4.01	9.36	 i = BMP
24.64	0.0	21.74	0.72	3.62	16.67	4.35	4.35	2.9	9.42	2.9	8.7	 j = DZ
11.47	0.92	9.63	0.0	0.92	19.27	2.75	1.38	0.46	2.29	42.66	8.26	 k = U
21.71	0.26	19.38	1.29	12.14	8.53	6.98	6.98	2.58	2.58	2.58	14.99	 l = SNRTXY
