Timestamp:2013-07-22-22:59:30
Inventario:I4
Intervalo:3ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I4-3ms-2gramas
Num Instances:  61134
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1764





=== Stratified cross-validation ===

Correctly Classified Instances       52504               85.8835 %
Incorrectly Classified Instances      8630               14.1165 %
Kappa statistic                          0.8167
K&B Relative Info Score            4804713.5762 %
K&B Information Score               122705.9001 bits      2.0072 bits/instance
Class complexity | order 0          156107.6099 bits      2.5535 bits/instance
Class complexity | scheme          3109730.9556 bits     50.8675 bits/instance
Complexity improvement     (Sf)    -2953623.3457 bits    -48.3139 bits/instance
Mean absolute error                      0.0432
Root mean squared error                  0.1482
Relative absolute error                 27.9865 %
Root relative squared error             53.3307 %
Coverage of cases (0.95 level)          95.1418 %
Mean rel. region size (0.95 level)      19.5917 %
Total Number of Instances            61134     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,832    0,001    0,875      0,832    0,853      0,853    0,963     0,869     C
                 0,893    0,014    0,842      0,893    0,867      0,856    0,972     0,914     E
                 0,616    0,003    0,733      0,616    0,669      0,667    0,905     0,622     FV
                 0,905    0,029    0,885      0,905    0,895      0,869    0,971     0,939     AI
                 0,794    0,048    0,755      0,794    0,774      0,732    0,943     0,818     CDGKNRSYZ
                 0,821    0,011    0,855      0,821    0,837      0,825    0,944     0,861     O
                 0,914    0,056    0,912      0,914    0,913      0,858    0,973     0,951     0
                 0,532    0,011    0,649      0,532    0,585      0,574    0,872     0,562     LT
                 0,895    0,001    0,949      0,895    0,921      0,920    0,984     0,944     U
                 0,690    0,006    0,805      0,690    0,743      0,737    0,918     0,745     MBP
Weighted Avg.    0,859    0,037    0,858      0,859    0,858      0,822    0,959     0,892     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   302    16     1    14    12     1    15     1     0     1 |     a = C
     6  4125    13   103   163    25   108    41     5    32 |     b = E
     1    36   532    38   176    22    30    14     3    12 |     c = FV
    10   163    22 10827   388    82   331    92     6    46 |     d = AI
     7   190    87   468  7608   154   778   206    11    67 |     e = CDGKNRSYZ
     3    46     5   114   223  3662   337    37     9    27 |     f = O
    14   193    30   389   891   224 21682   184    13   105 |     g = 0
     1    67    20   159   388    60   307  1182     9    28 |     h = LT
     1    17     2    18    39    22    20    19  1240     8 |     i = U
     0    44    14   110   185    32   165    44    10  1344 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
83.2	4.41	0.28	3.86	3.31	0.28	4.13	0.28	0.0	0.28	 a = C
0.13	89.27	0.28	2.23	3.53	0.54	2.34	0.89	0.11	0.69	 b = E
0.12	4.17	61.57	4.4	20.37	2.55	3.47	1.62	0.35	1.39	 c = FV
0.08	1.36	0.18	90.47	3.24	0.69	2.77	0.77	0.05	0.38	 d = AI
0.07	1.98	0.91	4.89	79.45	1.61	8.12	2.15	0.11	0.7	 e = CDGKNRSYZ
0.07	1.03	0.11	2.55	5.0	82.05	7.55	0.83	0.2	0.6	 f = O
0.06	0.81	0.13	1.64	3.76	0.94	91.39	0.78	0.05	0.44	 g = 0
0.05	3.02	0.9	7.16	17.47	2.7	13.82	53.22	0.41	1.26	 h = LT
0.07	1.23	0.14	1.3	2.81	1.59	1.44	1.37	89.47	0.58	 i = U
0.0	2.26	0.72	5.65	9.5	1.64	8.47	2.26	0.51	68.99	 j = MBP
