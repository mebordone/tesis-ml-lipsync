Timestamp:2013-08-30-05:26:17
Inventario:I4
Intervalo:3ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I4-3ms-5gramas
Num Instances:  61131
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(20): class 
0Pitch(Hz)(28): class 
0RawPitch(23): class 
0SmPitch(23): class 
0Melogram(st)(26): class 
0ZCross(14): class 
0F1(Hz)(227): class 
0F2(Hz)(301): class 
0F3(Hz)(333): class 
0F4(Hz)(254): class 
1Int(dB)(19): class 
1Pitch(Hz)(29): class 
1RawPitch(21): class 
1SmPitch(22): class 
1Melogram(st)(27): class 
1ZCross(15): class 
1F1(Hz)(248): class 
1F2(Hz)(168): class 
1F3(Hz)(421): class 
1F4(Hz)(217): class 
2Int(dB)(21): class 
2Pitch(Hz)(28): class 
2RawPitch(19): class 
2SmPitch(24): class 
2Melogram(st)(27): class 
2ZCross(15): class 
2F1(Hz)(221): class 
2F2(Hz)(270): class 
2F3(Hz)(243): class 
2F4(Hz)(293): class 
3Int(dB)(20): class 
3Pitch(Hz)(33): class 
3RawPitch(21): class 
3SmPitch(24): class 
3Melogram(st)(29): class 
3ZCross(15): class 
3F1(Hz)(187): class 
3F2(Hz)(156): class 
3F3(Hz)(274): class 
3F4(Hz)(275): class 
4Int(dB)(20): class 
4Pitch(Hz)(30): class 
4RawPitch(22): class 
4SmPitch(23): class 
4Melogram(st)(30): class 
4ZCross(15): class 
4F1(Hz)(194): class 
4F2(Hz)(160): class 
4F3(Hz)(233): class 
4F4(Hz)(302): class 
class(10): 
LogScore Bayes: -6216171.076449386
LogScore BDeu: -6711104.613975718
LogScore MDL: -6631194.847798074
LogScore ENTROPY: -6322012.5329131475
LogScore AIC: -6378121.5329131475




=== Stratified cross-validation ===

Correctly Classified Instances       38173               62.4446 %
Incorrectly Classified Instances     22958               37.5554 %
Kappa statistic                          0.5118
K&B Relative Info Score            3179700.9678 %
K&B Information Score                81206.1808 bits      1.3284 bits/instance
Class complexity | order 0          156103.513  bits      2.5536 bits/instance
Class complexity | scheme           984983.8904 bits     16.1127 bits/instance
Complexity improvement     (Sf)    -828880.3774 bits    -13.5591 bits/instance
Mean absolute error                      0.0754
Root mean squared error                  0.2644
Relative absolute error                 48.8187 %
Root relative squared error             95.1271 %
Coverage of cases (0.95 level)          66.1792 %
Mean rel. region size (0.95 level)      11.847  %
Total Number of Instances            61131     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,711    0,021    0,169      0,711    0,274      0,340    0,936     0,337     C
                 0,694    0,088    0,391      0,694    0,500      0,470    0,901     0,540     E
                 0,346    0,015    0,250      0,346    0,290      0,282    0,861     0,276     FV
                 0,599    0,057    0,720      0,599    0,654      0,583    0,897     0,740     AI
                 0,170    0,034    0,480      0,170    0,251      0,215    0,828     0,427     CDGKNRSYZ
                 0,530    0,034    0,549      0,530    0,539      0,504    0,875     0,560     O
                 0,896    0,156    0,785      0,896    0,837      0,726    0,947     0,913     0
                 0,138    0,013    0,286      0,138    0,186      0,178    0,817     0,162     LT
                 0,704    0,026    0,389      0,704    0,501      0,509    0,949     0,629     U
                 0,361    0,020    0,373      0,361    0,367      0,346    0,842     0,337     MBP
Weighted Avg.    0,624    0,088    0,626      0,624    0,605      0,537    0,901     0,684     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   258    53     0     9    10     3    22     7     0     1 |     a = C
   192  3209    86   317   209   128   197    96    87   100 |     b = E
    14   136   299    56    26    56   209    13    23    32 |     c = FV
   373  1675   160  7171   553   551   669   183   292   340 |     d = AI
   324  1476   280  1099  1625   566  3121   192   516   377 |     e = CDGKNRSYZ
   150   431    31   475   177  2366   480    56   248    49 |     f = O
    66   527   187   494   496   304 21260   123   108   157 |     g = 0
    53   364    74   152   170   160   735   306   114    93 |     h = LT
    29    91    17    51    28    86    38    37   976    33 |     i = U
    64   243    62   131    91    91   359    57   147   703 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
71.07	14.6	0.0	2.48	2.75	0.83	6.06	1.93	0.0	0.28	 a = C
4.15	69.44	1.86	6.86	4.52	2.77	4.26	2.08	1.88	2.16	 b = E
1.62	15.74	34.61	6.48	3.01	6.48	24.19	1.5	2.66	3.7	 c = FV
3.12	14.0	1.34	59.92	4.62	4.6	5.59	1.53	2.44	2.84	 d = AI
3.38	15.41	2.92	11.48	16.97	5.91	32.59	2.01	5.39	3.94	 e = CDGKNRSYZ
3.36	9.66	0.69	10.64	3.97	53.01	10.76	1.25	5.56	1.1	 f = O
0.28	2.22	0.79	2.08	2.09	1.28	89.62	0.52	0.46	0.66	 g = 0
2.39	16.39	3.33	6.84	7.65	7.2	33.09	13.78	5.13	4.19	 h = LT
2.09	6.57	1.23	3.68	2.02	6.2	2.74	2.67	70.42	2.38	 i = U
3.29	12.47	3.18	6.72	4.67	4.67	18.43	2.93	7.55	36.09	 j = MBP
