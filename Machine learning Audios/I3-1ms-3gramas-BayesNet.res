Timestamp:2013-08-30-04:45:43
Inventario:I3
Intervalo:1ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I3-1ms-3gramas
Num Instances:  183402
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(37): class 
0Pitch(Hz)(93): class 
0RawPitch(76): class 
0SmPitch(80): class 
0Melogram(st)(82): class 
0ZCross(20): class 
0F1(Hz)(2627): class 
0F2(Hz)(3312): class 
0F3(Hz)(3520): class 
0F4(Hz)(3447): class 
1Int(dB)(35): class 
1Pitch(Hz)(96): class 
1RawPitch(78): class 
1SmPitch(93): class 
1Melogram(st)(83): class 
1ZCross(20): class 
1F1(Hz)(2607): class 
1F2(Hz)(3303): class 
1F3(Hz)(3508): class 
1F4(Hz)(3434): class 
2Int(dB)(32): class 
2Pitch(Hz)(96): class 
2RawPitch(78): class 
2SmPitch(90): class 
2Melogram(st)(87): class 
2ZCross(20): class 
2F1(Hz)(2598): class 
2F2(Hz)(3309): class 
2F3(Hz)(3521): class 
2F4(Hz)(3448): class 
class(11): 
LogScore Bayes: -1.6725871940637575E7
LogScore BDeu: -2.239135983804559E7
LogScore MDL: -2.096805657218017E7
LogScore ENTROPY: -1.831505149066822E7
LogScore AIC: -1.8752861490668215E7




=== Stratified cross-validation ===

Correctly Classified Instances      148347               80.8862 %
Incorrectly Classified Instances     35055               19.1138 %
Kappa statistic                          0.7505
K&B Relative Info Score            14270461.5482 %
K&B Information Score               369490.099  bits      2.0146 bits/instance
Class complexity | order 0          474843.5697 bits      2.5891 bits/instance
Class complexity | scheme          1289668.7181 bits      7.0319 bits/instance
Complexity improvement     (Sf)    -814825.1484 bits     -4.4428 bits/instance
Mean absolute error                      0.0349
Root mean squared error                  0.1826
Relative absolute error                 24.7945 %
Root relative squared error             68.8427 %
Coverage of cases (0.95 level)          82.42   %
Mean rel. region size (0.95 level)       9.5853 %
Total Number of Instances           183402     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,870    0,012    0,932      0,870    0,900      0,883    0,967     0,937     A
                 0,885    0,012    0,855      0,885    0,870      0,859    0,970     0,904     E
                 0,496    0,017    0,866      0,496    0,630      0,604    0,962     0,843     CGJLNQSRT
                 0,688    0,008    0,554      0,688    0,614      0,612    0,931     0,668     FV
                 0,879    0,008    0,812      0,879    0,844      0,838    0,965     0,884     I
                 0,819    0,009    0,881      0,819    0,849      0,839    0,935     0,865     O
                 0,905    0,158    0,788      0,905    0,842      0,733    0,971     0,962     0
                 0,735    0,006    0,791      0,735    0,762      0,755    0,929     0,765     BMP
                 0,938    0,007    0,750      0,938    0,833      0,835    0,983     0,934     U
                 0,000    0,008    0,000      0,000    0,000      -0,002   0,815     0,001     DZ
                 0,954    0,016    0,466      0,954    0,626      0,661    0,990     0,866     D
Weighted Avg.    0,809    0,070    0,828      0,809    0,806      0,754    0,965     0,909     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 24855   153   493   188   100   177  1587   161    67   367   409 |     a = A
    57 12156   358   107    48    24   547   110    46   134   153 |     b = E
   917  1011 16591   145   534   575 11722   264   702    77   941 |     c = CGJLNQSRT
    16    27    66  1762    26     9   651     0     3     0     1 |     d = FV
    13    19   218    51  6226    15   377    59     3    83    19 |     e = I
    63    10   189    36    33 10843  1463   115    22   131   329 |     f = O
   686   788   966   875   591   583 65264   407   403   564   988 |     g = 0
    50    28   154     6   103    61  1083  4272    43    10     4 |     h = BMP
     5     3   112     7     1     1    76    16  3882    22    12 |     i = U
     0     0     0     0     0     0    65     0     0     0     0 |     j = DZ
     9    19    11     1     4    14    35     0     7    21  2496 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
87.04	0.54	1.73	0.66	0.35	0.62	5.56	0.56	0.23	1.29	1.43	 a = A
0.41	88.47	2.61	0.78	0.35	0.17	3.98	0.8	0.33	0.98	1.11	 b = E
2.74	3.02	49.56	0.43	1.6	1.72	35.01	0.79	2.1	0.23	2.81	 c = CGJLNQSRT
0.62	1.05	2.58	68.8	1.02	0.35	25.42	0.0	0.12	0.0	0.04	 d = FV
0.18	0.27	3.08	0.72	87.9	0.21	5.32	0.83	0.04	1.17	0.27	 e = I
0.48	0.08	1.43	0.27	0.25	81.93	11.05	0.87	0.17	0.99	2.49	 f = O
0.95	1.09	1.34	1.21	0.82	0.81	90.5	0.56	0.56	0.78	1.37	 g = 0
0.86	0.48	2.65	0.1	1.77	1.05	18.63	73.48	0.74	0.17	0.07	 h = BMP
0.12	0.07	2.71	0.17	0.02	0.02	1.84	0.39	93.84	0.53	0.29	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
0.34	0.73	0.42	0.04	0.15	0.53	1.34	0.0	0.27	0.8	95.38	 k = D
