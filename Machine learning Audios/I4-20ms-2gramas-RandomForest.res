Timestamp:2013-07-22-23:07:25
Inventario:I4
Intervalo:20ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I4-20ms-2gramas
Num Instances:  9128
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.362





=== Stratified cross-validation ===

Correctly Classified Instances        6297               68.9855 %
Incorrectly Classified Instances      2831               31.0145 %
Kappa statistic                          0.6019
K&B Relative Info Score             522526.3598 %
K&B Information Score                13676.1774 bits      1.4983 bits/instance
Class complexity | order 0           23872.4677 bits      2.6153 bits/instance
Class complexity | scheme           799132.8596 bits     87.5474 bits/instance
Complexity improvement     (Sf)    -775260.3919 bits    -84.9321 bits/instance
Mean absolute error                      0.0795
Root mean squared error                  0.2053
Relative absolute error                 50.3983 %
Root relative squared error             73.1056 %
Coverage of cases (0.95 level)          91.915  %
Mean rel. region size (0.95 level)      28.3227 %
Total Number of Instances             9128     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,233    0,002    0,483      0,233    0,315      0,333    0,852     0,251     C
                 0,597    0,049    0,520      0,597    0,556      0,514    0,892     0,549     E
                 0,080    0,005    0,183      0,080    0,111      0,112    0,723     0,062     FV
                 0,757    0,104    0,656      0,757    0,703      0,621    0,909     0,775     AI
                 0,578    0,105    0,517      0,578    0,546      0,452    0,851     0,550     CDGKNRSYZ
                 0,532    0,033    0,579      0,532    0,554      0,519    0,879     0,546     O
                 0,913    0,053    0,904      0,913    0,909      0,858    0,971     0,945     0
                 0,116    0,011    0,279      0,116    0,164      0,160    0,770     0,146     LT
                 0,495    0,004    0,755      0,495    0,598      0,604    0,897     0,580     U
                 0,177    0,009    0,405      0,177    0,247      0,252    0,813     0,227     MBP
Weighted Avg.    0,690    0,065    0,676      0,690    0,678      0,622    0,906     0,702     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   14   15    0   18    8    5    0    0    0    0 |    a = C
    5  445    3  136   96   32   13    6    1    9 |    b = E
    0   21   11   28   57   10    6    1    0    4 |    c = FV
    3  131    5 1431  176   47   64   14    3   16 |    d = AI
    3  118   18  246  862   65  109   43   11   17 |    e = CDGKNRSYZ
    2   47    2   89  115  379   53    6    7   12 |    f = O
    0   16    9   63  128   44 2955   18    1    2 |    g = 0
    0   29    5   75  114   18   46   39    2    9 |    h = LT
    1   10    0   17   30   36    1    6  108    9 |    i = U
    1   24    7   78   80   19   20    7   10   53 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
23.33	25.0	0.0	30.0	13.33	8.33	0.0	0.0	0.0	0.0	 a = C
0.67	59.65	0.4	18.23	12.87	4.29	1.74	0.8	0.13	1.21	 b = E
0.0	15.22	7.97	20.29	41.3	7.25	4.35	0.72	0.0	2.9	 c = FV
0.16	6.93	0.26	75.71	9.31	2.49	3.39	0.74	0.16	0.85	 d = AI
0.2	7.91	1.21	16.49	57.77	4.36	7.31	2.88	0.74	1.14	 e = CDGKNRSYZ
0.28	6.6	0.28	12.5	16.15	53.23	7.44	0.84	0.98	1.69	 f = O
0.0	0.49	0.28	1.95	3.96	1.36	91.32	0.56	0.03	0.06	 g = 0
0.0	8.61	1.48	22.26	33.83	5.34	13.65	11.57	0.59	2.67	 h = LT
0.46	4.59	0.0	7.8	13.76	16.51	0.46	2.75	49.54	4.13	 i = U
0.33	8.03	2.34	26.09	26.76	6.35	6.69	2.34	3.34	17.73	 j = MBP
