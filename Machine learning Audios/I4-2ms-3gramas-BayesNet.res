Timestamp:2013-08-30-05:20:01
Inventario:I4
Intervalo:2ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I4-2ms-3gramas
Num Instances:  91701
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(24): class 
0Pitch(Hz)(44): class 
0RawPitch(38): class 
0SmPitch(38): class 
0Melogram(st)(49): class 
0ZCross(17): class 
0F1(Hz)(1443): class 
0F2(Hz)(2097): class 
0F3(Hz)(2201): class 
0F4(Hz)(1978): class 
1Int(dB)(24): class 
1Pitch(Hz)(45): class 
1RawPitch(33): class 
1SmPitch(38): class 
1Melogram(st)(47): class 
1ZCross(18): class 
1F1(Hz)(1457): class 
1F2(Hz)(1987): class 
1F3(Hz)(1986): class 
1F4(Hz)(2005): class 
2Int(dB)(22): class 
2Pitch(Hz)(47): class 
2RawPitch(34): class 
2SmPitch(39): class 
2Melogram(st)(45): class 
2ZCross(18): class 
2F1(Hz)(1497): class 
2F2(Hz)(1915): class 
2F3(Hz)(1983): class 
2F4(Hz)(2163): class 
class(10): 
LogScore Bayes: -7649355.574737411
LogScore BDeu: -1.0461897607704481E7
LogScore MDL: -9758594.232822059
LogScore ENTROPY: -8427265.934012223
LogScore AIC: -8660294.934012223




=== Stratified cross-validation ===

Correctly Classified Instances       69417               75.6993 %
Incorrectly Classified Instances     22284               24.3007 %
Kappa statistic                          0.6812
K&B Relative Info Score            6418217.4608 %
K&B Information Score               163591.4602 bits      1.784  bits/instance
Class complexity | order 0          233714.4049 bits      2.5487 bits/instance
Class complexity | scheme           722778.033  bits      7.8819 bits/instance
Complexity improvement     (Sf)    -489063.6281 bits     -5.3332 bits/instance
Mean absolute error                      0.049 
Root mean squared error                  0.2124
Relative absolute error                 31.7747 %
Root relative squared error             76.5087 %
Coverage of cases (0.95 level)          79.0373 %
Mean rel. region size (0.95 level)      11.3022 %
Total Number of Instances            91701     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,849    0,018    0,215      0,849    0,343      0,422    0,950     0,696     C
                 0,843    0,035    0,664      0,843    0,743      0,725    0,956     0,834     E
                 0,625    0,009    0,484      0,625    0,546      0,543    0,902     0,596     FV
                 0,799    0,030    0,865      0,799    0,831      0,793    0,953     0,897     AI
                 0,360    0,021    0,758      0,360    0,488      0,470    0,927     0,703     CDGKNRSYZ
                 0,775    0,016    0,794      0,775    0,784      0,768    0,922     0,819     O
                 0,904    0,158    0,786      0,904    0,841      0,731    0,967     0,958     0
                 0,454    0,009    0,657      0,454    0,536      0,532    0,896     0,544     LT
                 0,904    0,012    0,641      0,904    0,750      0,755    0,976     0,903     U
                 0,675    0,010    0,685      0,675    0,680      0,670    0,899     0,706     MBP
Weighted Avg.    0,757    0,076    0,769      0,757    0,747      0,692    0,948     0,856     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   462    22     0     9     8     2    37     1     3     0 |     a = C
   157  5826    59   134   221    29   286    76    47    75 |     b = E
    18    45   804    32    24    16   327    10     4     6 |     c = FV
   396   699   221 14297   486   240  1001   200   139   215 |     d = AI
   594  1179   118  1076  5138   493  4804   130   479   261 |     e = CDGKNRSYZ
   174   144    24   170    92  5150   732    43    57    60 |     f = O
   167   610   410   627   484   437 32385   269   199   243 |     g = 0
   107   166    10   111   208    56  1062  1507    71    25 |     h = LT
    17    13     9     9    47    13    43    29  1880    20 |     i = U
    55    68     6    63    68    52   549    30    56  1968 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
84.93	4.04	0.0	1.65	1.47	0.37	6.8	0.18	0.55	0.0	 a = C
2.27	84.31	0.85	1.94	3.2	0.42	4.14	1.1	0.68	1.09	 b = E
1.4	3.5	62.52	2.49	1.87	1.24	25.43	0.78	0.31	0.47	 c = FV
2.21	3.91	1.24	79.9	2.72	1.34	5.59	1.12	0.78	1.2	 d = AI
4.16	8.26	0.83	7.54	36.0	3.45	33.66	0.91	3.36	1.83	 e = CDGKNRSYZ
2.62	2.17	0.36	2.56	1.38	77.49	11.01	0.65	0.86	0.9	 f = O
0.47	1.7	1.14	1.75	1.35	1.22	90.38	0.75	0.56	0.68	 g = 0
3.22	5.0	0.3	3.34	6.26	1.69	31.96	45.35	2.14	0.75	 h = LT
0.82	0.63	0.43	0.43	2.26	0.63	2.07	1.39	90.38	0.96	 i = U
1.89	2.33	0.21	2.16	2.33	1.78	18.83	1.03	1.92	67.51	 j = MBP
