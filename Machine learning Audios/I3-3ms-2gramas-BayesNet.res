Timestamp:2013-08-30-05:01:03
Inventario:I3
Intervalo:3ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I3-3ms-2gramas
Num Instances:  61134
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(21): class 
0Pitch(Hz)(29): class 
0RawPitch(22): class 
0SmPitch(24): class 
0Melogram(st)(32): class 
0ZCross(15): class 
0F1(Hz)(210): class 
0F2(Hz)(238): class 
0F3(Hz)(385): class 
0F4(Hz)(357): class 
1Int(dB)(20): class 
1Pitch(Hz)(35): class 
1RawPitch(22): class 
1SmPitch(25): class 
1Melogram(st)(32): class 
1ZCross(16): class 
1F1(Hz)(242): class 
1F2(Hz)(240): class 
1F3(Hz)(298): class 
1F4(Hz)(262): class 
class(11): 
LogScore Bayes: -2581975.1649439046
LogScore BDeu: -2832247.840152414
LogScore MDL: -2789668.316771559
LogScore ENTROPY: -2637773.817500233
LogScore AIC: -2665338.8175002327




=== Stratified cross-validation ===

Correctly Classified Instances       38893               63.6193 %
Incorrectly Classified Instances     22241               36.3807 %
Kappa statistic                          0.5311
K&B Relative Info Score            3396560.733  %
K&B Information Score                88318.7914 bits      1.4447 bits/instance
Class complexity | order 0          158935.881  bits      2.5998 bits/instance
Class complexity | scheme           417131.72   bits      6.8232 bits/instance
Complexity improvement     (Sf)    -258195.839  bits     -4.2234 bits/instance
Mean absolute error                      0.0676
Root mean squared error                  0.2404
Relative absolute error                 47.8543 %
Root relative squared error             90.4831 %
Coverage of cases (0.95 level)          70.8018 %
Mean rel. region size (0.95 level)      12.8948 %
Total Number of Instances            61134     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,679    0,049    0,722      0,679    0,700      0,646    0,921     0,767     A
                 0,662    0,065    0,454      0,662    0,538      0,504    0,913     0,582     E
                 0,180    0,033    0,552      0,180    0,272      0,240    0,854     0,500     CGJLNQSRT
                 0,244    0,009    0,288      0,244    0,264      0,256    0,853     0,212     FV
                 0,708    0,031    0,478      0,708    0,571      0,562    0,936     0,612     I
                 0,483    0,028    0,576      0,483    0,525      0,493    0,872     0,545     O
                 0,900    0,156    0,786      0,900    0,839      0,730    0,959     0,953     0
                 0,252    0,011    0,425      0,252    0,316      0,311    0,838     0,301     BMP
                 0,670    0,020    0,440      0,670    0,531      0,530    0,954     0,626     U
                 0,000    0,001    0,000      0,000    0,000      -0,001   0,944     0,003     DZ
                 0,538    0,045    0,149      0,538    0,234      0,265    0,920     0,252     D
Weighted Avg.    0,636    0,084    0,644      0,636    0,616      0,554    0,917     0,720     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  6506   724   366    86   201   460   523    55   139    13   512 |     a = A
   337  3061   241    51   276    83   180    83    93     4   212 |     b = E
   900  1406  2028   152   746   495  3878   253   422     5   971 |     c = CGJLNQSRT
    56   118    47   211    51    42   213    31    21     1    73 |     d = FV
    29   297   108    19  1686     3   132    34    26     8    40 |     e = I
   581   336   140    20    65  2154   489    48   232     7   391 |     f = O
   322   418   525   127   253   255 21354   109    68    36   258 |     g = 0
   136   200   136    40   194    74   355   491   151     2   169 |     h = BMP
    53    83    44     8     7   128    25    33   928     1    76 |     i = U
     0     0     0     0     0     0    23     0     0     0     0 |     j = DZ
    95   106    37    18    45    48     7    18    29     4   474 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
67.88	7.55	3.82	0.9	2.1	4.8	5.46	0.57	1.45	0.14	5.34	 a = A
7.29	66.24	5.22	1.1	5.97	1.8	3.9	1.8	2.01	0.09	4.59	 b = E
8.0	12.49	18.02	1.35	6.63	4.4	34.45	2.25	3.75	0.04	8.63	 c = CGJLNQSRT
6.48	13.66	5.44	24.42	5.9	4.86	24.65	3.59	2.43	0.12	8.45	 d = FV
1.22	12.47	4.53	0.8	70.78	0.13	5.54	1.43	1.09	0.34	1.68	 e = I
13.02	7.53	3.14	0.45	1.46	48.26	10.96	1.08	5.2	0.16	8.76	 f = O
1.36	1.76	2.21	0.54	1.07	1.07	90.01	0.46	0.29	0.15	1.09	 g = 0
6.98	10.27	6.98	2.05	9.96	3.8	18.22	25.21	7.75	0.1	8.68	 h = BMP
3.82	5.99	3.17	0.58	0.51	9.24	1.8	2.38	66.96	0.07	5.48	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
10.78	12.03	4.2	2.04	5.11	5.45	0.79	2.04	3.29	0.45	53.8	 k = D
