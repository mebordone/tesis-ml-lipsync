Timestamp:2013-07-22-20:46:22
Inventario:I0
Intervalo:15ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I0-15ms-5gramas
Num Instances:  12185
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  42 4Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  43 4RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  44 4SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  45 4Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  47 4F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  48 4F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  49 4F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  50 4F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    27 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3566





=== Stratified cross-validation ===

Correctly Classified Instances        8675               71.1941 %
Incorrectly Classified Instances      3510               28.8059 %
Kappa statistic                          0.6433
K&B Relative Info Score             749311.9928 %
K&B Information Score                25334.0957 bits      2.0791 bits/instance
Class complexity | order 0           41115.2727 bits      3.3743 bits/instance
Class complexity | scheme          1067547.0916 bits     87.6116 bits/instance
Complexity improvement     (Sf)    -1026431.819 bits    -84.2373 bits/instance
Mean absolute error                      0.0305
Root mean squared error                  0.1222
Relative absolute error                 49.7743 %
Root relative squared error             69.859  %
Coverage of cases (0.95 level)          91.9245 %
Mean rel. region size (0.95 level)      12.9962 %
Total Number of Instances            12185     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,155    0,003    0,200      0,155    0,175      0,173    0,788     0,123     ch
                 0,133    0,000    0,400      0,133    0,200      0,230    0,696     0,072     rr
                 0,949    0,086    0,863      0,949    0,904      0,848    0,979     0,960     0
                 0,279    0,004    0,453      0,279    0,345      0,350    0,830     0,254     hs
                 0,324    0,001    0,524      0,324    0,400      0,410    0,832     0,296     hg
                 0,868    0,064    0,703      0,868    0,777      0,739    0,956     0,869     a
                 0,209    0,007    0,297      0,209    0,245      0,239    0,826     0,137     c
                 0,451    0,002    0,587      0,451    0,510      0,512    0,910     0,423     b
                 0,695    0,047    0,562      0,695    0,622      0,589    0,929     0,649     e
                 0,311    0,006    0,437      0,311    0,363      0,361    0,891     0,307     d
                 0,250    0,001    0,600      0,250    0,353      0,385    0,920     0,350     g
                 0,128    0,002    0,278      0,128    0,175      0,185    0,827     0,111     f
                 0,650    0,017    0,624      0,650    0,637      0,621    0,939     0,620     i
                 0,258    0,001    0,607      0,258    0,362      0,393    0,916     0,394     j
                 0,462    0,004    0,644      0,462    0,538      0,540    0,912     0,500     m
                 0,363    0,007    0,491      0,363    0,418      0,413    0,891     0,354     l
                 0,585    0,029    0,624      0,585    0,604      0,572    0,901     0,626     o
                 0,585    0,013    0,623      0,585    0,604      0,590    0,941     0,592     n
                 0,036    0,001    0,182      0,036    0,061      0,080    0,751     0,101     q
                 0,135    0,003    0,340      0,135    0,194      0,209    0,771     0,129     p
                 0,593    0,022    0,591      0,593    0,592      0,571    0,914     0,578     s
                 0,224    0,005    0,523      0,224    0,314      0,332    0,825     0,273     r
                 0,586    0,005    0,732      0,586    0,651      0,648    0,943     0,692     u
                 0,201    0,006    0,392      0,201    0,266      0,271    0,822     0,200     t
                 0,152    0,001    0,556      0,152    0,238      0,287    0,809     0,218     v
                 0,313    0,000    0,769      0,313    0,444      0,489    0,857     0,360     y
                 0,000    0,000    0,000      0,000    0,000      0,000    0,499     0,000     z
Weighted Avg.    0,712    0,050    0,689      0,712    0,691      0,660    0,934     0,718     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m    n    o    p    q    r    s    t    u    v    w    x    y    z   aa   <-- classified as
    9    0    8    3    0    3    5    0    2    0    0    0    2    1    0    1    1    1    1    3   17    0    0    1    0    0    0 |    a = ch
    0    2    1    0    0    4    0    0    4    0    0    0    0    0    1    0    2    0    0    0    1    0    0    0    0    0    0 |    b = rr
    5    0 4190    6    0   36    8    0   19    1    0    4   18    1    1    5   27   10    0    7   50    5    4   16    1    0    0 |    c = 0
    2    0   32   39    0   18    2    0   13    0    0    1    6    0    0    1    9    3    0    1    7    2    3    1    0    0    0 |    d = hs
    0    0    2    0   11    3    0    0   11    0    0    0    1    0    2    0    3    0    0    0    0    0    1    0    0    0    0 |    e = hg
    1    1   55    2    1 1575    9    2   46    6    2    1    6    1    1    8   41   17    1    2   16   13    0    7    0    1    0 |    f = a
    4    0   67    1    0   19   38    0    5    0    0    0    4    1    0    0   10    0    2    1   21    2    1    6    0    0    0 |    g = c
    0    1    2    0    0    9    0   37    7    1    0    0    5    1    2    0   10    4    0    0    0    1    1    0    1    0    0 |    h = b
    3    1   31   11    4   84    6    4  675    7    2    1   33    2    6   10   36   18    0    2   14   10    6    2    2    1    0 |    i = e
    0    0    1    2    0   34    1    1   25   55    1    0    3    0    2    3   28   13    0    0    1    3    2    1    1    0    0 |    j = d
    0    0    4    0    0   15    0    1   16    2   18    0    5    0    2    0    4    0    0    0    1    1    3    0    0    0    0 |    k = g
    4    0   11    0    0    6    3    0    3    1    0   10    0    1    0    0    1    1    0    3   30    2    0    2    0    0    0 |    l = f
    0    0   18    1    2    9    5    1   88    1    2    0  327    1    3   10    7    7    1    0    6    3    3    6    1    1    0 |    m = i
    1    0   10    2    1   14    1    0    6    0    0    3    1   17    0    0    0    0    0    1    8    0    0    1    0    0    0 |    n = j
    0    0    6    0    0   17    0    2   19    0    1    0   23    0   85    5   12   10    0    0    0    1    1    0    2    0    0 |    o = m
    0    0    6    1    0   41    0    2   38    2    0    0   18    0    3   81   11    8    0    0    3    3    3    2    1    0    0 |    p = l
    0    0   87    3    0  127    5    4   55   23    1    0    9    0    9    8  550   24    0    2   15    1   13    3    1    0    0 |    q = o
    1    0   17    4    1   39    3    1   42    7    0    0   12    1    7    8   17  255    0    1    8    2    5    4    1    0    0 |    r = n
    3    0   19    0    0    4    8    0    4    0    0    0    4    0    0    0    2    0    2    0    7    0    0    2    0    0    0 |    s = q
    1    0   52    3    0   15    4    0    7    0    0    4    1    1    2    0    7    1    1   18    8    1    0    6    1    0    0 |    t = p
    4    0  110    4    1   31    6    0   22    2    2    7   17    0    0    6   14    5    1    1  363    4    3    8    0    1    0 |    u = s
    2    0   32    0    0   68    4    6   36    7    1    3   10    0    3    8   27   10    0    6   10   69    3    3    0    0    0 |    v = r
    0    0    4    0    0   24    3    1   21    2    0    0    9    0    2    3   37    5    0    1    3    2  167    1    0    0    0 |    w = u
    5    0   80    3    0   20   14    0    8    4    0    2    4    0    0    0   11    9    2    3   17    3    0   47    0    2    0 |    x = t
    0    0    5    0    0   17    2    0   23    1    0    0    4    0    0    5   14    6    0    1    0    2    3    1   15    0    0 |    y = v
    0    0    1    1    0    7    1    1    6    4    0    0    2    0    1    3    0    2    0    0    8    1    6    0    0   20    0 |    z = y
    0    0    4    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    1    0    0    0    0    0 |   aa = z

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z	aa	
15.52	0.0	13.79	5.17	0.0	5.17	8.62	0.0	3.45	0.0	0.0	0.0	3.45	1.72	0.0	1.72	1.72	1.72	1.72	5.17	29.31	0.0	0.0	1.72	0.0	0.0	0.0	 a = ch
0.0	13.33	6.67	0.0	0.0	26.67	0.0	0.0	26.67	0.0	0.0	0.0	0.0	0.0	6.67	0.0	13.33	0.0	0.0	0.0	6.67	0.0	0.0	0.0	0.0	0.0	0.0	 b = rr
0.11	0.0	94.93	0.14	0.0	0.82	0.18	0.0	0.43	0.02	0.0	0.09	0.41	0.02	0.02	0.11	0.61	0.23	0.0	0.16	1.13	0.11	0.09	0.36	0.02	0.0	0.0	 c = 0
1.43	0.0	22.86	27.86	0.0	12.86	1.43	0.0	9.29	0.0	0.0	0.71	4.29	0.0	0.0	0.71	6.43	2.14	0.0	0.71	5.0	1.43	2.14	0.71	0.0	0.0	0.0	 d = hs
0.0	0.0	5.88	0.0	32.35	8.82	0.0	0.0	32.35	0.0	0.0	0.0	2.94	0.0	5.88	0.0	8.82	0.0	0.0	0.0	0.0	0.0	2.94	0.0	0.0	0.0	0.0	 e = hg
0.06	0.06	3.03	0.11	0.06	86.78	0.5	0.11	2.53	0.33	0.11	0.06	0.33	0.06	0.06	0.44	2.26	0.94	0.06	0.11	0.88	0.72	0.0	0.39	0.0	0.06	0.0	 f = a
2.2	0.0	36.81	0.55	0.0	10.44	20.88	0.0	2.75	0.0	0.0	0.0	2.2	0.55	0.0	0.0	5.49	0.0	1.1	0.55	11.54	1.1	0.55	3.3	0.0	0.0	0.0	 g = c
0.0	1.22	2.44	0.0	0.0	10.98	0.0	45.12	8.54	1.22	0.0	0.0	6.1	1.22	2.44	0.0	12.2	4.88	0.0	0.0	0.0	1.22	1.22	0.0	1.22	0.0	0.0	 h = b
0.31	0.1	3.19	1.13	0.41	8.65	0.62	0.41	69.52	0.72	0.21	0.1	3.4	0.21	0.62	1.03	3.71	1.85	0.0	0.21	1.44	1.03	0.62	0.21	0.21	0.1	0.0	 i = e
0.0	0.0	0.56	1.13	0.0	19.21	0.56	0.56	14.12	31.07	0.56	0.0	1.69	0.0	1.13	1.69	15.82	7.34	0.0	0.0	0.56	1.69	1.13	0.56	0.56	0.0	0.0	 j = d
0.0	0.0	5.56	0.0	0.0	20.83	0.0	1.39	22.22	2.78	25.0	0.0	6.94	0.0	2.78	0.0	5.56	0.0	0.0	0.0	1.39	1.39	4.17	0.0	0.0	0.0	0.0	 k = g
5.13	0.0	14.1	0.0	0.0	7.69	3.85	0.0	3.85	1.28	0.0	12.82	0.0	1.28	0.0	0.0	1.28	1.28	0.0	3.85	38.46	2.56	0.0	2.56	0.0	0.0	0.0	 l = f
0.0	0.0	3.58	0.2	0.4	1.79	0.99	0.2	17.5	0.2	0.4	0.0	65.01	0.2	0.6	1.99	1.39	1.39	0.2	0.0	1.19	0.6	0.6	1.19	0.2	0.2	0.0	 m = i
1.52	0.0	15.15	3.03	1.52	21.21	1.52	0.0	9.09	0.0	0.0	4.55	1.52	25.76	0.0	0.0	0.0	0.0	0.0	1.52	12.12	0.0	0.0	1.52	0.0	0.0	0.0	 n = j
0.0	0.0	3.26	0.0	0.0	9.24	0.0	1.09	10.33	0.0	0.54	0.0	12.5	0.0	46.2	2.72	6.52	5.43	0.0	0.0	0.0	0.54	0.54	0.0	1.09	0.0	0.0	 o = m
0.0	0.0	2.69	0.45	0.0	18.39	0.0	0.9	17.04	0.9	0.0	0.0	8.07	0.0	1.35	36.32	4.93	3.59	0.0	0.0	1.35	1.35	1.35	0.9	0.45	0.0	0.0	 p = l
0.0	0.0	9.26	0.32	0.0	13.51	0.53	0.43	5.85	2.45	0.11	0.0	0.96	0.0	0.96	0.85	58.51	2.55	0.0	0.21	1.6	0.11	1.38	0.32	0.11	0.0	0.0	 q = o
0.23	0.0	3.9	0.92	0.23	8.94	0.69	0.23	9.63	1.61	0.0	0.0	2.75	0.23	1.61	1.83	3.9	58.49	0.0	0.23	1.83	0.46	1.15	0.92	0.23	0.0	0.0	 r = n
5.45	0.0	34.55	0.0	0.0	7.27	14.55	0.0	7.27	0.0	0.0	0.0	7.27	0.0	0.0	0.0	3.64	0.0	3.64	0.0	12.73	0.0	0.0	3.64	0.0	0.0	0.0	 s = q
0.75	0.0	39.1	2.26	0.0	11.28	3.01	0.0	5.26	0.0	0.0	3.01	0.75	0.75	1.5	0.0	5.26	0.75	0.75	13.53	6.02	0.75	0.0	4.51	0.75	0.0	0.0	 t = p
0.65	0.0	17.97	0.65	0.16	5.07	0.98	0.0	3.59	0.33	0.33	1.14	2.78	0.0	0.0	0.98	2.29	0.82	0.16	0.16	59.31	0.65	0.49	1.31	0.0	0.16	0.0	 u = s
0.65	0.0	10.39	0.0	0.0	22.08	1.3	1.95	11.69	2.27	0.32	0.97	3.25	0.0	0.97	2.6	8.77	3.25	0.0	1.95	3.25	22.4	0.97	0.97	0.0	0.0	0.0	 v = r
0.0	0.0	1.4	0.0	0.0	8.42	1.05	0.35	7.37	0.7	0.0	0.0	3.16	0.0	0.7	1.05	12.98	1.75	0.0	0.35	1.05	0.7	58.6	0.35	0.0	0.0	0.0	 w = u
2.14	0.0	34.19	1.28	0.0	8.55	5.98	0.0	3.42	1.71	0.0	0.85	1.71	0.0	0.0	0.0	4.7	3.85	0.85	1.28	7.26	1.28	0.0	20.09	0.0	0.85	0.0	 x = t
0.0	0.0	5.05	0.0	0.0	17.17	2.02	0.0	23.23	1.01	0.0	0.0	4.04	0.0	0.0	5.05	14.14	6.06	0.0	1.01	0.0	2.02	3.03	1.01	15.15	0.0	0.0	 y = v
0.0	0.0	1.56	1.56	0.0	10.94	1.56	1.56	9.38	6.25	0.0	0.0	3.13	0.0	1.56	4.69	0.0	3.13	0.0	0.0	12.5	1.56	9.38	0.0	0.0	31.25	0.0	 z = y
0.0	0.0	80.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	20.0	0.0	0.0	0.0	0.0	0.0	 aa = z
