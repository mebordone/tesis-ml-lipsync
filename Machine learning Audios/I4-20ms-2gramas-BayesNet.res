Timestamp:2013-08-30-05:29:45
Inventario:I4
Intervalo:20ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I4-20ms-2gramas
Num Instances:  9128
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(4): class 
0F1(Hz)(8): class 
0F2(Hz)(6): class 
0F3(Hz)(2): class 
0F4(Hz)(4): class 
1Int(dB)(9): class 
1Pitch(Hz)(5): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(6): class 
1F1(Hz)(9): class 
1F2(Hz)(9): class 
1F3(Hz)(6): class 
1F4(Hz)(4): class 
class(10): 
LogScore Bayes: -186205.6252970298
LogScore BDeu: -190007.57993713082
LogScore MDL: -189781.01061090743
LogScore ENTROPY: -185408.40125391306
LogScore AIC: -186367.40125391306




=== Stratified cross-validation ===

Correctly Classified Instances        5158               56.5074 %
Incorrectly Classified Instances      3970               43.4926 %
Kappa statistic                          0.4464
K&B Relative Info Score             407141.1102 %
K&B Information Score                10656.1783 bits      1.1674 bits/instance
Class complexity | order 0           23872.4677 bits      2.6153 bits/instance
Class complexity | scheme            55548.3162 bits      6.0855 bits/instance
Complexity improvement     (Sf)     -31675.8485 bits     -3.4702 bits/instance
Mean absolute error                      0.0898
Root mean squared error                  0.2617
Relative absolute error                 56.9102 %
Root relative squared error             93.1994 %
Coverage of cases (0.95 level)          70.7384 %
Mean rel. region size (0.95 level)      20.5609 %
Total Number of Instances             9128     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,267    0,038    0,044      0,267    0,075      0,094    0,829     0,032     C
                 0,560    0,105    0,321      0,560    0,408      0,357    0,860     0,376     E
                 0,036    0,008    0,064      0,036    0,046      0,037    0,747     0,037     FV
                 0,546    0,103    0,581      0,546    0,562      0,453    0,853     0,658     AI
                 0,185    0,054    0,403      0,185    0,254      0,184    0,752     0,357     CDGKNRSYZ
                 0,343    0,042    0,407      0,343    0,372      0,325    0,821     0,358     O
                 0,929    0,113    0,819      0,929    0,870      0,796    0,972     0,962     0
                 0,053    0,015    0,122      0,053    0,074      0,058    0,732     0,085     LT
                 0,468    0,017    0,398      0,468    0,430      0,417    0,909     0,468     U
                 0,144    0,024    0,171      0,144    0,156      0,131    0,762     0,115     MBP
Weighted Avg.    0,565    0,084    0,555      0,565    0,548      0,478    0,869     0,613     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   16   23    0   13    4    0    1    0    2    1 |    a = C
   69  418    3  104   52   31   26   17   12   14 |    b = E
   10   36    5   25   16   13   20    4    1    8 |    c = FV
   97  312   10 1031  133   94   83   41   16   73 |    d = AI
   86  224   16  293  276  118  342   28   43   66 |    e = CDGKNRSYZ
   28   97    6  160   40  244   67   18   44    8 |    f = O
    4   34   23   46   83   25 3005    3    4    9 |    g = 0
   23   65    8   43   42   20   88   18    9   21 |    h = LT
   10   21    0   20   11   36    4    6  102    8 |    i = U
   21   71    7   41   28   19   33   13   23   43 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
26.67	38.33	0.0	21.67	6.67	0.0	1.67	0.0	3.33	1.67	 a = C
9.25	56.03	0.4	13.94	6.97	4.16	3.49	2.28	1.61	1.88	 b = E
7.25	26.09	3.62	18.12	11.59	9.42	14.49	2.9	0.72	5.8	 c = FV
5.13	16.51	0.53	54.55	7.04	4.97	4.39	2.17	0.85	3.86	 d = AI
5.76	15.01	1.07	19.64	18.5	7.91	22.92	1.88	2.88	4.42	 e = CDGKNRSYZ
3.93	13.62	0.84	22.47	5.62	34.27	9.41	2.53	6.18	1.12	 f = O
0.12	1.05	0.71	1.42	2.56	0.77	92.86	0.09	0.12	0.28	 g = 0
6.82	19.29	2.37	12.76	12.46	5.93	26.11	5.34	2.67	6.23	 h = LT
4.59	9.63	0.0	9.17	5.05	16.51	1.83	2.75	46.79	3.67	 i = U
7.02	23.75	2.34	13.71	9.36	6.35	11.04	4.35	7.69	14.38	 j = MBP
