Timestamp:2013-08-30-04:27:19
Inventario:I2
Intervalo:1ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I2-1ms-5gramas
Num Instances:  183400
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  49 4F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(37): class 
0Pitch(Hz)(95): class 
0RawPitch(74): class 
0SmPitch(83): class 
0Melogram(st)(94): class 
0ZCross(21): class 
0F1(Hz)(2762): class 
0F2(Hz)(3528): class 
0F3(Hz)(3651): class 
0F4(Hz)(3602): class 
1Int(dB)(36): class 
1Pitch(Hz)(98): class 
1RawPitch(78): class 
1SmPitch(92): class 
1Melogram(st)(89): class 
1ZCross(21): class 
1F1(Hz)(2748): class 
1F2(Hz)(3533): class 
1F3(Hz)(3662): class 
1F4(Hz)(3604): class 
2Int(dB)(36): class 
2Pitch(Hz)(99): class 
2RawPitch(73): class 
2SmPitch(87): class 
2Melogram(st)(83): class 
2ZCross(21): class 
2F1(Hz)(2745): class 
2F2(Hz)(3494): class 
2F3(Hz)(3656): class 
2F4(Hz)(3597): class 
3Int(dB)(35): class 
3Pitch(Hz)(100): class 
3RawPitch(73): class 
3SmPitch(89): class 
3Melogram(st)(83): class 
3ZCross(21): class 
3F1(Hz)(2722): class 
3F2(Hz)(3506): class 
3F3(Hz)(3673): class 
3F4(Hz)(3598): class 
4Int(dB)(33): class 
4Pitch(Hz)(100): class 
4RawPitch(71): class 
4SmPitch(95): class 
4Melogram(st)(83): class 
4ZCross(21): class 
4F1(Hz)(2725): class 
4F2(Hz)(3494): class 
4F3(Hz)(3680): class 
4F4(Hz)(3605): class 
class(12): 
LogScore Bayes: -2.753496136159323E7
LogScore BDeu: -3.847255071331539E7
LogScore MDL: -3.566409204398459E7
LogScore ENTROPY: -3.0606153102620628E7
LogScore AIC: -3.1440836102620628E7




=== Stratified cross-validation ===

Correctly Classified Instances      152180               82.9771 %
Incorrectly Classified Instances     31220               17.0229 %
Kappa statistic                          0.7823
K&B Relative Info Score            14507821.6831 %
K&B Information Score               414497.8687 bits      2.2601 bits/instance
Class complexity | order 0          523966.3471 bits      2.857  bits/instance
Class complexity | scheme          2045424.3068 bits     11.1528 bits/instance
Complexity improvement     (Sf)    -1521457.9597 bits     -8.2958 bits/instance
Mean absolute error                      0.0285
Root mean squared error                  0.1658
Relative absolute error                 21.5513 %
Root relative squared error             64.4557 %
Coverage of cases (0.95 level)          83.8103 %
Mean rel. region size (0.95 level)       8.6423 %
Total Number of Instances           183400     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,870    0,008    0,950      0,870    0,908      0,893    0,969     0,942     A
                 0,588    0,009    0,771      0,588    0,667      0,659    0,952     0,709     LGJKC
                 0,880    0,009    0,890      0,880    0,885      0,875    0,972     0,909     E
                 0,695    0,009    0,535      0,695    0,605      0,603    0,942     0,666     FV
                 0,886    0,006    0,855      0,886    0,870      0,865    0,966     0,883     I
                 0,827    0,007    0,904      0,827    0,864      0,855    0,939     0,874     O
                 0,505    0,018    0,749      0,505    0,603      0,584    0,961     0,737     SNRTY
                 0,907    0,135    0,813      0,907    0,858      0,760    0,971     0,955     0
                 0,745    0,006    0,809      0,745    0,776      0,770    0,937     0,773     BMP
                 0,933    0,012    0,532      0,933    0,677      0,699    0,978     0,824     DZ
                 0,940    0,004    0,834      0,940    0,884      0,883    0,983     0,936     U
                 0,840    0,006    0,845      0,840    0,842      0,836    0,954     0,848     SNRTXY
Weighted Avg.    0,830    0,059    0,834      0,830    0,827      0,782    0,965     0,892     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 24831   323   102   305    95   162   440  1517   169   345    58   210 |     a = A
    98  5189   148    19    58    57   768  2288    24    26   113    39 |     b = LGJKC
    41   147 12088   109    35    28   237   552   125   155    44   179 |     c = E
    14     7    22  1780    26    10    76   623     0     1     2     0 |     d = FV
     8    91    21    44  6279    11   154   359    80    14     1    21 |     e = I
    43   119     7    46    28 10944   111  1437   115   280    10    94 |     f = O
   381   222   361    40   151   247  8721  6457   117   349   136    91 |     g = SNRTY
   611   535   741   965   563   555   572 65426   374   966   348   457 |     h = 0
    44    39    26     6    87    48   140  1054  4333     2    35     0 |     i = BMP
     9     0    12     1     4    10    11    91     0  2502     7    35 |     j = DZ
     2    48     5    11     1     2    73    69    16     7  3890    13 |     k = U
    43     8    56     2    18    36   342   595     0    59    23  6197 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
86.95	1.13	0.36	1.07	0.33	0.57	1.54	5.31	0.59	1.21	0.2	0.74	 a = A
1.11	58.79	1.68	0.22	0.66	0.65	8.7	25.92	0.27	0.29	1.28	0.44	 b = LGJKC
0.3	1.07	87.98	0.79	0.25	0.2	1.72	4.02	0.91	1.13	0.32	1.3	 c = E
0.55	0.27	0.86	69.5	1.02	0.39	2.97	24.33	0.0	0.04	0.08	0.0	 d = FV
0.11	1.28	0.3	0.62	88.65	0.16	2.17	5.07	1.13	0.2	0.01	0.3	 e = I
0.32	0.9	0.05	0.35	0.21	82.7	0.84	10.86	0.87	2.12	0.08	0.71	 f = O
2.21	1.29	2.09	0.23	0.87	1.43	50.49	37.38	0.68	2.02	0.79	0.53	 g = SNRTY
0.85	0.74	1.03	1.34	0.78	0.77	0.79	90.73	0.52	1.34	0.48	0.63	 h = 0
0.76	0.67	0.45	0.1	1.5	0.83	2.41	18.13	74.53	0.03	0.6	0.0	 i = BMP
0.34	0.0	0.45	0.04	0.15	0.37	0.41	3.39	0.0	93.29	0.26	1.3	 j = DZ
0.05	1.16	0.12	0.27	0.02	0.05	1.76	1.67	0.39	0.17	94.03	0.31	 k = U
0.58	0.11	0.76	0.03	0.24	0.49	4.63	8.06	0.0	0.8	0.31	83.98	 l = SNRTXY
