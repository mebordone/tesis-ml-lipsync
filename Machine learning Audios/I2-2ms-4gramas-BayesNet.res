Timestamp:2013-08-30-04:34:02
Inventario:I2
Intervalo:2ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I2-2ms-4gramas
Num Instances:  91700
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(24): class 
0Pitch(Hz)(43): class 
0RawPitch(32): class 
0SmPitch(42): class 
0Melogram(st)(45): class 
0ZCross(16): class 
0F1(Hz)(1338): class 
0F2(Hz)(2203): class 
0F3(Hz)(2483): class 
0F4(Hz)(2484): class 
1Int(dB)(23): class 
1Pitch(Hz)(44): class 
1RawPitch(34): class 
1SmPitch(39): class 
1Melogram(st)(46): class 
1ZCross(16): class 
1F1(Hz)(1715): class 
1F2(Hz)(2221): class 
1F3(Hz)(2405): class 
1F4(Hz)(2304): class 
2Int(dB)(25): class 
2Pitch(Hz)(46): class 
2RawPitch(33): class 
2SmPitch(36): class 
2Melogram(st)(46): class 
2ZCross(16): class 
2F1(Hz)(1679): class 
2F2(Hz)(2163): class 
2F3(Hz)(2662): class 
2F4(Hz)(2583): class 
3Int(dB)(23): class 
3Pitch(Hz)(46): class 
3RawPitch(35): class 
3SmPitch(39): class 
3Melogram(st)(47): class 
3ZCross(16): class 
3F1(Hz)(1632): class 
3F2(Hz)(2133): class 
3F3(Hz)(2518): class 
3F4(Hz)(2413): class 
class(12): 
LogScore Bayes: -9964967.053564755
LogScore BDeu: -1.5317818749685904E7
LogScore MDL: -1.3910269138187872E7
LogScore ENTROPY: -1.1462149157937173E7
LogScore AIC: -1.1890656157937173E7




=== Stratified cross-validation ===

Correctly Classified Instances       71914               78.4231 %
Incorrectly Classified Instances     19786               21.5769 %
Kappa statistic                          0.7249
K&B Relative Info Score            6754811.2524 %
K&B Information Score               193416.2682 bits      2.1092 bits/instance
Class complexity | order 0          262552.5757 bits      2.8632 bits/instance
Class complexity | scheme           916703.8431 bits      9.9968 bits/instance
Complexity improvement     (Sf)    -654151.2674 bits     -7.1336 bits/instance
Mean absolute error                      0.0361
Root mean squared error                  0.1833
Relative absolute error                 27.2132 %
Root relative squared error             71.2018 %
Coverage of cases (0.95 level)          80.6968 %
Mean rel. region size (0.95 level)       9.1355 %
Total Number of Instances            91700     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,816    0,015    0,909      0,816    0,860      0,837    0,959     0,913     A
                 0,458    0,009    0,720      0,458    0,560      0,558    0,908     0,592     LGJKC
                 0,838    0,022    0,755      0,838    0,794      0,778    0,962     0,864     E
                 0,629    0,009    0,509      0,629    0,563      0,559    0,905     0,606     FV
                 0,851    0,011    0,752      0,851    0,798      0,791    0,959     0,854     I
                 0,772    0,010    0,860      0,772    0,814      0,801    0,926     0,837     O
                 0,357    0,019    0,659      0,357    0,463      0,448    0,941     0,628     SNRTY
                 0,902    0,139    0,807      0,902    0,852      0,751    0,968     0,959     0
                 0,683    0,007    0,758      0,683    0,718      0,711    0,903     0,722     BMP
                 0,874    0,020    0,394      0,874    0,543      0,578    0,971     0,781     DZ
                 0,911    0,007    0,755      0,911    0,826      0,825    0,978     0,919     U
                 0,798    0,010    0,764      0,798    0,781      0,772    0,937     0,813     SNRTXY
Weighted Avg.    0,784    0,063    0,788      0,784    0,777      0,730    0,953     0,861     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 11696   140   300   178   135   174   291   746   105   319    69   180 |     a = A
   103  2027   259    12   132    54   343  1200    33   123    87    55 |     b = LGJKC
    99    95  5790    77    87    29   139   278    62   119    36    99 |     c = E
    20     5    32   809    27    11    41   307     5    20     7     2 |     d = FV
    11    41    78    21  3030    11   101   179    37    19     4    29 |     e = I
   128    65   129    21    27  5131    86   712    53   215    38    41 |     f = O
   342   127   428    65   180   131  3092  3414   136   436   159   153 |     g = SNRTY
   355   265   458   377   325   352   295 32322   175   457   134   315 |     h = 0
    43    21    64     8    51    37    84   526  1990    32    51     8 |     i = BMP
    27     0    33    10     4    15    10    42     0  1176     5    24 |     j = DZ
     4    28    14     8     0     5    44    40    20    14  1895     8 |     k = U
    44     2    81     2    31    17   168   310    10    57    24  2956 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
81.6	0.98	2.09	1.24	0.94	1.21	2.03	5.2	0.73	2.23	0.48	1.26	 a = A
2.33	45.78	5.85	0.27	2.98	1.22	7.75	27.1	0.75	2.78	1.96	1.24	 b = LGJKC
1.43	1.37	83.79	1.11	1.26	0.42	2.01	4.02	0.9	1.72	0.52	1.43	 c = E
1.56	0.39	2.49	62.91	2.1	0.86	3.19	23.87	0.39	1.56	0.54	0.16	 d = FV
0.31	1.15	2.19	0.59	85.09	0.31	2.84	5.03	1.04	0.53	0.11	0.81	 e = I
1.93	0.98	1.94	0.32	0.41	77.2	1.29	10.71	0.8	3.24	0.57	0.62	 f = O
3.95	1.47	4.94	0.75	2.08	1.51	35.69	39.41	1.57	5.03	1.84	1.77	 g = SNRTY
0.99	0.74	1.28	1.05	0.91	0.98	0.82	90.21	0.49	1.28	0.37	0.88	 h = 0
1.48	0.72	2.2	0.27	1.75	1.27	2.88	18.04	68.27	1.1	1.75	0.27	 i = BMP
2.01	0.0	2.45	0.74	0.3	1.11	0.74	3.12	0.0	87.37	0.37	1.78	 j = DZ
0.19	1.35	0.67	0.38	0.0	0.24	2.12	1.92	0.96	0.67	91.11	0.38	 k = U
1.19	0.05	2.19	0.05	0.84	0.46	4.54	8.37	0.27	1.54	0.65	79.85	 l = SNRTXY
