Timestamp:2013-08-30-05:05:16
Inventario:I3
Intervalo:10ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I3-10ms-4gramas
Num Instances:  18293
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(10): class 
0Pitch(Hz)(7): class 
0RawPitch(6): class 
0SmPitch(7): class 
0Melogram(st)(10): class 
0ZCross(8): class 
0F1(Hz)(10): class 
0F2(Hz)(13): class 
0F3(Hz)(7): class 
0F4(Hz)(11): class 
1Int(dB)(11): class 
1Pitch(Hz)(6): class 
1RawPitch(6): class 
1SmPitch(7): class 
1Melogram(st)(10): class 
1ZCross(9): class 
1F1(Hz)(12): class 
1F2(Hz)(12): class 
1F3(Hz)(9): class 
1F4(Hz)(12): class 
2Int(dB)(11): class 
2Pitch(Hz)(7): class 
2RawPitch(6): class 
2SmPitch(8): class 
2Melogram(st)(9): class 
2ZCross(10): class 
2F1(Hz)(14): class 
2F2(Hz)(12): class 
2F3(Hz)(9): class 
2F4(Hz)(12): class 
3Int(dB)(12): class 
3Pitch(Hz)(7): class 
3RawPitch(7): class 
3SmPitch(9): class 
3Melogram(st)(9): class 
3ZCross(12): class 
3F1(Hz)(12): class 
3F2(Hz)(13): class 
3F3(Hz)(10): class 
3F4(Hz)(8): class 
class(11): 
LogScore Bayes: -946275.4664949082
LogScore BDeu: -963704.9520790324
LogScore MDL: -962252.9830319119
LogScore ENTROPY: -943851.2197469281
LogScore AIC: -947601.2197469281




=== Stratified cross-validation ===

Correctly Classified Instances       10322               56.426  %
Incorrectly Classified Instances      7971               43.574  %
Kappa statistic                          0.4516
K&B Relative Info Score             836397.3286 %
K&B Information Score                22035.52   bits      1.2046 bits/instance
Class complexity | order 0           48172.4848 bits      2.6334 bits/instance
Class complexity | scheme           237447.5647 bits     12.9802 bits/instance
Complexity improvement     (Sf)    -189275.0798 bits    -10.3469 bits/instance
Mean absolute error                      0.0795
Root mean squared error                  0.2674
Relative absolute error                 55.7069 %
Root relative squared error            100.0703 %
Coverage of cases (0.95 level)          62.062  %
Mean rel. region size (0.95 level)      12.0279 %
Total Number of Instances            18293     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,576    0,060    0,647      0,576    0,609      0,541    0,890     0,690     A
                 0,507    0,058    0,427      0,507    0,463      0,415    0,875     0,438     E
                 0,151    0,052    0,404      0,151    0,220      0,152    0,774     0,375     CGJLNQSRT
                 0,130    0,017    0,099      0,130    0,112      0,099    0,788     0,061     FV
                 0,623    0,040    0,400      0,623    0,487      0,473    0,915     0,435     I
                 0,261    0,017    0,550      0,261    0,355      0,347    0,830     0,401     O
                 0,902    0,125    0,810      0,902    0,853      0,762    0,959     0,952     0
                 0,040    0,005    0,222      0,040    0,068      0,083    0,771     0,108     BMP
                 0,461    0,018    0,372      0,461    0,412      0,399    0,914     0,466     U
                 0,250    0,012    0,009      0,250    0,017      0,045    0,936     0,007     DZ
                 0,635    0,106    0,081      0,635    0,144      0,199    0,885     0,107     D
Weighted Avg.    0,564    0,076    0,591      0,564    0,555      0,493    0,884     0,634     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1694  222  204   59   74   94  136    8   17   16  416 |    a = A
  144  729  118   11  103   16   65    7   52    8  185 |    b = E
  326  336  521  121  260   76  916   35   91  106  655 |    c = CGJLNQSRT
   29   29   27   34   16   12   41    2    1    9   61 |    d = FV
   16   95   64    6  466    1   45    4    5    8   38 |    e = I
  213  109  102    9   37  360  132    2  104   18  291 |    f = O
   72   76  170   68   71   31 6128   18   15   43  103 |    g = 0
   57   70   45   29  115   15   83   24   36   10  110 |    h = BMP
   31   30   24    2   12   42   17    6  195    8   56 |    i = U
    0    0    0    0    0    0    6    0    0    2    0 |    j = DZ
   37   12   14    6   11    7    0    2    8    0  169 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
57.62	7.55	6.94	2.01	2.52	3.2	4.63	0.27	0.58	0.54	14.15	 a = A
10.01	50.7	8.21	0.76	7.16	1.11	4.52	0.49	3.62	0.56	12.87	 b = E
9.47	9.76	15.13	3.51	7.55	2.21	26.6	1.02	2.64	3.08	19.02	 c = CGJLNQSRT
11.11	11.11	10.34	13.03	6.13	4.6	15.71	0.77	0.38	3.45	23.37	 d = FV
2.14	12.7	8.56	0.8	62.3	0.13	6.02	0.53	0.67	1.07	5.08	 e = I
15.47	7.92	7.41	0.65	2.69	26.14	9.59	0.15	7.55	1.31	21.13	 f = O
1.06	1.12	2.5	1.0	1.04	0.46	90.18	0.26	0.22	0.63	1.52	 g = 0
9.6	11.78	7.58	4.88	19.36	2.53	13.97	4.04	6.06	1.68	18.52	 h = BMP
7.33	7.09	5.67	0.47	2.84	9.93	4.02	1.42	46.1	1.89	13.24	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	75.0	0.0	0.0	25.0	0.0	 j = DZ
13.91	4.51	5.26	2.26	4.14	2.63	0.0	0.75	3.01	0.0	63.53	 k = D
