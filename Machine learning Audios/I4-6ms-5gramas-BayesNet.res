Timestamp:2013-08-30-05:27:59
Inventario:I4
Intervalo:6ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I4-6ms-5gramas
Num Instances:  30449
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  45 4Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(13): class 
0Pitch(Hz)(7): class 
0RawPitch(11): class 
0SmPitch(12): class 
0Melogram(st)(15): class 
0ZCross(10): class 
0F1(Hz)(21): class 
0F2(Hz)(15): class 
0F3(Hz)(20): class 
0F4(Hz)(18): class 
1Int(dB)(14): class 
1Pitch(Hz)(7): class 
1RawPitch(12): class 
1SmPitch(10): class 
1Melogram(st)(14): class 
1ZCross(11): class 
1F1(Hz)(19): class 
1F2(Hz)(15): class 
1F3(Hz)(16): class 
1F4(Hz)(14): class 
2Int(dB)(14): class 
2Pitch(Hz)(10): class 
2RawPitch(11): class 
2SmPitch(10): class 
2Melogram(st)(14): class 
2ZCross(12): class 
2F1(Hz)(21): class 
2F2(Hz)(15): class 
2F3(Hz)(16): class 
2F4(Hz)(14): class 
3Int(dB)(14): class 
3Pitch(Hz)(10): class 
3RawPitch(10): class 
3SmPitch(10): class 
3Melogram(st)(11): class 
3ZCross(13): class 
3F1(Hz)(20): class 
3F2(Hz)(17): class 
3F3(Hz)(14): class 
3F4(Hz)(16): class 
4Int(dB)(16): class 
4Pitch(Hz)(11): class 
4RawPitch(12): class 
4SmPitch(11): class 
4Melogram(st)(14): class 
4ZCross(13): class 
4F1(Hz)(23): class 
4F2(Hz)(18): class 
4F3(Hz)(13): class 
4F4(Hz)(12): class 
class(10): 
LogScore Bayes: -2283182.019772605
LogScore BDeu: -2315570.752636459
LogScore MDL: -2312178.5536761093
LogScore ENTROPY: -2279147.5285983216
LogScore AIC: -2285546.528598321




=== Stratified cross-validation ===

Correctly Classified Instances       17339               56.9444 %
Incorrectly Classified Instances     13110               43.0556 %
Kappa statistic                          0.4465
K&B Relative Info Score            1348992.245  %
K&B Information Score                34640.1152 bits      1.1376 bits/instance
Class complexity | order 0           78165.0348 bits      2.5671 bits/instance
Class complexity | scheme           503391.4729 bits     16.5323 bits/instance
Complexity improvement     (Sf)    -425226.4381 bits    -13.9652 bits/instance
Mean absolute error                      0.0865
Root mean squared error                  0.2812
Relative absolute error                 55.7433 %
Root relative squared error            100.9592 %
Coverage of cases (0.95 level)          61.3025 %
Mean rel. region size (0.95 level)      12.5998 %
Total Number of Instances            30449     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,519    0,041    0,071      0,519    0,126      0,181    0,894     0,149     C
                 0,611    0,097    0,345      0,611    0,441      0,399    0,869     0,427     E
                 0,191    0,025    0,099      0,191    0,131      0,121    0,805     0,094     FV
                 0,505    0,061    0,673      0,505    0,577      0,498    0,869     0,678     AI
                 0,146    0,041    0,403      0,146    0,215      0,166    0,775     0,351     CDGKNRSYZ
                 0,415    0,039    0,458      0,415    0,435      0,393    0,838     0,410     O
                 0,897    0,147    0,789      0,897    0,840      0,734    0,941     0,896     0
                 0,054    0,016    0,112      0,054    0,072      0,053    0,752     0,083     LT
                 0,525    0,025    0,326      0,525    0,402      0,397    0,919     0,483     U
                 0,215    0,026    0,218      0,215    0,217      0,191    0,791     0,150     MBP
Weighted Avg.    0,569    0,088    0,579      0,569    0,556      0,485    0,873     0,616     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
    96    49     0    17     7     2     7     2     0     5 |     a = C
   179  1433    58   199   111    70   114    68    59    56 |     b = E
    34    85    82    34    33    38    89    12     5    18 |     c = FV
   352   957   169  3047   326   307   347   138   139   249 |     d = AI
   363   690   224   580   702   324  1430    92   169   228 |     e = CDGKNRSYZ
   115   271    27   270   125   935   238    52   193    25 |     f = O
    42   241   141   211   274   133 10407    48    40    70 |     g = 0
    43   209    53    63    90   115   364    60    57    65 |     h = LT
    37    65    16    32    15    67    27    30   366    42 |     i = U
    83   152    56    77    58    52   162    35    94   211 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
51.89	26.49	0.0	9.19	3.78	1.08	3.78	1.08	0.0	2.7	 a = C
7.63	61.06	2.47	8.48	4.73	2.98	4.86	2.9	2.51	2.39	 b = E
7.91	19.77	19.07	7.91	7.67	8.84	20.7	2.79	1.16	4.19	 c = FV
5.84	15.87	2.8	50.52	5.41	5.09	5.75	2.29	2.3	4.13	 d = AI
7.56	14.37	4.66	12.08	14.62	6.75	29.78	1.92	3.52	4.75	 e = CDGKNRSYZ
5.11	12.04	1.2	11.99	5.55	41.54	10.57	2.31	8.57	1.11	 f = O
0.36	2.08	1.21	1.82	2.36	1.15	89.66	0.41	0.34	0.6	 g = 0
3.84	18.68	4.74	5.63	8.04	10.28	32.53	5.36	5.09	5.81	 h = LT
5.31	9.33	2.3	4.59	2.15	9.61	3.87	4.3	52.51	6.03	 i = U
8.47	15.51	5.71	7.86	5.92	5.31	16.53	3.57	9.59	21.53	 j = MBP
