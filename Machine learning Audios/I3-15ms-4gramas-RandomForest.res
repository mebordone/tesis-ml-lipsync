Timestamp:2013-07-22-22:32:05
Inventario:I3
Intervalo:15ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I3-15ms-4gramas
Num Instances:  12186
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3106





=== Stratified cross-validation ===

Correctly Classified Instances        9110               74.7579 %
Incorrectly Classified Instances      3076               25.2421 %
Kappa statistic                          0.677 
K&B Relative Info Score             781108.4129 %
K&B Information Score                20714.2885 bits      1.6998 bits/instance
Class complexity | order 0           32287.0828 bits      2.6495 bits/instance
Class complexity | scheme           678509.2119 bits     55.6794 bits/instance
Complexity improvement     (Sf)    -646222.1291 bits    -53.0299 bits/instance
Mean absolute error                      0.0654
Root mean squared error                  0.1797
Relative absolute error                 45.5749 %
Root relative squared error             67.088  %
Coverage of cases (0.95 level)          94.8876 %
Mean rel. region size (0.95 level)      25.4353 %
Total Number of Instances            12186     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,808    0,054    0,744      0,808    0,775      0,730    0,945     0,844     A
                 0,666    0,037    0,612      0,666    0,638      0,606    0,930     0,661     E
                 0,721    0,120    0,586      0,721    0,646      0,557    0,894     0,660     CGJLNQSRT
                 0,147    0,002    0,553      0,147    0,232      0,280    0,823     0,227     FV
                 0,584    0,012    0,679      0,584    0,628      0,615    0,929     0,637     I
                 0,569    0,026    0,646      0,569    0,605      0,576    0,906     0,615     O
                 0,912    0,045    0,920      0,912    0,916      0,868    0,977     0,957     0
                 0,281    0,005    0,655      0,281    0,393      0,417    0,858     0,389     BMP
                 0,519    0,003    0,809      0,519    0,632      0,642    0,929     0,653     U
                 0,000    0,000    0,000      0,000    0,000      0,000    0,598     0,005     DZ
                 0,215    0,002    0,567      0,215    0,311      0,343    0,892     0,307     D
Weighted Avg.    0,748    0,054    0,750      0,748    0,741      0,696    0,936     0,773     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1608   52  201    0   11   48   55    5    5    0    4 |    a = A
   81  647  152    2   28   31   18    7    1    0    4 |    b = E
  210  130 1677    3   47   64  164   11   10    0    9 |    c = CGJLNQSRT
   15   19   86   26    5   17    4    3    2    0    0 |    d = FV
   12   78   98    1  294    0   12    6    1    0    1 |    e = I
  115   47  133    4    3  535   68   16    9    0   10 |    f = O
   40   23  270    4   18   30 4025    3    2    0    0 |    g = 0
   36   28  136    7   18   31   26  112    4    0    1 |    h = BMP
   16   17   45    0    5   45    2    7  148    0    0 |    i = U
    1    1    3    0    0    0    0    0    0    0    0 |    j = DZ
   27   16   62    0    4   27    1    1    1    0   38 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
80.84	2.61	10.11	0.0	0.55	2.41	2.77	0.25	0.25	0.0	0.2	 a = A
8.34	66.63	15.65	0.21	2.88	3.19	1.85	0.72	0.1	0.0	0.41	 b = E
9.03	5.59	72.13	0.13	2.02	2.75	7.05	0.47	0.43	0.0	0.39	 c = CGJLNQSRT
8.47	10.73	48.59	14.69	2.82	9.6	2.26	1.69	1.13	0.0	0.0	 d = FV
2.39	15.51	19.48	0.2	58.45	0.0	2.39	1.19	0.2	0.0	0.2	 e = I
12.23	5.0	14.15	0.43	0.32	56.91	7.23	1.7	0.96	0.0	1.06	 f = O
0.91	0.52	6.12	0.09	0.41	0.68	91.17	0.07	0.05	0.0	0.0	 g = 0
9.02	7.02	34.09	1.75	4.51	7.77	6.52	28.07	1.0	0.0	0.25	 h = BMP
5.61	5.96	15.79	0.0	1.75	15.79	0.7	2.46	51.93	0.0	0.0	 i = U
20.0	20.0	60.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 j = DZ
15.25	9.04	35.03	0.0	2.26	15.25	0.56	0.56	0.56	0.0	21.47	 k = D
