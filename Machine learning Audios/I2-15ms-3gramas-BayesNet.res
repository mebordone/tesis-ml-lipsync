Timestamp:2013-08-30-04:42:34
Inventario:I2
Intervalo:15ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I2-15ms-3gramas
Num Instances:  12187
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(4): class 
0F1(Hz)(9): class 
0F2(Hz)(8): class 
0F3(Hz)(6): class 
0F4(Hz)(6): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(9): class 
1F1(Hz)(9): class 
1F2(Hz)(9): class 
1F3(Hz)(7): class 
1F4(Hz)(6): class 
2Int(dB)(10): class 
2Pitch(Hz)(6): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(10): class 
2F1(Hz)(10): class 
2F2(Hz)(10): class 
2F3(Hz)(7): class 
2F4(Hz)(7): class 
class(12): 
LogScore Bayes: -389508.59904329537
LogScore BDeu: -398479.15906308586
LogScore MDL: -397472.11695520324
LogScore ENTROPY: -387711.18717554875
LogScore AIC: -389786.18717554875




=== Stratified cross-validation ===

Correctly Classified Instances        6837               56.1008 %
Incorrectly Classified Instances      5350               43.8992 %
Kappa statistic                          0.455 
K&B Relative Info Score             544684.3588 %
K&B Information Score                15953.4247 bits      1.3091 bits/instance
Class complexity | order 0           35677.675  bits      2.9275 bits/instance
Class complexity | scheme           113793.567  bits      9.3373 bits/instance
Complexity improvement     (Sf)     -78115.892  bits     -6.4098 bits/instance
Mean absolute error                      0.0738
Root mean squared error                  0.2467
Relative absolute error                 54.548  %
Root relative squared error             94.8447 %
Coverage of cases (0.95 level)          66.3658 %
Mean rel. region size (0.95 level)      14.0806 %
Total Number of Instances            12187     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,587    0,064    0,642      0,587    0,613      0,543    0,887     0,685     A
                 0,027    0,011    0,112      0,027    0,043      0,032    0,697     0,087     LGJKC
                 0,485    0,057    0,424      0,485    0,452      0,403    0,873     0,426     E
                 0,068    0,016    0,057      0,068    0,062      0,047    0,775     0,043     FV
                 0,596    0,040    0,392      0,596    0,473      0,456    0,912     0,421     I
                 0,260    0,017    0,566      0,260    0,356      0,351    0,822     0,407     O
                 0,229    0,052    0,331      0,229    0,271      0,210    0,806     0,302     SNRTY
                 0,914    0,112    0,823      0,914    0,866      0,787    0,968     0,959     0
                 0,010    0,004    0,075      0,010    0,018      0,016    0,765     0,089     BMP
                 0,418    0,079    0,074      0,418    0,126      0,148    0,848     0,092     DZ
                 0,449    0,014    0,435      0,449    0,442      0,429    0,912     0,476     U
                 0,202    0,042    0,174      0,202    0,187      0,150    0,825     0,142     SNRTXY
Weighted Avg.    0,561    0,068    0,557      0,561    0,548      0,494    0,885     0,598     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1167   38  164   32   49   58  106   94    2  202   13   64 |    a = A
   70   16   66   16   63    8   67  150    4   67   11   60 |    b = LGJKC
  108   27  471   10   73   15   47   40    6   91   19   64 |    c = E
   20    2   23   12    7    9   26   22    2   36    1   17 |    d = FV
   12   14   77    4  300    1   32   26    2   16    2   17 |    e = I
  151   23   80    9   29  244   52   89    3  175   49   36 |    f = O
   97    9   56   43   67   13  279  363   16  134   22  119 |    g = SNRTY
   36    2   39   45   39   17  131 4037    6   26    5   33 |    h = 0
   40    1   46   19   71    8   40   43    4   67   20   40 |    i = BMP
   31    0    9    4   10   12    8    4    2   76    7   19 |    j = DZ
   19   10   20    1    3   31   14    8    2   30  128   19 |    k = U
   66    1   60   14   55   15   42   28    4  104   17  103 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
58.67	1.91	8.25	1.61	2.46	2.92	5.33	4.73	0.1	10.16	0.65	3.22	 a = A
11.71	2.68	11.04	2.68	10.54	1.34	11.2	25.08	0.67	11.2	1.84	10.03	 b = LGJKC
11.12	2.78	48.51	1.03	7.52	1.54	4.84	4.12	0.62	9.37	1.96	6.59	 c = E
11.3	1.13	12.99	6.78	3.95	5.08	14.69	12.43	1.13	20.34	0.56	9.6	 d = FV
2.39	2.78	15.31	0.8	59.64	0.2	6.36	5.17	0.4	3.18	0.4	3.38	 e = I
16.06	2.45	8.51	0.96	3.09	25.96	5.53	9.47	0.32	18.62	5.21	3.83	 f = O
7.96	0.74	4.6	3.53	5.5	1.07	22.91	29.8	1.31	11.0	1.81	9.77	 g = SNRTY
0.82	0.05	0.88	1.02	0.88	0.38	2.97	91.42	0.14	0.59	0.11	0.75	 h = 0
10.03	0.25	11.53	4.76	17.79	2.01	10.03	10.78	1.0	16.79	5.01	10.03	 i = BMP
17.03	0.0	4.95	2.2	5.49	6.59	4.4	2.2	1.1	41.76	3.85	10.44	 j = DZ
6.67	3.51	7.02	0.35	1.05	10.88	4.91	2.81	0.7	10.53	44.91	6.67	 k = U
12.97	0.2	11.79	2.75	10.81	2.95	8.25	5.5	0.79	20.43	3.34	20.24	 l = SNRTXY
