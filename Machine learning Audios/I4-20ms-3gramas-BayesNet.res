Timestamp:2013-08-30-05:29:49
Inventario:I4
Intervalo:20ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I4-20ms-3gramas
Num Instances:  9127
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(5): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(3): class 
0F1(Hz)(8): class 
0F2(Hz)(6): class 
0F3(Hz)(2): class 
0F4(Hz)(6): class 
1Int(dB)(9): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(4): class 
1F1(Hz)(8): class 
1F2(Hz)(6): class 
1F3(Hz)(2): class 
1F4(Hz)(4): class 
2Int(dB)(9): class 
2Pitch(Hz)(5): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(6): class 
2F1(Hz)(9): class 
2F2(Hz)(9): class 
2F3(Hz)(6): class 
2F4(Hz)(4): class 
class(10): 
LogScore Bayes: -268083.35525777267
LogScore BDeu: -273457.44113647996
LogScore MDL: -273137.17433302273
LogScore ENTROPY: -266895.224081415
LogScore AIC: -268264.224081415




=== Stratified cross-validation ===

Correctly Classified Instances        4943               54.158  %
Incorrectly Classified Instances      4184               45.842  %
Kappa statistic                          0.4248
K&B Relative Info Score             386954.3011 %
K&B Information Score                10127.669  bits      1.1096 bits/instance
Class complexity | order 0           23870.9714 bits      2.6154 bits/instance
Class complexity | scheme            75542.5236 bits      8.2768 bits/instance
Complexity improvement     (Sf)     -51671.5522 bits     -5.6614 bits/instance
Mean absolute error                      0.0933
Root mean squared error                  0.274 
Relative absolute error                 59.1369 %
Root relative squared error             97.5765 %
Coverage of cases (0.95 level)          66.0239 %
Mean rel. region size (0.95 level)      18.1659 %
Total Number of Instances             9127     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,400    0,071    0,036      0,400    0,066      0,102    0,829     0,028     C
                 0,500    0,102    0,303      0,500    0,378      0,319    0,846     0,349     E
                 0,065    0,019    0,050      0,065    0,057      0,041    0,765     0,042     FV
                 0,466    0,077    0,614      0,466    0,529      0,433    0,842     0,627     AI
                 0,199    0,048    0,447      0,199    0,276      0,215    0,750     0,366     CDGKNRSYZ
                 0,320    0,040    0,406      0,320    0,358      0,313    0,816     0,345     O
                 0,915    0,106    0,825      0,915    0,868      0,792    0,965     0,945     0
                 0,065    0,025    0,091      0,065    0,076      0,047    0,728     0,079     LT
                 0,454    0,018    0,381      0,454    0,414      0,400    0,906     0,458     U
                 0,171    0,028    0,170      0,171    0,170      0,142    0,760     0,117     MBP
Weighted Avg.    0,542    0,076    0,568      0,542    0,541      0,473    0,862     0,599     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   24   22    0    7    4    0    1    0    0    2 |    a = C
  111  373    3   77   31   37   35   33   23   23 |    b = E
   15   35    9   21   22   11   15    3    1    6 |    c = FV
  167  325   36  880  107  109   95   74   27   70 |    d = AI
  189  200   48  220  297   90  293   35   30   90 |    e = CDGKNRSYZ
   58   96   15  113   27  228   71   31   54   19 |    f = O
   12   34   35   36  105   18 2960   17    3   15 |    g = 0
   40   56   17   36   46   19   77   22    7   17 |    h = LT
   20   23    2   14    3   31    9   10   99    7 |    i = U
   34   66   14   30   22   19   31   16   16   51 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
40.0	36.67	0.0	11.67	6.67	0.0	1.67	0.0	0.0	3.33	 a = C
14.88	50.0	0.4	10.32	4.16	4.96	4.69	4.42	3.08	3.08	 b = E
10.87	25.36	6.52	15.22	15.94	7.97	10.87	2.17	0.72	4.35	 c = FV
8.84	17.2	1.9	46.56	5.66	5.77	5.03	3.92	1.43	3.7	 d = AI
12.67	13.4	3.22	14.75	19.91	6.03	19.64	2.35	2.01	6.03	 e = CDGKNRSYZ
8.15	13.48	2.11	15.87	3.79	32.02	9.97	4.35	7.58	2.67	 f = O
0.37	1.05	1.08	1.11	3.25	0.56	91.5	0.53	0.09	0.46	 g = 0
11.87	16.62	5.04	10.68	13.65	5.64	22.85	6.53	2.08	5.04	 h = LT
9.17	10.55	0.92	6.42	1.38	14.22	4.13	4.59	45.41	3.21	 i = U
11.37	22.07	4.68	10.03	7.36	6.35	10.37	5.35	5.35	17.06	 j = MBP
