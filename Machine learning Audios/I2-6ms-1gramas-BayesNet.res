Timestamp:2013-08-30-04:40:19
Inventario:I2
Intervalo:6ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I2-6ms-1gramas
Num Instances:  30453
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(14): class 
0Pitch(Hz)(12): class 
0RawPitch(13): class 
0SmPitch(11): class 
0Melogram(st)(18): class 
0ZCross(12): class 
0F1(Hz)(20): class 
0F2(Hz)(19): class 
0F3(Hz)(17): class 
0F4(Hz)(15): class 
class(12): 
LogScore Bayes: -503840.0971709922
LogScore BDeu: -512925.2185033385
LogScore MDL: -511708.7327146307
LogScore ENTROPY: -502917.8979830604
LogScore AIC: -504620.8979830604




=== Stratified cross-validation ===

Correctly Classified Instances       18629               61.173  %
Incorrectly Classified Instances     11824               38.827  %
Kappa statistic                          0.5044
K&B Relative Info Score            1536962.9588 %
K&B Information Score                44366.3832 bits      1.4569 bits/instance
Class complexity | order 0           87889.7675 bits      2.8861 bits/instance
Class complexity | scheme           119449.6322 bits      3.9224 bits/instance
Complexity improvement     (Sf)     -31559.8648 bits     -1.0363 bits/instance
Mean absolute error                      0.0699
Root mean squared error                  0.2205
Relative absolute error                 52.3658 %
Root relative squared error             85.3469 %
Coverage of cases (0.95 level)          77.5195 %
Mean rel. region size (0.95 level)      21.2549 %
Total Number of Instances            30453     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,707    0,068    0,660      0,707    0,683      0,621    0,912     0,736     A
                 0,035    0,010    0,160      0,035    0,058      0,054    0,743     0,108     LGJKC
                 0,638    0,077    0,408      0,638    0,498      0,459    0,898     0,530     E
                 0,060    0,005    0,150      0,060    0,086      0,087    0,804     0,066     FV
                 0,653    0,034    0,445      0,653    0,529      0,516    0,928     0,532     I
                 0,397    0,032    0,497      0,397    0,441      0,405    0,844     0,456     O
                 0,181    0,030    0,391      0,181    0,248      0,216    0,861     0,353     SNRTY
                 0,909    0,144    0,796      0,909    0,849      0,750    0,961     0,955     0
                 0,068    0,006    0,280      0,068    0,110      0,125    0,795     0,134     BMP
                 0,260    0,017    0,185      0,260    0,217      0,206    0,871     0,149     DZ
                 0,521    0,017    0,413      0,521    0,461      0,450    0,940     0,532     U
                 0,266    0,029    0,280      0,266    0,273      0,243    0,851     0,208     SNRTXY
Weighted Avg.    0,612    0,081    0,574      0,612    0,580      0,522    0,904     0,644     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  3410    48   387    19    82   243   127   254     9    68    44   134 |     a = A
   181    53   273    13   161    87   153   402    18    39    40    75 |     b = LGJKC
   239    39  1497     5   166    53    65    91    20    36    33   103 |     c = E
    42     1    80    26    30    36    47    97    14    24    10    23 |     d = FV
    22    16   208     3   787     2    52    58     8     3    10    37 |     e = I
   418    26   236     8    33   893    50   243    12   125   143    64 |     f = O
   270    57   254    35   106    98   528  1258    22    72    36   176 |     g = SNRTY
   201    40   189    35   118   116   188 10555    23    27    24    95 |     h = 0
    99    23   151    22   142    63    43   168    67    46    92    64 |     i = BMP
    77     3    95     4    21    42     6    17     8   118    20    42 |     j = DZ
    44    11    63     0     6   100    15    10    13    32   363    40 |     k = U
   161    14   235     3   117    65    77   106    25    47    64   332 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
70.67	0.99	8.02	0.39	1.7	5.04	2.63	5.26	0.19	1.41	0.91	2.78	 a = A
12.11	3.55	18.26	0.87	10.77	5.82	10.23	26.89	1.2	2.61	2.68	5.02	 b = LGJKC
10.18	1.66	63.78	0.21	7.07	2.26	2.77	3.88	0.85	1.53	1.41	4.39	 c = E
9.77	0.23	18.6	6.05	6.98	8.37	10.93	22.56	3.26	5.58	2.33	5.35	 d = FV
1.82	1.33	17.25	0.25	65.26	0.17	4.31	4.81	0.66	0.25	0.83	3.07	 e = I
18.57	1.16	10.48	0.36	1.47	39.67	2.22	10.8	0.53	5.55	6.35	2.84	 f = O
9.27	1.96	8.72	1.2	3.64	3.37	18.13	43.2	0.76	2.47	1.24	6.04	 g = SNRTY
1.73	0.34	1.63	0.3	1.02	1.0	1.62	90.91	0.2	0.23	0.21	0.82	 h = 0
10.1	2.35	15.41	2.24	14.49	6.43	4.39	17.14	6.84	4.69	9.39	6.53	 i = BMP
17.0	0.66	20.97	0.88	4.64	9.27	1.32	3.75	1.77	26.05	4.42	9.27	 j = DZ
6.31	1.58	9.04	0.0	0.86	14.35	2.15	1.43	1.87	4.59	52.08	5.74	 k = U
12.92	1.12	18.86	0.24	9.39	5.22	6.18	8.51	2.01	3.77	5.14	26.65	 l = SNRTXY
