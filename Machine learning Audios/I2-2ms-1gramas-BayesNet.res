Timestamp:2013-08-30-04:31:52
Inventario:I2
Intervalo:2ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I2-2ms-1gramas
Num Instances:  91703
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(23): class 
0Pitch(Hz)(46): class 
0RawPitch(35): class 
0SmPitch(39): class 
0Melogram(st)(47): class 
0ZCross(16): class 
0F1(Hz)(1632): class 
0F2(Hz)(2133): class 
0F3(Hz)(2518): class 
0F4(Hz)(2413): class 
class(12): 
LogScore Bayes: -2627920.6271126587
LogScore BDeu: -3959544.0871341643
LogScore MDL: -3609663.8808918577
LogScore ENTROPY: -2999984.5251599415
LogScore AIC: -3106699.5251599415




=== Stratified cross-validation ===

Correctly Classified Instances       70059               76.3977 %
Incorrectly Classified Instances     21644               23.6023 %
Kappa statistic                          0.6971
K&B Relative Info Score            6519070.1337 %
K&B Information Score               186663.4031 bits      2.0355 bits/instance
Class complexity | order 0          262556.643  bits      2.8631 bits/instance
Class complexity | scheme           270763.6606 bits      2.9526 bits/instance
Complexity improvement     (Sf)      -8207.0176 bits     -0.0895 bits/instance
Mean absolute error                      0.0417
Root mean squared error                  0.1807
Relative absolute error                 31.4194 %
Root relative squared error             70.1891 %
Coverage of cases (0.95 level)          83.476  %
Mean rel. region size (0.95 level)      12.5222 %
Total Number of Instances            91703     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,820    0,027    0,848      0,820    0,834      0,804    0,955     0,895     A
                 0,422    0,010    0,691      0,422    0,524      0,523    0,889     0,550     LGJKC
                 0,812    0,030    0,690      0,812    0,746      0,727    0,956     0,823     E
                 0,520    0,005    0,587      0,520    0,552      0,547    0,887     0,544     FV
                 0,821    0,012    0,729      0,821    0,773      0,764    0,957     0,826     I
                 0,723    0,016    0,783      0,723    0,751      0,734    0,917     0,792     O
                 0,308    0,018    0,646      0,308    0,417      0,409    0,932     0,604     SNRTY
                 0,908    0,152    0,793      0,908    0,847      0,742    0,968     0,959     0
                 0,605    0,007    0,749      0,605    0,669      0,664    0,890     0,662     BMP
                 0,762    0,012    0,495      0,762    0,600      0,607    0,961     0,695     DZ
                 0,848    0,007    0,726      0,848    0,782      0,779    0,976     0,876     U
                 0,717    0,012    0,718      0,717    0,717      0,705    0,927     0,747     SNRTXY
Weighted Avg.    0,764    0,071    0,759      0,764    0,751      0,701    0,948     0,839     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 11760   147   399    88   137   250   266   791    69   159    81   186 |     a = A
   189  1868   316     4   125   101   310  1250    40    51    93    81 |     b = LGJKC
   228    81  5613    53    92    79   145   287    64    97    54   117 |     c = E
    41     3    66   669    39    36    46   327    23    23     6     7 |     d = FV
    33    57   134    14  2925    27    92   179    32    15    13    40 |     e = I
   338    46   194    17    29  4803    75   734    69   167    85    89 |     f = O
   456   156   459    48   172   237  2669  3938    87   182   115   144 |     g = SNRTY
   501   266   563   214   303   376   268 32544   158   258   102   280 |     h = 0
   104    32   103    14    80    78    81   525  1764    34    69    31 |     i = BMP
    65     2    65     7    18    52     7    45     4  1026     6    49 |     j = DZ
    32    34    49     4    12    48    38    36    21    22  1764    20 |     k = U
   125    10   174     8    79    50   137   358    25    39    43  2654 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
82.05	1.03	2.78	0.61	0.96	1.74	1.86	5.52	0.48	1.11	0.57	1.3	 a = A
4.27	42.19	7.14	0.09	2.82	2.28	7.0	28.23	0.9	1.15	2.1	1.83	 b = LGJKC
3.3	1.17	81.23	0.77	1.33	1.14	2.1	4.15	0.93	1.4	0.78	1.69	 c = E
3.19	0.23	5.13	52.02	3.03	2.8	3.58	25.43	1.79	1.79	0.47	0.54	 d = FV
0.93	1.6	3.76	0.39	82.14	0.76	2.58	5.03	0.9	0.42	0.37	1.12	 e = I
5.09	0.69	2.92	0.26	0.44	72.27	1.13	11.04	1.04	2.51	1.28	1.34	 f = O
5.26	1.8	5.3	0.55	1.99	2.74	30.81	45.46	1.0	2.1	1.33	1.66	 g = SNRTY
1.4	0.74	1.57	0.6	0.85	1.05	0.75	90.82	0.44	0.72	0.28	0.78	 h = 0
3.57	1.1	3.53	0.48	2.74	2.68	2.78	18.01	60.51	1.17	2.37	1.06	 i = BMP
4.83	0.15	4.83	0.52	1.34	3.86	0.52	3.34	0.3	76.23	0.45	3.64	 j = DZ
1.54	1.63	2.36	0.19	0.58	2.31	1.83	1.73	1.01	1.06	84.81	0.96	 k = U
3.38	0.27	4.7	0.22	2.13	1.35	3.7	9.67	0.68	1.05	1.16	71.69	 l = SNRTXY
