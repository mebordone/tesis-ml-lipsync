Timestamp:2013-07-22-22:31:36
Inventario:I3
Intervalo:15ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I3-15ms-2gramas
Num Instances:  12188
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.317





=== Stratified cross-validation ===

Correctly Classified Instances        8859               72.6862 %
Incorrectly Classified Instances      3329               27.3138 %
Kappa statistic                          0.6514
K&B Relative Info Score             767123.8325 %
K&B Information Score                20343.4659 bits      1.6691 bits/instance
Class complexity | order 0           32290.0118 bits      2.6493 bits/instance
Class complexity | scheme           883401.4064 bits     72.4812 bits/instance
Complexity improvement     (Sf)    -851111.3946 bits    -69.8319 bits/instance
Mean absolute error                      0.0658
Root mean squared error                  0.1855
Relative absolute error                 45.8    %
Root relative squared error             69.2544 %
Coverage of cases (0.95 level)          93.3049 %
Mean rel. region size (0.95 level)      24.1385 %
Total Number of Instances            12188     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,788    0,058    0,727      0,788    0,756      0,707    0,935     0,827     A
                 0,647    0,040    0,586      0,647    0,615      0,580    0,915     0,615     E
                 0,668    0,124    0,559      0,668    0,609      0,509    0,871     0,605     CGJLNQSRT
                 0,090    0,003    0,320      0,090    0,141      0,164    0,772     0,127     FV
                 0,567    0,014    0,628      0,567    0,596      0,580    0,926     0,595     I
                 0,570    0,029    0,625      0,570    0,597      0,565    0,887     0,606     O
                 0,899    0,047    0,915      0,899    0,907      0,855    0,971     0,949     0
                 0,301    0,009    0,529      0,301    0,383      0,384    0,839     0,325     BMP
                 0,537    0,004    0,781      0,537    0,636      0,641    0,923     0,672     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,499     0,000     DZ
                 0,169    0,002    0,536      0,169    0,258      0,296    0,884     0,268     D
Weighted Avg.    0,727    0,057    0,726      0,727    0,721      0,670    0,924     0,747     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1567   75  219    4   13   53   50    5    3    0    0 |    a = A
   93  628  150    2   27   35   18   10    4    0    4 |    b = E
  225  147 1553   12   62   75  201   21   15    3   11 |    c = CGJLNQSRT
   23   21   82   16    8   13    7    4    2    0    1 |    d = FV
   15   79   97    1  285    2   10   13    1    0    0 |    e = I
  100   44  148    5    6  536   56   24   12    0    9 |    f = O
   60   24  275    3   23   43 3971   17    1    0    0 |    g = 0
   30   31  137    4   26   23   26  120    2    0    0 |    h = BMP
   10    9   52    0    2   46    0   12  153    0    1 |    i = U
    0    0    4    0    0    0    1    0    0    0    0 |    j = DZ
   33   14   60    3    2   31    0    1    3    0   30 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
78.78	3.77	11.01	0.2	0.65	2.66	2.51	0.25	0.15	0.0	0.0	 a = A
9.58	64.68	15.45	0.21	2.78	3.6	1.85	1.03	0.41	0.0	0.41	 b = E
9.68	6.32	66.8	0.52	2.67	3.23	8.65	0.9	0.65	0.13	0.47	 c = CGJLNQSRT
12.99	11.86	46.33	9.04	4.52	7.34	3.95	2.26	1.13	0.0	0.56	 d = FV
2.98	15.71	19.28	0.2	56.66	0.4	1.99	2.58	0.2	0.0	0.0	 e = I
10.64	4.68	15.74	0.53	0.64	57.02	5.96	2.55	1.28	0.0	0.96	 f = O
1.36	0.54	6.23	0.07	0.52	0.97	89.9	0.38	0.02	0.0	0.0	 g = 0
7.52	7.77	34.34	1.0	6.52	5.76	6.52	30.08	0.5	0.0	0.0	 h = BMP
3.51	3.16	18.25	0.0	0.7	16.14	0.0	4.21	53.68	0.0	0.35	 i = U
0.0	0.0	80.0	0.0	0.0	0.0	20.0	0.0	0.0	0.0	0.0	 j = DZ
18.64	7.91	33.9	1.69	1.13	17.51	0.0	0.56	1.69	0.0	16.95	 k = D
