Timestamp:2013-08-30-05:29:15
Inventario:I4
Intervalo:15ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I4-15ms-2gramas
Num Instances:  12188
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(7): class 
0F1(Hz)(10): class 
0F2(Hz)(8): class 
0F3(Hz)(7): class 
0F4(Hz)(6): class 
1Int(dB)(9): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(8): class 
1ZCross(8): class 
1F1(Hz)(11): class 
1F2(Hz)(10): class 
1F3(Hz)(9): class 
1F4(Hz)(5): class 
class(10): 
LogScore Bayes: -274543.47322690714
LogScore BDeu: -279511.58319881477
LogScore MDL: -279138.02023925306
LogScore ENTROPY: -273544.8410944602
LogScore AIC: -274733.8410944602




=== Stratified cross-validation ===

Correctly Classified Instances        6998               57.4171 %
Incorrectly Classified Instances      5190               42.5829 %
Kappa statistic                          0.4532
K&B Relative Info Score             554945.5519 %
K&B Information Score                14429.4819 bits      1.1839 bits/instance
Class complexity | order 0           31673.8078 bits      2.5988 bits/instance
Class complexity | scheme            77682.3014 bits      6.3737 bits/instance
Complexity improvement     (Sf)     -46008.4935 bits     -3.7749 bits/instance
Mean absolute error                      0.0872
Root mean squared error                  0.2622
Relative absolute error                 55.5055 %
Root relative squared error             93.5861 %
Coverage of cases (0.95 level)          69.913  %
Mean rel. region size (0.95 level)      18.725  %
Total Number of Instances            12188     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,306    0,037    0,046      0,306    0,080      0,106    0,842     0,030     C
                 0,576    0,101    0,331      0,576    0,420      0,372    0,865     0,409     E
                 0,045    0,010    0,061      0,045    0,052      0,040    0,757     0,038     FV
                 0,563    0,097    0,599      0,563    0,580      0,477    0,865     0,680     AI
                 0,176    0,051    0,403      0,176    0,245      0,181    0,770     0,364     CDGKNRSYZ
                 0,380    0,043    0,425      0,380    0,401      0,355    0,829     0,403     O
                 0,929    0,129    0,803      0,929    0,861      0,779    0,971     0,963     0
                 0,039    0,010    0,136      0,039    0,061      0,054    0,745     0,089     LT
                 0,456    0,016    0,400      0,456    0,426      0,413    0,921     0,488     U
                 0,125    0,019    0,181      0,125    0,148      0,127    0,772     0,117     MBP
Weighted Avg.    0,574    0,088    0,560      0,574    0,553      0,483    0,877     0,630     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   22   28    0   12    5    1    2    0    2    0 |    a = C
   87  559    7  140   60   30   42   17   16   13 |    b = E
   13   46    8   23   20   23   32    3    1    8 |    c = FV
  119  402   25 1403  158  123  125   39   20   78 |    d = AI
  110  286   33  365  349  155  526   24   63   67 |    e = CDGKNRSYZ
   43  101    8  200   60  357   92   10   50   19 |    f = O
   10   54   27   76  100   28 4102    3    2   15 |    g = 0
   22   96   15   51   65   38  124   18   11   17 |    h = LT
   19   26    2   24   12   49    7    7  130    9 |    i = U
   31   92    7   49   37   36   56   11   30   50 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
30.56	38.89	0.0	16.67	6.94	1.39	2.78	0.0	2.78	0.0	 a = C
8.96	57.57	0.72	14.42	6.18	3.09	4.33	1.75	1.65	1.34	 b = E
7.34	25.99	4.52	12.99	11.3	12.99	18.08	1.69	0.56	4.52	 c = FV
4.78	16.13	1.0	56.3	6.34	4.94	5.02	1.57	0.8	3.13	 d = AI
5.56	14.46	1.67	18.45	17.64	7.84	26.59	1.21	3.19	3.39	 e = CDGKNRSYZ
4.57	10.74	0.85	21.28	6.38	37.98	9.79	1.06	5.32	2.02	 f = O
0.23	1.22	0.61	1.72	2.26	0.63	92.87	0.07	0.05	0.34	 g = 0
4.81	21.01	3.28	11.16	14.22	8.32	27.13	3.94	2.41	3.72	 h = LT
6.67	9.12	0.7	8.42	4.21	17.19	2.46	2.46	45.61	3.16	 i = U
7.77	23.06	1.75	12.28	9.27	9.02	14.04	2.76	7.52	12.53	 j = MBP
