Timestamp:2013-08-30-05:06:03
Inventario:I3
Intervalo:15ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I3-15ms-5gramas
Num Instances:  12185
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  42 4Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  43 4RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  44 4SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  45 4Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  47 4F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  48 4F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  49 4F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  50 4F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(5): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(6): class 
0ZCross(4): class 
0F1(Hz)(8): class 
0F2(Hz)(8): class 
0F3(Hz)(3): class 
0F4(Hz)(7): class 
1Int(dB)(9): class 
1Pitch(Hz)(5): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(5): class 
1F1(Hz)(9): class 
1F2(Hz)(9): class 
1F3(Hz)(5): class 
1F4(Hz)(7): class 
2Int(dB)(8): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(8): class 
2ZCross(7): class 
2F1(Hz)(10): class 
2F2(Hz)(9): class 
2F3(Hz)(7): class 
2F4(Hz)(7): class 
3Int(dB)(10): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(9): class 
3ZCross(9): class 
3F1(Hz)(10): class 
3F2(Hz)(11): class 
3F3(Hz)(8): class 
3F4(Hz)(7): class 
4Int(dB)(9): class 
4Pitch(Hz)(6): class 
4RawPitch(4): class 
4SmPitch(4): class 
4Melogram(st)(7): class 
4ZCross(10): class 
4F1(Hz)(11): class 
4F2(Hz)(9): class 
4F3(Hz)(8): class 
4F4(Hz)(7): class 
class(11): 
LogScore Bayes: -648618.4023827333
LogScore BDeu: -662145.0889149923
LogScore MDL: -661297.3055637702
LogScore ENTROPY: -646348.0555887019
LogScore AIC: -649526.0555887019




=== Stratified cross-validation ===

Correctly Classified Instances        6537               53.6479 %
Incorrectly Classified Instances      5648               46.3521 %
Kappa statistic                          0.4271
K&B Relative Info Score             519388.6413 %
K&B Information Score                13773.234  bits      1.1303 bits/instance
Class complexity | order 0           32285.6179 bits      2.6496 bits/instance
Class complexity | scheme           171236.4809 bits     14.0531 bits/instance
Complexity improvement     (Sf)    -138950.8629 bits    -11.4034 bits/instance
Mean absolute error                      0.0846
Root mean squared error                  0.2758
Relative absolute error                 58.9025 %
Root relative squared error            102.9528 %
Coverage of cases (0.95 level)          59.3106 %
Mean rel. region size (0.95 level)      12.1409 %
Total Number of Instances            12185     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,483    0,059    0,614      0,483    0,540      0,468    0,870     0,625     A
                 0,365    0,047    0,400      0,365    0,382      0,331    0,855     0,369     E
                 0,200    0,046    0,508      0,200    0,287      0,230    0,774     0,416     CGJLNQSRT
                 0,107    0,019    0,075      0,107    0,089      0,074    0,783     0,047     FV
                 0,588    0,042    0,374      0,588    0,457      0,441    0,907     0,430     I
                 0,227    0,020    0,485      0,227    0,309      0,296    0,811     0,353     O
                 0,896    0,101    0,835      0,896    0,864      0,784    0,958     0,939     0
                 0,070    0,011    0,175      0,070    0,100      0,092    0,767     0,098     BMP
                 0,460    0,017    0,396      0,460    0,425      0,412    0,908     0,453     U
                 0,200    0,018    0,005      0,200    0,009      0,028    0,920     0,012     DZ
                 0,655    0,148    0,061      0,655    0,112      0,168    0,861     0,097     D
Weighted Avg.    0,536    0,065    0,601      0,536    0,545      0,488    0,875     0,612     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
  960  145   88   37   60  115  107   14   27   33  403 |    a = A
  108  354   44    7  111   14   52   11   34   18  218 |    b = E
  213  165  464  103  160   25  414   70   43   82  586 |    c = CGJLNQSRT
   21   20   26   19    4    7   18    1    3    4   54 |    d = FV
   13   46   30    4  296    0   37   10    3    4   60 |    e = I
  137   59   48    9   42  213  105    9   59   11  248 |    f = O
   29   32  157   51   34   16 3955   15    5   48   72 |    g = 0
   38   41   40   18   68    9   30   28   18   13   96 |    h = BMP
   21   15    5    1    9   38   16    2  131    8   39 |    i = U
    0    0    1    0    0    0    3    0    0    1    0 |    j = DZ
   24    7   10    3    7    2    0    0    8    0  116 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
48.27	7.29	4.42	1.86	3.02	5.78	5.38	0.7	1.36	1.66	20.26	 a = A
11.12	36.46	4.53	0.72	11.43	1.44	5.36	1.13	3.5	1.85	22.45	 b = E
9.16	7.1	19.96	4.43	6.88	1.08	17.81	3.01	1.85	3.53	25.2	 c = CGJLNQSRT
11.86	11.3	14.69	10.73	2.26	3.95	10.17	0.56	1.69	2.26	30.51	 d = FV
2.58	9.15	5.96	0.8	58.85	0.0	7.36	1.99	0.6	0.8	11.93	 e = I
14.57	6.28	5.11	0.96	4.47	22.66	11.17	0.96	6.28	1.17	26.38	 f = O
0.66	0.72	3.56	1.16	0.77	0.36	89.6	0.34	0.11	1.09	1.63	 g = 0
9.52	10.28	10.03	4.51	17.04	2.26	7.52	7.02	4.51	3.26	24.06	 h = BMP
7.37	5.26	1.75	0.35	3.16	13.33	5.61	0.7	45.96	2.81	13.68	 i = U
0.0	0.0	20.0	0.0	0.0	0.0	60.0	0.0	0.0	20.0	0.0	 j = DZ
13.56	3.95	5.65	1.69	3.95	1.13	0.0	0.0	4.52	0.0	65.54	 k = D
