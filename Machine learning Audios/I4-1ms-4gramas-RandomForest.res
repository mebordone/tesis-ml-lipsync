Timestamp:2013-07-22-22:42:22
Inventario:I4
Intervalo:1ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I4-1ms-4gramas
Num Instances:  183401
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1191





=== Stratified cross-validation ===

Correctly Classified Instances      166313               90.6827 %
Incorrectly Classified Instances     17088                9.3173 %
Kappa statistic                          0.8783
K&B Relative Info Score            15767734.5465 %
K&B Information Score               401085.9973 bits      2.1869 bits/instance
Class complexity | order 0          466503.9053 bits      2.5436 bits/instance
Class complexity | scheme          6507396.1069 bits     35.4818 bits/instance
Complexity improvement     (Sf)    -6040892.2016 bits    -32.9382 bits/instance
Mean absolute error                      0.0277
Root mean squared error                  0.1187
Relative absolute error                 17.9814 %
Root relative squared error             42.782  %
Coverage of cases (0.95 level)          96.6734 %
Mean rel. region size (0.95 level)      16.1642 %
Total Number of Instances           183401     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,918    0,000    0,946      0,918    0,932      0,931    0,973     0,936     C
                 0,943    0,004    0,946      0,943    0,944      0,940    0,983     0,963     E
                 0,732    0,003    0,798      0,732    0,764      0,761    0,922     0,774     FV
                 0,941    0,013    0,946      0,941    0,943      0,930    0,981     0,968     AI
                 0,862    0,035    0,820      0,862    0,840      0,811    0,970     0,907     CDGKNRSYZ
                 0,876    0,005    0,933      0,876    0,904      0,897    0,958     0,911     O
                 0,942    0,053    0,920      0,942    0,931      0,885    0,984     0,967     0
                 0,656    0,006    0,796      0,656    0,719      0,713    0,913     0,732     LT
                 0,958    0,000    0,982      0,958    0,970      0,969    0,992     0,980     U
                 0,789    0,003    0,890      0,789    0,837      0,833    0,944     0,847     MBP
Weighted Avg.    0,907    0,030    0,907      0,907    0,906      0,880    0,975     0,939     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   994    12     2    13    31     3    21     3     2     2 |     a = C
     7 12954    18    89   315    17   245    52     4    39 |     b = E
     3    30  1875    58   431    26   107    17     1    13 |     c = FV
     6    81    46 33547   740    58   926   164    14    58 |     d = AI
    14   221   216   544 24520   180  2223   368    18   155 |     e = CDGKNRSYZ
     3    31    40   112   365 11591   994    60     2    36 |     f = O
    18   200    86   673  2218   419 67940   358     9   193 |     g = 0
     2    80    35   233   835    67   961  4339     9    58 |     h = LT
     1    17     0    38    40    15    33    18  3963    12 |     i = U
     3    71    32   169   397    41   422    75    14  4590 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
91.78	1.11	0.18	1.2	2.86	0.28	1.94	0.28	0.18	0.18	 a = C
0.05	94.28	0.13	0.65	2.29	0.12	1.78	0.38	0.03	0.28	 b = E
0.12	1.17	73.21	2.26	16.83	1.02	4.18	0.66	0.04	0.51	 c = FV
0.02	0.23	0.13	94.13	2.08	0.16	2.6	0.46	0.04	0.16	 d = AI
0.05	0.78	0.76	1.91	86.16	0.63	7.81	1.29	0.06	0.54	 e = CDGKNRSYZ
0.02	0.23	0.3	0.85	2.76	87.59	7.51	0.45	0.02	0.27	 f = O
0.02	0.28	0.12	0.93	3.08	0.58	94.21	0.5	0.01	0.27	 g = 0
0.03	1.21	0.53	3.52	12.62	1.01	14.52	65.55	0.14	0.88	 h = LT
0.02	0.41	0.0	0.92	0.97	0.36	0.8	0.44	95.79	0.29	 i = U
0.05	1.22	0.55	2.91	6.83	0.71	7.26	1.29	0.24	78.95	 j = MBP
