Timestamp:2013-07-22-21:56:36
Inventario:I2
Intervalo:10ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I2-10ms-5gramas
Num Instances:  18292
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  43 4RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  44 4SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  45 4Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  47 4F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  49 4F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  50 4F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.2971





=== Stratified cross-validation ===

Correctly Classified Instances       14107               77.1211 %
Incorrectly Classified Instances      4185               22.8789 %
Kappa statistic                          0.7145
K&B Relative Info Score            1235885.1882 %
K&B Information Score                35956.7662 bits      1.9657 bits/instance
Class complexity | order 0           53202.6243 bits      2.9085 bits/instance
Class complexity | scheme           968809.9237 bits     52.9636 bits/instance
Complexity improvement     (Sf)    -915607.2994 bits    -50.0551 bits/instance
Mean absolute error                      0.0589
Root mean squared error                  0.1673
Relative absolute error                 43.8123 %
Root relative squared error             64.5164 %
Coverage of cases (0.95 level)          95.14   %
Mean rel. region size (0.95 level)      24.9066 %
Total Number of Instances            18292     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,866    0,054    0,755      0,866    0,807      0,769    0,957     0,879     A
                 0,466    0,021    0,539      0,466    0,500      0,478    0,872     0,434     LGJKC
                 0,761    0,031    0,677      0,761    0,717      0,693    0,952     0,750     E
                 0,341    0,004    0,578      0,341    0,429      0,438    0,893     0,381     FV
                 0,697    0,014    0,674      0,697    0,685      0,672    0,948     0,722     I
                 0,679    0,022    0,713      0,679    0,695      0,672    0,929     0,719     O
                 0,624    0,048    0,585      0,624    0,604      0,560    0,914     0,615     SNRTY
                 0,921    0,055    0,909      0,921    0,915      0,864    0,976     0,958     0
                 0,468    0,006    0,734      0,468    0,571      0,575    0,899     0,537     BMP
                 0,420    0,002    0,737      0,420    0,535      0,551    0,945     0,543     DZ
                 0,660    0,003    0,823      0,660    0,732      0,731    0,950     0,762     U
                 0,599    0,009    0,740      0,599    0,662      0,653    0,946     0,672     SNRTXY
Weighted Avg.    0,771    0,040    0,770      0,771    0,767      0,732    0,949     0,798     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 2547   51   59    5   23   51   90   81   10    3    4   16 |    a = A
  122  417   47    6   31   27  120   91    6    1    8   18 |    b = LGJKC
   89   40 1095    5   41   24   68   21   16    8    7   24 |    c = E
   33   16   32   89   10   10   52   10    2    0    3    4 |    d = FV
   22   21   89    6  521    4   39   25    8    2    4    7 |    e = I
  136   35   47    7   11  935   45  101   14   13    9   24 |    f = O
  157   64   69   24   44   60 1117  207   15    2    4   27 |    g = SNRTY
   94   58   47    2   36   60  211 6259   13    0    4   10 |    h = 0
   51   21   38    7   36   29   68   48  278    2    9    7 |    i = BMP
   35    6   29    0    3   43   15    5    2  115    2   19 |    j = DZ
   24   17   18    2    5   45   14    9    6    0  279    4 |    k = U
   65   28   47    1   12   24   70   32    9   10    6  455 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
86.63	1.73	2.01	0.17	0.78	1.73	3.06	2.76	0.34	0.1	0.14	0.54	 a = A
13.65	46.64	5.26	0.67	3.47	3.02	13.42	10.18	0.67	0.11	0.89	2.01	 b = LGJKC
6.19	2.78	76.15	0.35	2.85	1.67	4.73	1.46	1.11	0.56	0.49	1.67	 c = E
12.64	6.13	12.26	34.1	3.83	3.83	19.92	3.83	0.77	0.0	1.15	1.53	 d = FV
2.94	2.81	11.9	0.8	69.65	0.53	5.21	3.34	1.07	0.27	0.53	0.94	 e = I
9.88	2.54	3.41	0.51	0.8	67.9	3.27	7.33	1.02	0.94	0.65	1.74	 f = O
8.77	3.58	3.85	1.34	2.46	3.35	62.4	11.56	0.84	0.11	0.22	1.51	 g = SNRTY
1.38	0.85	0.69	0.03	0.53	0.88	3.11	92.13	0.19	0.0	0.06	0.15	 h = 0
8.59	3.54	6.4	1.18	6.06	4.88	11.45	8.08	46.8	0.34	1.52	1.18	 i = BMP
12.77	2.19	10.58	0.0	1.09	15.69	5.47	1.82	0.73	41.97	0.73	6.93	 j = DZ
5.67	4.02	4.26	0.47	1.18	10.64	3.31	2.13	1.42	0.0	65.96	0.95	 k = U
8.56	3.69	6.19	0.13	1.58	3.16	9.22	4.22	1.19	1.32	0.79	59.95	 l = SNRTXY
