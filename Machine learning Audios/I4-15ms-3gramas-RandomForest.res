Timestamp:2013-07-22-23:06:26
Inventario:I4
Intervalo:15ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I4-15ms-3gramas
Num Instances:  12187
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3302





=== Stratified cross-validation ===

Correctly Classified Instances        8833               72.4789 %
Incorrectly Classified Instances      3354               27.5211 %
Kappa statistic                          0.6455
K&B Relative Info Score             739314.9298 %
K&B Information Score                19224.5657 bits      1.5775 bits/instance
Class complexity | order 0           31672.3435 bits      2.5989 bits/instance
Class complexity | scheme           796084.0082 bits     65.3224 bits/instance
Complexity improvement     (Sf)    -764411.6648 bits    -62.7235 bits/instance
Mean absolute error                      0.0752
Root mean squared error                  0.195 
Relative absolute error                 47.9055 %
Root relative squared error             69.6108 %
Coverage of cases (0.95 level)          93.9936 %
Mean rel. region size (0.95 level)      28.5206 %
Total Number of Instances            12187     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,250    0,002    0,429      0,250    0,316      0,324    0,883     0,258     C
                 0,664    0,042    0,578      0,664    0,618      0,585    0,924     0,637     E
                 0,186    0,005    0,340      0,186    0,241      0,244    0,776     0,115     FV
                 0,787    0,088    0,697      0,787    0,739      0,669    0,929     0,814     AI
                 0,646    0,102    0,551      0,646    0,595      0,511    0,878     0,607     CDGKNRSYZ
                 0,546    0,025    0,650      0,546    0,593      0,565    0,899     0,606     O
                 0,911    0,052    0,909      0,911    0,910      0,859    0,975     0,953     0
                 0,184    0,009    0,438      0,184    0,259      0,266    0,815     0,225     LT
                 0,565    0,003    0,805      0,565    0,664      0,668    0,918     0,662     U
                 0,291    0,006    0,604      0,291    0,393      0,406    0,835     0,362     MBP
Weighted Avg.    0,725    0,059    0,720      0,725    0,716      0,666    0,925     0,747     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   18   10    1   26    7    6    3    1    0    0 |    a = C
    7  645    1  142  105   33   14   13    2    9 |    b = E
    0   25   33   23   70   10    7    2    1    6 |    c = FV
    7  148    9 1961  221   49   68   16    3   10 |    d = AI
    3  116   23  282 1278   64  151   37    8   16 |    e = CDGKNRSYZ
    4   58    2  126  136  513   62   12   13   14 |    f = O
    0   28   13   68  229   32 4024   14    2    6 |    g = 0
    1   37   10   85  146   22   59   84    3   10 |    h = LT
    1   17    0   27   27   39    3    5  161    5 |    i = U
    1   31    5   74  102   21   34    8    7  116 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
25.0	13.89	1.39	36.11	9.72	8.33	4.17	1.39	0.0	0.0	 a = C
0.72	66.43	0.1	14.62	10.81	3.4	1.44	1.34	0.21	0.93	 b = E
0.0	14.12	18.64	12.99	39.55	5.65	3.95	1.13	0.56	3.39	 c = FV
0.28	5.94	0.36	78.69	8.87	1.97	2.73	0.64	0.12	0.4	 d = AI
0.15	5.86	1.16	14.26	64.61	3.24	7.63	1.87	0.4	0.81	 e = CDGKNRSYZ
0.43	6.17	0.21	13.4	14.47	54.57	6.6	1.28	1.38	1.49	 f = O
0.0	0.63	0.29	1.54	5.19	0.72	91.12	0.32	0.05	0.14	 g = 0
0.22	8.1	2.19	18.6	31.95	4.81	12.91	18.38	0.66	2.19	 h = LT
0.35	5.96	0.0	9.47	9.47	13.68	1.05	1.75	56.49	1.75	 i = U
0.25	7.77	1.25	18.55	25.56	5.26	8.52	2.01	1.75	29.07	 j = MBP
