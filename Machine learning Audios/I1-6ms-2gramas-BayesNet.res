Timestamp:2013-08-30-04:16:18
Inventario:I1
Intervalo:6ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I1-6ms-2gramas
Num Instances:  30452
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(16): class 
0Pitch(Hz)(12): class 
0RawPitch(11): class 
0SmPitch(11): class 
0Melogram(st)(14): class 
0ZCross(13): class 
0F1(Hz)(18): class 
0F2(Hz)(19): class 
0F3(Hz)(20): class 
0F4(Hz)(17): class 
1Int(dB)(14): class 
1Pitch(Hz)(12): class 
1RawPitch(13): class 
1SmPitch(10): class 
1Melogram(st)(14): class 
1ZCross(13): class 
1F1(Hz)(22): class 
1F2(Hz)(20): class 
1F3(Hz)(18): class 
1F4(Hz)(23): class 
class(13): 
LogScore Bayes: -928984.6820233978
LogScore BDeu: -949691.5195326869
LogScore MDL: -947318.6022705544
LogScore ENTROPY: -927796.094223517
LogScore AIC: -931578.094223517




=== Stratified cross-validation ===

Correctly Classified Instances       18531               60.8531 %
Incorrectly Classified Instances     11921               39.1469 %
Kappa statistic                          0.5071
K&B Relative Info Score            1536457.1051 %
K&B Information Score                44713.2808 bits      1.4683 bits/instance
Class complexity | order 0           88598.1754 bits      2.9094 bits/instance
Class complexity | scheme           206949.8127 bits      6.7959 bits/instance
Complexity improvement     (Sf)    -118351.6373 bits     -3.8865 bits/instance
Mean absolute error                      0.0619
Root mean squared error                  0.2205
Relative absolute error                 50.2395 %
Root relative squared error             88.8256 %
Coverage of cases (0.95 level)          72.8524 %
Mean rel. region size (0.95 level)      13.6083 %
Total Number of Instances            30452     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,641    0,050    0,707      0,641    0,673      0,615    0,911     0,745     a
                 0,047    0,005    0,236      0,047    0,078      0,093    0,793     0,121     b
                 0,601    0,065    0,436      0,601    0,505      0,464    0,897     0,520     e
                 0,433    0,046    0,124      0,433    0,193      0,211    0,877     0,167     d
                 0,114    0,013    0,114      0,114    0,114      0,101    0,805     0,066     f
                 0,673    0,037    0,431      0,673    0,526      0,516    0,925     0,509     i
                 0,110    0,010    0,185      0,110    0,138      0,129    0,863     0,118     k
                 0,199    0,007    0,261      0,199    0,226      0,220    0,841     0,160     j
                 0,178    0,042    0,310      0,178    0,226      0,175    0,800     0,264     l
                 0,349    0,022    0,558      0,349    0,430      0,407    0,844     0,450     o
                 0,902    0,117    0,826      0,902    0,862      0,773    0,960     0,955     0
                 0,344    0,023    0,479      0,344    0,401      0,375    0,901     0,388     s
                 0,567    0,023    0,365      0,567    0,444      0,439    0,934     0,520     u
Weighted Avg.    0,609    0,068    0,603      0,609    0,594      0,540    0,906     0,647     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  3095     8   385   269    59    94    42    29   271   200   226    79    68 |     a = a
    66    46   108   113    40   160    24    24    81    35   150    35    98 |     b = b
   196    17  1410   111    20   166    24    38   139    42    86    35    63 |     c = e
    56    11    61   196     8    21     0     0    32    26    14     3    25 |     d = d
    27     8    60    59    49    28    11     9    32    20    86    30    11 |     e = f
    14     9   183    10    11   812    23     7    32     2    50    35    18 |     f = i
    14     7    15     0    16    28    65     6    25    10   300   103     4 |     g = k
    67     4    61    15     7    33     6    70    15     4    39    25     5 |     h = j
   272    35   458   371    75   270    51    28   520   112   407   136   192 |     i = l
   348    16   211   255    18    47    19    18   105   786   230    31   167 |     j = o
   120    17   167    87    85   127    43    20   202    94 10474   146    28 |     k = 0
    66     7    64    47    31    91    33    18   178    14   613   613     7 |     l = s
    37    10    52    46    12     6    11     1    45    64    10     8   395 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
64.15	0.17	7.98	5.58	1.22	1.95	0.87	0.6	5.62	4.15	4.68	1.64	1.41	 a = a
6.73	4.69	11.02	11.53	4.08	16.33	2.45	2.45	8.27	3.57	15.31	3.57	10.0	 b = b
8.35	0.72	60.08	4.73	0.85	7.07	1.02	1.62	5.92	1.79	3.66	1.49	2.68	 c = e
12.36	2.43	13.47	43.27	1.77	4.64	0.0	0.0	7.06	5.74	3.09	0.66	5.52	 d = d
6.28	1.86	13.95	13.72	11.4	6.51	2.56	2.09	7.44	4.65	20.0	6.98	2.56	 e = f
1.16	0.75	15.17	0.83	0.91	67.33	1.91	0.58	2.65	0.17	4.15	2.9	1.49	 f = i
2.36	1.18	2.53	0.0	2.7	4.72	10.96	1.01	4.22	1.69	50.59	17.37	0.67	 g = k
19.09	1.14	17.38	4.27	1.99	9.4	1.71	19.94	4.27	1.14	11.11	7.12	1.42	 h = j
9.29	1.2	15.65	12.68	2.56	9.22	1.74	0.96	17.77	3.83	13.91	4.65	6.56	 i = l
15.46	0.71	9.37	11.33	0.8	2.09	0.84	0.8	4.66	34.92	10.22	1.38	7.42	 j = o
1.03	0.15	1.44	0.75	0.73	1.09	0.37	0.17	1.74	0.81	90.22	1.26	0.24	 k = 0
3.7	0.39	3.59	2.64	1.74	5.11	1.85	1.01	9.99	0.79	34.4	34.4	0.39	 l = s
5.31	1.43	7.46	6.6	1.72	0.86	1.58	0.14	6.46	9.18	1.43	1.15	56.67	 m = u
