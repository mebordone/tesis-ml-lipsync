Timestamp:2013-07-22-21:19:56
Inventario:I1
Intervalo:10ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I1-10ms-3gramas
Num Instances:  18294
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3089





=== Stratified cross-validation ===

Correctly Classified Instances       13810               75.4892 %
Incorrectly Classified Instances      4484               24.5108 %
Kappa statistic                          0.6939
K&B Relative Info Score            1211685.8246 %
K&B Information Score                35538.1682 bits      1.9426 bits/instance
Class complexity | order 0           53632.982  bits      2.9317 bits/instance
Class complexity | scheme          1245505.054  bits     68.0827 bits/instance
Complexity improvement     (Sf)    -1191872.072 bits    -65.151  bits/instance
Mean absolute error                      0.0547
Root mean squared error                  0.164 
Relative absolute error                 44.0156 %
Root relative squared error             65.8249 %
Coverage of cases (0.95 level)          93.7302 %
Mean rel. region size (0.95 level)      22.2511 %
Total Number of Instances            18294     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,847    0,054    0,751      0,847    0,796      0,756    0,952     0,867     a
                 0,525    0,013    0,576      0,525    0,549      0,535    0,879     0,493     b
                 0,735    0,035    0,644      0,735    0,686      0,659    0,944     0,727     e
                 0,369    0,003    0,623      0,369    0,463      0,474    0,916     0,456     d
                 0,276    0,004    0,477      0,276    0,350      0,356    0,847     0,261     f
                 0,718    0,012    0,711      0,718    0,715      0,702    0,942     0,726     i
                 0,259    0,009    0,368      0,259    0,304      0,297    0,792     0,227     k
                 0,365    0,002    0,691      0,365    0,478      0,499    0,884     0,486     j
                 0,565    0,048    0,562      0,565    0,564      0,516    0,890     0,562     l
                 0,652    0,023    0,701      0,652    0,676      0,651    0,919     0,704     o
                 0,917    0,063    0,896      0,917    0,906      0,850    0,972     0,954     0
                 0,602    0,020    0,657      0,602    0,628      0,607    0,930     0,658     s
                 0,657    0,003    0,835      0,657    0,735      0,735    0,947     0,752     u
Weighted Avg.    0,755    0,044    0,749      0,755    0,749      0,711    0,940     0,781     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 2489   20   82    8    3   20   21    4  114   62   86   28    3 |    a = a
   50  312   34    2    6   20    8    1   57   33   52   15    4 |    b = b
   88   27 1057   11    4   48    9    8   91   37   35   21    2 |    c = e
   40    7   27  101    1    2    0    1   40   40    2    7    6 |    d = d
   35   10   28    1   72    6    3    1   36   13   13   39    4 |    e = f
   23   19   79    0    2  537    7    1   36    2   24   15    3 |    f = i
   35    9   10    2    5    7   93    1   39   13   99   42    4 |    g = k
   34    4   28    1    6    7    2   76   17    6   10   16    1 |    h = j
  213   51  131    9   13   48   38    6 1011   57  147   54   10 |    i = l
  114   24   71   15    4    5    9    1   95  898  109   21   11 |    j = o
  118   32   46    5   12   35   30    6  136   61 6231   81    3 |    k = 0
   55   15   28    6   18   18   30    4   89   24  142  655    4 |    l = s
   21   12   21    1    5    2    3    0   37   35    5    3  278 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
84.66	0.68	2.79	0.27	0.1	0.68	0.71	0.14	3.88	2.11	2.93	0.95	0.1	 a = a
8.42	52.53	5.72	0.34	1.01	3.37	1.35	0.17	9.6	5.56	8.75	2.53	0.67	 b = b
6.12	1.88	73.5	0.76	0.28	3.34	0.63	0.56	6.33	2.57	2.43	1.46	0.14	 c = e
14.6	2.55	9.85	36.86	0.36	0.73	0.0	0.36	14.6	14.6	0.73	2.55	2.19	 d = d
13.41	3.83	10.73	0.38	27.59	2.3	1.15	0.38	13.79	4.98	4.98	14.94	1.53	 e = f
3.07	2.54	10.56	0.0	0.27	71.79	0.94	0.13	4.81	0.27	3.21	2.01	0.4	 f = i
9.75	2.51	2.79	0.56	1.39	1.95	25.91	0.28	10.86	3.62	27.58	11.7	1.11	 g = k
16.35	1.92	13.46	0.48	2.88	3.37	0.96	36.54	8.17	2.88	4.81	7.69	0.48	 h = j
11.91	2.85	7.33	0.5	0.73	2.68	2.13	0.34	56.54	3.19	8.22	3.02	0.56	 i = l
8.28	1.74	5.16	1.09	0.29	0.36	0.65	0.07	6.9	65.21	7.92	1.53	0.8	 j = o
1.74	0.47	0.68	0.07	0.18	0.52	0.44	0.09	2.0	0.9	91.69	1.19	0.04	 k = 0
5.06	1.38	2.57	0.55	1.65	1.65	2.76	0.37	8.18	2.21	13.05	60.2	0.37	 l = s
4.96	2.84	4.96	0.24	1.18	0.47	0.71	0.0	8.75	8.27	1.18	0.71	65.72	 m = u
