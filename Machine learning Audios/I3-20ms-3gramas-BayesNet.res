Timestamp:2013-08-30-05:06:19
Inventario:I3
Intervalo:20ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I3-20ms-3gramas
Num Instances:  9127
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(4): class 
0F1(Hz)(8): class 
0F2(Hz)(8): class 
0F3(Hz)(3): class 
0F4(Hz)(6): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(4): class 
1F1(Hz)(9): class 
1F2(Hz)(8): class 
1F3(Hz)(5): class 
1F4(Hz)(3): class 
2Int(dB)(8): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(8): class 
2F1(Hz)(9): class 
2F2(Hz)(9): class 
2F3(Hz)(5): class 
2F4(Hz)(6): class 
class(11): 
LogScore Bayes: -271379.17637160287
LogScore BDeu: -277849.8254244112
LogScore MDL: -277500.7806205455
LogScore ENTROPY: -270232.9437315295
LogScore AIC: -271826.9437315295




=== Stratified cross-validation ===

Correctly Classified Instances        5004               54.8263 %
Incorrectly Classified Instances      4123               45.1737 %
Kappa statistic                          0.4397
K&B Relative Info Score             406623.0404 %
K&B Information Score                10859.3702 bits      1.1898 bits/instance
Class complexity | order 0           24349.2613 bits      2.6678 bits/instance
Class complexity | scheme            78487.8993 bits      8.5995 bits/instance
Complexity improvement     (Sf)     -54138.6379 bits     -5.9317 bits/instance
Mean absolute error                      0.0831
Root mean squared error                  0.2633
Relative absolute error                 57.5404 %
Root relative squared error             98.0307 %
Coverage of cases (0.95 level)          64.6215 %
Mean rel. region size (0.95 level)      14.8162 %
Total Number of Instances             9127     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,546    0,071    0,602      0,546    0,573      0,494    0,877     0,652     A
                 0,458    0,068    0,374      0,458    0,412      0,356    0,866     0,388     E
                 0,188    0,054    0,455      0,188    0,266      0,196    0,762     0,392     CGJLNQSRT
                 0,087    0,018    0,069      0,087    0,077      0,062    0,772     0,041     FV
                 0,579    0,045    0,361      0,579    0,445      0,428    0,903     0,407     I
                 0,211    0,019    0,481      0,211    0,293      0,283    0,805     0,322     O
                 0,913    0,099    0,835      0,913    0,872      0,799    0,966     0,951     0
                 0,013    0,005    0,083      0,013    0,023      0,021    0,749     0,082     BMP
                 0,440    0,018    0,374      0,440    0,404      0,390    0,899     0,424     U
                 0,200    0,012    0,009      0,200    0,017      0,040    0,917     0,011     DZ
                 0,541    0,110    0,068      0,541    0,120      0,161    0,858     0,079     D
Weighted Avg.    0,548    0,069    0,579      0,548    0,543      0,485    0,876     0,608     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
  822  135  104   28   38   66   63    4   16   12  217 |    a = A
   88  342   49    6   74   13   33    2   26    5  108 |    b = E
  214  186  330   66  158   23  344   23   36   49  322 |    c = CGJLNQSRT
   21   21   18   12    4    7   13    2    1    6   33 |    d = FV
    7   67   28    2  223    1   25    1    4    3   24 |    e = I
  112   60   48    7   23  150   68    3   55   10  176 |    f = O
   25   22  103   37   27   13 2952    7    4   20   25 |    g = 0
   33   49   30   12   59    8   27    4   16    5   56 |    h = BMP
   17   20    8    2    8   26    8    0   96    3   30 |    i = U
    0    0    1    0    0    0    3    0    0    1    0 |    j = DZ
   26   13    7    2    3    5    0    2    3    0   72 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
54.62	8.97	6.91	1.86	2.52	4.39	4.19	0.27	1.06	0.8	14.42	 a = A
11.8	45.84	6.57	0.8	9.92	1.74	4.42	0.27	3.49	0.67	14.48	 b = E
12.22	10.62	18.85	3.77	9.02	1.31	19.65	1.31	2.06	2.8	18.39	 c = CGJLNQSRT
15.22	15.22	13.04	8.7	2.9	5.07	9.42	1.45	0.72	4.35	23.91	 d = FV
1.82	17.4	7.27	0.52	57.92	0.26	6.49	0.26	1.04	0.78	6.23	 e = I
15.73	8.43	6.74	0.98	3.23	21.07	9.55	0.42	7.72	1.4	24.72	 f = O
0.77	0.68	3.18	1.14	0.83	0.4	91.25	0.22	0.12	0.62	0.77	 g = 0
11.04	16.39	10.03	4.01	19.73	2.68	9.03	1.34	5.35	1.67	18.73	 h = BMP
7.8	9.17	3.67	0.92	3.67	11.93	3.67	0.0	44.04	1.38	13.76	 i = U
0.0	0.0	20.0	0.0	0.0	0.0	60.0	0.0	0.0	20.0	0.0	 j = DZ
19.55	9.77	5.26	1.5	2.26	3.76	0.0	1.5	2.26	0.0	54.14	 k = D
