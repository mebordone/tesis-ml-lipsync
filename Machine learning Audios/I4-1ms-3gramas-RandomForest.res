Timestamp:2013-07-22-22:38:53
Inventario:I4
Intervalo:1ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I4-1ms-3gramas
Num Instances:  183402
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1217





=== Stratified cross-validation ===

Correctly Classified Instances      165579               90.282  %
Incorrectly Classified Instances     17823                9.718  %
Kappa statistic                          0.8732
K&B Relative Info Score            15718994.3426 %
K&B Information Score               399845.0463 bits      2.1802 bits/instance
Class complexity | order 0          466505.2519 bits      2.5436 bits/instance
Class complexity | scheme          7413877.1171 bits     40.4242 bits/instance
Complexity improvement     (Sf)    -6947371.8652 bits    -37.8806 bits/instance
Mean absolute error                      0.0278
Root mean squared error                  0.1218
Relative absolute error                 18.0821 %
Root relative squared error             43.8954 %
Coverage of cases (0.95 level)          96.0649 %
Mean rel. region size (0.95 level)      15.7035 %
Total Number of Instances           183402     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,903    0,000    0,928      0,903    0,915      0,915    0,970     0,929     C
                 0,940    0,005    0,937      0,940    0,939      0,934    0,982     0,961     E
                 0,730    0,003    0,780      0,730    0,754      0,751    0,919     0,764     FV
                 0,940    0,014    0,941      0,940    0,941      0,926    0,980     0,966     AI
                 0,852    0,034    0,819      0,852    0,835      0,805    0,964     0,895     CDGKNRSYZ
                 0,875    0,006    0,921      0,875    0,898      0,890    0,957     0,910     O
                 0,938    0,052    0,921      0,938    0,929      0,883    0,981     0,964     0
                 0,653    0,008    0,761      0,653    0,703      0,695    0,902     0,717     LT
                 0,957    0,000    0,983      0,957    0,970      0,970    0,992     0,981     U
                 0,787    0,004    0,867      0,787    0,825      0,821    0,937     0,838     MBP
Weighted Avg.    0,903    0,030    0,902      0,903    0,902      0,875    0,972     0,934     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   978    16     3    19    29     4    24     7     2     1 |     a = C
     7 12922    18   100   311    31   245    54     3    49 |     b = E
     2    36  1870    63   408    22    95    34     0    31 |     c = FV
     9   112    45 33508   696    82   923   162     8    95 |     d = AI
    24   277   269   615 24237   225  2185   427    18   182 |     e = CDGKNRSYZ
     4    41    27   119   376 11582   973    66     3    43 |     f = O
    24   221   104   739  2189   475 67619   514    10   220 |     g = 0
     3    79    29   243   868    93   909  4325     5    65 |     h = LT
     1    10     1    36    49    12    41    12  3961    14 |     i = U
     2    70    31   173   413    43   402    85    18  4577 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
90.3	1.48	0.28	1.75	2.68	0.37	2.22	0.65	0.18	0.09	 a = C
0.05	94.05	0.13	0.73	2.26	0.23	1.78	0.39	0.02	0.36	 b = E
0.08	1.41	73.02	2.46	15.93	0.86	3.71	1.33	0.0	1.21	 c = FV
0.03	0.31	0.13	94.02	1.95	0.23	2.59	0.45	0.02	0.27	 d = AI
0.08	0.97	0.95	2.16	85.16	0.79	7.68	1.5	0.06	0.64	 e = CDGKNRSYZ
0.03	0.31	0.2	0.9	2.84	87.52	7.35	0.5	0.02	0.32	 f = O
0.03	0.31	0.14	1.02	3.04	0.66	93.77	0.71	0.01	0.31	 g = 0
0.05	1.19	0.44	3.67	13.11	1.41	13.73	65.34	0.08	0.98	 h = LT
0.02	0.24	0.02	0.87	1.18	0.29	0.99	0.29	95.75	0.34	 i = U
0.03	1.2	0.53	2.98	7.1	0.74	6.91	1.46	0.31	78.72	 j = MBP
