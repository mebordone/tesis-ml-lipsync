Timestamp:2013-08-30-05:07:26
Inventario:I4
Intervalo:1ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I4-1ms-2gramas
Num Instances:  183403
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(33): class 
0Pitch(Hz)(97): class 
0RawPitch(72): class 
0SmPitch(95): class 
0Melogram(st)(91): class 
0ZCross(20): class 
0F1(Hz)(2635): class 
0F2(Hz)(3341): class 
0F3(Hz)(3378): class 
0F4(Hz)(3371): class 
1Int(dB)(33): class 
1Pitch(Hz)(93): class 
1RawPitch(72): class 
1SmPitch(93): class 
1Melogram(st)(91): class 
1ZCross(20): class 
1F1(Hz)(2623): class 
1F2(Hz)(3318): class 
1F3(Hz)(3383): class 
1F4(Hz)(3362): class 
class(10): 
LogScore Bayes: -1.1373916795539657E7
LogScore BDeu: -1.4732962177018298E7
LogScore MDL: -1.3901230690346664E7
LogScore ENTROPY: -1.2313468758932298E7
LogScore AIC: -1.2575487758932298E7




=== Stratified cross-validation ===

Correctly Classified Instances      148338               80.8809 %
Incorrectly Classified Instances     35065               19.1191 %
Kappa statistic                          0.7477
K&B Relative Info Score            13995434.7028 %
K&B Information Score               356003.7914 bits      1.9411 bits/instance
Class complexity | order 0          466506.5985 bits      2.5436 bits/instance
Class complexity | scheme           883342.371  bits      4.8164 bits/instance
Complexity improvement     (Sf)    -416835.7725 bits     -2.2728 bits/instance
Mean absolute error                      0.0385
Root mean squared error                  0.1898
Relative absolute error                 25.0314 %
Root relative squared error             68.4179 %
Coverage of cases (0.95 level)          82.9937 %
Mean rel. region size (0.95 level)      10.8226 %
Total Number of Instances           183403     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,885    0,015    0,261      0,885    0,403      0,476    0,956     0,828     C
                 0,881    0,013    0,842      0,881    0,861      0,850    0,970     0,901     E
                 0,676    0,009    0,526      0,676    0,592      0,590    0,926     0,662     FV
                 0,857    0,015    0,933      0,857    0,893      0,870    0,966     0,937     AI
                 0,508    0,019    0,831      0,508    0,631      0,604    0,961     0,826     CDGKNRSYZ
                 0,828    0,009    0,872      0,828    0,849      0,838    0,933     0,865     O
                 0,911    0,160    0,786      0,911    0,844      0,736    0,972     0,963     0
                 0,560    0,008    0,715      0,560    0,628      0,621    0,932     0,629     LT
                 0,938    0,007    0,757      0,938    0,838      0,839    0,982     0,934     U
                 0,737    0,007    0,781      0,737    0,758      0,751    0,925     0,763     MBP
Weighted Avg.    0,809    0,071    0,822      0,809    0,805      0,753    0,963     0,901     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   958    18     0    18    10     1    77     0     1     0 |     a = C
   173 12104   115    83   362    31   571   124    53   124 |     b = E
    18    37  1731    27    54    14   661    12     5     2 |     c = FV
   746   259   307 30551   734   195  2005   429   116   298 |     d = AI
   885   956   117   786 14469   608  9820   211   434   173 |     e = CDGKNRSYZ
   223    27    51    77   178 10952  1476   104    20   126 |     f = O
   519   854   927   998   991   626 65698   534   540   429 |     g = 0
    70    91    18   123   406    72  2067  3708    29    35 |     h = LT
    26     0    12     8    80     1    77    35  3881    17 |     i = U
    54    28    11    79   132    61  1085    31    47  4286 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
88.46	1.66	0.0	1.66	0.92	0.09	7.11	0.0	0.09	0.0	 a = C
1.26	88.09	0.84	0.6	2.63	0.23	4.16	0.9	0.39	0.9	 b = E
0.7	1.44	67.59	1.05	2.11	0.55	25.81	0.47	0.2	0.08	 c = FV
2.09	0.73	0.86	85.72	2.06	0.55	5.63	1.2	0.33	0.84	 d = AI
3.11	3.36	0.41	2.76	50.84	2.14	34.51	0.74	1.53	0.61	 e = CDGKNRSYZ
1.69	0.2	0.39	0.58	1.35	82.76	11.15	0.79	0.15	0.95	 f = O
0.72	1.18	1.29	1.38	1.37	0.87	91.1	0.74	0.75	0.59	 g = 0
1.06	1.37	0.27	1.86	6.13	1.09	31.23	56.02	0.44	0.53	 h = LT
0.63	0.0	0.29	0.19	1.93	0.02	1.86	0.85	93.81	0.41	 i = U
0.93	0.48	0.19	1.36	2.27	1.05	18.66	0.53	0.81	73.72	 j = MBP
