Timestamp:2013-08-30-04:18:33
Inventario:I1
Intervalo:15ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I1-15ms-4gramas
Num Instances:  12186
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(5): class 
0F1(Hz)(9): class 
0F2(Hz)(8): class 
0F3(Hz)(3): class 
0F4(Hz)(7): class 
1Int(dB)(9): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(5): class 
1F1(Hz)(9): class 
1F2(Hz)(8): class 
1F3(Hz)(7): class 
1F4(Hz)(6): class 
2Int(dB)(9): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(8): class 
2F1(Hz)(9): class 
2F2(Hz)(9): class 
2F3(Hz)(7): class 
2F4(Hz)(6): class 
3Int(dB)(9): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(9): class 
3F1(Hz)(11): class 
3F2(Hz)(10): class 
3F3(Hz)(7): class 
3F4(Hz)(7): class 
class(13): 
LogScore Bayes: -504694.24836527265
LogScore BDeu: -517173.89276472735
LogScore MDL: -515884.18881992163
LogScore ENTROPY: -502435.3913073809
LogScore AIC: -505294.39130738104




=== Stratified cross-validation ===

Correctly Classified Instances        6572               53.9307 %
Incorrectly Classified Instances      5614               46.0693 %
Kappa statistic                          0.4404
K&B Relative Info Score             515586.8417 %
K&B Information Score                15221.0442 bits      1.2491 bits/instance
Class complexity | order 0           35954.2498 bits      2.9505 bits/instance
Class complexity | scheme           142987.8761 bits     11.7338 bits/instance
Complexity improvement     (Sf)    -107033.6263 bits     -8.7833 bits/instance
Mean absolute error                      0.0714
Root mean squared error                  0.2475
Relative absolute error                 57.1483 %
Root relative squared error             99.0512 %
Coverage of cases (0.95 level)          62.6867 %
Mean rel. region size (0.95 level)      11.8295 %
Total Number of Instances            12186     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,532    0,058    0,642      0,532    0,582      0,512    0,877     0,665     a
                 0,025    0,004    0,161      0,025    0,043      0,052    0,765     0,094     b
                 0,424    0,053    0,410      0,424    0,417      0,366    0,861     0,392     e
                 0,549    0,107    0,072      0,549    0,127      0,169    0,853     0,101     d
                 0,085    0,017    0,068      0,085    0,075      0,060    0,775     0,044     f
                 0,604    0,042    0,385      0,604    0,470      0,455    0,903     0,403     i
                 0,312    0,053    0,105      0,312    0,157      0,153    0,828     0,086     k
                 0,232    0,024    0,100      0,232    0,139      0,137    0,842     0,067     j
                 0,061    0,018    0,274      0,061    0,100      0,088    0,781     0,236     l
                 0,221    0,017    0,520      0,221    0,310      0,306    0,813     0,363     o
                 0,896    0,070    0,880      0,896    0,888      0,823    0,964     0,956     0
                 0,263    0,032    0,345      0,263    0,299      0,263    0,860     0,310     s
                 0,477    0,015    0,429      0,477    0,452      0,439    0,904     0,469     u
Weighted Avg.    0,539    0,049    0,581      0,539    0,541      0,498    0,887     0,592     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1058    6  158  280   48   51  112   35   32   85   62   42   20 |    a = a
   36   10   48   67   17   71   31   28   16    6   22   26   21 |    b = b
   95    6  412  133    9   92   52   61   18   13   34   19   27 |    c = e
   27    2   11  100    2    7    3    4   10    3    3    2    8 |    d = d
   16    0   22   40   15    3   17   11    9   10   11   21    2 |    e = f
    3    6   58   31    2  304   41   20    4    0   17   11    6 |    f = i
    3    2    3    5    7   12   74    7    6    3   90   25    0 |    g = k
   24    2   28   10    3   10    7   32    5    1    2   13    1 |    h = j
  159    9  117  349   35  117   86   54   74   22   79   87   28 |    i = l
  141    4   68  217    6   39   61   16   27  208   76   19   58 |    j = o
   37    8   33   45   49   38   87   12   31   15 3956   98    6 |    k = 0
   30    7   25   73   28   42  111   39   37    4  141  193    4 |    l = s
   20    0   22   38    1    4   23    2    1   30    5    3  136 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
53.19	0.3	7.94	14.08	2.41	2.56	5.63	1.76	1.61	4.27	3.12	2.11	1.01	 a = a
9.02	2.51	12.03	16.79	4.26	17.79	7.77	7.02	4.01	1.5	5.51	6.52	5.26	 b = b
9.78	0.62	42.43	13.7	0.93	9.47	5.36	6.28	1.85	1.34	3.5	1.96	2.78	 c = e
14.84	1.1	6.04	54.95	1.1	3.85	1.65	2.2	5.49	1.65	1.65	1.1	4.4	 d = d
9.04	0.0	12.43	22.6	8.47	1.69	9.6	6.21	5.08	5.65	6.21	11.86	1.13	 e = f
0.6	1.19	11.53	6.16	0.4	60.44	8.15	3.98	0.8	0.0	3.38	2.19	1.19	 f = i
1.27	0.84	1.27	2.11	2.95	5.06	31.22	2.95	2.53	1.27	37.97	10.55	0.0	 g = k
17.39	1.45	20.29	7.25	2.17	7.25	5.07	23.19	3.62	0.72	1.45	9.42	0.72	 h = j
13.08	0.74	9.62	28.7	2.88	9.62	7.07	4.44	6.09	1.81	6.5	7.15	2.3	 i = l
15.0	0.43	7.23	23.09	0.64	4.15	6.49	1.7	2.87	22.13	8.09	2.02	6.17	 j = o
0.84	0.18	0.75	1.02	1.11	0.86	1.97	0.27	0.7	0.34	89.6	2.22	0.14	 k = 0
4.09	0.95	3.41	9.95	3.81	5.72	15.12	5.31	5.04	0.54	19.21	26.29	0.54	 l = s
7.02	0.0	7.72	13.33	0.35	1.4	8.07	0.7	0.35	10.53	1.75	1.05	47.72	 m = u
