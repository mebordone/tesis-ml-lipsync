Timestamp:2013-08-30-05:09:05
Inventario:I4
Intervalo:1ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I4-1ms-3gramas
Num Instances:  183402
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(33): class 
0Pitch(Hz)(95): class 
0RawPitch(74): class 
0SmPitch(95): class 
0Melogram(st)(92): class 
0ZCross(20): class 
0F1(Hz)(2627): class 
0F2(Hz)(3334): class 
0F3(Hz)(3371): class 
0F4(Hz)(3370): class 
1Int(dB)(33): class 
1Pitch(Hz)(97): class 
1RawPitch(72): class 
1SmPitch(95): class 
1Melogram(st)(91): class 
1ZCross(20): class 
1F1(Hz)(2635): class 
1F2(Hz)(3341): class 
1F3(Hz)(3378): class 
1F4(Hz)(3371): class 
2Int(dB)(33): class 
2Pitch(Hz)(93): class 
2RawPitch(72): class 
2SmPitch(93): class 
2Melogram(st)(91): class 
2ZCross(20): class 
2F1(Hz)(2623): class 
2F2(Hz)(3318): class 
2F3(Hz)(3383): class 
2F4(Hz)(3362): class 
class(10): 
LogScore Bayes: -1.69093340253665E7
LogScore BDeu: -2.194774759545408E7
LogScore MDL: -2.0700220602802698E7
LogScore ENTROPY: -1.8318575747313872E7
LogScore AIC: -1.8711604747313872E7




=== Stratified cross-validation ===

Correctly Classified Instances      148454               80.9446 %
Incorrectly Classified Instances     34948               19.0554 %
Kappa statistic                          0.7488
K&B Relative Info Score            14013477.6238 %
K&B Information Score               356461.7104 bits      1.9436 bits/instance
Class complexity | order 0          466505.2519 bits      2.5436 bits/instance
Class complexity | scheme          1297481.5113 bits      7.0745 bits/instance
Complexity improvement     (Sf)    -830976.2593 bits     -4.5309 bits/instance
Mean absolute error                      0.0382
Root mean squared error                  0.1912
Relative absolute error                 24.8458 %
Root relative squared error             68.9067 %
Coverage of cases (0.95 level)          82.413  %
Mean rel. region size (0.95 level)      10.5372 %
Total Number of Instances           183402     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,894    0,017    0,237      0,894    0,375      0,455    0,957     0,821     C
                 0,883    0,013    0,851      0,883    0,866      0,856    0,970     0,904     E
                 0,691    0,009    0,511      0,691    0,587      0,588    0,928     0,664     FV
                 0,855    0,014    0,937      0,855    0,894      0,872    0,966     0,938     AI
                 0,509    0,018    0,841      0,509    0,635      0,610    0,963     0,831     CDGKNRSYZ
                 0,831    0,009    0,879      0,831    0,854      0,844    0,934     0,867     O
                 0,910    0,159    0,787      0,910    0,844      0,736    0,972     0,963     0
                 0,569    0,009    0,707      0,569    0,631      0,622    0,934     0,634     LT
                 0,937    0,007    0,762      0,937    0,840      0,841    0,982     0,932     U
                 0,746    0,007    0,781      0,746    0,763      0,756    0,927     0,766     MBP
Weighted Avg.    0,809    0,071    0,825      0,809    0,807      0,755    0,963     0,902     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   968    14     0    13    11     0    76     0     1     0 |     a = C
   212 12131   116    70   337    25   569   124    45   111 |     b = E
    17    24  1769    20    53    10   652    12     4     0 |     c = FV
   855   206   356 30489   681   184  1994   469    90   316 |     d = AI
  1054   926    98   760 14495   560  9715   212   457   182 |     e = CDGKNRSYZ
   239    16    48    62   160 11000  1465   110    18   116 |     f = O
   586   830  1038   953   919   620 65618   567   535   449 |     g = 0
    81    81    16   102   376    63  2078  3769    25    28 |     h = LT
    23     5    12     8    78     0    78    39  3878    16 |     i = U
    47    28     9    61   117    54  1093    30    38  4337 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
89.38	1.29	0.0	1.2	1.02	0.0	7.02	0.0	0.09	0.0	 a = C
1.54	88.29	0.84	0.51	2.45	0.18	4.14	0.9	0.33	0.81	 b = E
0.66	0.94	69.07	0.78	2.07	0.39	25.46	0.47	0.16	0.0	 c = FV
2.4	0.58	1.0	85.55	1.91	0.52	5.59	1.32	0.25	0.89	 d = AI
3.7	3.25	0.34	2.67	50.93	1.97	34.14	0.74	1.61	0.64	 e = CDGKNRSYZ
1.81	0.12	0.36	0.47	1.21	83.12	11.07	0.83	0.14	0.88	 f = O
0.81	1.15	1.44	1.32	1.27	0.86	90.99	0.79	0.74	0.62	 g = 0
1.22	1.22	0.24	1.54	5.68	0.95	31.39	56.94	0.38	0.42	 h = LT
0.56	0.12	0.29	0.19	1.89	0.0	1.89	0.94	93.74	0.39	 i = U
0.81	0.48	0.15	1.05	2.01	0.93	18.8	0.52	0.65	74.6	 j = MBP
