Timestamp:2013-07-22-21:19:31
Inventario:I1
Intervalo:10ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I1-10ms-2gramas
Num Instances:  18295
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3148





=== Stratified cross-validation ===

Correctly Classified Instances       13500               73.7907 %
Incorrectly Classified Instances      4795               26.2093 %
Kappa statistic                          0.6735
K&B Relative Info Score            1196814.7068 %
K&B Information Score                35100.8695 bits      1.9186 bits/instance
Class complexity | order 0           53634.4104 bits      2.9316 bits/instance
Class complexity | scheme          1507042.103  bits     82.3745 bits/instance
Complexity improvement     (Sf)    -1453407.6926 bits    -79.4429 bits/instance
Mean absolute error                      0.0548
Root mean squared error                  0.1682
Relative absolute error                 44.1511 %
Root relative squared error             67.4996 %
Coverage of cases (0.95 level)          92.375  %
Mean rel. region size (0.95 level)      21.1878 %
Total Number of Instances            18295     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,828    0,057    0,737      0,828    0,780      0,737    0,947     0,854     a
                 0,446    0,014    0,518      0,446    0,479      0,464    0,852     0,413     b
                 0,736    0,035    0,644      0,736    0,687      0,660    0,937     0,718     e
                 0,347    0,004    0,562      0,347    0,429      0,435    0,889     0,395     d
                 0,264    0,005    0,445      0,264    0,332      0,336    0,835     0,243     f
                 0,703    0,014    0,677      0,703    0,690      0,676    0,935     0,701     i
                 0,220    0,011    0,293      0,220    0,251      0,241    0,778     0,166     k
                 0,317    0,002    0,647      0,317    0,426      0,449    0,873     0,413     j
                 0,556    0,052    0,536      0,556    0,546      0,496    0,868     0,517     l
                 0,643    0,025    0,673      0,643    0,658      0,631    0,917     0,684     o
                 0,899    0,063    0,894      0,899    0,897      0,835    0,968     0,946     0
                 0,584    0,022    0,623      0,584    0,602      0,579    0,919     0,613     s
                 0,664    0,003    0,824      0,664    0,736      0,734    0,948     0,755     u
Weighted Avg.    0,738    0,045    0,733      0,738    0,733      0,692    0,932     0,760     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 2435   22   93    8    4   26   17    3  134   67   91   36    4 |    a = a
   65  265   32    6    8   37    7    1   69   30   52   17    5 |    b = b
  101   29 1058   11    8   35   11    4   98   26   36   16    5 |    c = e
   39    6   30   95    3    2    1    1   40   43    4    6    4 |    d = d
   30    9   22    2   69    5    3    4   46   19   18   33    1 |    e = f
   18   23   80    0    4  526    6    0   40    9   24   14    4 |    f = i
   21   11   17    0    2   10   79    3   42   25   86   62    1 |    g = k
   42    4   29    2    3   13    6   66   13    4    9   15    2 |    h = j
  237   43  125   15   14   54   34    6  994   59  140   58    9 |    i = l
  114   32   50   17    4    9   13    5  107  886  110   17   13 |    j = o
  121   38   60    6   17   35   54    3  156   84 6111  106    6 |    k = 0
   67   15   28    3   19   21   37    2   82   27  146  635    6 |    l = s
   14   15   19    4    0    4    2    4   33   37    5    5  281 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
82.82	0.75	3.16	0.27	0.14	0.88	0.58	0.1	4.56	2.28	3.1	1.22	0.14	 a = a
10.94	44.61	5.39	1.01	1.35	6.23	1.18	0.17	11.62	5.05	8.75	2.86	0.84	 b = b
7.02	2.02	73.57	0.76	0.56	2.43	0.76	0.28	6.82	1.81	2.5	1.11	0.35	 c = e
14.23	2.19	10.95	34.67	1.09	0.73	0.36	0.36	14.6	15.69	1.46	2.19	1.46	 d = d
11.49	3.45	8.43	0.77	26.44	1.92	1.15	1.53	17.62	7.28	6.9	12.64	0.38	 e = f
2.41	3.07	10.7	0.0	0.53	70.32	0.8	0.0	5.35	1.2	3.21	1.87	0.53	 f = i
5.85	3.06	4.74	0.0	0.56	2.79	22.01	0.84	11.7	6.96	23.96	17.27	0.28	 g = k
20.19	1.92	13.94	0.96	1.44	6.25	2.88	31.73	6.25	1.92	4.33	7.21	0.96	 h = j
13.26	2.4	6.99	0.84	0.78	3.02	1.9	0.34	55.59	3.3	7.83	3.24	0.5	 i = l
8.28	2.32	3.63	1.23	0.29	0.65	0.94	0.36	7.77	64.34	7.99	1.23	0.94	 j = o
1.78	0.56	0.88	0.09	0.25	0.51	0.79	0.04	2.3	1.24	89.91	1.56	0.09	 k = 0
6.16	1.38	2.57	0.28	1.75	1.93	3.4	0.18	7.54	2.48	13.42	58.36	0.55	 l = s
3.31	3.55	4.49	0.95	0.0	0.95	0.47	0.95	7.8	8.75	1.18	1.18	66.43	 m = u
