Timestamp:2013-08-30-04:08:02
Inventario:I1
Intervalo:2ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I1-2ms-2gramas
Num Instances:  91702
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(22): class 
0Pitch(Hz)(46): class 
0RawPitch(29): class 
0SmPitch(36): class 
0Melogram(st)(45): class 
0ZCross(16): class 
0F1(Hz)(1612): class 
0F2(Hz)(2243): class 
0F3(Hz)(2379): class 
0F4(Hz)(2445): class 
1Int(dB)(25): class 
1Pitch(Hz)(45): class 
1RawPitch(31): class 
1SmPitch(39): class 
1Melogram(st)(44): class 
1ZCross(17): class 
1F1(Hz)(1439): class 
1F2(Hz)(2328): class 
1F3(Hz)(2303): class 
1F4(Hz)(2522): class 
class(13): 
LogScore Bayes: -5092670.058956352
LogScore BDeu: -7973357.683438968
LogScore MDL: -7208393.971186233
LogScore ENTROPY: -5897740.290679753
LogScore AIC: -6127150.290679752




=== Stratified cross-validation ===

Correctly Classified Instances       72533               79.0964 %
Incorrectly Classified Instances     19169               20.9036 %
Kappa statistic                          0.7336
K&B Relative Info Score            6758961.0125 %
K&B Information Score               195111.7707 bits      2.1277 bits/instance
Class complexity | order 0          264697.5104 bits      2.8865 bits/instance
Class complexity | scheme           472077.2191 bits      5.1479 bits/instance
Complexity improvement     (Sf)    -207379.7087 bits     -2.2615 bits/instance
Mean absolute error                      0.033 
Root mean squared error                  0.1694
Relative absolute error                 26.9669 %
Root relative squared error             68.4695 %
Coverage of cases (0.95 level)          82.9415 %
Mean rel. region size (0.95 level)       9.2521 %
Total Number of Instances            91702     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,819    0,017    0,898      0,819    0,856      0,833    0,959     0,912     a
                 0,649    0,007    0,748      0,649    0,695      0,688    0,901     0,698     b
                 0,821    0,020    0,770      0,821    0,795      0,778    0,961     0,855     e
                 0,826    0,015    0,455      0,826    0,587      0,606    0,969     0,753     d
                 0,583    0,007    0,553      0,583    0,568      0,562    0,902     0,586     f
                 0,846    0,011    0,755      0,846    0,798      0,791    0,960     0,849     i
                 0,187    0,006    0,396      0,187    0,254      0,262    0,919     0,287     k
                 0,702    0,006    0,572      0,702    0,631      0,629    0,914     0,692     j
                 0,622    0,019    0,774      0,622    0,690      0,666    0,907     0,733     l
                 0,753    0,011    0,838      0,753    0,793      0,779    0,924     0,822     o
                 0,904    0,128    0,819      0,904    0,860      0,765    0,968     0,959     0
                 0,495    0,014    0,678      0,495    0,572      0,558    0,959     0,653     s
                 0,898    0,007    0,758      0,898    0,822      0,821    0,979     0,908     u
Weighted Avg.    0,791    0,059    0,791      0,791    0,786      0,739    0,952     0,857     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 11732   115   288   239   108   111   108    97   365   194   738   170    68 |     a = a
    57  1891    75    33    12    80    20    10    56    54   516    52    59 |     b = b
   113    81  5674   130    49    87    47    50   246    33   278    68    54 |     c = e
    42     3    44  1112     9    12     6     5    28    28    44     7     6 |     d = d
    32    12    50    19   750    27    14     3    17    12   313    33     4 |     e = f
    14    31    71    27    20  3012    41    15    74    19   169    56    12 |     f = i
    16     8    47     7     9    19   332    16    44    31   903   320    28 |     g = k
    16     3    41    10     2    12    14   727     7     3   138    53     9 |     h = j
   369    84   440   162    18   176    59    58  5402   168  1335   275   134 |     i = l
   169    67    93   220    20    39    43    49   132  5005   728    22    59 |     j = o
   399   187   465   377   290   299   117   194   456   357 32407   167   117 |     k = 0
   100    25    66    89    61   116    23    34    98    48  1972  2621    45 |     l = s
     5    20    11    19     8     0    15    13    50    20    31    20  1868 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
81.85	0.8	2.01	1.67	0.75	0.77	0.75	0.68	2.55	1.35	5.15	1.19	0.47	 a = a
1.96	64.87	2.57	1.13	0.41	2.74	0.69	0.34	1.92	1.85	17.7	1.78	2.02	 b = b
1.64	1.17	82.11	1.88	0.71	1.26	0.68	0.72	3.56	0.48	4.02	0.98	0.78	 c = e
3.12	0.22	3.27	82.62	0.67	0.89	0.45	0.37	2.08	2.08	3.27	0.52	0.45	 d = d
2.49	0.93	3.89	1.48	58.32	2.1	1.09	0.23	1.32	0.93	24.34	2.57	0.31	 e = f
0.39	0.87	1.99	0.76	0.56	84.58	1.15	0.42	2.08	0.53	4.75	1.57	0.34	 f = i
0.9	0.45	2.64	0.39	0.51	1.07	18.65	0.9	2.47	1.74	50.73	17.98	1.57	 g = k
1.55	0.29	3.96	0.97	0.19	1.16	1.35	70.24	0.68	0.29	13.33	5.12	0.87	 h = j
4.25	0.97	5.07	1.87	0.21	2.03	0.68	0.67	62.24	1.94	15.38	3.17	1.54	 i = l
2.54	1.01	1.4	3.31	0.3	0.59	0.65	0.74	1.99	75.31	10.95	0.33	0.89	 j = o
1.11	0.52	1.3	1.05	0.81	0.83	0.33	0.54	1.27	1.0	90.44	0.47	0.33	 k = 0
1.89	0.47	1.25	1.68	1.15	2.19	0.43	0.64	1.85	0.91	37.22	49.47	0.85	 l = s
0.24	0.96	0.53	0.91	0.38	0.0	0.72	0.63	2.4	0.96	1.49	0.96	89.81	 m = u
