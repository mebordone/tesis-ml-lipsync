Timestamp:2013-08-30-04:51:28
Inventario:I3
Intervalo:1ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I3-1ms-5gramas
Num Instances:  183400
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  49 4F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(35): class 
0Pitch(Hz)(99): class 
0RawPitch(75): class 
0SmPitch(78): class 
0Melogram(st)(86): class 
0ZCross(20): class 
0F1(Hz)(2632): class 
0F2(Hz)(3356): class 
0F3(Hz)(3509): class 
0F4(Hz)(3459): class 
1Int(dB)(34): class 
1Pitch(Hz)(92): class 
1RawPitch(78): class 
1SmPitch(82): class 
1Melogram(st)(83): class 
1ZCross(20): class 
1F1(Hz)(2622): class 
1F2(Hz)(3346): class 
1F3(Hz)(3507): class 
1F4(Hz)(3456): class 
2Int(dB)(37): class 
2Pitch(Hz)(93): class 
2RawPitch(76): class 
2SmPitch(80): class 
2Melogram(st)(82): class 
2ZCross(20): class 
2F1(Hz)(2627): class 
2F2(Hz)(3312): class 
2F3(Hz)(3520): class 
2F4(Hz)(3447): class 
3Int(dB)(35): class 
3Pitch(Hz)(96): class 
3RawPitch(78): class 
3SmPitch(93): class 
3Melogram(st)(83): class 
3ZCross(20): class 
3F1(Hz)(2607): class 
3F2(Hz)(3303): class 
3F3(Hz)(3508): class 
3F4(Hz)(3434): class 
4Int(dB)(32): class 
4Pitch(Hz)(96): class 
4RawPitch(78): class 
4SmPitch(90): class 
4Melogram(st)(87): class 
4ZCross(20): class 
4F1(Hz)(2598): class 
4F2(Hz)(3309): class 
4F3(Hz)(3521): class 
4F4(Hz)(3448): class 
class(11): 
LogScore Bayes: -2.7652386929451317E7
LogScore BDeu: -3.7114292462500244E7
LogScore MDL: -3.473674301755095E7
LogScore ENTROPY: -3.030740228430131E7
LogScore AIC: -3.1038351284301315E7




=== Stratified cross-validation ===

Correctly Classified Instances      148409               80.9209 %
Incorrectly Classified Instances     34991               19.0791 %
Kappa statistic                          0.7513
K&B Relative Info Score            14282919.0846 %
K&B Information Score               369815.3117 bits      2.0164 bits/instance
Class complexity | order 0          474840.8764 bits      2.5891 bits/instance
Class complexity | scheme          2104531.6806 bits     11.4751 bits/instance
Complexity improvement     (Sf)    -1629690.8043 bits     -8.886  bits/instance
Mean absolute error                      0.0348
Root mean squared error                  0.1838
Relative absolute error                 24.71   %
Root relative squared error             69.3256 %
Coverage of cases (0.95 level)          81.8675 %
Mean rel. region size (0.95 level)       9.3868 %
Total Number of Instances           183400     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,873    0,011    0,938      0,873    0,904      0,888    0,967     0,937     A
                 0,887    0,011    0,866      0,887    0,876      0,866    0,970     0,900     E
                 0,500    0,016    0,874      0,500    0,636      0,611    0,964     0,850     CGJLNQSRT
                 0,695    0,008    0,550      0,695    0,614      0,613    0,934     0,660     FV
                 0,879    0,008    0,810      0,879    0,843      0,837    0,965     0,872     I
                 0,824    0,008    0,889      0,824    0,855      0,845    0,937     0,868     O
                 0,901    0,157    0,788      0,901    0,840      0,730    0,965     0,935     0
                 0,743    0,006    0,795      0,743    0,768      0,761    0,931     0,767     BMP
                 0,939    0,007    0,753      0,939    0,835      0,837    0,982     0,918     U
                 0,000    0,010    0,000      0,000    0,000      -0,002   0,825     0,001     DZ
                 0,959    0,017    0,452      0,959    0,615      0,652    0,990     0,829     D
Weighted Avg.    0,809    0,069    0,832      0,809    0,808      0,756    0,963     0,898     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 24917   107   463   207    99   171  1575   157    65   401   395 |     a = A
    50 12187   333   104    42    21   550   107    42   142   162 |     b = E
   859   941 16727   129   544   526 11628   240   715   118  1052 |     c = CGJLNQSRT
    12    23    62  1781    28     7   644     0     3     0     1 |     d = FV
     9    19   214    45  6223     6   382    66     0    98    21 |     e = I
    45     9   179    37    26 10910  1450   113     8   151   306 |     f = O
   623   746   908   921   628   567 64953   416   398   862  1091 |     g = 0
    41    26   138     5    88    55  1088  4319    38    14     2 |     h = BMP
     3     6   113     7     1     2    81    11  3883    22     8 |     i = U
     0     0     0     0     0     0    65     0     0     0     0 |     j = DZ
     9    13     9     1     0     9    34     1     7    25  2509 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
87.25	0.37	1.62	0.72	0.35	0.6	5.52	0.55	0.23	1.4	1.38	 a = A
0.36	88.7	2.42	0.76	0.31	0.15	4.0	0.78	0.31	1.03	1.18	 b = E
2.57	2.81	49.96	0.39	1.62	1.57	34.73	0.72	2.14	0.35	3.14	 c = CGJLNQSRT
0.47	0.9	2.42	69.54	1.09	0.27	25.15	0.0	0.12	0.0	0.04	 d = FV
0.13	0.27	3.02	0.64	87.86	0.08	5.39	0.93	0.0	1.38	0.3	 e = I
0.34	0.07	1.35	0.28	0.2	82.44	10.96	0.85	0.06	1.14	2.31	 f = O
0.86	1.03	1.26	1.28	0.87	0.79	90.07	0.58	0.55	1.2	1.51	 g = 0
0.71	0.45	2.37	0.09	1.51	0.95	18.71	74.29	0.65	0.24	0.03	 h = BMP
0.07	0.15	2.73	0.17	0.02	0.05	1.96	0.27	93.86	0.53	0.19	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
0.34	0.5	0.34	0.04	0.0	0.34	1.3	0.04	0.27	0.96	95.87	 k = D
