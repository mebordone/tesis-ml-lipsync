Timestamp:2013-07-22-22:31:23
Inventario:I3
Intervalo:15ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I3-15ms-1gramas
Num Instances:  12189
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.3086





=== Stratified cross-validation ===

Correctly Classified Instances        8841               72.5326 %
Incorrectly Classified Instances      3348               27.4674 %
Kappa statistic                          0.6485
K&B Relative Info Score             760075.8328 %
K&B Information Score                20153.5878 bits      1.6534 bits/instance
Class complexity | order 0           32291.476  bits      2.6492 bits/instance
Class complexity | scheme          1010880.4679 bits     82.9338 bits/instance
Complexity improvement     (Sf)    -978588.992  bits    -80.2846 bits/instance
Mean absolute error                      0.0654
Root mean squared error                  0.1867
Relative absolute error                 45.5799 %
Root relative squared error             69.6964 %
Coverage of cases (0.95 level)          92.1158 %
Mean rel. region size (0.95 level)      23.4481 %
Total Number of Instances            12189     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,779    0,055    0,736      0,779    0,757      0,708    0,926     0,819     A
                 0,640    0,036    0,603      0,640    0,621      0,587    0,913     0,613     E
                 0,641    0,124    0,550      0,641    0,592      0,489    0,871     0,610     CGJLNQSRT
                 0,107    0,004    0,302      0,107    0,158      0,173    0,733     0,119     FV
                 0,583    0,014    0,638      0,583    0,609      0,594    0,917     0,611     I
                 0,563    0,027    0,635      0,563    0,597      0,566    0,878     0,605     O
                 0,912    0,058    0,899      0,912    0,905      0,851    0,972     0,955     0
                 0,286    0,010    0,483      0,286    0,359      0,356    0,804     0,317     BMP
                 0,572    0,003    0,815      0,572    0,672      0,677    0,915     0,664     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,499     0,000     DZ
                 0,198    0,003    0,500      0,198    0,283      0,308    0,825     0,238     D
Weighted Avg.    0,725    0,060    0,721      0,725    0,719      0,667    0,919     0,748     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1549   76  218    1   12   53   67    5    1    0    7 |    a = A
   75  621  158    8   36   32   18   14    4    0    5 |    b = E
  240  137 1490   15   60   76  246   40   10    1   10 |    c = CGJLNQSRT
   14   25   86   19    5   10    4   11    0    0    3 |    d = FV
   19   70   89    3  293    1   16    9    3    0    0 |    e = I
   90   45  164    7    5  529   66   20    6    0    8 |    f = O
   51   18  263    3   15   30 4028    8    2    0    0 |    g = 0
   34   20  131    4   28   28   32  114    7    0    1 |    h = BMP
    8    7   54    0    1   38    1   12  163    0    1 |    i = U
    0    0    5    0    0    0    0    0    0    0    0 |    j = DZ
   26   11   53    3    4   36    2    3    4    0   35 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
77.88	3.82	10.96	0.05	0.6	2.66	3.37	0.25	0.05	0.0	0.35	 a = A
7.72	63.95	16.27	0.82	3.71	3.3	1.85	1.44	0.41	0.0	0.51	 b = E
10.32	5.89	64.09	0.65	2.58	3.27	10.58	1.72	0.43	0.04	0.43	 c = CGJLNQSRT
7.91	14.12	48.59	10.73	2.82	5.65	2.26	6.21	0.0	0.0	1.69	 d = FV
3.78	13.92	17.69	0.6	58.25	0.2	3.18	1.79	0.6	0.0	0.0	 e = I
9.57	4.79	17.45	0.74	0.53	56.28	7.02	2.13	0.64	0.0	0.85	 f = O
1.15	0.41	5.95	0.07	0.34	0.68	91.17	0.18	0.05	0.0	0.0	 g = 0
8.52	5.01	32.83	1.0	7.02	7.02	8.02	28.57	1.75	0.0	0.25	 h = BMP
2.81	2.46	18.95	0.0	0.35	13.33	0.35	4.21	57.19	0.0	0.35	 i = U
0.0	0.0	100.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 j = DZ
14.69	6.21	29.94	1.69	2.26	20.34	1.13	1.69	2.26	0.0	19.77	 k = D
