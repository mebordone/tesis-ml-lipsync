Timestamp:2013-07-23-03:03:07
Inventario:I3
Intervalo:6ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I3-6ms-4gramas
Num Instances:  30450
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.2241





=== Stratified cross-validation ===

Correctly Classified Instances       25269               82.9852 %
Incorrectly Classified Instances      5181               17.0148 %
Kappa statistic                          0.7807
K&B Relative Info Score            2248989.764  %
K&B Information Score                58799.3144 bits      1.931  bits/instance
Class complexity | order 0           79591.8766 bits      2.6139 bits/instance
Class complexity | scheme          1169823.9877 bits     38.4179 bits/instance
Complexity improvement     (Sf)    -1090232.111 bits    -35.804  bits/instance
Mean absolute error                      0.0508
Root mean squared error                  0.1537
Relative absolute error                 35.8318 %
Root relative squared error             57.7299 %
Coverage of cases (0.95 level)          96.4762 %
Mean rel. region size (0.95 level)      22.0657 %
Total Number of Instances            30450     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,878    0,032    0,836      0,878    0,856      0,829    0,968     0,913     A
                 0,813    0,021    0,760      0,813    0,785      0,767    0,961     0,836     E
                 0,792    0,080    0,693      0,792    0,740      0,678    0,938     0,790     CGJLNQSRT
                 0,395    0,001    0,813      0,395    0,532      0,563    0,904     0,544     FV
                 0,754    0,006    0,840      0,754    0,795      0,788    0,961     0,826     I
                 0,729    0,013    0,823      0,729    0,773      0,758    0,942     0,805     O
                 0,908    0,052    0,915      0,908    0,912      0,858    0,975     0,957     0
                 0,579    0,004    0,825      0,579    0,680      0,683    0,917     0,678     BMP
                 0,782    0,002    0,924      0,782    0,847      0,847    0,965     0,871     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,624     0,047     DZ
                 0,608    0,002    0,845      0,608    0,707      0,713    0,976     0,767     D
Weighted Avg.    0,830    0,043    0,834      0,830    0,829      0,790    0,960     0,873     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  4236    73   299     2    16    46   126    12     2     1    12 |     a = A
   107  1907   208     4    27    22    48    13     4     0     7 |     b = E
   325   171  4480    16    41    84   477    31    13     3    12 |     c = CGJLNQSRT
    23    40   148   170     6    16    21     3     3     0     0 |     d = FV
    21    81   130     3   909     6    43    12     1     0     0 |     e = I
   118    52   215     1     4  1642   178    18     9     0    14 |     f = O
   128    95   673    11    46    81 10545    19     6     0     4 |     g = 0
    58    40   189     2    23    26    70   567     5     0     0 |     h = BMP
    20    19    58     0     5    28    13     9   545     0     0 |     i = U
     1     0     9     0     0     0     1     1     0     0     0 |     j = DZ
    31    31    54     0     5    44     4     2     2     0   268 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
87.79	1.51	6.2	0.04	0.33	0.95	2.61	0.25	0.04	0.02	0.25	 a = A
4.56	81.25	8.86	0.17	1.15	0.94	2.05	0.55	0.17	0.0	0.3	 b = E
5.75	3.02	79.25	0.28	0.73	1.49	8.44	0.55	0.23	0.05	0.21	 c = CGJLNQSRT
5.35	9.3	34.42	39.53	1.4	3.72	4.88	0.7	0.7	0.0	0.0	 d = FV
1.74	6.72	10.78	0.25	75.37	0.5	3.57	1.0	0.08	0.0	0.0	 e = I
5.24	2.31	9.55	0.04	0.18	72.95	7.91	0.8	0.4	0.0	0.62	 f = O
1.1	0.82	5.8	0.09	0.4	0.7	90.84	0.16	0.05	0.0	0.03	 g = 0
5.92	4.08	19.29	0.2	2.35	2.65	7.14	57.86	0.51	0.0	0.0	 h = BMP
2.87	2.73	8.32	0.0	0.72	4.02	1.87	1.29	78.19	0.0	0.0	 i = U
8.33	0.0	75.0	0.0	0.0	0.0	8.33	8.33	0.0	0.0	0.0	 j = DZ
7.03	7.03	12.24	0.0	1.13	9.98	0.91	0.45	0.45	0.0	60.77	 k = D
