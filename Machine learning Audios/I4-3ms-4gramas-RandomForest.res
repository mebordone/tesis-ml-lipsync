Timestamp:2013-07-22-23:01:41
Inventario:I4
Intervalo:3ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I4-3ms-4gramas
Num Instances:  61132
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1728





=== Stratified cross-validation ===

Correctly Classified Instances       53379               87.3176 %
Incorrectly Classified Instances      7753               12.6824 %
Kappa statistic                          0.8347
K&B Relative Info Score            4837612.8206 %
K&B Information Score               123544.1439 bits      2.0209 bits/instance
Class complexity | order 0          156104.8787 bits      2.5536 bits/instance
Class complexity | scheme          2180351.5333 bits     35.6663 bits/instance
Complexity improvement     (Sf)    -2024246.6546 bits    -33.1127 bits/instance
Mean absolute error                      0.0436
Root mean squared error                  0.1412
Relative absolute error                 28.2078 %
Root relative squared error             50.8144 %
Coverage of cases (0.95 level)          96.7186 %
Mean rel. region size (0.95 level)      21.2573 %
Total Number of Instances            61132     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,857    0,000    0,912      0,857    0,884      0,883    0,970     0,891     C
                 0,892    0,012    0,857      0,892    0,875      0,864    0,977     0,922     E
                 0,624    0,003    0,761      0,624    0,686      0,685    0,927     0,684     FV
                 0,911    0,027    0,892      0,911    0,901      0,877    0,975     0,948     AI
                 0,823    0,045    0,774      0,823    0,798      0,759    0,960     0,863     CDGKNRSYZ
                 0,830    0,007    0,904      0,830    0,865      0,856    0,953     0,879     O
                 0,932    0,058    0,910      0,932    0,921      0,870    0,981     0,962     0
                 0,532    0,007    0,742      0,532    0,620      0,617    0,900     0,619     LT
                 0,899    0,001    0,955      0,899    0,926      0,925    0,984     0,945     U
                 0,704    0,003    0,876      0,704    0,781      0,779    0,938     0,787     MBP
Weighted Avg.    0,873    0,037    0,872      0,873    0,871      0,839    0,969     0,912     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   311    14     1    11    16     0     6     3     1     0 |     a = C
     6  4124    10   114   176    19   109    36     5    22 |     b = E
     1    41   539    34   171    22    35    10     1    10 |     c = FV
     8   134    25 10904   379    50   357    71     4    35 |     d = AI
     3   178    71   403  7881    94   761   127    13    45 |     e = CDGKNRSYZ
     2    36     9   103   196  3703   370    19     8    17 |     f = O
     6   157    36   340   791   129 22118   103     9    34 |     g = 0
     2    61     8   185   356    39   359  1182     8    21 |     h = LT
     2    14     1    28    40    17    21     7  1246    10 |     i = U
     0    51     8   109   176    25   163    35    10  1371 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
85.67	3.86	0.28	3.03	4.41	0.0	1.65	0.83	0.28	0.0	 a = C
0.13	89.24	0.22	2.47	3.81	0.41	2.36	0.78	0.11	0.48	 b = E
0.12	4.75	62.38	3.94	19.79	2.55	4.05	1.16	0.12	1.16	 c = FV
0.07	1.12	0.21	91.12	3.17	0.42	2.98	0.59	0.03	0.29	 d = AI
0.03	1.86	0.74	4.21	82.3	0.98	7.95	1.33	0.14	0.47	 e = CDGKNRSYZ
0.04	0.81	0.2	2.31	4.39	82.97	8.29	0.43	0.18	0.38	 f = O
0.03	0.66	0.15	1.43	3.33	0.54	93.23	0.43	0.04	0.14	 g = 0
0.09	2.75	0.36	8.33	16.03	1.76	16.16	53.22	0.36	0.95	 h = LT
0.14	1.01	0.07	2.02	2.89	1.23	1.52	0.51	89.9	0.72	 i = U
0.0	2.62	0.41	5.6	9.03	1.28	8.37	1.8	0.51	70.38	 j = MBP
