Timestamp:2013-08-30-05:27:41
Inventario:I4
Intervalo:6ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I4-6ms-4gramas
Num Instances:  30450
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(14): class 
0Pitch(Hz)(7): class 
0RawPitch(12): class 
0SmPitch(10): class 
0Melogram(st)(14): class 
0ZCross(11): class 
0F1(Hz)(19): class 
0F2(Hz)(15): class 
0F3(Hz)(16): class 
0F4(Hz)(14): class 
1Int(dB)(14): class 
1Pitch(Hz)(10): class 
1RawPitch(11): class 
1SmPitch(10): class 
1Melogram(st)(14): class 
1ZCross(12): class 
1F1(Hz)(21): class 
1F2(Hz)(15): class 
1F3(Hz)(16): class 
1F4(Hz)(14): class 
2Int(dB)(14): class 
2Pitch(Hz)(10): class 
2RawPitch(10): class 
2SmPitch(10): class 
2Melogram(st)(11): class 
2ZCross(13): class 
2F1(Hz)(20): class 
2F2(Hz)(17): class 
2F3(Hz)(14): class 
2F4(Hz)(16): class 
3Int(dB)(16): class 
3Pitch(Hz)(11): class 
3RawPitch(12): class 
3SmPitch(11): class 
3Melogram(st)(14): class 
3ZCross(13): class 
3F1(Hz)(23): class 
3F2(Hz)(18): class 
3F3(Hz)(13): class 
3F4(Hz)(12): class 
class(10): 
LogScore Bayes: -1835051.6560843526
LogScore BDeu: -1860669.3779822842
LogScore MDL: -1857973.0439804434
LogScore ENTROPY: -1831755.64906731
LogScore AIC: -1836834.6490673097




=== Stratified cross-validation ===

Correctly Classified Instances       17521               57.5402 %
Incorrectly Classified Instances     12929               42.4598 %
Kappa statistic                          0.4526
K&B Relative Info Score            1378384.3956 %
K&B Information Score                35390.344  bits      1.1622 bits/instance
Class complexity | order 0           78166.4262 bits      2.567  bits/instance
Class complexity | scheme           407719.0637 bits     13.3898 bits/instance
Complexity improvement     (Sf)    -329552.6375 bits    -10.8227 bits/instance
Mean absolute error                      0.0855
Root mean squared error                  0.2765
Relative absolute error                 55.0548 %
Root relative squared error             99.2543 %
Coverage of cases (0.95 level)          62.913  %
Mean rel. region size (0.95 level)      13.2509 %
Total Number of Instances            30450     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,470    0,036    0,073      0,470    0,127      0,174    0,896     0,130     C
                 0,624    0,096    0,351      0,624    0,450      0,410    0,875     0,440     E
                 0,177    0,022    0,104      0,177    0,131      0,120    0,802     0,097     FV
                 0,529    0,064    0,672      0,529    0,592      0,511    0,874     0,689     AI
                 0,144    0,040    0,404      0,144    0,212      0,165    0,784     0,357     CDGKNRSYZ
                 0,409    0,040    0,447      0,409    0,427      0,384    0,838     0,410     O
                 0,900    0,149    0,788      0,900    0,841      0,736    0,953     0,934     0
                 0,048    0,014    0,120      0,048    0,069      0,054    0,761     0,089     LT
                 0,551    0,029    0,311      0,551    0,397      0,396    0,923     0,486     U
                 0,203    0,024    0,221      0,203    0,212      0,187    0,790     0,148     MBP
Weighted Avg.    0,575    0,089    0,578      0,575    0,559      0,488    0,881     0,635     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
    87    53     0    20     7     3     8     1     0     6 |     a = C
   165  1465    51   208   111    77   107    54    59    50 |     b = E
    32    87    76    37    31    36    95    10     4    22 |     c = FV
   318   955   126  3191   323   313   335   110   142   218 |     d = AI
   300   695   206   603   692   325  1475    94   201   211 |     e = CDGKNRSYZ
   108   259    26   306   116   921   239    39   215    22 |     f = O
    34   235   126   205   261   146 10452    38    47    64 |     g = 0
    31   204    45    70   102   117   361    54    70    65 |     h = LT
    29    70    15    30    11    70    23    22   384    43 |     i = U
    82   146    59    77    57    53   165    28   114   199 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
47.03	28.65	0.0	10.81	3.78	1.62	4.32	0.54	0.0	3.24	 a = C
7.03	62.42	2.17	8.86	4.73	3.28	4.56	2.3	2.51	2.13	 b = E
7.44	20.23	17.67	8.6	7.21	8.37	22.09	2.33	0.93	5.12	 c = FV
5.27	15.83	2.09	52.91	5.36	5.19	5.55	1.82	2.35	3.61	 d = AI
6.25	14.47	4.29	12.56	14.41	6.77	30.72	1.96	4.19	4.39	 e = CDGKNRSYZ
4.8	11.51	1.16	13.59	5.15	40.92	10.62	1.73	9.55	0.98	 f = O
0.29	2.02	1.09	1.77	2.25	1.26	90.04	0.33	0.4	0.55	 g = 0
2.77	18.23	4.02	6.26	9.12	10.46	32.26	4.83	6.26	5.81	 h = LT
4.16	10.04	2.15	4.3	1.58	10.04	3.3	3.16	55.09	6.17	 i = U
8.37	14.9	6.02	7.86	5.82	5.41	16.84	2.86	11.63	20.31	 j = MBP
