Timestamp:2013-07-22-20:42:57
Inventario:I0
Intervalo:10ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I0-10ms-1gramas
Num Instances:  18296
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    27 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.3093





=== Stratified cross-validation ===

Correctly Classified Instances       13526               73.9287 %
Incorrectly Classified Instances      4770               26.0713 %
Kappa statistic                          0.6761
K&B Relative Info Score            1171370.8444 %
K&B Information Score                39274.5591 bits      2.1466 bits/instance
Class complexity | order 0           61258.8315 bits      3.3482 bits/instance
Class complexity | scheme          2055118.8969 bits    112.3261 bits/instance
Complexity improvement     (Sf)    -1993860.0654 bits   -108.9779 bits/instance
Mean absolute error                      0.0271
Root mean squared error                  0.1179
Relative absolute error                 44.5734 %
Root relative squared error             67.6539 %
Coverage of cases (0.95 level)          89.3966 %
Mean rel. region size (0.95 level)      10.9558 %
Total Number of Instances            18296     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,182    0,002    0,276      0,182    0,219      0,221    0,688     0,107     ch
                 0,429    0,000    0,563      0,429    0,486      0,490    0,807     0,402     rr
                 0,937    0,093    0,857      0,937    0,895      0,831    0,970     0,956     0
                 0,388    0,003    0,559      0,388    0,458      0,461    0,818     0,402     hs
                 0,580    0,001    0,659      0,580    0,617      0,617    0,907     0,539     hg
                 0,859    0,044    0,772      0,859    0,813      0,781    0,958     0,881     a
                 0,151    0,008    0,216      0,151    0,178      0,170    0,727     0,101     c
                 0,420    0,002    0,575      0,420    0,485      0,489    0,902     0,414     b
                 0,755    0,036    0,643      0,755    0,695      0,669    0,939     0,731     e
                 0,500    0,006    0,568      0,500    0,532      0,527    0,894     0,460     d
                 0,449    0,002    0,600      0,449    0,513      0,516    0,905     0,507     g
                 0,143    0,003    0,254      0,143    0,183      0,186    0,707     0,124     f
                 0,721    0,014    0,693      0,721    0,706      0,694    0,935     0,731     i
                 0,307    0,001    0,585      0,307    0,403      0,421    0,787     0,321     j
                 0,632    0,004    0,714      0,632    0,671      0,667    0,926     0,624     m
                 0,474    0,005    0,615      0,474    0,535      0,533    0,899     0,510     l
                 0,676    0,022    0,714      0,676    0,695      0,671    0,913     0,714     o
                 0,646    0,013    0,653      0,646    0,650      0,637    0,938     0,668     n
                 0,136    0,001    0,289      0,136    0,185      0,196    0,729     0,119     q
                 0,158    0,003    0,352      0,158    0,218      0,230    0,709     0,162     p
                 0,551    0,025    0,531      0,551    0,541      0,517    0,893     0,534     s
                 0,252    0,007    0,470      0,252    0,328      0,333    0,802     0,273     r
                 0,697    0,003    0,836      0,697    0,760      0,758    0,936     0,767     u
                 0,151    0,008    0,260      0,151    0,191      0,187    0,765     0,143     t
                 0,317    0,001    0,643      0,317    0,425      0,449    0,828     0,330     v
                 0,505    0,001    0,750      0,505    0,604      0,614    0,879     0,546     y
                 0,000    0,000    0,000      0,000    0,000      0,000    0,498     0,000     z
Weighted Avg.    0,739    0,048    0,719      0,739    0,724      0,690    0,927     0,752     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m    n    o    p    q    r    s    t    u    v    w    x    y    z   aa   <-- classified as
   16    0   13    0    0    6    2    0    3    0    0    2    2    0    1    0    0    1    2    2   36    1    0    1    0    0    0 |    a = ch
    0    9    2    0    0    1    0    0    4    0    0    0    0    0    1    1    0    1    0    0    0    1    0    0    1    0    0 |    b = rr
    3    0 6368   12    4   64   26    1   36    7    4   12   24    3    2    5   35   19    5   20   90   15    7   35    1    0    0 |    c = 0
    0    0   56   80    0   19    2    0    9    1    0    0    5    0    2    1    9    1    1    1   12    1    3    2    1    0    0 |    d = hs
    0    0    2    0   29    3    0    0   12    0    0    0    2    0    0    0    1    0    0    0    1    0    0    0    0    0    0 |    e = hg
    1    4   86    4    0 2306    7    1   79   10    4    2   12    3    2   16   52   20    2    6   24   32    2    9    0    0    0 |    f = a
    5    0   97    3    0   13   42    0    8    1    1    5    7    3    1    1   15    1    3    4   46    1    0   21    0    0    0 |    g = c
    1    0    0    1    0    9    1   50   12    2    1    0    7    0    7    2   13    5    0    0    0    2    6    0    0    0    0 |    h = b
    0    1   55    2    4   78    9    7 1086   10    6    0   42    1    5    9   35   37    1    3   19   11    6    5    6    0    0 |    i = e
    0    0    5    0    0   36    1    0   30  133    1    0    3    0    1    5   27   12    0    0    2    1    5    1    3    0    0 |    j = d
    1    0    8    0    0   14    1    0   17    1   48    0    6    0    0    0    1    3    0    0    2    1    2    1    1    0    0 |    k = g
    1    0   24    0    0    6    8    0    4    0    0   17    0    2    0    0    7    3    1    2   32    3    0    7    1    1    0 |    l = f
    0    0   36    7    1   16    7    5   73    2    1    1  539    0    8    7    2   11    1    1   14    2    4    8    2    0    0 |    m = i
    2    0   13    3    0   16    4    0    9    0    0    3    2   31    1    0    3    1    0    0   10    1    1    0    0    1    0 |    n = j
    0    0    6    1    0   11    3    4   17    2    0    0   16    0  172    3   16   11    0    0    2    2    3    1    2    0    0 |    o = m
    1    0    7    5    2   31    1    3   34    5    0    0   24    1    5  155   23   16    0    0    3    8    0    1    0    2    0 |    p = l
    0    1  137    6    0   92   10    4   54   24    4    3    8    1   16   18  931   23    0    2   17    7    8    7    3    1    0 |    q = o
    0    0   25    4    2   37    1    3   58   14    0    0   16    0    6    8   20  420    0    3   10   12    5    6    0    0    0 |    r = n
    2    0   20    1    0    2    6    0    4    0    0    0    4    1    0    0    3    1   11    0   18    1    0    6    0    1    0 |    s = q
    3    0   72    1    0   20    6    0   14    2    0    1    4    0    1    2    3    4    0   32   24    6    0    7    1    0    0 |    t = p
   19    0  175    5    1   38   30    0   20    2    1   14   22    3    0    1   14    9    8    6  499    8    0   24    1    5    0 |    u = s
    1    1   48    1    0  119    3    5   46    8    2    3   13    1    3    7   28   12    0    4   16  111    3    3    0    2    0 |    v = r
    0    0   17    2    0   11    0    4   19    3    4    0    4    0    5    6   34   10    0    0    2    1  295    5    1    0    0 |    w = u
    2    0  144    2    1   15   22    0   13    1    1    3    9    2    0    1   13   11    3    4   45    5    0   53    0    0    0 |    x = t
    0    0    7    0    0   18    1    0   23    2    2    0    5    0    2    3   17    8    0    1    0    2    2    1   45    3    0 |    y = v
    0    0    8    3    0    5    1    0    5    4    0    0    2    1    0    1    2    1    0    0   11    1    1    0    1   48    0 |    z = y
    0    0    1    0    0    0    0    0    0    0    0    1    0    0    0    0    0    2    0    0    4    0    0    0    0    0    0 |   aa = z

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z	aa	
18.18	0.0	14.77	0.0	0.0	6.82	2.27	0.0	3.41	0.0	0.0	2.27	2.27	0.0	1.14	0.0	0.0	1.14	2.27	2.27	40.91	1.14	0.0	1.14	0.0	0.0	0.0	 a = ch
0.0	42.86	9.52	0.0	0.0	4.76	0.0	0.0	19.05	0.0	0.0	0.0	0.0	0.0	4.76	4.76	0.0	4.76	0.0	0.0	0.0	4.76	0.0	0.0	4.76	0.0	0.0	 b = rr
0.04	0.0	93.67	0.18	0.06	0.94	0.38	0.01	0.53	0.1	0.06	0.18	0.35	0.04	0.03	0.07	0.51	0.28	0.07	0.29	1.32	0.22	0.1	0.51	0.01	0.0	0.0	 c = 0
0.0	0.0	27.18	38.83	0.0	9.22	0.97	0.0	4.37	0.49	0.0	0.0	2.43	0.0	0.97	0.49	4.37	0.49	0.49	0.49	5.83	0.49	1.46	0.97	0.49	0.0	0.0	 d = hs
0.0	0.0	4.0	0.0	58.0	6.0	0.0	0.0	24.0	0.0	0.0	0.0	4.0	0.0	0.0	0.0	2.0	0.0	0.0	0.0	2.0	0.0	0.0	0.0	0.0	0.0	0.0	 e = hg
0.04	0.15	3.2	0.15	0.0	85.92	0.26	0.04	2.94	0.37	0.15	0.07	0.45	0.11	0.07	0.6	1.94	0.75	0.07	0.22	0.89	1.19	0.07	0.34	0.0	0.0	0.0	 f = a
1.8	0.0	34.89	1.08	0.0	4.68	15.11	0.0	2.88	0.36	0.36	1.8	2.52	1.08	0.36	0.36	5.4	0.36	1.08	1.44	16.55	0.36	0.0	7.55	0.0	0.0	0.0	 g = c
0.84	0.0	0.0	0.84	0.0	7.56	0.84	42.02	10.08	1.68	0.84	0.0	5.88	0.0	5.88	1.68	10.92	4.2	0.0	0.0	0.0	1.68	5.04	0.0	0.0	0.0	0.0	 h = b
0.0	0.07	3.82	0.14	0.28	5.42	0.63	0.49	75.52	0.7	0.42	0.0	2.92	0.07	0.35	0.63	2.43	2.57	0.07	0.21	1.32	0.76	0.42	0.35	0.42	0.0	0.0	 i = e
0.0	0.0	1.88	0.0	0.0	13.53	0.38	0.0	11.28	50.0	0.38	0.0	1.13	0.0	0.38	1.88	10.15	4.51	0.0	0.0	0.75	0.38	1.88	0.38	1.13	0.0	0.0	 j = d
0.93	0.0	7.48	0.0	0.0	13.08	0.93	0.0	15.89	0.93	44.86	0.0	5.61	0.0	0.0	0.0	0.93	2.8	0.0	0.0	1.87	0.93	1.87	0.93	0.93	0.0	0.0	 k = g
0.84	0.0	20.17	0.0	0.0	5.04	6.72	0.0	3.36	0.0	0.0	14.29	0.0	1.68	0.0	0.0	5.88	2.52	0.84	1.68	26.89	2.52	0.0	5.88	0.84	0.84	0.0	 l = f
0.0	0.0	4.81	0.94	0.13	2.14	0.94	0.67	9.76	0.27	0.13	0.13	72.06	0.0	1.07	0.94	0.27	1.47	0.13	0.13	1.87	0.27	0.53	1.07	0.27	0.0	0.0	 m = i
1.98	0.0	12.87	2.97	0.0	15.84	3.96	0.0	8.91	0.0	0.0	2.97	1.98	30.69	0.99	0.0	2.97	0.99	0.0	0.0	9.9	0.99	0.99	0.0	0.0	0.99	0.0	 n = j
0.0	0.0	2.21	0.37	0.0	4.04	1.1	1.47	6.25	0.74	0.0	0.0	5.88	0.0	63.24	1.1	5.88	4.04	0.0	0.0	0.74	0.74	1.1	0.37	0.74	0.0	0.0	 o = m
0.31	0.0	2.14	1.53	0.61	9.48	0.31	0.92	10.4	1.53	0.0	0.0	7.34	0.31	1.53	47.4	7.03	4.89	0.0	0.0	0.92	2.45	0.0	0.31	0.0	0.61	0.0	 p = l
0.0	0.07	9.95	0.44	0.0	6.68	0.73	0.29	3.92	1.74	0.29	0.22	0.58	0.07	1.16	1.31	67.61	1.67	0.0	0.15	1.23	0.51	0.58	0.51	0.22	0.07	0.0	 q = o
0.0	0.0	3.85	0.62	0.31	5.69	0.15	0.46	8.92	2.15	0.0	0.0	2.46	0.0	0.92	1.23	3.08	64.62	0.0	0.46	1.54	1.85	0.77	0.92	0.0	0.0	0.0	 r = n
2.47	0.0	24.69	1.23	0.0	2.47	7.41	0.0	4.94	0.0	0.0	0.0	4.94	1.23	0.0	0.0	3.7	1.23	13.58	0.0	22.22	1.23	0.0	7.41	0.0	1.23	0.0	 s = q
1.48	0.0	35.47	0.49	0.0	9.85	2.96	0.0	6.9	0.99	0.0	0.49	1.97	0.0	0.49	0.99	1.48	1.97	0.0	15.76	11.82	2.96	0.0	3.45	0.49	0.0	0.0	 t = p
2.1	0.0	19.34	0.55	0.11	4.2	3.31	0.0	2.21	0.22	0.11	1.55	2.43	0.33	0.0	0.11	1.55	0.99	0.88	0.66	55.14	0.88	0.0	2.65	0.11	0.55	0.0	 u = s
0.23	0.23	10.91	0.23	0.0	27.05	0.68	1.14	10.45	1.82	0.45	0.68	2.95	0.23	0.68	1.59	6.36	2.73	0.0	0.91	3.64	25.23	0.68	0.68	0.0	0.45	0.0	 v = r
0.0	0.0	4.02	0.47	0.0	2.6	0.0	0.95	4.49	0.71	0.95	0.0	0.95	0.0	1.18	1.42	8.04	2.36	0.0	0.0	0.47	0.24	69.74	1.18	0.24	0.0	0.0	 w = u
0.57	0.0	41.14	0.57	0.29	4.29	6.29	0.0	3.71	0.29	0.29	0.86	2.57	0.57	0.0	0.29	3.71	3.14	0.86	1.14	12.86	1.43	0.0	15.14	0.0	0.0	0.0	 x = t
0.0	0.0	4.93	0.0	0.0	12.68	0.7	0.0	16.2	1.41	1.41	0.0	3.52	0.0	1.41	2.11	11.97	5.63	0.0	0.7	0.0	1.41	1.41	0.7	31.69	2.11	0.0	 y = v
0.0	0.0	8.42	3.16	0.0	5.26	1.05	0.0	5.26	4.21	0.0	0.0	2.11	1.05	0.0	1.05	2.11	1.05	0.0	0.0	11.58	1.05	1.05	0.0	1.05	50.53	0.0	 z = y
0.0	0.0	12.5	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	12.5	0.0	0.0	0.0	0.0	0.0	25.0	0.0	0.0	50.0	0.0	0.0	0.0	0.0	0.0	0.0	 aa = z
