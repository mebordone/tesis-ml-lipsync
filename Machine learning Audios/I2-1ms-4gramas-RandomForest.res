Timestamp:2013-07-22-21:32:44
Inventario:I2
Intervalo:1ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I2-1ms-4gramas
Num Instances:  183401
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1265





=== Stratified cross-validation ===

Correctly Classified Instances      165153               90.0502 %
Incorrectly Classified Instances     18248                9.9498 %
Kappa statistic                          0.8741
K&B Relative Info Score            15818058.9824 %
K&B Information Score               451929.4697 bits      2.4642 bits/instance
Class complexity | order 0          523967.6938 bits      2.857  bits/instance
Class complexity | scheme          7380937.7306 bits     40.2448 bits/instance
Complexity improvement     (Sf)    -6856970.0369 bits    -37.3879 bits/instance
Mean absolute error                      0.0243
Root mean squared error                  0.1113
Relative absolute error                 18.3328 %
Root relative squared error             43.2734 %
Coverage of cases (0.95 level)          96.2339 %
Mean rel. region size (0.95 level)      13.8592 %
Total Number of Instances           183401     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,942    0,010    0,944      0,942    0,943      0,933    0,981     0,965     A
                 0,742    0,011    0,771      0,742    0,756      0,744    0,927     0,792     LGJKC
                 0,941    0,004    0,947      0,941    0,944      0,940    0,983     0,962     E
                 0,740    0,003    0,791      0,740    0,764      0,762    0,923     0,774     FV
                 0,925    0,002    0,945      0,925    0,935      0,932    0,978     0,948     I
                 0,877    0,005    0,926      0,877    0,901      0,894    0,959     0,913     O
                 0,764    0,031    0,721      0,764    0,742      0,715    0,953     0,808     SNRTY
                 0,943    0,053    0,920      0,943    0,931      0,886    0,984     0,968     0
                 0,791    0,003    0,887      0,791    0,836      0,833    0,941     0,846     BMP
                 0,931    0,001    0,960      0,931    0,945      0,945    0,985     0,961     DZ
                 0,958    0,000    0,981      0,958    0,969      0,969    0,993     0,982     U
                 0,875    0,002    0,944      0,875    0,908      0,905    0,964     0,916     SNRTXY
Weighted Avg.    0,901    0,027    0,901      0,901    0,901      0,877    0,973     0,930     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 26905   192    39    26    21    45   482   739    50    11     5    42 |     a = A
   188  6547    71    60    29    69   890   864    55     1    16    37 |     b = LGJKC
    69   106 12935    12    28    37   222   253    35    14     7    22 |     c = E
    47    64    27  1894    11    26   352    97    21     4     1    17 |     d = FV
    52    55    26    17  6550    11   150   193    19     2     4     4 |     e = I
    93    90    42    39    13 11610   266   995    32    19     2    33 |     f = O
   416   642   157   193    85   160 13194  2115   160    22    12   117 |     g = SNRTY
   508   575   215    86   123   454  1870 67999   172    16    13    83 |     h = 0
   109   115    65    26    41    47   361   426  4600     2    10    12 |     i = BMP
    23     8    23     9     5    29    45    25     6  2497     1    11 |     j = DZ
    16    19    13     1    18    16    35    34    17     0  3963     5 |     k = U
    79    77    50    31    10    29   425   182    19    12     6  6459 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
94.22	0.67	0.14	0.09	0.07	0.16	1.69	2.59	0.18	0.04	0.02	0.15	 a = A
2.13	74.17	0.8	0.68	0.33	0.78	10.08	9.79	0.62	0.01	0.18	0.42	 b = LGJKC
0.5	0.77	94.14	0.09	0.2	0.27	1.62	1.84	0.25	0.1	0.05	0.16	 c = E
1.84	2.5	1.05	73.96	0.43	1.02	13.74	3.79	0.82	0.16	0.04	0.66	 d = FV
0.73	0.78	0.37	0.24	92.47	0.16	2.12	2.72	0.27	0.03	0.06	0.06	 e = I
0.7	0.68	0.32	0.29	0.1	87.73	2.01	7.52	0.24	0.14	0.02	0.25	 f = O
2.41	3.72	0.91	1.12	0.49	0.93	76.39	12.24	0.93	0.13	0.07	0.68	 g = SNRTY
0.7	0.8	0.3	0.12	0.17	0.63	2.59	94.29	0.24	0.02	0.02	0.12	 h = 0
1.87	1.98	1.12	0.45	0.71	0.81	6.21	7.33	79.12	0.03	0.17	0.21	 i = BMP
0.86	0.3	0.86	0.34	0.19	1.08	1.68	0.93	0.22	93.1	0.04	0.41	 j = DZ
0.39	0.46	0.31	0.02	0.44	0.39	0.85	0.82	0.41	0.0	95.79	0.12	 k = U
1.07	1.04	0.68	0.42	0.14	0.39	5.76	2.47	0.26	0.16	0.08	87.53	 l = SNRTXY
