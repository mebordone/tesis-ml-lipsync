Timestamp:2013-08-30-04:43:05
Inventario:I2
Intervalo:20ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I2-20ms-3gramas
Num Instances:  9127
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(4): class 
0F1(Hz)(8): class 
0F2(Hz)(8): class 
0F3(Hz)(2): class 
0F4(Hz)(3): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(4): class 
1F1(Hz)(9): class 
1F2(Hz)(8): class 
1F3(Hz)(2): class 
1F4(Hz)(3): class 
2Int(dB)(9): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(8): class 
2F1(Hz)(9): class 
2F2(Hz)(9): class 
2F3(Hz)(5): class 
2F4(Hz)(5): class 
class(12): 
LogScore Bayes: -265842.2543305333
LogScore BDeu: -272765.5436986379
LogScore MDL: -272034.3688505526
LogScore ENTROPY: -264433.68874139973
LogScore AIC: -266100.68874139973




=== Stratified cross-validation ===

Correctly Classified Instances        5002               54.8044 %
Incorrectly Classified Instances      4125               45.1956 %
Kappa statistic                          0.4449
K&B Relative Info Score             397682.7942 %
K&B Information Score                11728.4366 bits      1.285  bits/instance
Class complexity | order 0           26899.5449 bits      2.9472 bits/instance
Class complexity | scheme            79887.6342 bits      8.7529 bits/instance
Complexity improvement     (Sf)     -52988.0893 bits     -5.8056 bits/instance
Mean absolute error                      0.0766
Root mean squared error                  0.2473
Relative absolute error                 56.2937 %
Root relative squared error             94.8289 %
Coverage of cases (0.95 level)          66.1663 %
Mean rel. region size (0.95 level)      15.3081 %
Total Number of Instances             9127     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,560    0,066    0,627      0,560    0,592      0,517    0,878     0,661     A
                 0,016    0,009    0,084      0,016    0,026      0,016    0,680     0,078     LGJKC
                 0,420    0,049    0,435      0,420    0,427      0,377    0,869     0,414     E
                 0,065    0,017    0,055      0,065    0,059      0,044    0,760     0,040     FV
                 0,558    0,045    0,354      0,558    0,433      0,414    0,899     0,398     I
                 0,209    0,018    0,502      0,209    0,295      0,290    0,809     0,341     O
                 0,279    0,056    0,355      0,279    0,312      0,248    0,788     0,301     SNRTY
                 0,912    0,094    0,842      0,912    0,875      0,804    0,968     0,957     0
                 0,023    0,004    0,152      0,023    0,041      0,048    0,751     0,084     BMP
                 0,449    0,076    0,083      0,449    0,140      0,166    0,839     0,079     DZ
                 0,450    0,017    0,394      0,450    0,420      0,405    0,900     0,436     U
                 0,245    0,063    0,148      0,245    0,185      0,144    0,814     0,127     SNRTXY
Weighted Avg.    0,548    0,062    0,554      0,548    0,539      0,487    0,877     0,580     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
  843   25  113   30   43   58   85   64    3  149   15   77 |    a = A
   51    7   43   12   45    6   61  100    2   44   10   69 |    b = LGJKC
   87   20  313    7   78   13   32   30    3   71   26   66 |    c = E
   19    3   17    9    4    6   20   12    3   24    1   20 |    d = FV
    5    3   56    2  215    0   30   22    3   14    3   32 |    e = I
  111   11   56    6   24  149   47   66    3  143   51   45 |    f = O
   81    1   27   37   61    9  255  210   11   89   13  120 |    g = SNRTY
   21    4   11   35   32   13  113 2949    7   17    5   28 |    h = 0
   31    4   21   14   57    8   30   25    7   35   11   56 |    i = BMP
   23    0    8    3    3    3    9    3    0   62    6   18 |    j = DZ
   16    5   14    1    5   27    8    8    0   20   98   16 |    k = U
   57    0   41    9   41    5   29   15    4   81   10   95 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
56.01	1.66	7.51	1.99	2.86	3.85	5.65	4.25	0.2	9.9	1.0	5.12	 a = A
11.33	1.56	9.56	2.67	10.0	1.33	13.56	22.22	0.44	9.78	2.22	15.33	 b = LGJKC
11.66	2.68	41.96	0.94	10.46	1.74	4.29	4.02	0.4	9.52	3.49	8.85	 c = E
13.77	2.17	12.32	6.52	2.9	4.35	14.49	8.7	2.17	17.39	0.72	14.49	 d = FV
1.3	0.78	14.55	0.52	55.84	0.0	7.79	5.71	0.78	3.64	0.78	8.31	 e = I
15.59	1.54	7.87	0.84	3.37	20.93	6.6	9.27	0.42	20.08	7.16	6.32	 f = O
8.86	0.11	2.95	4.05	6.67	0.98	27.9	22.98	1.2	9.74	1.42	13.13	 g = SNRTY
0.65	0.12	0.34	1.08	0.99	0.4	3.49	91.16	0.22	0.53	0.15	0.87	 h = 0
10.37	1.34	7.02	4.68	19.06	2.68	10.03	8.36	2.34	11.71	3.68	18.73	 i = BMP
16.67	0.0	5.8	2.17	2.17	2.17	6.52	2.17	0.0	44.93	4.35	13.04	 j = DZ
7.34	2.29	6.42	0.46	2.29	12.39	3.67	3.67	0.0	9.17	44.95	7.34	 k = U
14.73	0.0	10.59	2.33	10.59	1.29	7.49	3.88	1.03	20.93	2.58	24.55	 l = SNRTXY
