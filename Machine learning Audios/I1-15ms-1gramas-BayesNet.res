Timestamp:2013-08-30-04:18:18
Inventario:I1
Intervalo:15ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I1-15ms-1gramas
Num Instances:  12189
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(9): class 
0F1(Hz)(11): class 
0F2(Hz)(10): class 
0F3(Hz)(7): class 
0F4(Hz)(7): class 
class(13): 
LogScore Bayes: -146900.04283018323
LogScore BDeu: -150569.4807090553
LogScore MDL: -150154.78849493206
LogScore ENTROPY: -146306.7982184078
LogScore AIC: -147124.7982184078




=== Stratified cross-validation ===

Correctly Classified Instances        7513               61.6375 %
Incorrectly Classified Instances      4676               38.3625 %
Kappa statistic                          0.5159
K&B Relative Info Score             609528.1295 %
K&B Information Score                17993.2147 bits      1.4762 bits/instance
Class complexity | order 0           35958.643  bits      2.9501 bits/instance
Class complexity | scheme            44831.2047 bits      3.678  bits/instance
Complexity improvement     (Sf)      -8872.5618 bits     -0.7279 bits/instance
Mean absolute error                      0.0659
Root mean squared error                  0.2087
Relative absolute error                 52.7683 %
Root relative squared error             83.4988 %
Coverage of cases (0.95 level)          81.9673 %
Mean rel. region size (0.95 level)      21.9952 %
Total Number of Instances            12189     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,715    0,087    0,616      0,715    0,662      0,593    0,904     0,729     a
                 0,013    0,003    0,119      0,013    0,023      0,029    0,779     0,106     b
                 0,596    0,068    0,431      0,596    0,500      0,457    0,892     0,492     e
                 0,088    0,007    0,151      0,088    0,111      0,105    0,844     0,092     d
                 0,017    0,003    0,068      0,017    0,027      0,027    0,767     0,043     f
                 0,610    0,035    0,429      0,610    0,504      0,487    0,926     0,512     i
                 0,034    0,004    0,143      0,034    0,055      0,061    0,843     0,101     k
                 0,014    0,001    0,133      0,014    0,026      0,040    0,816     0,057     j
                 0,290    0,067    0,324      0,290    0,306      0,234    0,797     0,273     l
                 0,341    0,030    0,492      0,341    0,403      0,370    0,834     0,412     o
                 0,936    0,112    0,826      0,936    0,878      0,805    0,974     0,963     0
                 0,311    0,026    0,431      0,311    0,361      0,332    0,899     0,338     s
                 0,474    0,013    0,475      0,474    0,475      0,462    0,927     0,497     u
Weighted Avg.    0,616    0,073    0,571      0,616    0,585      0,532    0,904     0,628     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1422    0  138   11    5   37   10    1  152   75   94   38    6 |    a = a
   45    5   57   12    2   66    4    3   83   28   55   15   24 |    b = b
  135    4  579    6    4   67    3    3   79   23   34   20   14 |    c = e
   41    4   32   16    0    9    1    0   35   27    6    2    9 |    d = d
   21    2   32    5    3    4    2    1   43   17   31   14    2 |    e = f
   11    4  108    0    2  307    4    0   27    1   19   19    1 |    f = i
    9    1   10    0    3   11    8    1   14    8  108   62    2 |    g = k
   39    0   29    3    1   16    0    2   17    4   13   13    1 |    h = j
  209   10  172   20    6  108   10    0  353   66  166   54   42 |    i = l
  239    4   93   23    2   17    4    0   83  321   94   16   44 |    j = o
   54    3   32    1   14   34    4    0   73   23 4134   44    2 |    k = 0
   54    2   36    5    2   38    4    4  104   11  244  228    2 |    l = s
   28    3   26    4    0    1    2    0   28   49    5    4  135 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
71.49	0.0	6.94	0.55	0.25	1.86	0.5	0.05	7.64	3.77	4.73	1.91	0.3	 a = a
11.28	1.25	14.29	3.01	0.5	16.54	1.0	0.75	20.8	7.02	13.78	3.76	6.02	 b = b
13.9	0.41	59.63	0.62	0.41	6.9	0.31	0.31	8.14	2.37	3.5	2.06	1.44	 c = e
22.53	2.2	17.58	8.79	0.0	4.95	0.55	0.0	19.23	14.84	3.3	1.1	4.95	 d = d
11.86	1.13	18.08	2.82	1.69	2.26	1.13	0.56	24.29	9.6	17.51	7.91	1.13	 e = f
2.19	0.8	21.47	0.0	0.4	61.03	0.8	0.0	5.37	0.2	3.78	3.78	0.2	 f = i
3.8	0.42	4.22	0.0	1.27	4.64	3.38	0.42	5.91	3.38	45.57	26.16	0.84	 g = k
28.26	0.0	21.01	2.17	0.72	11.59	0.0	1.45	12.32	2.9	9.42	9.42	0.72	 h = j
17.19	0.82	14.14	1.64	0.49	8.88	0.82	0.0	29.03	5.43	13.65	4.44	3.45	 i = l
25.43	0.43	9.89	2.45	0.21	1.81	0.43	0.0	8.83	34.15	10.0	1.7	4.68	 j = o
1.22	0.07	0.72	0.02	0.32	0.77	0.09	0.0	1.65	0.52	93.57	1.0	0.05	 k = 0
7.36	0.27	4.9	0.68	0.27	5.18	0.54	0.54	14.17	1.5	33.24	31.06	0.27	 l = s
9.82	1.05	9.12	1.4	0.0	0.35	0.7	0.0	9.82	17.19	1.75	1.4	47.37	 m = u
