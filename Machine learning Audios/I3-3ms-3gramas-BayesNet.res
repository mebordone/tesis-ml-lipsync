Timestamp:2013-08-30-05:01:26
Inventario:I3
Intervalo:3ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I3-3ms-3gramas
Num Instances:  61133
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(22): class 
0Pitch(Hz)(31): class 
0RawPitch(22): class 
0SmPitch(25): class 
0Melogram(st)(33): class 
0ZCross(17): class 
0F1(Hz)(275): class 
0F2(Hz)(217): class 
0F3(Hz)(382): class 
0F4(Hz)(338): class 
1Int(dB)(21): class 
1Pitch(Hz)(29): class 
1RawPitch(22): class 
1SmPitch(24): class 
1Melogram(st)(32): class 
1ZCross(15): class 
1F1(Hz)(210): class 
1F2(Hz)(238): class 
1F3(Hz)(385): class 
1F4(Hz)(357): class 
2Int(dB)(20): class 
2Pitch(Hz)(35): class 
2RawPitch(22): class 
2SmPitch(25): class 
2Melogram(st)(32): class 
2ZCross(16): class 
2F1(Hz)(242): class 
2F2(Hz)(240): class 
2F3(Hz)(298): class 
2F4(Hz)(262): class 
class(11): 
LogScore Bayes: -3852009.7201878307
LogScore BDeu: -4239318.606978557
LogScore MDL: -4172693.8998918193
LogScore ENTROPY: -3938848.9044922674
LogScore AIC: -3981285.9044922674




=== Stratified cross-validation ===

Correctly Classified Instances       38624               63.1803 %
Incorrectly Classified Instances     22509               36.8197 %
Kappa statistic                          0.5265
K&B Relative Info Score            3354695.5961 %
K&B Information Score                87230.2935 bits      1.4269 bits/instance
Class complexity | order 0          158934.5154 bits      2.5998 bits/instance
Class complexity | scheme           615198.3513 bits     10.0633 bits/instance
Complexity improvement     (Sf)    -456263.8358 bits     -7.4635 bits/instance
Mean absolute error                      0.0675
Root mean squared error                  0.2463
Relative absolute error                 47.7904 %
Root relative squared error             92.6971 %
Coverage of cases (0.95 level)          68.454  %
Mean rel. region size (0.95 level)      11.513  %
Total Number of Instances            61133     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,665    0,045    0,734      0,665    0,698      0,646    0,921     0,770     A
                 0,658    0,062    0,463      0,658    0,543      0,508    0,913     0,582     E
                 0,173    0,033    0,545      0,173    0,263      0,232    0,856     0,503     CGJLNQSRT
                 0,253    0,010    0,260      0,253    0,257      0,246    0,859     0,220     FV
                 0,704    0,033    0,467      0,704    0,561      0,552    0,935     0,611     I
                 0,469    0,027    0,575      0,469    0,517      0,486    0,873     0,547     O
                 0,898    0,155    0,786      0,898    0,838      0,729    0,959     0,953     0
                 0,260    0,011    0,436      0,260    0,326      0,321    0,841     0,311     BMP
                 0,683    0,021    0,427      0,683    0,525      0,527    0,954     0,636     U
                 0,000    0,002    0,000      0,000    0,000      -0,001   0,949     0,004     DZ
                 0,577    0,052    0,140      0,577    0,225      0,264    0,923     0,273     D
Weighted Avg.    0,632    0,083    0,645      0,632    0,613      0,552    0,917     0,722     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  6375   724   369   104   213   484   513    60   142    14   587 |     a = A
   296  3040   238    56   293    86   184    84    90     4   250 |     b = E
   831  1322  1949   204   771   459  3864   243   457    11  1145 |     c = CGJLNQSRT
    62   106    41   219    59    35   212    23    18     3    86 |     d = FV
    25   282   114    19  1676     2   130    33    36    13    52 |     e = I
   544   317   148    32    67  2093   481    48   276     9   448 |     f = O
   315   407   507   128   274   262 21311   117    69    46   288 |     g = 0
   111   182   130    53   193    69   358   507   156     2   187 |     h = BMP
    48    85    46    13     5   104    27    29   946     0    83 |     i = U
     0     0     0     0     0     0    23     0     0     0     0 |     j = DZ
    78   102    37    15    41    46     5    18    26     5   508 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
66.51	7.55	3.85	1.09	2.22	5.05	5.35	0.63	1.48	0.15	6.12	 a = A
6.41	65.79	5.15	1.21	6.34	1.86	3.98	1.82	1.95	0.09	5.41	 b = E
7.38	11.74	17.32	1.81	6.85	4.08	34.33	2.16	4.06	0.1	10.17	 c = CGJLNQSRT
7.18	12.27	4.75	25.35	6.83	4.05	24.54	2.66	2.08	0.35	9.95	 d = FV
1.05	11.84	4.79	0.8	70.36	0.08	5.46	1.39	1.51	0.55	2.18	 e = I
12.19	7.1	3.32	0.72	1.5	46.9	10.78	1.08	6.18	0.2	10.04	 f = O
1.33	1.72	2.14	0.54	1.15	1.1	89.83	0.49	0.29	0.19	1.21	 g = 0
5.7	9.34	6.67	2.72	9.91	3.54	18.38	26.03	8.01	0.1	9.6	 h = BMP
3.46	6.13	3.32	0.94	0.36	7.5	1.95	2.09	68.25	0.0	5.99	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
8.85	11.58	4.2	1.7	4.65	5.22	0.57	2.04	2.95	0.57	57.66	 k = D
