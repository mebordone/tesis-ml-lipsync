Timestamp:2013-08-30-04:18:54
Inventario:I1
Intervalo:20ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I1-20ms-2gramas
Num Instances:  9128
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(7): class 
0F1(Hz)(9): class 
0F2(Hz)(8): class 
0F3(Hz)(3): class 
0F4(Hz)(3): class 
1Int(dB)(9): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(8): class 
1F1(Hz)(10): class 
1F2(Hz)(9): class 
1F3(Hz)(6): class 
1F4(Hz)(5): class 
class(13): 
LogScore Bayes: -188183.24806413706
LogScore BDeu: -194009.0359250683
LogScore MDL: -193411.03400123303
LogScore ENTROPY: -187251.0806734965
LogScore AIC: -188602.0806734965




=== Stratified cross-validation ===

Correctly Classified Instances        5223               57.2195 %
Incorrectly Classified Instances      3905               42.7805 %
Kappa statistic                          0.4731
K&B Relative Info Score             425951.3683 %
K&B Information Score                12668.3552 bits      1.3879 bits/instance
Class complexity | order 0           27126.5299 bits      2.9718 bits/instance
Class complexity | scheme            56617.7715 bits      6.2026 bits/instance
Complexity improvement     (Sf)     -29491.2415 bits     -3.2309 bits/instance
Mean absolute error                      0.0682
Root mean squared error                  0.2251
Relative absolute error                 54.2823 %
Root relative squared error             89.8351 %
Coverage of cases (0.95 level)          73.6854 %
Mean rel. region size (0.95 level)      16.9773 %
Total Number of Instances             9128     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,638    0,076    0,623      0,638    0,630      0,556    0,888     0,699     a
                 0,013    0,004    0,108      0,013    0,024      0,027    0,754     0,085     b
                 0,540    0,068    0,416      0,540    0,470      0,420    0,877     0,438     e
                 0,326    0,043    0,104      0,326    0,157      0,162    0,835     0,085     d
                 0,043    0,011    0,056      0,043    0,049      0,037    0,752     0,040     f
                 0,587    0,045    0,365      0,587    0,450      0,433    0,905     0,422     i
                 0,210    0,032    0,117      0,210    0,150      0,134    0,831     0,093     k
                 0,037    0,005    0,082      0,037    0,051      0,047    0,818     0,056     j
                 0,147    0,045    0,265      0,147    0,189      0,134    0,772     0,234     l
                 0,257    0,021    0,506      0,257    0,341      0,324    0,820     0,373     o
                 0,918    0,084    0,857      0,918    0,887      0,823    0,972     0,962     0
                 0,265    0,034    0,331      0,265    0,294      0,256    0,880     0,321     s
                 0,468    0,019    0,375      0,468    0,416      0,403    0,904     0,425     u
Weighted Avg.    0,572    0,060    0,560      0,572    0,556      0,507    0,892     0,599     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
  960    2  120   58   19   43   44    3   90   56   49   49   12 |    a = a
   28    4   48   32    8   59   16    7   30    7   27   14   19 |    b = b
   86    1  403   38    7   64   25   11   31   17   21   20   22 |    c = e
   29    1   14   45    0    7    2    0   18   11    4    2    5 |    d = d
   19    2   23   16    6    7    9    2   20    8   11   13    2 |    e = f
    3    1   83    6    1  226   22    1    8    0   11   20    3 |    f = i
    6    2    2    0    3   12   38    0   10    4   79   23    2 |    g = k
   26    0   30    4    4   13    2    4   11    1    4    8    2 |    h = j
  159   14  121  112   16   95   46    4  134   28   82   56   43 |    i = l
  134    3   69   88    7   19   18    1   53  183   65   21   51 |    j = o
   27    4   17    8   26   30   34    2   35   11 2972   64    6 |    k = 0
   43    3   21   13   10   40   57   14   56    6  139  146    3 |    l = s
   21    0   18   14    0    5   11    0   10   30    2    5  102 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
63.79	0.13	7.97	3.85	1.26	2.86	2.92	0.2	5.98	3.72	3.26	3.26	0.8	 a = a
9.36	1.34	16.05	10.7	2.68	19.73	5.35	2.34	10.03	2.34	9.03	4.68	6.35	 b = b
11.53	0.13	54.02	5.09	0.94	8.58	3.35	1.47	4.16	2.28	2.82	2.68	2.95	 c = e
21.01	0.72	10.14	32.61	0.0	5.07	1.45	0.0	13.04	7.97	2.9	1.45	3.62	 d = d
13.77	1.45	16.67	11.59	4.35	5.07	6.52	1.45	14.49	5.8	7.97	9.42	1.45	 e = f
0.78	0.26	21.56	1.56	0.26	58.7	5.71	0.26	2.08	0.0	2.86	5.19	0.78	 f = i
3.31	1.1	1.1	0.0	1.66	6.63	20.99	0.0	5.52	2.21	43.65	12.71	1.1	 g = k
23.85	0.0	27.52	3.67	3.67	11.93	1.83	3.67	10.09	0.92	3.67	7.34	1.83	 h = j
17.47	1.54	13.3	12.31	1.76	10.44	5.05	0.44	14.73	3.08	9.01	6.15	4.73	 i = l
18.82	0.42	9.69	12.36	0.98	2.67	2.53	0.14	7.44	25.7	9.13	2.95	7.16	 j = o
0.83	0.12	0.53	0.25	0.8	0.93	1.05	0.06	1.08	0.34	91.84	1.98	0.19	 k = 0
7.8	0.54	3.81	2.36	1.81	7.26	10.34	2.54	10.16	1.09	25.23	26.5	0.54	 l = s
9.63	0.0	8.26	6.42	0.0	2.29	5.05	0.0	4.59	13.76	0.92	2.29	46.79	 m = u
