Timestamp:2013-08-30-05:28:57
Inventario:I4
Intervalo:10ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I4-10ms-5gramas
Num Instances:  18292
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  43 4RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  44 4SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  45 4Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  47 4F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  49 4F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  50 4F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(6): class 
0RawPitch(4): class 
0SmPitch(6): class 
0Melogram(st)(10): class 
0ZCross(7): class 
0F1(Hz)(10): class 
0F2(Hz)(11): class 
0F3(Hz)(8): class 
0F4(Hz)(9): class 
1Int(dB)(10): class 
1Pitch(Hz)(6): class 
1RawPitch(4): class 
1SmPitch(6): class 
1Melogram(st)(9): class 
1ZCross(8): class 
1F1(Hz)(10): class 
1F2(Hz)(10): class 
1F3(Hz)(8): class 
1F4(Hz)(8): class 
2Int(dB)(12): class 
2Pitch(Hz)(7): class 
2RawPitch(4): class 
2SmPitch(6): class 
2Melogram(st)(9): class 
2ZCross(10): class 
2F1(Hz)(15): class 
2F2(Hz)(12): class 
2F3(Hz)(11): class 
2F4(Hz)(9): class 
3Int(dB)(10): class 
3Pitch(Hz)(7): class 
3RawPitch(5): class 
3SmPitch(7): class 
3Melogram(st)(9): class 
3ZCross(10): class 
3F1(Hz)(13): class 
3F2(Hz)(10): class 
3F3(Hz)(9): class 
3F4(Hz)(9): class 
4Int(dB)(11): class 
4Pitch(Hz)(7): class 
4RawPitch(5): class 
4SmPitch(5): class 
4Melogram(st)(9): class 
4ZCross(11): class 
4F1(Hz)(13): class 
4F2(Hz)(13): class 
4F3(Hz)(9): class 
4F4(Hz)(8): class 
class(10): 
LogScore Bayes: -1113877.4966908654
LogScore BDeu: -1131041.414087114
LogScore MDL: -1129770.6911966773
LogScore ENTROPY: -1110883.2265680218
LogScore AIC: -1114732.2265680218




=== Stratified cross-validation ===

Correctly Classified Instances       10069               55.0459 %
Incorrectly Classified Instances      8223               44.9541 %
Kappa statistic                          0.4288
K&B Relative Info Score             777266.6462 %
K&B Information Score                20079.6987 bits      1.0977 bits/instance
Class complexity | order 0           47237.6759 bits      2.5824 bits/instance
Class complexity | scheme           278130.9603 bits     15.2051 bits/instance
Complexity improvement     (Sf)    -230893.2844 bits    -12.6226 bits/instance
Mean absolute error                      0.0904
Root mean squared error                  0.2851
Relative absolute error                 57.8748 %
Root relative squared error            102.0316 %
Coverage of cases (0.95 level)          60.5183 %
Mean rel. region size (0.95 level)      13.232  %
Total Number of Instances            18292     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,514    0,059    0,049      0,514    0,089      0,144    0,865     0,131     C
                 0,522    0,094    0,321      0,522    0,398      0,345    0,851     0,391     E
                 0,126    0,025    0,068      0,126    0,089      0,075    0,798     0,059     FV
                 0,474    0,068    0,637      0,474    0,543      0,456    0,854     0,647     AI
                 0,164    0,043    0,424      0,164    0,237      0,185    0,749     0,338     CDGKNRSYZ
                 0,389    0,038    0,456      0,389    0,420      0,378    0,832     0,395     O
                 0,898    0,130    0,804      0,898    0,848      0,754    0,935     0,871     0
                 0,064    0,028    0,079      0,064    0,071      0,039    0,729     0,074     LT
                 0,475    0,018    0,381      0,475    0,423      0,410    0,912     0,465     U
                 0,199    0,029    0,188      0,199    0,193      0,165    0,768     0,127     MBP
Weighted Avg.    0,550    0,082    0,574      0,550    0,546      0,475    0,859     0,588     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   55   31    0    7    4    3    3    2    0    2 |    a = C
  184  750   26  130   67   47   71   75   46   42 |    b = E
   21   59   33   32   31   23   40    8    3   11 |    c = FV
  306  576   86 1748  197  194  211  168   42  160 |    d = AI
  295  385  169  407  482  183  717   74   63  158 |    e = CDGKNRSYZ
   84  168    6  187   68  535  145   61  101   22 |    f = O
   34  113   82  114  179   50 6104   59   14   45 |    g = 0
   55  109   43   36   62   54  200   43   24   51 |    h = LT
   31   38    4   28    8   42   23   28  201   20 |    i = U
   63  104   33   57   38   41   82   24   34  118 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
51.4	28.97	0.0	6.54	3.74	2.8	2.8	1.87	0.0	1.87	 a = C
12.8	52.16	1.81	9.04	4.66	3.27	4.94	5.22	3.2	2.92	 b = E
8.05	22.61	12.64	12.26	11.88	8.81	15.33	3.07	1.15	4.21	 c = FV
8.3	15.62	2.33	47.4	5.34	5.26	5.72	4.56	1.14	4.34	 d = AI
10.06	13.13	5.76	13.88	16.43	6.24	24.45	2.52	2.15	5.39	 e = CDGKNRSYZ
6.1	12.2	0.44	13.58	4.94	38.85	10.53	4.43	7.33	1.6	 f = O
0.5	1.66	1.21	1.68	2.63	0.74	89.84	0.87	0.21	0.66	 g = 0
8.12	16.1	6.35	5.32	9.16	7.98	29.54	6.35	3.55	7.53	 h = LT
7.33	8.98	0.95	6.62	1.89	9.93	5.44	6.62	47.52	4.73	 i = U
10.61	17.51	5.56	9.6	6.4	6.9	13.8	4.04	5.72	19.87	 j = MBP
