Timestamp:2013-08-30-05:05:06
Inventario:I3
Intervalo:10ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I3-10ms-3gramas
Num Instances:  18294
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(11): class 
0Pitch(Hz)(6): class 
0RawPitch(6): class 
0SmPitch(7): class 
0Melogram(st)(10): class 
0ZCross(9): class 
0F1(Hz)(12): class 
0F2(Hz)(12): class 
0F3(Hz)(9): class 
0F4(Hz)(12): class 
1Int(dB)(11): class 
1Pitch(Hz)(7): class 
1RawPitch(6): class 
1SmPitch(8): class 
1Melogram(st)(9): class 
1ZCross(10): class 
1F1(Hz)(14): class 
1F2(Hz)(12): class 
1F3(Hz)(9): class 
1F4(Hz)(12): class 
2Int(dB)(12): class 
2Pitch(Hz)(7): class 
2RawPitch(7): class 
2SmPitch(9): class 
2Melogram(st)(9): class 
2ZCross(12): class 
2F1(Hz)(12): class 
2F2(Hz)(13): class 
2F3(Hz)(10): class 
2F4(Hz)(8): class 
class(11): 
LogScore Bayes: -717046.4349468062
LogScore BDeu: -730507.9185523556
LogScore MDL: -729359.8339118763
LogScore ENTROPY: -715222.2938283167
LogScore AIC: -718103.2938283167




=== Stratified cross-validation ===

Correctly Classified Instances       10540               57.6145 %
Incorrectly Classified Instances      7754               42.3855 %
Kappa statistic                          0.4645
K&B Relative Info Score             867966.9224 %
K&B Information Score                22866.9307 bits      1.25   bits/instance
Class complexity | order 0           48173.9135 bits      2.6333 bits/instance
Class complexity | scheme           182040.2615 bits      9.9508 bits/instance
Complexity improvement     (Sf)    -133866.348  bits     -7.3175 bits/instance
Mean absolute error                      0.0775
Root mean squared error                  0.2595
Relative absolute error                 54.2907 %
Root relative squared error             97.1153 %
Coverage of cases (0.95 level)          64.8081 %
Mean rel. region size (0.95 level)      13.0336 %
Total Number of Instances            18294     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,607    0,063    0,650      0,607    0,628      0,560    0,897     0,710     A
                 0,546    0,062    0,429      0,546    0,481      0,434    0,881     0,468     E
                 0,155    0,047    0,434      0,155    0,228      0,168    0,789     0,395     CGJLNQSRT
                 0,115    0,018    0,085      0,115    0,098      0,084    0,784     0,061     FV
                 0,632    0,040    0,401      0,632    0,490      0,477    0,917     0,452     I
                 0,273    0,020    0,530      0,273    0,360      0,346    0,832     0,408     O
                 0,910    0,129    0,806      0,910    0,855      0,764    0,962     0,954     0
                 0,037    0,005    0,216      0,037    0,063      0,077    0,772     0,103     BMP
                 0,470    0,021    0,345      0,470    0,398      0,386    0,919     0,475     U
                 0,125    0,009    0,006      0,125    0,012      0,026    0,931     0,004     DZ
                 0,583    0,088    0,089      0,583    0,154      0,202    0,879     0,106     D
Weighted Avg.    0,576    0,077    0,593      0,576    0,561      0,501    0,890     0,646     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1785  230  184   57   68  103  131    5   20    9  348 |    a = A
  145  785  109   15  112   23   60   10   47    4  128 |    b = E
  346  335  532  114  268   85  970   29  120   76  568 |    c = CGJLNQSRT
   25   34   24   30   18   15   47    3    6    8   51 |    d = FV
   14  112   61    7  473    0   40    7    8    6   20 |    e = I
  228  127   93   16   37  376  131    7  104   15  243 |    f = O
   83   78  134   79   72   32 6182   11   15   28   82 |    g = 0
   58   76   52   24  113   23   86   22   47    6   87 |    h = BMP
   29   33   24    5    7   43   13    4  199    3   63 |    i = U
    0    0    0    0    0    0    7    0    0    1    0 |    j = DZ
   35   18   14    5   13   10    0    4   11    1  155 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
60.71	7.82	6.26	1.94	2.31	3.5	4.46	0.17	0.68	0.31	11.84	 a = A
10.08	54.59	7.58	1.04	7.79	1.6	4.17	0.7	3.27	0.28	8.9	 b = E
10.05	9.73	15.45	3.31	7.78	2.47	28.17	0.84	3.49	2.21	16.5	 c = CGJLNQSRT
9.58	13.03	9.2	11.49	6.9	5.75	18.01	1.15	2.3	3.07	19.54	 d = FV
1.87	14.97	8.16	0.94	63.24	0.0	5.35	0.94	1.07	0.8	2.67	 e = I
16.56	9.22	6.75	1.16	2.69	27.31	9.51	0.51	7.55	1.09	17.65	 f = O
1.22	1.15	1.97	1.16	1.06	0.47	90.97	0.16	0.22	0.41	1.21	 g = 0
9.76	12.79	8.75	4.04	19.02	3.87	14.48	3.7	7.91	1.01	14.65	 h = BMP
6.86	7.8	5.67	1.18	1.65	10.17	3.07	0.95	47.04	0.71	14.89	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	87.5	0.0	0.0	12.5	0.0	 j = DZ
13.16	6.77	5.26	1.88	4.89	3.76	0.0	1.5	4.14	0.38	58.27	 k = D
