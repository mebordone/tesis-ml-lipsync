Timestamp:2013-08-30-05:22:33
Inventario:I4
Intervalo:2ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I4-2ms-5gramas
Num Instances:  91699
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(24): class 
0Pitch(Hz)(45): class 
0RawPitch(29): class 
0SmPitch(38): class 
0Melogram(st)(45): class 
0ZCross(17): class 
0F1(Hz)(1256): class 
0F2(Hz)(1962): class 
0F3(Hz)(2074): class 
0F4(Hz)(1969): class 
1Int(dB)(25): class 
1Pitch(Hz)(45): class 
1RawPitch(32): class 
1SmPitch(36): class 
1Melogram(st)(48): class 
1ZCross(17): class 
1F1(Hz)(1227): class 
1F2(Hz)(2144): class 
1F3(Hz)(2236): class 
1F4(Hz)(2060): class 
2Int(dB)(24): class 
2Pitch(Hz)(44): class 
2RawPitch(38): class 
2SmPitch(38): class 
2Melogram(st)(49): class 
2ZCross(17): class 
2F1(Hz)(1443): class 
2F2(Hz)(2097): class 
2F3(Hz)(2201): class 
2F4(Hz)(1978): class 
3Int(dB)(24): class 
3Pitch(Hz)(45): class 
3RawPitch(33): class 
3SmPitch(38): class 
3Melogram(st)(47): class 
3ZCross(18): class 
3F1(Hz)(1457): class 
3F2(Hz)(1987): class 
3F3(Hz)(1986): class 
3F4(Hz)(2005): class 
4Int(dB)(22): class 
4Pitch(Hz)(47): class 
4RawPitch(34): class 
4SmPitch(39): class 
4Melogram(st)(45): class 
4ZCross(18): class 
4F1(Hz)(1497): class 
4F2(Hz)(1915): class 
4F3(Hz)(1983): class 
4F4(Hz)(2163): class 
class(10): 
LogScore Bayes: -1.2585857092388425E7
LogScore BDeu: -1.724579837795863E7
LogScore MDL: -1.6081642401868712E7
LogScore ENTROPY: -1.387569305565639E7
LogScore AIC: -1.426181205565639E7




=== Stratified cross-validation ===

Correctly Classified Instances       70152               76.5025 %
Incorrectly Classified Instances     21547               23.4975 %
Kappa statistic                          0.6921
K&B Relative Info Score            6516446.7101 %
K&B Information Score               166097.8977 bits      1.8113 bits/instance
Class complexity | order 0          233711.6934 bits      2.5487 bits/instance
Class complexity | scheme          1156460.8919 bits     12.6115 bits/instance
Complexity improvement     (Sf)    -922749.1985 bits    -10.0628 bits/instance
Mean absolute error                      0.0471
Root mean squared error                  0.2117
Relative absolute error                 30.5434 %
Root relative squared error             76.2237 %
Coverage of cases (0.95 level)          78.5254 %
Mean rel. region size (0.95 level)      10.7407 %
Total Number of Instances            91699     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,860    0,019    0,211      0,860    0,338      0,420    0,955     0,687     C
                 0,860    0,033    0,679      0,860    0,759      0,743    0,958     0,848     E
                 0,654    0,010    0,489      0,654    0,560      0,559    0,907     0,639     FV
                 0,804    0,025    0,884      0,804    0,842      0,807    0,955     0,905     AI
                 0,376    0,020    0,775      0,376    0,506      0,488    0,932     0,722     CDGKNRSYZ
                 0,794    0,014    0,815      0,794    0,804      0,789    0,925     0,836     O
                 0,902    0,157    0,786      0,902    0,840      0,730    0,955     0,913     0
                 0,494    0,009    0,681      0,494    0,573      0,567    0,903     0,574     LT
                 0,918    0,011    0,651      0,918    0,762      0,767    0,975     0,915     U
                 0,698    0,010    0,694      0,698    0,696      0,686    0,901     0,722     MBP
Weighted Avg.    0,765    0,074    0,780      0,765    0,757      0,702    0,946     0,848     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   468    24     0     2     9     1    35     1     4     0 |     a = C
   144  5940    51    81   221    14   297    70    30    62 |     b = E
    16    32   841    19    20    16   318    11     6     7 |     c = FV
   431   605   221 14378   478   213  1007   214   132   215 |     d = AI
   633  1249   107   918  5361   444  4706   116   485   253 |     e = CDGKNRSYZ
   169   118    27   109   101  5275   725    37    38    47 |     f = O
   194   585   448   635   452   444 32303   267   222   279 |     g = 0
    96   127    11    64   170    43  1086  1642    68    16 |     h = LT
    15     5     2     1    50     2    49    28  1910    18 |     i = U
    56    62    11    52    58    23   554    24    41  2034 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
86.03	4.41	0.0	0.37	1.65	0.18	6.43	0.18	0.74	0.0	 a = C
2.08	85.96	0.74	1.17	3.2	0.2	4.3	1.01	0.43	0.9	 b = E
1.24	2.49	65.4	1.48	1.56	1.24	24.73	0.86	0.47	0.54	 c = FV
2.41	3.38	1.24	80.35	2.67	1.19	5.63	1.2	0.74	1.2	 d = AI
4.44	8.75	0.75	6.43	37.56	3.11	32.97	0.81	3.4	1.77	 e = CDGKNRSYZ
2.54	1.78	0.41	1.64	1.52	79.37	10.91	0.56	0.57	0.71	 f = O
0.54	1.63	1.25	1.77	1.26	1.24	90.16	0.75	0.62	0.78	 g = 0
2.89	3.82	0.33	1.93	5.12	1.29	32.68	49.41	2.05	0.48	 h = LT
0.72	0.24	0.1	0.05	2.4	0.1	2.36	1.35	91.83	0.87	 i = U
1.92	2.13	0.38	1.78	1.99	0.79	19.01	0.82	1.41	69.78	 j = MBP
