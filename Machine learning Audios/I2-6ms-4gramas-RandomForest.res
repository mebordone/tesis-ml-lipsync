Timestamp:2013-07-23-03:00:08
Inventario:I2
Intervalo:6ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I2-6ms-4gramas
Num Instances:  30450
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.2457





=== Stratified cross-validation ===

Correctly Classified Instances       24836               81.5632 %
Incorrectly Classified Instances      5614               18.4368 %
Kappa statistic                          0.7686
K&B Relative Info Score            2216162.9535 %
K&B Information Score                63973.1854 bits      2.1009 bits/instance
Class complexity | order 0           87885.5941 bits      2.8862 bits/instance
Class complexity | scheme          1538578.7519 bits     50.528  bits/instance
Complexity improvement     (Sf)    -1450693.1578 bits    -47.6418 bits/instance
Mean absolute error                      0.0505
Root mean squared error                  0.1533
Relative absolute error                 37.7891 %
Root relative squared error             59.3267 %
Coverage of cases (0.95 level)          95.353  %
Mean rel. region size (0.95 level)      22.301  %
Total Number of Instances            30450     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,887    0,040    0,805      0,887    0,844      0,815    0,967     0,912     A
                 0,554    0,016    0,642      0,554    0,595      0,577    0,894     0,582     LGJKC
                 0,821    0,023    0,748      0,821    0,783      0,765    0,962     0,834     E
                 0,437    0,003    0,681      0,437    0,533      0,541    0,897     0,506     FV
                 0,787    0,008    0,796      0,787    0,791      0,783    0,961     0,829     I
                 0,775    0,016    0,791      0,775    0,783      0,766    0,940     0,813     O
                 0,661    0,040    0,634      0,661    0,647      0,609    0,927     0,671     SNRTY
                 0,919    0,059    0,905      0,919    0,912      0,857    0,974     0,956     0
                 0,605    0,005    0,797      0,605    0,688      0,686    0,914     0,682     BMP
                 0,638    0,002    0,830      0,638    0,722      0,724    0,966     0,730     DZ
                 0,799    0,002    0,909      0,799    0,850      0,849    0,973     0,873     U
                 0,717    0,006    0,832      0,717    0,770      0,763    0,955     0,790     SNRTXY
Weighted Avg.    0,816    0,038    0,815      0,816    0,813      0,780    0,956     0,851     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  4282    61    95     7    22    49   126   127    15    10     6    25 |     a = A
   136   828    54    11    26    42   173   181    16     1     7    20 |     b = LGJKC
   115    45  1926     5    37    32    67    58    17     8     7    30 |     c = E
    39    19    33   188     7    21    73    29     8     2     4     7 |     d = FV
    27    21    93     4   949     8    45    40    12     0     3     4 |     e = I
   129    24    46     5     9  1744    61   179    14    13     8    19 |     f = O
   225   110    87    23    47    71  1924   365    21     7     6    26 |     g = SNRTY
   177    91   100    13    46    99   346 10663    31     8     7    27 |     h = 0
    61    29    34    12    32    38    91    82   593     2     4     2 |     i = BMP
    36     9    19     2     4    51    17     9     0   289     2    15 |     j = DZ
    26    20    23     2     2    30    14    10     8     0   557     5 |     k = U
    63    33    64     4    11    20   100    39     9     8     2   893 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
88.75	1.26	1.97	0.15	0.46	1.02	2.61	2.63	0.31	0.21	0.12	0.52	 a = A
9.1	55.38	3.61	0.74	1.74	2.81	11.57	12.11	1.07	0.07	0.47	1.34	 b = LGJKC
4.9	1.92	82.06	0.21	1.58	1.36	2.85	2.47	0.72	0.34	0.3	1.28	 c = E
9.07	4.42	7.67	43.72	1.63	4.88	16.98	6.74	1.86	0.47	0.93	1.63	 d = FV
2.24	1.74	7.71	0.33	78.69	0.66	3.73	3.32	1.0	0.0	0.25	0.33	 e = I
5.73	1.07	2.04	0.22	0.4	77.48	2.71	7.95	0.62	0.58	0.36	0.84	 f = O
7.73	3.78	2.99	0.79	1.61	2.44	66.07	12.53	0.72	0.24	0.21	0.89	 g = SNRTY
1.52	0.78	0.86	0.11	0.4	0.85	2.98	91.86	0.27	0.07	0.06	0.23	 h = 0
6.22	2.96	3.47	1.22	3.27	3.88	9.29	8.37	60.51	0.2	0.41	0.2	 i = BMP
7.95	1.99	4.19	0.44	0.88	11.26	3.75	1.99	0.0	63.8	0.44	3.31	 j = DZ
3.73	2.87	3.3	0.29	0.29	4.3	2.01	1.43	1.15	0.0	79.91	0.72	 k = U
5.06	2.65	5.14	0.32	0.88	1.61	8.03	3.13	0.72	0.64	0.16	71.67	 l = SNRTXY
