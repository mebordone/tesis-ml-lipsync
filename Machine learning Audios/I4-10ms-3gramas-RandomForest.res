Timestamp:2013-07-22-23:04:52
Inventario:I4
Intervalo:10ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I4-10ms-3gramas
Num Instances:  18294
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.292





=== Stratified cross-validation ===

Correctly Classified Instances       14063               76.8722 %
Incorrectly Classified Instances      4231               23.1278 %
Kappa statistic                          0.7011
K&B Relative Info Score            1204821.4707 %
K&B Information Score                31123.3478 bits      1.7013 bits/instance
Class complexity | order 0           47240.5333 bits      2.5823 bits/instance
Class complexity | scheme          1012909.6611 bits     55.3684 bits/instance
Complexity improvement     (Sf)    -965669.1278 bits    -52.7861 bits/instance
Mean absolute error                      0.0676
Root mean squared error                  0.1821
Relative absolute error                 43.2708 %
Root relative squared error             65.1787 %
Coverage of cases (0.95 level)          94.9109 %
Mean rel. region size (0.95 level)      26.8979 %
Total Number of Instances            18294     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,523    0,001    0,683      0,523    0,593      0,596    0,924     0,542     C
                 0,737    0,034    0,651      0,737    0,691      0,665    0,942     0,729     E
                 0,245    0,004    0,496      0,245    0,328      0,342    0,837     0,265     FV
                 0,827    0,071    0,745      0,827    0,784      0,727    0,947     0,860     AI
                 0,692    0,084    0,613      0,692    0,650      0,580    0,908     0,688     CDGKNRSYZ
                 0,649    0,020    0,728      0,649    0,686      0,664    0,920     0,710     O
                 0,911    0,054    0,909      0,911    0,910      0,856    0,973     0,954     0
                 0,284    0,009    0,561      0,284    0,377      0,383    0,848     0,333     LT
                 0,664    0,003    0,841      0,664    0,742      0,742    0,949     0,767     U
                 0,414    0,005    0,739      0,414    0,531      0,543    0,881     0,523     MBP
Weighted Avg.    0,769    0,053    0,767      0,769    0,763      0,717    0,940     0,803     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   56   12    0   18   12    4    4    0    1    0 |    a = C
    9 1060    7  162  127   24   29   11    1    8 |    b = E
    1   32   64   24  102   12   13    6    1    6 |    c = FV
    7  171   11 3050  253   46  106   23   10   11 |    d = AI
    1  160   28  333 2031   98  208   45   10   19 |    e = CDGKNRSYZ
    3   46    5  124  155  894  105   18    9   18 |    f = O
    2   48    4  143  311   59 6189   27    5    8 |    g = 0
    0   38    3  113  182   29  106  192    6    8 |    h = LT
    2   22    2   23   41   33    4    6  281    9 |    i = U
    1   40    5  103  100   29   46   14   10  246 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
52.34	11.21	0.0	16.82	11.21	3.74	3.74	0.0	0.93	0.0	 a = C
0.63	73.71	0.49	11.27	8.83	1.67	2.02	0.76	0.07	0.56	 b = E
0.38	12.26	24.52	9.2	39.08	4.6	4.98	2.3	0.38	2.3	 c = FV
0.19	4.64	0.3	82.7	6.86	1.25	2.87	0.62	0.27	0.3	 d = AI
0.03	5.46	0.95	11.35	69.25	3.34	7.09	1.53	0.34	0.65	 e = CDGKNRSYZ
0.22	3.34	0.36	9.01	11.26	64.92	7.63	1.31	0.65	1.31	 f = O
0.03	0.71	0.06	2.1	4.58	0.87	91.07	0.4	0.07	0.12	 g = 0
0.0	5.61	0.44	16.69	26.88	4.28	15.66	28.36	0.89	1.18	 h = LT
0.47	5.2	0.47	5.44	9.69	7.8	0.95	1.42	66.43	2.13	 i = U
0.17	6.73	0.84	17.34	16.84	4.88	7.74	2.36	1.68	41.41	 j = MBP
