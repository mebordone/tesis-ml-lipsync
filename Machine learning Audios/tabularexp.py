#!/usr/bin/python
# -*- coding: utf8 -*-
import sys, os
import re
from pprint import pprint

timestamp = re.compile(r'Timestamp:(.*?)\n')
inventario = re.compile(r'Inventario:(.*?)\n')
intervalo = re.compile(r'Intervalo:(.*?)\n')
ngramas = re.compile(r'Ngramas:(.*?)\n')
clasificador = re.compile(r'Clasificador:(.*?)\n')
numinstancias = re.compile(r'Num Instances:  (.*?)\n')
correctos = re.compile(r'Correctly Classified Instances\s+\d+\s+(.*?) %\n')
incorrectos = re.compile(r'Incorrectly Classified Instances\s+\d+\s+(.*?) %\n')
kappa = re.compile(r'Kappa statistic\s+(.*?)\n')
clasificacion = re.compile(r'Weighted Avg.\s+(.*?)\s+\n')


out = open ('tabladeexperimentos.csv','w')
for file in os.listdir(os.getcwd()):
	if '.res' in file:
		resfile = open (file,'r')
		res = resfile.read()
		resfile.close()
		tableline = []
		tableline.append(timestamp.search(res).groups()[0])
		tableline.append(inventario.search(res).groups()[0])
		tableline.append(intervalo.search(res).groups()[0])
		tableline.append(ngramas.search(res).groups()[0])
		tableline.append(clasificador.search(res).groups()[0])
		tableline.append(numinstancias.search(res).groups()[0])
		#~ tableline.append(float(correctos.search(res).groups()[0]))
		#~ tableline.append(float(incorrectos.search(res).groups()[0]))
		#~ tableline.append(float(kappa.search(res).groups()[0]))
		tableline.append(correctos.search(res).groups()[0].replace('.',','))
		tableline.append(incorrectos.search(res).groups()[0].replace('.',','))
		tableline.append(kappa.search(res).groups()[0].replace('.',','))
		#~ tableline = tableline + [float (i.replace(',','.')) for i in (clasificacion.search(res).groups()[0].split())]
		tableline = tableline + clasificacion.search(res).groups()[0].split()
		#~ print ';'.join(tableline)
		#~ out.write(';'.join(['{}'.format(i) for i in(tableline)]))
		out.write(';'.join(tableline))
		out.write('\n')
out.close()
