Timestamp:2013-07-22-21:41:00
Inventario:I2
Intervalo:2ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I2-2ms-1gramas
Num Instances:  91703
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1379





=== Stratified cross-validation ===

Correctly Classified Instances       81198               88.5445 %
Incorrectly Classified Instances     10505               11.4555 %
Kappa statistic                          0.854 
K&B Relative Info Score            7578921.7747 %
K&B Information Score               217010.6014 bits      2.3665 bits/instance
Class complexity | order 0          262556.643  bits      2.8631 bits/instance
Class complexity | scheme          1709390.564  bits     18.6405 bits/instance
Complexity improvement     (Sf)    -1446833.921 bits    -15.7774 bits/instance
Mean absolute error                      0.0313
Root mean squared error                  0.1205
Relative absolute error                 23.5985 %
Root relative squared error             46.7873 %
Coverage of cases (0.95 level)          97.3916 %
Mean rel. region size (0.95 level)      18.0966 %
Total Number of Instances            91703     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,923    0,011    0,942      0,923    0,932      0,920    0,984     0,962     A
                 0,657    0,006    0,840      0,657    0,737      0,731    0,946     0,761     LGJKC
                 0,912    0,006    0,925      0,912    0,918      0,912    0,986     0,949     E
                 0,663    0,001    0,910      0,663    0,767      0,774    0,966     0,767     FV
                 0,900    0,003    0,927      0,900    0,913      0,910    0,978     0,931     I
                 0,847    0,004    0,939      0,847    0,890      0,884    0,977     0,912     O
                 0,737    0,037    0,677      0,737    0,706      0,674    0,965     0,792     SNRTY
                 0,959    0,077    0,889      0,959    0,923      0,872    0,987     0,980     0
                 0,733    0,002    0,922      0,733    0,817      0,817    0,961     0,832     BMP
                 0,863    0,001    0,940      0,863    0,900      0,899    0,986     0,926     DZ
                 0,927    0,001    0,967      0,927    0,946      0,945    0,986     0,960     U
                 0,835    0,003    0,927      0,835    0,879      0,875    0,964     0,883     SNRTXY
Weighted Avg.    0,885    0,037    0,888      0,885    0,885      0,858    0,979     0,927     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 13226    69    36     8    28    36   317   543    22    10     5    33 |     a = A
   126  2908    49     5    21    32   640   602    13     2    12    18 |     b = LGJKC
    47    50  6302     9    22    21   172   206    19    11    11    40 |     c = E
    21     9    26   852    10    14   265    84     2     0     0     3 |     d = FV
    30    24    30    10  3205     6    71   162    16     0     3     4 |     e = I
    69    40    29    12    12  5627   173   623    17    20     3    21 |     f = O
   220   185    93    12    41    75  6381  1540    33     3     8    72 |     g = SNRTY
   166   106   118    15    71    68   814 34377    35    19    13    31 |     h = 0
    49    21    36     7    32    35   257   330  2138     0     5     5 |     i = BMP
    26     3    34     0     1    37    35    27     3  1162     3    15 |     j = DZ
    11    21    15     3     9    17    25    27    18     3  1928     3 |     k = U
    51    27    45     3     4    27   274   168     2     6     3  3092 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
92.28	0.48	0.25	0.06	0.2	0.25	2.21	3.79	0.15	0.07	0.03	0.23	 a = A
2.85	65.67	1.11	0.11	0.47	0.72	14.45	13.6	0.29	0.05	0.27	0.41	 b = LGJKC
0.68	0.72	91.2	0.13	0.32	0.3	2.49	2.98	0.27	0.16	0.16	0.58	 c = E
1.63	0.7	2.02	66.25	0.78	1.09	20.61	6.53	0.16	0.0	0.0	0.23	 d = FV
0.84	0.67	0.84	0.28	90.0	0.17	1.99	4.55	0.45	0.0	0.08	0.11	 e = I
1.04	0.6	0.44	0.18	0.18	84.67	2.6	9.37	0.26	0.3	0.05	0.32	 f = O
2.54	2.14	1.07	0.14	0.47	0.87	73.66	17.78	0.38	0.03	0.09	0.83	 g = SNRTY
0.46	0.3	0.33	0.04	0.2	0.19	2.27	95.94	0.1	0.05	0.04	0.09	 h = 0
1.68	0.72	1.23	0.24	1.1	1.2	8.82	11.32	73.34	0.0	0.17	0.17	 i = BMP
1.93	0.22	2.53	0.0	0.07	2.75	2.6	2.01	0.22	86.33	0.22	1.11	 j = DZ
0.53	1.01	0.72	0.14	0.43	0.82	1.2	1.3	0.87	0.14	92.69	0.14	 k = U
1.38	0.73	1.22	0.08	0.11	0.73	7.4	4.54	0.05	0.16	0.08	83.52	 l = SNRTXY
