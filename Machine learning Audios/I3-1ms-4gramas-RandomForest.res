Timestamp:2013-07-22-22:08:11
Inventario:I3
Intervalo:1ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I3-1ms-4gramas
Num Instances:  183401
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.111





=== Stratified cross-validation ===

Correctly Classified Instances      167478               91.3179 %
Incorrectly Classified Instances     15923                8.6821 %
Kappa statistic                          0.8873
K&B Relative Info Score            16029724.486 %
K&B Information Score               415043.7863 bits      2.263  bits/instance
Class complexity | order 0          474842.223  bits      2.5891 bits/instance
Class complexity | scheme          5644752.4425 bits     30.7782 bits/instance
Complexity improvement     (Sf)    -5169910.2195 bits    -28.1891 bits/instance
Mean absolute error                      0.0238
Root mean squared error                  0.1099
Relative absolute error                 16.8909 %
Root relative squared error             41.4282 %
Coverage of cases (0.95 level)          97.1167 %
Mean rel. region size (0.95 level)      14.2156 %
Total Number of Instances           183401     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,940    0,009    0,951      0,940    0,946      0,936    0,981     0,965     A
                 0,940    0,004    0,949      0,940    0,945      0,941    0,983     0,962     E
                 0,869    0,042    0,822      0,869    0,845      0,809    0,970     0,910     CGJLNQSRT
                 0,723    0,002    0,835      0,723    0,775      0,774    0,926     0,781     FV
                 0,922    0,002    0,956      0,922    0,939      0,936    0,977     0,948     I
                 0,874    0,005    0,936      0,874    0,904      0,897    0,958     0,912     O
                 0,938    0,049    0,926      0,938    0,932      0,887    0,984     0,968     0
                 0,786    0,003    0,904      0,786    0,841      0,838    0,944     0,849     BMP
                 0,954    0,000    0,985      0,954    0,970      0,969    0,993     0,981     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,522     0,001     DZ
                 0,955    0,000    0,972      0,955    0,963      0,963    0,996     0,984     D
Weighted Avg.    0,913    0,029    0,914      0,913    0,913      0,887    0,977     0,945     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 26850    43   841    25    26    41   672    43     6     2     8 |     a = A
    60 12922   427    16    13    29   229    28     3     0    13 |     b = E
   583   268 29097   203    76   206  2825   175    23    16     7 |     c = CGJLNQSRT
    36    26   501  1852     7    23    96    18     0     1     1 |     d = FV
    40    29   266    13  6529     8   184    10     3     0     1 |     e = I
    82    34   481    21     9 11560   979    39     4     1    24 |     f = O
   457   202  3032    62   117   401 67651   157    13     5    17 |     g = 0
    89    56   601    17    39    44   389  4571     7     1     0 |     h = BMP
    20    11    94     1     9    10    32    12  3948     0     0 |     i = U
     3     1    42     6     0     1    10     2     0     0     0 |     j = DZ
    18    18    33     1     5    31    13     0     0     0  2498 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
94.02	0.15	2.94	0.09	0.09	0.14	2.35	0.15	0.02	0.01	0.03	 a = A
0.44	94.05	3.11	0.12	0.09	0.21	1.67	0.2	0.02	0.0	0.09	 b = E
1.74	0.8	86.91	0.61	0.23	0.62	8.44	0.52	0.07	0.05	0.02	 c = CGJLNQSRT
1.41	1.02	19.56	72.32	0.27	0.9	3.75	0.7	0.0	0.04	0.04	 d = FV
0.56	0.41	3.76	0.18	92.18	0.11	2.6	0.14	0.04	0.0	0.01	 e = I
0.62	0.26	3.63	0.16	0.07	87.35	7.4	0.29	0.03	0.01	0.18	 f = O
0.63	0.28	4.2	0.09	0.16	0.56	93.81	0.22	0.02	0.01	0.02	 g = 0
1.53	0.96	10.34	0.29	0.67	0.76	6.69	78.62	0.12	0.02	0.0	 h = BMP
0.48	0.27	2.27	0.02	0.22	0.24	0.77	0.29	95.43	0.0	0.0	 i = U
4.62	1.54	64.62	9.23	0.0	1.54	15.38	3.08	0.0	0.0	0.0	 j = DZ
0.69	0.69	1.26	0.04	0.19	1.18	0.5	0.0	0.0	0.0	95.45	 k = D
