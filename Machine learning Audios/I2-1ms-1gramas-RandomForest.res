Timestamp:2013-07-22-21:23:31
Inventario:I2
Intervalo:1ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I2-1ms-1gramas
Num Instances:  183404
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1074





=== Stratified cross-validation ===

Correctly Classified Instances      166659               90.8699 %
Incorrectly Classified Instances     16745                9.1301 %
Kappa statistic                          0.8834
K&B Relative Info Score            15823928.9802 %
K&B Information Score               452096.317  bits      2.465  bits/instance
Class complexity | order 0          523971.7337 bits      2.8569 bits/instance
Class complexity | scheme          1738365.3668 bits      9.4783 bits/instance
Complexity improvement     (Sf)    -1214393.6331 bits     -6.6214 bits/instance
Mean absolute error                      0.0239
Root mean squared error                  0.1061
Relative absolute error                 18.0784 %
Root relative squared error             41.2477 %
Coverage of cases (0.95 level)          98.2132 %
Mean rel. region size (0.95 level)      16.4438 %
Total Number of Instances           183404     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,937    0,004    0,975      0,937    0,955      0,948    0,991     0,976     A
                 0,700    0,004    0,904      0,700    0,789      0,787    0,963     0,818     LGJKC
                 0,941    0,003    0,964      0,941    0,952      0,949    0,992     0,971     E
                 0,708    0,000    0,962      0,708    0,816      0,823    0,980     0,816     FV
                 0,923    0,001    0,966      0,923    0,944      0,942    0,987     0,954     I
                 0,873    0,002    0,974      0,873    0,920      0,916    0,985     0,935     O
                 0,782    0,033    0,712      0,782    0,746      0,719    0,976     0,850     SNRTY
                 0,970    0,074    0,894      0,970    0,931      0,885    0,991     0,985     0
                 0,780    0,001    0,967      0,780    0,863      0,864    0,977     0,873     BMP
                 0,933    0,000    0,972      0,933    0,952      0,952    0,992     0,965     DZ
                 0,961    0,001    0,978      0,961    0,969      0,968    0,991     0,979     U
                 0,865    0,001    0,962      0,865    0,911      0,909    0,980     0,923     SNRTXY
Weighted Avg.    0,909    0,034    0,913      0,909    0,908      0,886    0,987     0,948     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 26753    85    32     3    20    29   522  1030    30     8     7    38 |     a = A
   103  6181    65     7    19    30  1156  1202    12     1    24    27 |     b = LGJKC
    33    50 12935    12    14    18   237   375    20     7     5    34 |     c = E
    15     9    22  1814    14     6   531   145     2     0     1     2 |     d = FV
    29    29    28    11  6537     8   110   307    12     1     8     3 |     e = I
    50    25    28     7    13 11549   288  1219    15    20     2    18 |     f = O
   157   252    85    17    52    66 13515  2997    21     3    14    94 |     g = SNRTY
   164   114   108    12    57    58  1537 69980    30    23    15    19 |     h = 0
    50    34    35     2    27    36   441   643  4533     0     8     5 |     i = BMP
    18     1    20     0     2    25    78    25     0  2503     2     8 |     j = DZ
    11    21    10     1     5    12    41    42    12     3  3974     5 |     k = U
    58    39    56     0     5    23   525   273     3     7     5  6385 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
93.68	0.3	0.11	0.01	0.07	0.1	1.83	3.61	0.11	0.03	0.02	0.13	 a = A
1.17	70.02	0.74	0.08	0.22	0.34	13.1	13.62	0.14	0.01	0.27	0.31	 b = LGJKC
0.24	0.36	94.14	0.09	0.1	0.13	1.72	2.73	0.15	0.05	0.04	0.25	 c = E
0.59	0.35	0.86	70.83	0.55	0.23	20.73	5.66	0.08	0.0	0.04	0.08	 d = FV
0.41	0.41	0.4	0.16	92.29	0.11	1.55	4.33	0.17	0.01	0.11	0.04	 e = I
0.38	0.19	0.21	0.05	0.1	87.27	2.18	9.21	0.11	0.15	0.02	0.14	 f = O
0.91	1.46	0.49	0.1	0.3	0.38	78.24	17.35	0.12	0.02	0.08	0.54	 g = SNRTY
0.23	0.16	0.15	0.02	0.08	0.08	2.13	97.04	0.04	0.03	0.02	0.03	 h = 0
0.86	0.58	0.6	0.03	0.46	0.62	7.59	11.06	77.97	0.0	0.14	0.09	 i = BMP
0.67	0.04	0.75	0.0	0.07	0.93	2.91	0.93	0.0	93.33	0.07	0.3	 j = DZ
0.27	0.51	0.24	0.02	0.12	0.29	0.99	1.02	0.29	0.07	96.06	0.12	 k = U
0.79	0.53	0.76	0.0	0.07	0.31	7.11	3.7	0.04	0.09	0.07	86.53	 l = SNRTXY
