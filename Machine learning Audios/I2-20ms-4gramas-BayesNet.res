Timestamp:2013-08-30-04:43:10
Inventario:I2
Intervalo:20ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I2-20ms-4gramas
Num Instances:  9126
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(4): class 
0F1(Hz)(8): class 
0F2(Hz)(5): class 
0F3(Hz)(2): class 
0F4(Hz)(3): class 
1Int(dB)(9): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(4): class 
1F1(Hz)(8): class 
1F2(Hz)(8): class 
1F3(Hz)(2): class 
1F4(Hz)(3): class 
2Int(dB)(8): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(4): class 
2F1(Hz)(9): class 
2F2(Hz)(8): class 
2F3(Hz)(2): class 
2F4(Hz)(3): class 
3Int(dB)(9): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(8): class 
3F1(Hz)(9): class 
3F2(Hz)(9): class 
3F3(Hz)(5): class 
3F4(Hz)(5): class 
class(12): 
LogScore Bayes: -345966.9061140442
LogScore BDeu: -354633.31405003107
LogScore MDL: -353723.6236677357
LogScore ENTROPY: -344098.64291298576
LogScore AIC: -346209.64291298576




=== Stratified cross-validation ===

Correctly Classified Instances        4829               52.9147 %
Incorrectly Classified Instances      4297               47.0853 %
Kappa statistic                          0.4253
K&B Relative Info Score             379475.1613 %
K&B Information Score                11193.1157 bits      1.2265 bits/instance
Class complexity | order 0           26898.0483 bits      2.9474 bits/instance
Class complexity | scheme           100327.9446 bits     10.9936 bits/instance
Complexity improvement     (Sf)     -73429.8963 bits     -8.0462 bits/instance
Mean absolute error                      0.0792
Root mean squared error                  0.2567
Relative absolute error                 58.2023 %
Root relative squared error             98.4201 %
Coverage of cases (0.95 level)          62.1302 %
Mean rel. region size (0.95 level)      13.9318 %
Total Number of Instances             9126     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,486    0,054    0,640      0,486    0,553      0,485    0,870     0,635     A
                 0,004    0,003    0,063      0,004    0,008      0,004    0,684     0,079     LGJKC
                 0,316    0,034    0,456      0,316    0,373      0,335    0,859     0,385     E
                 0,065    0,018    0,053      0,065    0,058      0,042    0,772     0,042     FV
                 0,553    0,044    0,354      0,553    0,432      0,412    0,897     0,418     I
                 0,221    0,023    0,447      0,221    0,295      0,275    0,799     0,311     O
                 0,296    0,059    0,358      0,296    0,324      0,258    0,781     0,293     SNRTY
                 0,900    0,092    0,842      0,900    0,870      0,796    0,954     0,909     0
                 0,023    0,005    0,130      0,023    0,040      0,042    0,745     0,083     BMP
                 0,413    0,088    0,068      0,413    0,116      0,137    0,830     0,079     DZ
                 0,422    0,018    0,369      0,422    0,394      0,379    0,894     0,408     U
                 0,372    0,092    0,151      0,372    0,215      0,185    0,819     0,139     SNRTXY
Weighted Avg.    0,529    0,060    0,551      0,529    0,527      0,475    0,869     0,554     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
  732   10  109   28   42   95   73   76    2  195   21  122 |    a = A
   40    2   26   16   37    9   68   90    4   46    8  104 |    b = LGJKC
   69    7  236    6   95   18   29   39    6   97   25  119 |    c = E
   16    4   12    9    3    4   23    9    3   23    0   32 |    d = FV
    4    0   32    1  213    1   29   24    3   18    4   56 |    e = I
   97    4   36    7   27  157   38   79    1  151   56   59 |    f = O
   68    0   18   38   52   11  271  184   11  100   12  149 |    g = SNRTY
   14    2    8   38   27    9  142 2909   11   24    4   46 |    h = 0
   28    1    7   16   50    8   37   20    7   37   13   75 |    i = BMP
   20    0    6    2    4    2    9    3    1   57    6   28 |    j = DZ
   16    1    9    2   11   32    7    9    2   20   92   17 |    k = U
   40    1   19    8   40    5   31   12    3   76    8  144 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
48.64	0.66	7.24	1.86	2.79	6.31	4.85	5.05	0.13	12.96	1.4	8.11	 a = A
8.89	0.44	5.78	3.56	8.22	2.0	15.11	20.0	0.89	10.22	1.78	23.11	 b = LGJKC
9.25	0.94	31.64	0.8	12.73	2.41	3.89	5.23	0.8	13.0	3.35	15.95	 c = E
11.59	2.9	8.7	6.52	2.17	2.9	16.67	6.52	2.17	16.67	0.0	23.19	 d = FV
1.04	0.0	8.31	0.26	55.32	0.26	7.53	6.23	0.78	4.68	1.04	14.55	 e = I
13.62	0.56	5.06	0.98	3.79	22.05	5.34	11.1	0.14	21.21	7.87	8.29	 f = O
7.44	0.0	1.97	4.16	5.69	1.2	29.65	20.13	1.2	10.94	1.31	16.3	 g = SNRTY
0.43	0.06	0.25	1.18	0.83	0.28	4.39	89.95	0.34	0.74	0.12	1.42	 h = 0
9.36	0.33	2.34	5.35	16.72	2.68	12.37	6.69	2.34	12.37	4.35	25.08	 i = BMP
14.49	0.0	4.35	1.45	2.9	1.45	6.52	2.17	0.72	41.3	4.35	20.29	 j = DZ
7.34	0.46	4.13	0.92	5.05	14.68	3.21	4.13	0.92	9.17	42.2	7.8	 k = U
10.34	0.26	4.91	2.07	10.34	1.29	8.01	3.1	0.78	19.64	2.07	37.21	 l = SNRTXY
