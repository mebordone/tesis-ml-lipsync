Timestamp:2014-12-07-11:06:41
Inventario:I3
Intervalo:2ms
Ngramas:4
Clasificador:NaiveBayes
Relation Name:  I3-2ms-4gramas
Num Instances:  91700
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Naive Bayes Classifier

                    Class
Attribute               A         E CGJLNQSRT        FV         I         O         0       BMP         U        DZ         D
                   (0.16)    (0.08)    (0.18)    (0.01)    (0.04)    (0.07)    (0.39)    (0.03)    (0.02)       (0)    (0.01)
==============================================================================================================================
0Int(dB)
  mean             -4.2384   -5.2725  -16.7214  -14.9878   -8.8919    -8.154  -48.1014   -12.821    -5.542  -27.0297   -7.9123
  std. dev.         9.6628    8.2012   10.5677    9.0002    8.4155   12.1079   16.4153   10.9576    8.2546    2.1797    6.0943
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision          0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104

0Pitch(Hz)
  mean            215.2523  188.7056  114.6031   132.578  171.6528   179.115   17.4128  158.5726   227.888         0  202.4816
  std. dev.       108.4244   88.8894  112.9078  107.6288    77.843  105.6864    57.392  112.1031   87.0635    0.0207   84.0743
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242

0RawPitch
  mean            204.6555  184.6007   96.4484  107.2731   163.826  173.0518   14.2988  146.0307  218.9774         0  196.0974
  std. dev.       114.9714   95.7338  112.1676  104.2544   84.4289  113.1417   51.8247  117.1223   95.0579    0.0207   94.5196
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243

0SmPitch
  mean            213.1285  187.0281  108.1223  122.2039   169.294  176.9741   15.7543  154.4121  227.3868         0  201.7878
  std. dev.       110.1559   90.2619   112.967   104.892   80.1089  107.8927   53.6098  114.4599   87.2185    0.0208   85.4406
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248

0Melogram(st)
  mean             51.2208   50.1378   30.0176   37.3403   48.1087   46.5873     5.111   41.2076   53.4674         0   53.0329
  std. dev.        17.1425    15.863   27.4035   24.8755   16.7578   20.5055   15.8762    24.495   15.3794    0.0041   11.3319
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246

0ZCross
  mean              7.1251    5.2821     3.008    2.0824    3.7217    4.4284    0.2716     2.462    4.0615         0    4.0422
  std. dev.         4.5896    3.2153    4.9213    2.2428    3.2672    3.0869    1.1738    2.2685    2.9528    0.1792    2.3029
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755

0F1(Hz)
  mean            673.2103  458.0799  314.6773  393.0414  356.2436  478.8171   56.1152  372.9024  491.0591         0  516.4293
  std. dev.       277.2349   153.926  280.1525  278.0738  151.5566  229.7252    170.12  238.8814  315.0618    0.0608  153.3271
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648

0F2(Hz)
  mean           1433.1553 1757.7095  1017.288 1181.0302 1928.7407 1136.7974  175.3222  1310.527 1034.0495         0 1484.2771
  std. dev.       471.5399  560.9633  863.9258  771.3771  710.0757  547.7396  525.6561  755.0799  450.6215    0.0738  317.9388
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429

0F3(Hz)
  mean           2635.5247 2586.6864 1731.5084   2017.54 2604.0226 2478.8191  299.2665 2205.0132 2621.0935         0 2869.9416
  std. dev.       840.5679  831.7891 1447.3003  1304.859  977.1249 1044.4223  881.7239 1213.1591  864.0366    0.1103  490.8925
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision          0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662

0F4(Hz)
  mean           3059.0113 3119.9567 1981.2416 2408.5536 3102.2711 2854.0576  359.4005 2639.3069 3004.6925         0 3324.9964
  std. dev.       962.1251  971.1133 1646.7333 1523.4941 1123.1453 1186.1932 1056.6559 1421.0299  906.3874    0.1138  535.6773
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829

1Int(dB)
  mean             -4.2251    -5.205  -16.6744  -15.0221   -8.7895   -8.1324    -48.18  -12.6603   -5.3672   -26.973   -7.8714
  std. dev.         9.6253    8.1176   10.4482    8.9257    8.3703   12.1307   16.3016   10.8312     8.007    2.1072    6.1238
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision          0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104

1Pitch(Hz)
  mean            215.3388  189.4672  114.7461   131.712  172.4288  178.9677   17.0614  158.9952  228.8154         0  202.3729
  std. dev.       108.3673   88.3909  113.0138  107.4401   77.1572  105.4237   56.8538   111.641   85.6072    0.0207   84.3095
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242

1RawPitch
  mean            204.8262  185.2835   96.3608  106.7449  164.8572  173.0225   13.9623  146.6041  219.9054         0  196.0699
  std. dev.       114.8351   95.4466  112.1859  103.7582   83.8605  112.8916   51.2547  116.7477   93.8544    0.0207   94.5325
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243

1SmPitch
  mean            213.2942  187.8876  108.1541  121.4041  170.0743  176.8295   15.4232  154.5794  228.2489         0  201.7462
  std. dev.       110.0116   89.6855  113.0596  104.6191   79.5741  107.6308   53.0677  114.1868   85.8949    0.0208   85.5321
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248

1Melogram(st)
  mean             51.2738    50.296   30.0127   37.1801   48.3177   46.5862    5.0217   41.2827   53.7993         0   53.0238
  std. dev.        17.0546   15.6299   27.4226   24.9375   16.4721   20.4797   15.7547   24.4294     14.79    0.0041   11.3416
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246

1ZCross
  mean              7.1301    5.2767    3.0165     2.069    3.7235    4.4336    0.2613    2.4852    4.0842         0    4.0832
  std. dev.         4.5874     3.209    4.9351    2.2383    3.2289    3.0623    1.1465    2.2611    2.9396    0.1792    2.3026
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755

1F1(Hz)
  mean            677.0705  461.0472  310.8017  388.0168  359.1654  481.7752   54.9845  372.2266  494.6223         0  516.9777
  std. dev.       274.1678  149.9473  279.3941   278.164  147.6819  227.7162  168.4345  238.0257   311.451    0.0608  153.3857
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648

1F2(Hz)
  mean           1438.0676 1772.0574 1007.7614 1173.9193 1952.7441 1139.4043  171.9905 1309.0897 1043.9884         0 1484.0198
  std. dev.       463.2855  544.5249   864.368  775.1169  688.7429  541.1664  521.2421  752.5784  438.5623    0.0738  319.4464
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429

1F3(Hz)
  mean           2645.3531 2603.6899 1716.1767 2003.0171 2631.3817 2490.7242  293.4349 2203.0948 2648.1475         0 2869.5564
  std. dev.       826.9081  809.0505 1449.4623 1310.3914  948.5385 1033.2776  874.0581 1210.0929  826.0863    0.1103  497.3337
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision          0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662

1F4(Hz)
  mean           3069.6694 3139.0513 1964.4989 2393.9651 3132.5813 2866.8671  352.5503 2641.4508 3035.0863         0 3323.5678
  std. dev.        945.882  942.3082 1650.1559 1531.3561 1087.3648 1172.3377 1047.8566 1419.3848  857.2082    0.1138  543.3417
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829

2Int(dB)
  mean             -4.2367   -5.1801  -16.5949  -15.0289   -8.7172    -8.146  -48.2528  -12.4737   -5.2139  -26.9195   -7.7997
  std. dev.         9.6141     8.069   10.3539    8.8787    8.3524   12.1698   16.2026   10.7103    7.7793    2.0302    6.1523
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039

2Pitch(Hz)
  mean            215.3054  190.0868  115.2128  131.0502  173.0539  178.5997   16.6931  159.5378  229.3808         0  202.2726
  std. dev.       108.4186   87.9634  113.1551  107.2576   76.5412   105.265   56.2811  111.1102    84.526    0.0207   84.5208
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242

2RawPitch
  mean            204.9204  185.5725   96.5081  106.2993  165.7052  172.7286   13.6791   147.555  220.4839         0  195.9629
  std. dev.        114.845   95.2181  112.2628  103.3674   83.3185  112.6688   50.8138  116.3293   93.0053    0.0207    94.473
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243

2SmPitch
  mean            213.3064  188.5362  108.5296  120.8089  170.6841  176.5062   15.0755  154.9178  228.8712         0  201.7143
  std. dev.       109.9963   89.2414  113.2143  104.3726   79.0929  107.4391   52.4903  113.8128   84.8121    0.0208   85.5991
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248

2Melogram(st)
  mean             51.3027   50.4037   30.0627   37.0227   48.4912   46.5516    4.9296    41.476   54.0704         0   53.0165
  std. dev.        16.9973   15.4597   27.4399   25.0004   16.2162    20.484   15.6284   24.3018   14.2652    0.0041   11.3502
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246

2ZCross
  mean              7.1259    5.2659    3.0292    2.0682    3.7314    4.4315    0.2535     2.518    4.0992         0    4.1291
  std. dev.         4.5903    3.2124    4.9373    2.2445    3.2172    3.0586    1.1335    2.2548    2.9273    0.1792    2.2988
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755

2F1(Hz)
  mean            680.5164   463.877  307.6156  383.2741  361.6146  484.2054   53.8391  371.8273  498.0573         0  517.6211
  std. dev.       271.5055   146.053   278.497  277.9272  144.1442  226.2125  166.7264   237.112   307.944    0.0608  153.3903
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648

2F2(Hz)
  mean           1442.0523  1785.616 1000.5225 1168.3225 1974.0792 1140.4987  168.5717 1308.1584  1053.554         0 1484.0387
  std. dev.       456.0769  528.1297  864.4378  778.4294  669.1502  535.6978  516.6505   749.768  426.6176    0.0738  321.3149
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429

2F3(Hz)
  mean           2653.6177 2619.8245 1704.5582 1990.5358 2655.6632  2499.568  287.4349 2202.5227  2673.852         0 2868.9125
  std. dev.       815.2889  786.5019 1450.8094 1314.6764  922.4738 1024.8703  866.0332 1206.5921  787.5699    0.1103  503.7764
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision          0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662

2F4(Hz)
  mean           3078.5421 3157.1809 1952.1789 2382.1202 3159.0187 2876.1426  345.5055 2644.3886  3063.919         0  3321.865
  std. dev.       932.0325  913.7543 1652.7631 1537.9929 1054.7924 1161.7476 1038.6648 1416.7903  806.6532    0.1138  550.7347
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829

3Int(dB)
  mean             -4.2733    -5.199  -16.4813  -15.0003    -8.676   -8.1936  -48.3071  -12.2641   -5.0927  -26.9132   -7.7016
  std. dev.         9.6287    8.0531   10.2875    8.8607    8.3577   12.2208   16.1119   10.5968    7.5769    1.9983     6.187
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037

3Pitch(Hz)
  mean            215.1446  190.3791  115.9271  130.8258  173.4856  178.0537   16.3392  160.3717  229.8416         0  202.2426
  std. dev.       108.5552   87.7367  113.3329  107.1381   76.0683  105.2226   55.7292  110.4325   83.5753    0.0207   84.5819
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242

3RawPitch
  mean            204.7828   185.837   97.0368  105.9763  166.2787  172.2852   13.3735  148.5095  220.9278         0  195.8324
  std. dev.       114.9802   95.0002  112.4475  102.9581   83.0617  112.5036   50.3498  115.8451   92.1417    0.0207    94.326
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243

3SmPitch
  mean            213.2239  188.9642  109.1119  120.6473  171.0525  175.9682   14.7415  155.6124  229.2536         0  201.7614
  std. dev.       110.0447   88.9263  113.4077  104.2291   78.7886  107.3853   51.9308  113.2741   84.0032    0.0208   85.5097
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248

3Melogram(st)
  mean             51.2983   50.4842   30.1853   37.0069   48.6341   46.4749    4.8257   41.6893   54.3072         0   53.0765
  std. dev.        16.9875   15.3213   27.4518   25.0249   15.9951   20.5262   15.4835   24.1614   13.7628    0.0041   11.1742
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246

3ZCross
  mean               7.111    5.2438    3.0478    2.0698    3.7483    4.4237    0.2484    2.5583    4.1111         0    4.1913
  std. dev.         4.6004    3.2168     4.934    2.2498    3.2275    3.0692    1.1311    2.2519    2.9164    0.1792    2.3027
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755

3F1(Hz)
  mean            683.4195  466.4894  305.1041  378.5922    363.75  486.2046   52.7357   372.086  500.6341         0  518.4309
  std. dev.        269.455  142.2388   277.473  277.5826  140.8431  225.0865  165.1041  236.0116  305.2064    0.0608  153.3851
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648

3F2(Hz)
  mean           1444.7727 1798.4442  995.4145 1163.3139  1993.047 1140.7371   165.125 1309.1173 1061.2779         0 1484.1075
  std. dev.       450.4532  511.8666  864.2114   782.141  650.7685  531.1147  511.8774    746.06  416.4942    0.0738  323.4684
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429

3F3(Hz)
  mean           2659.8447 2634.9785 1696.3721 1978.2255 2677.3565 2506.4348  281.5403 2204.7048 2694.0925         0 2868.2959
  std. dev.       806.7783  764.1593 1451.3307 1318.9077  898.3116 1018.6433  858.0755 1201.2074  755.1652    0.1103  510.0445
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision          0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662

3F4(Hz)
  mean           3084.7647   3174.33 1944.3398 2370.2376 3182.3609 2882.7851   338.536 2650.5175 3086.3027         0 3319.7801
  std. dev.       921.7526  885.5429 1654.8525 1544.5093 1024.5862 1153.5603 1029.4246 1411.8243   763.546    0.1138  557.8761
  weight sum         14333      6910     16793      1286      3561      6646     35830      2915      2080        33      1313
  precision         0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829





=== Stratified cross-validation ===

Correctly Classified Instances       30659               33.434  %
Incorrectly Classified Instances     61041               66.566  %
Kappa statistic                          0.249 
K&B Relative Info Score            2058979.3646 %
K&B Information Score                53424.431  bits      0.5826 bits/instance
Class complexity | order 0          237912.3107 bits      2.5945 bits/instance
Class complexity | scheme          6712548.0874 bits     73.2012 bits/instance
Complexity improvement     (Sf)    -6474635.7767 bits    -70.6067 bits/instance
Mean absolute error                      0.1212
Root mean squared error                  0.3351
Relative absolute error                 86.0321 %
Root relative squared error            126.2542 %
Coverage of cases (0.95 level)          39.6696 %
Mean rel. region size (0.95 level)      12.2987 %
Total Number of Instances            91700     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,373    0,035    0,663      0,373    0,477      0,433    0,855     0,571     A
                 0,336    0,031    0,472      0,336    0,392      0,357    0,850     0,379     E
                 0,140    0,024    0,563      0,140    0,224      0,214    0,631     0,378     CGJLNQSRT
                 0,067    0,029    0,032      0,067    0,043      0,026    0,635     0,027     FV
                 0,363    0,032    0,314      0,363    0,337      0,309    0,858     0,271     I
                 0,008    0,004    0,150      0,008    0,015      0,019    0,734     0,166     O
                 0,487    0,026    0,924      0,487    0,638      0,556    0,862     0,838     0
                 0,008    0,006    0,044      0,008    0,013      0,005    0,665     0,054     BMP
                 0,348    0,010    0,442      0,348    0,389      0,380    0,864     0,345     U
                 1,000    0,230    0,002      1,000    0,003      0,035    0,890     0,002     DZ
                 0,751    0,268    0,039      0,751    0,074      0,129    0,825     0,048     D
Weighted Avg.    0,334    0,029    0,639      0,334    0,419      0,376    0,798     0,548     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  5348   487   368   370   259   100   162    48   105   598  6488 |     a = A
   554  2319   217   155   383    13    40    70    45   240  2874 |     b = E
   720   534  2347   953  1029   115   988   199   166  4113  5629 |     c = CGJLNQSRT
    84    57    86    86   107     2    42     6     5   277   534 |     d = FV
   169   989   182    91  1292     1    50    36     1   153   597 |     e = I
   614   247   207   328    64    54    45    39   432   675  3941 |     f = O
    88   175   515   488   505    51 17448    71    56 14553  1880 |     g = 0
   172    67   147   131   381    15    84    23    61   459  1375 |     h = BMP
   190    14    79    57    54    10    18    21   723    26   888 |     i = U
     0     0     0     0     0     0     0     0     0    33     0 |     j = DZ
   131    27    18    49    36     0    12     7    41     6   986 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
37.31	3.4	2.57	2.58	1.81	0.7	1.13	0.33	0.73	4.17	45.27	 a = A
8.02	33.56	3.14	2.24	5.54	0.19	0.58	1.01	0.65	3.47	41.59	 b = E
4.29	3.18	13.98	5.67	6.13	0.68	5.88	1.19	0.99	24.49	33.52	 c = CGJLNQSRT
6.53	4.43	6.69	6.69	8.32	0.16	3.27	0.47	0.39	21.54	41.52	 d = FV
4.75	27.77	5.11	2.56	36.28	0.03	1.4	1.01	0.03	4.3	16.76	 e = I
9.24	3.72	3.11	4.94	0.96	0.81	0.68	0.59	6.5	10.16	59.3	 f = O
0.25	0.49	1.44	1.36	1.41	0.14	48.7	0.2	0.16	40.62	5.25	 g = 0
5.9	2.3	5.04	4.49	13.07	0.51	2.88	0.79	2.09	15.75	47.17	 h = BMP
9.13	0.67	3.8	2.74	2.6	0.48	0.87	1.01	34.76	1.25	42.69	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	 j = DZ
9.98	2.06	1.37	3.73	2.74	0.0	0.91	0.53	3.12	0.46	75.1	 k = D
