Timestamp:2013-07-23-03:06:40
Inventario:I4
Intervalo:6ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I4-6ms-5gramas
Num Instances:  30449
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  45 4Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.2283





=== Stratified cross-validation ===

Correctly Classified Instances       25189               82.7252 %
Incorrectly Classified Instances      5260               17.2748 %
Kappa statistic                          0.7757
K&B Relative Info Score            2200946.1867 %
K&B Information Score                56517.0258 bits      1.8561 bits/instance
Class complexity | order 0           78165.0348 bits      2.5671 bits/instance
Class complexity | scheme          1188827.1428 bits     39.0432 bits/instance
Complexity improvement     (Sf)    -1110662.108 bits    -36.4761 bits/instance
Mean absolute error                      0.0576
Root mean squared error                  0.1628
Relative absolute error                 37.1358 %
Root relative squared error             58.4386 %
Coverage of cases (0.95 level)          96.4202 %
Mean rel. region size (0.95 level)      25.2652 %
Total Number of Instances            30449     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,714    0,001    0,857      0,714    0,779      0,781    0,960     0,807     C
                 0,819    0,024    0,742      0,819    0,779      0,760    0,965     0,834     E
                 0,486    0,003    0,690      0,486    0,570      0,574    0,922     0,552     FV
                 0,873    0,046    0,825      0,873    0,848      0,810    0,966     0,912     AI
                 0,778    0,060    0,707      0,778    0,741      0,690    0,944     0,796     CDGKNRSYZ
                 0,736    0,012    0,828      0,736    0,779      0,764    0,940     0,809     O
                 0,920    0,057    0,908      0,920    0,914      0,861    0,975     0,958     0
                 0,439    0,008    0,674      0,439    0,532      0,530    0,895     0,516     LT
                 0,772    0,001    0,928      0,772    0,843      0,843    0,968     0,866     U
                 0,565    0,004    0,839      0,565    0,676      0,681    0,919     0,680     MBP
Weighted Avg.    0,827    0,044    0,827      0,827    0,824      0,786    0,959     0,869     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   132    10     1    21     7     3     6     0     4     1 |     a = C
     5  1923     9   143   150    27    57    18     3    12 |     b = E
     0    36   209    37   102    19    21     4     0     2 |     c = FV
     5   181    13  5266   283    46   165    50     4    18 |     d = AI
     4   165    22   323  3736    84   369    69     9    21 |     e = CDGKNRSYZ
     1    59    10   141   169  1657   180    19     5    10 |     f = O
     4   109    24   197   445    66 10683    48     7    24 |     g = 0
     1    40     7   123   233    25   186   491     6     7 |     h = LT
     1    22     3    34    40    37     7     4   538    11 |     i = U
     1    47     5    99   122    38    85    25     4   554 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
71.35	5.41	0.54	11.35	3.78	1.62	3.24	0.0	2.16	0.54	 a = C
0.21	81.93	0.38	6.09	6.39	1.15	2.43	0.77	0.13	0.51	 b = E
0.0	8.37	48.6	8.6	23.72	4.42	4.88	0.93	0.0	0.47	 c = FV
0.08	3.0	0.22	87.32	4.69	0.76	2.74	0.83	0.07	0.3	 d = AI
0.08	3.44	0.46	6.73	77.8	1.75	7.68	1.44	0.19	0.44	 e = CDGKNRSYZ
0.04	2.62	0.44	6.26	7.51	73.61	8.0	0.84	0.22	0.44	 f = O
0.03	0.94	0.21	1.7	3.83	0.57	92.04	0.41	0.06	0.21	 g = 0
0.09	3.57	0.63	10.99	20.82	2.23	16.62	43.88	0.54	0.63	 h = LT
0.14	3.16	0.43	4.88	5.74	5.31	1.0	0.57	77.19	1.58	 i = U
0.1	4.8	0.51	10.1	12.45	3.88	8.67	2.55	0.41	56.53	 j = MBP
