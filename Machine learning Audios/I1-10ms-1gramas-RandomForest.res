Timestamp:2013-07-22-21:19:10
Inventario:I1
Intervalo:10ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I1-10ms-1gramas
Num Instances:  18296
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.2898





=== Stratified cross-validation ===

Correctly Classified Instances       13786               75.3498 %
Incorrectly Classified Instances      4510               24.6502 %
Kappa statistic                          0.6911
K&B Relative Info Score            1206151.3931 %
K&B Information Score                35373.6449 bits      1.9334 bits/instance
Class complexity | order 0           53635.8387 bits      2.9316 bits/instance
Class complexity | scheme          1629712.2298 bits     89.0748 bits/instance
Complexity improvement     (Sf)    -1576076.391 bits    -86.1432 bits/instance
Mean absolute error                      0.0533
Root mean squared error                  0.165 
Relative absolute error                 42.9031 %
Root relative squared error             66.2341 %
Coverage of cases (0.95 level)          91.5774 %
Mean rel. region size (0.95 level)      20.947  %
Total Number of Instances            18296     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,833    0,048    0,770      0,833    0,800      0,761    0,947     0,863     a
                 0,470    0,012    0,576      0,470    0,518      0,506    0,850     0,479     b
                 0,742    0,033    0,659      0,742    0,698      0,672    0,935     0,724     e
                 0,416    0,004    0,585      0,416    0,486      0,487    0,894     0,422     d
                 0,280    0,005    0,462      0,280    0,348      0,352    0,783     0,274     f
                 0,725    0,012    0,725      0,725    0,725      0,713    0,936     0,733     i
                 0,201    0,008    0,326      0,201    0,248      0,244    0,743     0,167     k
                 0,341    0,003    0,602      0,341    0,436      0,449    0,855     0,399     j
                 0,534    0,047    0,551      0,534    0,542      0,493    0,869     0,554     l
                 0,671    0,019    0,745      0,671    0,706      0,684    0,911     0,728     o
                 0,928    0,075    0,879      0,928    0,903      0,844    0,971     0,957     0
                 0,591    0,025    0,600      0,591    0,596      0,570    0,913     0,622     s
                 0,690    0,004    0,813      0,690    0,747      0,744    0,945     0,775     u
Weighted Avg.    0,753    0,047    0,744      0,753    0,746      0,707    0,931     0,778     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 2449   20   79    9    8   15   12    5  132   58  109   39    5 |    a = a
   46  279   42    2    6   25   10    0   65   22   62   25   10 |    b = b
   88   23 1067    9    8   42    8    8   92   27   39   23    4 |    c = e
   39    7   23  114    1    3    0    1   42   29    3    7    5 |    d = d
   26   13   25    3   73    3    9    3   36   17   14   37    2 |    e = f
   17   16   71    2    3  542    5    2   46    2   27   12    3 |    f = i
   24    4   16    3    6    4   72    7   40   13  105   65    0 |    g = k
   34    1   28    1    4   12    7   71   14    3   12   14    7 |    h = j
  208   43  135   21   13   49   27    4  954   64  189   71   10 |    i = l
   97   25   42   19    5    5    9    3   92  924  125   19   12 |    j = o
   85   25   48    6   12   25   27    4  111   36 6306  109    4 |    k = 0
   53   10   26    3   18   20   35    8   84   13  170  643    5 |    l = s
   15   18   16    3    1    3    0    2   24   33    9    7  292 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
83.3	0.68	2.69	0.31	0.27	0.51	0.41	0.17	4.49	1.97	3.71	1.33	0.17	 a = a
7.74	46.97	7.07	0.34	1.01	4.21	1.68	0.0	10.94	3.7	10.44	4.21	1.68	 b = b
6.12	1.6	74.2	0.63	0.56	2.92	0.56	0.56	6.4	1.88	2.71	1.6	0.28	 c = e
14.23	2.55	8.39	41.61	0.36	1.09	0.0	0.36	15.33	10.58	1.09	2.55	1.82	 d = d
9.96	4.98	9.58	1.15	27.97	1.15	3.45	1.15	13.79	6.51	5.36	14.18	0.77	 e = f
2.27	2.14	9.49	0.27	0.4	72.46	0.67	0.27	6.15	0.27	3.61	1.6	0.4	 f = i
6.69	1.11	4.46	0.84	1.67	1.11	20.06	1.95	11.14	3.62	29.25	18.11	0.0	 g = k
16.35	0.48	13.46	0.48	1.92	5.77	3.37	34.13	6.73	1.44	5.77	6.73	3.37	 h = j
11.63	2.4	7.55	1.17	0.73	2.74	1.51	0.22	53.36	3.58	10.57	3.97	0.56	 i = l
7.04	1.82	3.05	1.38	0.36	0.36	0.65	0.22	6.68	67.1	9.08	1.38	0.87	 j = o
1.25	0.37	0.71	0.09	0.18	0.37	0.4	0.06	1.63	0.53	92.76	1.6	0.06	 k = 0
4.87	0.92	2.39	0.28	1.65	1.84	3.22	0.74	7.72	1.19	15.63	59.1	0.46	 l = s
3.55	4.26	3.78	0.71	0.24	0.71	0.0	0.47	5.67	7.8	2.13	1.65	69.03	 m = u
