Timestamp:2013-08-30-05:29:53
Inventario:I4
Intervalo:20ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I4-20ms-4gramas
Num Instances:  9126
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(4): class 
0F1(Hz)(8): class 
0F2(Hz)(6): class 
0F3(Hz)(3): class 
0F4(Hz)(4): class 
1Int(dB)(8): class 
1Pitch(Hz)(5): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(3): class 
1F1(Hz)(8): class 
1F2(Hz)(6): class 
1F3(Hz)(2): class 
1F4(Hz)(6): class 
2Int(dB)(9): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(4): class 
2F1(Hz)(8): class 
2F2(Hz)(6): class 
2F3(Hz)(2): class 
2F4(Hz)(4): class 
3Int(dB)(9): class 
3Pitch(Hz)(5): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(6): class 
3F1(Hz)(9): class 
3F2(Hz)(9): class 
3F3(Hz)(6): class 
3F4(Hz)(4): class 
class(10): 
LogScore Bayes: -353378.2792208312
LogScore BDeu: -360261.92165513604
LogScore MDL: -359834.05949538667
LogScore ENTROPY: -351768.40769285103
LogScore AIC: -353537.407692851




=== Stratified cross-validation ===

Correctly Classified Instances        4763               52.1915 %
Incorrectly Classified Instances      4363               47.8085 %
Kappa statistic                          0.4072
K&B Relative Info Score             369960.6517 %
K&B Information Score                 9683.3248 bits      1.0611 bits/instance
Class complexity | order 0           23869.4748 bits      2.6155 bits/instance
Class complexity | scheme            94344.1923 bits     10.338  bits/instance
Complexity improvement     (Sf)     -70474.7174 bits     -7.7224 bits/instance
Mean absolute error                      0.0965
Root mean squared error                  0.2829
Relative absolute error                 61.1671 %
Root relative squared error            100.7145 %
Coverage of cases (0.95 level)          62.9301 %
Mean rel. region size (0.95 level)      16.9636 %
Total Number of Instances             9126     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,467    0,095    0,031      0,467    0,059      0,101    0,830     0,030     C
                 0,462    0,091    0,311      0,462    0,372      0,311    0,837     0,345     E
                 0,138    0,035    0,057      0,138    0,081      0,067    0,762     0,040     FV
                 0,402    0,058    0,643      0,402    0,494      0,414    0,838     0,612     AI
                 0,199    0,047    0,452      0,199    0,276      0,217    0,757     0,379     CDGKNRSYZ
                 0,294    0,040    0,384      0,294    0,333      0,287    0,808     0,321     O
                 0,905    0,098    0,835      0,905    0,869      0,794    0,949     0,894     0
                 0,080    0,026    0,104      0,080    0,091      0,061    0,732     0,082     LT
                 0,445    0,023    0,321      0,445    0,373      0,360    0,898     0,419     U
                 0,181    0,033    0,158      0,181    0,168      0,139    0,753     0,106     MBP
Weighted Avg.    0,522    0,069    0,576      0,522    0,531      0,468    0,855     0,576     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   28   17    2    7    2    0    0    1    1    2 |    a = C
  138  345   20   58   26   43   43   19   34   20 |    b = E
   22   28   19   14   22   10    9    5    3    6 |    c = FV
  240  305   69  759   93  134  106   61   55   68 |    d = AI
  235  182  105  168  297   63  241   64   26  111 |    e = CDGKNRSYZ
   81   91   23   93   33  209   79   17   61   25 |    f = O
   18   32   47   20  107   12 2928   43    4   23 |    g = 0
   45   47   28   25   47   19   67   27    7   25 |    h = LT
   24   16    2   17    3   35    9    7   97    8 |    i = U
   62   47   18   20   27   19   23   15   14   54 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
46.67	28.33	3.33	11.67	3.33	0.0	0.0	1.67	1.67	3.33	 a = C
18.5	46.25	2.68	7.77	3.49	5.76	5.76	2.55	4.56	2.68	 b = E
15.94	20.29	13.77	10.14	15.94	7.25	6.52	3.62	2.17	4.35	 c = FV
12.7	16.14	3.65	40.16	4.92	7.09	5.61	3.23	2.91	3.6	 d = AI
15.75	12.2	7.04	11.26	19.91	4.22	16.15	4.29	1.74	7.44	 e = CDGKNRSYZ
11.38	12.78	3.23	13.06	4.63	29.35	11.1	2.39	8.57	3.51	 f = O
0.56	0.99	1.45	0.62	3.31	0.37	90.54	1.33	0.12	0.71	 g = 0
13.35	13.95	8.31	7.42	13.95	5.64	19.88	8.01	2.08	7.42	 h = LT
11.01	7.34	0.92	7.8	1.38	16.06	4.13	3.21	44.5	3.67	 i = U
20.74	15.72	6.02	6.69	9.03	6.35	7.69	5.02	4.68	18.06	 j = MBP
