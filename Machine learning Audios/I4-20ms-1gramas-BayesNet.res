Timestamp:2013-08-30-05:29:42
Inventario:I4
Intervalo:20ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I4-20ms-1gramas
Num Instances:  9129
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(5): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(6): class 
0F1(Hz)(9): class 
0F2(Hz)(9): class 
0F3(Hz)(6): class 
0F4(Hz)(4): class 
class(10): 
LogScore Bayes: -104692.04840009817
LogScore BDeu: -106864.80288041133
LogScore MDL: -106739.07865981874
LogScore ENTROPY: -104281.45117712545
LogScore AIC: -104820.45117712545




=== Stratified cross-validation ===

Correctly Classified Instances        5345               58.5497 %
Incorrectly Classified Instances      3784               41.4503 %
Kappa statistic                          0.4622
K&B Relative Info Score             419345.9544 %
K&B Information Score                10974.1849 bits      1.2021 bits/instance
Class complexity | order 0           23873.9636 bits      2.6152 bits/instance
Class complexity | scheme            32863.455  bits      3.5999 bits/instance
Complexity improvement     (Sf)      -8989.4913 bits     -0.9847 bits/instance
Mean absolute error                      0.0894
Root mean squared error                  0.2464
Relative absolute error                 56.6643 %
Root relative squared error             87.7311 %
Coverage of cases (0.95 level)          81.2247 %
Mean rel. region size (0.95 level)      27.9012 %
Total Number of Instances             9129     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,067    0,009    0,047      0,067    0,055      0,049    0,812     0,027     C
                 0,623    0,112    0,331      0,623    0,433      0,388    0,878     0,405     E
                 0,007    0,002    0,043      0,007    0,012      0,012    0,748     0,037     FV
                 0,646    0,148    0,533      0,646    0,584      0,466    0,864     0,677     AI
                 0,180    0,056    0,386      0,180    0,246      0,173    0,773     0,366     CDGKNRSYZ
                 0,336    0,036    0,444      0,336    0,382      0,342    0,827     0,388     O
                 0,937    0,126    0,803      0,937    0,865      0,787    0,975     0,964     0
                 0,000    0,001    0,000      0,000    0,000      -0,005   0,736     0,085     LT
                 0,454    0,013    0,465      0,454    0,459      0,446    0,917     0,472     U
                 0,050    0,009    0,155      0,050    0,076      0,071    0,777     0,108     MBP
Weighted Avg.    0,585    0,097    0,537      0,585    0,547      0,476    0,878     0,624     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
    4   27    0   24    3    0    2    0    0    0 |    a = C
   13  465    0  173   41   17   25    0   10    2 |    b = E
    2   37    1   29   22   16   22    0    4    5 |    c = FV
   29  323    3 1221  127   66   83    1    5   32 |    d = AI
   18  244    6  395  269   97  403    3   33   24 |    e = CDGKNRSYZ
    6   86    4  224   38  239   73    1   37    4 |    f = O
    1   32    4   55   92   18 3032    1    1    1 |    g = 0
    0   79    3   65   60   24   91    0    5   10 |    h = LT
    2   21    1   37   11   40    3    0   99    4 |    i = U
   10   89    1   68   33   21   42    1   19   15 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
6.67	45.0	0.0	40.0	5.0	0.0	3.33	0.0	0.0	0.0	 a = C
1.74	62.33	0.0	23.19	5.5	2.28	3.35	0.0	1.34	0.27	 b = E
1.45	26.81	0.72	21.01	15.94	11.59	15.94	0.0	2.9	3.62	 c = FV
1.53	17.09	0.16	64.6	6.72	3.49	4.39	0.05	0.26	1.69	 d = AI
1.21	16.35	0.4	26.47	18.03	6.5	27.01	0.2	2.21	1.61	 e = CDGKNRSYZ
0.84	12.08	0.56	31.46	5.34	33.57	10.25	0.14	5.2	0.56	 f = O
0.03	0.99	0.12	1.7	2.84	0.56	93.67	0.03	0.03	0.03	 g = 0
0.0	23.44	0.89	19.29	17.8	7.12	27.0	0.0	1.48	2.97	 h = LT
0.92	9.63	0.46	16.97	5.05	18.35	1.38	0.0	45.41	1.83	 i = U
3.34	29.77	0.33	22.74	11.04	7.02	14.05	0.33	6.35	5.02	 j = MBP
