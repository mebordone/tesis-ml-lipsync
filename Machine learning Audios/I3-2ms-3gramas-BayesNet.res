Timestamp:2013-08-30-04:56:28
Inventario:I3
Intervalo:2ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I3-2ms-3gramas
Num Instances:  91701
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(24): class 
0Pitch(Hz)(48): class 
0RawPitch(35): class 
0SmPitch(40): class 
0Melogram(st)(41): class 
0ZCross(17): class 
0F1(Hz)(1307): class 
0F2(Hz)(1893): class 
0F3(Hz)(2235): class 
0F4(Hz)(2060): class 
1Int(dB)(22): class 
1Pitch(Hz)(46): class 
1RawPitch(34): class 
1SmPitch(35): class 
1Melogram(st)(45): class 
1ZCross(17): class 
1F1(Hz)(1379): class 
1F2(Hz)(1939): class 
1F3(Hz)(2284): class 
1F4(Hz)(2125): class 
2Int(dB)(25): class 
2Pitch(Hz)(48): class 
2RawPitch(32): class 
2SmPitch(39): class 
2Melogram(st)(45): class 
2ZCross(17): class 
2F1(Hz)(1291): class 
2F2(Hz)(1920): class 
2F3(Hz)(2248): class 
2F4(Hz)(2101): class 
class(11): 
LogScore Bayes: -7521452.404535253
LogScore BDeu: -1.0652678398567855E7
LogScore MDL: -9859079.810827967
LogScore ENTROPY: -8390847.43559681
LogScore AIC: -8647839.43559681




=== Stratified cross-validation ===

Correctly Classified Instances       68984               75.2271 %
Incorrectly Classified Instances     22717               24.7729 %
Kappa statistic                          0.6791
K&B Relative Info Score            6530452.5326 %
K&B Information Score               169444.7639 bits      1.8478 bits/instance
Class complexity | order 0          237913.6665 bits      2.5945 bits/instance
Class complexity | scheme           725876.6239 bits      7.9157 bits/instance
Complexity improvement     (Sf)    -487962.9575 bits     -5.3212 bits/instance
Mean absolute error                      0.0453
Root mean squared error                  0.2046
Relative absolute error                 32.1774 %
Root relative squared error             77.0959 %
Coverage of cases (0.95 level)          78.3557 %
Mean rel. region size (0.95 level)      10.2785 %
Total Number of Instances            91701     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,812    0,023    0,868      0,812    0,839      0,811    0,956     0,897     A
                 0,840    0,031    0,688      0,840    0,757      0,739    0,958     0,840     E
                 0,327    0,020    0,790      0,327    0,463      0,450    0,934     0,731     CGJLNQSRT
                 0,586    0,008    0,498      0,586    0,539      0,533    0,903     0,572     FV
                 0,851    0,017    0,673      0,851    0,751      0,746    0,958     0,835     I
                 0,750    0,014    0,805      0,750    0,777      0,760    0,922     0,813     O
                 0,902    0,157    0,787      0,902    0,840      0,731    0,967     0,958     0
                 0,659    0,008    0,718      0,659    0,687      0,678    0,899     0,697     BMP
                 0,901    0,011    0,656      0,901    0,759      0,763    0,977     0,902     U
                 0,000    0,004    0,000      0,000    0,000      -0,001   0,851     0,001     DZ
                 0,895    0,028    0,319      0,895    0,470      0,524    0,980     0,749     D
Weighted Avg.    0,752    0,074    0,773      0,752    0,740      0,688    0,952     0,864     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 11638   359   348   159   153   243   802    97    89    76   369 |     a = A
   117  5802   235    62    98    31   288    58    44    33   142 |     b = E
   898  1357  5496   122   715   407  5808   285   582    21  1102 |     c = CGJLNQSRT
    31    40    38   754    30    13   323    11     9     1    36 |     d = FV
    23    71   117    24  3030    10   192    26     8    39    21 |     e = I
   188   159   108    12    33  4982   727    60    44    38   295 |     f = O
   423   497   453   358   371   412 32312   201   144   184   476 |     g = 0
    63    88    83     6    67    48   539  1920    61     1    39 |     h = BMP
     3    20    68     7     3    15    42    12  1875     4    31 |     i = U
     0     0     0     0     0     0    33     0     0     0     0 |     j = DZ
    23    35    11     9     4    24    15     3     4    10  1175 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
81.2	2.5	2.43	1.11	1.07	1.7	5.6	0.68	0.62	0.53	2.57	 a = A
1.69	83.97	3.4	0.9	1.42	0.45	4.17	0.84	0.64	0.48	2.05	 b = E
5.35	8.08	32.73	0.73	4.26	2.42	34.59	1.7	3.47	0.13	6.56	 c = CGJLNQSRT
2.41	3.11	2.95	58.63	2.33	1.01	25.12	0.86	0.7	0.08	2.8	 d = FV
0.65	1.99	3.29	0.67	85.09	0.28	5.39	0.73	0.22	1.1	0.59	 e = I
2.83	2.39	1.63	0.18	0.5	74.96	10.94	0.9	0.66	0.57	4.44	 f = O
1.18	1.39	1.26	1.0	1.04	1.15	90.18	0.56	0.4	0.51	1.33	 g = 0
2.16	3.02	2.85	0.21	2.3	1.65	18.49	65.87	2.09	0.03	1.34	 h = BMP
0.14	0.96	3.27	0.34	0.14	0.72	2.02	0.58	90.14	0.19	1.49	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
1.75	2.67	0.84	0.69	0.3	1.83	1.14	0.23	0.3	0.76	89.49	 k = D
