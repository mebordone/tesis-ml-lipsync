Timestamp:2013-08-30-04:38:44
Inventario:I2
Intervalo:3ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I2-3ms-4gramas
Num Instances:  61132
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(20): class 
0Pitch(Hz)(26): class 
0RawPitch(22): class 
0SmPitch(24): class 
0Melogram(st)(30): class 
0ZCross(15): class 
0F1(Hz)(247): class 
0F2(Hz)(262): class 
0F3(Hz)(351): class 
0F4(Hz)(257): class 
1Int(dB)(20): class 
1Pitch(Hz)(28): class 
1RawPitch(22): class 
1SmPitch(23): class 
1Melogram(st)(32): class 
1ZCross(15): class 
1F1(Hz)(222): class 
1F2(Hz)(233): class 
1F3(Hz)(413): class 
1F4(Hz)(233): class 
2Int(dB)(21): class 
2Pitch(Hz)(28): class 
2RawPitch(19): class 
2SmPitch(23): class 
2Melogram(st)(35): class 
2ZCross(15): class 
2F1(Hz)(199): class 
2F2(Hz)(282): class 
2F3(Hz)(351): class 
2F4(Hz)(217): class 
3Int(dB)(19): class 
3Pitch(Hz)(27): class 
3RawPitch(21): class 
3SmPitch(24): class 
3Melogram(st)(36): class 
3ZCross(16): class 
3F1(Hz)(208): class 
3F2(Hz)(219): class 
3F3(Hz)(297): class 
3F4(Hz)(205): class 
class(12): 
LogScore Bayes: -4912526.571869286
LogScore BDeu: -5426464.802198135
LogScore MDL: -5337430.081074288
LogScore ENTROPY: -5025459.04721171
LogScore AIC: -5082074.04721171




=== Stratified cross-validation ===

Correctly Classified Instances       38679               63.2713 %
Incorrectly Classified Instances     22453               36.7287 %
Kappa statistic                          0.5347
K&B Relative Info Score            3228220.4017 %
K&B Information Score                92657.2175 bits      1.5157 bits/instance
Class complexity | order 0          175443.7931 bits      2.8699 bits/instance
Class complexity | scheme           807550.9154 bits     13.21   bits/instance
Complexity improvement     (Sf)    -632107.1223 bits    -10.34   bits/instance
Mean absolute error                      0.0617
Root mean squared error                  0.2358
Relative absolute error                 46.4189 %
Root relative squared error             91.4964 %
Coverage of cases (0.95 level)          68.2327 %
Mean rel. region size (0.95 level)      10.4555 %
Total Number of Instances            61132     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,644    0,042    0,742      0,644    0,690      0,639    0,920     0,768     A
                 0,086    0,011    0,279      0,086    0,132      0,132    0,793     0,179     LGJKC
                 0,637    0,058    0,473      0,637    0,543      0,506    0,910     0,573     E
                 0,238    0,013    0,204      0,238    0,220      0,209    0,852     0,169     FV
                 0,695    0,030    0,484      0,695    0,571      0,560    0,933     0,601     I
                 0,437    0,022    0,608      0,437    0,509      0,484    0,872     0,545     O
                 0,258    0,028    0,492      0,258    0,338      0,310    0,875     0,418     SNRTY
                 0,894    0,132    0,811      0,894    0,851      0,751    0,957     0,951     0
                 0,207    0,008    0,453      0,207    0,284      0,291    0,839     0,289     BMP
                 0,513    0,045    0,147      0,513    0,228      0,255    0,912     0,265     DZ
                 0,652    0,018    0,461      0,652    0,540      0,536    0,951     0,629     U
                 0,406    0,031    0,357      0,406    0,380      0,353    0,879     0,340     SNRTXY
Weighted Avg.    0,633    0,071    0,635      0,633    0,620      0,565    0,915     0,688     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  6174   143   755   123   166   445   231   485    47   569   128   319 |     a = A
   234   255   366    61   295   138   295   796    35   211    93   184 |     b = LGJKC
   309    56  2942    61   302    69   125   177    79   236    75   190 |     c = E
    52     5   103   206    55    31    57   197    24    99    16    19 |     d = FV
    23    32   260    28  1656     3    92   122    27    40    39    60 |     e = I
   565    67   344    37    79  1951    95   465    41   432   238   149 |     f = O
   301   146   344   166   191   113  1497  2120    82   354   133   361 |     g = SNRTY
   263   133   374   189   256   221   370 21219    88   282    54   274 |     h = 0
   127    40   201    71   205    51    91   337   403   153   150   119 |     i = BMP
    77     2    97    20    40    32    17    27    11   464    34    83 |     j = DZ
    45    21    78    22     9   102    30    29    24    67   903    56 |     k = U
   149    15   353    24   168    52   145   188    28   257    97  1009 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
64.41	1.49	7.88	1.28	1.73	4.64	2.41	5.06	0.49	5.94	1.34	3.33	 a = A
7.9	8.61	12.35	2.06	9.96	4.66	9.96	26.86	1.18	7.12	3.14	6.21	 b = LGJKC
6.69	1.21	63.67	1.32	6.54	1.49	2.71	3.83	1.71	5.11	1.62	4.11	 c = E
6.02	0.58	11.92	23.84	6.37	3.59	6.6	22.8	2.78	11.46	1.85	2.2	 d = FV
0.97	1.34	10.92	1.18	69.52	0.13	3.86	5.12	1.13	1.68	1.64	2.52	 e = I
12.66	1.5	7.71	0.83	1.77	43.71	2.13	10.42	0.92	9.68	5.33	3.34	 f = O
5.18	2.51	5.92	2.86	3.29	1.95	25.77	36.5	1.41	6.1	2.29	6.22	 g = SNRTY
1.11	0.56	1.58	0.8	1.08	0.93	1.56	89.44	0.37	1.19	0.23	1.15	 h = 0
6.52	2.05	10.32	3.64	10.52	2.62	4.67	17.3	20.69	7.85	7.7	6.11	 i = BMP
8.52	0.22	10.73	2.21	4.42	3.54	1.88	2.99	1.22	51.33	3.76	9.18	 j = DZ
3.25	1.52	5.63	1.59	0.65	7.36	2.16	2.09	1.73	4.83	65.15	4.04	 k = U
6.0	0.6	14.21	0.97	6.76	2.09	5.84	7.57	1.13	10.34	3.9	40.6	 l = SNRTXY
