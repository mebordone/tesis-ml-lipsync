Timestamp:2013-08-30-03:52:02
Inventario:I0
Intervalo:6ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I0-6ms-3gramas
Num Instances:  30451
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    27 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(14): class 
0Pitch(Hz)(13): class 
0RawPitch(10): class 
0SmPitch(11): class 
0Melogram(st)(13): class 
0ZCross(12): class 
0F1(Hz)(21): class 
0F2(Hz)(19): class 
0F3(Hz)(20): class 
0F4(Hz)(14): class 
1Int(dB)(12): class 
1Pitch(Hz)(12): class 
1RawPitch(12): class 
1SmPitch(7): class 
1Melogram(st)(12): class 
1ZCross(12): class 
1F1(Hz)(20): class 
1F2(Hz)(18): class 
1F3(Hz)(22): class 
1F4(Hz)(16): class 
2Int(dB)(11): class 
2Pitch(Hz)(11): class 
2RawPitch(11): class 
2SmPitch(9): class 
2Melogram(st)(12): class 
2ZCross(12): class 
2F1(Hz)(20): class 
2F2(Hz)(17): class 
2F3(Hz)(18): class 
2F4(Hz)(13): class 
class(27): 
LogScore Bayes: -1281185.7313129639
LogScore BDeu: -1346187.785068979
LogScore MDL: -1337886.700698365
LogScore ENTROPY: -1282839.8039261354
LogScore AIC: -1293503.8039261359




=== Stratified cross-validation ===

Correctly Classified Instances       17326               56.898  %
Incorrectly Classified Instances     13125               43.102  %
Kappa statistic                          0.471 
K&B Relative Info Score            1368709.2314 %
K&B Information Score                45489.1374 bits      1.4938 bits/instance
Class complexity | order 0          101112.8454 bits      3.3205 bits/instance
Class complexity | scheme           309004.1558 bits     10.1476 bits/instance
Complexity improvement     (Sf)    -207891.3104 bits     -6.8271 bits/instance
Mean absolute error                      0.0326
Root mean squared error                  0.1626
Relative absolute error                 54.0851 %
Root relative squared error             93.6554 %
Coverage of cases (0.95 level)          66.3689 %
Mean rel. region size (0.95 level)       6.4069 %
Total Number of Instances            30451     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,236    0,005    0,185      0,236    0,207      0,205    0,918     0,153     ch
                 0,500    0,012    0,046      0,500    0,084      0,149    0,958     0,207     rr
                 0,893    0,100    0,846      0,893    0,869      0,786    0,959     0,953     0
                 0,193    0,020    0,098      0,193    0,130      0,124    0,840     0,069     hs
                 0,690    0,024    0,073      0,690    0,132      0,219    0,970     0,208     hg
                 0,641    0,040    0,733      0,641    0,684      0,636    0,927     0,764     a
                 0,188    0,013    0,177      0,188    0,183      0,170    0,881     0,117     c
                 0,246    0,014    0,107      0,246    0,149      0,154    0,927     0,080     b
                 0,427    0,033    0,522      0,427    0,470      0,432    0,881     0,451     e
                 0,268    0,022    0,150      0,268    0,192      0,185    0,871     0,149     d
                 0,259    0,015    0,098      0,259    0,142      0,151    0,881     0,105     g
                 0,134    0,005    0,157      0,134    0,144      0,140    0,909     0,096     f
                 0,488    0,017    0,546      0,488    0,516      0,498    0,921     0,486     i
                 0,283    0,003    0,346      0,283    0,311      0,309    0,926     0,282     j
                 0,174    0,008    0,233      0,174    0,199      0,192    0,897     0,159     m
                 0,399    0,044    0,144      0,399    0,211      0,217    0,886     0,142     l
                 0,202    0,009    0,649      0,202    0,308      0,337    0,839     0,408     o
                 0,186    0,015    0,311      0,186    0,233      0,220    0,874     0,202     n
                 0,135    0,002    0,209      0,135    0,164      0,165    0,885     0,121     q
                 0,075    0,005    0,140      0,075    0,097      0,095    0,855     0,074     p
                 0,303    0,011    0,583      0,303    0,399      0,400    0,890     0,409     s
                 0,079    0,008    0,191      0,079    0,112      0,110    0,813     0,107     r
                 0,448    0,010    0,522      0,448    0,482      0,472    0,926     0,524     u
                 0,088    0,008    0,181      0,088    0,118      0,115    0,876     0,119     t
                 0,242    0,023    0,077      0,242    0,117      0,125    0,871     0,069     v
                 0,486    0,017    0,120      0,486    0,193      0,235    0,918     0,243     y
                 0,000    0,001    0,000      0,000    0,000      -0,001   0,970     0,006     z
Weighted Avg.    0,569    0,052    0,619      0,569    0,576      0,536    0,916     0,619     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m     n     o     p     q     r     s     t     u     v     w     x     y     z    aa   <-- classified as
    34     2    31     1     2     5    21     0     2     0     0     0     3     2     0     4     0     6     1     5    24     0     0     1     0     0     0 |     a = ch
     0    18     0     0     3     0     0     0     3     0     0     0     0     0     1     1     0     2     0     0     0     1     0     0     6     1     0 |     b = rr
    15     7 10372   112    63    86    47    25    68    58    18    26   100    25    13    88    39    63    18    37    85    38    15    38    52    86    15 |     c = 0
     0     1    76    65    27    18     7     3     9    11     1     7     8     0     5    16     5     3     0     7    10     5     3     7    17    24     1 |     d = hs
     0     0     0     0    58     3     0     5    10     1     0     0     2     0     0     4     0     0     0     0     0     0     0     0     1     0     0 |     e = hg
     9    95   109    52    15  2823    45    41   215   164    49    12    27    11     6   207    76    68     9    12    28    63    21    27   154    57    10 |     f = a
    16     0   221    18     3     1    85     0    10     0     0     9     9     6     0     2     4     2     3     6    27     2     5    12     4     7     0 |     g = c
     0    10     0     0    14    14     0    50    22     2     6     0     4     0    10    35    10     0     0     0     0     0    11     0     9     6     0 |     h = b
     7    48    78    44   145   175    26    28  1002    51   122     4    89     7    15   196    22    57     8    13     7    12    18    28    98    46     1 |     i = e
     0    21     1    16     6    41     0    20    29   118    15     0    12     0    14    58     1    24     0     0     0     2     9     2    36    16     0 |     j = d
     0     7     7     0    16    23     1     6    17     7    48     0    22     3     1     3     1     5     0     0     3     2     4     2     4     3     0 |     k = g
     0     4    67    12     6    11    11     0     7     1     0    26     0     3     0     2     1     4     0     6    13     2     0     3     5     9     1 |     l = f
    11     3    45    18   110    11    16    38   110     0    29     1   589     0    45    64     2    15    14     8    11     1     1    19    11    34     0 |     m = i
     0     0    24     8     8    20     3     2    17     0     3     3     2    47     0     1     0     1     0     2    17     2     0     3     3     0     0 |     n = j
     0     4     6    17    44    27     2    33    38    11    25     2    25     0    77    71     3    12     0     0     0     2    12     1    13    17     0 |     o = m
     0     9     1    10    19    40     4    22    31    22     3     0    26     0    21   220     9    19     1     1     4     4    14     0    32    39     0 |     p = l
     5    60   216   113   101   309    22    96   117   211    42     3     8     2    23   150   454    45     1    13    13    19   127    17    55    23     6 |     q = o
     0    58    23    44    84    60     5    28    57    24    57     4    54     0    68   138     3   198     4     1     8    12    22     6    49    56     3 |     r = n
     8     0    46     0     1     8    12     0     2     0     1     0    17     0     0     0     0     1    19     2    19     0     0     2     1     2     0 |     s = q
     1     0   125    20     5    13    19     5     8    13     0    20     0    10     1    12     2     2     0    25     8    12     3    16     6     8     1 |     t = p
    64    12   437    38     9    35    68     1    18    17    19    21    57    15     5    38    12    37     7    15   451    30     0    19    15    50     0 |     u = s
     2     8    63    48     7    90     7    22    72    48    23    15    17     2     2    86     5    11     1     6     8    56    15     8    80     4     0 |     v = r
     1     7    10     5    20    31    12    37    36     9    14     1     1     0    13    77    40    19     2     3     1     2   312    13    16    15     0 |     w = u
     9     6   275    16     7     1    58     0     1     2     5     7     4     3     1     1     7    32     2    14    22    17     4    50    11    13     0 |     x = t
     0    10     4     9    18     7     3     6    19    18     9     1     1     0     5    39     3     3     0     1     1     7     1     2    57    12     0 |     y = v
     2     1     6     0     2     0     5     0     1     0     0     4     1     0     4    20     0     7     1     1    13     2     1     1     4    72     0 |     z = y
     0     0    12     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0 |    aa = z

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z	aa	
23.61	1.39	21.53	0.69	1.39	3.47	14.58	0.0	1.39	0.0	0.0	0.0	2.08	1.39	0.0	2.78	0.0	4.17	0.69	3.47	16.67	0.0	0.0	0.69	0.0	0.0	0.0	 a = ch
0.0	50.0	0.0	0.0	8.33	0.0	0.0	0.0	8.33	0.0	0.0	0.0	0.0	0.0	2.78	2.78	0.0	5.56	0.0	0.0	0.0	2.78	0.0	0.0	16.67	2.78	0.0	 b = rr
0.13	0.06	89.34	0.96	0.54	0.74	0.4	0.22	0.59	0.5	0.16	0.22	0.86	0.22	0.11	0.76	0.34	0.54	0.16	0.32	0.73	0.33	0.13	0.33	0.45	0.74	0.13	 c = 0
0.0	0.3	22.62	19.35	8.04	5.36	2.08	0.89	2.68	3.27	0.3	2.08	2.38	0.0	1.49	4.76	1.49	0.89	0.0	2.08	2.98	1.49	0.89	2.08	5.06	7.14	0.3	 d = hs
0.0	0.0	0.0	0.0	69.05	3.57	0.0	5.95	11.9	1.19	0.0	0.0	2.38	0.0	0.0	4.76	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	1.19	0.0	0.0	 e = hg
0.2	2.16	2.47	1.18	0.34	64.09	1.02	0.93	4.88	3.72	1.11	0.27	0.61	0.25	0.14	4.7	1.73	1.54	0.2	0.27	0.64	1.43	0.48	0.61	3.5	1.29	0.23	 f = a
3.54	0.0	48.89	3.98	0.66	0.22	18.81	0.0	2.21	0.0	0.0	1.99	1.99	1.33	0.0	0.44	0.88	0.44	0.66	1.33	5.97	0.44	1.11	2.65	0.88	1.55	0.0	 g = c
0.0	4.93	0.0	0.0	6.9	6.9	0.0	24.63	10.84	0.99	2.96	0.0	1.97	0.0	4.93	17.24	4.93	0.0	0.0	0.0	0.0	0.0	5.42	0.0	4.43	2.96	0.0	 h = b
0.3	2.05	3.32	1.87	6.18	7.46	1.11	1.19	42.69	2.17	5.2	0.17	3.79	0.3	0.64	8.35	0.94	2.43	0.34	0.55	0.3	0.51	0.77	1.19	4.18	1.96	0.04	 i = e
0.0	4.76	0.23	3.63	1.36	9.3	0.0	4.54	6.58	26.76	3.4	0.0	2.72	0.0	3.17	13.15	0.23	5.44	0.0	0.0	0.0	0.45	2.04	0.45	8.16	3.63	0.0	 j = d
0.0	3.78	3.78	0.0	8.65	12.43	0.54	3.24	9.19	3.78	25.95	0.0	11.89	1.62	0.54	1.62	0.54	2.7	0.0	0.0	1.62	1.08	2.16	1.08	2.16	1.62	0.0	 k = g
0.0	2.06	34.54	6.19	3.09	5.67	5.67	0.0	3.61	0.52	0.0	13.4	0.0	1.55	0.0	1.03	0.52	2.06	0.0	3.09	6.7	1.03	0.0	1.55	2.58	4.64	0.52	 l = f
0.91	0.25	3.73	1.49	9.12	0.91	1.33	3.15	9.12	0.0	2.4	0.08	48.84	0.0	3.73	5.31	0.17	1.24	1.16	0.66	0.91	0.08	0.08	1.58	0.91	2.82	0.0	 m = i
0.0	0.0	14.46	4.82	4.82	12.05	1.81	1.2	10.24	0.0	1.81	1.81	1.2	28.31	0.0	0.6	0.0	0.6	0.0	1.2	10.24	1.2	0.0	1.81	1.81	0.0	0.0	 n = j
0.0	0.9	1.36	3.85	9.95	6.11	0.45	7.47	8.6	2.49	5.66	0.45	5.66	0.0	17.42	16.06	0.68	2.71	0.0	0.0	0.0	0.45	2.71	0.23	2.94	3.85	0.0	 o = m
0.0	1.63	0.18	1.81	3.45	7.26	0.73	3.99	5.63	3.99	0.54	0.0	4.72	0.0	3.81	39.93	1.63	3.45	0.18	0.18	0.73	0.73	2.54	0.0	5.81	7.08	0.0	 p = l
0.22	2.67	9.6	5.02	4.49	13.73	0.98	4.26	5.2	9.37	1.87	0.13	0.36	0.09	1.02	6.66	20.17	2.0	0.04	0.58	0.58	0.84	5.64	0.76	2.44	1.02	0.27	 q = o
0.0	5.44	2.16	4.13	7.88	5.63	0.47	2.63	5.35	2.25	5.35	0.38	5.07	0.0	6.38	12.95	0.28	18.57	0.38	0.09	0.75	1.13	2.06	0.56	4.6	5.25	0.28	 r = n
5.67	0.0	32.62	0.0	0.71	5.67	8.51	0.0	1.42	0.0	0.71	0.0	12.06	0.0	0.0	0.0	0.0	0.71	13.48	1.42	13.48	0.0	0.0	1.42	0.71	1.42	0.0	 s = q
0.3	0.0	37.31	5.97	1.49	3.88	5.67	1.49	2.39	3.88	0.0	5.97	0.0	2.99	0.3	3.58	0.6	0.6	0.0	7.46	2.39	3.58	0.9	4.78	1.79	2.39	0.3	 t = p
4.3	0.81	29.33	2.55	0.6	2.35	4.56	0.07	1.21	1.14	1.28	1.41	3.83	1.01	0.34	2.55	0.81	2.48	0.47	1.01	30.27	2.01	0.0	1.28	1.01	3.36	0.0	 u = s
0.28	1.13	8.92	6.8	0.99	12.75	0.99	3.12	10.2	6.8	3.26	2.12	2.41	0.28	0.28	12.18	0.71	1.56	0.14	0.85	1.13	7.93	2.12	1.13	11.33	0.57	0.0	 v = r
0.14	1.0	1.43	0.72	2.87	4.45	1.72	5.31	5.16	1.29	2.01	0.14	0.14	0.0	1.87	11.05	5.74	2.73	0.29	0.43	0.14	0.29	44.76	1.87	2.3	2.15	0.0	 w = u
1.58	1.06	48.42	2.82	1.23	0.18	10.21	0.0	0.18	0.35	0.88	1.23	0.7	0.53	0.18	0.18	1.23	5.63	0.35	2.46	3.87	2.99	0.7	8.8	1.94	2.29	0.0	 x = t
0.0	4.24	1.69	3.81	7.63	2.97	1.27	2.54	8.05	7.63	3.81	0.42	0.42	0.0	2.12	16.53	1.27	1.27	0.0	0.42	0.42	2.97	0.42	0.85	24.15	5.08	0.0	 y = v
1.35	0.68	4.05	0.0	1.35	0.0	3.38	0.0	0.68	0.0	0.0	2.7	0.68	0.0	2.7	13.51	0.0	4.73	0.68	0.68	8.78	1.35	0.68	0.68	2.7	48.65	0.0	 z = y
0.0	0.0	100.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 aa = z
