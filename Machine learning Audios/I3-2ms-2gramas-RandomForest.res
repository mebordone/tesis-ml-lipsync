Timestamp:2013-07-22-22:17:16
Inventario:I3
Intervalo:2ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I3-2ms-2gramas
Num Instances:  91702
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1405





=== Stratified cross-validation ===

Correctly Classified Instances       81367               88.7298 %
Incorrectly Classified Instances     10335               11.2702 %
Kappa statistic                          0.8541
K&B Relative Info Score            7670872.5305 %
K&B Information Score               199035.5122 bits      2.1705 bits/instance
Class complexity | order 0          237915.0222 bits      2.5944 bits/instance
Class complexity | scheme          3893121.4164 bits     42.4541 bits/instance
Complexity improvement     (Sf)    -3655206.3942 bits    -39.8596 bits/instance
Mean absolute error                      0.0311
Root mean squared error                  0.1273
Relative absolute error                 22.0683 %
Root relative squared error             47.9532 %
Coverage of cases (0.95 level)          95.8267 %
Mean rel. region size (0.95 level)      15.4886 %
Total Number of Instances            91702     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,924    0,015    0,919      0,924    0,921      0,906    0,975     0,951     A
                 0,915    0,008    0,899      0,915    0,907      0,899    0,977     0,938     E
                 0,823    0,048    0,792      0,823    0,807      0,763    0,952     0,857     CGJLNQSRT
                 0,667    0,003    0,763      0,667    0,712      0,710    0,908     0,699     FV
                 0,884    0,003    0,914      0,884    0,899      0,895    0,971     0,921     I
                 0,845    0,008    0,893      0,845    0,869      0,859    0,950     0,887     O
                 0,924    0,054    0,916      0,924    0,920      0,869    0,975     0,955     0
                 0,739    0,005    0,839      0,739    0,785      0,781    0,921     0,789     BMP
                 0,919    0,001    0,972      0,919    0,945      0,944    0,989     0,963     U
                 0,030    0,000    0,045      0,030    0,036      0,037    0,545     0,004     DZ
                 0,890    0,001    0,950      0,890    0,919      0,918    0,993     0,954     D
Weighted Avg.    0,887    0,034    0,888      0,887    0,887      0,855    0,967     0,920     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 13238    57   526    15    20    48   387    31     5     0     6 |     a = A
    55  6320   278    15    17    27   138    37    10     0    13 |     b = E
   486   251 13824   137    75   229  1599   154    19    12     7 |     c = CGJLNQSRT
    30    28   267   858    12    21    57    10     0     2     1 |     d = FV
    28    52   183     8  3149     9   111    16     4     1     0 |     e = I
    91    41   327    16    10  5619   488    31     3     0    20 |     f = O
   367   187  1599    52   111   251 33125   115     7     4    14 |     g = 0
    78    52   317    14    39    38   216  2153     6     2     0 |     h = BMP
    12    14    76     1     8    16    23    18  1911     0     1 |     i = U
     0     0    19     6     1     0     4     2     0     1     0 |     j = DZ
    25    27    38     2     3    31    17     0     1     0  1169 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
92.36	0.4	3.67	0.1	0.14	0.33	2.7	0.22	0.03	0.0	0.04	 a = A
0.8	91.46	4.02	0.22	0.25	0.39	2.0	0.54	0.14	0.0	0.19	 b = E
2.89	1.49	82.32	0.82	0.45	1.36	9.52	0.92	0.11	0.07	0.04	 c = CGJLNQSRT
2.33	2.18	20.76	66.72	0.93	1.63	4.43	0.78	0.0	0.16	0.08	 d = FV
0.79	1.46	5.14	0.22	88.43	0.25	3.12	0.45	0.11	0.03	0.0	 e = I
1.37	0.62	4.92	0.24	0.15	84.55	7.34	0.47	0.05	0.0	0.3	 f = O
1.02	0.52	4.46	0.15	0.31	0.7	92.45	0.32	0.02	0.01	0.04	 g = 0
2.68	1.78	10.87	0.48	1.34	1.3	7.41	73.86	0.21	0.07	0.0	 h = BMP
0.58	0.67	3.65	0.05	0.38	0.77	1.11	0.87	91.88	0.0	0.05	 i = U
0.0	0.0	57.58	18.18	3.03	0.0	12.12	6.06	0.0	3.03	0.0	 j = DZ
1.9	2.06	2.89	0.15	0.23	2.36	1.29	0.0	0.08	0.0	89.03	 k = D
