Timestamp:2013-08-30-05:28:26
Inventario:I4
Intervalo:10ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I4-10ms-1gramas
Num Instances:  18296
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(11): class 
0Pitch(Hz)(7): class 
0RawPitch(5): class 
0SmPitch(5): class 
0Melogram(st)(9): class 
0ZCross(11): class 
0F1(Hz)(13): class 
0F2(Hz)(13): class 
0F3(Hz)(9): class 
0F4(Hz)(8): class 
class(10): 
LogScore Bayes: -248319.50024959948
LogScore BDeu: -252013.57912005874
LogScore MDL: -251702.1511219494
LogScore ENTROPY: -247683.13886917918
LogScore AIC: -248502.13886917918




=== Stratified cross-validation ===

Correctly Classified Instances       10935               59.7672 %
Incorrectly Classified Instances      7361               40.2328 %
Kappa statistic                          0.4716
K&B Relative Info Score             869838.8049 %
K&B Information Score                22468.7458 bits      1.2281 bits/instance
Class complexity | order 0           47243.3901 bits      2.5822 bits/instance
Class complexity | scheme            68213.7062 bits      3.7283 bits/instance
Complexity improvement     (Sf)     -20970.3161 bits     -1.1462 bits/instance
Mean absolute error                      0.0859
Root mean squared error                  0.2463
Relative absolute error                 55.0002 %
Root relative squared error             88.1591 %
Coverage of cases (0.95 level)          79.1976 %
Mean rel. region size (0.95 level)      24.6546 %
Total Number of Instances            18296     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,168    0,007    0,117      0,168    0,138      0,134    0,857     0,118     C
                 0,620    0,099    0,348      0,620    0,446      0,404    0,886     0,459     E
                 0,023    0,002    0,140      0,023    0,039      0,051    0,772     0,058     FV
                 0,678    0,135    0,560      0,678    0,613      0,507    0,880     0,702     AI
                 0,156    0,051    0,371      0,156    0,220      0,155    0,787     0,359     CDGKNRSYZ
                 0,378    0,036    0,463      0,378    0,416      0,376    0,843     0,432     O
                 0,924    0,147    0,788      0,924    0,850      0,757    0,968     0,960     0
                 0,007    0,003    0,088      0,007    0,014      0,015    0,761     0,094     LT
                 0,489    0,015    0,433      0,489    0,459      0,447    0,931     0,502     U
                 0,082    0,009    0,237      0,082    0,122      0,123    0,777     0,131     MBP
Weighted Avg.    0,598    0,101    0,551      0,598    0,558      0,485    0,887     0,645     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   18   45    0   27    6    4    6    0    0    1 |    a = C
   40  892    1  301   77   36   56    6   17   12 |    b = E
    3   64    6   46   36   28   57    1    6   14 |    c = FV
   24  561    7 2500  220  118  177   11   22   48 |    d = AI
   35  432    9  726  459  193  945   15   69   50 |    e = CDGKNRSYZ
   10  137    6  388   65  521  144    5   98    3 |    f = O
    4  108    3  157  170   51 6278    5    8   14 |    g = 0
    6  146    6  130  122   45  198    5    8   11 |    h = LT
    7   43    1   53   14   78    8    7  207    5 |    i = U
    7  135    4  139   67   51   97    2   43   49 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
16.82	42.06	0.0	25.23	5.61	3.74	5.61	0.0	0.0	0.93	 a = C
2.78	62.03	0.07	20.93	5.35	2.5	3.89	0.42	1.18	0.83	 b = E
1.15	24.52	2.3	17.62	13.79	10.73	21.84	0.38	2.3	5.36	 c = FV
0.65	15.21	0.19	67.79	5.97	3.2	4.8	0.3	0.6	1.3	 d = AI
1.19	14.73	0.31	24.75	15.65	6.58	32.22	0.51	2.35	1.7	 e = CDGKNRSYZ
0.73	9.95	0.44	28.18	4.72	37.84	10.46	0.36	7.12	0.22	 f = O
0.06	1.59	0.04	2.31	2.5	0.75	92.35	0.07	0.12	0.21	 g = 0
0.89	21.57	0.89	19.2	18.02	6.65	29.25	0.74	1.18	1.62	 h = LT
1.65	10.17	0.24	12.53	3.31	18.44	1.89	1.65	48.94	1.18	 i = U
1.18	22.73	0.67	23.4	11.28	8.59	16.33	0.34	7.24	8.25	 j = MBP
