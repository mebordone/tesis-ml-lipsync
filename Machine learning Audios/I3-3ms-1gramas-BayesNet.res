Timestamp:2013-08-30-05:00:51
Inventario:I3
Intervalo:3ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I3-3ms-1gramas
Num Instances:  61135
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(20): class 
0Pitch(Hz)(35): class 
0RawPitch(22): class 
0SmPitch(25): class 
0Melogram(st)(32): class 
0ZCross(16): class 
0F1(Hz)(242): class 
0F2(Hz)(240): class 
0F3(Hz)(298): class 
0F4(Hz)(262): class 
class(11): 
LogScore Bayes: -1332400.960525738
LogScore BDeu: -1448667.210353531
LogScore MDL: -1429531.054074329
LogScore ENTROPY: -1357829.4702536953
LogScore AIC: -1370841.4702536953




=== Stratified cross-validation ===

Correctly Classified Instances       39136               64.0157 %
Incorrectly Classified Instances     21999               35.9843 %
Kappa statistic                          0.5334
K&B Relative Info Score            3424350.2364 %
K&B Information Score                89042.1075 bits      1.4565 bits/instance
Class complexity | order 0          158937.2465 bits      2.5998 bits/instance
Class complexity | scheme           222256.8087 bits      3.6355 bits/instance
Complexity improvement     (Sf)     -63319.5622 bits     -1.0357 bits/instance
Mean absolute error                      0.0694
Root mean squared error                  0.2283
Relative absolute error                 49.1547 %
Root relative squared error             85.9204 %
Coverage of cases (0.95 level)          78.4984 %
Mean rel. region size (0.95 level)      17.9388 %
Total Number of Instances            61135     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,716    0,059    0,693      0,716    0,704      0,648    0,921     0,768     A
                 0,665    0,074    0,425      0,665    0,519      0,484    0,912     0,575     E
                 0,195    0,039    0,531      0,195    0,285      0,241    0,846     0,492     CGJLNQSRT
                 0,152    0,005    0,324      0,152    0,207      0,214    0,842     0,183     FV
                 0,678    0,028    0,492      0,678    0,570      0,557    0,936     0,606     I
                 0,489    0,032    0,550      0,489    0,518      0,483    0,870     0,540     O
                 0,902    0,158    0,784      0,902    0,839      0,730    0,959     0,953     0
                 0,228    0,010    0,437      0,228    0,299      0,300    0,836     0,296     BMP
                 0,602    0,016    0,474      0,602    0,530      0,522    0,952     0,604     U
                 0,000    0,001    0,000      0,000    0,000      -0,001   0,937     0,003     DZ
                 0,437    0,028    0,186      0,437    0,260      0,269    0,913     0,247     D
Weighted Avg.    0,640    0,088    0,634      0,640    0,616      0,551    0,915     0,717     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  6863   703   415    55   160   448   530    40    92     5   274 |     a = A
   413  3075   262    26   277   117   182    76    73     1   119 |     b = E
  1078  1571  2196    58   664   561  3943   230   350     1   604 |     c = CGJLNQSRT
    68   145    73   131    56    60   217    34    22     1    57 |     d = FV
    33   356   148     7  1614     4   126    33    25     3    33 |     e = I
   690   398   162    14    54  2183   496    39   159     4   264 |     f = O
   409   450   613    74   234   232 21410    70    61    23   150 |     g = 0
   162   275   175    17   175   110   351   444   117     1   121 |     h = BMP
    70   109    48     6     8   187    22    32   835     1    68 |     i = U
     0     0     0     0     0     0    23     0     0     0     0 |     j = DZ
   121   150    41    16    41    68    10    19    29     1   385 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
71.6	7.33	4.33	0.57	1.67	4.67	5.53	0.42	0.96	0.05	2.86	 a = A
8.94	66.54	5.67	0.56	5.99	2.53	3.94	1.64	1.58	0.02	2.58	 b = E
9.58	13.96	19.51	0.52	5.9	4.98	35.03	2.04	3.11	0.01	5.37	 c = CGJLNQSRT
7.87	16.78	8.45	15.16	6.48	6.94	25.12	3.94	2.55	0.12	6.6	 d = FV
1.39	14.95	6.21	0.29	67.76	0.17	5.29	1.39	1.05	0.13	1.39	 e = I
15.46	8.92	3.63	0.31	1.21	48.91	11.11	0.87	3.56	0.09	5.92	 f = O
1.72	1.9	2.58	0.31	0.99	0.98	90.24	0.3	0.26	0.1	0.63	 g = 0
8.32	14.12	8.98	0.87	8.98	5.65	18.02	22.79	6.01	0.05	6.21	 h = BMP
5.05	7.86	3.46	0.43	0.58	13.49	1.59	2.31	60.25	0.07	4.91	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
13.73	17.03	4.65	1.82	4.65	7.72	1.14	2.16	3.29	0.11	43.7	 k = D
