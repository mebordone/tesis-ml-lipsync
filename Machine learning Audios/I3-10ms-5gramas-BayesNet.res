Timestamp:2013-08-30-05:05:27
Inventario:I3
Intervalo:10ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I3-10ms-5gramas
Num Instances:  18292
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  43 4RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  44 4SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  45 4Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  47 4F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  49 4F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  50 4F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(10): class 
0Pitch(Hz)(7): class 
0RawPitch(4): class 
0SmPitch(7): class 
0Melogram(st)(9): class 
0ZCross(7): class 
0F1(Hz)(9): class 
0F2(Hz)(11): class 
0F3(Hz)(7): class 
0F4(Hz)(9): class 
1Int(dB)(10): class 
1Pitch(Hz)(7): class 
1RawPitch(6): class 
1SmPitch(7): class 
1Melogram(st)(10): class 
1ZCross(8): class 
1F1(Hz)(10): class 
1F2(Hz)(13): class 
1F3(Hz)(7): class 
1F4(Hz)(11): class 
2Int(dB)(11): class 
2Pitch(Hz)(6): class 
2RawPitch(6): class 
2SmPitch(7): class 
2Melogram(st)(10): class 
2ZCross(9): class 
2F1(Hz)(12): class 
2F2(Hz)(12): class 
2F3(Hz)(9): class 
2F4(Hz)(12): class 
3Int(dB)(11): class 
3Pitch(Hz)(7): class 
3RawPitch(6): class 
3SmPitch(8): class 
3Melogram(st)(9): class 
3ZCross(10): class 
3F1(Hz)(14): class 
3F2(Hz)(12): class 
3F3(Hz)(9): class 
3F4(Hz)(12): class 
4Int(dB)(12): class 
4Pitch(Hz)(7): class 
4RawPitch(7): class 
4SmPitch(9): class 
4Melogram(st)(9): class 
4ZCross(12): class 
4F1(Hz)(12): class 
4F2(Hz)(13): class 
4F3(Hz)(10): class 
4F4(Hz)(8): class 
class(11): 
LogScore Bayes: -1163690.8099692587
LogScore BDeu: -1184531.0305981531
LogScore MDL: -1182881.2515969328
LogScore ENTROPY: -1160701.1164653348
LogScore AIC: -1165221.1164653348




=== Stratified cross-validation ===

Correctly Classified Instances       10116               55.3029 %
Incorrectly Classified Instances      8176               44.6971 %
Kappa statistic                          0.4399
K&B Relative Info Score             809050.6348 %
K&B Information Score                21315.1061 bits      1.1653 bits/instance
Class complexity | order 0           48171.0561 bits      2.6334 bits/instance
Class complexity | scheme           289763.3158 bits     15.841  bits/instance
Complexity improvement     (Sf)    -241592.2597 bits    -13.2075 bits/instance
Mean absolute error                      0.0815
Root mean squared error                  0.273 
Relative absolute error                 57.044  %
Root relative squared error            102.1594 %
Coverage of cases (0.95 level)          59.9716 %
Mean rel. region size (0.95 level)      11.5202 %
Total Number of Instances            18292     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,540    0,058    0,641      0,540    0,586      0,517    0,884     0,669     A
                 0,463    0,054    0,421      0,463    0,441      0,391    0,869     0,413     E
                 0,162    0,052    0,419      0,162    0,233      0,165    0,762     0,373     CGJLNQSRT
                 0,123    0,019    0,084      0,123    0,099      0,085    0,798     0,065     FV
                 0,611    0,040    0,397      0,611    0,481      0,466    0,913     0,431     I
                 0,252    0,016    0,555      0,252    0,347      0,342    0,826     0,389     O
                 0,894    0,121    0,814      0,894    0,852      0,761    0,956     0,946     0
                 0,056    0,006    0,236      0,056    0,090      0,101    0,769     0,110     BMP
                 0,435    0,017    0,382      0,435    0,407      0,393    0,911     0,462     U
                 0,250    0,014    0,007      0,250    0,015      0,041    0,933     0,016     DZ
                 0,665    0,123    0,074      0,665    0,133      0,192    0,886     0,110     D
Weighted Avg.    0,553    0,074    0,594      0,553    0,551      0,489    0,879     0,625     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1587  230  206   67   81  100  143    9   18   24  475 |    a = A
  148  666  112   14  114   21   71    9   54   15  214 |    b = E
  313  292  557  144  242   51  841   48   71  110  774 |    c = CGJLNQSRT
   28   30   30   32   12    8   38    3    3    9   68 |    d = FV
   17   83   58    7  457    1   52    7    5    8   53 |    e = I
  190  101   95    6   47  347  137    3  103   25  323 |    f = O
   72   79  187   76   71   31 6074   26   12   57  109 |    g = 0
   57   67   44   29  106   13   76   33   23   14  132 |    h = BMP
   30   24   25    3   13   46   23    1  184    3   71 |    i = U
    0    0    1    0    0    0    5    0    0    2    0 |    j = DZ
   35   10   13    5    9    7    0    1    9    0  177 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
53.98	7.82	7.01	2.28	2.76	3.4	4.86	0.31	0.61	0.82	16.16	 a = A
10.29	46.31	7.79	0.97	7.93	1.46	4.94	0.63	3.76	1.04	14.88	 b = E
9.09	8.48	16.18	4.18	7.03	1.48	24.43	1.39	2.06	3.19	22.48	 c = CGJLNQSRT
10.73	11.49	11.49	12.26	4.6	3.07	14.56	1.15	1.15	3.45	26.05	 d = FV
2.27	11.1	7.75	0.94	61.1	0.13	6.95	0.94	0.67	1.07	7.09	 e = I
13.8	7.33	6.9	0.44	3.41	25.2	9.95	0.22	7.48	1.82	23.46	 f = O
1.06	1.16	2.75	1.12	1.05	0.46	89.4	0.38	0.18	0.84	1.6	 g = 0
9.6	11.28	7.41	4.88	17.85	2.19	12.79	5.56	3.87	2.36	22.22	 h = BMP
7.09	5.67	5.91	0.71	3.07	10.87	5.44	0.24	43.5	0.71	16.78	 i = U
0.0	0.0	12.5	0.0	0.0	0.0	62.5	0.0	0.0	25.0	0.0	 j = DZ
13.16	3.76	4.89	1.88	3.38	2.63	0.0	0.38	3.38	0.0	66.54	 k = D
