Timestamp:2013-08-30-04:42:41
Inventario:I2
Intervalo:15ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I2-15ms-4gramas
Num Instances:  12186
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(4): class 
0F1(Hz)(9): class 
0F2(Hz)(8): class 
0F3(Hz)(3): class 
0F4(Hz)(7): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(4): class 
1F1(Hz)(9): class 
1F2(Hz)(8): class 
1F3(Hz)(6): class 
1F4(Hz)(6): class 
2Int(dB)(8): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(9): class 
2F1(Hz)(9): class 
2F2(Hz)(9): class 
2F3(Hz)(7): class 
2F4(Hz)(6): class 
3Int(dB)(10): class 
3Pitch(Hz)(6): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(10): class 
3F1(Hz)(10): class 
3F2(Hz)(10): class 
3F3(Hz)(7): class 
3F4(Hz)(7): class 
class(12): 
LogScore Bayes: -501630.1954288546
LogScore BDeu: -512878.39387401845
LogScore MDL: -511668.72143896145
LogScore ENTROPY: -499311.2569179947
LogScore AIC: -501938.2569179947




=== Stratified cross-validation ===

Correctly Classified Instances        6663               54.6775 %
Incorrectly Classified Instances      5523               45.3225 %
Kappa statistic                          0.4412
K&B Relative Info Score             521717.1439 %
K&B Information Score                15281.4263 bits      1.254  bits/instance
Class complexity | order 0           35676.2104 bits      2.9276 bits/instance
Class complexity | scheme           144239.2209 bits     11.8365 bits/instance
Complexity improvement     (Sf)    -108563.0105 bits     -8.9088 bits/instance
Mean absolute error                      0.0761
Root mean squared error                  0.2551
Relative absolute error                 56.258  %
Root relative squared error             98.0745 %
Coverage of cases (0.95 level)          62.3913 %
Mean rel. region size (0.95 level)      12.818  %
Total Number of Instances            12186     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,540    0,057    0,648      0,540    0,590      0,521    0,878     0,659     A
                 0,023    0,011    0,097      0,023    0,038      0,024    0,688     0,081     LGJKC
                 0,408    0,047    0,427      0,408    0,417      0,368    0,864     0,399     E
                 0,085    0,018    0,064      0,085    0,073      0,058    0,780     0,042     FV
                 0,579    0,038    0,396      0,579    0,470      0,452    0,909     0,418     I
                 0,219    0,015    0,551      0,219    0,314      0,316    0,815     0,372     O
                 0,267    0,056    0,346      0,267    0,301      0,237    0,789     0,293     SNRTY
                 0,904    0,103    0,833      0,904    0,867      0,789    0,960     0,935     0
                 0,023    0,005    0,136      0,023    0,039      0,043    0,763     0,091     BMP
                 0,511    0,100    0,072      0,511    0,126      0,162    0,848     0,098     DZ
                 0,467    0,015    0,430      0,467    0,448      0,434    0,906     0,470     U
                 0,222    0,051    0,159      0,222    0,185      0,146    0,827     0,146     SNRTXY
Weighted Avg.    0,547    0,064    0,563      0,547    0,542      0,489    0,878     0,579     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1075   45  152   39   44   76  106   92    5  262   14   79 |    a = A
   59   14   51   22   56    7   71  140    9   77    7   85 |    b = LGJKC
  104   23  396   11   92    9   50   46    4  132   29   75 |    c = E
   16    4   22   15    3    6   29   17    1   41    3   20 |    d = FV
   12   10   54    4  291    0   39   24    3   27    6   33 |    e = I
  135   22   67   10   32  206   51   91    6  219   59   42 |    f = O
   85    9   50   52   55   10  325  308   17  163   19  125 |    g = SNRTY
   25    3   31   48   40   15  162 3993    7   41    4   46 |    h = 0
   37    1   29   18   70    9   41   40    9   65   18   62 |    i = BMP
   28    0    5    3    6    4   10    4    0   93    8   21 |    j = DZ
   19   10   17    1    6   28   10   13    1   37  133   10 |    k = U
   63    3   54   13   39    4   46   24    4  137    9  113 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
54.05	2.26	7.64	1.96	2.21	3.82	5.33	4.63	0.25	13.17	0.7	3.97	 a = A
9.87	2.34	8.53	3.68	9.36	1.17	11.87	23.41	1.51	12.88	1.17	14.21	 b = LGJKC
10.71	2.37	40.78	1.13	9.47	0.93	5.15	4.74	0.41	13.59	2.99	7.72	 c = E
9.04	2.26	12.43	8.47	1.69	3.39	16.38	9.6	0.56	23.16	1.69	11.3	 d = FV
2.39	1.99	10.74	0.8	57.85	0.0	7.75	4.77	0.6	5.37	1.19	6.56	 e = I
14.36	2.34	7.13	1.06	3.4	21.91	5.43	9.68	0.64	23.3	6.28	4.47	 f = O
6.98	0.74	4.11	4.27	4.52	0.82	26.68	25.29	1.4	13.38	1.56	10.26	 g = SNRTY
0.57	0.07	0.7	1.09	0.91	0.34	3.67	90.44	0.16	0.93	0.09	1.04	 h = 0
9.27	0.25	7.27	4.51	17.54	2.26	10.28	10.03	2.26	16.29	4.51	15.54	 i = BMP
15.38	0.0	2.75	1.65	3.3	2.2	5.49	2.2	0.0	51.1	4.4	11.54	 j = DZ
6.67	3.51	5.96	0.35	2.11	9.82	3.51	4.56	0.35	12.98	46.67	3.51	 k = U
12.38	0.59	10.61	2.55	7.66	0.79	9.04	4.72	0.79	26.92	1.77	22.2	 l = SNRTXY
