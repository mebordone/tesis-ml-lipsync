Timestamp:2013-08-30-04:19:03
Inventario:I1
Intervalo:20ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I1-20ms-4gramas
Num Instances:  9126
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(4): class 
0F1(Hz)(6): class 
0F2(Hz)(5): class 
0F3(Hz)(3): class 
0F4(Hz)(3): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(5): class 
1F1(Hz)(8): class 
1F2(Hz)(7): class 
1F3(Hz)(3): class 
1F4(Hz)(3): class 
2Int(dB)(8): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(7): class 
2F1(Hz)(9): class 
2F2(Hz)(8): class 
2F3(Hz)(3): class 
2F4(Hz)(3): class 
3Int(dB)(9): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(8): class 
3F1(Hz)(10): class 
3F2(Hz)(9): class 
3F3(Hz)(6): class 
3F4(Hz)(5): class 
class(13): 
LogScore Bayes: -348319.36715530464
LogScore BDeu: -358153.0775894535
LogScore MDL: -357162.64810218
LogScore ENTROPY: -346438.8419746709
LogScore AIC: -348790.84197467094




=== Stratified cross-validation ===

Correctly Classified Instances        4701               51.5122 %
Incorrectly Classified Instances      4425               48.4878 %
Kappa statistic                          0.4155
K&B Relative Info Score             373931.9178 %
K&B Information Score                11123.1745 bits      1.2188 bits/instance
Class complexity | order 0           27123.537  bits      2.9721 bits/instance
Class complexity | scheme           100174.6153 bits     10.9768 bits/instance
Complexity improvement     (Sf)     -73051.0782 bits     -8.0047 bits/instance
Mean absolute error                      0.0747
Root mean squared error                  0.2484
Relative absolute error                 59.4189 %
Root relative squared error             99.1144 %
Coverage of cases (0.95 level)          63.4341 %
Mean rel. region size (0.95 level)      13.4105 %
Total Number of Instances             9126     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,487    0,057    0,627      0,487    0,548      0,477    0,868     0,642     a
                 0,027    0,007    0,121      0,027    0,044      0,042    0,749     0,090     b
                 0,327    0,045    0,395      0,327    0,358      0,308    0,852     0,375     e
                 0,457    0,103    0,064      0,457    0,112      0,139    0,829     0,086     d
                 0,065    0,014    0,065      0,065    0,065      0,051    0,772     0,045     f
                 0,561    0,049    0,333      0,561    0,418      0,400    0,893     0,423     i
                 0,265    0,052    0,093      0,265    0,138      0,129    0,812     0,076     k
                 0,174    0,031    0,063      0,174    0,093      0,087    0,834     0,049     j
                 0,100    0,035    0,239      0,100    0,141      0,097    0,775     0,228     l
                 0,211    0,022    0,453      0,211    0,288      0,271    0,798     0,313     o
                 0,891    0,064    0,885      0,891    0,888      0,826    0,964     0,953     0
                 0,260    0,036    0,315      0,260    0,285      0,245    0,830     0,305     s
                 0,440    0,019    0,364      0,440    0,398      0,384    0,888     0,413     u
Weighted Avg.    0,515    0,049    0,559      0,515    0,523      0,476    0,878     0,575     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
  733    5  100  223   24   49   69   36   59   91   49   40   27 |    a = a
   26    8   19   44    9   58   24   31   25    9   15   20   11 |    b = b
   66    7  244  121    2  104   39   65   20   12   23   16   27 |    c = e
   23    0    9   63    0    6    4    7   14    3    2    2    5 |    d = d
   13    3   18   29    9    5   14    9   14    5    6   13    0 |    e = f
    4    2   49   28    1  216   33   14   10    0    9   15    4 |    f = i
    3    5    1    3    5    9   48    5   10    2   63   27    0 |    g = k
   22    2   16    8    5   11    3   19    9    0    1    7    6 |    h = j
  119   15   75  222   14   84   60   62   91   23   50   73   22 |    i = l
  102    1   44  157    9   28   43   11   34  150   61   14   58 |    j = o
   15    8   10   25   37   33   84    7   36   10 2881   83    5 |    k = 0
   25    9   13   40   23   34   78   34   56    3   90  143    3 |    l = s
   18    1   19   22    1   11   16    1    3   23    6    1   96 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
48.7	0.33	6.64	14.82	1.59	3.26	4.58	2.39	3.92	6.05	3.26	2.66	1.79	 a = a
8.7	2.68	6.35	14.72	3.01	19.4	8.03	10.37	8.36	3.01	5.02	6.69	3.68	 b = b
8.85	0.94	32.71	16.22	0.27	13.94	5.23	8.71	2.68	1.61	3.08	2.14	3.62	 c = e
16.67	0.0	6.52	45.65	0.0	4.35	2.9	5.07	10.14	2.17	1.45	1.45	3.62	 d = d
9.42	2.17	13.04	21.01	6.52	3.62	10.14	6.52	10.14	3.62	4.35	9.42	0.0	 e = f
1.04	0.52	12.73	7.27	0.26	56.1	8.57	3.64	2.6	0.0	2.34	3.9	1.04	 f = i
1.66	2.76	0.55	1.66	2.76	4.97	26.52	2.76	5.52	1.1	34.81	14.92	0.0	 g = k
20.18	1.83	14.68	7.34	4.59	10.09	2.75	17.43	8.26	0.0	0.92	6.42	5.5	 h = j
13.08	1.65	8.24	24.4	1.54	9.23	6.59	6.81	10.0	2.53	5.49	8.02	2.42	 i = l
14.33	0.14	6.18	22.05	1.26	3.93	6.04	1.54	4.78	21.07	8.57	1.97	8.15	 j = o
0.46	0.25	0.31	0.77	1.14	1.02	2.6	0.22	1.11	0.31	89.08	2.57	0.15	 k = 0
4.54	1.63	2.36	7.26	4.17	6.17	14.16	6.17	10.16	0.54	16.33	25.95	0.54	 l = s
8.26	0.46	8.72	10.09	0.46	5.05	7.34	0.46	1.38	10.55	2.75	0.46	44.04	 m = u
