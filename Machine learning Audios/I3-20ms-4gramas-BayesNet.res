Timestamp:2013-08-30-05:06:24
Inventario:I3
Intervalo:20ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I3-20ms-4gramas
Num Instances:  9126
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(4): class 
0F1(Hz)(8): class 
0F2(Hz)(6): class 
0F3(Hz)(3): class 
0F4(Hz)(6): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(4): class 
1F1(Hz)(8): class 
1F2(Hz)(8): class 
1F3(Hz)(3): class 
1F4(Hz)(6): class 
2Int(dB)(8): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(4): class 
2F1(Hz)(9): class 
2F2(Hz)(8): class 
2F3(Hz)(5): class 
2F4(Hz)(3): class 
3Int(dB)(8): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(8): class 
3F1(Hz)(9): class 
3F2(Hz)(9): class 
3F3(Hz)(5): class 
3F4(Hz)(6): class 
class(11): 
LogScore Bayes: -355079.230619514
LogScore BDeu: -363357.4806409525
LogScore MDL: -362916.9162223153
LogScore ENTROPY: -353542.7047435063
LogScore AIC: -355598.7047435063




=== Stratified cross-validation ===

Correctly Classified Instances        4860               53.2544 %
Incorrectly Classified Instances      4266               46.7456 %
Kappa statistic                          0.4247
K&B Relative Info Score             391160.3594 %
K&B Information Score                10449.1952 bits      1.145  bits/instance
Class complexity | order 0           24347.7647 bits      2.668  bits/instance
Class complexity | scheme            98026.9745 bits     10.7415 bits/instance
Complexity improvement     (Sf)     -73679.2098 bits     -8.0735 bits/instance
Mean absolute error                      0.0853
Root mean squared error                  0.2707
Relative absolute error                 59.1061 %
Root relative squared error            100.783  %
Coverage of cases (0.95 level)          61.5604 %
Mean rel. region size (0.95 level)      13.8555 %
Total Number of Instances             9126     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,488    0,065    0,597      0,488    0,537      0,460    0,867     0,621     A
                 0,375    0,057    0,371      0,375    0,373      0,317    0,854     0,359     E
                 0,214    0,048    0,512      0,214    0,302      0,240    0,772     0,411     CGJLNQSRT
                 0,080    0,022    0,053      0,080    0,064      0,048    0,782     0,044     FV
                 0,551    0,047    0,340      0,551    0,420      0,401    0,901     0,435     I
                 0,225    0,024    0,442      0,225    0,298      0,276    0,799     0,304     O
                 0,901    0,090    0,846      0,901    0,873      0,800    0,961     0,940     0
                 0,037    0,007    0,145      0,037    0,059      0,058    0,756     0,093     BMP
                 0,408    0,019    0,340      0,408    0,371      0,356    0,896     0,417     U
                 0,200    0,015    0,007      0,200    0,014      0,035    0,915     0,009     DZ
                 0,541    0,136    0,056      0,541    0,101      0,139    0,847     0,073     D
Weighted Avg.    0,533    0,063    0,590      0,533    0,540      0,483    0,873     0,601     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
  735  126   78   28   50  105   73    4   22   11  273 |    a = A
   90  280   27   10   92   12   39    7   28   11  150 |    b = E
  182  156  375   84  141   26  276   25   41   53  392 |    c = CGJLNQSRT
   21   18   24   11    2    6    8    2    2    5   39 |    d = FV
    8   54   28    2  212    0   24    8    4    4   41 |    e = I
   99   46   36    7   30  160   77    6   53   10  188 |    f = O
   21   18  117   45   26    8 2914   13    3   34   35 |    g = 0
   32   34   32   15   53    8   23   11   15    8   68 |    h = BMP
   17   13    6    1   11   34    8    0   89    4   35 |    i = U
    0    0    1    0    0    0    3    0    0    1    0 |    j = DZ
   26    9    8    3    7    3    0    0    5    0   72 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
48.84	8.37	5.18	1.86	3.32	6.98	4.85	0.27	1.46	0.73	18.14	 a = A
12.06	37.53	3.62	1.34	12.33	1.61	5.23	0.94	3.75	1.47	20.11	 b = E
10.39	8.91	21.42	4.8	8.05	1.48	15.76	1.43	2.34	3.03	22.39	 c = CGJLNQSRT
15.22	13.04	17.39	7.97	1.45	4.35	5.8	1.45	1.45	3.62	28.26	 d = FV
2.08	14.03	7.27	0.52	55.06	0.0	6.23	2.08	1.04	1.04	10.65	 e = I
13.9	6.46	5.06	0.98	4.21	22.47	10.81	0.84	7.44	1.4	26.4	 f = O
0.65	0.56	3.62	1.39	0.8	0.25	90.11	0.4	0.09	1.05	1.08	 g = 0
10.7	11.37	10.7	5.02	17.73	2.68	7.69	3.68	5.02	2.68	22.74	 h = BMP
7.8	5.96	2.75	0.46	5.05	15.6	3.67	0.0	40.83	1.83	16.06	 i = U
0.0	0.0	20.0	0.0	0.0	0.0	60.0	0.0	0.0	20.0	0.0	 j = DZ
19.55	6.77	6.02	2.26	5.26	2.26	0.0	0.0	3.76	0.0	54.14	 k = D
