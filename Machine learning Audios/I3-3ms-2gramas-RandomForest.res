Timestamp:2013-07-22-22:25:02
Inventario:I3
Intervalo:3ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I3-3ms-2gramas
Num Instances:  61134
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1658





=== Stratified cross-validation ===

Correctly Classified Instances       53075               86.8175 %
Incorrectly Classified Instances      8059               13.1825 %
Kappa statistic                          0.8298
K&B Relative Info Score            4927215.4427 %
K&B Information Score               128119.5148 bits      2.0957 bits/instance
Class complexity | order 0          158935.881  bits      2.5998 bits/instance
Class complexity | scheme          2740160.8912 bits     44.8222 bits/instance
Complexity improvement     (Sf)    -2581225.0102 bits    -42.2224 bits/instance
Mean absolute error                      0.0371
Root mean squared error                  0.1373
Relative absolute error                 26.2839 %
Root relative squared error             51.6815 %
Coverage of cases (0.95 level)          95.7111 %
Mean rel. region size (0.95 level)      17.1421 %
Total Number of Instances            61134     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,910    0,020    0,894      0,910    0,902      0,883    0,972     0,939     A
                 0,886    0,012    0,856      0,886    0,871      0,860    0,973     0,914     E
                 0,808    0,056    0,765      0,808    0,786      0,736    0,944     0,826     CGJLNQSRT
                 0,613    0,003    0,752      0,613    0,676      0,675    0,894     0,613     FV
                 0,854    0,005    0,877      0,854    0,865      0,860    0,966     0,894     I
                 0,821    0,010    0,866      0,821    0,843      0,832    0,948     0,869     O
                 0,912    0,055    0,913      0,912    0,913      0,857    0,973     0,951     0
                 0,690    0,005    0,808      0,690    0,744      0,739    0,914     0,742     BMP
                 0,893    0,001    0,957      0,893    0,924      0,923    0,982     0,942     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,499     0,000     DZ
                 0,818    0,001    0,906      0,818    0,860      0,859    0,988     0,899     D
Weighted Avg.    0,868    0,037    0,869      0,868    0,868      0,832    0,963     0,902     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  8722    70   421    14    20    54   244    25     4     1    10 |     a = A
    65  4094   232    20    32    21   112    22     9     0    14 |     b = E
   432   227  9096    95    69   152  1049   105    12     9    10 |     c = CGJLNQSRT
    31    32   202   530    11    15    30     6     3     2     2 |     d = FV
    28    55   140     7  2034     9    81    23     3     0     2 |     e = I
   104    42   250     6    14  3666   330    27     3     1    20 |     f = O
   286   169  1179    24    89   222 21630    97    12     3    14 |     g = 0
    54    52   242     8    39    37   164  1344     6     1     1 |     h = BMP
    14    15    67     0     6    17    14    13  1238     0     2 |     i = U
     1     0    15     1     0     1     4     1     0     0     0 |     j = DZ
    22    25    46     0     5    37    21     1     3     0   721 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
91.0	0.73	4.39	0.15	0.21	0.56	2.55	0.26	0.04	0.01	0.1	 a = A
1.41	88.6	5.02	0.43	0.69	0.45	2.42	0.48	0.19	0.0	0.3	 b = E
3.84	2.02	80.81	0.84	0.61	1.35	9.32	0.93	0.11	0.08	0.09	 c = CGJLNQSRT
3.59	3.7	23.38	61.34	1.27	1.74	3.47	0.69	0.35	0.23	0.23	 d = FV
1.18	2.31	5.88	0.29	85.39	0.38	3.4	0.97	0.13	0.0	0.08	 e = I
2.33	0.94	5.6	0.13	0.31	82.14	7.39	0.6	0.07	0.02	0.45	 f = O
1.21	0.71	4.97	0.1	0.38	0.94	91.17	0.41	0.05	0.01	0.06	 g = 0
2.77	2.67	12.42	0.41	2.0	1.9	8.42	68.99	0.31	0.05	0.05	 h = BMP
1.01	1.08	4.83	0.0	0.43	1.23	1.01	0.94	89.32	0.0	0.14	 i = U
4.35	0.0	65.22	4.35	0.0	4.35	17.39	4.35	0.0	0.0	0.0	 j = DZ
2.5	2.84	5.22	0.0	0.57	4.2	2.38	0.11	0.34	0.0	81.84	 k = D
