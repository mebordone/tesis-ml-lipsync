Timestamp:2013-07-22-21:21:22
Inventario:I1
Intervalo:15ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I1-15ms-2gramas
Num Instances:  12188
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3476





=== Stratified cross-validation ===

Correctly Classified Instances        8547               70.1264 %
Incorrectly Classified Instances      3641               29.8736 %
Kappa statistic                          0.6296
K&B Relative Info Score             745155.4198 %
K&B Information Score                21999.4434 bits      1.805  bits/instance
Class complexity | order 0           35957.1789 bits      2.9502 bits/instance
Class complexity | scheme          1171645.1604 bits     96.131  bits/instance
Complexity improvement     (Sf)    -1135687.9816 bits    -93.1808 bits/instance
Mean absolute error                      0.0596
Root mean squared error                  0.1772
Relative absolute error                 47.7415 %
Root relative squared error             70.9139 %
Coverage of cases (0.95 level)          91.106  %
Mean rel. region size (0.95 level)      22.3385 %
Total Number of Instances            12188     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,803    0,066    0,703      0,803    0,749      0,699    0,934     0,826     a
                 0,386    0,017    0,440      0,386    0,411      0,394    0,847     0,348     b
                 0,656    0,042    0,574      0,656    0,612      0,578    0,916     0,610     e
                 0,269    0,004    0,510      0,269    0,353      0,364    0,843     0,283     d
                 0,119    0,006    0,233      0,119    0,157      0,158    0,735     0,093     f
                 0,648    0,019    0,599      0,648    0,623      0,606    0,927     0,618     i
                 0,232    0,011    0,293      0,232    0,259      0,248    0,742     0,165     k
                 0,239    0,002    0,524      0,239    0,328      0,349    0,850     0,309     j
                 0,469    0,058    0,471      0,469    0,470      0,412    0,851     0,417     l
                 0,574    0,031    0,604      0,574    0,589      0,556    0,889     0,600     o
                 0,907    0,061    0,894      0,907    0,901      0,844    0,972     0,950     0
                 0,530    0,024    0,589      0,530    0,558      0,532    0,890     0,545     s
                 0,589    0,005    0,730      0,589    0,652      0,649    0,927     0,672     u
Weighted Avg.    0,701    0,048    0,692      0,701    0,694      0,651    0,920     0,713     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1597   14   81    7    6   20   15    2   88   55   69   30    5 |    a = a
   38  154   23    2    7   23    5    2   67   26   32   11    9 |    b = b
   84   21  637    6    7   41    6    5   82   36   22   18    6 |    c = e
   34    4   17   49    2    4    0    1   36   29    0    3    3 |    d = d
   22   14   21    4   21    6    1    3   35   18    9   22    1 |    e = f
   19   16   64    0    2  326    1    1   39    6   17   10    2 |    f = i
   18    5   11    1    2   11   55    1   25   11   57   36    4 |    g = k
   35    1   19    0    1   11    1   33    7    8    7   14    1 |    h = j
  183   44  112   10   15   43   22    6  570   56  100   41   14 |    i = l
  110   35   49   10    7    7    6    1   80  540   68   14   13 |    j = o
   66   20   31    1    8   25   37    4   88   60 4008   67    2 |    k = 0
   53   12   31    3   12   25   36    2   64   13   92  389    2 |    l = s
   14   10   14    3    0    2    3    2   28   36    0    5  168 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
80.29	0.7	4.07	0.35	0.3	1.01	0.75	0.1	4.42	2.77	3.47	1.51	0.25	 a = a
9.52	38.6	5.76	0.5	1.75	5.76	1.25	0.5	16.79	6.52	8.02	2.76	2.26	 b = b
8.65	2.16	65.6	0.62	0.72	4.22	0.62	0.51	8.44	3.71	2.27	1.85	0.62	 c = e
18.68	2.2	9.34	26.92	1.1	2.2	0.0	0.55	19.78	15.93	0.0	1.65	1.65	 d = d
12.43	7.91	11.86	2.26	11.86	3.39	0.56	1.69	19.77	10.17	5.08	12.43	0.56	 e = f
3.78	3.18	12.72	0.0	0.4	64.81	0.2	0.2	7.75	1.19	3.38	1.99	0.4	 f = i
7.59	2.11	4.64	0.42	0.84	4.64	23.21	0.42	10.55	4.64	24.05	15.19	1.69	 g = k
25.36	0.72	13.77	0.0	0.72	7.97	0.72	23.91	5.07	5.8	5.07	10.14	0.72	 h = j
15.05	3.62	9.21	0.82	1.23	3.54	1.81	0.49	46.88	4.61	8.22	3.37	1.15	 i = l
11.7	3.72	5.21	1.06	0.74	0.74	0.64	0.11	8.51	57.45	7.23	1.49	1.38	 j = o
1.49	0.45	0.7	0.02	0.18	0.57	0.84	0.09	1.99	1.36	90.74	1.52	0.05	 k = 0
7.22	1.63	4.22	0.41	1.63	3.41	4.9	0.27	8.72	1.77	12.53	53.0	0.27	 l = s
4.91	3.51	4.91	1.05	0.0	0.7	1.05	0.7	9.82	12.63	0.0	1.75	58.95	 m = u
