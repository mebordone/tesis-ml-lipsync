Timestamp:2013-08-30-04:13:25
Inventario:I1
Intervalo:3ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I1-3ms-2gramas
Num Instances:  61134
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(21): class 
0Pitch(Hz)(28): class 
0RawPitch(19): class 
0SmPitch(27): class 
0Melogram(st)(31): class 
0ZCross(15): class 
0F1(Hz)(280): class 
0F2(Hz)(355): class 
0F3(Hz)(427): class 
0F4(Hz)(299): class 
1Int(dB)(21): class 
1Pitch(Hz)(29): class 
1RawPitch(20): class 
1SmPitch(28): class 
1Melogram(st)(37): class 
1ZCross(15): class 
1F1(Hz)(301): class 
1F2(Hz)(250): class 
1F3(Hz)(382): class 
1F4(Hz)(354): class 
class(13): 
LogScore Bayes: -2566144.701062941
LogScore BDeu: -2927675.156010396
LogScore MDL: -2860000.74175412
LogScore ENTROPY: -2650831.022986107
LogScore AIC: -2688790.022986107




=== Stratified cross-validation ===

Correctly Classified Instances       40219               65.7883 %
Incorrectly Classified Instances     20915               34.2117 %
Kappa statistic                          0.5665
K&B Relative Info Score            3477990.6152 %
K&B Information Score               100640.9648 bits      1.6462 bits/instance
Class complexity | order 0          176878.6521 bits      2.8933 bits/instance
Class complexity | scheme           394340.988  bits      6.4504 bits/instance
Complexity improvement     (Sf)    -217462.3358 bits     -3.5571 bits/instance
Mean absolute error                      0.0542
Root mean squared error                  0.2098
Relative absolute error                 44.1694 %
Root relative squared error             84.7139 %
Coverage of cases (0.95 level)          75.8122 %
Mean rel. region size (0.95 level)      11.9124 %
Total Number of Instances            61134     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,673    0,042    0,750      0,673    0,709      0,660    0,925     0,786     a
                 0,237    0,009    0,470      0,237    0,315      0,319    0,840     0,313     b
                 0,654    0,058    0,479      0,654    0,553      0,518    0,916     0,599     e
                 0,502    0,031    0,196      0,502    0,281      0,298    0,914     0,312     d
                 0,236    0,011    0,240      0,236    0,238      0,227    0,853     0,206     f
                 0,696    0,027    0,508      0,696    0,587      0,576    0,938     0,618     i
                 0,148    0,008    0,272      0,148    0,191      0,189    0,886     0,202     k
                 0,352    0,007    0,366      0,352    0,359      0,352    0,885     0,324     j
                 0,303    0,036    0,468      0,303    0,368      0,326    0,835     0,393     l
                 0,483    0,023    0,619      0,483    0,543      0,516    0,878     0,575     o
                 0,898    0,123    0,823      0,898    0,859      0,764    0,959     0,953     0
                 0,400    0,018    0,578      0,400    0,473      0,455    0,924     0,486     s
                 0,659    0,017    0,478      0,659    0,554      0,550    0,955     0,647     u
Weighted Avg.    0,658    0,067    0,658      0,658    0,649      0,595    0,922     0,710     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  6449    60   664   415    96   152    74    72   499   408   463   118   115 |     a = a
   112   462   202   125    51   192    25    30   158    83   336    44   128 |     b = b
   306    74  3021   170    64   247    45    64   255    81   168    45    81 |     c = e
    87    19   104   454    18    28     2     2    78    46    28     8    30 |     d = d
    54    24   113    65   204    51    18     2    51    35   199    31    17 |     e = f
    19    33   294    29    33  1659    31    18    76     7   113    42    28 |     f = i
    22    16    32     5    14    39   176    10    48    11   596   205    18 |     g = k
    76     3    97    12    16    55    12   244    24    17    83    46     8 |     h = j
   477   103   884   380    92   400    85    69  1766   234   852   212   265 |     i = l
   563    38   322   340    33    60    36    41   149  2154   473    30   224 |     j = o
   286    92   383   177   158   250    85    80   386   228 21296   242    62 |     k = 0
   102    34   115    93    57   129    43    33   213    36  1254  1420    23 |     l = s
    48    24    71    57    13     6    16     2    68   137    18    12   914 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
67.28	0.63	6.93	4.33	1.0	1.59	0.77	0.75	5.21	4.26	4.83	1.23	1.2	 a = a
5.75	23.72	10.37	6.42	2.62	9.86	1.28	1.54	8.11	4.26	17.25	2.26	6.57	 b = b
6.62	1.6	65.38	3.68	1.38	5.35	0.97	1.38	5.52	1.75	3.64	0.97	1.75	 c = e
9.62	2.1	11.5	50.22	1.99	3.1	0.22	0.22	8.63	5.09	3.1	0.88	3.32	 d = d
6.25	2.78	13.08	7.52	23.61	5.9	2.08	0.23	5.9	4.05	23.03	3.59	1.97	 e = f
0.8	1.39	12.34	1.22	1.39	69.65	1.3	0.76	3.19	0.29	4.74	1.76	1.18	 f = i
1.85	1.34	2.68	0.42	1.17	3.27	14.77	0.84	4.03	0.92	50.0	17.2	1.51	 g = k
10.97	0.43	14.0	1.73	2.31	7.94	1.73	35.21	3.46	2.45	11.98	6.64	1.15	 h = j
8.2	1.77	15.19	6.53	1.58	6.87	1.46	1.19	30.35	4.02	14.64	3.64	4.55	 i = l
12.61	0.85	7.21	7.62	0.74	1.34	0.81	0.92	3.34	48.26	10.6	0.67	5.02	 j = o
1.21	0.39	1.61	0.75	0.67	1.05	0.36	0.34	1.63	0.96	89.76	1.02	0.26	 k = 0
2.87	0.96	3.24	2.62	1.6	3.63	1.21	0.93	6.0	1.01	35.3	39.98	0.65	 l = s
3.46	1.73	5.12	4.11	0.94	0.43	1.15	0.14	4.91	9.88	1.3	0.87	65.95	 m = u
