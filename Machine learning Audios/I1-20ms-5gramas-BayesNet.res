Timestamp:2013-08-30-04:19:09
Inventario:I1
Intervalo:20ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I1-20ms-5gramas
Num Instances:  9125
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 4Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  42 4Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  43 4RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  44 4SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  45 4Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  47 4F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  48 4F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  49 4F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  50 4F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(4): class 
0F1(Hz)(6): class 
0F2(Hz)(5): class 
0F3(Hz)(3): class 
0F4(Hz)(3): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(4): class 
1F1(Hz)(6): class 
1F2(Hz)(5): class 
1F3(Hz)(3): class 
1F4(Hz)(3): class 
2Int(dB)(8): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(5): class 
2ZCross(5): class 
2F1(Hz)(8): class 
2F2(Hz)(7): class 
2F3(Hz)(3): class 
2F4(Hz)(3): class 
3Int(dB)(8): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(7): class 
3F1(Hz)(9): class 
3F2(Hz)(8): class 
3F3(Hz)(3): class 
3F4(Hz)(3): class 
4Int(dB)(9): class 
4Pitch(Hz)(4): class 
4RawPitch(4): class 
4SmPitch(4): class 
4Melogram(st)(7): class 
4ZCross(8): class 
4F1(Hz)(10): class 
4F2(Hz)(9): class 
4F3(Hz)(6): class 
4F4(Hz)(5): class 
class(13): 
LogScore Bayes: -427356.3651874068
LogScore BDeu: -439023.5876511123
LogScore MDL: -437857.22236701933
LogScore ENTROPY: -424999.75218540384
LogScore AIC: -427819.75218540384




=== Stratified cross-validation ===

Correctly Classified Instances        4578               50.1699 %
Incorrectly Classified Instances      4547               49.8301 %
Kappa statistic                          0.4013
K&B Relative Info Score             357691.9745 %
K&B Information Score                10639.371  bits      1.166  bits/instance
Class complexity | order 0           27122.0401 bits      2.9723 bits/instance
Class complexity | scheme           119874.4271 bits     13.1369 bits/instance
Complexity improvement     (Sf)     -92752.3871 bits    -10.1646 bits/instance
Mean absolute error                      0.0771
Root mean squared error                  0.2548
Relative absolute error                 61.3286 %
Root relative squared error            101.6543 %
Coverage of cases (0.95 level)          60.2192 %
Mean rel. region size (0.95 level)      12.9003 %
Total Number of Instances             9125     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,434    0,054    0,615      0,434    0,509      0,440    0,861     0,625     a
                 0,030    0,007    0,123      0,030    0,048      0,046    0,746     0,090     b
                 0,279    0,033    0,432      0,279    0,339      0,302    0,841     0,356     e
                 0,435    0,099    0,063      0,435    0,110      0,134    0,833     0,077     d
                 0,051    0,017    0,044      0,051    0,047      0,031    0,774     0,040     f
                 0,545    0,048    0,334      0,545    0,414      0,395    0,888     0,395     i
                 0,265    0,052    0,094      0,265    0,139      0,130    0,812     0,079     k
                 0,303    0,053    0,065      0,303    0,107      0,118    0,833     0,050     j
                 0,138    0,039    0,281      0,138    0,185      0,137    0,770     0,226     l
                 0,208    0,024    0,428      0,208    0,280      0,259    0,791     0,293     o
                 0,879    0,068    0,877      0,879    0,878      0,811    0,960     0,947     0
                 0,274    0,037    0,324      0,274    0,297      0,257    0,824     0,300     s
                 0,385    0,019    0,337      0,385    0,360      0,344    0,884     0,393     u
Weighted Avg.    0,502    0,050    0,559      0,502    0,515      0,467    0,873     0,564     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
  653    9   91  229   24   59   53   87   70   94   71   41   24 |    a = a
   25    9    9   43   11   57   19   44   25   10   17   20   10 |    b = b
   56    6  208  121    7  102   31   94   31   14   33   15   28 |    c = e
   22    0    9   60    2    4    5    7   18    3    2    1    5 |    d = d
   14    2   16   21    7    2   12   17   20    5    6   15    1 |    e = f
    4    6   27   35    2  210   28   32    8    0   14   14    5 |    f = i
    3    2    1    5    9    7   48   11    7    1   58   28    1 |    g = k
   15    2   11    8    5    9    4   33   10    0    0    7    5 |    h = j
  109   13   48  210   16   81   68   80  126   25   36   76   22 |    i = l
  101    3   40  139    9   32   32   24   42  148   73   16   53 |    j = o
   15   10    6   31   38   25  120   17   35    9 2841   81    5 |    k = 0
   25   10    5   34   29   29   71   51   53    4   83  151    6 |    l = s
   19    1   10   18    1   12   18   12    4   33    5    1   84 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
43.39	0.6	6.05	15.22	1.59	3.92	3.52	5.78	4.65	6.25	4.72	2.72	1.59	 a = a
8.36	3.01	3.01	14.38	3.68	19.06	6.35	14.72	8.36	3.34	5.69	6.69	3.34	 b = b
7.51	0.8	27.88	16.22	0.94	13.67	4.16	12.6	4.16	1.88	4.42	2.01	3.75	 c = e
15.94	0.0	6.52	43.48	1.45	2.9	3.62	5.07	13.04	2.17	1.45	0.72	3.62	 d = d
10.14	1.45	11.59	15.22	5.07	1.45	8.7	12.32	14.49	3.62	4.35	10.87	0.72	 e = f
1.04	1.56	7.01	9.09	0.52	54.55	7.27	8.31	2.08	0.0	3.64	3.64	1.3	 f = i
1.66	1.1	0.55	2.76	4.97	3.87	26.52	6.08	3.87	0.55	32.04	15.47	0.55	 g = k
13.76	1.83	10.09	7.34	4.59	8.26	3.67	30.28	9.17	0.0	0.0	6.42	4.59	 h = j
11.98	1.43	5.27	23.08	1.76	8.9	7.47	8.79	13.85	2.75	3.96	8.35	2.42	 i = l
14.19	0.42	5.62	19.52	1.26	4.49	4.49	3.37	5.9	20.79	10.25	2.25	7.44	 j = o
0.46	0.31	0.19	0.96	1.18	0.77	3.71	0.53	1.08	0.28	87.88	2.51	0.15	 k = 0
4.54	1.81	0.91	6.17	5.26	5.26	12.89	9.26	9.62	0.73	15.06	27.4	1.09	 l = s
8.72	0.46	4.59	8.26	0.46	5.5	8.26	5.5	1.83	15.14	2.29	0.46	38.53	 m = u
