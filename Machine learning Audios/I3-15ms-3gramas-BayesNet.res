Timestamp:2013-08-30-05:05:49
Inventario:I3
Intervalo:15ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I3-15ms-3gramas
Num Instances:  12187
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(8): class 
0ZCross(7): class 
0F1(Hz)(10): class 
0F2(Hz)(9): class 
0F3(Hz)(7): class 
0F4(Hz)(7): class 
1Int(dB)(10): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(9): class 
1ZCross(9): class 
1F1(Hz)(10): class 
1F2(Hz)(11): class 
1F3(Hz)(8): class 
1F4(Hz)(7): class 
2Int(dB)(9): class 
2Pitch(Hz)(6): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(10): class 
2F1(Hz)(11): class 
2F2(Hz)(9): class 
2F3(Hz)(8): class 
2F4(Hz)(7): class 
class(11): 
LogScore Bayes: -410220.2710755959
LogScore BDeu: -419294.2456211552
LogScore MDL: -418651.91874135553
LogScore ENTROPY: -408825.13208607945
LogScore AIC: -410914.13208607945




=== Stratified cross-validation ===

Correctly Classified Instances        6867               56.3469 %
Incorrectly Classified Instances      5320               43.6531 %
Kappa statistic                          0.4528
K&B Relative Info Score             561376.6297 %
K&B Information Score                14885.1169 bits      1.2214 bits/instance
Class complexity | order 0           32288.5474 bits      2.6494 bits/instance
Class complexity | scheme           112797.7372 bits      9.2556 bits/instance
Complexity improvement     (Sf)     -80509.1898 bits     -6.6062 bits/instance
Mean absolute error                      0.0799
Root mean squared error                  0.2618
Relative absolute error                 55.6588 %
Root relative squared error             97.7064 %
Coverage of cases (0.95 level)          64.7001 %
Mean rel. region size (0.95 level)      13.6345 %
Total Number of Instances            12187     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,575    0,066    0,629      0,575    0,601      0,528    0,886     0,679     A
                 0,486    0,063    0,401      0,486    0,439      0,388    0,872     0,417     E
                 0,179    0,057    0,425      0,179    0,252      0,176    0,771     0,382     CGJLNQSRT
                 0,062    0,015    0,058      0,062    0,060      0,045    0,778     0,042     FV
                 0,600    0,041    0,384      0,600    0,469      0,453    0,915     0,428     I
                 0,240    0,015    0,565      0,240    0,337      0,337    0,821     0,388     O
                 0,916    0,116    0,818      0,916    0,864      0,783    0,966     0,955     0
                 0,033    0,005    0,183      0,033    0,055      0,065    0,773     0,101     BMP
                 0,477    0,016    0,424      0,477    0,449      0,436    0,913     0,469     U
                 0,000    0,007    0,000      0,000    0,000      -0,002   0,913     0,003     DZ
                 0,593    0,109    0,074      0,593    0,132      0,181    0,869     0,094     D
Weighted Avg.    0,563    0,074    0,589      0,563    0,555      0,495    0,883     0,627     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1143  162  143   34   50   55   98    6   11    5  282 |    a = A
  108  472   82    9   72   13   40    1   33    6  135 |    b = E
  248  234  416   67  198   36  566   26   50   38  446 |    c = CGJLNQSRT
   25   28   27   11    6    9   22    4    1    3   41 |    d = FV
   13   79   47    2  302    1   27    3    2    1   26 |    e = I
  148   76   76   10   32  226   89    9   57    7  210 |    f = O
   45   40  118   38   42   16 4043    7    6   18   43 |    g = 0
   45   57   41   15   71    9   45   13   19    3   81 |    h = BMP
   15   20   20    2    3   29    9    0  136    4   47 |    i = U
    0    0    0    0    0    0    5    0    0    0    0 |    j = DZ
   27    9    9    3   10    6    0    2    6    0  105 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
57.47	8.14	7.19	1.71	2.51	2.77	4.93	0.3	0.55	0.25	14.18	 a = A
11.12	48.61	8.44	0.93	7.42	1.34	4.12	0.1	3.4	0.62	13.9	 b = E
10.67	10.06	17.89	2.88	8.52	1.55	24.34	1.12	2.15	1.63	19.18	 c = CGJLNQSRT
14.12	15.82	15.25	6.21	3.39	5.08	12.43	2.26	0.56	1.69	23.16	 d = FV
2.58	15.71	9.34	0.4	60.04	0.2	5.37	0.6	0.4	0.2	5.17	 e = I
15.74	8.09	8.09	1.06	3.4	24.04	9.47	0.96	6.06	0.74	22.34	 f = O
1.02	0.91	2.67	0.86	0.95	0.36	91.55	0.16	0.14	0.41	0.97	 g = 0
11.28	14.29	10.28	3.76	17.79	2.26	11.28	3.26	4.76	0.75	20.3	 h = BMP
5.26	7.02	7.02	0.7	1.05	10.18	3.16	0.0	47.72	1.4	16.49	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
15.25	5.08	5.08	1.69	5.65	3.39	0.0	1.13	3.39	0.0	59.32	 k = D
