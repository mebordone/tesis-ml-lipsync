Timestamp:2014-12-07-10:34:34
Inventario:I0
Intervalo:2ms
Ngramas:2
Clasificador:NaiveBayes
Relation Name:  I0-2ms-2gramas
Num Instances:  91702
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    27 


Naive Bayes Classifier

                    Class
Attribute              ch        rr         0        hs        hg         a         c         b         e         d         g         f         i         j         m         l         o         n         q         p         s         r         u         t         v         y         z
                      (0)       (0)    (0.39)    (0.01)       (0)    (0.14)    (0.01)    (0.01)    (0.08)    (0.01)    (0.01)    (0.01)    (0.04)    (0.01)    (0.01)    (0.02)    (0.07)    (0.03)       (0)    (0.01)    (0.05)    (0.02)    (0.02)    (0.02)    (0.01)       (0)       (0)
==============================================================================================================================================================================================================================================================================================
0Int(dB)
  mean            -19.5109  -11.0073  -48.2526  -20.9187  -10.9119   -2.8635  -24.5216   -5.9294   -5.1801   -7.7997    -7.701  -22.0464   -8.7172  -15.7648   -7.5829   -8.2546    -8.146   -9.3069  -22.3902  -22.7543  -21.6704  -11.9208   -5.2139  -25.5417   -9.2274  -16.3545  -26.9195
  std. dev.         8.2851    4.3947   16.2021   11.2611    6.2832     8.205    9.9872    4.2721     8.069    6.1523    8.0193    5.4006    8.3524       6.1    6.0121    5.6912   12.1698    7.6889   11.3302   10.5956    6.5601    9.3387    7.7793   10.1924    6.7468    4.7964    2.0302
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039

0Pitch(Hz)
  mean              53.055  245.4112   16.6922   81.4297  147.2402  226.5987   52.1217  216.1255  190.0868  202.2726  211.8361   74.5716  173.0539  152.4343  193.9847   174.651  178.5997  189.8479   82.7165   80.8753   45.0192  161.8022  229.3808   70.4675  177.7413  112.9413         0
  std. dev.       101.6332  121.6508   56.2797   86.7208   38.6144  103.4873   96.0705   72.3848   87.9634   84.5208  101.2436  114.9864   76.5412  131.6271   87.2817   64.2206   105.265   90.2682  115.2103  114.4697   88.2177   95.5801    84.526  109.6127   72.5746   69.2632    0.0207
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242

0RawPitch
  mean             35.0966  237.4787   13.6783   63.5842  125.6235  216.9833   35.7069  213.7345  185.5725  195.9629  201.2902   37.3932  165.7052    94.585  182.9788  158.1608  172.7286  176.3158   42.8127    61.901   24.5966  155.4059  220.4839   45.1998  163.2642   76.9389         0
  std. dev.        85.5772  130.3747   50.8125   84.1847   81.4257  109.5973    82.006   75.7161   95.2181    94.473  112.1972   78.9964   83.3185  113.5516   98.3378   78.6389  112.6688   103.679    90.899  107.8677   67.9056  103.0698   93.0053   92.3545   84.7866   77.6904    0.0207
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243

0SmPitch
  mean             49.2589  245.4038   15.0746   75.0475  149.1843  224.8531   44.3406  216.1252  188.5362  201.7143  210.1091   56.9592  170.6841  109.9994  191.0396  173.0212  176.5062   187.086   65.3564    71.317   37.6125  158.7242  228.8712   59.7232  173.5937  108.3503         0
  std. dev.        98.2717  121.6464    52.489   83.7122   36.5302  105.0838   91.2308   72.3842   89.2414   85.5991  103.2196   96.9061   79.0929   137.282   91.5792   66.2464  107.4391   92.9814  107.1905  112.1782   82.2768   97.2184   84.8121  104.3006   77.3273   71.3178    0.0208
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248

0Melogram(st)
  mean             14.3645   56.5827    4.9293   25.3245   49.3918   53.2812    14.256   55.2076   50.4037   53.0165   51.1216   20.5647   48.4912   15.2451   50.9419    51.089   46.5516   49.7864   18.1628   20.9437    12.231   42.8431   54.0704   16.9436   50.6286    40.174         0
  std. dev.        25.1228    8.9962    15.628   25.0675    4.6052    14.633   24.6395    7.3502   15.4597   11.3502   17.8042   26.9454   16.2162   25.9051   14.5333     9.813    20.484   16.8012   26.6158   27.4663   22.9916   22.5552   14.2652    26.274   11.5084   18.3961    0.0041
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246

0ZCross
  mean              8.6263    3.4928    0.2535    0.9954     3.344    7.6556    1.6523    4.1942    5.2659    4.1291    4.6044    0.6301    3.7314    3.8397    2.8999    3.3331    4.4315    2.5136    4.2429    1.0209    3.6677    3.2398    4.0992     0.817     3.257    1.5047         0
  std. dev.        12.8551     2.655    1.1335    1.7306    2.4749    4.3982    3.6493    1.8633    3.2124    2.2988    2.8002     1.563    3.2172    3.8168    1.5523    2.0447    3.0586    1.6801    7.2062    2.3298    7.0481    2.5951    2.9273    1.7098    2.0151    1.7367    0.1792
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755

0F1(Hz)
  mean            128.2914  606.6561   53.8361   377.559  445.9981  707.5898   96.5191  478.7776   463.877  517.6211  493.6582  264.6058  361.6146  444.4025  431.9925  470.7042  484.2054  464.4221  178.9209  229.5986  173.7896  482.9867  498.0573  114.8691  481.3778  339.0682         0
  std. dev.       253.6563  135.8221  166.7222  301.1992   84.9548  255.0771  210.0173  132.9683   146.053  153.3903  193.7355  331.4298  144.1442  346.8653  162.9402  139.8782  226.2125  156.5183  244.9245   293.483  278.8392   236.414   307.944  211.3763  170.3673  184.8167    0.0608
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648

0F2(Hz)
  mean             388.191 1642.3654  168.5623 1087.3619 1933.3843 1459.3145   281.343 1574.8537  1785.616 1484.0387  1685.462  688.1209 1974.0792 1247.7422 1666.6278 1669.5867 1140.4987 1596.3773  719.3351  681.5872  542.5552 1365.7541  1053.554  382.8591 1565.3074 1340.2619         0
  std. dev.       735.3138  128.3605  516.6376  847.2366  347.9232  396.5725  626.9333  345.2052  528.1297  321.3149  595.4503  840.7309  669.1502  913.5333  436.4381  340.5242  535.6978  455.6778  956.3571  841.6713  844.8303  621.6712  426.6176  686.2131  417.5257  668.9618    0.0738
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429

0F3(Hz)
  mean            640.1455 2864.7468  287.4189  1776.831 2525.9047 2721.5862  515.2883 2756.1064 2619.8245 2868.9125 2892.2992 1172.7962 2655.6632  1839.866 2749.0804 2811.0606  2499.568 2757.8361 1074.4145 1159.6036  901.6451 2375.2496  2673.852  691.9532 2666.5649 2305.4337         0
  std. dev.      1212.3465  414.6611  866.0117 1323.5103  113.6465   730.267  1108.999  469.1644  786.5019  503.7764  938.5627 1427.4483  922.4738 1336.2367  565.8469   581.352 1024.8703  674.8642 1393.0709  1419.103 1388.2759  1088.151  787.5699 1228.7246  680.3656 1165.4962    0.1103
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision          0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662

0F4(Hz)
  mean            759.1191 3240.2202  345.4862  2212.445 3589.7874 3133.6693  600.4093 3298.5511 3157.1809  3321.865  2929.841 1351.3161 3159.0187 2300.3113 3313.8615 3236.6187 2876.1426 3165.1653 1296.8087 1381.1823 1018.8495 2747.0761  3063.919  796.0222 3234.2907 2673.9087         0
  std. dev.      1437.6027  405.9827  1038.639 1634.5471   90.0103  828.2659 1293.8916  440.6092  913.7543  550.7347  952.5181 1634.1033 1054.7924 1658.6161  611.0322  588.5113 1161.7476   778.559 1692.8335 1668.5326 1566.5853 1205.2967  806.6532 1418.5861  713.2798 1340.9217    0.1138
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829

1Int(dB)
  mean             -19.167  -11.1625  -48.3069  -21.3733  -10.7149   -2.8733  -23.7499   -5.8567    -5.199   -7.7016    -7.722  -22.1621    -8.676  -15.7996   -7.5498   -8.2338   -8.1936   -9.4747  -21.6533  -22.2328  -21.7905  -11.9633   -5.0927   -24.646   -9.0796  -16.2809  -26.9132
  std. dev.         8.1498    4.3485   16.1114   11.2969    6.3965     8.153    10.064    4.2899    8.0531     6.187    8.0048     5.303    8.3577    6.1062    5.9847    5.7838   12.2208    7.8239   11.1291    10.732    6.5355    9.4776    7.5769   10.6113    6.5355    4.7292    1.9983
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037

1Pitch(Hz)
  mean             56.1744  245.4989   16.3383   78.7475  147.7994  226.6129   58.8225  216.1719  190.3791  202.2426  211.6788   72.9488  173.4856   151.728  194.2012  174.5722  178.0537  188.9788   86.5762   82.9837   43.4105  161.9067  229.8416   76.3545   178.673  113.0563         0
  std. dev.       103.5574  122.1103   55.7277   86.2324   39.2295  103.4134  100.8457   72.2007   87.7367   84.5819  101.7909  114.3539   76.0683  131.4173    87.328   64.1927  105.2226   90.9256  117.3245  113.9286   87.1375   95.8943   83.5753  112.1165   71.4053   69.2765    0.0207
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242

1RawPitch
  mean             36.8283  237.1082   13.3728   61.0304  126.6695   217.004   41.3269   214.048   185.837  195.8324  201.3727     35.58  166.2787   93.7222  183.0521  158.1521  172.2852  175.0833   46.7847   64.3873   22.8249  155.0697  220.9278   51.7319  164.1733   78.1119         0
  std. dev.        86.8567  131.2913   50.3485   83.1284   81.3085  109.5778   87.5523   75.3247   95.0002    94.326  112.4119   76.9137   83.0617  112.7751   98.5689   79.0227  112.5036  104.3204   94.8817  108.0557   65.5791  103.4592   92.1417   97.0795   83.6029   77.8304    0.0207
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243

1SmPitch
  mean             51.4241   245.493   14.7407   72.3683   149.744  224.9527   50.8196  216.1717  188.9642  201.7614  210.3094   55.3229  171.0525  109.2932  191.0026  172.7326  175.9682  185.8484   68.5748   73.3523   35.9417  158.7909  229.2536   65.8623  174.6514  108.4533         0
  std. dev.        99.8801  122.1055   51.9295   83.0011   37.1513  104.9064     96.64   72.2005   88.9263   85.5097  103.3642   95.8289   78.7886  136.8568   91.9871   66.4974  107.3853     93.87  109.6847  111.7359   80.8162   97.5388   84.0032   107.596   76.2155     71.34    0.0208
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248

1Melogram(st)
  mean             15.0364   56.5644    4.8254   24.5896   49.4453   53.3304   15.5492   55.2167   50.4842   53.0765   51.0353   20.2223   48.6341   15.3027    50.967   51.0478   46.4749   49.5504   19.1315   21.5242   11.7665   42.8546   54.3072   18.3824   50.8827   40.1928         0
  std. dev.        25.5007    9.0321   15.4831   25.0323    4.6454   14.5188   25.3908    7.3415   15.3213   11.1742    17.943   26.8757   15.9951   25.9932   14.5387    9.8934   20.5262   17.1147   26.9971   27.5209   22.7033   22.5632   13.7628   26.8468   11.0179   18.4023    0.0041
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246

1ZCross
  mean              8.6965    3.5125    0.2484    0.9526    3.4311    7.6409    1.7471    4.2516    5.2438    4.1913    4.6044    0.6024    3.7483      3.82    2.9073    3.3424    4.4237    2.4864    4.2993    1.0937    3.6553    3.2352    4.1111    0.9711    3.2829     1.524         0
  std. dev.        12.8365    2.6848    1.1311    1.7085    2.5088    4.4087    3.6599    1.8832    3.2168    2.3027    2.7858    1.5323    3.2275    3.8153     1.545    2.0294    3.0692    1.6824    7.1565    2.3383    7.0516    2.5918    2.9164    1.8529    2.0133    1.7378    0.1792
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755

1F1(Hz)
  mean            123.1942  610.3237   52.7328  368.3069  446.2683  711.4514  103.9964  479.6898  466.4894  518.4309  493.4169  253.9126    363.75  441.3575  431.6196  469.7752  486.2046  460.1312  179.5033  230.2918  164.4121  481.1626  500.6341  121.5571  481.6653  338.4737         0
  std. dev.       248.6749   136.924  165.0999  303.3364   86.2804  251.2217  215.8387  132.7071  142.2388  153.3851  193.2624   328.652  140.8431  349.6798  162.0165  137.8408  225.0865  156.9404  245.2362  291.8796  273.8217  235.4575  305.2064  216.3685  167.2786   184.244    0.0608
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648

1F2(Hz)
  mean            376.7654 1635.2349  165.1158  1060.434 1928.7405 1464.3906    301.66 1572.2172 1798.4442 1484.1075 1683.3446  663.3563  1993.047 1228.3789 1668.4402 1670.8104 1140.7371 1591.1826  718.1291  683.5759  514.6794 1366.8989 1061.2779  404.8713 1576.6312 1342.8267         0
  std. dev.       728.5768  126.5237  511.8646  852.8257  349.1637  386.4941  641.4848  344.4831  511.8666  323.4684  595.5085  836.3005  650.7685   916.305  429.7958  333.6483  531.1147  461.0869  954.1605  837.7058  831.9432  622.7216  416.4942  702.9583  402.2539  670.1431    0.0738
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429

1F3(Hz)
  mean            616.4638 2865.6822  281.5246 1732.2063 2524.3394 2731.7626  554.9263 2754.3524 2634.9785 2868.2959 2891.9207 1126.9161 2677.3565 1815.2793 2749.5582 2817.0102 2506.4348 2749.7681 1073.0478 1166.3549  854.4172 2375.0752 2694.0925  725.1378 2682.0069 2305.2749         0
  std. dev.      1191.5389  419.1793  858.0542 1335.6596  113.0258  712.9707 1138.7381  469.7834  764.1593  510.0445  938.3439 1415.7652  898.3116 1341.4367  555.7281  574.4479 1018.6433   687.776 1391.3525 1416.0614 1366.2348 1089.0427  755.1652  1247.971  652.7644 1165.0756    0.1103
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision          0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662

1F4(Hz)
  mean              737.11  3236.831  338.5171 2157.8741 3590.7467  3144.537  651.7847 3297.5319   3174.33 3319.7801 2931.5846 1301.8229 3182.3609 2273.9788 3317.6185 3241.5892 2882.7851 3156.8141 1294.2891 1394.6656  966.9549  2746.307 3086.3027  837.3184 3253.5009 2674.3681         0
  std. dev.      1424.4748  401.7635  1029.399 1650.7102   89.0647  807.8569 1338.2161  439.7233  885.5429  557.8761  953.5669 1625.1562 1024.5862 1668.8588  598.0784  577.5834 1153.5603  794.8923 1689.1398 1671.0354 1544.0952 1207.1513   763.546  1446.252  671.0726 1341.4249    0.1138
  weight sum           429       109     35832       980       247     13106      1361       599      6910      1313       544       582      3561       491      1311      1613      6646      3164       419      1005      4423      2084      2080      1710       704       446        33
  precision         0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829





=== Stratified cross-validation ===

Correctly Classified Instances       28969               31.5904 %
Incorrectly Classified Instances     62733               68.4096 %
Kappa statistic                          0.237 
K&B Relative Info Score            1846175.2555 %
K&B Information Score                60775.5137 bits      0.6628 bits/instance
Class complexity | order 0          301796.917  bits      3.2911 bits/instance
Class complexity | scheme          3345491.5927 bits     36.4822 bits/instance
Complexity improvement     (Sf)    -3043694.6757 bits    -33.1911 bits/instance
Mean absolute error                      0.0512
Root mean squared error                  0.2082
Relative absolute error                 85.6012 %
Root relative squared error            120.3647 %
Coverage of cases (0.95 level)          42.8551 %
Mean rel. region size (0.95 level)       8.5551 %
Total Number of Instances            91702     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,191    0,003    0,255      0,191    0,218      0,217    0,620     0,104     ch
                 0,266    0,010    0,032      0,266    0,057      0,089    0,896     0,046     rr
                 0,504    0,015    0,955      0,504    0,660      0,589    0,884     0,865     0
                 0,122    0,023    0,055      0,122    0,076      0,067    0,617     0,037     hs
                 0,931    0,126    0,020      0,931    0,038      0,125    0,953     0,104     hg
                 0,524    0,049    0,640      0,524    0,576      0,517    0,887     0,624     a
                 0,035    0,004    0,117      0,035    0,054      0,057    0,459     0,044     c
                 0,357    0,104    0,022      0,357    0,042      0,066    0,827     0,026     b
                 0,096    0,009    0,451      0,096    0,158      0,181    0,765     0,238     e
                 0,022    0,009    0,034      0,022    0,027      0,016    0,779     0,035     d
                 0,013    0,001    0,080      0,013    0,022      0,030    0,750     0,023     g
                 0,003    0,002    0,013      0,003    0,005      0,004    0,484     0,016     f
                 0,082    0,002    0,583      0,082    0,144      0,209    0,841     0,308     i
                 0,149    0,003    0,231      0,149    0,181      0,182    0,722     0,114     j
                 0,002    0,002    0,010      0,002    0,003      -0,002   0,797     0,039     m
                 0,331    0,058    0,093      0,331    0,145      0,148    0,836     0,074     l
                 0,007    0,004    0,130      0,007    0,013      0,014    0,750     0,176     o
                 0,023    0,013    0,061      0,023    0,033      0,017    0,777     0,078     n
                 0,019    0,001    0,121      0,019    0,033      0,046    0,622     0,025     q
                 0,007    0,001    0,057      0,007    0,012      0,016    0,497     0,026     p
                 0,172    0,004    0,661      0,172    0,273      0,323    0,558     0,267     s
                 0,014    0,004    0,077      0,014    0,024      0,023    0,690     0,043     r
                 0,259    0,003    0,640      0,259    0,368      0,399    0,863     0,345     u
                 0,092    0,009    0,162      0,092    0,117      0,109    0,479     0,061     t
                 0,000    0,001    0,000      0,000    0,000      -0,003   0,761     0,016     v
                 0,152    0,016    0,044      0,152    0,068      0,073    0,842     0,030     y
                 1,000    0,226    0,002      1,000    0,003      0,035    0,911     0,002     z
Weighted Avg.    0,316    0,018    0,593      0,316    0,392      0,363    0,807     0,501     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m     n     o     p     q     r     s     t     u     v     w     x     y     z    aa   <-- classified as
    82    11    27     4    14    10     6     9     1     0     0     1     0     3     0     9     1    13     0     2    78     2     0    10     0     1   145 |     a = ch
     0    29     0     0    49    12     0     6     0     4     0     0     0     0     0     3     0     0     0     0     0     0     0     0     0     6     0 |     b = rr
     0    90 18058   484   881   154    31   520    36   103     0    36    59    12    28   521    45   273     0    16    46    43    28   148     7   259 13954 |     c = 0
     0     9    22   120   264    31     0    45     2     7     0     5     3     6     0    55     0     9     0     0    17    16     0     0     5    51   313 |     d = hs
     0     0     0     0   230     1     0    11     0     2     0     0     0     0     0     2     0     1     0     0     0     0     0     0     0     0     0 |     e = hg
     2   142    93   129  1734  6868    56  1881   107   371     0    13     9    48     0   875    97    41    18     8    11    80     0    71    19   117   316 |     f = a
    32     0    89    62    45     1    48    35     6     7     0     8     3    14     0    21    15    28     6     9    83    12     8   117     0    12   700 |     g = c
     0    25     0     0   168    60     0   214    15     0     0     0     0     0     0    92     0     1     0     0     0     2    20     0     0     2     0 |     h = b
     0    78    17   142  2559   887    42  1382   661    10    29     3    16    24    11   508    51    78     5    16     4    18     1    59     0    63   246 |     i = e
     0    45     9    17   356   168     0   461    17    29     0     1     0     0     1   165     0     1     0     0     1     7     0     0     0    27     8 |     j = d
     0    15     1    14   137    94     0   132    18     2     7     2    22     0     3    49     0    11     0     0     2     0     0     4     0     0    31 |     k = g
     0    18    14    57    39    17     6    22     3     2     0     2     0     3     0    13     3    18     0     0     8    10     0    31     4    22   290 |     l = f
     0     2    18    72   833   186    30   583   383     0    15     2   292     8   100   544    14    15     1    14     7    15     0    66     0   202   159 |     m = i
     1     7    29    38    26    56     2    23     5     1     0     4     1    73     5     0    23    32     5     0     5    31     0    11     0     8   105 |     n = j
     0    10     0    54   501    99     0   332     0    10     0     0     0     0     2   210     0     5     0     0     1     1    45     5     0    16    20 |     o = m
     0    32     1    20   532    92     3   292    14     4     0     1     0     0     2   534     2    16     0     3     0     2     0    16     0    38     9 |     p = l
     0    81    19   201  1074  1127    18  1698    78   176     0     6     0    16     6   719    46   130    11    19     6    17   179    37    35   250   697 |     q = o
     0   109     3   146   909   210     0   873    27    14    18     1    46     1     0   464     1    73     0     0     7     5    18     3     0   118   118 |     r = n
    28     0    25    19    33    10     7    15     4     8     0     2     6    11     1     1     1    27     8     2    55     0     0    11     0     8   137 |     s = q
     0    15    44    78    66    35    28    22     1     8     0    14     0     2     0    68     8    48     3     7     6    31     0    38     0    30   453 |     t = p
   177    82   308   290   124    97    24    21    24    27     1    22    31    65    11   137    12   166     4     2   761    33     0    76     0    86  1842 |     u = s
     0    68    24   115   410   181    12   357    40    33    18     7    11     3     3   356     3    85     1     3     7    29     4    37     9    40   228 |     v = r
     0    15     8    19   154   280    12   579     8    23     0     1     0     4     4   238    13    13     0     5     1     6   538    41     4    90    24 |     w = u
     0    34    92    94    56     8    85    44     1     8     0    17     2    22     3    19    18    96     4    14    28    12     0   157     0    20   876 |     x = t
     0     0     3     6   364    50     0   129    15     1     0     1     0     0    13    71     0     8     0     0     0     4     0    13     0    19     7 |     y = v
     0     0    10    13   173     0     0    28     0     3     0     2     0     1     0    58     0    15     0     3    17     1     0    20     0    68    34 |     z = y
     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0    33 |    aa = z

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z	aa	
19.11	2.56	6.29	0.93	3.26	2.33	1.4	2.1	0.23	0.0	0.0	0.23	0.0	0.7	0.0	2.1	0.23	3.03	0.0	0.47	18.18	0.47	0.0	2.33	0.0	0.23	33.8	 a = ch
0.0	26.61	0.0	0.0	44.95	11.01	0.0	5.5	0.0	3.67	0.0	0.0	0.0	0.0	0.0	2.75	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	5.5	0.0	 b = rr
0.0	0.25	50.4	1.35	2.46	0.43	0.09	1.45	0.1	0.29	0.0	0.1	0.16	0.03	0.08	1.45	0.13	0.76	0.0	0.04	0.13	0.12	0.08	0.41	0.02	0.72	38.94	 c = 0
0.0	0.92	2.24	12.24	26.94	3.16	0.0	4.59	0.2	0.71	0.0	0.51	0.31	0.61	0.0	5.61	0.0	0.92	0.0	0.0	1.73	1.63	0.0	0.0	0.51	5.2	31.94	 d = hs
0.0	0.0	0.0	0.0	93.12	0.4	0.0	4.45	0.0	0.81	0.0	0.0	0.0	0.0	0.0	0.81	0.0	0.4	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 e = hg
0.02	1.08	0.71	0.98	13.23	52.4	0.43	14.35	0.82	2.83	0.0	0.1	0.07	0.37	0.0	6.68	0.74	0.31	0.14	0.06	0.08	0.61	0.0	0.54	0.14	0.89	2.41	 f = a
2.35	0.0	6.54	4.56	3.31	0.07	3.53	2.57	0.44	0.51	0.0	0.59	0.22	1.03	0.0	1.54	1.1	2.06	0.44	0.66	6.1	0.88	0.59	8.6	0.0	0.88	51.43	 g = c
0.0	4.17	0.0	0.0	28.05	10.02	0.0	35.73	2.5	0.0	0.0	0.0	0.0	0.0	0.0	15.36	0.0	0.17	0.0	0.0	0.0	0.33	3.34	0.0	0.0	0.33	0.0	 h = b
0.0	1.13	0.25	2.05	37.03	12.84	0.61	20.0	9.57	0.14	0.42	0.04	0.23	0.35	0.16	7.35	0.74	1.13	0.07	0.23	0.06	0.26	0.01	0.85	0.0	0.91	3.56	 i = e
0.0	3.43	0.69	1.29	27.11	12.8	0.0	35.11	1.29	2.21	0.0	0.08	0.0	0.0	0.08	12.57	0.0	0.08	0.0	0.0	0.08	0.53	0.0	0.0	0.0	2.06	0.61	 j = d
0.0	2.76	0.18	2.57	25.18	17.28	0.0	24.26	3.31	0.37	1.29	0.37	4.04	0.0	0.55	9.01	0.0	2.02	0.0	0.0	0.37	0.0	0.0	0.74	0.0	0.0	5.7	 k = g
0.0	3.09	2.41	9.79	6.7	2.92	1.03	3.78	0.52	0.34	0.0	0.34	0.0	0.52	0.0	2.23	0.52	3.09	0.0	0.0	1.37	1.72	0.0	5.33	0.69	3.78	49.83	 l = f
0.0	0.06	0.51	2.02	23.39	5.22	0.84	16.37	10.76	0.0	0.42	0.06	8.2	0.22	2.81	15.28	0.39	0.42	0.03	0.39	0.2	0.42	0.0	1.85	0.0	5.67	4.47	 m = i
0.2	1.43	5.91	7.74	5.3	11.41	0.41	4.68	1.02	0.2	0.0	0.81	0.2	14.87	1.02	0.0	4.68	6.52	1.02	0.0	1.02	6.31	0.0	2.24	0.0	1.63	21.38	 n = j
0.0	0.76	0.0	4.12	38.22	7.55	0.0	25.32	0.0	0.76	0.0	0.0	0.0	0.0	0.15	16.02	0.0	0.38	0.0	0.0	0.08	0.08	3.43	0.38	0.0	1.22	1.53	 o = m
0.0	1.98	0.06	1.24	32.98	5.7	0.19	18.1	0.87	0.25	0.0	0.06	0.0	0.0	0.12	33.11	0.12	0.99	0.0	0.19	0.0	0.12	0.0	0.99	0.0	2.36	0.56	 p = l
0.0	1.22	0.29	3.02	16.16	16.96	0.27	25.55	1.17	2.65	0.0	0.09	0.0	0.24	0.09	10.82	0.69	1.96	0.17	0.29	0.09	0.26	2.69	0.56	0.53	3.76	10.49	 q = o
0.0	3.45	0.09	4.61	28.73	6.64	0.0	27.59	0.85	0.44	0.57	0.03	1.45	0.03	0.0	14.66	0.03	2.31	0.0	0.0	0.22	0.16	0.57	0.09	0.0	3.73	3.73	 r = n
6.68	0.0	5.97	4.53	7.88	2.39	1.67	3.58	0.95	1.91	0.0	0.48	1.43	2.63	0.24	0.24	0.24	6.44	1.91	0.48	13.13	0.0	0.0	2.63	0.0	1.91	32.7	 s = q
0.0	1.49	4.38	7.76	6.57	3.48	2.79	2.19	0.1	0.8	0.0	1.39	0.0	0.2	0.0	6.77	0.8	4.78	0.3	0.7	0.6	3.08	0.0	3.78	0.0	2.99	45.07	 t = p
4.0	1.85	6.96	6.56	2.8	2.19	0.54	0.47	0.54	0.61	0.02	0.5	0.7	1.47	0.25	3.1	0.27	3.75	0.09	0.05	17.21	0.75	0.0	1.72	0.0	1.94	41.65	 u = s
0.0	3.26	1.15	5.52	19.67	8.69	0.58	17.13	1.92	1.58	0.86	0.34	0.53	0.14	0.14	17.08	0.14	4.08	0.05	0.14	0.34	1.39	0.19	1.78	0.43	1.92	10.94	 v = r
0.0	0.72	0.38	0.91	7.4	13.46	0.58	27.84	0.38	1.11	0.0	0.05	0.0	0.19	0.19	11.44	0.63	0.63	0.0	0.24	0.05	0.29	25.87	1.97	0.19	4.33	1.15	 w = u
0.0	1.99	5.38	5.5	3.27	0.47	4.97	2.57	0.06	0.47	0.0	0.99	0.12	1.29	0.18	1.11	1.05	5.61	0.23	0.82	1.64	0.7	0.0	9.18	0.0	1.17	51.23	 x = t
0.0	0.0	0.43	0.85	51.7	7.1	0.0	18.32	2.13	0.14	0.0	0.14	0.0	0.0	1.85	10.09	0.0	1.14	0.0	0.0	0.0	0.57	0.0	1.85	0.0	2.7	0.99	 y = v
0.0	0.0	2.24	2.91	38.79	0.0	0.0	6.28	0.0	0.67	0.0	0.45	0.0	0.22	0.0	13.0	0.0	3.36	0.0	0.67	3.81	0.22	0.0	4.48	0.0	15.25	7.62	 z = y
0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	100.0	 aa = z
