Timestamp:2013-08-30-04:16:11
Inventario:I1
Intervalo:6ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I1-6ms-1gramas
Num Instances:  30453
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(14): class 
0Pitch(Hz)(12): class 
0RawPitch(13): class 
0SmPitch(10): class 
0Melogram(st)(14): class 
0ZCross(13): class 
0F1(Hz)(22): class 
0F2(Hz)(20): class 
0F3(Hz)(18): class 
0F4(Hz)(23): class 
class(13): 
LogScore Bayes: -494291.19042507117
LogScore BDeu: -505032.51713578927
LogScore MDL: -503815.5311187718
LogScore ENTROPY: -493754.85179297574
LogScore AIC: -495703.85179297574




=== Stratified cross-validation ===

Correctly Classified Instances       19066               62.608  %
Incorrectly Classified Instances     11387               37.392  %
Kappa statistic                          0.5244
K&B Relative Info Score            1574513.6737 %
K&B Information Score                45820.6729 bits      1.5046 bits/instance
Class complexity | order 0           88599.5666 bits      2.9094 bits/instance
Class complexity | scheme           115102.7271 bits      3.7797 bits/instance
Complexity improvement     (Sf)     -26503.1605 bits     -0.8703 bits/instance
Mean absolute error                      0.0634
Root mean squared error                  0.2068
Relative absolute error                 51.3896 %
Root relative squared error             83.2714 %
Coverage of cases (0.95 level)          81.414  %
Mean rel. region size (0.95 level)      19.6421 %
Total Number of Instances            30453     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,696    0,064    0,671      0,696    0,683      0,623    0,913     0,744     a
                 0,035    0,004    0,207      0,035    0,059      0,073    0,798     0,129     b
                 0,625    0,071    0,423      0,625    0,505      0,465    0,900     0,533     e
                 0,280    0,018    0,193      0,280    0,229      0,219    0,874     0,171     d
                 0,060    0,005    0,155      0,060    0,087      0,089    0,801     0,061     f
                 0,654    0,031    0,465      0,654    0,544      0,530    0,928     0,522     i
                 0,062    0,005    0,193      0,062    0,094      0,100    0,856     0,121     k
                 0,134    0,003    0,356      0,134    0,195      0,213    0,829     0,152     j
                 0,273    0,060    0,326      0,273    0,297      0,230    0,804     0,274     l
                 0,378    0,025    0,548      0,378    0,447      0,420    0,847     0,461     o
                 0,909    0,125    0,818      0,909    0,861      0,770    0,961     0,955     0
                 0,348    0,020    0,517      0,348    0,416      0,396    0,906     0,407     s
                 0,516    0,017    0,414      0,516    0,460      0,449    0,938     0,524     u
Weighted Avg.    0,626    0,074    0,600      0,626    0,604      0,547    0,908     0,652     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  3358    11   362    73    20    75    29    10   353   183   248    63    40 |     a = a
    96    34   140    49    21   135    12    11   149    58   163    26    86 |     b = b
   247    18  1468    47     3   151    12    19   179    44    92    30    37 |     c = e
    70     7    90   127     1    18     0     0    67    36    15     2    20 |     d = d
    37     9    70    24    26    23     6     4    60    24    95    34    18 |     e = f
    15    10   213     2     5   789    11     5    59     3    57    28     9 |     f = i
    15     6    25     0     6    24    37     3    44    13   301   113     6 |     g = k
    79     3    68    10     4    29     7    47    29     7    46    19     3 |     h = j
   370    29   491   124    31   234    24     7   798   144   430   119   126 |     i = l
   416    13   225   129    11    30    13     7   154   850   244    22   137 |     j = o
   168    12   180    26    32   112    17    10   275    85 10551   120    23 |     k = 0
    87     5    73    18     7    75    16     9   204    11   652   621     4 |     l = s
    44     7    62    28     1     1     8     0    79    92    10     5   360 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
69.6	0.23	7.5	1.51	0.41	1.55	0.6	0.21	7.32	3.79	5.14	1.31	0.83	 a = a
9.8	3.47	14.29	5.0	2.14	13.78	1.22	1.12	15.2	5.92	16.63	2.65	8.78	 b = b
10.52	0.77	62.55	2.0	0.13	6.43	0.51	0.81	7.63	1.87	3.92	1.28	1.58	 c = e
15.45	1.55	19.87	28.04	0.22	3.97	0.0	0.0	14.79	7.95	3.31	0.44	4.42	 d = d
8.6	2.09	16.28	5.58	6.05	5.35	1.4	0.93	13.95	5.58	22.09	7.91	4.19	 e = f
1.24	0.83	17.66	0.17	0.41	65.42	0.91	0.41	4.89	0.25	4.73	2.32	0.75	 f = i
2.53	1.01	4.22	0.0	1.01	4.05	6.24	0.51	7.42	2.19	50.76	19.06	1.01	 g = k
22.51	0.85	19.37	2.85	1.14	8.26	1.99	13.39	8.26	1.99	13.11	5.41	0.85	 h = j
12.64	0.99	16.77	4.24	1.06	7.99	0.82	0.24	27.26	4.92	14.69	4.07	4.3	 i = l
18.48	0.58	10.0	5.73	0.49	1.33	0.58	0.31	6.84	37.76	10.84	0.98	6.09	 j = o
1.45	0.1	1.55	0.22	0.28	0.96	0.15	0.09	2.37	0.73	90.87	1.03	0.2	 k = 0
4.88	0.28	4.1	1.01	0.39	4.21	0.9	0.51	11.45	0.62	36.59	34.85	0.22	 l = s
6.31	1.0	8.9	4.02	0.14	0.14	1.15	0.0	11.33	13.2	1.43	0.72	51.65	 m = u
