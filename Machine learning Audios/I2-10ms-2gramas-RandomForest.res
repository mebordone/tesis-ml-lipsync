Timestamp:2013-07-22-21:55:28
Inventario:I2
Intervalo:10ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I2-10ms-2gramas
Num Instances:  18295
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3146





=== Stratified cross-validation ===

Correctly Classified Instances       13576               74.2061 %
Incorrectly Classified Instances      4719               25.7939 %
Kappa statistic                          0.6787
K&B Relative Info Score            1200532.691  %
K&B Information Score                34926.6397 bits      1.9091 bits/instance
Class complexity | order 0           53206.9102 bits      2.9083 bits/instance
Class complexity | scheme          1436442.6744 bits     78.5156 bits/instance
Complexity improvement     (Sf)    -1383235.7642 bits    -75.6073 bits/instance
Mean absolute error                      0.0596
Root mean squared error                  0.1748
Relative absolute error                 44.3379 %
Root relative squared error             67.4087 %
Coverage of cases (0.95 level)          92.7193 %
Mean rel. region size (0.95 level)      23.326  %
Total Number of Instances            18295     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,847    0,059    0,735      0,847    0,787      0,745    0,950     0,863     A
                 0,402    0,026    0,443      0,402    0,421      0,394    0,826     0,342     LGJKC
                 0,746    0,036    0,640      0,746    0,689      0,662    0,941     0,716     E
                 0,253    0,005    0,423      0,253    0,317      0,320    0,828     0,242     FV
                 0,719    0,014    0,694      0,719    0,707      0,694    0,940     0,715     I
                 0,671    0,028    0,664      0,671    0,668      0,640    0,907     0,691     O
                 0,532    0,048    0,544      0,532    0,538      0,489    0,876     0,509     SNRTY
                 0,901    0,061    0,897      0,901    0,899      0,839    0,968     0,945     0
                 0,412    0,009    0,605      0,412    0,490      0,486    0,874     0,444     BMP
                 0,361    0,003    0,660      0,361    0,467      0,483    0,930     0,486     DZ
                 0,660    0,003    0,833      0,660    0,736      0,736    0,941     0,753     U
                 0,567    0,012    0,677      0,567    0,617      0,604    0,920     0,589     SNRTXY
Weighted Avg.    0,742    0,044    0,738      0,742    0,737      0,697    0,933     0,761     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 2490   52   86    3   16   61   99   87   12    3    5   26 |    a = A
  121  359   66   13   31   46  120   94   14    0    4   26 |    b = LGJKC
  103   55 1073    8   33   21   56   29   23   14    1   22 |    c = E
   31   17   28   66    8   20   62   10    4    2    4    9 |    d = FV
   18   24   78    6  538    3   34   20    8    1    6   12 |    e = I
  111   36   64    3   14  924   43  117   24   10    8   23 |    f = O
  207   97   84   26   45   61  952  255   25    6    5   27 |    g = SNRTY
  133   80   51   11   35   92  229 6121   22    1    3   19 |    h = 0
   53   27   41   11   34   47   56   49  245    6    9   16 |    i = BMP
   45   11   26    4    4   42   13    5    6   99    3   16 |    j = DZ
   16   13   23    2    4   42   17    6   10    2  279    9 |    k = U
   62   39   57    3   13   32   68   29   12    6    8  430 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
84.69	1.77	2.93	0.1	0.54	2.07	3.37	2.96	0.41	0.1	0.17	0.88	 a = A
13.53	40.16	7.38	1.45	3.47	5.15	13.42	10.51	1.57	0.0	0.45	2.91	 b = LGJKC
7.16	3.82	74.62	0.56	2.29	1.46	3.89	2.02	1.6	0.97	0.07	1.53	 c = E
11.88	6.51	10.73	25.29	3.07	7.66	23.75	3.83	1.53	0.77	1.53	3.45	 d = FV
2.41	3.21	10.43	0.8	71.93	0.4	4.55	2.67	1.07	0.13	0.8	1.6	 e = I
8.06	2.61	4.65	0.22	1.02	67.1	3.12	8.5	1.74	0.73	0.58	1.67	 f = O
11.56	5.42	4.69	1.45	2.51	3.41	53.18	14.25	1.4	0.34	0.28	1.51	 g = SNRTY
1.96	1.18	0.75	0.16	0.51	1.35	3.37	90.05	0.32	0.01	0.04	0.28	 h = 0
8.92	4.55	6.9	1.85	5.72	7.91	9.43	8.25	41.25	1.01	1.52	2.69	 i = BMP
16.42	4.01	9.49	1.46	1.46	15.33	4.74	1.82	2.19	36.13	1.09	5.84	 j = DZ
3.78	3.07	5.44	0.47	0.95	9.93	4.02	1.42	2.36	0.47	65.96	2.13	 k = U
8.17	5.14	7.51	0.4	1.71	4.22	8.96	3.82	1.58	0.79	1.05	56.65	 l = SNRTXY
