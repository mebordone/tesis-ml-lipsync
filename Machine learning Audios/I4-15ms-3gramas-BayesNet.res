Timestamp:2013-08-30-05:29:19
Inventario:I4
Intervalo:15ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I4-15ms-3gramas
Num Instances:  12187
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(5): class 
0RawPitch(4): class 
0SmPitch(5): class 
0Melogram(st)(9): class 
0ZCross(4): class 
0F1(Hz)(9): class 
0F2(Hz)(9): class 
0F3(Hz)(6): class 
0F4(Hz)(7): class 
1Int(dB)(9): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(7): class 
1F1(Hz)(10): class 
1F2(Hz)(8): class 
1F3(Hz)(7): class 
1F4(Hz)(6): class 
2Int(dB)(9): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(8): class 
2ZCross(8): class 
2F1(Hz)(11): class 
2F2(Hz)(10): class 
2F3(Hz)(9): class 
2F4(Hz)(5): class 
class(10): 
LogScore Bayes: -405774.05695247743
LogScore BDeu: -413099.8576348804
LogScore MDL: -412530.8519188496
LogScore ENTROPY: -404256.40590322943
LogScore AIC: -406015.40590322943




=== Stratified cross-validation ===

Correctly Classified Instances        6769               55.5428 %
Incorrectly Classified Instances      5418               44.4572 %
Kappa statistic                          0.4352
K&B Relative Info Score             528540.3781 %
K&B Information Score                13743.7495 bits      1.1277 bits/instance
Class complexity | order 0           31672.3435 bits      2.5989 bits/instance
Class complexity | scheme           110401.0854 bits      9.0589 bits/instance
Complexity improvement     (Sf)     -78728.742  bits     -6.4601 bits/instance
Mean absolute error                      0.0899
Root mean squared error                  0.2748
Relative absolute error                 57.2715 %
Root relative squared error             98.0661 %
Coverage of cases (0.95 level)          64.7329 %
Mean rel. region size (0.95 level)      16.077  %
Total Number of Instances            12187     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,417    0,054    0,044      0,417    0,079      0,121    0,844     0,033     C
                 0,537    0,102    0,313      0,537    0,395      0,343    0,852     0,388     E
                 0,040    0,017    0,032      0,040    0,036      0,020    0,766     0,038     FV
                 0,502    0,078    0,624      0,502    0,556      0,461    0,854     0,655     AI
                 0,180    0,053    0,398      0,180    0,248      0,180    0,753     0,348     CDGKNRSYZ
                 0,355    0,039    0,430      0,355    0,389      0,345    0,823     0,390     O
                 0,918    0,121    0,811      0,918    0,861      0,778    0,966     0,953     0
                 0,048    0,020    0,088      0,048    0,062      0,038    0,732     0,076     LT
                 0,449    0,018    0,376      0,449    0,410      0,396    0,914     0,467     U
                 0,170    0,025    0,189      0,170    0,179      0,153    0,771     0,119     MBP
Weighted Avg.    0,555    0,082    0,563      0,555    0,546      0,476    0,869     0,615     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   30   27    1    6    3    0    2    0    2    1 |    a = C
  100  521   13  118   67   34   43   28   29   18 |    b = E
   15   47    7   23   25   19   23    8    2    8 |    c = FV
  176  414   47 1250  162  122  129   78   22   92 |    d = AI
  179  285   62  302  356  131  475   42   48   98 |    e = CDGKNRSYZ
   64  115   16  149   56  334   92   32   66   16 |    f = O
   16   56   42   59  131   26 4053    9    6   18 |    g = 0
   45   88   14   36   52   35  125   22   12   28 |    h = LT
   21   24    2   21   10   44    9   13  128   13 |    i = U
   37   88   12   40   32   32   46   19   25   68 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
41.67	37.5	1.39	8.33	4.17	0.0	2.78	0.0	2.78	1.39	 a = C
10.3	53.66	1.34	12.15	6.9	3.5	4.43	2.88	2.99	1.85	 b = E
8.47	26.55	3.95	12.99	14.12	10.73	12.99	4.52	1.13	4.52	 c = FV
7.06	16.61	1.89	50.16	6.5	4.9	5.18	3.13	0.88	3.69	 d = AI
9.05	14.41	3.13	15.27	18.0	6.62	24.01	2.12	2.43	4.95	 e = CDGKNRSYZ
6.81	12.23	1.7	15.85	5.96	35.53	9.79	3.4	7.02	1.7	 f = O
0.36	1.27	0.95	1.34	2.97	0.59	91.78	0.2	0.14	0.41	 g = 0
9.85	19.26	3.06	7.88	11.38	7.66	27.35	4.81	2.63	6.13	 h = LT
7.37	8.42	0.7	7.37	3.51	15.44	3.16	4.56	44.91	4.56	 i = U
9.27	22.06	3.01	10.03	8.02	8.02	11.53	4.76	6.27	17.04	 j = MBP
