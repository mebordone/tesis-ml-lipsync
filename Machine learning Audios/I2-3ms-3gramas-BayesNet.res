Timestamp:2013-08-30-04:38:04
Inventario:I2
Intervalo:3ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I2-3ms-3gramas
Num Instances:  61133
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(20): class 
0Pitch(Hz)(28): class 
0RawPitch(22): class 
0SmPitch(23): class 
0Melogram(st)(32): class 
0ZCross(15): class 
0F1(Hz)(222): class 
0F2(Hz)(233): class 
0F3(Hz)(413): class 
0F4(Hz)(233): class 
1Int(dB)(21): class 
1Pitch(Hz)(28): class 
1RawPitch(19): class 
1SmPitch(23): class 
1Melogram(st)(35): class 
1ZCross(15): class 
1F1(Hz)(199): class 
1F2(Hz)(282): class 
1F3(Hz)(351): class 
1F4(Hz)(217): class 
2Int(dB)(19): class 
2Pitch(Hz)(27): class 
2RawPitch(21): class 
2SmPitch(24): class 
2Melogram(st)(36): class 
2ZCross(16): class 
2F1(Hz)(208): class 
2F2(Hz)(219): class 
2F3(Hz)(297): class 
2F4(Hz)(205): class 
class(12): 
LogScore Bayes: -3703687.326976691
LogScore BDeu: -4080756.4024682147
LogScore MDL: -4015728.3511797683
LogScore ENTROPY: -3786016.158441803
LogScore AIC: -3827703.158441803




=== Stratified cross-validation ===

Correctly Classified Instances       38633               63.195  %
Incorrectly Classified Instances     22500               36.805  %
Kappa statistic                          0.5331
K&B Relative Info Score            3242454.1494 %
K&B Information Score                93063.298  bits      1.5223 bits/instance
Class complexity | order 0          175445.1587 bits      2.8699 bits/instance
Class complexity | scheme           614317.4421 bits     10.0489 bits/instance
Complexity improvement     (Sf)    -438872.2834 bits     -7.179  bits/instance
Mean absolute error                      0.062 
Root mean squared error                  0.2327
Relative absolute error                 46.6239 %
Root relative squared error             90.2852 %
Coverage of cases (0.95 level)          69.5827 %
Mean rel. region size (0.95 level)      11.2443 %
Total Number of Instances            61133     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,648    0,044    0,734      0,648    0,688      0,636    0,920     0,768     A
                 0,080    0,012    0,261      0,080    0,122      0,122    0,793     0,176     LGJKC
                 0,633    0,058    0,470      0,633    0,540      0,502    0,910     0,569     E
                 0,227    0,012    0,218      0,227    0,222      0,211    0,851     0,171     FV
                 0,693    0,031    0,478      0,693    0,566      0,555    0,933     0,598     I
                 0,442    0,025    0,584      0,442    0,503      0,475    0,870     0,541     O
                 0,250    0,027    0,492      0,250    0,332      0,305    0,875     0,417     SNRTY
                 0,897    0,134    0,810      0,897    0,851      0,751    0,958     0,952     0
                 0,213    0,008    0,457      0,213    0,290      0,297    0,835     0,284     BMP
                 0,489    0,042    0,150      0,489    0,229      0,252    0,904     0,254     DZ
                 0,638    0,019    0,442      0,638    0,522      0,518    0,952     0,621     U
                 0,394    0,030    0,357      0,394    0,375      0,348    0,879     0,333     SNRTXY
Weighted Avg.    0,632    0,072    0,630      0,632    0,618      0,562    0,916     0,687     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  6211   129   743   107   167   481   215   490    52   530   129   331 |     a = A
   241   237   385    48   303   167   319   779    39   176   102   167 |     b = LGJKC
   335    69  2923    63   303    73   121   173    69   207    94   191 |     c = E
    53     5   100   196    58    36    55   201    25    94    22    19 |     d = FV
    26    37   251    20  1650     5    86   120    41    45    39    62 |     e = I
   572    60   351    28    76  1972    85   473    37   425   248   136 |     f = O
   319   146   354   154   204   126  1454  2172    77   338   119   345 |     g = SNRTY
   278   149   363   157   254   228   348 21270    87   257    64   269 |     h = 0
   124    32   204    67   210    60    93   340   414   144   153   107 |     i = BMP
    85     3   104    19    38    42    15    29    10   442    33    84 |     j = DZ
    50    23    85    17     9   124    26    25    24    67   884    52 |     k = U
   166    17   350    24   181    62   137   200    31   225   112   980 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
64.8	1.35	7.75	1.12	1.74	5.02	2.24	5.11	0.54	5.53	1.35	3.45	 a = A
8.13	8.0	12.99	1.62	10.23	5.64	10.77	26.29	1.32	5.94	3.44	5.64	 b = LGJKC
7.25	1.49	63.25	1.36	6.56	1.58	2.62	3.74	1.49	4.48	2.03	4.13	 c = E
6.13	0.58	11.57	22.69	6.71	4.17	6.37	23.26	2.89	10.88	2.55	2.2	 d = FV
1.09	1.55	10.54	0.84	69.27	0.21	3.61	5.04	1.72	1.89	1.64	2.6	 e = I
12.82	1.34	7.86	0.63	1.7	44.19	1.9	10.6	0.83	9.52	5.56	3.05	 f = O
5.49	2.51	6.1	2.65	3.51	2.17	25.03	37.4	1.33	5.82	2.05	5.94	 g = SNRTY
1.17	0.63	1.53	0.66	1.07	0.96	1.47	89.66	0.37	1.08	0.27	1.13	 h = 0
6.37	1.64	10.47	3.44	10.78	3.08	4.77	17.45	21.25	7.39	7.85	5.49	 i = BMP
9.4	0.33	11.5	2.1	4.2	4.65	1.66	3.21	1.11	48.89	3.65	9.29	 j = DZ
3.61	1.66	6.13	1.23	0.65	8.95	1.88	1.8	1.73	4.83	63.78	3.75	 k = U
6.68	0.68	14.08	0.97	7.28	2.49	5.51	8.05	1.25	9.05	4.51	39.44	 l = SNRTXY
