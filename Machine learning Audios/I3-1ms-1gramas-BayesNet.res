Timestamp:2013-08-30-04:43:23
Inventario:I3
Intervalo:1ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I3-1ms-1gramas
Num Instances:  183404
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(32): class 
0Pitch(Hz)(96): class 
0RawPitch(78): class 
0SmPitch(90): class 
0Melogram(st)(87): class 
0ZCross(20): class 
0F1(Hz)(2598): class 
0F2(Hz)(3309): class 
0F3(Hz)(3521): class 
0F4(Hz)(3448): class 
class(11): 
LogScore Bayes: -5779984.303672843
LogScore BDeu: -7668706.643499544
LogScore MDL: -7194247.291039149
LogScore ENTROPY: -6309715.53709853
LogScore AIC: -6455684.537098531




=== Stratified cross-validation ===

Correctly Classified Instances      148587               81.0162 %
Incorrectly Classified Instances     34817               18.9838 %
Kappa statistic                          0.7509
K&B Relative Info Score            14236525.8284 %
K&B Information Score               368612.0687 bits      2.0098 bits/instance
Class complexity | order 0          474846.2629 bits      2.5891 bits/instance
Class complexity | scheme           461735.8649 bits      2.5176 bits/instance
Complexity improvement     (Sf)      13110.398  bits      0.0715 bits/instance
Mean absolute error                      0.0352
Root mean squared error                  0.1763
Relative absolute error                 25.0502 %
Root relative squared error             66.481  %
Coverage of cases (0.95 level)          85.183  %
Mean rel. region size (0.95 level)      10.7453 %
Total Number of Instances           183404     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,873    0,016    0,909      0,873    0,890      0,871    0,965     0,933     A
                 0,865    0,015    0,824      0,865    0,844      0,831    0,969     0,897     E
                 0,514    0,021    0,844      0,514    0,639      0,605    0,957     0,831     CGJLNQSRT
                 0,651    0,005    0,630      0,651    0,641      0,636    0,922     0,654     FV
                 0,860    0,008    0,811      0,860    0,835      0,828    0,963     0,875     I
                 0,801    0,010    0,862      0,801    0,830      0,818    0,931     0,856     O
                 0,915    0,162    0,786      0,915    0,845      0,738    0,972     0,963     0
                 0,707    0,006    0,800      0,707    0,751      0,745    0,922     0,749     BMP
                 0,913    0,006    0,772      0,913    0,837      0,836    0,982     0,926     U
                 0,000    0,002    0,000      0,000    0,000      -0,001   0,792     0,001     DZ
                 0,917    0,011    0,547      0,917    0,685      0,703    0,988     0,851     D
Weighted Avg.    0,810    0,073    0,819      0,810    0,804      0,751    0,963     0,904     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 24916   270   609   102   113   238  1622   140    83   195   269 |     a = A
   156 11880   451   107    91    65   574   101    84    50   181 |     b = E
  1076  1053 17194    85   425   616 11992   205   411    13   409 |     c = CGJLNQSRT
    29    45    73  1668    34    15   671     3    10     0    13 |     d = FV
    27    84   277    54  6089    19   379    65    16    35    38 |     e = I
   180    84   234    41    62 10598  1494   131    73    11   326 |     f = O
   879   872  1212   555   536   613 65953   354   380    61   702 |     g = 0
    86    55   178    15   138    75  1084  4111    44     3    25 |     h = BMP
    26    26   120    13    11    21    75    24  3779    16    26 |     i = U
     0     0     0     0     0     0    65     0     0     0     0 |     j = DZ
    39    44    22     6    11    30    36     4    14    12  2399 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
87.25	0.95	2.13	0.36	0.4	0.83	5.68	0.49	0.29	0.68	0.94	 a = A
1.14	86.46	3.28	0.78	0.66	0.47	4.18	0.74	0.61	0.36	1.32	 b = E
3.21	3.15	51.36	0.25	1.27	1.84	35.82	0.61	1.23	0.04	1.22	 c = CGJLNQSRT
1.13	1.76	2.85	65.13	1.33	0.59	26.2	0.12	0.39	0.0	0.51	 d = FV
0.38	1.19	3.91	0.76	85.97	0.27	5.35	0.92	0.23	0.49	0.54	 e = I
1.36	0.63	1.77	0.31	0.47	80.08	11.29	0.99	0.55	0.08	2.46	 f = O
1.22	1.21	1.68	0.77	0.74	0.85	91.45	0.49	0.53	0.08	0.97	 g = 0
1.48	0.95	3.06	0.26	2.37	1.29	18.64	70.71	0.76	0.05	0.43	 h = BMP
0.63	0.63	2.9	0.31	0.27	0.51	1.81	0.58	91.35	0.39	0.63	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
1.49	1.68	0.84	0.23	0.42	1.15	1.38	0.15	0.53	0.46	91.67	 k = D
