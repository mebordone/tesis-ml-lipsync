Timestamp:2013-08-30-04:16:29
Inventario:I1
Intervalo:6ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I1-6ms-3gramas
Num Instances:  30451
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(15): class 
0Pitch(Hz)(12): class 
0RawPitch(11): class 
0SmPitch(10): class 
0Melogram(st)(15): class 
0ZCross(12): class 
0F1(Hz)(22): class 
0F2(Hz)(15): class 
0F3(Hz)(19): class 
0F4(Hz)(16): class 
1Int(dB)(16): class 
1Pitch(Hz)(12): class 
1RawPitch(11): class 
1SmPitch(11): class 
1Melogram(st)(14): class 
1ZCross(13): class 
1F1(Hz)(18): class 
1F2(Hz)(19): class 
1F3(Hz)(20): class 
1F4(Hz)(17): class 
2Int(dB)(14): class 
2Pitch(Hz)(12): class 
2RawPitch(13): class 
2SmPitch(10): class 
2Melogram(st)(14): class 
2ZCross(13): class 
2F1(Hz)(22): class 
2F2(Hz)(20): class 
2F3(Hz)(18): class 
2F4(Hz)(23): class 
class(13): 
LogScore Bayes: -1368364.1105466506
LogScore BDeu: -1398704.744062075
LogScore MDL: -1395195.730557784
LogScore ENTROPY: -1366479.8747115813
LogScore AIC: -1372042.8747115815




=== Stratified cross-validation ===

Correctly Classified Instances       18177               59.6926 %
Incorrectly Classified Instances     12274               40.3074 %
Kappa statistic                          0.4964
K&B Relative Info Score            1488140.7835 %
K&B Information Score                43307.2498 bits      1.4222 bits/instance
Class complexity | order 0           88596.7844 bits      2.9095 bits/instance
Class complexity | scheme           302998.2989 bits      9.9504 bits/instance
Complexity improvement     (Sf)    -214401.5146 bits     -7.0409 bits/instance
Mean absolute error                      0.0629
Root mean squared error                  0.2297
Relative absolute error                 50.9817 %
Root relative squared error             92.5028 %
Coverage of cases (0.95 level)          68.3065 %
Mean rel. region size (0.95 level)      11.6222 %
Total Number of Instances            30451     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,614    0,046    0,716      0,614    0,661      0,606    0,909     0,739     a
                 0,052    0,007    0,205      0,052    0,083      0,089    0,790     0,115     b
                 0,587    0,063    0,439      0,587    0,503      0,460    0,892     0,507     e
                 0,499    0,061    0,110      0,499    0,181      0,212    0,877     0,152     d
                 0,128    0,017    0,095      0,128    0,109      0,095    0,808     0,070     f
                 0,672    0,039    0,418      0,672    0,516      0,506    0,922     0,496     i
                 0,214    0,021    0,168      0,214    0,189      0,172    0,867     0,119     k
                 0,231    0,008    0,245      0,231    0,238      0,229    0,846     0,164     j
                 0,130    0,033    0,296      0,130    0,181      0,142    0,795     0,256     l
                 0,337    0,020    0,573      0,337    0,424      0,407    0,841     0,448     o
                 0,896    0,106    0,839      0,896    0,867      0,782    0,959     0,953     0
                 0,316    0,021    0,478      0,316    0,380      0,358    0,899     0,376     s
                 0,544    0,024    0,345      0,544    0,423      0,417    0,931     0,511     u
Weighted Avg.    0,597    0,062    0,606      0,597    0,588      0,537    0,904     0,642     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  2963    12   384   377    90   107    89    40   224   193   194    72    80 |     a = a
    64    51   107   125    56   168    35    22    52    31   139    33    97 |     b = b
   185    28  1378   144    30   171    43    47   110    36    84    30    61 |     c = e
    49    11    48   226    10    19     0     2    26    17    14     4    27 |     d = d
    26    12    60    62    55    29    18     7    21    18    76    32    14 |     e = f
    13    13   162    19    15   811    40     9    28     1    45    32    18 |     f = i
    11     8    10     2    20    31   127     6    14    12   288    63     1 |     g = k
    60     1    54    21    12    34     8    81     8     3    34    27     8 |     h = j
   242    45   446   507   100   283   102    38   380    93   377   119   195 |     i = l
   316    18   193   299    32    52    34    20    95   759   221    36   176 |     j = o
   107    30   179   127   100   137    73    29   143    87 10404   163    30 |     k = 0
    66     8    63    72    46    91   167    30   142    14   509   563    11 |     l = s
    34    12    53    68    13     5    18     0    39    61    10     5   379 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
61.41	0.25	7.96	7.81	1.87	2.22	1.84	0.83	4.64	4.0	4.02	1.49	1.66	 a = a
6.53	5.2	10.92	12.76	5.71	17.14	3.57	2.24	5.31	3.16	14.18	3.37	9.9	 b = b
7.88	1.19	58.71	6.14	1.28	7.29	1.83	2.0	4.69	1.53	3.58	1.28	2.6	 c = e
10.82	2.43	10.6	49.89	2.21	4.19	0.0	0.44	5.74	3.75	3.09	0.88	5.96	 d = d
6.05	2.79	13.95	14.42	12.79	6.74	4.19	1.63	4.88	4.19	17.67	7.44	3.26	 e = f
1.08	1.08	13.43	1.58	1.24	67.25	3.32	0.75	2.32	0.08	3.73	2.65	1.49	 f = i
1.85	1.35	1.69	0.34	3.37	5.23	21.42	1.01	2.36	2.02	48.57	10.62	0.17	 g = k
17.09	0.28	15.38	5.98	3.42	9.69	2.28	23.08	2.28	0.85	9.69	7.69	2.28	 h = j
8.27	1.54	15.24	17.32	3.42	9.67	3.48	1.3	12.98	3.18	12.88	4.07	6.66	 i = l
14.04	0.8	8.57	13.28	1.42	2.31	1.51	0.89	4.22	33.72	9.82	1.6	7.82	 j = o
0.92	0.26	1.54	1.09	0.86	1.18	0.63	0.25	1.23	0.75	89.62	1.4	0.26	 k = 0
3.7	0.45	3.54	4.04	2.58	5.11	9.37	1.68	7.97	0.79	28.56	31.59	0.62	 l = s
4.88	1.72	7.6	9.76	1.87	0.72	2.58	0.0	5.6	8.75	1.43	0.72	54.38	 m = u
