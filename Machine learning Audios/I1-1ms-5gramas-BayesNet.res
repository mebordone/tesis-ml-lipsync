Timestamp:2013-08-30-04:03:45
Inventario:I1
Intervalo:1ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I1-1ms-5gramas
Num Instances:  183400
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  49 4F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(34): class 
0Pitch(Hz)(101): class 
0RawPitch(72): class 
0SmPitch(85): class 
0Melogram(st)(87): class 
0ZCross(21): class 
0F1(Hz)(2727): class 
0F2(Hz)(3482): class 
0F3(Hz)(3609): class 
0F4(Hz)(3546): class 
1Int(dB)(34): class 
1Pitch(Hz)(97): class 
1RawPitch(80): class 
1SmPitch(85): class 
1Melogram(st)(88): class 
1ZCross(21): class 
1F1(Hz)(2728): class 
1F2(Hz)(3468): class 
1F3(Hz)(3630): class 
1F4(Hz)(3539): class 
2Int(dB)(34): class 
2Pitch(Hz)(99): class 
2RawPitch(67): class 
2SmPitch(82): class 
2Melogram(st)(86): class 
2ZCross(21): class 
2F1(Hz)(2720): class 
2F2(Hz)(3451): class 
2F3(Hz)(3623): class 
2F4(Hz)(3519): class 
3Int(dB)(32): class 
3Pitch(Hz)(96): class 
3RawPitch(64): class 
3SmPitch(88): class 
3Melogram(st)(89): class 
3ZCross(21): class 
3F1(Hz)(2707): class 
3F2(Hz)(3429): class 
3F3(Hz)(3620): class 
3F4(Hz)(3530): class 
4Int(dB)(33): class 
4Pitch(Hz)(95): class 
4RawPitch(63): class 
4SmPitch(93): class 
4Melogram(st)(85): class 
4ZCross(21): class 
4F1(Hz)(2711): class 
4F2(Hz)(3422): class 
4F3(Hz)(3617): class 
4F4(Hz)(3544): class 
class(13): 
LogScore Bayes: -2.7399888835361745E7
LogScore BDeu: -3.9129785790763006E7
LogScore MDL: -3.609479503126512E7
LogScore ENTROPY: -3.069492469721156E7
LogScore AIC: -3.1586034697211564E7




=== Stratified cross-validation ===

Correctly Classified Instances      152954               83.3991 %
Incorrectly Classified Instances     30446               16.6009 %
Kappa statistic                          0.7884
K&B Relative Info Score            14487441.8304 %
K&B Information Score               417307.7493 bits      2.2754 bits/instance
Class complexity | order 0          528255.3218 bits      2.8803 bits/instance
Class complexity | scheme          2036524.0839 bits     11.1043 bits/instance
Complexity improvement     (Sf)    -1508268.7621 bits     -8.2239 bits/instance
Mean absolute error                      0.0256
Root mean squared error                  0.157 
Relative absolute error                 20.9107 %
Root relative squared error             63.5348 %
Coverage of cases (0.95 level)          84.2356 %
Mean rel. region size (0.95 level)       7.9713 %
Total Number of Instances           183400     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,865    0,008    0,953      0,865    0,907      0,892    0,969     0,942     a
                 0,739    0,006    0,803      0,739    0,770      0,763    0,937     0,772     b
                 0,879    0,008    0,896      0,879    0,888      0,879    0,972     0,912     e
                 0,942    0,011    0,557      0,942    0,700      0,719    0,978     0,830     d
                 0,690    0,007    0,572      0,690    0,626      0,623    0,943     0,666     f
                 0,883    0,006    0,855      0,883    0,869      0,864    0,966     0,881     i
                 0,261    0,008    0,389      0,261    0,312      0,308    0,935     0,350     k
                 0,786    0,006    0,613      0,786    0,689      0,690    0,941     0,746     j
                 0,716    0,012    0,865      0,716    0,784      0,767    0,951     0,825     l
                 0,829    0,007    0,899      0,829    0,862      0,853    0,939     0,875     o
                 0,904    0,122    0,828      0,904    0,864      0,772    0,973     0,964     0
                 0,601    0,014    0,719      0,601    0,655      0,638    0,975     0,751     s
                 0,932    0,004    0,847      0,932    0,887      0,886    0,983     0,936     u
Weighted Avg.    0,834    0,053    0,837      0,834    0,832      0,790    0,965     0,894     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 24699   174   103   346   187   101   447   203   406   182  1375   273    61 |     a = a
    49  4295    27     1    11    94    68     2    43    65  1011   112    36 |     b = b
    47   117 12083   154   108    33   151    76   251    38   528   112    42 |     c = e
    10     0    12  2526     0     0     7     0    13    11    88     8     7 |     d = d
    14     0    19     0  1768    25    42     0    14     8   600    68     3 |     e = f
     9    84    20    17    40  6256    76    11   101    21   326   119     3 |     f = i
    10    13    52     5     3    16   927     4    60    40  1727   657    41 |     g = k
    10     0    20     1     0     4    33  1621     2     3   253   109     6 |     h = j
   341   105   371   161    47   141   191    68 12381   237  2571   544   134 |     i = l
    51   126    12   282    34    31   102    22   146 10966  1397    54    11 |     j = o
   581   393   710   925   863   562   258   570   767   556 65222   392   314 |     k = 0
    90    12    48   115    24    49    36    47    65    63  3627  6354    40 |     l = s
     1    28     5     6     6     1    43    21    62     8    61    39  3856 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
86.49	0.61	0.36	1.21	0.65	0.35	1.57	0.71	1.42	0.64	4.81	0.96	0.21	 a = a
0.84	73.87	0.46	0.02	0.19	1.62	1.17	0.03	0.74	1.12	17.39	1.93	0.62	 b = b
0.34	0.85	87.94	1.12	0.79	0.24	1.1	0.55	1.83	0.28	3.84	0.82	0.31	 c = e
0.37	0.0	0.45	94.18	0.0	0.0	0.26	0.0	0.48	0.41	3.28	0.3	0.26	 d = d
0.55	0.0	0.74	0.0	69.04	0.98	1.64	0.0	0.55	0.31	23.43	2.66	0.12	 e = f
0.13	1.19	0.28	0.24	0.56	88.32	1.07	0.16	1.43	0.3	4.6	1.68	0.04	 f = i
0.28	0.37	1.46	0.14	0.08	0.45	26.08	0.11	1.69	1.13	48.58	18.48	1.15	 g = k
0.48	0.0	0.97	0.05	0.0	0.19	1.6	78.61	0.1	0.15	12.27	5.29	0.29	 h = j
1.97	0.61	2.15	0.93	0.27	0.82	1.1	0.39	71.6	1.37	14.87	3.15	0.77	 i = l
0.39	0.95	0.09	2.13	0.26	0.23	0.77	0.17	1.1	82.86	10.56	0.41	0.08	 j = o
0.81	0.54	0.98	1.28	1.2	0.78	0.36	0.79	1.06	0.77	90.44	0.54	0.44	 k = 0
0.85	0.11	0.45	1.09	0.23	0.46	0.34	0.44	0.61	0.6	34.31	60.11	0.38	 l = s
0.02	0.68	0.12	0.15	0.15	0.02	1.04	0.51	1.5	0.19	1.47	0.94	93.21	 m = u
