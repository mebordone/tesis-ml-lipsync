Timestamp:2013-07-22-22:51:38
Inventario:I4
Intervalo:2ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I4-2ms-2gramas
Num Instances:  91702
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.151





=== Stratified cross-validation ===

Correctly Classified Instances       80641               87.9381 %
Incorrectly Classified Instances     11061               12.0619 %
Kappa statistic                          0.843 
K&B Relative Info Score            7493480.2575 %
K&B Information Score               191000.3686 bits      2.0828 bits/instance
Class complexity | order 0          233715.7607 bits      2.5486 bits/instance
Class complexity | scheme          4423142.0293 bits     48.2339 bits/instance
Complexity improvement     (Sf)    -4189426.2686 bits    -45.6852 bits/instance
Mean absolute error                      0.0364
Root mean squared error                  0.1376
Relative absolute error                 23.6223 %
Root relative squared error             49.5434 %
Coverage of cases (0.95 level)          95.2564 %
Mean rel. region size (0.95 level)      17.7154 %
Total Number of Instances            91702     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,866    0,001    0,902      0,866    0,884      0,883    0,963     0,897     C
                 0,917    0,009    0,892      0,917    0,904      0,897    0,976     0,937     E
                 0,673    0,004    0,731      0,673    0,701      0,698    0,905     0,693     FV
                 0,924    0,022    0,912      0,924    0,918      0,898    0,974     0,952     AI
                 0,813    0,040    0,790      0,813    0,802      0,764    0,952     0,850     CDGKNRSYZ
                 0,846    0,008    0,887      0,846    0,866      0,856    0,951     0,886     O
                 0,926    0,056    0,913      0,926    0,920      0,868    0,976     0,956     0
                 0,581    0,010    0,690      0,581    0,631      0,621    0,882     0,628     LT
                 0,928    0,001    0,968      0,928    0,948      0,947    0,987     0,962     U
                 0,737    0,005    0,828      0,737    0,780      0,775    0,921     0,785     MBP
Weighted Avg.    0,879    0,034    0,878      0,879    0,878      0,846    0,964     0,911     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   471     9     1    17    18     4    15     7     2     0 |     a = C
     5  6338    17    99   204    26   146    38     7    30 |     b = E
     2    30   866    37   236    22    58    23     1    11 |     c = FV
    11   117    35 16529   458    65   492   126     9    52 |     d = AI
     6   239   145   484 11610   192  1185   259    18   134 |     e = CDGKNRSYZ
     3    37    18   124   253  5621   506    41     4    39 |     f = O
    18   196    59   493  1177   272 33197   287     8   125 |     g = 0
     3    74    21   205   489    75   478  1930     6    42 |     h = LT
     1    13     2    29    39    12    25    16  1931    12 |     i = U
     2    52    20   113   212    47   244    69     8  2148 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
86.58	1.65	0.18	3.13	3.31	0.74	2.76	1.29	0.37	0.0	 a = C
0.07	91.72	0.25	1.43	2.95	0.38	2.11	0.55	0.1	0.43	 b = E
0.16	2.33	67.34	2.88	18.35	1.71	4.51	1.79	0.08	0.86	 c = FV
0.06	0.65	0.2	92.37	2.56	0.36	2.75	0.7	0.05	0.29	 d = AI
0.04	1.67	1.02	3.39	81.35	1.35	8.3	1.81	0.13	0.94	 e = CDGKNRSYZ
0.05	0.56	0.27	1.87	3.81	84.58	7.61	0.62	0.06	0.59	 f = O
0.05	0.55	0.16	1.38	3.28	0.76	92.65	0.8	0.02	0.35	 g = 0
0.09	2.23	0.63	6.17	14.72	2.26	14.38	58.08	0.18	1.26	 h = LT
0.05	0.63	0.1	1.39	1.88	0.58	1.2	0.77	92.84	0.58	 i = U
0.07	1.78	0.69	3.88	7.27	1.61	8.37	2.37	0.27	73.69	 j = MBP
