Timestamp:2013-07-22-21:06:01
Inventario:I1
Intervalo:2ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I1-2ms-2gramas
Num Instances:  91702
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1593





=== Stratified cross-validation ===

Correctly Classified Instances       79806               87.0275 %
Incorrectly Classified Instances     11896               12.9725 %
Kappa statistic                          0.8364
K&B Relative Info Score            7483875.8789 %
K&B Information Score               216037.9786 bits      2.3559 bits/instance
Class complexity | order 0          264697.5104 bits      2.8865 bits/instance
Class complexity | scheme          5343968.2536 bits     58.2754 bits/instance
Complexity improvement     (Sf)    -5079270.7433 bits    -55.3889 bits/instance
Mean absolute error                      0.0294
Root mean squared error                  0.1243
Relative absolute error                 24.0236 %
Root relative squared error             50.2392 %
Coverage of cases (0.95 level)          94.3262 %
Mean rel. region size (0.95 level)      14.0671 %
Total Number of Instances            91702     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,925    0,017    0,909      0,925    0,917      0,901    0,975     0,950     a
                 0,759    0,007    0,786      0,759    0,772      0,765    0,924     0,786     b
                 0,911    0,009    0,891      0,911    0,901      0,893    0,976     0,937     e
                 0,888    0,001    0,909      0,888    0,898      0,897    0,983     0,928     d
                 0,673    0,004    0,714      0,673    0,693      0,689    0,909     0,690     f
                 0,893    0,004    0,902      0,893    0,897      0,893    0,971     0,921     i
                 0,380    0,009    0,446      0,380    0,410      0,401    0,806     0,339     k
                 0,758    0,002    0,831      0,758    0,793      0,792    0,926     0,791     j
                 0,784    0,021    0,797      0,784    0,791      0,769    0,940     0,833     l
                 0,845    0,008    0,888      0,845    0,866      0,856    0,950     0,886     o
                 0,928    0,058    0,911      0,928    0,919      0,867    0,976     0,956     0
                 0,704    0,019    0,695      0,704    0,700      0,681    0,926     0,726     s
                 0,928    0,001    0,969      0,928    0,948      0,947    0,989     0,964     u
Weighted Avg.    0,870    0,030    0,869      0,870    0,869      0,841    0,961     0,899     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 13265    47    66    11    23    31    82    13   219    52   401   118     5 |     a = a
    84  2212    41     3    17    33    42     7   107    40   228    94     7 |     b = b
    69    50  6297    16    20    24    36    10   152    27   150    50     9 |     c = e
    25     3    30  1195     3     1     0     0    25    23    22    17     2 |     d = d
    40    17    33     4   865    11    24    11    49    30    59   142     1 |     e = f
    31    24    54     1    13  3181    19     1    57    10   122    46     2 |     f = i
    82    54    44     1    29    18   676    19   150    39   430   229     9 |     g = k
    40     6    20     0    15     6    16   785    19    11    41    73     3 |     h = j
   315    99   151    20    29    61   146    16  6806   111   644   269    13 |     i = l
    98    41    51    26    27    12    43     7   142  5613   496    88     2 |     j = o
   389   168   219    27    67   105   254    36   534   276 33251   499     7 |     k = 0
   149    78    48    10   103    32   166    36   241    78   626  3729     2 |     l = s
    12    16    10     1     1    13    13     4    34    13    24     8  1931 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
92.55	0.33	0.46	0.08	0.16	0.22	0.57	0.09	1.53	0.36	2.8	0.82	0.03	 a = a
2.88	75.88	1.41	0.1	0.58	1.13	1.44	0.24	3.67	1.37	7.82	3.22	0.24	 b = b
1.0	0.72	91.13	0.23	0.29	0.35	0.52	0.14	2.2	0.39	2.17	0.72	0.13	 c = e
1.86	0.22	2.23	88.78	0.22	0.07	0.0	0.0	1.86	1.71	1.63	1.26	0.15	 d = d
3.11	1.32	2.57	0.31	67.26	0.86	1.87	0.86	3.81	2.33	4.59	11.04	0.08	 e = f
0.87	0.67	1.52	0.03	0.37	89.33	0.53	0.03	1.6	0.28	3.43	1.29	0.06	 f = i
4.61	3.03	2.47	0.06	1.63	1.01	37.98	1.07	8.43	2.19	24.16	12.87	0.51	 g = k
3.86	0.58	1.93	0.0	1.45	0.58	1.55	75.85	1.84	1.06	3.96	7.05	0.29	 h = j
3.63	1.14	1.74	0.23	0.33	0.7	1.68	0.18	78.41	1.28	7.42	3.1	0.15	 i = l
1.47	0.62	0.77	0.39	0.41	0.18	0.65	0.11	2.14	84.46	7.46	1.32	0.03	 j = o
1.09	0.47	0.61	0.08	0.19	0.29	0.71	0.1	1.49	0.77	92.8	1.39	0.02	 k = 0
2.81	1.47	0.91	0.19	1.94	0.6	3.13	0.68	4.55	1.47	11.82	70.39	0.04	 l = s
0.58	0.77	0.48	0.05	0.05	0.63	0.63	0.19	1.63	0.63	1.15	0.38	92.84	 m = u
