Timestamp:2013-08-30-05:05:45
Inventario:I3
Intervalo:15ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I3-15ms-2gramas
Num Instances:  12188
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(10): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(9): class 
0ZCross(9): class 
0F1(Hz)(10): class 
0F2(Hz)(11): class 
0F3(Hz)(8): class 
0F4(Hz)(7): class 
1Int(dB)(9): class 
1Pitch(Hz)(6): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(10): class 
1F1(Hz)(11): class 
1F2(Hz)(9): class 
1F3(Hz)(8): class 
1F4(Hz)(7): class 
class(11): 
LogScore Bayes: -280329.1727118748
LogScore BDeu: -286691.72598819545
LogScore MDL: -286220.87475141283
LogScore ENTROPY: -279395.22047126613
LogScore AIC: -280846.22047126613




=== Stratified cross-validation ===

Correctly Classified Instances        7182               58.9268 %
Incorrectly Classified Instances      5006               41.0732 %
Kappa statistic                          0.4808
K&B Relative Info Score             599740.0528 %
K&B Information Score                15904.5917 bits      1.3049 bits/instance
Class complexity | order 0           32290.0118 bits      2.6493 bits/instance
Class complexity | scheme            78439.4928 bits      6.4358 bits/instance
Complexity improvement     (Sf)     -46149.481  bits     -3.7865 bits/instance
Mean absolute error                      0.0767
Root mean squared error                  0.2475
Relative absolute error                 53.4109 %
Root relative squared error             92.3811 %
Coverage of cases (0.95 level)          69.8064 %
Mean rel. region size (0.95 level)      16.1866 %
Total Number of Instances            12188     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,642    0,073    0,632      0,642    0,637      0,565    0,897     0,711     A
                 0,563    0,071    0,407      0,563    0,472      0,425    0,882     0,454     E
                 0,185    0,050    0,466      0,185    0,265      0,200    0,787     0,411     CGJLNQSRT
                 0,062    0,012    0,070      0,062    0,066      0,053    0,774     0,042     FV
                 0,634    0,042    0,397      0,634    0,488      0,475    0,919     0,466     I
                 0,297    0,024    0,513      0,297    0,376      0,353    0,827     0,404     O
                 0,927    0,124    0,809      0,927    0,864      0,783    0,970     0,960     0
                 0,020    0,005    0,119      0,020    0,034      0,036    0,777     0,101     BMP
                 0,474    0,015    0,426      0,474    0,449      0,435    0,918     0,478     U
                 0,000    0,003    0,000      0,000    0,000      -0,001   0,912     0,003     DZ
                 0,469    0,069    0,091      0,469    0,152      0,182    0,861     0,099     D
Weighted Avg.    0,589    0,077    0,590      0,589    0,569      0,510    0,891     0,645     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1276  169  119   32   48   80   98    2    7    4  154 |    a = A
  108  547   74    5   70   25   39    6   24    3   70 |    b = E
  275  271  430   52  203   69  616   24   60   22  303 |    c = CGJLNQSRT
   21   30   22   11   13   11   31    3    0    2   33 |    d = FV
   10   92   40    1  319    1   25    3    3    0    9 |    e = I
  171   87   67   14   27  279   89    7   52    1  146 |    f = O
   61   41   99   27   41   18 4094    7    2    7   20 |    g = 0
   45   68   44   11   71   15   54    8   26    1   56 |    h = BMP
   21   21   19    2    4   34    7    2  135    2   38 |    i = U
    0    0    0    0    0    0    5    0    0    0    0 |    j = DZ
   31   19    9    2    8   12    0    5    8    0   83 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
64.15	8.5	5.98	1.61	2.41	4.02	4.93	0.1	0.35	0.2	7.74	 a = A
11.12	56.33	7.62	0.51	7.21	2.57	4.02	0.62	2.47	0.31	7.21	 b = E
11.83	11.66	18.49	2.24	8.73	2.97	26.49	1.03	2.58	0.95	13.03	 c = CGJLNQSRT
11.86	16.95	12.43	6.21	7.34	6.21	17.51	1.69	0.0	1.13	18.64	 d = FV
1.99	18.29	7.95	0.2	63.42	0.2	4.97	0.6	0.6	0.0	1.79	 e = I
18.19	9.26	7.13	1.49	2.87	29.68	9.47	0.74	5.53	0.11	15.53	 f = O
1.38	0.93	2.24	0.61	0.93	0.41	92.69	0.16	0.05	0.16	0.45	 g = 0
11.28	17.04	11.03	2.76	17.79	3.76	13.53	2.01	6.52	0.25	14.04	 h = BMP
7.37	7.37	6.67	0.7	1.4	11.93	2.46	0.7	47.37	0.7	13.33	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
17.51	10.73	5.08	1.13	4.52	6.78	0.0	2.82	4.52	0.0	46.89	 k = D
