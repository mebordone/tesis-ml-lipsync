Timestamp:2013-08-30-04:43:16
Inventario:I2
Intervalo:20ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I2-20ms-5gramas
Num Instances:  9125
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 4Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  42 4Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  43 4RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  44 4SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  45 4Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  47 4F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  48 4F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  49 4F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  50 4F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(4): class 
0F1(Hz)(6): class 
0F2(Hz)(6): class 
0F3(Hz)(2): class 
0F4(Hz)(3): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(4): class 
1F1(Hz)(8): class 
1F2(Hz)(5): class 
1F3(Hz)(2): class 
1F4(Hz)(3): class 
2Int(dB)(9): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(5): class 
2ZCross(4): class 
2F1(Hz)(8): class 
2F2(Hz)(8): class 
2F3(Hz)(2): class 
2F4(Hz)(3): class 
3Int(dB)(8): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(4): class 
3F1(Hz)(9): class 
3F2(Hz)(8): class 
3F3(Hz)(2): class 
3F4(Hz)(3): class 
4Int(dB)(9): class 
4Pitch(Hz)(4): class 
4RawPitch(4): class 
4SmPitch(4): class 
4Melogram(st)(7): class 
4ZCross(8): class 
4F1(Hz)(9): class 
4F2(Hz)(9): class 
4F3(Hz)(5): class 
4F4(Hz)(5): class 
class(12): 
LogScore Bayes: -424718.51748794137
LogScore BDeu: -435058.6329876524
LogScore MDL: -433982.35292360897
LogScore ENTROPY: -422387.83282720926
LogScore AIC: -424930.83282720926




=== Stratified cross-validation ===

Correctly Classified Instances        4702               51.5288 %
Incorrectly Classified Instances      4423               48.4712 %
Kappa statistic                          0.4115
K&B Relative Info Score             363560.4343 %
K&B Information Score                10723.0007 bits      1.1751 bits/instance
Class complexity | order 0           26896.5513 bits      2.9476 bits/instance
Class complexity | scheme           119370.9684 bits     13.0817 bits/instance
Complexity improvement     (Sf)     -92474.4171 bits    -10.1342 bits/instance
Mean absolute error                      0.0813
Root mean squared error                  0.264 
Relative absolute error                 59.7118 %
Root relative squared error            101.227  %
Coverage of cases (0.95 level)          59.2329 %
Mean rel. region size (0.95 level)      12.9872 %
Total Number of Instances             9125     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,443    0,050    0,635      0,443    0,522      0,456    0,864     0,617     A
                 0,004    0,003    0,077      0,004    0,008      0,007    0,695     0,085     LGJKC
                 0,247    0,027    0,446      0,247    0,318      0,289    0,849     0,365     E
                 0,080    0,019    0,061      0,080    0,069      0,054    0,766     0,042     FV
                 0,532    0,043    0,353      0,532    0,424      0,403    0,893     0,410     I
                 0,201    0,025    0,403      0,201    0,268      0,244    0,788     0,292     O
                 0,332    0,067    0,354      0,332    0,342      0,272    0,787     0,287     SNRTY
                 0,892    0,081    0,858      0,892    0,874      0,804    0,945     0,885     0
                 0,023    0,004    0,171      0,023    0,041      0,052    0,739     0,085     BMP
                 0,399    0,087    0,066      0,399    0,113      0,132    0,830     0,064     DZ
                 0,385    0,021    0,311      0,385    0,344      0,328    0,892     0,403     U
                 0,408    0,114    0,137      0,408    0,205      0,178    0,809     0,137     SNRTXY
Weighted Avg.    0,515    0,057    0,551      0,515    0,516      0,467    0,863     0,538     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
  666   12   87   24   52  100   76   80    4  206   30  168 |    a = A
   38    2   20   17   33    9   85   69    1   44   11  121 |    b = LGJKC
   64    3  184    7  106   18   35   38    3  102   32  154 |    c = E
    9    1   12   11    2    5   22    9    3   19    1   44 |    d = FV
    5    1   21    2  205    1   37   17    1   20    2   73 |    e = I
   96    1   32    6   27  143   38   80    0  142   62   85 |    f = O
   56    2   11   47   46   15  303  147    8   97   16  166 |    g = SNRTY
   13    2    6   36   23    8  165 2884   12   32    5   47 |    h = 0
   27    1    6   16   44   10   40   18    7   29   11   90 |    i = BMP
   18    0    7    2    5    3   11    2    0   55    8   27 |    j = DZ
   16    1    8    1   12   35    8   12    1   16   84   24 |    k = U
   41    0   19   10   26    8   36    7    1   73    8  158 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
44.25	0.8	5.78	1.59	3.46	6.64	5.05	5.32	0.27	13.69	1.99	11.16	 a = A
8.44	0.44	4.44	3.78	7.33	2.0	18.89	15.33	0.22	9.78	2.44	26.89	 b = LGJKC
8.58	0.4	24.66	0.94	14.21	2.41	4.69	5.09	0.4	13.67	4.29	20.64	 c = E
6.52	0.72	8.7	7.97	1.45	3.62	15.94	6.52	2.17	13.77	0.72	31.88	 d = FV
1.3	0.26	5.45	0.52	53.25	0.26	9.61	4.42	0.26	5.19	0.52	18.96	 e = I
13.48	0.14	4.49	0.84	3.79	20.08	5.34	11.24	0.0	19.94	8.71	11.94	 f = O
6.13	0.22	1.2	5.14	5.03	1.64	33.15	16.08	0.88	10.61	1.75	18.16	 g = SNRTY
0.4	0.06	0.19	1.11	0.71	0.25	5.1	89.21	0.37	0.99	0.15	1.45	 h = 0
9.03	0.33	2.01	5.35	14.72	3.34	13.38	6.02	2.34	9.7	3.68	30.1	 i = BMP
13.04	0.0	5.07	1.45	3.62	2.17	7.97	1.45	0.0	39.86	5.8	19.57	 j = DZ
7.34	0.46	3.67	0.46	5.5	16.06	3.67	5.5	0.46	7.34	38.53	11.01	 k = U
10.59	0.0	4.91	2.58	6.72	2.07	9.3	1.81	0.26	18.86	2.07	40.83	 l = SNRTXY
