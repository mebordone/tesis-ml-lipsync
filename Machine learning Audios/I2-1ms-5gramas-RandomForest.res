Timestamp:2013-07-22-21:36:50
Inventario:I2
Intervalo:1ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I2-1ms-5gramas
Num Instances:  183400
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  49 4F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1241





=== Stratified cross-validation ===

Correctly Classified Instances      165859               90.4357 %
Incorrectly Classified Instances     17541                9.5643 %
Kappa statistic                          0.8789
K&B Relative Info Score            15848942.9765 %
K&B Information Score               452814.5733 bits      2.469  bits/instance
Class complexity | order 0          523966.3471 bits      2.857  bits/instance
Class complexity | scheme          6867210.8048 bits     37.4439 bits/instance
Complexity improvement     (Sf)    -6343244.4577 bits    -34.5869 bits/instance
Mean absolute error                      0.0242
Root mean squared error                  0.1095
Relative absolute error                 18.3191 %
Root relative squared error             42.5703 %
Coverage of cases (0.95 level)          96.5218 %
Mean rel. region size (0.95 level)      14.1543 %
Total Number of Instances           183400     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,943    0,009    0,948      0,943    0,946      0,936    0,982     0,967     A
                 0,746    0,011    0,780      0,746    0,763      0,751    0,931     0,800     LGJKC
                 0,941    0,004    0,952      0,941    0,946      0,942    0,983     0,963     E
                 0,744    0,003    0,793      0,744    0,768      0,765    0,929     0,789     FV
                 0,926    0,002    0,953      0,926    0,939      0,937    0,979     0,951     I
                 0,881    0,005    0,938      0,881    0,909      0,902    0,960     0,915     O
                 0,773    0,030    0,725      0,773    0,748      0,722    0,956     0,816     SNRTY
                 0,948    0,054    0,919      0,948    0,933      0,889    0,986     0,970     0
                 0,797    0,002    0,913      0,797    0,852      0,849    0,944     0,851     BMP
                 0,937    0,000    0,968      0,937    0,952      0,952    0,986     0,964     DZ
                 0,958    0,000    0,981      0,958    0,970      0,969    0,993     0,982     U
                 0,877    0,002    0,955      0,877    0,914      0,912    0,966     0,919     SNRTXY
Weighted Avg.    0,904    0,027    0,905      0,904    0,904      0,881    0,975     0,933     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 26932   187    44    34    17    35   495   730    41     9     6    27 |     a = A
   183  6582    78    61    20    50   880   880    38     2    20    33 |     b = LGJKC
    66   107 12931    21    28    31   227   253    29    11     6    30 |     c = E
    54    48    26  1905     9    26   354   101    21     3     1    13 |     d = FV
    40    49    30    15  6562    14   131   220    13     1     4     4 |     e = I
    81    92    28    31    15 11659   258  1004    29    20     0    17 |     f = O
   400   565   151   189    77   157 13344  2156   105    12    10   107 |     g = SNRTY
   443   564   185    85   110   341  1814 68358   129    14    12    58 |     h = 0
   101   129    40    24    31    41   379   419  4636     2     9     3 |     i = BMP
    18     8    16     8     3    27    50    23     5  2513     3     8 |     j = DZ
    16    27    16     0    10    10    41    36    10     2  3965     4 |     k = U
    65    76    43    28     6    35   425   197    19     7     6  6472 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
94.31	0.65	0.15	0.12	0.06	0.12	1.73	2.56	0.14	0.03	0.02	0.09	 a = A
2.07	74.57	0.88	0.69	0.23	0.57	9.97	9.97	0.43	0.02	0.23	0.37	 b = LGJKC
0.48	0.78	94.11	0.15	0.2	0.23	1.65	1.84	0.21	0.08	0.04	0.22	 c = E
2.11	1.87	1.02	74.39	0.35	1.02	13.82	3.94	0.82	0.12	0.04	0.51	 d = FV
0.56	0.69	0.42	0.21	92.64	0.2	1.85	3.11	0.18	0.01	0.06	0.06	 e = I
0.61	0.7	0.21	0.23	0.11	88.1	1.95	7.59	0.22	0.15	0.0	0.13	 f = O
2.32	3.27	0.87	1.09	0.45	0.91	77.25	12.48	0.61	0.07	0.06	0.62	 g = SNRTY
0.61	0.78	0.26	0.12	0.15	0.47	2.52	94.79	0.18	0.02	0.02	0.08	 h = 0
1.74	2.22	0.69	0.41	0.53	0.71	6.52	7.21	79.74	0.03	0.15	0.05	 i = BMP
0.67	0.3	0.6	0.3	0.11	1.01	1.86	0.86	0.19	93.7	0.11	0.3	 j = DZ
0.39	0.65	0.39	0.0	0.24	0.24	0.99	0.87	0.24	0.05	95.84	0.1	 k = U
0.88	1.03	0.58	0.38	0.08	0.47	5.76	2.67	0.26	0.09	0.08	87.71	 l = SNRTXY
