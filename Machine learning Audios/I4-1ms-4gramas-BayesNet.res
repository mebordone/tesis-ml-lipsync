Timestamp:2013-08-30-05:11:26
Inventario:I4
Intervalo:1ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I4-1ms-4gramas
Num Instances:  183401
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(34): class 
0Pitch(Hz)(96): class 
0RawPitch(77): class 
0SmPitch(100): class 
0Melogram(st)(87): class 
0ZCross(20): class 
0F1(Hz)(2660): class 
0F2(Hz)(3340): class 
0F3(Hz)(3375): class 
0F4(Hz)(3363): class 
1Int(dB)(33): class 
1Pitch(Hz)(95): class 
1RawPitch(74): class 
1SmPitch(95): class 
1Melogram(st)(92): class 
1ZCross(20): class 
1F1(Hz)(2627): class 
1F2(Hz)(3334): class 
1F3(Hz)(3371): class 
1F4(Hz)(3370): class 
2Int(dB)(33): class 
2Pitch(Hz)(97): class 
2RawPitch(72): class 
2SmPitch(95): class 
2Melogram(st)(91): class 
2ZCross(20): class 
2F1(Hz)(2635): class 
2F2(Hz)(3341): class 
2F3(Hz)(3378): class 
2F4(Hz)(3371): class 
3Int(dB)(33): class 
3Pitch(Hz)(93): class 
3RawPitch(72): class 
3SmPitch(93): class 
3Melogram(st)(91): class 
3ZCross(20): class 
3F1(Hz)(2623): class 
3F2(Hz)(3318): class 
3F3(Hz)(3383): class 
3F4(Hz)(3362): class 
class(10): 
LogScore Bayes: -2.2464819185470987E7
LogScore BDeu: -2.918816518138664E7
LogScore MDL: -2.7523381487081453E7
LogScore ENTROPY: -2.434536993864673E7
LogScore AIC: -2.486981893864673E7




=== Stratified cross-validation ===

Correctly Classified Instances      148438               80.9363 %
Incorrectly Classified Instances     34963               19.0637 %
Kappa statistic                          0.7488
K&B Relative Info Score            14014128.1022 %
K&B Information Score               356479.2729 bits      1.9437 bits/instance
Class complexity | order 0          466503.9053 bits      2.5436 bits/instance
Class complexity | scheme          1707644.4686 bits      9.311  bits/instance
Complexity improvement     (Sf)    -1241140.5633 bits     -6.7674 bits/instance
Mean absolute error                      0.0382
Root mean squared error                  0.192 
Relative absolute error                 24.8014 %
Root relative squared error             69.2214 %
Coverage of cases (0.95 level)          82.107  %
Mean rel. region size (0.95 level)      10.4005 %
Total Number of Instances           183401     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,897    0,018    0,226      0,897    0,361      0,445    0,958     0,780     C
                 0,883    0,012    0,858      0,883    0,871      0,860    0,970     0,905     E
                 0,695    0,010    0,492      0,695    0,576      0,578    0,930     0,662     FV
                 0,852    0,013    0,941      0,852    0,894      0,872    0,967     0,939     AI
                 0,512    0,017    0,846      0,512    0,638      0,614    0,964     0,836     CDGKNRSYZ
                 0,834    0,009    0,881      0,834    0,857      0,846    0,935     0,867     O
                 0,909    0,159    0,787      0,909    0,844      0,736    0,968     0,946     0
                 0,574    0,009    0,704      0,574    0,632      0,623    0,936     0,634     LT
                 0,939    0,007    0,761      0,939    0,841      0,841    0,982     0,928     U
                 0,747    0,007    0,780      0,747    0,763      0,756    0,928     0,768     MBP
Weighted Avg.    0,809    0,070    0,827      0,809    0,808      0,756    0,962     0,897     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   971    16     0     8    12     0    75     0     1     0 |     a = C
   221 12138   115    58   332    21   574   119    43   119 |     b = E
    15    26  1779    12    49    10   651    16     3     0 |     c = FV
   922   187   419 30357   669   180  1992   483   102   329 |     d = AI
  1137   884    96   722 14561   541  9672   217   442   187 |     e = CDGKNRSYZ
   236    13    43    61   152 11041  1461   103    11   113 |     f = O
   630   772  1128   928   887   628 65567   589   545   440 |     g = 0
    82    70    18    76   364    58  2095  3798    33    25 |     h = LT
    25     7    10     5    70     0    81    46  3883    10 |     i = U
    54    31     9    43   115    58  1096    27    38  4343 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
89.66	1.48	0.0	0.74	1.11	0.0	6.93	0.0	0.09	0.0	 a = C
1.61	88.34	0.84	0.42	2.42	0.15	4.18	0.87	0.31	0.87	 b = E
0.59	1.02	69.47	0.47	1.91	0.39	25.42	0.62	0.12	0.0	 c = FV
2.59	0.52	1.18	85.18	1.88	0.51	5.59	1.36	0.29	0.92	 d = AI
4.0	3.11	0.34	2.54	51.16	1.9	33.99	0.76	1.55	0.66	 e = CDGKNRSYZ
1.78	0.1	0.32	0.46	1.15	83.43	11.04	0.78	0.08	0.85	 f = O
0.87	1.07	1.56	1.29	1.23	0.87	90.92	0.82	0.76	0.61	 g = 0
1.24	1.06	0.27	1.15	5.5	0.88	31.65	57.38	0.5	0.38	 h = LT
0.6	0.17	0.24	0.12	1.69	0.0	1.96	1.11	93.86	0.24	 i = U
0.93	0.53	0.15	0.74	1.98	1.0	18.85	0.46	0.65	74.7	 j = MBP
