Timestamp:2013-07-22-23:04:31
Inventario:I4
Intervalo:10ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I4-10ms-2gramas
Num Instances:  18295
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.2972





=== Stratified cross-validation ===

Correctly Classified Instances       13861               75.7639 %
Incorrectly Classified Instances      4434               24.2361 %
Kappa statistic                          0.6872
K&B Relative Info Score            1197405.0682 %
K&B Information Score                30931.5668 bits      1.6907 bits/instance
Class complexity | order 0           47241.9618 bits      2.5822 bits/instance
Class complexity | scheme          1192971.8537 bits     65.2075 bits/instance
Complexity improvement     (Sf)    -1145729.8919 bits    -62.6253 bits/instance
Mean absolute error                      0.0672
Root mean squared error                  0.1857
Relative absolute error                 43.0715 %
Root relative squared error             66.4598 %
Coverage of cases (0.95 level)          93.9601 %
Mean rel. region size (0.95 level)      25.66   %
Total Number of Instances            18295     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,467    0,001    0,658      0,467    0,546      0,552    0,924     0,508     C
                 0,726    0,036    0,630      0,726    0,675      0,647    0,939     0,710     E
                 0,291    0,004    0,484      0,291    0,364      0,369    0,819     0,233     FV
                 0,819    0,071    0,745      0,819    0,781      0,723    0,943     0,854     AI
                 0,676    0,084    0,605      0,676    0,639      0,567    0,900     0,660     CDGKNRSYZ
                 0,633    0,022    0,704      0,633    0,667      0,642    0,910     0,688     O
                 0,898    0,058    0,901      0,898    0,900      0,840    0,968     0,945     0
                 0,300    0,010    0,526      0,300    0,382      0,380    0,830     0,295     LT
                 0,667    0,003    0,847      0,667    0,746      0,746    0,948     0,772     U
                 0,379    0,007    0,650      0,379    0,479      0,484    0,864     0,443     MBP
Weighted Avg.    0,758    0,055    0,756      0,758    0,753      0,704    0,934     0,786     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   50   17    0   24    5    4    3    1    2    1 |    a = C
    6 1044    7  154  136   27   26   22    3   13 |    b = E
    0   34   76   28   92   14   12    2    0    3 |    c = FV
    7  203    8 3022  221   47  117   36    8   19 |    d = AI
    4  154   28  335 1984   88  263   36   10   31 |    e = CDGKNRSYZ
    2   68    8  119  154  872  107   17   10   20 |    f = O
    1   44   12  157  340   76 6103   45    4   15 |    g = 0
    0   41    6  106  187   28   92  203    5    9 |    h = LT
    4   14    1   21   37   45    4    5  282   10 |    i = U
    2   37   11   88  122   37   44   19    9  225 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
46.73	15.89	0.0	22.43	4.67	3.74	2.8	0.93	1.87	0.93	 a = C
0.42	72.6	0.49	10.71	9.46	1.88	1.81	1.53	0.21	0.9	 b = E
0.0	13.03	29.12	10.73	35.25	5.36	4.6	0.77	0.0	1.15	 c = FV
0.19	5.5	0.22	81.94	5.99	1.27	3.17	0.98	0.22	0.52	 d = AI
0.14	5.25	0.95	11.42	67.64	3.0	8.97	1.23	0.34	1.06	 e = CDGKNRSYZ
0.15	4.94	0.58	8.64	11.18	63.33	7.77	1.23	0.73	1.45	 f = O
0.01	0.65	0.18	2.31	5.0	1.12	89.79	0.66	0.06	0.22	 g = 0
0.0	6.06	0.89	15.66	27.62	4.14	13.59	29.99	0.74	1.33	 h = LT
0.95	3.31	0.24	4.96	8.75	10.64	0.95	1.18	66.67	2.36	 i = U
0.34	6.23	1.85	14.81	20.54	6.23	7.41	3.2	1.52	37.88	 j = MBP
