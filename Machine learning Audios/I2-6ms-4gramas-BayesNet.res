Timestamp:2013-08-30-04:40:52
Inventario:I2
Intervalo:6ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I2-6ms-4gramas
Num Instances:  30450
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(15): class 
0Pitch(Hz)(11): class 
0RawPitch(10): class 
0SmPitch(9): class 
0Melogram(st)(16): class 
0ZCross(12): class 
0F1(Hz)(17): class 
0F2(Hz)(20): class 
0F3(Hz)(18): class 
0F4(Hz)(14): class 
1Int(dB)(15): class 
1Pitch(Hz)(11): class 
1RawPitch(11): class 
1SmPitch(10): class 
1Melogram(st)(17): class 
1ZCross(12): class 
1F1(Hz)(21): class 
1F2(Hz)(18): class 
1F3(Hz)(17): class 
1F4(Hz)(14): class 
2Int(dB)(15): class 
2Pitch(Hz)(11): class 
2RawPitch(11): class 
2SmPitch(11): class 
2Melogram(st)(19): class 
2ZCross(12): class 
2F1(Hz)(20): class 
2F2(Hz)(19): class 
2F3(Hz)(16): class 
2F4(Hz)(18): class 
3Int(dB)(14): class 
3Pitch(Hz)(12): class 
3RawPitch(13): class 
3SmPitch(11): class 
3Melogram(st)(18): class 
3ZCross(12): class 
3F1(Hz)(20): class 
3F2(Hz)(19): class 
3F3(Hz)(17): class 
3F4(Hz)(15): class 
class(12): 
LogScore Bayes: -1823937.3927352324
LogScore BDeu: -1859232.7278516386
LogScore MDL: -1854651.9965856157
LogScore ENTROPY: -1820464.5962096192
LogScore AIC: -1827087.5962096192




=== Stratified cross-validation ===

Correctly Classified Instances       17751               58.2956 %
Incorrectly Classified Instances     12699               41.7044 %
Kappa statistic                          0.4755
K&B Relative Info Score            1411112.9972 %
K&B Information Score                40734.0955 bits      1.3377 bits/instance
Class complexity | order 0           87885.5941 bits      2.8862 bits/instance
Class complexity | scheme           413990.88   bits     13.5958 bits/instance
Complexity improvement     (Sf)    -326105.2859 bits    -10.7095 bits/instance
Mean absolute error                      0.0697
Root mean squared error                  0.249 
Relative absolute error                 52.2105 %
Root relative squared error             96.3447 %
Coverage of cases (0.95 level)          64.3645 %
Mean rel. region size (0.95 level)      11.2485 %
Total Number of Instances            30450     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,596    0,047    0,707      0,596    0,647      0,590    0,903     0,719     A
                 0,029    0,010    0,127      0,029    0,047      0,038    0,734     0,100     LGJKC
                 0,565    0,058    0,449      0,565    0,500      0,457    0,887     0,489     E
                 0,116    0,019    0,082      0,116    0,096      0,082    0,808     0,077     FV
                 0,661    0,038    0,417      0,661    0,511      0,501    0,920     0,482     I
                 0,297    0,020    0,539      0,297    0,383      0,366    0,838     0,428     O
                 0,219    0,038    0,376      0,219    0,277      0,232    0,849     0,341     SNRTY
                 0,896    0,130    0,810      0,896    0,851      0,754    0,957     0,951     0
                 0,054    0,005    0,277      0,054    0,091      0,110    0,791     0,133     BMP
                 0,550    0,064    0,115      0,550    0,190      0,229    0,879     0,157     DZ
                 0,522    0,025    0,327      0,522    0,402      0,396    0,927     0,505     U
                 0,233    0,036    0,218      0,233    0,225      0,191    0,850     0,186     SNRTXY
Weighted Avg.    0,583    0,072    0,582      0,583    0,568      0,512    0,898     0,629     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  2875    67   372    97   115   191   175   234    11   425    94   169 |     a = A
   132    43   166    52   171    46   157   409     5   155    49   110 |     b = LGJKC
   200    39  1325    44   176    39    94    98    17   140    63   112 |     c = E
    24     2    55    50    30    20    48    84     8    75    14    20 |     d = FV
    15    21   154     9   797     0    74    60    10    15    16    35 |     e = I
   305    38   188    24    55   669    89   227    15   340   214    87 |     f = O
   165    48   170   104   108    50   638  1062    26   253    52   236 |     g = SNRTY
   115    43   163   130   131    80   240 10398    22   122    29   135 |     h = 0
    66    13    90    55   175    31    61   153    53   120   101    62 |     i = BMP
    44     0    28    12    19    15    13    12     3   249    23    35 |     j = DZ
    32    18    50    11     5    65    20    17     8    67   364    40 |     k = U
    94     6   189    21   129    35    87    85    13   204    93   290 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
59.59	1.39	7.71	2.01	2.38	3.96	3.63	4.85	0.23	8.81	1.95	3.5	 a = A
8.83	2.88	11.1	3.48	11.44	3.08	10.5	27.36	0.33	10.37	3.28	7.36	 b = LGJKC
8.52	1.66	56.46	1.87	7.5	1.66	4.01	4.18	0.72	5.97	2.68	4.77	 c = E
5.58	0.47	12.79	11.63	6.98	4.65	11.16	19.53	1.86	17.44	3.26	4.65	 d = FV
1.24	1.74	12.77	0.75	66.09	0.0	6.14	4.98	0.83	1.24	1.33	2.9	 e = I
13.55	1.69	8.35	1.07	2.44	29.72	3.95	10.08	0.67	15.1	9.51	3.86	 f = O
5.67	1.65	5.84	3.57	3.71	1.72	21.91	36.47	0.89	8.69	1.79	8.1	 g = SNRTY
0.99	0.37	1.4	1.12	1.13	0.69	2.07	89.58	0.19	1.05	0.25	1.16	 h = 0
6.73	1.33	9.18	5.61	17.86	3.16	6.22	15.61	5.41	12.24	10.31	6.33	 i = BMP
9.71	0.0	6.18	2.65	4.19	3.31	2.87	2.65	0.66	54.97	5.08	7.73	 j = DZ
4.59	2.58	7.17	1.58	0.72	9.33	2.87	2.44	1.15	9.61	52.22	5.74	 k = U
7.54	0.48	15.17	1.69	10.35	2.81	6.98	6.82	1.04	16.37	7.46	23.27	 l = SNRTXY
