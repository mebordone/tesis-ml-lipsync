Timestamp:2013-07-22-23:00:34
Inventario:I4
Intervalo:3ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I4-3ms-3gramas
Num Instances:  61133
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1762





=== Stratified cross-validation ===

Correctly Classified Instances       52950               86.6144 %
Incorrectly Classified Instances      8183               13.3856 %
Kappa statistic                          0.8256
K&B Relative Info Score            4822047.244  %
K&B Information Score               123147.7713 bits      2.0144 bits/instance
Class complexity | order 0          156106.2443 bits      2.5536 bits/instance
Class complexity | scheme          2451620.7047 bits     40.1031 bits/instance
Complexity improvement     (Sf)    -2295514.4604 bits    -37.5495 bits/instance
Mean absolute error                      0.0435
Root mean squared error                  0.1434
Relative absolute error                 28.1744 %
Root relative squared error             51.5929 %
Coverage of cases (0.95 level)          96.3015 %
Mean rel. region size (0.95 level)      20.757  %
Total Number of Instances            61133     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,840    0,001    0,902      0,840    0,870      0,870    0,969     0,887     C
                 0,892    0,012    0,858      0,892    0,875      0,865    0,974     0,920     E
                 0,619    0,003    0,749      0,619    0,678      0,677    0,918     0,670     FV
                 0,905    0,029    0,884      0,905    0,895      0,869    0,973     0,943     AI
                 0,812    0,047    0,763      0,812    0,787      0,746    0,956     0,851     CDGKNRSYZ
                 0,820    0,008    0,887      0,820    0,852      0,842    0,951     0,877     O
                 0,926    0,059    0,908      0,926    0,917      0,864    0,979     0,960     0
                 0,525    0,008    0,711      0,525    0,604      0,598    0,891     0,600     LT
                 0,884    0,001    0,947      0,884    0,915      0,913    0,983     0,939     U
                 0,692    0,004    0,860      0,692    0,767      0,765    0,928     0,771     MBP
Weighted Avg.    0,866    0,038    0,865      0,866    0,864      0,831    0,966     0,906     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   305    14     1    11    18     2     9     1     2     0 |     a = C
     6  4124    11   111   183    19   113    34     6    14 |     b = E
     1    30   535    40   184    18    35    13     0     8 |     c = FV
    10   148    21 10834   385    63   380    76     8    42 |     d = AI
     4   157    70   434  7780   108   798   160    15    50 |     e = CDGKNRSYZ
     1    45    11   133   201  3660   363    29     4    16 |     f = O
     5   165    32   375   823   167 21974   112     7    64 |     g = 0
     2    61    16   166   401    37   344  1165    10    19 |     h = LT
     4    17     2    32    43    21    19    17  1225     6 |     i = U
     0    46    15   118   183    30   160    32    16  1348 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
84.02	3.86	0.28	3.03	4.96	0.55	2.48	0.28	0.55	0.0	 a = C
0.13	89.24	0.24	2.4	3.96	0.41	2.45	0.74	0.13	0.3	 b = E
0.12	3.47	61.92	4.63	21.3	2.08	4.05	1.5	0.0	0.93	 c = FV
0.08	1.24	0.18	90.53	3.22	0.53	3.18	0.64	0.07	0.35	 d = AI
0.04	1.64	0.73	4.53	81.24	1.13	8.33	1.67	0.16	0.52	 e = CDGKNRSYZ
0.02	1.01	0.25	2.98	4.5	82.01	8.13	0.65	0.09	0.36	 f = O
0.02	0.7	0.13	1.58	3.47	0.7	92.62	0.47	0.03	0.27	 g = 0
0.09	2.75	0.72	7.47	18.05	1.67	15.49	52.45	0.45	0.86	 h = LT
0.29	1.23	0.14	2.31	3.1	1.52	1.37	1.23	88.38	0.43	 i = U
0.0	2.36	0.77	6.06	9.39	1.54	8.21	1.64	0.82	69.2	 j = MBP
