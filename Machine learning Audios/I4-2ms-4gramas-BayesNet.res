Timestamp:2013-08-30-05:21:12
Inventario:I4
Intervalo:2ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I4-2ms-4gramas
Num Instances:  91700
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(25): class 
0Pitch(Hz)(45): class 
0RawPitch(32): class 
0SmPitch(36): class 
0Melogram(st)(48): class 
0ZCross(17): class 
0F1(Hz)(1227): class 
0F2(Hz)(2144): class 
0F3(Hz)(2236): class 
0F4(Hz)(2060): class 
1Int(dB)(24): class 
1Pitch(Hz)(44): class 
1RawPitch(38): class 
1SmPitch(38): class 
1Melogram(st)(49): class 
1ZCross(17): class 
1F1(Hz)(1443): class 
1F2(Hz)(2097): class 
1F3(Hz)(2201): class 
1F4(Hz)(1978): class 
2Int(dB)(24): class 
2Pitch(Hz)(45): class 
2RawPitch(33): class 
2SmPitch(38): class 
2Melogram(st)(47): class 
2ZCross(18): class 
2F1(Hz)(1457): class 
2F2(Hz)(1987): class 
2F3(Hz)(1986): class 
2F4(Hz)(2005): class 
3Int(dB)(22): class 
3Pitch(Hz)(47): class 
3RawPitch(34): class 
3SmPitch(39): class 
3Melogram(st)(45): class 
3ZCross(18): class 
3F1(Hz)(1497): class 
3F2(Hz)(1915): class 
3F3(Hz)(1983): class 
3F4(Hz)(2163): class 
class(10): 
LogScore Bayes: -1.0130324265541028E7
LogScore BDeu: -1.3894975571635818E7
LogScore MDL: -1.295288121179351E7
LogScore ENTROPY: -1.1172501471612968E7
LogScore AIC: -1.1484130471612966E7




=== Stratified cross-validation ===

Correctly Classified Instances       69839               76.1603 %
Incorrectly Classified Instances     21861               23.8397 %
Kappa statistic                          0.6875
K&B Relative Info Score            6471784.8169 %
K&B Information Score               164958.4696 bits      1.7989 bits/instance
Class complexity | order 0          233713.0492 bits      2.5487 bits/instance
Class complexity | scheme           942734.2571 bits     10.2806 bits/instance
Complexity improvement     (Sf)    -709021.2079 bits     -7.732  bits/instance
Mean absolute error                      0.0479
Root mean squared error                  0.2121
Relative absolute error                 31.0435 %
Root relative squared error             76.3862 %
Coverage of cases (0.95 level)          78.6848 %
Mean rel. region size (0.95 level)      10.9413 %
Total Number of Instances            91700     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,844    0,019    0,206      0,844    0,332      0,412    0,952     0,699     C
                 0,854    0,034    0,670      0,854    0,751      0,734    0,957     0,842     E
                 0,634    0,010    0,484      0,634    0,549      0,547    0,905     0,622     FV
                 0,802    0,027    0,878      0,802    0,838      0,802    0,954     0,902     AI
                 0,369    0,021    0,768      0,369    0,499      0,480    0,931     0,715     CDGKNRSYZ
                 0,786    0,015    0,805      0,786    0,795      0,779    0,924     0,829     O
                 0,903    0,157    0,786      0,903    0,840      0,731    0,963     0,941     0
                 0,473    0,009    0,671      0,473    0,555      0,550    0,900     0,561     LT
                 0,914    0,011    0,656      0,914    0,764      0,769    0,976     0,912     U
                 0,687    0,010    0,690      0,687    0,689      0,679    0,900     0,716     MBP
Weighted Avg.    0,762    0,075    0,776      0,762    0,753      0,698    0,948     0,855     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   459    29     1     3     9     1    36     0     6     0 |     a = C
   152  5902    48    95   224    24   295    70    36    64 |     b = E
    21    47   815    25    20    14   323     9     5     7 |     c = FV
   408   660   218 14349   482   225   997   201   133   221 |     d = AI
   650  1208   113   971  5272   465  4747   126   447   273 |     e = CDGKNRSYZ
   168   145    20   123    93  5223   729    42    47    56 |     f = O
   185   587   441   642   461   454 32341   264   220   235 |     g = 0
   110   155    13    79   193    50  1073  1572    54    24 |     h = LT
    14     8     7     8    44     4    44    29  1902    20 |     i = U
    57    68     8    54    69    30   547    29    49  2004 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
84.38	5.33	0.18	0.55	1.65	0.18	6.62	0.0	1.1	0.0	 a = C
2.2	85.41	0.69	1.37	3.24	0.35	4.27	1.01	0.52	0.93	 b = E
1.63	3.65	63.37	1.94	1.56	1.09	25.12	0.7	0.39	0.54	 c = FV
2.28	3.69	1.22	80.19	2.69	1.26	5.57	1.12	0.74	1.24	 d = AI
4.55	8.46	0.79	6.8	36.94	3.26	33.26	0.88	3.13	1.91	 e = CDGKNRSYZ
2.53	2.18	0.3	1.85	1.4	78.59	10.97	0.63	0.71	0.84	 f = O
0.52	1.64	1.23	1.79	1.29	1.27	90.26	0.74	0.61	0.66	 g = 0
3.31	4.66	0.39	2.38	5.81	1.5	32.29	47.31	1.63	0.72	 h = LT
0.67	0.38	0.34	0.38	2.12	0.19	2.12	1.39	91.44	0.96	 i = U
1.96	2.33	0.27	1.85	2.37	1.03	18.77	0.99	1.68	68.75	 j = MBP
