Timestamp:2013-07-22-21:51:24
Inventario:I2
Intervalo:3ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I2-3ms-3gramas
Num Instances:  61133
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1836





=== Stratified cross-validation ===

Correctly Classified Instances       52566               85.9863 %
Incorrectly Classified Instances      8567               14.0137 %
Kappa statistic                          0.8233
K&B Relative Info Score            4855765.009  %
K&B Information Score               139367.7397 bits      2.2797 bits/instance
Class complexity | order 0          175445.1587 bits      2.8699 bits/instance
Class complexity | scheme          2826360.5607 bits     46.233  bits/instance
Complexity improvement     (Sf)    -2650915.402 bits    -43.3631 bits/instance
Mean absolute error                      0.0381
Root mean squared error                  0.1345
Relative absolute error                 28.6901 %
Root relative squared error             52.1648 %
Coverage of cases (0.95 level)          95.7273 %
Mean rel. region size (0.95 level)      18.1415 %
Total Number of Instances            61133     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,916    0,023    0,881      0,916    0,898      0,879    0,974     0,943     A
                 0,650    0,014    0,708      0,650    0,678      0,663    0,913     0,695     LGJKC
                 0,892    0,011    0,865      0,892    0,878      0,868    0,976     0,921     E
                 0,628    0,003    0,746      0,628    0,682      0,681    0,917     0,677     FV
                 0,866    0,005    0,869      0,866    0,868      0,863    0,965     0,896     I
                 0,830    0,010    0,869      0,830    0,849      0,838    0,949     0,872     O
                 0,700    0,037    0,667      0,700    0,683      0,649    0,938     0,719     SNRTY
                 0,928    0,060    0,908      0,928    0,918      0,865    0,979     0,958     0
                 0,702    0,004    0,840      0,702    0,765      0,761    0,927     0,769     BMP
                 0,814    0,001    0,894      0,814    0,852      0,851    0,974     0,886     DZ
                 0,896    0,001    0,948      0,896    0,921      0,920    0,983     0,942     U
                 0,809    0,004    0,903      0,809    0,854      0,849    0,955     0,864     SNRTXY
Weighted Avg.    0,860    0,033    0,860      0,860    0,859      0,829    0,964     0,894     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  8776    89    45     4    17    56   228   307    21     9     7    26 |     a = A
   152  1927    61    22    27    55   349   319    13     3    16    19 |     b = LGJKC
    92    56  4120    10    34    24   105   106    27    15     6    26 |     c = E
    27    25    28   543    16    16   153    35    14     1     0     6 |     d = FV
    23    35    59     9  2064    11    68    93    13     1     1     5 |     e = I
   108    43    44    12    14  3704   111   359    19    18    10    21 |     f = O
   307   239   110    72    53    96  4067   732    67    12     6    47 |     g = SNRTY
   293   213   164    33    86   171   626 22008    66    12     9    43 |     h = 0
    71    25    43    17    40    34   173   161  1368     1    11     4 |     i = BMP
    29     3    29     4     4    40    26    15     2   736     1    15 |     j = DZ
    14    22     9     1    11    26    26    17    12     3  1242     3 |     k = U
    65    44    51     1     8    28   168    89     7    12     1  2011 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
91.56	0.93	0.47	0.04	0.18	0.58	2.38	3.2	0.22	0.09	0.07	0.27	 a = A
5.13	65.04	2.06	0.74	0.91	1.86	11.78	10.77	0.44	0.1	0.54	0.64	 b = LGJKC
1.99	1.21	89.16	0.22	0.74	0.52	2.27	2.29	0.58	0.32	0.13	0.56	 c = E
3.13	2.89	3.24	62.85	1.85	1.85	17.71	4.05	1.62	0.12	0.0	0.69	 d = FV
0.97	1.47	2.48	0.38	86.65	0.46	2.85	3.9	0.55	0.04	0.04	0.21	 e = I
2.42	0.96	0.99	0.27	0.31	82.99	2.49	8.04	0.43	0.4	0.22	0.47	 f = O
5.29	4.12	1.89	1.24	0.91	1.65	70.02	12.6	1.15	0.21	0.1	0.81	 g = SNRTY
1.24	0.9	0.69	0.14	0.36	0.72	2.64	92.77	0.28	0.05	0.04	0.18	 h = 0
3.64	1.28	2.21	0.87	2.05	1.75	8.88	8.26	70.23	0.05	0.56	0.21	 i = BMP
3.21	0.33	3.21	0.44	0.44	4.42	2.88	1.66	0.22	81.42	0.11	1.66	 j = DZ
1.01	1.59	0.65	0.07	0.79	1.88	1.88	1.23	0.87	0.22	89.61	0.22	 k = U
2.62	1.77	2.05	0.04	0.32	1.13	6.76	3.58	0.28	0.48	0.04	80.93	 l = SNRTXY
