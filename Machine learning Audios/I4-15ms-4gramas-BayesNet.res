Timestamp:2013-08-30-05:29:25
Inventario:I4
Intervalo:15ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I4-15ms-4gramas
Num Instances:  12186
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(5): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(5): class 
0F1(Hz)(9): class 
0F2(Hz)(6): class 
0F3(Hz)(4): class 
0F4(Hz)(7): class 
1Int(dB)(9): class 
1Pitch(Hz)(5): class 
1RawPitch(4): class 
1SmPitch(5): class 
1Melogram(st)(9): class 
1ZCross(4): class 
1F1(Hz)(9): class 
1F2(Hz)(9): class 
1F3(Hz)(6): class 
1F4(Hz)(7): class 
2Int(dB)(9): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(7): class 
2F1(Hz)(10): class 
2F2(Hz)(8): class 
2F3(Hz)(7): class 
2F4(Hz)(6): class 
3Int(dB)(9): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(8): class 
3ZCross(8): class 
3F1(Hz)(11): class 
3F2(Hz)(10): class 
3F3(Hz)(9): class 
3F4(Hz)(5): class 
class(10): 
LogScore Bayes: -522519.9268290028
LogScore BDeu: -531686.5964477648
LogScore MDL: -531037.691186383
LogScore ENTROPY: -520552.4272285469
LogScore AIC: -522781.4272285469




=== Stratified cross-validation ===

Correctly Classified Instances        6597               54.1359 %
Incorrectly Classified Instances      5589               45.8641 %
Kappa statistic                          0.4224
K&B Relative Info Score             512410.2197 %
K&B Information Score                13324.8432 bits      1.0935 bits/instance
Class complexity | order 0           31670.8788 bits      2.599  bits/instance
Class complexity | scheme           139280.2861 bits     11.4295 bits/instance
Complexity improvement     (Sf)    -107609.4073 bits     -8.8306 bits/instance
Mean absolute error                      0.0922
Root mean squared error                  0.2821
Relative absolute error                 58.7495 %
Root relative squared error            100.6836 %
Coverage of cases (0.95 level)          62.0466 %
Mean rel. region size (0.95 level)      15.0353 %
Total Number of Instances            12186     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,403    0,074    0,031      0,403    0,058      0,095    0,848     0,031     C
                 0,508    0,097    0,312      0,508    0,386      0,331    0,845     0,369     E
                 0,079    0,022    0,049      0,079    0,061      0,045    0,770     0,041     FV
                 0,446    0,068    0,629      0,446    0,522      0,433    0,846     0,632     AI
                 0,188    0,047    0,434      0,188    0,262      0,202    0,748     0,358     CDGKNRSYZ
                 0,347    0,037    0,438      0,347    0,387      0,345    0,820     0,376     O
                 0,910    0,116    0,817      0,910    0,861      0,778    0,951     0,904     0
                 0,074    0,025    0,102      0,074    0,086      0,057    0,728     0,077     LT
                 0,460    0,019    0,372      0,460    0,411      0,398    0,910     0,460     U
                 0,178    0,030    0,167      0,178    0,172      0,143    0,766     0,111     MBP
Weighted Avg.    0,541    0,077    0,573      0,541    0,541      0,474    0,860     0,591     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   29   22    0    6    6    0    1    0    3    5 |    a = C
  132  493   14   98   41   32   50   46   37   28 |    b = E
   21   43   14   21   25   14   23    5    2    9 |    c = FV
  257  420   55 1111  129  137  139  101   41  102 |    d = AI
  244  268   93  262  371  107  420   56   41  116 |    e = CDGKNRSYZ
   74  123   15  131   46  326   98   34   65   28 |    f = O
   23   51   47   53  147   24 4017   24    5   24 |    g = 0
   54   79   28   34   51   27  113   34    8   29 |    h = LT
   31   18    1   14    5   45   14   13  131   13 |    i = U
   63   64   17   36   34   33   42   20   19   71 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
40.28	30.56	0.0	8.33	8.33	0.0	1.39	0.0	4.17	6.94	 a = C
13.59	50.77	1.44	10.09	4.22	3.3	5.15	4.74	3.81	2.88	 b = E
11.86	24.29	7.91	11.86	14.12	7.91	12.99	2.82	1.13	5.08	 c = FV
10.31	16.85	2.21	44.58	5.18	5.5	5.58	4.05	1.65	4.09	 d = AI
12.34	13.55	4.7	13.25	18.76	5.41	21.23	2.83	2.07	5.86	 e = CDGKNRSYZ
7.87	13.09	1.6	13.94	4.89	34.68	10.43	3.62	6.91	2.98	 f = O
0.52	1.16	1.06	1.2	3.33	0.54	90.99	0.54	0.11	0.54	 g = 0
11.82	17.29	6.13	7.44	11.16	5.91	24.73	7.44	1.75	6.35	 h = LT
10.88	6.32	0.35	4.91	1.75	15.79	4.91	4.56	45.96	4.56	 i = U
15.79	16.04	4.26	9.02	8.52	8.27	10.53	5.01	4.76	17.79	 j = MBP
