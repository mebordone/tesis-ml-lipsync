Timestamp:2013-08-30-05:04:12
Inventario:I3
Intervalo:6ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I3-6ms-4gramas
Num Instances:  30450
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(15): class 
0Pitch(Hz)(12): class 
0RawPitch(12): class 
0SmPitch(10): class 
0Melogram(st)(16): class 
0ZCross(12): class 
0F1(Hz)(17): class 
0F2(Hz)(20): class 
0F3(Hz)(16): class 
0F4(Hz)(15): class 
1Int(dB)(16): class 
1Pitch(Hz)(12): class 
1RawPitch(12): class 
1SmPitch(11): class 
1Melogram(st)(15): class 
1ZCross(13): class 
1F1(Hz)(17): class 
1F2(Hz)(19): class 
1F3(Hz)(15): class 
1F4(Hz)(14): class 
2Int(dB)(14): class 
2Pitch(Hz)(12): class 
2RawPitch(13): class 
2SmPitch(11): class 
2Melogram(st)(14): class 
2ZCross(13): class 
2F1(Hz)(18): class 
2F2(Hz)(20): class 
2F3(Hz)(20): class 
2F4(Hz)(14): class 
3Int(dB)(16): class 
3Pitch(Hz)(12): class 
3RawPitch(13): class 
3SmPitch(11): class 
3Melogram(st)(19): class 
3ZCross(13): class 
3F1(Hz)(20): class 
3F2(Hz)(16): class 
3F3(Hz)(19): class 
3F4(Hz)(15): class 
class(11): 
LogScore Bayes: -1860688.8129293453
LogScore BDeu: -1892504.3514358439
LogScore MDL: -1889509.0138316492
LogScore ENTROPY: -1858114.2125200364
LogScore AIC: -1864196.2125200364




=== Stratified cross-validation ===

Correctly Classified Instances       17711               58.1642 %
Incorrectly Classified Instances     12739               41.8358 %
Kappa statistic                          0.4691
K&B Relative Info Score            1449640.8397 %
K&B Information Score                37900.5227 bits      1.2447 bits/instance
Class complexity | order 0           79591.8766 bits      2.6139 bits/instance
Class complexity | scheme           422504.7229 bits     13.8754 bits/instance
Complexity improvement     (Sf)    -342912.8463 bits    -11.2615 bits/instance
Mean absolute error                      0.0765
Root mean squared error                  0.2635
Relative absolute error                 53.906  %
Root relative squared error             98.9414 %
Coverage of cases (0.95 level)          62.7849 %
Mean rel. region size (0.95 level)      11.5334 %
Total Number of Instances            30450     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,595    0,046    0,709      0,595    0,647      0,590    0,902     0,715     A
                 0,569    0,059    0,447      0,569    0,500      0,457    0,886     0,487     E
                 0,144    0,039    0,454      0,144    0,218      0,172    0,807     0,415     CGJLNQSRT
                 0,140    0,018    0,101      0,140    0,117      0,104    0,809     0,085     FV
                 0,658    0,040    0,406      0,658    0,502      0,492    0,920     0,473     I
                 0,310    0,024    0,513      0,310    0,386      0,362    0,837     0,412     O
                 0,899    0,141    0,797      0,899    0,845      0,744    0,956     0,950     0
                 0,069    0,006    0,286      0,069    0,112      0,128    0,795     0,138     BMP
                 0,522    0,025    0,325      0,522    0,401      0,395    0,926     0,497     U
                 0,000    0,009    0,000      0,000    0,000      -0,002   0,940     0,003     DZ
                 0,630    0,090    0,093      0,630    0,162      0,217    0,890     0,138     D
Weighted Avg.    0,582    0,078    0,608      0,582    0,569      0,508    0,895     0,658     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  2871   349   231    92   130   214   253    14    93    30   548 |     a = A
   180  1335   146    35   174    47   102    24    71    11   222 |     b = E
   396   572   812   178   443   168  1715    62   215   145   947 |     c = CGJLNQSRT
    26    51    36    60    28    26    88     9     9     9    88 |     d = FV
    17   160    80    10   793     1    69    11    22     8    35 |     e = I
   297   180   128    24    51   697   232    10   198    19   415 |     f = O
   123   163   241   123   138   102 10433    24    32    41   188 |     g = 0
    66    95    64    53   167    28   161    68    95    10   173 |     h = BMP
    32    54    31     8     9    64    18    12   364     8    97 |     i = U
     0     0     0     0     0     0    12     0     0     0     0 |     j = DZ
    43    29    20     9    22    13     1     4    21     1   278 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
59.5	7.23	4.79	1.91	2.69	4.44	5.24	0.29	1.93	0.62	11.36	 a = A
7.67	56.88	6.22	1.49	7.41	2.0	4.35	1.02	3.03	0.47	9.46	 b = E
7.01	10.12	14.36	3.15	7.84	2.97	30.34	1.1	3.8	2.57	16.75	 c = CGJLNQSRT
6.05	11.86	8.37	13.95	6.51	6.05	20.47	2.09	2.09	2.09	20.47	 d = FV
1.41	13.27	6.63	0.83	65.75	0.08	5.72	0.91	1.82	0.66	2.9	 e = I
13.19	8.0	5.69	1.07	2.27	30.96	10.31	0.44	8.8	0.84	18.44	 f = O
1.06	1.4	2.08	1.06	1.19	0.88	89.88	0.21	0.28	0.35	1.62	 g = 0
6.73	9.69	6.53	5.41	17.04	2.86	16.43	6.94	9.69	1.02	17.65	 h = BMP
4.59	7.75	4.45	1.15	1.29	9.18	2.58	1.72	52.22	1.15	13.92	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
9.75	6.58	4.54	2.04	4.99	2.95	0.23	0.91	4.76	0.23	63.04	 k = D
