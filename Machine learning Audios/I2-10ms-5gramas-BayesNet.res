Timestamp:2013-08-30-04:42:11
Inventario:I2
Intervalo:10ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I2-10ms-5gramas
Num Instances:  18292
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  43 4RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  44 4SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  45 4Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  47 4F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  49 4F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  50 4F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(7): class 
0RawPitch(4): class 
0SmPitch(7): class 
0Melogram(st)(9): class 
0ZCross(7): class 
0F1(Hz)(10): class 
0F2(Hz)(13): class 
0F3(Hz)(7): class 
0F4(Hz)(8): class 
1Int(dB)(10): class 
1Pitch(Hz)(6): class 
1RawPitch(4): class 
1SmPitch(6): class 
1Melogram(st)(8): class 
1ZCross(8): class 
1F1(Hz)(10): class 
1F2(Hz)(13): class 
1F3(Hz)(7): class 
1F4(Hz)(9): class 
2Int(dB)(11): class 
2Pitch(Hz)(6): class 
2RawPitch(4): class 
2SmPitch(6): class 
2Melogram(st)(8): class 
2ZCross(11): class 
2F1(Hz)(11): class 
2F2(Hz)(11): class 
2F3(Hz)(9): class 
2F4(Hz)(10): class 
3Int(dB)(10): class 
3Pitch(Hz)(5): class 
3RawPitch(6): class 
3SmPitch(6): class 
3Melogram(st)(9): class 
3ZCross(11): class 
3F1(Hz)(13): class 
3F2(Hz)(15): class 
3F3(Hz)(10): class 
3F4(Hz)(11): class 
4Int(dB)(10): class 
4Pitch(Hz)(7): class 
4RawPitch(6): class 
4SmPitch(7): class 
4Melogram(st)(8): class 
4ZCross(12): class 
4F1(Hz)(12): class 
4F2(Hz)(11): class 
4F3(Hz)(10): class 
4F4(Hz)(8): class 
class(12): 
LogScore Bayes: -1103805.1414060807
LogScore BDeu: -1125387.7747493577
LogScore MDL: -1122911.5656723098
LogScore ENTROPY: -1100127.856066998
LogScore AIC: -1104770.856066998




=== Stratified cross-validation ===

Correctly Classified Instances       10168               55.5871 %
Incorrectly Classified Instances      8124               44.4129 %
Kappa statistic                          0.4478
K&B Relative Info Score             789023.6315 %
K&B Information Score                22955.8041 bits      1.255  bits/instance
Class complexity | order 0           53202.6243 bits      2.9085 bits/instance
Class complexity | scheme           286779.3885 bits     15.6779 bits/instance
Complexity improvement     (Sf)    -233576.7642 bits    -12.7693 bits/instance
Mean absolute error                      0.0743
Root mean squared error                  0.2579
Relative absolute error                 55.2057 %
Root relative squared error             99.4652 %
Coverage of cases (0.95 level)          61.1087 %
Mean rel. region size (0.95 level)      11.2003 %
Total Number of Instances            18292     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,552    0,055    0,656      0,552    0,600      0,534    0,885     0,679     A
                 0,021    0,013    0,075      0,021    0,033      0,014    0,693     0,082     LGJKC
                 0,458    0,049    0,446      0,458    0,452      0,405    0,873     0,431     E
                 0,119    0,021    0,076      0,119    0,093      0,079    0,792     0,056     FV
                 0,610    0,036    0,418      0,610    0,496      0,480    0,913     0,443     I
                 0,251    0,016    0,566      0,251    0,347      0,345    0,828     0,404     O
                 0,237    0,051    0,337      0,237    0,278      0,219    0,809     0,297     SNRTY
                 0,893    0,117    0,819      0,893    0,854      0,764    0,950     0,919     0
                 0,032    0,005    0,176      0,032    0,054      0,062    0,767     0,096     BMP
                 0,536    0,090    0,083      0,536    0,144      0,183    0,865     0,109     DZ
                 0,452    0,014    0,440      0,452    0,446      0,432    0,911     0,471     U
                 0,244    0,046    0,185      0,244    0,211      0,173    0,835     0,152     SNRTXY
Weighted Avg.    0,556    0,068    0,567      0,556    0,549      0,492    0,881     0,589     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1624   73  217   66   73  109  143  137    8  357   13  120 |    a = A
   78   19   85   34   83   14   96  240    6  124   10  105 |    b = LGJKC
  141   46  659   16  107   16   75   67    8  164   43   96 |    c = E
   23    4   28   31   10   11   34   35    3   59    4   19 |    d = FV
   15   20   73    6  456    0   50   49    6   32    3   38 |    e = I
  195   40   98   10   38  345   73  137    4  258   96   83 |    f = O
  130   16   85   95   75    7  424  536   26  226   19  151 |    g = SNRTY
   59    7   75   84   67   30  225 6068   18   72   13   76 |    h = 0
   58    5   44   32   99   16   55   72   19   94   23   77 |    i = BMP
   38    0    8    9    7   10   13    6    1  147    7   28 |    j = DZ
   27   21   24    2    7   40   14   22    1   54  191   20 |    k = U
   86    2   82   21   69   12   56   43    8  183   12  185 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
55.24	2.48	7.38	2.24	2.48	3.71	4.86	4.66	0.27	12.14	0.44	4.08	 a = A
8.72	2.13	9.51	3.8	9.28	1.57	10.74	26.85	0.67	13.87	1.12	11.74	 b = LGJKC
9.81	3.2	45.83	1.11	7.44	1.11	5.22	4.66	0.56	11.4	2.99	6.68	 c = E
8.81	1.53	10.73	11.88	3.83	4.21	13.03	13.41	1.15	22.61	1.53	7.28	 d = FV
2.01	2.67	9.76	0.8	60.96	0.0	6.68	6.55	0.8	4.28	0.4	5.08	 e = I
14.16	2.9	7.12	0.73	2.76	25.05	5.3	9.95	0.29	18.74	6.97	6.03	 f = O
7.26	0.89	4.75	5.31	4.19	0.39	23.69	29.94	1.45	12.63	1.06	8.44	 g = SNRTY
0.87	0.1	1.1	1.24	0.99	0.44	3.31	89.31	0.26	1.06	0.19	1.12	 h = 0
9.76	0.84	7.41	5.39	16.67	2.69	9.26	12.12	3.2	15.82	3.87	12.96	 i = BMP
13.87	0.0	2.92	3.28	2.55	3.65	4.74	2.19	0.36	53.65	2.55	10.22	 j = DZ
6.38	4.96	5.67	0.47	1.65	9.46	3.31	5.2	0.24	12.77	45.15	4.73	 k = U
11.33	0.26	10.8	2.77	9.09	1.58	7.38	5.67	1.05	24.11	1.58	24.37	 l = SNRTXY
