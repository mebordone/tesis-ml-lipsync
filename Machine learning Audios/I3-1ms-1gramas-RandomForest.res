Timestamp:2013-07-22-21:59:23
Inventario:I3
Intervalo:1ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I3-1ms-1gramas
Num Instances:  183404
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.094





=== Stratified cross-validation ===

Correctly Classified Instances      168992               92.1419 %
Incorrectly Classified Instances     14412                7.8581 %
Kappa statistic                          0.8975
K&B Relative Info Score            16068482.5307 %
K&B Information Score               416045.0841 bits      2.2685 bits/instance
Class complexity | order 0          474846.2629 bits      2.5891 bits/instance
Class complexity | scheme          1210801.7593 bits      6.6018 bits/instance
Complexity improvement     (Sf)    -735955.4965 bits     -4.0128 bits/instance
Mean absolute error                      0.0232
Root mean squared error                  0.1037
Relative absolute error                 16.5294 %
Root relative squared error             39.108  %
Coverage of cases (0.95 level)          98.5753 %
Mean rel. region size (0.95 level)      16.0437 %
Total Number of Instances           183404     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,936    0,004    0,975      0,936    0,955      0,948    0,991     0,976     A
                 0,941    0,003    0,966      0,941    0,953      0,949    0,992     0,970     E
                 0,876    0,041    0,828      0,876    0,852      0,818    0,984     0,940     CGJLNQSRT
                 0,705    0,000    0,965      0,705    0,815      0,823    0,980     0,818     FV
                 0,920    0,001    0,969      0,920    0,944      0,942    0,987     0,955     I
                 0,873    0,001    0,979      0,873    0,923      0,919    0,986     0,935     O
                 0,959    0,058    0,915      0,959    0,936      0,894    0,991     0,985     0
                 0,779    0,001    0,969      0,779    0,864      0,865    0,976     0,872     BMP
                 0,961    0,000    0,981      0,961    0,971      0,970    0,992     0,979     U
                 0,000    0,000    0,000      0,000    0,000      0,000    0,774     0,007     DZ
                 0,949    0,000    0,976      0,949    0,962      0,962    0,995     0,980     D
Weighted Avg.    0,921    0,031    0,924      0,921    0,921      0,897    0,988     0,963     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 26737    29   889     4    22    27   815    24     4     0     6 |     a = A
    29 12926   444    12    10    13   267    20    10     0     9 |     b = E
   315   189 29331    18    50    76  3433    32    25     0    10 |     c = CGJLNQSRT
    12    18   627  1806    13     7    77     0     0     0     1 |     d = FV
    26    32   220    11  6518     8   245    14     9     0     0 |     e = I
    40    18   491     5    11 11556  1079    14     2     0    18 |     f = O
   164    99  2540    12    64    52 69132    26    14     0    14 |     g = 0
    56    38   694     2    24    22   439  4528     9     0     2 |     h = BMP
     8     8    77     1     7    15    30    15  3974     0     2 |     i = U
     0     0    59     0     0     0     6     0     0     0     0 |     j = DZ
    22    27    36     1     6    23    15     0     3     0  2484 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
93.63	0.1	3.11	0.01	0.08	0.09	2.85	0.08	0.01	0.0	0.02	 a = A
0.21	94.08	3.23	0.09	0.07	0.09	1.94	0.15	0.07	0.0	0.07	 b = E
0.94	0.56	87.61	0.05	0.15	0.23	10.25	0.1	0.07	0.0	0.03	 c = CGJLNQSRT
0.47	0.7	24.48	70.52	0.51	0.27	3.01	0.0	0.0	0.0	0.04	 d = FV
0.37	0.45	3.11	0.16	92.02	0.11	3.46	0.2	0.13	0.0	0.0	 e = I
0.3	0.14	3.71	0.04	0.08	87.32	8.15	0.11	0.02	0.0	0.14	 f = O
0.23	0.14	3.52	0.02	0.09	0.07	95.86	0.04	0.02	0.0	0.02	 g = 0
0.96	0.65	11.94	0.03	0.41	0.38	7.55	77.88	0.15	0.0	0.03	 h = BMP
0.19	0.19	1.86	0.02	0.17	0.36	0.73	0.36	96.06	0.0	0.05	 i = U
0.0	0.0	90.77	0.0	0.0	0.0	9.23	0.0	0.0	0.0	0.0	 j = DZ
0.84	1.03	1.38	0.04	0.23	0.88	0.57	0.0	0.11	0.0	94.92	 k = D
