Timestamp:2013-07-22-21:58:20
Inventario:I2
Intervalo:20ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I2-20ms-1gramas
Num Instances:  9129
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.3741





=== Stratified cross-validation ===

Correctly Classified Instances        6016               65.8999 %
Incorrectly Classified Instances      3113               34.1001 %
Kappa statistic                          0.5782
K&B Relative Info Score             515333.2397 %
K&B Information Score                15196.8367 bits      1.6647 bits/instance
Class complexity | order 0           26902.5371 bits      2.9469 bits/instance
Class complexity | scheme          1196240.1503 bits    131.0374 bits/instance
Complexity improvement     (Sf)    -1169337.6132 bits   -128.0904 bits/instance
Mean absolute error                      0.0699
Root mean squared error                  0.1957
Relative absolute error                 51.3665 %
Root relative squared error             75.0284 %
Coverage of cases (0.95 level)          87.6547 %
Mean rel. region size (0.95 level)      24.5874 %
Total Number of Instances             9129     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,766    0,081    0,653      0,766    0,705      0,644    0,912     0,781     A
                 0,227    0,030    0,282      0,227    0,251      0,218    0,748     0,173     LGJKC
                 0,602    0,049    0,521      0,602    0,559      0,518    0,894     0,536     E
                 0,116    0,005    0,250      0,116    0,158      0,162    0,676     0,069     FV
                 0,579    0,020    0,558      0,579    0,568      0,549    0,908     0,572     I
                 0,549    0,044    0,515      0,549    0,532      0,491    0,863     0,540     O
                 0,429    0,062    0,437      0,429    0,433      0,370    0,820     0,405     SNRTY
                 0,918    0,061    0,893      0,918    0,905      0,852    0,971     0,949     0
                 0,241    0,014    0,375      0,241    0,293      0,282    0,765     0,211     BMP
                 0,065    0,004    0,196      0,065    0,098      0,105    0,779     0,105     DZ
                 0,500    0,007    0,649      0,500    0,565      0,560    0,878     0,557     U
                 0,333    0,018    0,454      0,333    0,385      0,366    0,831     0,310     SNRTXY
Weighted Avg.    0,659    0,052    0,642      0,659    0,647      0,602    0,895     0,660     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1153   29   75    7   14   67   71   52   15    3    5   14 |    a = A
   77  102   46    7   23   35   78   50    8    1    9   14 |    b = LGJKC
   99   42  449    5   32   34   34   13   11    1    6   20 |    c = E
   26    7   19   16    1   14   31    7    8    2    1    6 |    d = FV
   18   14   68    0  223    7   23   12    8    0    1   11 |    e = I
   97   20   42    7    5  391   39   47   12   10   10   32 |    f = O
  134   72   54   13   35   44  392  129   12    7    2   20 |    g = SNRTY
   45   26   12    2   14   30  116 2971   13    2    1    5 |    h = 0
   28   14   19    3   26   33   45   28   72    5   12   14 |    i = BMP
   27    6   22    1    2   36   12    5    6    9    3    9 |    j = DZ
   13    9    9    1    6   36   13    0   10    2  109   10 |    k = U
   50   21   46    2   19   32   44   14   17    4    9  129 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
76.61	1.93	4.98	0.47	0.93	4.45	4.72	3.46	1.0	0.2	0.33	0.93	 a = A
17.11	22.67	10.22	1.56	5.11	7.78	17.33	11.11	1.78	0.22	2.0	3.11	 b = LGJKC
13.27	5.63	60.19	0.67	4.29	4.56	4.56	1.74	1.47	0.13	0.8	2.68	 c = E
18.84	5.07	13.77	11.59	0.72	10.14	22.46	5.07	5.8	1.45	0.72	4.35	 d = FV
4.68	3.64	17.66	0.0	57.92	1.82	5.97	3.12	2.08	0.0	0.26	2.86	 e = I
13.62	2.81	5.9	0.98	0.7	54.92	5.48	6.6	1.69	1.4	1.4	4.49	 f = O
14.66	7.88	5.91	1.42	3.83	4.81	42.89	14.11	1.31	0.77	0.22	2.19	 g = SNRTY
1.39	0.8	0.37	0.06	0.43	0.93	3.58	91.78	0.4	0.06	0.03	0.15	 h = 0
9.36	4.68	6.35	1.0	8.7	11.04	15.05	9.36	24.08	1.67	4.01	4.68	 i = BMP
19.57	4.35	15.94	0.72	1.45	26.09	8.7	3.62	4.35	6.52	2.17	6.52	 j = DZ
5.96	4.13	4.13	0.46	2.75	16.51	5.96	0.0	4.59	0.92	50.0	4.59	 k = U
12.92	5.43	11.89	0.52	4.91	8.27	11.37	3.62	4.39	1.03	2.33	33.33	 l = SNRTXY
