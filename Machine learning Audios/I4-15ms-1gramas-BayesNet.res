Timestamp:2013-08-30-05:29:12
Inventario:I4
Intervalo:15ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I4-15ms-1gramas
Num Instances:  12189
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(8): class 
0ZCross(8): class 
0F1(Hz)(11): class 
0F2(Hz)(10): class 
0F3(Hz)(9): class 
0F4(Hz)(5): class 
class(10): 
LogScore Bayes: -150104.04962743126
LogScore BDeu: -152767.05843358694
LogScore MDL: -152540.73146477854
LogScore ENTROPY: -149581.82451620427
LogScore AIC: -150210.82451620427




=== Stratified cross-validation ===

Correctly Classified Instances        7267               59.6193 %
Incorrectly Classified Instances      4922               40.3807 %
Kappa statistic                          0.4726
K&B Relative Info Score             574610.7494 %
K&B Information Score                14940.8962 bits      1.2258 bits/instance
Class complexity | order 0           31675.272  bits      2.5987 bits/instance
Class complexity | scheme            44421.1522 bits      3.6444 bits/instance
Complexity improvement     (Sf)     -12745.8802 bits     -1.0457 bits/instance
Mean absolute error                      0.087 
Root mean squared error                  0.2459
Relative absolute error                 55.4039 %
Root relative squared error             87.7611 %
Coverage of cases (0.95 level)          80.4086 %
Mean rel. region size (0.95 level)      26.0546 %
Total Number of Instances            12189     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,111    0,009    0,066      0,111    0,083      0,079    0,832     0,034     C
                 0,611    0,101    0,345      0,611    0,441      0,397    0,883     0,442     E
                 0,006    0,002    0,036      0,006    0,010      0,009    0,761     0,042     FV
                 0,674    0,141    0,551      0,674    0,606      0,496    0,875     0,695     AI
                 0,167    0,051    0,386      0,167    0,233      0,167    0,780     0,356     CDGKNRSYZ
                 0,377    0,037    0,462      0,377    0,415      0,373    0,833     0,419     O
                 0,936    0,137    0,796      0,936    0,860      0,777    0,974     0,964     0
                 0,004    0,001    0,133      0,004    0,008      0,018    0,756     0,095     LT
                 0,439    0,013    0,451      0,439    0,445      0,432    0,927     0,493     U
                 0,093    0,010    0,237      0,093    0,133      0,131    0,783     0,122     MBP
Weighted Avg.    0,596    0,098    0,551      0,596    0,556      0,486    0,885     0,637     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
    8   26    0   29    4    2    3    0    0    0 |    a = C
   26  593    3  214   50   25   36    0   12   12 |    b = E
    2   50    1   33   29   20   34    0    3    5 |    c = FV
   29  384    5 1680  152   77  116    2    8   39 |    d = AI
   29  296    7  497  330  140  598    5   43   33 |    e = CDGKNRSYZ
    8  100    2  281   46  354   95    0   49    5 |    f = O
    5   47    4   89  101   21 4137    3    4    7 |    g = 0
    1  113    3   88   82   31  120    2    7   10 |    h = LT
    4   26    0   48    8   59    5    2  125    8 |    i = U
    9   86    3   91   52   38   56    1   26   37 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
11.11	36.11	0.0	40.28	5.56	2.78	4.17	0.0	0.0	0.0	 a = C
2.68	61.07	0.31	22.04	5.15	2.57	3.71	0.0	1.24	1.24	 b = E
1.13	28.25	0.56	18.64	16.38	11.3	19.21	0.0	1.69	2.82	 c = FV
1.16	15.41	0.2	67.42	6.1	3.09	4.65	0.08	0.32	1.57	 d = AI
1.47	14.96	0.35	25.13	16.68	7.08	30.23	0.25	2.17	1.67	 e = CDGKNRSYZ
0.85	10.64	0.21	29.89	4.89	37.66	10.11	0.0	5.21	0.53	 f = O
0.11	1.06	0.09	2.01	2.29	0.48	93.64	0.07	0.09	0.16	 g = 0
0.22	24.73	0.66	19.26	17.94	6.78	26.26	0.44	1.53	2.19	 h = LT
1.4	9.12	0.0	16.84	2.81	20.7	1.75	0.7	43.86	2.81	 i = U
2.26	21.55	0.75	22.81	13.03	9.52	14.04	0.25	6.52	9.27	 j = MBP
