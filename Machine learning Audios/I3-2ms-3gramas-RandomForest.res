Timestamp:2013-07-22-22:18:48
Inventario:I3
Intervalo:2ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I3-2ms-3gramas
Num Instances:  91701
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1416





=== Stratified cross-validation ===

Correctly Classified Instances       81762               89.1615 %
Incorrectly Classified Instances      9939               10.8385 %
Kappa statistic                          0.8596
K&B Relative Info Score            7675900.9954 %
K&B Information Score               199165.5594 bits      2.1719 bits/instance
Class complexity | order 0          237913.6665 bits      2.5945 bits/instance
Class complexity | scheme          3135422.7538 bits     34.1918 bits/instance
Complexity improvement     (Sf)    -2897509.0873 bits    -31.5974 bits/instance
Mean absolute error                      0.0315
Root mean squared error                  0.1237
Relative absolute error                 22.3632 %
Root relative squared error             46.6136 %
Coverage of cases (0.95 level)          96.8146 %
Mean rel. region size (0.95 level)      16.3304 %
Total Number of Instances            91701     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,923    0,014    0,923      0,923    0,923      0,908    0,977     0,953     A
                 0,911    0,007    0,910      0,911    0,910      0,903    0,979     0,943     E
                 0,842    0,050    0,791      0,842    0,815      0,773    0,961     0,875     CGJLNQSRT
                 0,656    0,002    0,815      0,656    0,727      0,728    0,922     0,719     FV
                 0,891    0,003    0,923      0,891    0,907      0,903    0,972     0,925     I
                 0,848    0,007    0,902      0,848    0,874      0,865    0,953     0,891     O
                 0,927    0,053    0,918      0,927    0,923      0,873    0,981     0,963     0
                 0,738    0,003    0,879      0,738    0,802      0,800    0,934     0,806     BMP
                 0,922    0,000    0,978      0,922    0,949      0,949    0,988     0,964     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,529     0,001     DZ
                 0,883    0,001    0,943      0,883    0,912      0,911    0,994     0,958     D
Weighted Avg.    0,892    0,034    0,893      0,892    0,892      0,861    0,972     0,928     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 13227    40   558     7    19    47   389    30     5     1    10 |     a = A
    76  6294   311    16    23    22   123    26     5     0    14 |     b = E
   441   229 14135    91    58   161  1544    95    15    15     9 |     c = CGJLNQSRT
    31    25   273   844    14    19    76     2     2     0     0 |     d = FV
    29    47   181     8  3172     4   104    14     2     0     0 |     e = I
    99    40   306    15     9  5635   493    22     3     0    24 |     f = O
   321   149  1629    42    97   260 33227    88     3     4    11 |     g = 0
    70    53   349    11    33    45   197  2151     6     0     0 |     h = BMP
     9    15    70     0     8    19    23    16  1918     0     2 |     i = U
     0     0    23     1     0     0     7     2     0     0     0 |     j = DZ
    32    26    39     0     2    35    17     1     2     0  1159 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
92.28	0.28	3.89	0.05	0.13	0.33	2.71	0.21	0.03	0.01	0.07	 a = A
1.1	91.09	4.5	0.23	0.33	0.32	1.78	0.38	0.07	0.0	0.2	 b = E
2.63	1.36	84.17	0.54	0.35	0.96	9.19	0.57	0.09	0.09	0.05	 c = CGJLNQSRT
2.41	1.94	21.23	65.63	1.09	1.48	5.91	0.16	0.16	0.0	0.0	 d = FV
0.81	1.32	5.08	0.22	89.08	0.11	2.92	0.39	0.06	0.0	0.0	 e = I
1.49	0.6	4.6	0.23	0.14	84.79	7.42	0.33	0.05	0.0	0.36	 f = O
0.9	0.42	4.55	0.12	0.27	0.73	92.73	0.25	0.01	0.01	0.03	 g = 0
2.4	1.82	11.97	0.38	1.13	1.54	6.76	73.79	0.21	0.0	0.0	 h = BMP
0.43	0.72	3.37	0.0	0.38	0.91	1.11	0.77	92.21	0.0	0.1	 i = U
0.0	0.0	69.7	3.03	0.0	0.0	21.21	6.06	0.0	0.0	0.0	 j = DZ
2.44	1.98	2.97	0.0	0.15	2.67	1.29	0.08	0.15	0.0	88.27	 k = D
