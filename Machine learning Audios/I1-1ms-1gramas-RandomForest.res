Timestamp:2013-07-22-20:47:57
Inventario:I1
Intervalo:1ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I1-1ms-1gramas
Num Instances:  183404
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1085





=== Stratified cross-validation ===

Correctly Classified Instances      166332               90.6916 %
Incorrectly Classified Instances     17072                9.3084 %
Kappa statistic                          0.8807
K&B Relative Info Score            15706481.4769 %
K&B Information Score               452417.6016 bits      2.4668 bits/instance
Class complexity | order 0          528260.7083 bits      2.8803 bits/instance
Class complexity | scheme          1770655.0201 bits      9.6544 bits/instance
Complexity improvement     (Sf)    -1242394.3118 bits     -6.7741 bits/instance
Mean absolute error                      0.0224
Root mean squared error                  0.1027
Relative absolute error                 18.315  %
Root relative squared error             41.5375 %
Coverage of cases (0.95 level)          98.2051 %
Mean rel. region size (0.95 level)      15.7597 %
Total Number of Instances           183404     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,937    0,005    0,974      0,937    0,955      0,948    0,991     0,976     a
                 0,787    0,001    0,955      0,787    0,863      0,863    0,977     0,873     b
                 0,942    0,003    0,964      0,942    0,952      0,949    0,992     0,971     e
                 0,931    0,000    0,971      0,931    0,951      0,950    0,993     0,966     d
                 0,709    0,000    0,958      0,709    0,815      0,822    0,981     0,816     f
                 0,923    0,001    0,966      0,923    0,944      0,942    0,987     0,955     i
                 0,387    0,002    0,757      0,387    0,512      0,535    0,924     0,496     k
                 0,800    0,000    0,956      0,800    0,871      0,873    0,966     0,863     j
                 0,813    0,006    0,936      0,813    0,870      0,860    0,980     0,916     l
                 0,870    0,002    0,977      0,870    0,920      0,916    0,985     0,935     o
                 0,977    0,086    0,880      0,977    0,926      0,878    0,990     0,985     0
                 0,772    0,022    0,680      0,772    0,723      0,707    0,981     0,832     s
                 0,960    0,000    0,980      0,960    0,970      0,969    0,992     0,980     u
Weighted Avg.    0,907    0,037    0,911      0,907    0,905      0,883    0,986     0,945     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 26770    26    30     7     7    14    38     6   149    27  1172   304     7 |     a = a
    52  4575    29     1     3    31    19     3    66    21   774   231     9 |     b = b
    35    29 12938     8    13    15    27     6    99    13   451    98     8 |     c = e
    25     0    25  2497     0     4     0     1    21    20    36    51     2 |     d = d
    16     2    29     1  1816     9     7     3    29     9   218   422     0 |     e = f
    25    27    29     2    10  6540     7     3    39     5   318    72     6 |     f = i
    50    17    31     0     6     8  1377    15    89    18  1218   714    12 |     g = k
    18     3    21     0     2     7    17  1649    21     2   130   187     5 |     h = j
   208    27   126     9     5    48    67     5 14050    64  1994   678    11 |     i = l
    60    25    18    26     7     7    17     2    96 11512  1302   158     4 |     j = o
   155    34   113    18    12    63    75    15   181    60 70479   899    13 |     k = 0
    52    15    32     0    12    16   156    11   151    20  1944  8158     3 |     l = s
    14    11     7     2     2     9    12     5    25    14    47    18  3971 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
93.74	0.09	0.11	0.02	0.02	0.05	0.13	0.02	0.52	0.09	4.1	1.06	0.02	 a = a
0.89	78.69	0.5	0.02	0.05	0.53	0.33	0.05	1.14	0.36	13.31	3.97	0.15	 b = b
0.25	0.21	94.16	0.06	0.09	0.11	0.2	0.04	0.72	0.09	3.28	0.71	0.06	 c = e
0.93	0.0	0.93	93.1	0.0	0.15	0.0	0.04	0.78	0.75	1.34	1.9	0.07	 d = d
0.62	0.08	1.13	0.04	70.91	0.35	0.27	0.12	1.13	0.35	8.51	16.48	0.0	 e = f
0.35	0.38	0.41	0.03	0.14	92.33	0.1	0.04	0.55	0.07	4.49	1.02	0.08	 f = i
1.41	0.48	0.87	0.0	0.17	0.23	38.73	0.42	2.5	0.51	34.26	20.08	0.34	 g = k
0.87	0.15	1.02	0.0	0.1	0.34	0.82	79.97	1.02	0.1	6.3	9.07	0.24	 h = j
1.2	0.16	0.73	0.05	0.03	0.28	0.39	0.03	81.25	0.37	11.53	3.92	0.06	 i = l
0.45	0.19	0.14	0.2	0.05	0.05	0.13	0.02	0.73	86.99	9.84	1.19	0.03	 j = o
0.21	0.05	0.16	0.02	0.02	0.09	0.1	0.02	0.25	0.08	97.73	1.25	0.02	 k = 0
0.49	0.14	0.3	0.0	0.11	0.15	1.48	0.1	1.43	0.19	18.39	77.18	0.03	 l = s
0.34	0.27	0.17	0.05	0.05	0.22	0.29	0.12	0.6	0.34	1.14	0.44	95.99	 m = u
