Timestamp:2013-08-30-04:42:00
Inventario:I2
Intervalo:10ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I2-10ms-4gramas
Num Instances:  18293
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(10): class 
0Pitch(Hz)(6): class 
0RawPitch(4): class 
0SmPitch(6): class 
0Melogram(st)(8): class 
0ZCross(8): class 
0F1(Hz)(10): class 
0F2(Hz)(13): class 
0F3(Hz)(7): class 
0F4(Hz)(9): class 
1Int(dB)(11): class 
1Pitch(Hz)(6): class 
1RawPitch(4): class 
1SmPitch(6): class 
1Melogram(st)(8): class 
1ZCross(11): class 
1F1(Hz)(11): class 
1F2(Hz)(11): class 
1F3(Hz)(9): class 
1F4(Hz)(10): class 
2Int(dB)(10): class 
2Pitch(Hz)(5): class 
2RawPitch(6): class 
2SmPitch(6): class 
2Melogram(st)(9): class 
2ZCross(11): class 
2F1(Hz)(13): class 
2F2(Hz)(15): class 
2F3(Hz)(10): class 
2F4(Hz)(11): class 
3Int(dB)(10): class 
3Pitch(Hz)(7): class 
3RawPitch(6): class 
3SmPitch(7): class 
3Melogram(st)(8): class 
3ZCross(12): class 
3F1(Hz)(12): class 
3F2(Hz)(11): class 
3F3(Hz)(10): class 
3F4(Hz)(8): class 
class(12): 
LogScore Bayes: -886003.5796220186
LogScore BDeu: -903710.8165264179
LogScore MDL: -901664.8262454122
LogScore ENTROPY: -883061.8703485128
LogScore AIC: -886852.8703485128




=== Stratified cross-validation ===

Correctly Classified Instances       10376               56.7212 %
Incorrectly Classified Instances      7917               43.2788 %
Kappa statistic                          0.46  
K&B Relative Info Score             817522.1827 %
K&B Information Score                23785.6696 bits      1.3003 bits/instance
Class complexity | order 0           53204.0531 bits      2.9084 bits/instance
Class complexity | scheme           235054.5949 bits     12.8494 bits/instance
Complexity improvement     (Sf)    -181850.5418 bits     -9.941  bits/instance
Mean absolute error                      0.0725
Root mean squared error                  0.2518
Relative absolute error                 53.8672 %
Root relative squared error             97.1102 %
Coverage of cases (0.95 level)          63.2373 %
Mean rel. region size (0.95 level)      11.8493 %
Total Number of Instances            18293     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,585    0,057    0,661      0,585    0,621      0,555    0,892     0,699     A
                 0,023    0,013    0,088      0,023    0,037      0,021    0,704     0,088     LGJKC
                 0,509    0,054    0,444      0,509    0,474      0,427    0,879     0,457     E
                 0,115    0,022    0,070      0,115    0,087      0,073    0,783     0,051     FV
                 0,619    0,038    0,411      0,619    0,494      0,479    0,916     0,453     I
                 0,276    0,016    0,591      0,276    0,376      0,373    0,831     0,417     O
                 0,218    0,047    0,333      0,218    0,264      0,207    0,826     0,309     SNRTY
                 0,900    0,120    0,816      0,900    0,856      0,767    0,959     0,947     0
                 0,019    0,004    0,138      0,019    0,033      0,039    0,768     0,096     BMP
                 0,529    0,077    0,095      0,529    0,161      0,199    0,869     0,118     DZ
                 0,449    0,014    0,424      0,449    0,436      0,423    0,914     0,479     U
                 0,231    0,040    0,199      0,231    0,213      0,177    0,837     0,160     SNRTXY
Weighted Avg.    0,567    0,070    0,567      0,567    0,555      0,499    0,889     0,608     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1721   64  231   80   70   89  143  129    4  305   10   94 |    a = A
   86   21  100   37   92   17   98  238    4  109   15   77 |    b = LGJKC
  141   42  732   24   98   19   70   60    6  131   31   84 |    c = E
   24    2   34   30   15   13   31   40    2   49    2   19 |    d = FV
   15   20   92    6  463    0   51   43    6   16    7   29 |    e = I
  207   38   95    8   37  380   74  128    4  226  104   76 |    f = O
  129   16   96   92   81   16  391  588   18  204   19  140 |    g = SNRTY
   65    8   79   81   66   29  188 6117   14   59   12   77 |    h = 0
   59    8   62   31  109   17   46   82   11   80   26   63 |    i = BMP
   39    0   12   10    9    7   12    6    1  145    9   24 |    j = DZ
   26   19   27    2    8   41   16   16    3   52  190   23 |    k = U
   90    2   90   25   79   15   53   49    7  151   23  175 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
58.54	2.18	7.86	2.72	2.38	3.03	4.86	4.39	0.14	10.37	0.34	3.2	 a = A
9.62	2.35	11.19	4.14	10.29	1.9	10.96	26.62	0.45	12.19	1.68	8.61	 b = LGJKC
9.81	2.92	50.9	1.67	6.82	1.32	4.87	4.17	0.42	9.11	2.16	5.84	 c = E
9.2	0.77	13.03	11.49	5.75	4.98	11.88	15.33	0.77	18.77	0.77	7.28	 d = FV
2.01	2.67	12.3	0.8	61.9	0.0	6.82	5.75	0.8	2.14	0.94	3.88	 e = I
15.03	2.76	6.9	0.58	2.69	27.6	5.37	9.3	0.29	16.41	7.55	5.52	 f = O
7.21	0.89	5.36	5.14	4.53	0.89	21.84	32.85	1.01	11.4	1.06	7.82	 g = SNRTY
0.96	0.12	1.16	1.19	0.97	0.43	2.77	90.02	0.21	0.87	0.18	1.13	 h = 0
9.93	1.35	10.44	5.22	18.35	2.86	7.74	13.8	1.85	13.47	4.38	10.61	 i = BMP
14.23	0.0	4.38	3.65	3.28	2.55	4.38	2.19	0.36	52.92	3.28	8.76	 j = DZ
6.15	4.49	6.38	0.47	1.89	9.69	3.78	3.78	0.71	12.29	44.92	5.44	 k = U
11.86	0.26	11.86	3.29	10.41	1.98	6.98	6.46	0.92	19.89	3.03	23.06	 l = SNRTXY
