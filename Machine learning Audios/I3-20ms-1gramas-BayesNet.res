Timestamp:2013-08-30-05:06:13
Inventario:I3
Intervalo:20ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I3-20ms-1gramas
Num Instances:  9129
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(8): class 
0F1(Hz)(9): class 
0F2(Hz)(9): class 
0F3(Hz)(5): class 
0F4(Hz)(6): class 
class(11): 
LogScore Bayes: -102212.53746759995
LogScore BDeu: -104714.88841216757
LogScore MDL: -104596.37231441974
LogScore ENTROPY: -101842.37045997308
LogScore AIC: -102446.37045997308




=== Stratified cross-validation ===

Correctly Classified Instances        5516               60.4228 %
Incorrectly Classified Instances      3613               39.5772 %
Kappa statistic                          0.4968
K&B Relative Info Score             461730.0222 %
K&B Information Score                12330.9406 bits      1.3507 bits/instance
Class complexity | order 0           24352.2535 bits      2.6676 bits/instance
Class complexity | scheme            32515.8966 bits      3.5618 bits/instance
Complexity improvement     (Sf)      -8163.6431 bits     -0.8943 bits/instance
Mean absolute error                      0.0781
Root mean squared error                  0.2326
Relative absolute error                 54.1334 %
Root relative squared error             86.5803 %
Coverage of cases (0.95 level)          80.2936 %
Mean rel. region size (0.95 level)      24.5641 %
Total Number of Instances             9129     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,720    0,096    0,596      0,720    0,652      0,579    0,900     0,717     A
                 0,631    0,089    0,388      0,631    0,481      0,438    0,892     0,486     E
                 0,192    0,052    0,469      0,192    0,273      0,206    0,786     0,420     CGJLNQSRT
                 0,007    0,002    0,059      0,007    0,013      0,015    0,759     0,040     FV
                 0,584    0,039    0,400      0,584    0,475      0,456    0,914     0,481     I
                 0,337    0,033    0,462      0,337    0,390      0,352    0,829     0,403     O
                 0,936    0,125    0,804      0,936    0,865      0,788    0,975     0,963     0
                 0,013    0,005    0,083      0,013    0,023      0,021    0,770     0,106     BMP
                 0,463    0,016    0,419      0,463    0,440      0,426    0,910     0,441     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,936     0,005     DZ
                 0,173    0,022    0,104      0,173    0,130      0,118    0,845     0,077     D
Weighted Avg.    0,604    0,083    0,573      0,604    0,570      0,510    0,892     0,647     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1083  146   96    4   24   66   65    0    7    0   14 |    a = A
   96  471   48    0   57   17   25    2   20    0   10 |    b = E
  295  230  337    4  162   93  486   23   44    0   77 |    c = CGJLNQSRT
   21   35   25    1    7    9   21    2    2    1   14 |    d = FV
    7   93   32    0  225    1   18    4    3    0    2 |    e = I
  179   89   40    0   15  240   73    2   37    0   37 |    f = O
   39   28   87    7   19   15 3031    1    2    1    7 |    g = 0
   43   69   32    1   45   19   42    4   20    0   24 |    h = BMP
   21   23   10    0    2   41    3    4  101    0   13 |    i = U
    0    0    0    0    0    0    5    0    0    0    0 |    j = DZ
   34   29   11    0    6   18    1    6    5    0   23 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
71.96	9.7	6.38	0.27	1.59	4.39	4.32	0.0	0.47	0.0	0.93	 a = A
12.87	63.14	6.43	0.0	7.64	2.28	3.35	0.27	2.68	0.0	1.34	 b = E
16.85	13.14	19.25	0.23	9.25	5.31	27.76	1.31	2.51	0.0	4.4	 c = CGJLNQSRT
15.22	25.36	18.12	0.72	5.07	6.52	15.22	1.45	1.45	0.72	10.14	 d = FV
1.82	24.16	8.31	0.0	58.44	0.26	4.68	1.04	0.78	0.0	0.52	 e = I
25.14	12.5	5.62	0.0	2.11	33.71	10.25	0.28	5.2	0.0	5.2	 f = O
1.2	0.86	2.69	0.22	0.59	0.46	93.64	0.03	0.06	0.03	0.22	 g = 0
14.38	23.08	10.7	0.33	15.05	6.35	14.05	1.34	6.69	0.0	8.03	 h = BMP
9.63	10.55	4.59	0.0	0.92	18.81	1.38	1.83	46.33	0.0	5.96	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
25.56	21.8	8.27	0.0	4.51	13.53	0.75	4.51	3.76	0.0	17.29	 k = D
