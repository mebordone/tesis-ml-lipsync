Timestamp:2013-07-22-21:45:32
Inventario:I2
Intervalo:2ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I2-2ms-4gramas
Num Instances:  91700
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1558





=== Stratified cross-validation ===

Correctly Classified Instances       80985               88.3152 %
Incorrectly Classified Instances     10715               11.6848 %
Kappa statistic                          0.8523
K&B Relative Info Score            7592119.9245 %
K&B Information Score               217391.6411 bits      2.3707 bits/instance
Class complexity | order 0          262552.5757 bits      2.8632 bits/instance
Class complexity | scheme          3731215.7643 bits     40.6894 bits/instance
Complexity improvement     (Sf)    -3468663.1886 bits    -37.8262 bits/instance
Mean absolute error                      0.0318
Root mean squared error                  0.1224
Relative absolute error                 23.9899 %
Root relative squared error             47.5439 %
Coverage of cases (0.95 level)          96.2432 %
Mean rel. region size (0.95 level)      16.493  %
Total Number of Instances            91700     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,931    0,015    0,919      0,931    0,925      0,911    0,978     0,956     A
                 0,696    0,013    0,737      0,696    0,716      0,702    0,923     0,746     LGJKC
                 0,919    0,008    0,906      0,919    0,912      0,905    0,979     0,944     E
                 0,700    0,003    0,783      0,700    0,739      0,737    0,926     0,739     FV
                 0,896    0,003    0,918      0,896    0,907      0,903    0,974     0,928     I
                 0,855    0,007    0,906      0,855    0,880      0,871    0,956     0,897     O
                 0,744    0,034    0,697      0,744    0,720      0,690    0,949     0,776     SNRTY
                 0,937    0,057    0,913      0,937    0,925      0,876    0,983     0,966     0
                 0,745    0,003    0,877      0,745    0,806      0,803    0,934     0,812     BMP
                 0,874    0,001    0,945      0,874    0,908      0,908    0,986     0,938     DZ
                 0,923    0,001    0,969      0,923    0,945      0,944    0,988     0,964     U
                 0,845    0,002    0,940      0,845    0,890      0,887    0,961     0,898     SNRTXY
Weighted Avg.    0,883    0,030    0,884      0,883    0,883      0,857    0,970     0,916     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 13346   109    54    14    17    48   273   399    34     5     8    26 |     a = A
   159  3084    70    22    16    48   472   489    28     1    12    27 |     b = LGJKC
    77    75  6349    14    22    29   140   138    29    12     6    19 |     c = E
    29    32    23   900    14    21   200    55     6     1     1     4 |     d = FV
    34    32    54    15  3189    11    97   107    10     1     4     7 |     e = I
    85    68    38    16    14  5685   144   530    23    17     2    24 |     f = O
   288   317   111    94    61   103  6448  1116    70     8     5    42 |     g = SNRTY
   325   316   178    47    88   216   948 33585    75    12     9    31 |     h = 0
    72    67    45    14    35    45   232   217  2173     0     8     7 |     i = BMP
    29     3    29     4     5    31    30    24     4  1176     1    10 |     j = DZ
    18    23    17     2    11    11    30    27    15     2  1920     4 |     k = U
    67    61    43     8     1    29   237   101    10     9     6  3130 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
93.11	0.76	0.38	0.1	0.12	0.33	1.9	2.78	0.24	0.03	0.06	0.18	 a = A
3.59	69.65	1.58	0.5	0.36	1.08	10.66	11.04	0.63	0.02	0.27	0.61	 b = LGJKC
1.11	1.09	91.88	0.2	0.32	0.42	2.03	2.0	0.42	0.17	0.09	0.27	 c = E
2.26	2.49	1.79	69.98	1.09	1.63	15.55	4.28	0.47	0.08	0.08	0.31	 d = FV
0.95	0.9	1.52	0.42	89.55	0.31	2.72	3.0	0.28	0.03	0.11	0.2	 e = I
1.28	1.02	0.57	0.24	0.21	85.54	2.17	7.97	0.35	0.26	0.03	0.36	 f = O
3.32	3.66	1.28	1.09	0.7	1.19	74.43	12.88	0.81	0.09	0.06	0.48	 g = SNRTY
0.91	0.88	0.5	0.13	0.25	0.6	2.65	93.73	0.21	0.03	0.03	0.09	 h = 0
2.47	2.3	1.54	0.48	1.2	1.54	7.96	7.44	74.55	0.0	0.27	0.24	 i = BMP
2.15	0.22	2.15	0.3	0.37	2.3	2.23	1.78	0.3	87.37	0.07	0.74	 j = DZ
0.87	1.11	0.82	0.1	0.53	0.53	1.44	1.3	0.72	0.1	92.31	0.19	 k = U
1.81	1.65	1.16	0.22	0.03	0.78	6.4	2.73	0.27	0.24	0.16	84.55	 l = SNRTXY
