Timestamp:2013-08-30-04:48:15
Inventario:I3
Intervalo:1ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I3-1ms-4gramas
Num Instances:  183401
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(34): class 
0Pitch(Hz)(92): class 
0RawPitch(78): class 
0SmPitch(82): class 
0Melogram(st)(83): class 
0ZCross(20): class 
0F1(Hz)(2622): class 
0F2(Hz)(3346): class 
0F3(Hz)(3507): class 
0F4(Hz)(3456): class 
1Int(dB)(37): class 
1Pitch(Hz)(93): class 
1RawPitch(76): class 
1SmPitch(80): class 
1Melogram(st)(82): class 
1ZCross(20): class 
1F1(Hz)(2627): class 
1F2(Hz)(3312): class 
1F3(Hz)(3520): class 
1F4(Hz)(3447): class 
2Int(dB)(35): class 
2Pitch(Hz)(96): class 
2RawPitch(78): class 
2SmPitch(93): class 
2Melogram(st)(83): class 
2ZCross(20): class 
2F1(Hz)(2607): class 
2F2(Hz)(3303): class 
2F3(Hz)(3508): class 
2F4(Hz)(3434): class 
3Int(dB)(32): class 
3Pitch(Hz)(96): class 
3RawPitch(78): class 
3SmPitch(90): class 
3Melogram(st)(87): class 
3ZCross(20): class 
3F1(Hz)(2598): class 
3F2(Hz)(3309): class 
3F3(Hz)(3521): class 
3F4(Hz)(3448): class 
class(11): 
LogScore Bayes: -2.2187220296236664E7
LogScore BDeu: -2.9748715845175363E7
LogScore MDL: -2.7848826823083904E7
LogScore ENTROPY: -2.4308620040676977E7
LogScore AIC: -2.4892840040676977E7




=== Stratified cross-validation ===

Correctly Classified Instances      148218               80.8164 %
Incorrectly Classified Instances     35183               19.1836 %
Kappa statistic                          0.7498
K&B Relative Info Score            14262141.9233 %
K&B Information Score               369277.3004 bits      2.0135 bits/instance
Class complexity | order 0          474842.223  bits      2.5891 bits/instance
Class complexity | scheme          1700790.5725 bits      9.2736 bits/instance
Complexity improvement     (Sf)    -1225948.3494 bits     -6.6845 bits/instance
Mean absolute error                      0.0349
Root mean squared error                  0.1836
Relative absolute error                 24.8231 %
Root relative squared error             69.2412 %
Coverage of cases (0.95 level)          82.0535 %
Mean rel. region size (0.95 level)       9.4671 %
Total Number of Instances           183401     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,872    0,011    0,934      0,872    0,902      0,885    0,967     0,937     A
                 0,884    0,012    0,861      0,884    0,873      0,862    0,970     0,904     E
                 0,496    0,017    0,870      0,496    0,632      0,607    0,963     0,846     CGJLNQSRT
                 0,691    0,008    0,548      0,691    0,611      0,609    0,932     0,667     FV
                 0,876    0,008    0,811      0,876    0,842      0,837    0,965     0,880     I
                 0,820    0,008    0,886      0,820    0,852      0,842    0,936     0,867     O
                 0,902    0,158    0,787      0,902    0,841      0,731    0,969     0,951     0
                 0,738    0,006    0,790      0,738    0,763      0,756    0,930     0,766     BMP
                 0,938    0,007    0,748      0,938    0,833      0,834    0,982     0,926     U
                 0,000    0,009    0,000      0,000    0,000      -0,002   0,820     0,001     DZ
                 0,957    0,017    0,455      0,957    0,617      0,654    0,990     0,848     D
Weighted Avg.    0,808    0,069    0,830      0,808    0,806      0,754    0,964     0,905     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 24897   122   475   193    98   177  1580   158    63   387   407 |     a = A
    63 12148   341   111    46    26   547   113    47   137   161 |     b = E
   891   969 16611   143   541   539 11682   262   713    98  1030 |     c = CGJLNQSRT
    15    24    63  1770    25    10   651     0     2     0     1 |     d = FV
    11    18   214    52  6208    12   381    68     3    94    22 |     e = I
    56     9   175    44    31 10858  1457   119    18   148   319 |     f = O
   660   769   944   906   600   559 65050   407   412   766  1041 |     g = 0
    46    25   146     4   103    58  1086  4290    40    14     2 |     h = BMP
     4     5   109     6     1     3    82    11  3882    22    12 |     i = U
     0     0     0     0     0     0    65     0     0     0     0 |     j = DZ
    10    16    10     1     2    10    34     1     7    22  2504 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
87.18	0.43	1.66	0.68	0.34	0.62	5.53	0.55	0.22	1.36	1.43	 a = A
0.46	88.41	2.48	0.81	0.33	0.19	3.98	0.82	0.34	1.0	1.17	 b = E
2.66	2.89	49.62	0.43	1.62	1.61	34.89	0.78	2.13	0.29	3.08	 c = CGJLNQSRT
0.59	0.94	2.46	69.11	0.98	0.39	25.42	0.0	0.08	0.0	0.04	 d = FV
0.16	0.25	3.02	0.73	87.65	0.17	5.38	0.96	0.04	1.33	0.31	 e = I
0.42	0.07	1.32	0.33	0.23	82.05	11.01	0.9	0.14	1.12	2.41	 f = O
0.92	1.07	1.31	1.26	0.83	0.78	90.2	0.56	0.57	1.06	1.44	 g = 0
0.79	0.43	2.51	0.07	1.77	1.0	18.68	73.79	0.69	0.24	0.03	 h = BMP
0.1	0.12	2.63	0.15	0.02	0.07	1.98	0.27	93.84	0.53	0.29	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
0.38	0.61	0.38	0.04	0.08	0.38	1.3	0.04	0.27	0.84	95.68	 k = D
