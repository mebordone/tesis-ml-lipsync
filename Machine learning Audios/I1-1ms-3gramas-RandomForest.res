Timestamp:2013-07-22-20:53:17
Inventario:I1
Intervalo:1ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I1-1ms-3gramas
Num Instances:  183402
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1285





=== Stratified cross-validation ===

Correctly Classified Instances      164205               89.5328 %
Incorrectly Classified Instances     19197               10.4672 %
Kappa statistic                          0.8676
K&B Relative Info Score            15668576.6829 %
K&B Information Score               451324.6425 bits      2.4608 bits/instance
Class complexity | order 0          528258.0151 bits      2.8803 bits/instance
Class complexity | scheme          8980497.7327 bits     48.9662 bits/instance
Complexity improvement     (Sf)    -8452239.7176 bits    -46.0859 bits/instance
Mean absolute error                      0.0226
Root mean squared error                  0.1103
Relative absolute error                 18.5074 %
Root relative squared error             44.6367 %
Coverage of cases (0.95 level)          95.265  %
Mean rel. region size (0.95 level)      12.4732 %
Total Number of Instances           183402     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,942    0,011    0,940      0,942    0,941      0,930    0,980     0,963     a
                 0,804    0,005    0,838      0,804    0,820      0,815    0,938     0,841     b
                 0,941    0,005    0,937      0,941    0,939      0,934    0,982     0,960     e
                 0,940    0,001    0,945      0,940    0,942      0,942    0,985     0,961     d
                 0,734    0,003    0,771      0,734    0,752      0,749    0,920     0,769     f
                 0,924    0,002    0,939      0,924    0,932      0,929    0,977     0,947     i
                 0,457    0,009    0,510      0,457    0,482      0,473    0,833     0,440     k
                 0,805    0,001    0,869      0,805    0,836      0,835    0,933     0,840     j
                 0,830    0,015    0,850      0,830    0,840      0,823    0,953     0,885     l
                 0,876    0,006    0,920      0,876    0,897      0,890    0,956     0,909     o
                 0,939    0,055    0,917      0,939    0,928      0,881    0,981     0,964     0
                 0,742    0,017    0,723      0,742    0,732      0,716    0,940     0,776     s
                 0,959    0,000    0,979      0,959    0,969      0,968    0,992     0,980     u
Weighted Avg.    0,895    0,027    0,894      0,895    0,895      0,870    0,969     0,923     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 26888    74    63    15    28    27   146    16   248    56   757   228    11 |     a = a
   132  4673    52     5    29    35    91     8   154    47   422   155    11 |     b = b
    74    67 12924    14    16    29    65     9   147    28   277    84     6 |     c = e
    27     4    25  2520     4     1     6     2    23    19    22    28     1 |     d = d
    40    31    33     6  1880    14    45    17    75    27   113   280     0 |     e = f
    45    34    40     1    15  6546    32     2    82     9   221    53     3 |     f = i
   119    72    75     7    47    28  1624    22   275    54   789   434     9 |     g = k
    31    14    24     2    24     8    27  1659    31    10    74   155     3 |     h = j
   344   138   173    21    63    61   284    24 14346   130  1249   448    11 |     i = l
    92    52    42    22    37    20    55    10   170 11590  1000   141     3 |     j = o
   586   275   243    31   104   148   499    51   926   495 67751   986    20 |     k = 0
   209   130    79    22   188    45   292    84   376   124  1176  7838     7 |     l = s
    16    14    14     0     2     7    18     4    31    12    40    13  3966 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
94.16	0.26	0.22	0.05	0.1	0.09	0.51	0.06	0.87	0.2	2.65	0.8	0.04	 a = a
2.27	80.37	0.89	0.09	0.5	0.6	1.57	0.14	2.65	0.81	7.26	2.67	0.19	 b = b
0.54	0.49	94.06	0.1	0.12	0.21	0.47	0.07	1.07	0.2	2.02	0.61	0.04	 c = e
1.01	0.15	0.93	93.96	0.15	0.04	0.22	0.07	0.86	0.71	0.82	1.04	0.04	 d = d
1.56	1.21	1.29	0.23	73.41	0.55	1.76	0.66	2.93	1.05	4.41	10.93	0.0	 e = f
0.64	0.48	0.56	0.01	0.21	92.42	0.45	0.03	1.16	0.13	3.12	0.75	0.04	 f = i
3.35	2.03	2.11	0.2	1.32	0.79	45.68	0.62	7.74	1.52	22.19	12.21	0.25	 g = k
1.5	0.68	1.16	0.1	1.16	0.39	1.31	80.46	1.5	0.48	3.59	7.52	0.15	 h = j
1.99	0.8	1.0	0.12	0.36	0.35	1.64	0.14	82.96	0.75	7.22	2.59	0.06	 i = l
0.7	0.39	0.32	0.17	0.28	0.15	0.42	0.08	1.28	87.58	7.56	1.07	0.02	 j = o
0.81	0.38	0.34	0.04	0.14	0.21	0.69	0.07	1.28	0.69	93.95	1.37	0.03	 k = 0
1.98	1.23	0.75	0.21	1.78	0.43	2.76	0.79	3.56	1.17	11.13	74.15	0.07	 l = s
0.39	0.34	0.34	0.0	0.05	0.17	0.44	0.1	0.75	0.29	0.97	0.31	95.87	 m = u
