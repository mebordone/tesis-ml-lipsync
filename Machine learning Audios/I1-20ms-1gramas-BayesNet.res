Timestamp:2013-08-30-04:18:51
Inventario:I1
Intervalo:20ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I1-20ms-1gramas
Num Instances:  9129
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(8): class 
0F1(Hz)(10): class 
0F2(Hz)(9): class 
0F3(Hz)(6): class 
0F4(Hz)(5): class 
class(13): 
LogScore Bayes: -106693.74092118705
LogScore BDeu: -109924.67013436009
LogScore MDL: -109578.69819766328
LogScore ENTROPY: -106204.58996539417
LogScore AIC: -106944.58996539417




=== Stratified cross-validation ===

Correctly Classified Instances        5478               60.0066 %
Incorrectly Classified Instances      3651               39.9934 %
Kappa statistic                          0.498 
K&B Relative Info Score             444472.8973 %
K&B Information Score                13218.0591 bits      1.4479 bits/instance
Class complexity | order 0           27128.0259 bits      2.9716 bits/instance
Class complexity | scheme            33571.8552 bits      3.6775 bits/instance
Complexity improvement     (Sf)      -6443.8293 bits     -0.7059 bits/instance
Mean absolute error                      0.0679
Root mean squared error                  0.2111
Relative absolute error                 54.0398 %
Root relative squared error             84.2376 %
Coverage of cases (0.95 level)          81.5423 %
Mean rel. region size (0.95 level)      23.0618 %
Total Number of Instances             9129     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,694    0,090    0,603      0,694    0,645      0,571    0,899     0,720     a
                 0,003    0,002    0,053      0,003    0,006      0,005    0,761     0,087     b
                 0,601    0,078    0,407      0,601    0,485      0,439    0,892     0,482     e
                 0,087    0,004    0,245      0,087    0,128      0,138    0,829     0,091     d
                 0,000    0,003    0,000      0,000    0,000      -0,006   0,754     0,036     f
                 0,551    0,037    0,398      0,551    0,462      0,441    0,916     0,464     i
                 0,055    0,003    0,250      0,055    0,090      0,110    0,847     0,119     k
                 0,018    0,001    0,182      0,018    0,033      0,054    0,786     0,049     j
                 0,277    0,074    0,294      0,277    0,285      0,209    0,785     0,248     l
                 0,316    0,028    0,491      0,316    0,385      0,354    0,830     0,396     o
                 0,935    0,111    0,823      0,935    0,875      0,804    0,976     0,964     0
                 0,267    0,029    0,375      0,267    0,312      0,280    0,897     0,329     s
                 0,445    0,015    0,420      0,445    0,432      0,418    0,914     0,438     u
Weighted Avg.    0,600    0,074    0,554      0,600    0,568      0,514    0,900     0,613     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1044    1  121    6    2   30    3    2  138   54   64   34    6 |    a = a
   39    1   47    5    1   52    3    2   60   20   39   13   17 |    b = b
  103    1  448    2    3   49    0    0   69   15   25   14   17 |    c = e
   32    0   28   12    0    7    1    0   27   18    6    2    5 |    d = d
   22    1   30    2    0    5    1    1   28   11   21   13    3 |    e = f
    4    2  102    0    0  212    0    0   25    2   17   19    2 |    f = i
    8    0    3    0    2   11   10    0   15    5   83   41    3 |    g = k
   33    0   27    0    2   11    1    2   14    5    9    5    0 |    h = j
  166    6  152    6    2   79    9    1  252   42  116   45   34 |    i = l
  175    5   77   11    1   11    2    0   76  225   73   12   44 |    j = o
   34    1   22    2   11   23    7    2   48   15 3028   43    1 |    k = 0
   50    1   26    0    0   41    1    1   78    7  197  147    2 |    l = s
   22    0   19    3    0    2    2    0   27   39    3    4   97 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
69.37	0.07	8.04	0.4	0.13	1.99	0.2	0.13	9.17	3.59	4.25	2.26	0.4	 a = a
13.04	0.33	15.72	1.67	0.33	17.39	1.0	0.67	20.07	6.69	13.04	4.35	5.69	 b = b
13.81	0.13	60.05	0.27	0.4	6.57	0.0	0.0	9.25	2.01	3.35	1.88	2.28	 c = e
23.19	0.0	20.29	8.7	0.0	5.07	0.72	0.0	19.57	13.04	4.35	1.45	3.62	 d = d
15.94	0.72	21.74	1.45	0.0	3.62	0.72	0.72	20.29	7.97	15.22	9.42	2.17	 e = f
1.04	0.52	26.49	0.0	0.0	55.06	0.0	0.0	6.49	0.52	4.42	4.94	0.52	 f = i
4.42	0.0	1.66	0.0	1.1	6.08	5.52	0.0	8.29	2.76	45.86	22.65	1.66	 g = k
30.28	0.0	24.77	0.0	1.83	10.09	0.92	1.83	12.84	4.59	8.26	4.59	0.0	 h = j
18.24	0.66	16.7	0.66	0.22	8.68	0.99	0.11	27.69	4.62	12.75	4.95	3.74	 i = l
24.58	0.7	10.81	1.54	0.14	1.54	0.28	0.0	10.67	31.6	10.25	1.69	6.18	 j = o
1.05	0.03	0.68	0.06	0.34	0.71	0.22	0.06	1.48	0.46	93.54	1.33	0.03	 k = 0
9.07	0.18	4.72	0.0	0.0	7.44	0.18	0.18	14.16	1.27	35.75	26.68	0.36	 l = s
10.09	0.0	8.72	1.38	0.0	0.92	0.92	0.0	12.39	17.89	1.38	1.83	44.5	 m = u
