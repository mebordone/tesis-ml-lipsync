Timestamp:2013-08-30-04:55:27
Inventario:I3
Intervalo:2ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I3-2ms-1gramas
Num Instances:  91703
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(25): class 
0Pitch(Hz)(48): class 
0RawPitch(32): class 
0SmPitch(39): class 
0Melogram(st)(45): class 
0ZCross(17): class 
0F1(Hz)(1291): class 
0F2(Hz)(1920): class 
0F3(Hz)(2248): class 
0F4(Hz)(2101): class 
class(11): 
LogScore Bayes: -2606243.1694392567
LogScore BDeu: -3645255.025486057
LogScore MDL: -3382209.6018295013
LogScore ENTROPY: -2894728.9223825485
LogScore AIC: -2980054.9223825485




=== Stratified cross-validation ===

Correctly Classified Instances       68865               75.0957 %
Incorrectly Classified Instances     22838               24.9043 %
Kappa statistic                          0.6753
K&B Relative Info Score            6448253.0819 %
K&B Information Score               167316.1783 bits      1.8245 bits/instance
Class complexity | order 0          237916.3779 bits      2.5944 bits/instance
Class complexity | scheme           268227.8314 bits      2.925  bits/instance
Complexity improvement     (Sf)     -30311.4535 bits     -0.3305 bits/instance
Mean absolute error                      0.0478
Root mean squared error                  0.1958
Relative absolute error                 33.9376 %
Root relative squared error             73.7698 %
Coverage of cases (0.95 level)          83.4389 %
Mean rel. region size (0.95 level)      13.3478 %
Total Number of Instances            91703     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,819    0,033    0,823      0,819    0,821      0,788    0,952     0,886     A
                 0,810    0,036    0,647      0,810    0,720      0,699    0,953     0,807     E
                 0,365    0,026    0,762      0,365    0,493      0,464    0,921     0,704     CGJLNQSRT
                 0,498    0,005    0,584      0,498    0,538      0,533    0,888     0,520     FV
                 0,821    0,015    0,687      0,821    0,748      0,740    0,956     0,817     I
                 0,722    0,018    0,760      0,722    0,741      0,721    0,916     0,781     O
                 0,907    0,161    0,783      0,907    0,841      0,732    0,967     0,958     0
                 0,603    0,008    0,724      0,603    0,658      0,651    0,889     0,656     BMP
                 0,848    0,009    0,681      0,848    0,756      0,754    0,974     0,861     U
                 0,000    0,001    0,000      0,000    0,000      -0,001   0,816     0,001     DZ
                 0,776    0,016    0,414      0,776    0,540      0,558    0,972     0,672     D
Weighted Avg.    0,751    0,078    0,757      0,751    0,737      0,681    0,947     0,848     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 11737   436   433    74   122   312   823    89    63    37   207 |     a = A
   235  5597   287    48   114    84   288    61    72     8   116 |     b = E
  1104  1356  6126    60   563   482  5984   221   389     3   505 |     c = CGJLNQSRT
    61    80    51   640    40    32   333    18     6     0    25 |     d = FV
    34   134   164    17  2923    14   185    27    16    20    27 |     e = I
   346   216   157    15    37  4797   743    57    73     9   196 |     f = O
   550   554   630   213   356   402 32503   167   127    29   302 |     g = 0
   100   139   106     8    80    73   540  1759    68     0    42 |     h = BMP
    32    59    71    12    11    49    38    18  1764     1    25 |     i = U
     0     0     0     0     0     0    33     0     0     0     0 |     j = DZ
    70    77    18     8    11    63    17    11    11     8  1019 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
81.89	3.04	3.02	0.52	0.85	2.18	5.74	0.62	0.44	0.26	1.44	 a = A
3.4	81.0	4.15	0.69	1.65	1.22	4.17	0.88	1.04	0.12	1.68	 b = E
6.57	8.07	36.48	0.36	3.35	2.87	35.63	1.32	2.32	0.02	3.01	 c = CGJLNQSRT
4.74	6.22	3.97	49.77	3.11	2.49	25.89	1.4	0.47	0.0	1.94	 d = FV
0.95	3.76	4.61	0.48	82.08	0.39	5.2	0.76	0.45	0.56	0.76	 e = I
5.21	3.25	2.36	0.23	0.56	72.18	11.18	0.86	1.1	0.14	2.95	 f = O
1.53	1.55	1.76	0.59	0.99	1.12	90.71	0.47	0.35	0.08	0.84	 g = 0
3.43	4.77	3.64	0.27	2.74	2.5	18.52	60.34	2.33	0.0	1.44	 h = BMP
1.54	2.84	3.41	0.58	0.53	2.36	1.83	0.87	84.81	0.05	1.2	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
5.33	5.86	1.37	0.61	0.84	4.8	1.29	0.84	0.84	0.61	77.61	 k = D
