Timestamp:2013-07-22-22:04:39
Inventario:I3
Intervalo:1ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I3-1ms-3gramas
Num Instances:  183402
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1111





=== Stratified cross-validation ===

Correctly Classified Instances      166735               90.9123 %
Incorrectly Classified Instances     16667                9.0877 %
Kappa statistic                          0.8821
K&B Relative Info Score            15994608.356 %
K&B Information Score               414131.6246 bits      2.2581 bits/instance
Class complexity | order 0          474843.5697 bits      2.5891 bits/instance
Class complexity | scheme          6436704.9228 bits     35.0962 bits/instance
Complexity improvement     (Sf)    -5961861.3532 bits    -32.5071 bits/instance
Mean absolute error                      0.0238
Root mean squared error                  0.1128
Relative absolute error                 16.9135 %
Root relative squared error             42.5356 %
Coverage of cases (0.95 level)          96.5704 %
Mean rel. region size (0.95 level)      13.8084 %
Total Number of Instances           183402     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,939    0,010    0,946      0,939    0,943      0,932    0,979     0,963     A
                 0,939    0,005    0,942      0,939    0,941      0,936    0,982     0,961     E
                 0,857    0,041    0,824      0,857    0,840      0,804    0,965     0,900     CGJLNQSRT
                 0,714    0,002    0,808      0,714    0,758      0,756    0,918     0,761     FV
                 0,918    0,002    0,944      0,918    0,931      0,928    0,977     0,946     I
                 0,874    0,006    0,924      0,874    0,899      0,891    0,958     0,911     O
                 0,935    0,051    0,923      0,935    0,929      0,882    0,981     0,964     0
                 0,787    0,004    0,878      0,787    0,830      0,826    0,938     0,839     BMP
                 0,955    0,000    0,982      0,955    0,968      0,968    0,992     0,980     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,507     0,000     DZ
                 0,950    0,000    0,973      0,950    0,961      0,961    0,996     0,986     D
Weighted Avg.    0,909    0,030    0,910      0,909    0,909      0,881    0,974     0,941     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 26829    57   793    22    21    49   700    64     8     4    10 |     a = A
    72 12901   414    19    15    24   227    48     5     3    12 |     b = E
   602   299 28687   242   103   280  2982   223    27    19    15 |     c = CGJLNQSRT
    40    31   485  1828    17    31    99    28     0     1     1 |     d = FV
    29    48   249    13  6501    12   202    20     8     0     1 |     e = I
    91    34   471    30    20 11569   958    40     5     1    15 |     f = O
   544   226  3004    79   158   471 67409   190    12     7    15 |     g = 0
   103    65   562    25    37    41   395  4574     8     3     1 |     h = BMP
    14    12    83     1    10    14    36    16  3951     0     0 |     i = U
     3     2    42     2     0     1    12     3     0     0     0 |     j = DZ
    23    19    38     2     3    22    20     2     1     1  2486 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
93.95	0.2	2.78	0.08	0.07	0.17	2.45	0.22	0.03	0.01	0.04	 a = A
0.52	93.89	3.01	0.14	0.11	0.17	1.65	0.35	0.04	0.02	0.09	 b = E
1.8	0.89	85.69	0.72	0.31	0.84	8.91	0.67	0.08	0.06	0.04	 c = CGJLNQSRT
1.56	1.21	18.94	71.38	0.66	1.21	3.87	1.09	0.0	0.04	0.04	 d = FV
0.41	0.68	3.52	0.18	91.78	0.17	2.85	0.28	0.11	0.0	0.01	 e = I
0.69	0.26	3.56	0.23	0.15	87.42	7.24	0.3	0.04	0.01	0.11	 f = O
0.75	0.31	4.17	0.11	0.22	0.65	93.47	0.26	0.02	0.01	0.02	 g = 0
1.77	1.12	9.67	0.43	0.64	0.71	6.79	78.67	0.14	0.05	0.02	 h = BMP
0.34	0.29	2.01	0.02	0.24	0.34	0.87	0.39	95.5	0.0	0.0	 i = U
4.62	3.08	64.62	3.08	0.0	1.54	18.46	4.62	0.0	0.0	0.0	 j = DZ
0.88	0.73	1.45	0.08	0.11	0.84	0.76	0.08	0.04	0.04	94.99	 k = D
