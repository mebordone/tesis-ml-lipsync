Timestamp:2013-08-30-04:19:17
Inventario:I2
Intervalo:1ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I2-1ms-1gramas
Num Instances:  183404
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(33): class 
0Pitch(Hz)(100): class 
0RawPitch(71): class 
0SmPitch(95): class 
0Melogram(st)(83): class 
0ZCross(21): class 
0F1(Hz)(2725): class 
0F2(Hz)(3494): class 
0F3(Hz)(3680): class 
0F4(Hz)(3605): class 
class(12): 
LogScore Bayes: -5781048.036456256
LogScore BDeu: -7966282.7246133005
LogScore MDL: -7405218.7523008175
LogScore ENTROPY: -6394608.394872549
LogScore AIC: -6561383.394872549




=== Stratified cross-validation ===

Correctly Classified Instances      149064               81.2763 %
Incorrectly Classified Instances     34340               18.7237 %
Kappa statistic                          0.759 
K&B Relative Info Score            14204568.7983 %
K&B Information Score               405830.5145 bits      2.2128 bits/instance
Class complexity | order 0          523971.7337 bits      2.8569 bits/instance
Class complexity | scheme           469446.2363 bits      2.5596 bits/instance
Complexity improvement     (Sf)      54525.4974 bits      0.2973 bits/instance
Mean absolute error                      0.0315
Root mean squared error                  0.165 
Relative absolute error                 23.837  %
Root relative squared error             64.1219 %
Coverage of cases (0.95 level)          85.2391 %
Mean rel. region size (0.95 level)      10.036  %
Total Number of Instances           183404     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,870    0,014    0,921      0,870    0,895      0,877    0,965     0,935     A
                 0,557    0,009    0,753      0,557    0,640      0,633    0,942     0,685     LGJKC
                 0,856    0,013    0,844      0,856    0,850      0,838    0,970     0,899     E
                 0,651    0,005    0,638      0,651    0,644      0,639    0,928     0,656     FV
                 0,859    0,007    0,835      0,859    0,847      0,840    0,964     0,880     I
                 0,796    0,009    0,872      0,796    0,832      0,821    0,932     0,858     O
                 0,418    0,016    0,734      0,418    0,532      0,521    0,956     0,718     SNRTY
                 0,914    0,154    0,794      0,914    0,850      0,746    0,973     0,964     0
                 0,707    0,006    0,787      0,707    0,745      0,738    0,926     0,754     BMP
                 0,886    0,010    0,574      0,886    0,697      0,708    0,975     0,835     DZ
                 0,912    0,005    0,810      0,912    0,858      0,856    0,983     0,930     U
                 0,795    0,007    0,826      0,795    0,810      0,802    0,948     0,832     SNRTXY
Weighted Avg.    0,813    0,067    0,813      0,813    0,806      0,758    0,962     0,888     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 24851   331   270    97   110   237   384  1595   151   256    73   202 |     a = A
   153  4920   187    21    82    92   595  2520    57    36   110    54 |     b = LGJKC
   151   162 11762   119    90    78   213   572   130   179    83   201 |     c = E
    26     8    48  1666    34    19    71   662     4     7    13     3 |     d = FV
    26   112    81    52  6083    36   141   367    97    30    22    36 |     e = I
   182   111    86    47    65 10530    83  1490   134   319    75   112 |     f = O
   491   238   404    35   155   314  7214  7952   119   156    91   104 |     g = SNRTY
   826   530   838   525   457   581   666 65912   355   668   322   437 |     h = 0
    85    47    59    18   137    82   145  1063  4108    15    38    17 |     i = BMP
    39     2    43     6    13    40     5    97     4  2377    14    42 |     j = DZ
    26    61    20     8     5    23    59    74    32    25  3774    30 |     k = U
   124    15   140    16    56    41   252   725    27    74    42  5867 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
87.02	1.16	0.95	0.34	0.39	0.83	1.34	5.59	0.53	0.9	0.26	0.71	 a = A
1.73	55.74	2.12	0.24	0.93	1.04	6.74	28.55	0.65	0.41	1.25	0.61	 b = LGJKC
1.1	1.18	85.6	0.87	0.66	0.57	1.55	4.16	0.95	1.3	0.6	1.46	 c = E
1.02	0.31	1.87	65.05	1.33	0.74	2.77	25.85	0.16	0.27	0.51	0.12	 d = FV
0.37	1.58	1.14	0.73	85.88	0.51	1.99	5.18	1.37	0.42	0.31	0.51	 e = I
1.38	0.84	0.65	0.36	0.49	79.57	0.63	11.26	1.01	2.41	0.57	0.85	 f = O
2.84	1.38	2.34	0.2	0.9	1.82	41.76	46.04	0.69	0.9	0.53	0.6	 g = SNRTY
1.15	0.73	1.16	0.73	0.63	0.81	0.92	91.4	0.49	0.93	0.45	0.61	 h = 0
1.46	0.81	1.01	0.31	2.36	1.41	2.49	18.28	70.66	0.26	0.65	0.29	 i = BMP
1.45	0.07	1.6	0.22	0.48	1.49	0.19	3.62	0.15	88.63	0.52	1.57	 j = DZ
0.63	1.47	0.48	0.19	0.12	0.56	1.43	1.79	0.77	0.6	91.23	0.73	 k = U
1.68	0.2	1.9	0.22	0.76	0.56	3.42	9.83	0.37	1.0	0.57	79.51	 l = SNRTXY
