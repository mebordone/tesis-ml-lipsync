Timestamp:2013-08-30-04:07:39
Inventario:I1
Intervalo:2ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I1-2ms-1gramas
Num Instances:  91703
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(25): class 
0Pitch(Hz)(45): class 
0RawPitch(31): class 
0SmPitch(39): class 
0Melogram(st)(44): class 
0ZCross(17): class 
0F1(Hz)(1439): class 
0F2(Hz)(2328): class 
0F3(Hz)(2303): class 
0F4(Hz)(2522): class 
class(13): 
LogScore Bayes: -2634270.4897012594
LogScore BDeu: -4067637.636232899
LogScore MDL: -3687222.704838777
LogScore ENTROPY: -3034831.800932265
LogScore AIC: -3149022.800932265




=== Stratified cross-validation ===

Correctly Classified Instances       71313               77.7652 %
Incorrectly Classified Instances     20390               22.2348 %
Kappa statistic                          0.7148
K&B Relative Info Score            6597790.9826 %
K&B Information Score               190460.7584 bits      2.0769 bits/instance
Class complexity | order 0          264698.8661 bits      2.8865 bits/instance
Class complexity | scheme           263898.8374 bits      2.8778 bits/instance
Complexity improvement     (Sf)        800.0287 bits      0.0087 bits/instance
Mean absolute error                      0.0367
Root mean squared error                  0.1679
Relative absolute error                 30.0169 %
Root relative squared error             67.8825 %
Coverage of cases (0.95 level)          85.5217 %
Mean rel. region size (0.95 level)      11.6299 %
Total Number of Instances            91703     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,819    0,024    0,865      0,819    0,841      0,813    0,956     0,901     a
                 0,609    0,007    0,741      0,609    0,669      0,662    0,892     0,666     b
                 0,805    0,025    0,722      0,805    0,761      0,742    0,957     0,830     e
                 0,759    0,010    0,523      0,759    0,619      0,623    0,963     0,712     d
                 0,505    0,005    0,593      0,505    0,545      0,541    0,890     0,541     f
                 0,814    0,011    0,752      0,814    0,782      0,773    0,958     0,831     i
                 0,163    0,003    0,485      0,163    0,244      0,273    0,916     0,284     k
                 0,643    0,004    0,641      0,643    0,642      0,638    0,907     0,664     j
                 0,617    0,024    0,732      0,617    0,669      0,641    0,900     0,711     l
                 0,734    0,015    0,796      0,734    0,763      0,746    0,919     0,799     o
                 0,908    0,143    0,803      0,908    0,853      0,752    0,968     0,959     0
                 0,405    0,011    0,684      0,405    0,509      0,505    0,956     0,640     s
                 0,855    0,007    0,749      0,855    0,798      0,795    0,976     0,874     u
Weighted Avg.    0,778    0,066    0,772      0,778    0,769      0,719    0,949     0,845     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 11740    92   367   154    62   102    91    60   425   255   791   132    62 |     a = a
    80  1776   119    22    14    85     5     8    90    75   531    45    65 |     b = b
   196    77  5565    99    30   108    35    34   269    89   286    62    60 |     c = e
    56     5    71  1021     2    13     5     4    47    56    48     7    11 |     d = d
    41    20    85    23   649    34     7     2    34    27   329    31     4 |     e = f
    24    45   117    31    18  2899    29    18   107    22   178    52    21 |     f = i
    18    11    54     2     3    21   290    10    59    34   997   250    31 |     g = k
    41     6    54    12     3    16    11   666    17     8   155    37     9 |     h = j
   479    78   517    98    19   152    25    34  5354   202  1416   202   104 |     i = l
   295    65   131   165    14    38    15    34   175  4877   751    21    65 |     j = o
   459   176   525   246   230   272    63   143   537   372 32549   136   125 |     k = 0
   116    25    78    61    44   104    15    13   133    71  2450  2148    40 |     l = s
    30    21    23    20     7    11     7    13    70    42    39    18  1779 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
81.91	0.64	2.56	1.07	0.43	0.71	0.63	0.42	2.97	1.78	5.52	0.92	0.43	 a = a
2.74	60.93	4.08	0.75	0.48	2.92	0.17	0.27	3.09	2.57	18.22	1.54	2.23	 b = b
2.84	1.11	80.54	1.43	0.43	1.56	0.51	0.49	3.89	1.29	4.14	0.9	0.87	 c = e
4.16	0.37	5.27	75.85	0.15	0.97	0.37	0.3	3.49	4.16	3.57	0.52	0.82	 d = d
3.19	1.56	6.61	1.79	50.47	2.64	0.54	0.16	2.64	2.1	25.58	2.41	0.31	 e = f
0.67	1.26	3.29	0.87	0.51	81.41	0.81	0.51	3.0	0.62	5.0	1.46	0.59	 f = i
1.01	0.62	3.03	0.11	0.17	1.18	16.29	0.56	3.31	1.91	56.01	14.04	1.74	 g = k
3.96	0.58	5.22	1.16	0.29	1.55	1.06	64.35	1.64	0.77	14.98	3.57	0.87	 h = j
5.52	0.9	5.96	1.13	0.22	1.75	0.29	0.39	61.68	2.33	16.31	2.33	1.2	 i = l
4.44	0.98	1.97	2.48	0.21	0.57	0.23	0.51	2.63	73.38	11.3	0.32	0.98	 j = o
1.28	0.49	1.47	0.69	0.64	0.76	0.18	0.4	1.5	1.04	90.84	0.38	0.35	 k = 0
2.19	0.47	1.47	1.15	0.83	1.96	0.28	0.25	2.51	1.34	46.24	40.54	0.76	 l = s
1.44	1.01	1.11	0.96	0.34	0.53	0.34	0.63	3.37	2.02	1.88	0.87	85.53	 m = u
