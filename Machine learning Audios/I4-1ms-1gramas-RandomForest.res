Timestamp:2013-07-22-22:33:34
Inventario:I4
Intervalo:1ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I4-1ms-1gramas
Num Instances:  183404
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1004





=== Stratified cross-validation ===

Correctly Classified Instances      168001               91.6016 %
Incorrectly Classified Instances     15403                8.3984 %
Kappa statistic                          0.8895
K&B Relative Info Score            15784409.7856 %
K&B Information Score               401513.4802 bits      2.1892 bits/instance
Class complexity | order 0          466507.9452 bits      2.5436 bits/instance
Class complexity | scheme          1258225.3219 bits      6.8604 bits/instance
Complexity improvement     (Sf)    -791717.3767 bits     -4.3168 bits/instance
Mean absolute error                      0.0272
Root mean squared error                  0.1124
Relative absolute error                 17.6856 %
Root relative squared error             40.5209 %
Coverage of cases (0.95 level)          98.4684 %
Mean rel. region size (0.95 level)      18.6797 %
Total Number of Instances           183404     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,898    0,000    0,975      0,898    0,935      0,935    0,978     0,933     C
                 0,942    0,003    0,964      0,942    0,953      0,949    0,992     0,972     E
                 0,713    0,000    0,963      0,713    0,819      0,826    0,981     0,815     FV
                 0,935    0,006    0,974      0,935    0,954      0,944    0,990     0,977     AI
                 0,877    0,036    0,816      0,877    0,845      0,816    0,984     0,936     CDGKNRSYZ
                 0,870    0,002    0,977      0,870    0,920      0,916    0,985     0,935     O
                 0,966    0,067    0,903      0,966    0,934      0,889    0,991     0,985     0
                 0,648    0,002    0,928      0,648    0,763      0,769    0,961     0,773     LT
                 0,959    0,000    0,979      0,959    0,969      0,968    0,992     0,980     U
                 0,779    0,001    0,967      0,779    0,863      0,865    0,977     0,873     MBP
Weighted Avg.    0,916    0,034    0,919      0,916    0,915      0,891    0,988     0,957     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   972    11     0    20    34     1    43     0     2     0 |     a = C
     3 12948    10    32   335    14   341    27     6    24 |     b = E
     0    23  1825    27   565     7   110     4     0     0 |     c = FV
    10    60    19 33326   853    38  1210    69    12    43 |     d = AI
     3   150    15   257 24945    76  2853   109    31    20 |     e = CDGKNRSYZ
     1    28     8    64   411 11513  1158    29     7    15 |     f = O
     6   116     9   232  1909    62 69687    58    13    25 |     g = 0
     0    50     4   120   921    36  1179  4287     4    18 |     h = LT
     2     8     2    26    61    11    41    11  3966     9 |     i = U
     0    37     3    97   524    25   559    27    10  4532 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
89.75	1.02	0.0	1.85	3.14	0.09	3.97	0.0	0.18	0.0	 a = C
0.02	94.24	0.07	0.23	2.44	0.1	2.48	0.2	0.04	0.17	 b = E
0.0	0.9	71.26	1.05	22.06	0.27	4.3	0.16	0.0	0.0	 c = FV
0.03	0.17	0.05	93.51	2.39	0.11	3.4	0.19	0.03	0.12	 d = AI
0.01	0.53	0.05	0.9	87.65	0.27	10.02	0.38	0.11	0.07	 e = CDGKNRSYZ
0.01	0.21	0.06	0.48	3.11	87.0	8.75	0.22	0.05	0.11	 f = O
0.01	0.16	0.01	0.32	2.65	0.09	96.63	0.08	0.02	0.03	 g = 0
0.0	0.76	0.06	1.81	13.91	0.54	17.81	64.77	0.06	0.27	 h = LT
0.05	0.19	0.05	0.63	1.47	0.27	0.99	0.27	95.87	0.22	 i = U
0.0	0.64	0.05	1.67	9.01	0.43	9.61	0.46	0.17	77.95	 j = MBP
