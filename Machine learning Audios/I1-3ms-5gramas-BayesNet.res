Timestamp:2013-08-30-04:15:11
Inventario:I1
Intervalo:3ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I1-3ms-5gramas
Num Instances:  61131
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(21): class 
0Pitch(Hz)(28): class 
0RawPitch(21): class 
0SmPitch(24): class 
0Melogram(st)(32): class 
0ZCross(15): class 
0F1(Hz)(233): class 
0F2(Hz)(279): class 
0F3(Hz)(454): class 
0F4(Hz)(362): class 
1Int(dB)(20): class 
1Pitch(Hz)(32): class 
1RawPitch(20): class 
1SmPitch(25): class 
1Melogram(st)(35): class 
1ZCross(15): class 
1F1(Hz)(264): class 
1F2(Hz)(269): class 
1F3(Hz)(506): class 
1F4(Hz)(345): class 
2Int(dB)(21): class 
2Pitch(Hz)(33): class 
2RawPitch(20): class 
2SmPitch(26): class 
2Melogram(st)(34): class 
2ZCross(15): class 
2F1(Hz)(300): class 
2F2(Hz)(408): class 
2F3(Hz)(511): class 
2F4(Hz)(389): class 
3Int(dB)(21): class 
3Pitch(Hz)(28): class 
3RawPitch(19): class 
3SmPitch(27): class 
3Melogram(st)(31): class 
3ZCross(15): class 
3F1(Hz)(280): class 
3F2(Hz)(355): class 
3F3(Hz)(427): class 
3F4(Hz)(299): class 
4Int(dB)(21): class 
4Pitch(Hz)(29): class 
4RawPitch(20): class 
4SmPitch(28): class 
4Melogram(st)(37): class 
4ZCross(15): class 
4F1(Hz)(301): class 
4F2(Hz)(250): class 
4F3(Hz)(382): class 
4F4(Hz)(354): class 
class(13): 
LogScore Bayes: -6290693.304183828
LogScore BDeu: -7247780.626143318
LogScore MDL: -7065115.062898308
LogScore ENTROPY: -6517327.472267387
LogScore AIC: -6616737.472267389




=== Stratified cross-validation ===

Correctly Classified Instances       39918               65.2991 %
Incorrectly Classified Instances     21213               34.7009 %
Kappa statistic                          0.5639
K&B Relative Info Score            3405953.1572 %
K&B Information Score                98557.7462 bits      1.6122 bits/instance
Class complexity | order 0          176874.5552 bits      2.8934 bits/instance
Class complexity | scheme           940689.5997 bits     15.3881 bits/instance
Complexity improvement     (Sf)    -763815.0444 bits    -12.4947 bits/instance
Mean absolute error                      0.0538
Root mean squared error                  0.2211
Relative absolute error                 43.8821 %
Root relative squared error             89.2752 %
Coverage of cases (0.95 level)          69.7747 %
Mean rel. region size (0.95 level)       9.3966 %
Total Number of Instances            61131     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,645    0,037    0,762      0,645    0,699      0,651    0,922     0,781     a
                 0,251    0,008    0,500      0,251    0,334      0,339    0,844     0,327     b
                 0,654    0,056    0,488      0,654    0,559      0,524    0,914     0,595     e
                 0,592    0,043    0,172      0,592    0,267      0,302    0,925     0,330     d
                 0,286    0,014    0,221      0,286    0,249      0,239    0,860     0,212     f
                 0,712    0,029    0,496      0,712    0,584      0,575    0,935     0,619     i
                 0,284    0,017    0,245      0,284    0,263      0,248    0,894     0,201     k
                 0,395    0,008    0,369      0,395    0,382      0,375    0,895     0,346     j
                 0,259    0,029    0,488      0,259    0,338      0,309    0,833     0,387     l
                 0,476    0,022    0,625      0,476    0,541      0,515    0,879     0,571     o
                 0,889    0,108    0,839      0,889    0,863      0,774    0,958     0,952     0
                 0,418    0,018    0,583      0,418    0,487      0,468    0,921     0,469     s
                 0,667    0,016    0,485      0,667    0,562      0,557    0,954     0,668     u
Weighted Avg.    0,653    0,061    0,669      0,653    0,650      0,599    0,920     0,708     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  6182    50   702   548   135   203   155    65   455   424   405   138   123 |     a = a
    99   488   178   168    78   205    60    27    89    60   302    62   132 |     b = b
   278    76  3022   211    79   242    94    66   221    72   158    37    65 |     c = e
    72    14    86   535    21    25     2     1    46    35    28    10    29 |     d = d
    44    20   110    75   247    50    32     7    20    30   168    42    19 |     e = f
    24    27   237    40    32  1695    78    25    51     5    99    46    23 |     f = i
    16    15    19    11    21    40   339    11    26    12   557   116     9 |     g = k
    61     2    99    18    15    53    12   274    17    13    68    54     7 |     h = j
   444    94   856   616   129   405   204    80  1506   218   787   196   284 |     i = l
   505    46   308   425    54    66    69    54   112  2125   440    52   207 |     j = o
   243    95   393   269   199   278   145    85   313   251 21096   297    58 |     k = 0
    95    33   117   125    83   148   164    46   177    42  1013  1485    24 |     l = s
    49    16    68    70    24     8    30     1    50   112    22    12   924 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
64.5	0.52	7.32	5.72	1.41	2.12	1.62	0.68	4.75	4.42	4.23	1.44	1.28	 a = a
5.08	25.05	9.14	8.62	4.0	10.52	3.08	1.39	4.57	3.08	15.5	3.18	6.78	 b = b
6.02	1.64	65.4	4.57	1.71	5.24	2.03	1.43	4.78	1.56	3.42	0.8	1.41	 c = e
7.96	1.55	9.51	59.18	2.32	2.77	0.22	0.11	5.09	3.87	3.1	1.11	3.21	 d = d
5.09	2.31	12.73	8.68	28.59	5.79	3.7	0.81	2.31	3.47	19.44	4.86	2.2	 e = f
1.01	1.13	9.95	1.68	1.34	71.16	3.27	1.05	2.14	0.21	4.16	1.93	0.97	 f = i
1.34	1.26	1.59	0.92	1.76	3.36	28.44	0.92	2.18	1.01	46.73	9.73	0.76	 g = k
8.8	0.29	14.29	2.6	2.16	7.65	1.73	39.54	2.45	1.88	9.81	7.79	1.01	 h = j
7.63	1.62	14.71	10.59	2.22	6.96	3.51	1.37	25.88	3.75	13.52	3.37	4.88	 i = l
11.32	1.03	6.9	9.52	1.21	1.48	1.55	1.21	2.51	47.61	9.86	1.17	4.64	 j = o
1.02	0.4	1.66	1.13	0.84	1.17	0.61	0.36	1.32	1.06	88.93	1.25	0.24	 k = 0
2.67	0.93	3.29	3.52	2.34	4.17	4.62	1.3	4.98	1.18	28.52	41.81	0.68	 l = s
3.54	1.15	4.91	5.05	1.73	0.58	2.16	0.07	3.61	8.08	1.59	0.87	66.67	 m = u
