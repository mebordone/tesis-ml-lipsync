Timestamp:2013-08-30-05:24:19
Inventario:I4
Intervalo:3ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I4-3ms-1gramas
Num Instances:  61135
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(20): class 
0Pitch(Hz)(30): class 
0RawPitch(22): class 
0SmPitch(23): class 
0Melogram(st)(30): class 
0ZCross(15): class 
0F1(Hz)(194): class 
0F2(Hz)(160): class 
0F3(Hz)(233): class 
0F4(Hz)(302): class 
class(10): 
LogScore Bayes: -1302190.3837008514
LogScore BDeu: -1390098.3155952324
LogScore MDL: -1376614.467086565
LogScore ENTROPY: -1320413.694462322
LogScore AIC: -1330612.694462322




=== Stratified cross-validation ===

Correctly Classified Instances       38665               63.2453 %
Incorrectly Classified Instances     22470               36.7547 %
Kappa statistic                          0.5156
K&B Relative Info Score            3230773.7593 %
K&B Information Score                82507.7355 bits      1.3496 bits/instance
Class complexity | order 0          156108.9754 bits      2.5535 bits/instance
Class complexity | scheme           225174.8155 bits      3.6832 bits/instance
Complexity improvement     (Sf)     -69065.8401 bits     -1.1297 bits/instance
Mean absolute error                      0.078 
Root mean squared error                  0.2408
Relative absolute error                 50.4886 %
Root relative squared error             86.6487 %
Coverage of cases (0.95 level)          78.6096 %
Mean rel. region size (0.95 level)      19.9504 %
Total Number of Instances            61135     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,523    0,011    0,225      0,523    0,315      0,337    0,922     0,286     C
                 0,677    0,084    0,399      0,677    0,502      0,469    0,905     0,546     E
                 0,211    0,005    0,395      0,211    0,275      0,281    0,844     0,242     FV
                 0,683    0,096    0,635      0,683    0,658      0,572    0,899     0,739     AI
                 0,186    0,042    0,452      0,186    0,263      0,213    0,827     0,422     CDGKNRSYZ
                 0,495    0,031    0,554      0,495    0,523      0,488    0,871     0,552     O
                 0,903    0,158    0,783      0,903    0,839      0,729    0,959     0,953     0
                 0,096    0,007    0,331      0,096    0,149      0,163    0,806     0,172     LT
                 0,626    0,017    0,460      0,626    0,531      0,525    0,950     0,601     U
                 0,259    0,014    0,384      0,259    0,310      0,297    0,834     0,295     MBP
Weighted Avg.    0,632    0,097    0,611      0,632    0,605      0,533    0,905     0,697     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   190    75     0    33    18    14    25     3     4     1 |     a = C
   114  3130    37   603   248   101   176    60    67    85 |     b = E
    13   155   182   101    66    56   221     3    22    45 |     c = FV
   153  1490    57  8174   568   416   658    84   160   207 |     d = AI
   169  1356    74  1720  1777   521  3296   106   320   237 |     e = CDGKNRSYZ
    96   410     8   850   159  2210   494    37   165    34 |     f = O
    31   457    63   668   589   244 21416    82    75   101 |     g = 0
    29   386    13   283   329   158   681   213    62    67 |     h = LT
    15    96     4   121    32   176    22    20   868    32 |     i = U
    35   294    23   317   148    95   354    35   142   505 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
52.34	20.66	0.0	9.09	4.96	3.86	6.89	0.83	1.1	0.28	 a = C
2.47	67.73	0.8	13.05	5.37	2.19	3.81	1.3	1.45	1.84	 b = E
1.5	17.94	21.06	11.69	7.64	6.48	25.58	0.35	2.55	5.21	 c = FV
1.28	12.45	0.48	68.3	4.75	3.48	5.5	0.7	1.34	1.73	 d = AI
1.76	14.16	0.77	17.96	18.56	5.44	34.42	1.11	3.34	2.47	 e = CDGKNRSYZ
2.15	9.19	0.18	19.05	3.56	49.52	11.07	0.83	3.7	0.76	 f = O
0.13	1.93	0.27	2.82	2.48	1.03	90.26	0.35	0.32	0.43	 g = 0
1.31	17.38	0.59	12.74	14.81	7.11	30.66	9.59	2.79	3.02	 h = LT
1.08	6.93	0.29	8.73	2.31	12.7	1.59	1.44	62.63	2.31	 i = U
1.8	15.09	1.18	16.27	7.6	4.88	18.17	1.8	7.29	25.92	 j = MBP
