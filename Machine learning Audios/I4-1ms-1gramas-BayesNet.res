Timestamp:2013-08-30-05:06:37
Inventario:I4
Intervalo:1ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I4-1ms-1gramas
Num Instances:  183404
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(33): class 
0Pitch(Hz)(93): class 
0RawPitch(72): class 
0SmPitch(93): class 
0Melogram(st)(91): class 
0ZCross(20): class 
0F1(Hz)(2623): class 
0F2(Hz)(3318): class 
0F3(Hz)(3383): class 
0F4(Hz)(3362): class 
class(10): 
LogScore Bayes: -5838130.03037705
LogScore BDeu: -7514570.559201219
LogScore MDL: -7099599.077718435
LogScore ENTROPY: -6307053.92384234
LogScore AIC: -6437842.923842339




=== Stratified cross-validation ===

Correctly Classified Instances      148183               80.7959 %
Incorrectly Classified Instances     35221               19.2041 %
Kappa statistic                          0.7457
K&B Relative Info Score            13938417.7708 %
K&B Information Score               354556.3441 bits      1.9332 bits/instance
Class complexity | order 0          466507.9452 bits      2.5436 bits/instance
Class complexity | scheme           471979.5932 bits      2.5734 bits/instance
Complexity improvement     (Sf)      -5471.6481 bits     -0.0298 bits/instance
Mean absolute error                      0.0392
Root mean squared error                  0.1858
Relative absolute error                 25.4798 %
Root relative squared error             66.9712 %
Coverage of cases (0.95 level)          84.7855 %
Mean rel. region size (0.95 level)      11.8024 %
Total Number of Instances           183404     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,863    0,009    0,358      0,863    0,506      0,553    0,954     0,823     C
                 0,866    0,015    0,822      0,866    0,844      0,831    0,969     0,893     E
                 0,651    0,006    0,599      0,651    0,624      0,619    0,919     0,647     FV
                 0,863    0,020    0,912      0,863    0,887      0,861    0,964     0,933     AI
                 0,518    0,022    0,812      0,518    0,632      0,601    0,957     0,816     CDGKNRSYZ
                 0,809    0,011    0,852      0,809    0,830      0,817    0,930     0,857     O
                 0,915    0,162    0,785      0,915    0,845      0,738    0,972     0,963     0
                 0,540    0,007    0,738      0,540    0,623      0,620    0,926     0,620     LT
                 0,914    0,006    0,769      0,914    0,835      0,834    0,981     0,925     U
                 0,714    0,006    0,787      0,714    0,749      0,742    0,920     0,752     MBP
Weighted Avg.    0,808    0,074    0,814      0,808    0,803      0,748    0,961     0,896     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   935    20     1    29     8     4    78     2     4     2 |     a = C
    98 11905   109   238   435    71   580   107    76   121 |     b = E
    16    49  1666    47    73    20   673     6     8     3 |     c = FV
   401   377   190 30770   872   270  2023   354   115   268 |     d = AI
   517   927    76   933 14731   653  9992   185   304   141 |     e = CDGKNRSYZ
   149   102    49   213   231 10705  1492    93    65   135 |     f = O
   380   927   641  1168  1056   652 65965   462   481   385 |     g = 0
    38    98    17   181   493    86  2064  3571    30    41 |     h = LT
    39    19    12    32   104    25    75    25  3781    25 |     i = U
    37    56    18   144   148    81  1090    33    53  4154 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
86.33	1.85	0.09	2.68	0.74	0.37	7.2	0.18	0.37	0.18	 a = C
0.71	86.64	0.79	1.73	3.17	0.52	4.22	0.78	0.55	0.88	 b = E
0.62	1.91	65.05	1.84	2.85	0.78	26.28	0.23	0.31	0.12	 c = FV
1.13	1.06	0.53	86.34	2.45	0.76	5.68	0.99	0.32	0.75	 d = AI
1.82	3.26	0.27	3.28	51.76	2.29	35.11	0.65	1.07	0.5	 e = CDGKNRSYZ
1.13	0.77	0.37	1.61	1.75	80.89	11.27	0.7	0.49	1.02	 f = O
0.53	1.29	0.89	1.62	1.46	0.9	91.47	0.64	0.67	0.53	 g = 0
0.57	1.48	0.26	2.73	7.45	1.3	31.18	53.95	0.45	0.62	 h = LT
0.94	0.46	0.29	0.77	2.51	0.6	1.81	0.6	91.39	0.6	 i = U
0.64	0.96	0.31	2.48	2.55	1.39	18.75	0.57	0.91	71.45	 j = MBP
