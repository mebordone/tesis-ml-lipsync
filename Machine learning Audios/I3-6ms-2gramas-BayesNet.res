Timestamp:2013-08-30-05:03:47
Inventario:I3
Intervalo:6ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I3-6ms-2gramas
Num Instances:  30452
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(14): class 
0Pitch(Hz)(12): class 
0RawPitch(13): class 
0SmPitch(11): class 
0Melogram(st)(14): class 
0ZCross(13): class 
0F1(Hz)(18): class 
0F2(Hz)(20): class 
0F3(Hz)(20): class 
0F4(Hz)(14): class 
1Int(dB)(16): class 
1Pitch(Hz)(12): class 
1RawPitch(13): class 
1SmPitch(11): class 
1Melogram(st)(19): class 
1ZCross(13): class 
1F1(Hz)(20): class 
1F2(Hz)(16): class 
1F3(Hz)(19): class 
1F4(Hz)(15): class 
class(11): 
LogScore Bayes: -957111.8329412462
LogScore BDeu: -973542.0970384595
LogScore MDL: -972002.3198306402
LogScore ENTROPY: -955881.5391244271
LogScore AIC: -959004.5391244271




=== Stratified cross-validation ===

Correctly Classified Instances       18284               60.042  %
Incorrectly Classified Instances     12168               39.958  %
Kappa statistic                          0.4887
K&B Relative Info Score            1539678.8541 %
K&B Information Score                40256.6257 bits      1.322  bits/instance
Class complexity | order 0           79594.6588 bits      2.6138 bits/instance
Class complexity | scheme           215179.6958 bits      7.0662 bits/instance
Complexity improvement     (Sf)    -135585.037  bits     -4.4524 bits/instance
Mean absolute error                      0.074 
Root mean squared error                  0.2483
Relative absolute error                 52.1959 %
Root relative squared error             93.2228 %
Coverage of cases (0.95 level)          68.984  %
Mean rel. region size (0.95 level)      14.2773 %
Total Number of Instances            30452     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,642    0,053    0,697      0,642    0,668      0,609    0,910     0,734     A
                 0,612    0,067    0,431      0,612    0,506      0,465    0,896     0,519     E
                 0,158    0,038    0,490      0,158    0,239      0,197    0,824     0,444     CGJLNQSRT
                 0,102    0,010    0,126      0,102    0,113      0,102    0,808     0,069     FV
                 0,682    0,039    0,422      0,682    0,521      0,513    0,924     0,509     I
                 0,345    0,027    0,502      0,345    0,409      0,378    0,841     0,428     O
                 0,907    0,150    0,789      0,907    0,844      0,741    0,960     0,953     0
                 0,077    0,007    0,258      0,077    0,118      0,126    0,796     0,138     BMP
                 0,565    0,025    0,342      0,565    0,426      0,423    0,934     0,507     U
                 0,000    0,003    0,000      0,000    0,000      -0,001   0,938     0,003     DZ
                 0,488    0,063    0,101      0,488    0,168      0,199    0,885     0,138     D
Weighted Avg.    0,600    0,083    0,608      0,600    0,579      0,518    0,902     0,673     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  3096   388   218    44   103   241   255    14    72    11   383 |     a = A
   205  1437   132    11   186    46    95    35    56     1   143 |     b = E
   442   663   893   101   456   217  1868    77   231    41   664 |     c = CGJLNQSRT
    32    58    35    44    30    29   103    15    17     4    63 |     d = FV
    15   176    71     3   823     1    65    12    17     2    21 |     e = I
   341   210   108    17    45   777   238    13   189     3   310 |     f = O
   146   177   249    72   132   106 10530    24    40    15   119 |     g = 0
    78   104    75    38   148    44   170    75   110     3   135 |     h = BMP
    34    58    25     9     5    69    13    18   394     5    67 |     i = U
     0     0     0     0     0     0    12     0     0     0     0 |     j = DZ
    54    63    18    11    23    19     4     8    26     0   215 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
64.17	8.04	4.52	0.91	2.13	4.99	5.28	0.29	1.49	0.23	7.94	 a = A
8.73	61.23	5.62	0.47	7.93	1.96	4.05	1.49	2.39	0.04	6.09	 b = E
7.82	11.73	15.8	1.79	8.07	3.84	33.04	1.36	4.09	0.73	11.75	 c = CGJLNQSRT
7.44	13.49	8.14	10.23	6.98	6.74	23.95	3.49	3.95	0.93	14.65	 d = FV
1.24	14.59	5.89	0.25	68.24	0.08	5.39	1.0	1.41	0.17	1.74	 e = I
15.15	9.33	4.8	0.76	2.0	34.52	10.57	0.58	8.4	0.13	13.77	 f = O
1.26	1.52	2.14	0.62	1.14	0.91	90.7	0.21	0.34	0.13	1.02	 g = 0
7.96	10.61	7.65	3.88	15.1	4.49	17.35	7.65	11.22	0.31	13.78	 h = BMP
4.88	8.32	3.59	1.29	0.72	9.9	1.87	2.58	56.53	0.72	9.61	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
12.24	14.29	4.08	2.49	5.22	4.31	0.91	1.81	5.9	0.0	48.75	 k = D
