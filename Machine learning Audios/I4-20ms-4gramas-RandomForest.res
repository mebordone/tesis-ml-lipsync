Timestamp:2013-07-22-23:07:48
Inventario:I4
Intervalo:20ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I4-20ms-4gramas
Num Instances:  9126
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3572





=== Stratified cross-validation ===

Correctly Classified Instances        6389               70.0088 %
Incorrectly Classified Instances      2737               29.9912 %
Kappa statistic                          0.6143
K&B Relative Info Score             529026.2465 %
K&B Information Score                13846.6968 bits      1.5173 bits/instance
Class complexity | order 0           23869.4748 bits      2.6155 bits/instance
Class complexity | scheme           595570.3006 bits     65.2608 bits/instance
Complexity improvement     (Sf)    -571700.8258 bits    -62.6453 bits/instance
Mean absolute error                      0.0802
Root mean squared error                  0.2011
Relative absolute error                 50.8335 %
Root relative squared error             71.6103 %
Coverage of cases (0.95 level)          94.0061 %
Mean rel. region size (0.95 level)      30.0592 %
Total Number of Instances             9126     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,283    0,001    0,773      0,283    0,415      0,466    0,892     0,312     C
                 0,631    0,046    0,548      0,631    0,587      0,549    0,920     0,574     E
                 0,116    0,003    0,372      0,116    0,177      0,201    0,775     0,115     FV
                 0,753    0,109    0,643      0,753    0,694      0,609    0,913     0,777     AI
                 0,628    0,111    0,524      0,628    0,571      0,481    0,871     0,581     CDGKNRSYZ
                 0,489    0,028    0,593      0,489    0,536      0,503    0,884     0,551     O
                 0,920    0,047    0,915      0,920    0,917      0,872    0,978     0,958     0
                 0,125    0,009    0,359      0,125    0,185      0,195    0,796     0,185     LT
                 0,472    0,004    0,757      0,472    0,582      0,591    0,905     0,594     U
                 0,191    0,006    0,509      0,191    0,277      0,298    0,819     0,254     MBP
Weighted Avg.    0,700    0,064    0,693      0,700    0,687      0,635    0,918     0,719     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   17   12    0   21    7    2    0    0    1    0 |    a = C
    2  471    1  124  100   22   11    7    1    7 |    b = E
    0   17   16   30   52   10    5    2    0    6 |    c = FV
    1  134    4 1424  198   45   55   15    3   11 |    d = AI
    1  101   11  243  937   61   94   26    8   10 |    e = CDGKNRSYZ
    1   36    2  140  111  348   54    5    9    6 |    f = O
    0   17    5   57  137   27 2974    9    2    6 |    g = 0
    0   26    1   78  130   13   40   42    2    5 |    h = LT
    0   11    0   34   27   35    1    3  103    4 |    i = U
    0   34    3   62   89   24   15    8    7   57 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
28.33	20.0	0.0	35.0	11.67	3.33	0.0	0.0	1.67	0.0	 a = C
0.27	63.14	0.13	16.62	13.4	2.95	1.47	0.94	0.13	0.94	 b = E
0.0	12.32	11.59	21.74	37.68	7.25	3.62	1.45	0.0	4.35	 c = FV
0.05	7.09	0.21	75.34	10.48	2.38	2.91	0.79	0.16	0.58	 d = AI
0.07	6.77	0.74	16.29	62.8	4.09	6.3	1.74	0.54	0.67	 e = CDGKNRSYZ
0.14	5.06	0.28	19.66	15.59	48.88	7.58	0.7	1.26	0.84	 f = O
0.0	0.53	0.15	1.76	4.24	0.83	91.96	0.28	0.06	0.19	 g = 0
0.0	7.72	0.3	23.15	38.58	3.86	11.87	12.46	0.59	1.48	 h = LT
0.0	5.05	0.0	15.6	12.39	16.06	0.46	1.38	47.25	1.83	 i = U
0.0	11.37	1.0	20.74	29.77	8.03	5.02	2.68	2.34	19.06	 j = MBP
