Timestamp:2013-08-30-05:03:57
Inventario:I3
Intervalo:6ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I3-6ms-3gramas
Num Instances:  30451
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(16): class 
0Pitch(Hz)(12): class 
0RawPitch(12): class 
0SmPitch(11): class 
0Melogram(st)(15): class 
0ZCross(13): class 
0F1(Hz)(17): class 
0F2(Hz)(19): class 
0F3(Hz)(15): class 
0F4(Hz)(14): class 
1Int(dB)(14): class 
1Pitch(Hz)(12): class 
1RawPitch(13): class 
1SmPitch(11): class 
1Melogram(st)(14): class 
1ZCross(13): class 
1F1(Hz)(18): class 
1F2(Hz)(20): class 
1F3(Hz)(20): class 
1F4(Hz)(14): class 
2Int(dB)(16): class 
2Pitch(Hz)(12): class 
2RawPitch(13): class 
2SmPitch(11): class 
2Melogram(st)(19): class 
2ZCross(13): class 
2F1(Hz)(20): class 
2F2(Hz)(16): class 
2F3(Hz)(19): class 
2F4(Hz)(15): class 
class(11): 
LogScore Bayes: -1402435.5910285322
LogScore BDeu: -1426515.9355668337
LogScore MDL: -1424302.0956412575
LogScore ENTROPY: -1400572.6709917882
LogScore AIC: -1405169.6709917882




=== Stratified cross-validation ===

Correctly Classified Instances       17951               58.9504 %
Incorrectly Classified Instances     12500               41.0496 %
Kappa statistic                          0.4774
K&B Relative Info Score            1487336.4147 %
K&B Information Score                38885.9645 bits      1.277  bits/instance
Class complexity | order 0           79593.2678 bits      2.6138 bits/instance
Class complexity | scheme           319174.5791 bits     10.4816 bits/instance
Complexity improvement     (Sf)    -239581.3113 bits     -7.8678 bits/instance
Mean absolute error                      0.0752
Root mean squared error                  0.2576
Relative absolute error                 53.0398 %
Root relative squared error             96.7345 %
Coverage of cases (0.95 level)          64.9995 %
Mean rel. region size (0.95 level)      12.3895 %
Total Number of Instances            30451     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,612    0,048    0,708      0,612    0,657      0,599    0,906     0,724     A
                 0,591    0,062    0,443      0,591    0,506      0,464    0,891     0,508     E
                 0,149    0,039    0,466      0,149    0,226      0,181    0,818     0,430     CGJLNQSRT
                 0,128    0,015    0,112      0,128    0,119      0,106    0,807     0,078     FV
                 0,669    0,039    0,411      0,669    0,509      0,500    0,921     0,483     I
                 0,324    0,026    0,503      0,324    0,394      0,367    0,840     0,417     O
                 0,902    0,145    0,794      0,902    0,844      0,742    0,958     0,951     0
                 0,065    0,006    0,269      0,065    0,105      0,119    0,795     0,136     BMP
                 0,551    0,026    0,334      0,551    0,416      0,412    0,930     0,503     U
                 0,000    0,007    0,000      0,000    0,000      -0,002   0,932     0,003     DZ
                 0,590    0,080    0,098      0,590    0,168      0,216    0,888     0,146     D
Weighted Avg.    0,590    0,080    0,608      0,590    0,573      0,512    0,899     0,666     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  2954   361   244    62   119   231   248     9    85    21   491 |     a = A
   190  1387   135    27   182    47    98    23    64     8   186 |     b = E
   403   605   844   147   448   191  1790    60   237   106   822 |     c = CGJLNQSRT
    28    52    36    55    32    29    94    10    13     6    75 |     d = FV
    17   164    75     7   807     0    64    17    22     6    27 |     e = I
   320   200   113    14    50   730   237    15   185     9   378 |     f = O
   116   167   250   109   138   113 10466    23    35    31   161 |     g = 0
    67   104    71    48   154    32   163    64   102     5   170 |     h = BMP
    33    56    25    13     9    61    15    11   384     6    84 |     i = U
     0     0     0     0     0     0    12     0     0     0     0 |     j = DZ
    46    38    18     9    23    16     2     6    21     2   260 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
61.22	7.48	5.06	1.28	2.47	4.79	5.14	0.19	1.76	0.44	10.18	 a = A
8.1	59.1	5.75	1.15	7.75	2.0	4.18	0.98	2.73	0.34	7.93	 b = E
7.13	10.7	14.93	2.6	7.92	3.38	31.66	1.06	4.19	1.88	14.54	 c = CGJLNQSRT
6.51	12.09	8.37	12.79	7.44	6.74	21.86	2.33	3.02	1.4	17.44	 d = FV
1.41	13.6	6.22	0.58	66.92	0.0	5.31	1.41	1.82	0.5	2.24	 e = I
14.22	8.88	5.02	0.62	2.22	32.43	10.53	0.67	8.22	0.4	16.79	 f = O
1.0	1.44	2.15	0.94	1.19	0.97	90.15	0.2	0.3	0.27	1.39	 g = 0
6.84	10.61	7.24	4.9	15.71	3.27	16.63	6.53	10.41	0.51	17.35	 h = BMP
4.73	8.03	3.59	1.87	1.29	8.75	2.15	1.58	55.09	0.86	12.05	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
10.43	8.62	4.08	2.04	5.22	3.63	0.45	1.36	4.76	0.45	58.96	 k = D
