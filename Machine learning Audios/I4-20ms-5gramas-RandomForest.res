Timestamp:2013-07-22-23:08:01
Inventario:I4
Intervalo:20ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I4-20ms-5gramas
Num Instances:  9125
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 4Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  42 4Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  43 4RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  44 4SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  45 4Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  47 4F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  48 4F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  49 4F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  50 4F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3677





=== Stratified cross-validation ===

Correctly Classified Instances        6383               69.9507 %
Incorrectly Classified Instances      2742               30.0493 %
Kappa statistic                          0.6134
K&B Relative Info Score             525808.4539 %
K&B Information Score                13763.5462 bits      1.5083 bits/instance
Class complexity | order 0           23867.9778 bits      2.6157 bits/instance
Class complexity | scheme           550684.1977 bits     60.349  bits/instance
Complexity improvement     (Sf)    -526816.2198 bits    -57.7333 bits/instance
Mean absolute error                      0.081 
Root mean squared error                  0.2014
Relative absolute error                 51.3413 %
Root relative squared error             71.7144 %
Coverage of cases (0.95 level)          94.4658 %
Mean rel. region size (0.95 level)      30.4668 %
Total Number of Instances             9125     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,217    0,002    0,481      0,217    0,299      0,320    0,817     0,241     C
                 0,614    0,047    0,538      0,614    0,573      0,534    0,911     0,555     E
                 0,152    0,005    0,339      0,152    0,210      0,219    0,800     0,134     FV
                 0,759    0,109    0,645      0,759    0,697      0,614    0,912     0,776     AI
                 0,615    0,112    0,518      0,615    0,562      0,471    0,872     0,573     CDGKNRSYZ
                 0,508    0,027    0,614      0,508    0,556      0,525    0,894     0,570     O
                 0,922    0,048    0,914      0,922    0,918      0,873    0,979     0,959     0
                 0,122    0,009    0,331      0,122    0,178      0,183    0,808     0,172     LT
                 0,436    0,002    0,812      0,436    0,567      0,588    0,924     0,577     U
                 0,204    0,005    0,604      0,204    0,305      0,339    0,821     0,308     MBP
Weighted Avg.    0,700    0,064    0,694      0,700    0,687      0,635    0,919     0,718     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   13   12    0   18   11    3    1    0    0    2 |    a = C
    3  458    3  139  102   19   12    5    1    4 |    b = E
    0   20   21   24   56    7    3    4    2    1 |    c = FV
    0  132    9 1434  198   40   52   15    2    8 |    d = AI
    1  108   10  257  917   60   99   29    4    7 |    e = CDGKNRSYZ
    4   37    2  121  104  362   53   11   10    8 |    f = O
    3   13    5   53  139   26 2981   10    0    3 |    g = 0
    1   25    6   85  124   15   35   41    0    5 |    h = LT
    0   22    1   25   33   33    3    4   95    2 |    i = U
    2   25    5   66   85   25   22    5    3   61 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
21.67	20.0	0.0	30.0	18.33	5.0	1.67	0.0	0.0	3.33	 a = C
0.4	61.39	0.4	18.63	13.67	2.55	1.61	0.67	0.13	0.54	 b = E
0.0	14.49	15.22	17.39	40.58	5.07	2.17	2.9	1.45	0.72	 c = FV
0.0	6.98	0.48	75.87	10.48	2.12	2.75	0.79	0.11	0.42	 d = AI
0.07	7.24	0.67	17.23	61.46	4.02	6.64	1.94	0.27	0.47	 e = CDGKNRSYZ
0.56	5.2	0.28	16.99	14.61	50.84	7.44	1.54	1.4	1.12	 f = O
0.09	0.4	0.15	1.64	4.3	0.8	92.21	0.31	0.0	0.09	 g = 0
0.3	7.42	1.78	25.22	36.8	4.45	10.39	12.17	0.0	1.48	 h = LT
0.0	10.09	0.46	11.47	15.14	15.14	1.38	1.83	43.58	0.92	 i = U
0.67	8.36	1.67	22.07	28.43	8.36	7.36	1.67	1.0	20.4	 j = MBP
