Timestamp:2013-07-22-21:50:17
Inventario:I2
Intervalo:3ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I2-3ms-2gramas
Num Instances:  61134
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1846





=== Stratified cross-validation ===

Correctly Classified Instances       52104               85.2292 %
Incorrectly Classified Instances      9030               14.7708 %
Kappa statistic                          0.8141
K&B Relative Info Score            4842551.4155 %
K&B Information Score               138990.0298 bits      2.2735 bits/instance
Class complexity | order 0          175446.5243 bits      2.8699 bits/instance
Class complexity | scheme          3476744.153  bits     56.8709 bits/instance
Complexity improvement     (Sf)    -3301297.6287 bits    -54.001  bits/instance
Mean absolute error                      0.0376
Root mean squared error                  0.1384
Relative absolute error                 28.3029 %
Root relative squared error             53.7087 %
Coverage of cases (0.95 level)          94.5873 %
Mean rel. region size (0.95 level)      16.9757 %
Total Number of Instances            61134     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,913    0,023    0,882      0,913    0,897      0,878    0,972     0,939     A
                 0,650    0,016    0,676      0,650    0,663      0,646    0,899     0,655     LGJKC
                 0,891    0,012    0,854      0,891    0,872      0,862    0,972     0,913     E
                 0,618    0,004    0,711      0,618    0,661      0,658    0,895     0,617     FV
                 0,861    0,006    0,863      0,861    0,862      0,856    0,967     0,896     I
                 0,827    0,012    0,850      0,827    0,838      0,826    0,947     0,864     O
                 0,675    0,036    0,663      0,675    0,669      0,634    0,919     0,675     SNRTY
                 0,918    0,060    0,907      0,918    0,913      0,857    0,973     0,951     0
                 0,695    0,006    0,795      0,695    0,742      0,735    0,917     0,742     BMP
                 0,817    0,001    0,900      0,817    0,857      0,856    0,975     0,880     DZ
                 0,898    0,001    0,941      0,898    0,919      0,917    0,984     0,941     U
                 0,801    0,005    0,869      0,801    0,834      0,828    0,950     0,849     SNRTXY
Weighted Avg.    0,852    0,033    0,852      0,852    0,852      0,820    0,959     0,880     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  8748   101    63    13    20    75   211   277    30     9     2    36 |     a = A
   136  1925    79    20    29    46   321   326    31     2    21    27 |     b = LGJKC
    70    62  4118    17    26    29   106   116    30     9    10    28 |     c = E
    32    42    27   534    16    25   128    35    11     4     1     9 |     d = FV
    36    38    58    12  2052    10    68    83    19     1     0     5 |     e = I
   107    56    54    16    17  3692   114   333    21    18     7    28 |     f = O
   314   265   121    79    68   107  3923   756    82    10     7    76 |     g = SNRTY
   292   249   169    38    93   222   688 21785   102    14    12    61 |     h = 0
    75    41    44     9    39    42   157   164  1354     3    10    10 |     i = BMP
    37     5    18     5     3    37    17    22     3   739     3    15 |     j = DZ
    12    19    21     2     6    26    21    19    10     2  1244     4 |     k = U
    63    43    49     6    10    33   167    98    11    10     5  1990 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
91.27	1.05	0.66	0.14	0.21	0.78	2.2	2.89	0.31	0.09	0.02	0.38	 a = A
4.59	64.97	2.67	0.67	0.98	1.55	10.83	11.0	1.05	0.07	0.71	0.91	 b = LGJKC
1.51	1.34	89.11	0.37	0.56	0.63	2.29	2.51	0.65	0.19	0.22	0.61	 c = E
3.7	4.86	3.13	61.81	1.85	2.89	14.81	4.05	1.27	0.46	0.12	1.04	 d = FV
1.51	1.6	2.43	0.5	86.15	0.42	2.85	3.48	0.8	0.04	0.0	0.21	 e = I
2.4	1.25	1.21	0.36	0.38	82.72	2.55	7.46	0.47	0.4	0.16	0.63	 f = O
5.41	4.56	2.08	1.36	1.17	1.84	67.54	13.02	1.41	0.17	0.12	1.31	 g = SNRTY
1.23	1.05	0.71	0.16	0.39	0.94	2.9	91.82	0.43	0.06	0.05	0.26	 h = 0
3.85	2.1	2.26	0.46	2.0	2.16	8.06	8.42	69.51	0.15	0.51	0.51	 i = BMP
4.09	0.55	1.99	0.55	0.33	4.09	1.88	2.43	0.33	81.75	0.33	1.66	 j = DZ
0.87	1.37	1.52	0.14	0.43	1.88	1.52	1.37	0.72	0.14	89.75	0.29	 k = U
2.54	1.73	1.97	0.24	0.4	1.33	6.72	3.94	0.44	0.4	0.2	80.08	 l = SNRTXY
