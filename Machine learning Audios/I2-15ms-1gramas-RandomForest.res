Timestamp:2013-07-22-21:57:01
Inventario:I2
Intervalo:15ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I2-15ms-1gramas
Num Instances:  12189
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.3454





=== Stratified cross-validation ===

Correctly Classified Instances        8516               69.8663 %
Incorrectly Classified Instances      3673               30.1337 %
Kappa statistic                          0.6253
K&B Relative Info Score             737121.2968 %
K&B Information Score                21588.2615 bits      1.7711 bits/instance
Class complexity | order 0           35680.6035 bits      2.9273 bits/instance
Class complexity | scheme          1341009.6941 bits    110.018  bits/instance
Complexity improvement     (Sf)    -1305329.0906 bits   -107.0907 bits/instance
Mean absolute error                      0.0651
Root mean squared error                  0.1862
Relative absolute error                 48.0726 %
Root relative squared error             71.6075 %
Coverage of cases (0.95 level)          89.671  %
Mean rel. region size (0.95 level)      23.8535 %
Total Number of Instances            12189     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,800    0,063    0,713      0,800    0,754      0,704    0,928     0,822     A
                 0,316    0,029    0,359      0,316    0,336      0,305    0,788     0,243     LGJKC
                 0,656    0,043    0,572      0,656    0,611      0,576    0,912     0,617     E
                 0,164    0,006    0,274      0,164    0,205      0,203    0,736     0,117     FV
                 0,634    0,017    0,610      0,634    0,622      0,605    0,909     0,624     I
                 0,601    0,034    0,598      0,601    0,599      0,566    0,880     0,613     O
                 0,475    0,056    0,485      0,475    0,480      0,423    0,846     0,449     SNRTY
                 0,922    0,065    0,889      0,922    0,905      0,850    0,972     0,955     0
                 0,321    0,013    0,457      0,321    0,377      0,366    0,818     0,347     BMP
                 0,165    0,004    0,400      0,165    0,233      0,250    0,817     0,211     DZ
                 0,607    0,004    0,765      0,607    0,677      0,675    0,918     0,684     U
                 0,397    0,016    0,526      0,397    0,452      0,437    0,864     0,405     SNRTXY
Weighted Avg.    0,699    0,049    0,686      0,699    0,690      0,647    0,912     0,708     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1592   44   79    2   21   62   86   70   10    4    2   17 |    a = A
   77  189   51   10   30   36  100   68    9    3    5   20 |    b = LGJKC
   88   45  637   10   46   40   37   21    8    6    1   32 |    c = E
   19   16   26   29    2   16   37    9    7    4    4    8 |    d = FV
   19   22   81    2  319    3   22   17   10    1    0    7 |    e = I
  110   30   43    6    6  565   49   69   22    8    9   23 |    f = O
  152   74   55   16   30   53  579  193   32    3    3   28 |    g = SNRTY
   47   40   24   13   12   33  146 4073   18    1    4    7 |    h = 0
   38   18   35    7   35   37   40   36  128    3    9   13 |    i = BMP
   28   10   17    2    5   44   12    4    5   30    6   19 |    j = DZ
   13   11    9    1    2   34   14    2   16    2  173    8 |    k = U
   51   27   57    8   15   22   72   20   15   10   10  202 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
80.04	2.21	3.97	0.1	1.06	3.12	4.32	3.52	0.5	0.2	0.1	0.85	 a = A
12.88	31.61	8.53	1.67	5.02	6.02	16.72	11.37	1.51	0.5	0.84	3.34	 b = LGJKC
9.06	4.63	65.6	1.03	4.74	4.12	3.81	2.16	0.82	0.62	0.1	3.3	 c = E
10.73	9.04	14.69	16.38	1.13	9.04	20.9	5.08	3.95	2.26	2.26	4.52	 d = FV
3.78	4.37	16.1	0.4	63.42	0.6	4.37	3.38	1.99	0.2	0.0	1.39	 e = I
11.7	3.19	4.57	0.64	0.64	60.11	5.21	7.34	2.34	0.85	0.96	2.45	 f = O
12.48	6.08	4.52	1.31	2.46	4.35	47.54	15.85	2.63	0.25	0.25	2.3	 g = SNRTY
1.06	0.91	0.54	0.29	0.27	0.75	3.3	92.19	0.41	0.02	0.09	0.16	 h = 0
9.52	4.51	8.77	1.75	8.77	9.27	10.03	9.02	32.08	0.75	2.26	3.26	 i = BMP
15.38	5.49	9.34	1.1	2.75	24.18	6.59	2.2	2.75	16.48	3.3	10.44	 j = DZ
4.56	3.86	3.16	0.35	0.7	11.93	4.91	0.7	5.61	0.7	60.7	2.81	 k = U
10.02	5.3	11.2	1.57	2.95	4.32	14.15	3.93	2.95	1.96	1.96	39.69	 l = SNRTXY
