Timestamp:2013-08-30-04:41:50
Inventario:I2
Intervalo:10ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I2-10ms-3gramas
Num Instances:  18294
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(11): class 
0Pitch(Hz)(6): class 
0RawPitch(4): class 
0SmPitch(6): class 
0Melogram(st)(8): class 
0ZCross(11): class 
0F1(Hz)(11): class 
0F2(Hz)(11): class 
0F3(Hz)(9): class 
0F4(Hz)(10): class 
1Int(dB)(10): class 
1Pitch(Hz)(5): class 
1RawPitch(6): class 
1SmPitch(6): class 
1Melogram(st)(9): class 
1ZCross(11): class 
1F1(Hz)(13): class 
1F2(Hz)(15): class 
1F3(Hz)(10): class 
1F4(Hz)(11): class 
2Int(dB)(10): class 
2Pitch(Hz)(7): class 
2RawPitch(6): class 
2SmPitch(7): class 
2Melogram(st)(8): class 
2ZCross(12): class 
2F1(Hz)(12): class 
2F2(Hz)(11): class 
2F3(Hz)(10): class 
2F4(Hz)(8): class 
class(12): 
LogScore Bayes: -676498.8039685681
LogScore BDeu: -690320.7694224282
LogScore MDL: -688671.2033464992
LogScore ENTROPY: -674249.0477388696
LogScore AIC: -677188.0477388694




=== Stratified cross-validation ===

Correctly Classified Instances       10598               57.9316 %
Incorrectly Classified Instances      7696               42.0684 %
Kappa statistic                          0.4733
K&B Relative Info Score             850202.3727 %
K&B Information Score                24735.1174 bits      1.3521 bits/instance
Class complexity | order 0           53205.4817 bits      2.9084 bits/instance
Class complexity | scheme           180788.5606 bits      9.8824 bits/instance
Complexity improvement     (Sf)    -127583.0789 bits     -6.974  bits/instance
Mean absolute error                      0.0706
Root mean squared error                  0.2439
Relative absolute error                 52.5212 %
Root relative squared error             94.0439 %
Coverage of cases (0.95 level)          66.6175 %
Mean rel. region size (0.95 level)      13.1077 %
Total Number of Instances            18294     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,614    0,060    0,661      0,614    0,637      0,571    0,899     0,716     A
                 0,022    0,012    0,088      0,022    0,036      0,020    0,718     0,094     LGJKC
                 0,554    0,059    0,445      0,554    0,493      0,448    0,886     0,485     E
                 0,119    0,021    0,075      0,119    0,092      0,078    0,777     0,049     FV
                 0,638    0,039    0,412      0,638    0,500      0,487    0,917     0,453     I
                 0,290    0,019    0,553      0,290    0,381      0,368    0,834     0,415     O
                 0,214    0,042    0,356      0,214    0,267      0,217    0,839     0,324     SNRTY
                 0,909    0,121    0,816      0,909    0,860      0,772    0,963     0,956     0
                 0,019    0,004    0,139      0,019    0,033      0,040    0,769     0,098     BMP
                 0,460    0,062    0,101      0,460    0,166      0,192    0,865     0,113     DZ
                 0,468    0,015    0,418      0,468    0,441      0,428    0,918     0,486     U
                 0,228    0,036    0,217      0,228    0,222      0,188    0,839     0,172     SNRTXY
Weighted Avg.    0,579    0,070    0,568      0,579    0,562      0,507    0,894     0,618     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1806   50  235   77   67  101  123  130    4  251   11   85 |    a = A
   88   20  105   35  100   20  114  226    5   96   17   68 |    b = LGJKC
  139   41  797   22   99   24   61   55   10   84   25   81 |    c = E
   23    2   35   31   18   12   25   48    2   47    3   15 |    d = FV
   14   19  106    5  477    2   47   36    2    8   10   22 |    e = I
  217   32  115   16   33  400   58  133    5  196  106   66 |    f = O
  141   24   99   85   82   25  383  614   14  173   19  131 |    g = SNRTY
   81   13   81   77   66   32  145 6176    9   48   11   57 |    h = 0
   57   11   69   31  116   23   47   82   11   62   33   52 |    i = BMP
   41    0   17    8   11   17    8    7    2  126   11   26 |    j = DZ
   27   12   30    2    6   55   14   13    3   42  198   21 |    k = U
   99    3  104   26   84   12   50   52   12  114   30  173 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
61.43	1.7	7.99	2.62	2.28	3.44	4.18	4.42	0.14	8.54	0.37	2.89	 a = A
9.84	2.24	11.74	3.91	11.19	2.24	12.75	25.28	0.56	10.74	1.9	7.61	 b = LGJKC
9.67	2.85	55.42	1.53	6.88	1.67	4.24	3.82	0.7	5.84	1.74	5.63	 c = E
8.81	0.77	13.41	11.88	6.9	4.6	9.58	18.39	0.77	18.01	1.15	5.75	 d = FV
1.87	2.54	14.17	0.67	63.77	0.27	6.28	4.81	0.27	1.07	1.34	2.94	 e = I
15.76	2.32	8.35	1.16	2.4	29.05	4.21	9.66	0.36	14.23	7.7	4.79	 f = O
7.88	1.34	5.53	4.75	4.58	1.4	21.4	34.3	0.78	9.66	1.06	7.32	 g = SNRTY
1.19	0.19	1.19	1.13	0.97	0.47	2.13	90.88	0.13	0.71	0.16	0.84	 h = 0
9.6	1.85	11.62	5.22	19.53	3.87	7.91	13.8	1.85	10.44	5.56	8.75	 i = BMP
14.96	0.0	6.2	2.92	4.01	6.2	2.92	2.55	0.73	45.99	4.01	9.49	 j = DZ
6.38	2.84	7.09	0.47	1.42	13.0	3.31	3.07	0.71	9.93	46.81	4.96	 k = U
13.04	0.4	13.7	3.43	11.07	1.58	6.59	6.85	1.58	15.02	3.95	22.79	 l = SNRTXY
