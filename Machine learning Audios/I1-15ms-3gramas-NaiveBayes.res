Timestamp:2014-12-07-10:52:00
Inventario:I1
Intervalo:15ms
Ngramas:3
Clasificador:NaiveBayes
Relation Name:  I1-15ms-3gramas
Num Instances:  12187
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Naive Bayes Classifier

                    Class
Attribute               a         b         e         d         f         i         k         j         l         o         0         s         u
                   (0.16)    (0.03)    (0.08)    (0.02)    (0.01)    (0.04)    (0.02)    (0.01)     (0.1)    (0.08)    (0.36)    (0.06)    (0.02)
==================================================================================================================================================
0Int(dB)
  mean             -6.2827  -11.7003   -8.2853   -7.3441  -11.8337  -11.4664  -29.2708   -8.8896  -11.1942  -10.3306  -49.3157  -17.7204        -9
  std. dev.        11.7372   11.7463   10.4618    6.5773   10.8769    9.6852   13.2418    8.0494    11.235   12.5524   15.6452    9.9445   11.0737
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005

0Pitch(Hz)
  mean            205.2843  176.1634   171.376  197.3539  161.8432  155.6275   59.4971  206.6355  168.3081  170.5933   15.1236  110.0923  208.3385
  std. dev.       113.8386  105.3804   96.7489   83.7783  106.9302   89.3289    95.428  101.4278   98.6582  111.0794   53.6597  108.4521  113.7908
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828

0RawPitch
  mean            195.8224   166.948  166.4294  190.1855  138.8672  144.7636   51.4086  181.0241  154.8178  160.9027   12.3493   92.3083  197.8707
  std. dev.       117.0434  110.5987   99.7164   92.9929  107.0856    91.976   94.3017  110.5992  104.6144  115.9139   47.9354   107.107  115.5032
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision           0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19

0SmPitch
  mean            203.4414  174.3874  169.6121   195.611  150.9116  152.7216   56.5635  191.3968  162.5994  167.6951   13.4413  105.4691  204.0613
  std. dev.       114.9465  107.0721     98.43   86.4955  106.0938   90.5569   94.6866  112.5764   101.812  112.6857   49.2449  108.1517  114.8803
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841

0Melogram(st)
  mean             49.1246   46.2526    46.364   52.3734   44.3633   43.7828   17.0574   44.8799   45.2455   44.5801    4.5593   30.7743   46.8237
  std. dev.        19.5383   20.8639   19.9773   12.3578   21.0748   21.0046   25.3136   23.4494   21.0262   22.3034   15.1232   26.9429     23.28
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321

0ZCross
  mean              6.2828    2.9291    4.5363    3.7923    3.0523    3.4129     0.763     4.634    3.2243    3.8308    0.2319    3.1995    3.6211
  std. dev.         4.8016    2.7939    3.4214    2.1009     2.753    3.6124    2.1179    3.3028    2.7005    3.7224    1.1941     5.301    3.3263
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026

0F1(Hz)
  mean            606.1779  434.8028  421.4711  501.7492  494.2517  328.6039   179.891  544.0151  471.2128  427.7699    54.094  361.4604  447.6356
  std. dev.       314.3985  221.0826  199.8827  156.3981  234.8699  197.0056  251.3136  202.2328  228.0413  253.7566  172.0438  296.3546  351.3336
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision          0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438

0F2(Hz)
  mean           1360.0457 1481.8564 1557.4028  1468.899 1421.4392 1600.4272  634.7153 1762.1085 1434.0402 1095.9014  161.1049 1128.0592  941.4183
  std. dev.       582.3985    667.81  715.8668  366.0398  646.1569  875.8818  885.1336  528.4896  637.1432   642.731  507.0628  872.4989  583.0066
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776

0F3(Hz)
  mean             2479.17 2496.4215 2361.5599 2807.4597 2423.2784 2260.1467 1043.2433 2791.7535 2477.2564   2286.61  277.6514 1905.8658 2306.2699
  std. dev.      1020.8902 1031.3795  1058.658  570.2892 1066.6564 1230.4607 1412.5966  836.3858 1047.1635 1195.8853  854.2277 1444.7902 1189.9252
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494

0F4(Hz)
  mean           2885.4543 2926.4759 2879.2008 3271.3215  2839.318 2723.7025 1186.6457 3062.5934 2841.4314 2646.7638  332.5609 2158.9193 2634.9155
  std. dev.      1177.5077 1176.9755 1266.1181   629.099 1221.6089 1450.2143 1614.0026  884.4115  1177.612 1374.1393 1021.1349 1629.8962 1307.4223
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707

1Int(dB)
  mean             -5.1539  -12.6599   -6.5035   -8.0119  -13.4875  -10.0663  -27.9776  -10.3348  -12.4121   -8.9829   -50.013   -19.422   -7.1056
  std. dev.        10.4855   11.4853    9.1484    6.7561    9.7297    8.7445   11.1086    7.8285   11.1588   12.0392   14.6127    8.3375    9.4113
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005

1Pitch(Hz)
  mean            211.6164   169.157  183.4717  194.5985   151.416  170.1872   54.5724  200.3096  163.0522  178.6603   12.1273   87.4997   222.874
  std. dev.       108.5356  107.9278   90.6248   87.0546  107.3372    80.118   96.3726  108.1536  100.0747  105.9497   48.3511  105.5074   96.7144
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828

1RawPitch
  mean            201.8512  156.3794  179.2232  189.0935  126.9095  160.6735   39.5096  171.5768  149.7129  170.2865    9.8346   67.6261  214.8787
  std. dev.       112.7474  112.6669   94.3626   94.4871  104.8815   84.5436   84.3002  112.0719  106.3379  111.2599   42.7207   98.8623  100.3251
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision           0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19

1SmPitch
  mean            209.8362  166.7235  181.7709    193.94  140.8054  167.6412   45.6176  180.6449  157.7592  176.3381   10.5826   81.5026  222.2202
  std. dev.       109.8749  110.0496   92.0323   88.2607    105.09   82.3894    88.932  120.2434  102.7047  107.6496   43.6632  103.5428    96.345
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841

1Melogram(st)
  mean              50.884   44.1038    49.433   51.8812   42.1057   47.5936   14.8746   40.4552   43.8201    46.689    3.8391   24.7338   51.8876
  std. dev.        17.3482   22.6152   16.6562   13.0394   22.5058    17.453   24.6697    26.043   22.0426   20.4728   13.9988   27.0164   18.0236
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321

1ZCross
  mean              6.5278    2.5119      4.81    3.7075     2.392    3.4699    1.0979    4.2984    2.7591    4.0255    0.1446     3.515    3.6946
  std. dev.         4.5495    2.4124    3.2495    2.1562    2.3218    3.3736    2.7766    3.1753    2.4051    3.4165    0.8059    6.2894    3.0049
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026

1F1(Hz)
  mean            646.5551  407.9579  448.7867  496.8996   455.282  347.8788  140.3908  530.5918  441.1747  460.8448   44.5047  295.9167  479.4518
  std. dev.       288.6109  229.4716  167.2097  161.9561   257.875  165.9413  234.5799  234.8762  231.8044  237.0895  156.5715  300.4659  324.8579
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision          0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438

1F2(Hz)
  mean           1415.1431 1414.0003 1697.5367 1453.6687 1319.8463  1816.924  488.2649 1685.2956 1386.8572 1136.7816  133.6364  935.5638 1012.0156
  std. dev.       509.9497  704.2441  606.8332  367.5538  697.8781  769.2522  818.5231  625.2818  672.6677   583.467  464.4079  904.4936  498.3854
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776

1F3(Hz)
  mean           2589.7585 2386.6296 2529.5236 2793.2433  2267.597 2497.6197  807.6565 2687.4702 2387.7719 2424.0221  231.1637 1578.4534 2541.1097
  std. dev.       897.5737 1113.8644  892.9521  599.9523 1184.2012 1063.4499 1312.7141  983.5877 1117.6839 1093.5808   786.435 1503.9955  990.0091
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494

1F4(Hz)
  mean           3011.8957 2816.2806 3071.5364 3271.8573  2669.134 2991.7043   924.148 2948.2644 2745.0585 2797.3211  277.5098 1784.0486 2890.5441
  std. dev.      1031.5735 1281.9024 1053.8859  673.6241 1361.3599 1236.9689 1510.5341 1056.4644 1263.8789  1248.566   942.466 1695.0136 1057.0684
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707

2Int(dB)
  mean             -4.6544  -12.4692   -5.5049   -8.3797  -14.3263   -9.1576  -24.1574  -11.2999   -12.944   -8.3852  -50.5594  -20.8081   -5.5692
  std. dev.         9.8223    10.584    8.2833    6.9795    8.9225    8.3719    9.7036    7.9816   10.4277   12.0282   13.7511    7.1395    7.6498
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005

2Pitch(Hz)
  mean             214.311  169.0755  189.9846  193.7345   145.764  175.8084   81.2941  193.2924  164.0239  180.3637    9.3592    72.049  231.3108
  std. dev.       106.3793  105.9927   87.3032   88.2964  105.9069   73.8088  112.1046  112.2145   99.0431  102.9621   43.0337  100.6337   84.4017
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828    0.1828

2RawPitch
  mean            204.6048   155.219  186.8578  188.6822  119.8712  168.3408   54.0652  163.1869  150.4952  174.1912    7.3688   50.0984  222.5963
  std. dev.       110.9966  111.9695   91.2535   95.9007  101.1981   79.5481   95.1492  113.8058  105.2885  108.1025   36.8772   87.9211   90.9444
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision           0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19      0.19

2SmPitch
  mean            212.5582  164.9116  188.6266  192.9144  135.2172  174.1996   69.0467  174.2298  158.9159  178.2642    8.0645   64.6148  230.9021
  std. dev.       107.6671  109.5012   88.6539   89.8451  102.9917   75.9109  106.8168  123.5109  101.6767  104.6347   38.1459   96.5376   84.0603
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841    0.1841

2Melogram(st)
  mean             51.7631   44.2448   50.9897   51.2049   40.8426    49.433   20.3758   38.6135   43.6284   47.3663    3.0766   20.9705   54.7393
  std. dev.         15.996   22.4965   14.4101    14.175   23.1702   14.7392   27.2261   26.9882   22.2052   19.6193   12.6727   26.5014   13.2895
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321    0.0321

2ZCross
  mean              6.6459    2.3765    4.8701    3.7378    2.1179    3.5072    2.0097    4.1146      2.42    4.0889    0.1006    3.6457    3.8532
  std. dev.         4.4895    2.1579    2.9907    2.1488    2.1208    3.0758    3.8238    3.1341    2.0459     2.998    0.6801    6.8152    2.8492
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026    1.1026

2F1(Hz)
  mean            680.2157  391.9946  469.1052  501.1355  420.1259  370.0223  149.0756  512.7342  419.6976  487.7924   34.4895  230.8844  504.5577
  std. dev.       263.0262  227.7233  136.7924  166.0339  268.8685  133.9826  241.1664  254.1979  227.0331   218.941  138.1474  288.0245   299.786
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision          0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438     0.438

2F2(Hz)
  mean           1457.3331 1374.8709 1803.6497 1443.3798 1256.0162 2010.9745  492.3714 1597.3744 1366.5428 1164.8777  104.1934   752.285 1075.9553
  std. dev.       439.9568  707.8796  490.0179  376.3503  726.8151  600.2718  805.2505  696.4687  682.2074   526.346  413.0188  896.0285   411.661
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776    0.5776

2F3(Hz)
  mean            2676.475 2325.0319 2651.8402 2787.5447 2156.9356 2714.2403  831.7238 2576.0221  2351.715 2529.7403  181.0252 1261.1104 2728.7599
  std. dev.       781.3816 1133.6314  731.0473  632.9234 1243.2456  831.8369 1309.4045 1097.9216 1143.7696  990.6689   703.345 1486.1393   731.591
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494    0.8494

2F4(Hz)
  mean            3105.401 2772.6162 3206.8695 3262.7057 2560.0986 3233.1412  980.7342 2839.3045 2707.7342 2916.3463  217.3015 1430.7891 3100.5588
  std. dev.       892.4908 1317.5612  845.6004  714.9786 1438.2149  946.7373 1549.9003 1192.4973 1299.2549 1123.3047  842.6819 1682.7208  725.9921
  weight sum          1989       399       971       182       177       503       237       138      1216       940      4416       734       285
  precision         0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707    0.8707





=== Stratified cross-validation ===

Correctly Classified Instances        5644               46.3116 %
Incorrectly Classified Instances      6543               53.6884 %
Kappa statistic                          0.351 
K&B Relative Info Score             400289.9756 %
K&B Information Score                11817.7032 bits      0.9697 bits/instance
Class complexity | order 0           35955.7145 bits      2.9503 bits/instance
Class complexity | scheme           201225.6047 bits     16.5115 bits/instance
Complexity improvement     (Sf)    -165269.8902 bits    -13.5612 bits/instance
Mean absolute error                      0.0826
Root mean squared error                  0.2672
Relative absolute error                 66.0936 %
Root relative squared error            106.9244 %
Coverage of cases (0.95 level)          56.2074 %
Mean rel. region size (0.95 level)      12.5544 %
Total Number of Instances            12187     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,387    0,049    0,607      0,387    0,472      0,409    0,848     0,559     a
                 0,000    0,001    0,000      0,000    0,000      -0,005   0,712     0,057     b
                 0,206    0,027    0,399      0,206    0,272      0,244    0,831     0,307     e
                 0,764    0,286    0,039      0,764    0,074      0,127    0,818     0,053     d
                 0,017    0,003    0,086      0,017    0,028      0,032    0,718     0,031     f
                 0,268    0,025    0,315      0,268    0,290      0,263    0,857     0,270     i
                 0,262    0,041    0,114      0,262    0,158      0,148    0,825     0,097     k
                 0,058    0,003    0,200      0,058    0,090      0,102    0,814     0,091     j
                 0,052    0,029    0,165      0,052    0,079      0,039    0,723     0,168     l
                 0,007    0,005    0,119      0,007    0,014      0,011    0,732     0,160     o
                 0,912    0,085    0,859      0,912    0,884      0,816    0,966     0,959     0
                 0,248    0,027    0,368      0,248    0,296      0,266    0,805     0,298     s
                 0,175    0,009    0,311      0,175    0,224      0,220    0,846     0,191     u
Weighted Avg.    0,463    0,052    0,516      0,463    0,469      0,425    0,858     0,532     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
  769    0   72  783    6   49   85    2   43   30   71   35   44 |    a = a
   30    0    9  212    0   36   26    2   20    0   37   22    5 |    b = b
  107    0  200  455    0   71   48    0   22    8   29   20   11 |    c = e
   20    0    5  139    0    0    2    0    9    0    4    3    0 |    d = d
   12    0    7   94    3    1   14    1   13    0   19   12    1 |    e = f
   28    1  100  145    0  135   39    1   18    4   17   13    2 |    f = i
    3    0    5   17    1    6   62    1   15    2   99   25    1 |    g = k
   23    1   15   49    2    8    4    8   12    0    7    9    0 |    h = j
   90    4   43  707    4   37   73    5   63    2  120   62    6 |    i = l
  103    0   24  530    3   29   45    0   51    7   83   28   37 |    j = o
    7    0    5  162    7   25   43    3   56    3 4026   77    2 |    k = 0
   37    2   11  151    8   20   83   16   49    0  173  182    2 |    l = s
   38    0    5  132    1   12   22    1   11    3    4    6   50 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
38.66	0.0	3.62	39.37	0.3	2.46	4.27	0.1	2.16	1.51	3.57	1.76	2.21	 a = a
7.52	0.0	2.26	53.13	0.0	9.02	6.52	0.5	5.01	0.0	9.27	5.51	1.25	 b = b
11.02	0.0	20.6	46.86	0.0	7.31	4.94	0.0	2.27	0.82	2.99	2.06	1.13	 c = e
10.99	0.0	2.75	76.37	0.0	0.0	1.1	0.0	4.95	0.0	2.2	1.65	0.0	 d = d
6.78	0.0	3.95	53.11	1.69	0.56	7.91	0.56	7.34	0.0	10.73	6.78	0.56	 e = f
5.57	0.2	19.88	28.83	0.0	26.84	7.75	0.2	3.58	0.8	3.38	2.58	0.4	 f = i
1.27	0.0	2.11	7.17	0.42	2.53	26.16	0.42	6.33	0.84	41.77	10.55	0.42	 g = k
16.67	0.72	10.87	35.51	1.45	5.8	2.9	5.8	8.7	0.0	5.07	6.52	0.0	 h = j
7.4	0.33	3.54	58.14	0.33	3.04	6.0	0.41	5.18	0.16	9.87	5.1	0.49	 i = l
10.96	0.0	2.55	56.38	0.32	3.09	4.79	0.0	5.43	0.74	8.83	2.98	3.94	 j = o
0.16	0.0	0.11	3.67	0.16	0.57	0.97	0.07	1.27	0.07	91.17	1.74	0.05	 k = 0
5.04	0.27	1.5	20.57	1.09	2.72	11.31	2.18	6.68	0.0	23.57	24.8	0.27	 l = s
13.33	0.0	1.75	46.32	0.35	4.21	7.72	0.35	3.86	1.05	1.4	2.11	17.54	 m = u
