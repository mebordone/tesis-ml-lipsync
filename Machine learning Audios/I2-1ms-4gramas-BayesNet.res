Timestamp:2013-08-30-04:24:09
Inventario:I2
Intervalo:1ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I2-1ms-4gramas
Num Instances:  183401
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     2 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  39 3F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(36): class 
0Pitch(Hz)(98): class 
0RawPitch(78): class 
0SmPitch(92): class 
0Melogram(st)(89): class 
0ZCross(21): class 
0F1(Hz)(2748): class 
0F2(Hz)(3533): class 
0F3(Hz)(3662): class 
0F4(Hz)(3604): class 
1Int(dB)(36): class 
1Pitch(Hz)(99): class 
1RawPitch(73): class 
1SmPitch(87): class 
1Melogram(st)(83): class 
1ZCross(21): class 
1F1(Hz)(2745): class 
1F2(Hz)(3494): class 
1F3(Hz)(3656): class 
1F4(Hz)(3597): class 
2Int(dB)(34): class 
2Pitch(Hz)(100): class 
2RawPitch(73): class 
2SmPitch(89): class 
2Melogram(st)(83): class 
2ZCross(21): class 
2F1(Hz)(2722): class 
2F2(Hz)(3506): class 
2F3(Hz)(3673): class 
2F4(Hz)(3598): class 
3Int(dB)(33): class 
3Pitch(Hz)(100): class 
3RawPitch(71): class 
3SmPitch(95): class 
3Melogram(st)(83): class 
3ZCross(21): class 
3F1(Hz)(2725): class 
3F2(Hz)(3494): class 
3F3(Hz)(3680): class 
3F4(Hz)(3605): class 
class(12): 
LogScore Bayes: -2.2097083194694284E7
LogScore BDeu: -3.0842603803140957E7
LogScore MDL: -2.8596956185067907E7
LogScore ENTROPY: -2.4552538684534688E7
LogScore AIC: -2.5219965684534688E7




=== Stratified cross-validation ===

Correctly Classified Instances      151845               82.794  %
Incorrectly Classified Instances     31556               17.206  %
Kappa statistic                          0.7799
K&B Relative Info Score            14474169.471 %
K&B Information Score               413533.9071 bits      2.2548 bits/instance
Class complexity | order 0          523967.6938 bits      2.857  bits/instance
Class complexity | scheme          1658150.9357 bits      9.0411 bits/instance
Complexity improvement     (Sf)    -1134183.242 bits     -6.1842 bits/instance
Mean absolute error                      0.0288
Root mean squared error                  0.166 
Relative absolute error                 21.7855 %
Root relative squared error             64.5264 %
Coverage of cases (0.95 level)          83.8829 %
Mean rel. region size (0.95 level)       8.7145 %
Total Number of Instances           183401     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,869    0,009    0,947      0,869    0,906      0,891    0,968     0,942     A
                 0,583    0,009    0,761      0,583    0,660      0,652    0,951     0,707     LGJKC
                 0,877    0,009    0,887      0,877    0,882      0,872    0,972     0,913     E
                 0,691    0,008    0,544      0,691    0,609      0,607    0,941     0,668     FV
                 0,885    0,006    0,856      0,885    0,870      0,865    0,966     0,890     I
                 0,824    0,007    0,900      0,824    0,860      0,851    0,938     0,872     O
                 0,494    0,017    0,746      0,494    0,595      0,576    0,960     0,734     SNRTY
                 0,908    0,136    0,812      0,908    0,857      0,759    0,973     0,963     0
                 0,743    0,006    0,805      0,743    0,773      0,766    0,936     0,774     BMP
                 0,931    0,012    0,535      0,931    0,679      0,700    0,978     0,841     DZ
                 0,941    0,004    0,829      0,941    0,881      0,880    0,983     0,942     U
                 0,837    0,007    0,842      0,837    0,839      0,833    0,953     0,851     SNRTXY
Weighted Avg.    0,828    0,059    0,831      0,828    0,824      0,779    0,965     0,896     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 24815   343   123   283    96   170   425  1524   165   348    58   207 |     a = A
   101  5150   147    21    52    64   762  2305    31    36   118    40 |     b = LGJKC
    57   155 12045   114    41    28   240   549   126   154    43   188 |     c = E
    14     8    27  1769    25    10    73   633     0     0     2     0 |     d = FV
    11    98    19    47  6267    20   147   358    75    19     2    20 |     e = I
    56   124     5    34    34 10903   105  1444   122   287    14   106 |     f = O
   405   250   363    41   154   259  8538  6551   128   337   153    94 |     g = SNRTY
   639   540   749   917   530   562   604 65480   378   918   346   451 |     h = 0
    44    45    26    10    92    53   138  1048  4318     3    37     0 |     i = BMP
    11     0    15     1     2    11     9    92     0  2496     7    38 |     j = DZ
     4    48     6    11     0     1    68    68    18     8  3891    14 |     k = U
    46     9    57     2    27    37   333   612     0    59    24  6173 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
86.9	1.2	0.43	0.99	0.34	0.6	1.49	5.34	0.58	1.22	0.2	0.72	 a = A
1.14	58.34	1.67	0.24	0.59	0.73	8.63	26.11	0.35	0.41	1.34	0.45	 b = LGJKC
0.41	1.13	87.66	0.83	0.3	0.2	1.75	4.0	0.92	1.12	0.31	1.37	 c = E
0.55	0.31	1.05	69.07	0.98	0.39	2.85	24.72	0.0	0.0	0.08	0.0	 d = FV
0.16	1.38	0.27	0.66	88.48	0.28	2.08	5.05	1.06	0.27	0.03	0.28	 e = I
0.42	0.94	0.04	0.26	0.26	82.39	0.79	10.91	0.92	2.17	0.11	0.8	 f = O
2.34	1.45	2.1	0.24	0.89	1.5	49.43	37.93	0.74	1.95	0.89	0.54	 g = SNRTY
0.89	0.75	1.04	1.27	0.73	0.78	0.84	90.8	0.52	1.27	0.48	0.63	 h = 0
0.76	0.77	0.45	0.17	1.58	0.91	2.37	18.03	74.27	0.05	0.64	0.0	 i = BMP
0.41	0.0	0.56	0.04	0.07	0.41	0.34	3.43	0.0	93.06	0.26	1.42	 j = DZ
0.1	1.16	0.15	0.27	0.0	0.02	1.64	1.64	0.44	0.19	94.05	0.34	 k = U
0.62	0.12	0.77	0.03	0.37	0.5	4.51	8.29	0.0	0.8	0.33	83.66	 l = SNRTXY
