Timestamp:2013-08-30-04:32:15
Inventario:I2
Intervalo:2ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I2-2ms-2gramas
Num Instances:  91702
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(25): class 
0Pitch(Hz)(46): class 
0RawPitch(33): class 
0SmPitch(36): class 
0Melogram(st)(46): class 
0ZCross(16): class 
0F1(Hz)(1679): class 
0F2(Hz)(2163): class 
0F3(Hz)(2662): class 
0F4(Hz)(2583): class 
1Int(dB)(23): class 
1Pitch(Hz)(46): class 
1RawPitch(35): class 
1SmPitch(39): class 
1Melogram(st)(47): class 
1ZCross(16): class 
1F1(Hz)(1632): class 
1F2(Hz)(2133): class 
1F3(Hz)(2518): class 
1F4(Hz)(2413): class 
class(12): 
LogScore Bayes: -5099832.507563236
LogScore BDeu: -7829413.913507683
LogScore MDL: -7109346.615808939
LogScore ENTROPY: -5863520.04533562
LogScore AIC: -6081583.04533562




=== Stratified cross-validation ===

Correctly Classified Instances       71015               77.4411 %
Incorrectly Classified Instances     20687               22.5589 %
Kappa statistic                          0.7114
K&B Relative Info Score            6672140.1895 %
K&B Information Score               191045.8351 bits      2.0833 bits/instance
Class complexity | order 0          262555.2873 bits      2.8631 bits/instance
Class complexity | scheme           485401.138  bits      5.2932 bits/instance
Complexity improvement     (Sf)    -222845.8508 bits     -2.4301 bits/instance
Mean absolute error                      0.038 
Root mean squared error                  0.1825
Relative absolute error                 28.6784 %
Root relative squared error             70.8579 %
Coverage of cases (0.95 level)          81.6591 %
Mean rel. region size (0.95 level)      10.0818 %
Total Number of Instances            91702     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,818    0,020    0,885      0,818    0,850      0,824    0,958     0,906     A
                 0,436    0,009    0,701      0,436    0,538      0,536    0,902     0,572     LGJKC
                 0,829    0,025    0,730      0,829    0,776      0,759    0,960     0,845     E
                 0,587    0,008    0,515      0,587    0,549      0,543    0,898     0,573     FV
                 0,842    0,012    0,732      0,842    0,783      0,776    0,959     0,842     I
                 0,759    0,012    0,829      0,759    0,793      0,778    0,923     0,819     O
                 0,317    0,017    0,655      0,317    0,427      0,420    0,937     0,619     SNRTY
                 0,905    0,147    0,798      0,905    0,848      0,744    0,968     0,959     0
                 0,649    0,007    0,752      0,649    0,697      0,690    0,899     0,694     BMP
                 0,830    0,017    0,426      0,830    0,563      0,587    0,967     0,727     DZ
                 0,890    0,007    0,743      0,890    0,810      0,809    0,978     0,908     U
                 0,758    0,011    0,748      0,758    0,753      0,743    0,933     0,783     SNRTXY
Weighted Avg.    0,774    0,067    0,774      0,774    0,764      0,715    0,952     0,851     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 11719   126   352   164   133   209   263   772    83   282    57   173 |     a = A
   146  1931   271     7   162    80   319  1220    31    95    98    68 |     b = LGJKC
   140    84  5725    63    96    31   144   275    76   115    56   105 |     c = E
    30     5    46   755    30    19    40   316    13    18    10     4 |     d = FV
    15    52   100    25  2998    18    79   182    33    21    11    27 |     e = I
   182    55   131    22    25  5046    62   727    67   212    53    64 |     f = O
   386   154   426    71   195   194  2745  3782   120   296   147   147 |     g = SNRTY
   437   276   522   315   309   349   276 32428   160   358   108   294 |     h = 0
    71    34    76    10    76    54    72   522  1892    32    57    19 |     i = BMP
    31     4    48    13     9    37     6    46     2  1117     4    29 |     j = DZ
     9    30    22    10     3    22    36    34    27    18  1852    17 |     k = U
    82     3   120    10    59    28   147   341    11    56    38  2807 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
81.76	0.88	2.46	1.14	0.93	1.46	1.83	5.39	0.58	1.97	0.4	1.21	 a = A
3.3	43.61	6.12	0.16	3.66	1.81	7.2	27.55	0.7	2.15	2.21	1.54	 b = LGJKC
2.03	1.22	82.85	0.91	1.39	0.45	2.08	3.98	1.1	1.66	0.81	1.52	 c = E
2.33	0.39	3.58	58.71	2.33	1.48	3.11	24.57	1.01	1.4	0.78	0.31	 d = FV
0.42	1.46	2.81	0.7	84.19	0.51	2.22	5.11	0.93	0.59	0.31	0.76	 e = I
2.74	0.83	1.97	0.33	0.38	75.93	0.93	10.94	1.01	3.19	0.8	0.96	 f = O
4.46	1.78	4.92	0.82	2.25	2.24	31.69	43.66	1.39	3.42	1.7	1.7	 g = SNRTY
1.22	0.77	1.46	0.88	0.86	0.97	0.77	90.5	0.45	1.0	0.3	0.82	 h = 0
2.44	1.17	2.61	0.34	2.61	1.85	2.47	17.91	64.91	1.1	1.96	0.65	 i = BMP
2.3	0.3	3.57	0.97	0.67	2.75	0.45	3.42	0.15	82.99	0.3	2.15	 j = DZ
0.43	1.44	1.06	0.48	0.14	1.06	1.73	1.63	1.3	0.87	89.04	0.82	 k = U
2.22	0.08	3.24	0.27	1.59	0.76	3.97	9.21	0.3	1.51	1.03	75.82	 l = SNRTXY
