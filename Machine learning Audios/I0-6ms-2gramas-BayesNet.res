Timestamp:2013-08-30-03:51:49
Inventario:I0
Intervalo:6ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I0-6ms-2gramas
Num Instances:  30452
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    27 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(12): class 
0Pitch(Hz)(12): class 
0RawPitch(12): class 
0SmPitch(7): class 
0Melogram(st)(12): class 
0ZCross(12): class 
0F1(Hz)(20): class 
0F2(Hz)(18): class 
0F3(Hz)(22): class 
0F4(Hz)(16): class 
1Int(dB)(11): class 
1Pitch(Hz)(11): class 
1RawPitch(11): class 
1SmPitch(9): class 
1Melogram(st)(12): class 
1ZCross(12): class 
1F1(Hz)(20): class 
1F2(Hz)(17): class 
1F3(Hz)(18): class 
1F4(Hz)(13): class 
class(27): 
LogScore Bayes: -862146.7418400749
LogScore BDeu: -904419.7006915538
LogScore MDL: -899068.2657579883
LogScore ENTROPY: -863115.2597961645
LogScore AIC: -870080.2597961645




=== Stratified cross-validation ===

Correctly Classified Instances       17707               58.1472 %
Incorrectly Classified Instances     12745               41.8528 %
Kappa statistic                          0.4824
K&B Relative Info Score            1427588.1168 %
K&B Information Score                47442.6891 bits      1.5579 bits/instance
Class complexity | order 0          101114.2365 bits      3.3204 bits/instance
Class complexity | scheme           209706.0551 bits      6.8864 bits/instance
Complexity improvement     (Sf)    -108591.8186 bits     -3.566  bits/instance
Mean absolute error                      0.0321
Root mean squared error                  0.1561
Relative absolute error                 53.2861 %
Root relative squared error             89.8912 %
Coverage of cases (0.95 level)          71.4534 %
Mean rel. region size (0.95 level)       7.909  %
Total Number of Instances            30452     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,174    0,004    0,188      0,174    0,181      0,177    0,914     0,155     ch
                 0,500    0,009    0,060      0,500    0,107      0,170    0,964     0,176     rr
                 0,901    0,112    0,832      0,901    0,865      0,778    0,961     0,954     0
                 0,179    0,019    0,094      0,179    0,123      0,116    0,841     0,067     hs
                 0,595    0,018    0,084      0,595    0,147      0,219    0,967     0,220     hg
                 0,671    0,044    0,721      0,671    0,695      0,646    0,931     0,772     a
                 0,075    0,005    0,184      0,075    0,107      0,109    0,877     0,110     c
                 0,163    0,012    0,085      0,163    0,112      0,110    0,927     0,088     b
                 0,467    0,039    0,503      0,467    0,484      0,443    0,887     0,468     e
                 0,270    0,018    0,181      0,270    0,217      0,207    0,874     0,150     d
                 0,238    0,011    0,114      0,238    0,154      0,157    0,882     0,101     g
                 0,119    0,004    0,164      0,119    0,138      0,135    0,898     0,087     f
                 0,522    0,018    0,549      0,522    0,535      0,516    0,925     0,506     i
                 0,277    0,003    0,365      0,277    0,315      0,315    0,925     0,252     j
                 0,181    0,008    0,239      0,181    0,206      0,198    0,903     0,170     m
                 0,397    0,042    0,148      0,397    0,215      0,220    0,891     0,145     l
                 0,236    0,011    0,621      0,236    0,342      0,356    0,842     0,421     o
                 0,184    0,016    0,290      0,184    0,225      0,209    0,880     0,204     n
                 0,106    0,003    0,153      0,106    0,126      0,124    0,887     0,115     q
                 0,072    0,004    0,168      0,072    0,100      0,103    0,856     0,077     p
                 0,335    0,015    0,537      0,335    0,413      0,401    0,896     0,423     s
                 0,071    0,009    0,160      0,071    0,098      0,093    0,812     0,105     r
                 0,463    0,010    0,530      0,463    0,495      0,485    0,932     0,537     u
                 0,090    0,004    0,277      0,090    0,136      0,149    0,872     0,125     t
                 0,212    0,021    0,073      0,212    0,108      0,113    0,878     0,065     v
                 0,493    0,016    0,129      0,493    0,204      0,246    0,915     0,239     y
                 0,000    0,001    0,000      0,000    0,000      -0,001   0,962     0,005     z
Weighted Avg.    0,581    0,057    0,607      0,581    0,581      0,537    0,919     0,625     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m     n     o     p     q     r     s     t     u     v     w     x     y     z    aa   <-- classified as
    25     1    42     0     1     6     5     1     3     0     0     0     3     1     0     3     0     8     1     1    42     0     0     0     0     1     0 |     a = ch
     0    18     0     0     3     0     0     0     7     0     0     0     0     0     0     2     0     2     0     0     0     1     0     0     2     1     0 |     b = rr
     6     2 10460    94    60    94    18    20    80    44    12    22   100    27    12    82    46    63    21    35   102    35    13    11    54    87    10 |     c = 0
     0     0    95    60    21    23     0     1    15     7     0     9     8     0     5    15     5     0     0     8    15     6     2     0    18    22     1 |     d = hs
     0     0     0     0    50     2     0     4    13     1     0     0     3     0     1     7     1     0     0     0     0     0     1     0     0     1     0 |     e = hg
     8    73   124    59    11  2957    20    38   215   120    30    12    23     6     7   196    99    65     9     2    34    74    24    16   129    45     9 |     f = a
     9     0   243    13     8     2    34     0    10     0     0     7    11     5     1     3     4     2     5     4    55     5     7    13     2     8     1 |     g = c
     0     9     0     0     8    16     0    33    25     3     4     0     3     0     9    40    11     2     0     0     0     0    15     0    17     8     0 |     h = b
     4    32    82    54    97   192    16    30  1097    40   116     2    90     5    25   176    30    62     7    10     9    13    18    16    84    39     1 |     i = e
     0    19     2    15     2    47     0    11    51   119    13     0    13     0     9    50     6    25     1     0     0     1     5     1    36    15     0 |     j = d
     0     3     9     0    12    27     0     7    23     9    44     0    24     2     1     4     1     4     0     0     5     2     4     0     2     2     0 |     k = g
     0     3    80    12     4    11     0     0     7     0     0    23     0     4     0     1     2     5     0     3    17     7     0     5     5     5     0 |     l = f
     4     4    50    19    69    12    11    31   141     2    24     1   629     0    42    63     3    12    13     6    15     1     1    14    10    29     0 |     m = i
     0     0    28     5     8    23     1     1    15     0     3     3     4    46     0     1     0     1     1     2    17     4     0     0     3     0     0 |     n = j
     1     3     6    20    27    27     0    27    49     8    18     2    29     0    80    71     3    20     0     1     0     4    14     1    16    15     0 |     o = m
     0     2     2     7    14    41     1    32    34    15     5     0    31     0    20   219    10    23     1     0     4     4    12     2    28    44     0 |     p = l
     5    50   227   113    70   335    12    69   151   167    26     5     7     0    21   155   532    49     4     9     8    18   123    11    63    17     4 |     q = o
     0    49    31    47    68    78     1    21    75    24    45     4    61     0    73   128     4   196     4     2    10     9    25     4    52    55     0 |     r = n
     8     0    50     0     1     8     3     0     4     0     2     1    19     0     0     1     0     1    15     2    24     0     0     1     0     1     0 |     s = q
     1     0   142    17     4    13    11     4     7    10     0    15     0     7     0    14    10     7     1    24    11    15     2     9     3     7     1 |     t = p
    49    11   502    28     6    38    15     2    18    15    11    11    55    15     1    29    15    42     7    18   499    29     1    12    13    48     0 |     u = s
     1     5    73    50     7   104     3    18    73    38    19    11    19     2     4    94    10    20     0     4    10    50    14     3    69     5     0 |     v = r
     2     3     9     4    21    36     3    27    43    15     7     2     0     0    12    70    47    21     3     2     3     3   323     8    21    12     0 |     w = u
     8     6   292    11     4     1    27     0     3     4     1     5     9     6     1     1    12    31     5     9    34    20     2    51    10    15     0 |     x = t
     0     9     2     8    16     8     4     9    22    17     6     1     5     0     7    38     5     4     0     0     1     8     3     2    50    11     0 |     y = v
     2     0     9     0     2     0     0     0     2     0     0     4     0     0     4    19     0    10     0     1    14     3     0     4     1    73     0 |     z = y
     0     0    12     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0 |    aa = z

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z	aa	
17.36	0.69	29.17	0.0	0.69	4.17	3.47	0.69	2.08	0.0	0.0	0.0	2.08	0.69	0.0	2.08	0.0	5.56	0.69	0.69	29.17	0.0	0.0	0.0	0.0	0.69	0.0	 a = ch
0.0	50.0	0.0	0.0	8.33	0.0	0.0	0.0	19.44	0.0	0.0	0.0	0.0	0.0	0.0	5.56	0.0	5.56	0.0	0.0	0.0	2.78	0.0	0.0	5.56	2.78	0.0	 b = rr
0.05	0.02	90.09	0.81	0.52	0.81	0.16	0.17	0.69	0.38	0.1	0.19	0.86	0.23	0.1	0.71	0.4	0.54	0.18	0.3	0.88	0.3	0.11	0.09	0.47	0.75	0.09	 c = 0
0.0	0.0	28.27	17.86	6.25	6.85	0.0	0.3	4.46	2.08	0.0	2.68	2.38	0.0	1.49	4.46	1.49	0.0	0.0	2.38	4.46	1.79	0.6	0.0	5.36	6.55	0.3	 d = hs
0.0	0.0	0.0	0.0	59.52	2.38	0.0	4.76	15.48	1.19	0.0	0.0	3.57	0.0	1.19	8.33	1.19	0.0	0.0	0.0	0.0	0.0	1.19	0.0	0.0	1.19	0.0	 e = hg
0.18	1.66	2.81	1.34	0.25	67.13	0.45	0.86	4.88	2.72	0.68	0.27	0.52	0.14	0.16	4.45	2.25	1.48	0.2	0.05	0.77	1.68	0.54	0.36	2.93	1.02	0.2	 f = a
1.99	0.0	53.76	2.88	1.77	0.44	7.52	0.0	2.21	0.0	0.0	1.55	2.43	1.11	0.22	0.66	0.88	0.44	1.11	0.88	12.17	1.11	1.55	2.88	0.44	1.77	0.22	 g = c
0.0	4.43	0.0	0.0	3.94	7.88	0.0	16.26	12.32	1.48	1.97	0.0	1.48	0.0	4.43	19.7	5.42	0.99	0.0	0.0	0.0	0.0	7.39	0.0	8.37	3.94	0.0	 h = b
0.17	1.36	3.49	2.3	4.13	8.18	0.68	1.28	46.74	1.7	4.94	0.09	3.83	0.21	1.07	7.5	1.28	2.64	0.3	0.43	0.38	0.55	0.77	0.68	3.58	1.66	0.04	 i = e
0.0	4.31	0.45	3.4	0.45	10.66	0.0	2.49	11.56	26.98	2.95	0.0	2.95	0.0	2.04	11.34	1.36	5.67	0.23	0.0	0.0	0.23	1.13	0.23	8.16	3.4	0.0	 j = d
0.0	1.62	4.86	0.0	6.49	14.59	0.0	3.78	12.43	4.86	23.78	0.0	12.97	1.08	0.54	2.16	0.54	2.16	0.0	0.0	2.7	1.08	2.16	0.0	1.08	1.08	0.0	 k = g
0.0	1.55	41.24	6.19	2.06	5.67	0.0	0.0	3.61	0.0	0.0	11.86	0.0	2.06	0.0	0.52	1.03	2.58	0.0	1.55	8.76	3.61	0.0	2.58	2.58	2.58	0.0	 l = f
0.33	0.33	4.15	1.58	5.72	1.0	0.91	2.57	11.69	0.17	1.99	0.08	52.16	0.0	3.48	5.22	0.25	1.0	1.08	0.5	1.24	0.08	0.08	1.16	0.83	2.4	0.0	 m = i
0.0	0.0	16.87	3.01	4.82	13.86	0.6	0.6	9.04	0.0	1.81	1.81	2.41	27.71	0.0	0.6	0.0	0.6	0.6	1.2	10.24	2.41	0.0	0.0	1.81	0.0	0.0	 n = j
0.23	0.68	1.36	4.52	6.11	6.11	0.0	6.11	11.09	1.81	4.07	0.45	6.56	0.0	18.1	16.06	0.68	4.52	0.0	0.23	0.0	0.9	3.17	0.23	3.62	3.39	0.0	 o = m
0.0	0.36	0.36	1.27	2.54	7.44	0.18	5.81	6.17	2.72	0.91	0.0	5.63	0.0	3.63	39.75	1.81	4.17	0.18	0.0	0.73	0.73	2.18	0.36	5.08	7.99	0.0	 p = l
0.22	2.22	10.08	5.02	3.11	14.88	0.53	3.07	6.71	7.42	1.16	0.22	0.31	0.0	0.93	6.89	23.63	2.18	0.18	0.4	0.36	0.8	5.46	0.49	2.8	0.76	0.18	 q = o
0.0	4.6	2.91	4.41	6.38	7.32	0.09	1.97	7.04	2.25	4.22	0.38	5.72	0.0	6.85	12.01	0.38	18.39	0.38	0.19	0.94	0.84	2.35	0.38	4.88	5.16	0.0	 r = n
5.67	0.0	35.46	0.0	0.71	5.67	2.13	0.0	2.84	0.0	1.42	0.71	13.48	0.0	0.0	0.71	0.0	0.71	10.64	1.42	17.02	0.0	0.0	0.71	0.0	0.71	0.0	 s = q
0.3	0.0	42.39	5.07	1.19	3.88	3.28	1.19	2.09	2.99	0.0	4.48	0.0	2.09	0.0	4.18	2.99	2.09	0.3	7.16	3.28	4.48	0.6	2.69	0.9	2.09	0.3	 t = p
3.29	0.74	33.69	1.88	0.4	2.55	1.01	0.13	1.21	1.01	0.74	0.74	3.69	1.01	0.07	1.95	1.01	2.82	0.47	1.21	33.49	1.95	0.07	0.81	0.87	3.22	0.0	 u = s
0.14	0.71	10.34	7.08	0.99	14.73	0.42	2.55	10.34	5.38	2.69	1.56	2.69	0.28	0.57	13.31	1.42	2.83	0.0	0.57	1.42	7.08	1.98	0.42	9.77	0.71	0.0	 v = r
0.29	0.43	1.29	0.57	3.01	5.16	0.43	3.87	6.17	2.15	1.0	0.29	0.0	0.0	1.72	10.04	6.74	3.01	0.43	0.29	0.43	0.43	46.34	1.15	3.01	1.72	0.0	 w = u
1.41	1.06	51.41	1.94	0.7	0.18	4.75	0.0	0.53	0.7	0.18	0.88	1.58	1.06	0.18	0.18	2.11	5.46	0.88	1.58	5.99	3.52	0.35	8.98	1.76	2.64	0.0	 x = t
0.0	3.81	0.85	3.39	6.78	3.39	1.69	3.81	9.32	7.2	2.54	0.42	2.12	0.0	2.97	16.1	2.12	1.69	0.0	0.0	0.42	3.39	1.27	0.85	21.19	4.66	0.0	 y = v
1.35	0.0	6.08	0.0	1.35	0.0	0.0	0.0	1.35	0.0	0.0	2.7	0.0	0.0	2.7	12.84	0.0	6.76	0.0	0.68	9.46	2.03	0.0	2.7	0.68	49.32	0.0	 z = y
0.0	0.0	100.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 aa = z
