Timestamp:2013-08-30-03:56:31
Inventario:I1
Intervalo:1ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I1-1ms-2gramas
Num Instances:  183403
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(32): class 
0Pitch(Hz)(96): class 
0RawPitch(64): class 
0SmPitch(88): class 
0Melogram(st)(89): class 
0ZCross(21): class 
0F1(Hz)(2707): class 
0F2(Hz)(3429): class 
0F3(Hz)(3620): class 
0F4(Hz)(3530): class 
1Int(dB)(33): class 
1Pitch(Hz)(95): class 
1RawPitch(63): class 
1SmPitch(93): class 
1Melogram(st)(85): class 
1ZCross(21): class 
1F1(Hz)(2711): class 
1F2(Hz)(3422): class 
1F3(Hz)(3617): class 
1F4(Hz)(3544): class 
class(13): 
LogScore Bayes: -1.1165920560928147E7
LogScore BDeu: -1.5843601354665406E7
LogScore MDL: -1.4633421574385917E7
LogScore ENTROPY: -1.2479602962733809E7
LogScore AIC: -1.2835034962733805E7




=== Stratified cross-validation ===

Correctly Classified Instances      151884               82.8143 %
Incorrectly Classified Instances     31519               17.1857 %
Kappa statistic                          0.7804
K&B Relative Info Score            14368022.1241 %
K&B Information Score               413862.2924 bits      2.2566 bits/instance
Class complexity | order 0          528259.3617 bits      2.8803 bits/instance
Class complexity | scheme           857248.4721 bits      4.6741 bits/instance
Complexity improvement     (Sf)    -328989.1104 bits     -1.7938 bits/instance
Mean absolute error                      0.0268
Root mean squared error                  0.1562
Relative absolute error                 21.9004 %
Root relative squared error             63.2114 %
Coverage of cases (0.95 level)          84.9108 %
Mean rel. region size (0.95 level)       8.485  %
Total Number of Instances           183403     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,861    0,010    0,942      0,861    0,900      0,884    0,967     0,940     a
                 0,730    0,006    0,795      0,730    0,761      0,755    0,933     0,767     b
                 0,870    0,010    0,877      0,870    0,873      0,863    0,971     0,908     e
                 0,927    0,011    0,562      0,927    0,700      0,717    0,977     0,852     d
                 0,671    0,007    0,591      0,671    0,628      0,624    0,937     0,669     f
                 0,878    0,006    0,848      0,878    0,863      0,857    0,966     0,890     i
                 0,236    0,006    0,454      0,236    0,310      0,318    0,934     0,346     k
                 0,770    0,005    0,624      0,770    0,689      0,689    0,937     0,764     j
                 0,699    0,014    0,837      0,699    0,762      0,743    0,947     0,812     l
                 0,815    0,008    0,890      0,815    0,851      0,841    0,936     0,868     o
                 0,908    0,128    0,822      0,908    0,863      0,769    0,973     0,964     0
                 0,571    0,014    0,721      0,571    0,637      0,622    0,972     0,731     s
                 0,927    0,004    0,833      0,927    0,877      0,876    0,984     0,941     u
Weighted Avg.    0,828    0,056    0,829      0,828    0,825      0,781    0,964     0,892     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 24585   175   179   315   146   107   368   192   507   207  1483   222    71 |     a = a
    55  4246    27     4     9   105    36     8    71    74  1038   100    41 |     b = b
    70   120 11958   147   118    55   116    88   334    44   548    92    50 |     c = e
    14     1    25  2486     0     5     2     0    30    12    92     8     7 |     d = d
    23     0    38     2  1718    35    18     1    21    10   627    65     3 |     e = f
    16    75    25    17    54  6222    40    20   130    26   341   109     8 |     f = i
    10    22    61     7     4    13   839     6    76    53  1747   664    53 |     g = k
    14     0    22     4     0    10    23  1587     6     6   272   112     6 |     h = j
   415   129   440   215    40   193   103    87 12094   266  2656   527   127 |     i = l
    81   134    20   328    42    35    47    27   202 10787  1459    37    35 |     j = o
   684   394   783   797   736   493   193   476   812   564 65493   365   326 |     k = 0
   111    20    60    85    28    68    30    27    93    69  3901  6034    44 |     l = s
     7    24     4    13    12     0    35    25    81     4    59    38  3835 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
86.09	0.61	0.63	1.1	0.51	0.37	1.29	0.67	1.78	0.72	5.19	0.78	0.25	 a = a
0.95	73.03	0.46	0.07	0.15	1.81	0.62	0.14	1.22	1.27	17.85	1.72	0.71	 b = b
0.51	0.87	87.03	1.07	0.86	0.4	0.84	0.64	2.43	0.32	3.99	0.67	0.36	 c = e
0.52	0.04	0.93	92.69	0.0	0.19	0.07	0.0	1.12	0.45	3.43	0.3	0.26	 d = d
0.9	0.0	1.48	0.08	67.08	1.37	0.7	0.04	0.82	0.39	24.48	2.54	0.12	 e = f
0.23	1.06	0.35	0.24	0.76	87.84	0.56	0.28	1.84	0.37	4.81	1.54	0.11	 f = i
0.28	0.62	1.72	0.2	0.11	0.37	23.6	0.17	2.14	1.49	49.14	18.68	1.49	 g = k
0.68	0.0	1.07	0.19	0.0	0.48	1.12	76.96	0.29	0.29	13.19	5.43	0.29	 h = j
2.4	0.75	2.54	1.24	0.23	1.12	0.6	0.5	69.94	1.54	15.36	3.05	0.73	 i = l
0.61	1.01	0.15	2.48	0.32	0.26	0.36	0.2	1.53	81.51	11.02	0.28	0.26	 j = o
0.95	0.55	1.09	1.11	1.02	0.68	0.27	0.66	1.13	0.78	90.82	0.51	0.45	 k = 0
1.05	0.19	0.57	0.8	0.26	0.64	0.28	0.26	0.88	0.65	36.91	57.09	0.42	 l = s
0.17	0.58	0.1	0.31	0.29	0.0	0.85	0.6	1.96	0.1	1.43	0.92	92.7	 m = u
