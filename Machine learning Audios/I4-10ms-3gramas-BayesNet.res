Timestamp:2013-08-30-05:28:37
Inventario:I4
Intervalo:10ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I4-10ms-3gramas
Num Instances:  18294
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(12): class 
0Pitch(Hz)(7): class 
0RawPitch(4): class 
0SmPitch(6): class 
0Melogram(st)(9): class 
0ZCross(10): class 
0F1(Hz)(15): class 
0F2(Hz)(12): class 
0F3(Hz)(11): class 
0F4(Hz)(9): class 
1Int(dB)(10): class 
1Pitch(Hz)(7): class 
1RawPitch(5): class 
1SmPitch(7): class 
1Melogram(st)(9): class 
1ZCross(10): class 
1F1(Hz)(13): class 
1F2(Hz)(10): class 
1F3(Hz)(9): class 
1F4(Hz)(9): class 
2Int(dB)(11): class 
2Pitch(Hz)(7): class 
2RawPitch(5): class 
2SmPitch(5): class 
2Melogram(st)(9): class 
2ZCross(11): class 
2F1(Hz)(13): class 
2F2(Hz)(13): class 
2F3(Hz)(9): class 
2F4(Hz)(8): class 
class(10): 
LogScore Bayes: -688233.760143616
LogScore BDeu: -699374.1197249998
LogScore MDL: -698494.256999107
LogScore ENTROPY: -686427.5402113693
LogScore AIC: -688886.5402113693




=== Stratified cross-validation ===

Correctly Classified Instances       10450               57.1226 %
Incorrectly Classified Instances      7844               42.8774 %
Kappa statistic                          0.449 
K&B Relative Info Score             821562.7365 %
K&B Information Score                21222.8811 bits      1.1601 bits/instance
Class complexity | order 0           47240.5333 bits      2.5823 bits/instance
Class complexity | scheme           176614.6208 bits      9.6542 bits/instance
Complexity improvement     (Sf)    -129374.0875 bits     -7.0719 bits/instance
Mean absolute error                      0.0866
Root mean squared error                  0.2721
Relative absolute error                 55.4663 %
Root relative squared error             97.38   %
Coverage of cases (0.95 level)          64.8573 %
Mean rel. region size (0.95 level)      14.9973 %
Total Number of Instances            18294     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,374    0,037    0,056      0,374    0,097      0,132    0,858     0,130     C
                 0,570    0,098    0,331      0,570    0,419      0,371    0,867     0,422     E
                 0,111    0,017    0,086      0,111    0,097      0,083    0,783     0,065     FV
                 0,546    0,083    0,624      0,546    0,582      0,487    0,867     0,681     AI
                 0,156    0,044    0,406      0,156    0,225      0,171    0,770     0,351     CDGKNRSYZ
                 0,399    0,039    0,451      0,399    0,423      0,381    0,834     0,407     O
                 0,912    0,138    0,797      0,912    0,851      0,757    0,962     0,954     0
                 0,044    0,016    0,096      0,044    0,061      0,041    0,747     0,080     LT
                 0,501    0,021    0,366      0,501    0,423      0,412    0,919     0,480     U
                 0,168    0,024    0,191      0,168    0,179      0,153    0,770     0,133     MBP
Weighted Avg.    0,571    0,088    0,567      0,571    0,554      0,483    0,877     0,632     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   40   36    0   13    4    3    4    1    4    2 |    a = C
  126  820   22  184   77   36   62   43   31   37 |    b = E
   14   58   29   30   23   28   54    3    3   19 |    c = FV
  183  584   64 2012  230  169  183   83   42  138 |    d = AI
  187  409   99  474  457  209  820   55   96  127 |    e = CDGKNRSYZ
   60  164    7  225   86  549  133   32  106   15 |    f = O
   23  110   62  119  144   67 6201   24   13   33 |    g = 0
   22  136   28   66   56   61  219   30   27   32 |    h = LT
   21   44    4   30    9   51   14   17  212   21 |    i = U
   41  115   21   69   41   43   93   25   46  100 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
37.38	33.64	0.0	12.15	3.74	2.8	3.74	0.93	3.74	1.87	 a = C
8.76	57.02	1.53	12.8	5.35	2.5	4.31	2.99	2.16	2.57	 b = E
5.36	22.22	11.11	11.49	8.81	10.73	20.69	1.15	1.15	7.28	 c = FV
4.96	15.84	1.74	54.56	6.24	4.58	4.96	2.25	1.14	3.74	 d = AI
6.38	13.94	3.38	16.16	15.58	7.13	27.96	1.88	3.27	4.33	 e = CDGKNRSYZ
4.36	11.91	0.51	16.34	6.25	39.87	9.66	2.32	7.7	1.09	 f = O
0.34	1.62	0.91	1.75	2.12	0.99	91.24	0.35	0.19	0.49	 g = 0
3.25	20.09	4.14	9.75	8.27	9.01	32.35	4.43	3.99	4.73	 h = LT
4.96	10.4	0.95	7.09	2.13	12.06	3.31	4.02	50.12	4.96	 i = U
6.9	19.36	3.54	11.62	6.9	7.24	15.66	4.21	7.74	16.84	 j = MBP
