Timestamp:2013-08-30-03:58:04
Inventario:I1
Intervalo:1ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I1-1ms-3gramas
Num Instances:  183402
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(34): class 
0Pitch(Hz)(99): class 
0RawPitch(67): class 
0SmPitch(82): class 
0Melogram(st)(86): class 
0ZCross(21): class 
0F1(Hz)(2720): class 
0F2(Hz)(3451): class 
0F3(Hz)(3623): class 
0F4(Hz)(3519): class 
1Int(dB)(32): class 
1Pitch(Hz)(96): class 
1RawPitch(64): class 
1SmPitch(88): class 
1Melogram(st)(89): class 
1ZCross(21): class 
1F1(Hz)(2707): class 
1F2(Hz)(3429): class 
1F3(Hz)(3620): class 
1F4(Hz)(3530): class 
2Int(dB)(33): class 
2Pitch(Hz)(95): class 
2RawPitch(63): class 
2SmPitch(93): class 
2Melogram(st)(85): class 
2ZCross(21): class 
2F1(Hz)(2711): class 
2F2(Hz)(3422): class 
2F3(Hz)(3617): class 
2F4(Hz)(3544): class 
class(13): 
LogScore Bayes: -1.6568695433784742E7
LogScore BDeu: -2.3589427341690857E7
LogScore MDL: -2.1773005684623692E7
LogScore ENTROPY: -1.854058249963302E7
LogScore AIC: -1.9074010499633025E7




=== Stratified cross-validation ===

Correctly Classified Instances      152316               83.0503 %
Incorrectly Classified Instances     31086               16.9497 %
Kappa statistic                          0.7837
K&B Relative Info Score            14418449.0976 %
K&B Information Score               415315.4122 bits      2.2645 bits/instance
Class complexity | order 0          528258.0151 bits      2.8803 bits/instance
Class complexity | scheme          1254446.7301 bits      6.8399 bits/instance
Complexity improvement     (Sf)    -726188.715  bits     -3.9595 bits/instance
Mean absolute error                      0.0261
Root mean squared error                  0.157 
Relative absolute error                 21.3985 %
Root relative squared error             63.5215 %
Coverage of cases (0.95 level)          84.4696 %
Mean rel. region size (0.95 level)       8.1682 %
Total Number of Instances           183402     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,861    0,009    0,948      0,861    0,903      0,887    0,968     0,941     a
                 0,733    0,006    0,795      0,733    0,762      0,756    0,935     0,772     b
                 0,875    0,009    0,883      0,875    0,879      0,869    0,972     0,912     e
                 0,934    0,011    0,561      0,934    0,701      0,719    0,978     0,860     d
                 0,681    0,007    0,579      0,681    0,626      0,622    0,940     0,674     f
                 0,881    0,006    0,851      0,881    0,866      0,860    0,966     0,893     i
                 0,249    0,007    0,419      0,249    0,313      0,313    0,934     0,347     k
                 0,776    0,006    0,616      0,776    0,687      0,688    0,938     0,763     j
                 0,707    0,013    0,849      0,707    0,771      0,754    0,949     0,817     l
                 0,821    0,008    0,895      0,821    0,856      0,847    0,938     0,871     o
                 0,906    0,125    0,825      0,906    0,864      0,771    0,973     0,964     0
                 0,583    0,014    0,715      0,583    0,642      0,627    0,973     0,739     s
                 0,929    0,004    0,842      0,929    0,883      0,882    0,984     0,941     u
Weighted Avg.    0,831    0,055    0,832      0,831    0,828      0,785    0,965     0,894     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 24599   182   152   339   169   105   401   209   470   189  1426   258    58 |     a = a
    52  4261    29     3    14   106    46     8    57    65  1021   110    42 |     b = b
    54   121 12020   141   114    46   140    79   305    39   537    99    45 |     c = e
     7     1    17  2505     0     1     4     0    28    13    89    10     7 |     d = d
    15     0    29     1  1743    29    30     0    19    10   616    65     4 |     e = f
    12    85    22    15    56  6238    58    18   102    28   330   118     1 |     f = i
    10    18    58     5     4    13   886     5    68    44  1729   666    49 |     g = k
    12     0    24     3     0     7    30  1601     3     4   263   110     5 |     h = j
   382   120   437   186    40   168   149    76 12217   256  2599   544   118 |     i = l
    65   127    11   305    42    35    74    20   189 10869  1429    46    22 |     j = o
   633   405   742   856   792   519   223   523   781   561 65370   387   323 |     k = 0
    97    16    60   100    23    62    36    32    81    64  3788  6164    47 |     l = s
     6    27     4     9    11     0    36    28    72     5    55    41  3843 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
86.14	0.64	0.53	1.19	0.59	0.37	1.4	0.73	1.65	0.66	4.99	0.9	0.2	 a = a
0.89	73.29	0.5	0.05	0.24	1.82	0.79	0.14	0.98	1.12	17.56	1.89	0.72	 b = b
0.39	0.88	87.48	1.03	0.83	0.33	1.02	0.57	2.22	0.28	3.91	0.72	0.33	 c = e
0.26	0.04	0.63	93.4	0.0	0.04	0.15	0.0	1.04	0.48	3.32	0.37	0.26	 d = d
0.59	0.0	1.13	0.04	68.06	1.13	1.17	0.0	0.74	0.39	24.05	2.54	0.16	 e = f
0.17	1.2	0.31	0.21	0.79	88.07	0.82	0.25	1.44	0.4	4.66	1.67	0.01	 f = i
0.28	0.51	1.63	0.14	0.11	0.37	24.92	0.14	1.91	1.24	48.64	18.73	1.38	 g = k
0.58	0.0	1.16	0.15	0.0	0.34	1.45	77.64	0.15	0.19	12.75	5.33	0.24	 h = j
2.21	0.69	2.53	1.08	0.23	0.97	0.86	0.44	70.65	1.48	15.03	3.15	0.68	 i = l
0.49	0.96	0.08	2.3	0.32	0.26	0.56	0.15	1.43	82.13	10.8	0.35	0.17	 j = o
0.88	0.56	1.03	1.19	1.1	0.72	0.31	0.73	1.08	0.78	90.65	0.54	0.45	 k = 0
0.92	0.15	0.57	0.95	0.22	0.59	0.34	0.3	0.77	0.61	35.84	58.32	0.44	 l = s
0.15	0.65	0.1	0.22	0.27	0.0	0.87	0.68	1.74	0.12	1.33	0.99	92.89	 m = u
