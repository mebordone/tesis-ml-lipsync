Timestamp:2013-08-30-05:28:46
Inventario:I4
Intervalo:10ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I4-10ms-4gramas
Num Instances:  18293
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(10): class 
0Pitch(Hz)(6): class 
0RawPitch(4): class 
0SmPitch(6): class 
0Melogram(st)(9): class 
0ZCross(8): class 
0F1(Hz)(10): class 
0F2(Hz)(10): class 
0F3(Hz)(8): class 
0F4(Hz)(8): class 
1Int(dB)(12): class 
1Pitch(Hz)(7): class 
1RawPitch(4): class 
1SmPitch(6): class 
1Melogram(st)(9): class 
1ZCross(10): class 
1F1(Hz)(15): class 
1F2(Hz)(12): class 
1F3(Hz)(11): class 
1F4(Hz)(9): class 
2Int(dB)(10): class 
2Pitch(Hz)(7): class 
2RawPitch(5): class 
2SmPitch(7): class 
2Melogram(st)(9): class 
2ZCross(10): class 
2F1(Hz)(13): class 
2F2(Hz)(10): class 
2F3(Hz)(9): class 
2F4(Hz)(9): class 
3Int(dB)(11): class 
3Pitch(Hz)(7): class 
3RawPitch(5): class 
3SmPitch(5): class 
3Melogram(st)(9): class 
3ZCross(11): class 
3F1(Hz)(13): class 
3F2(Hz)(13): class 
3F3(Hz)(9): class 
3F4(Hz)(8): class 
class(10): 
LogScore Bayes: -901407.7225513487
LogScore BDeu: -915529.573769517
LogScore MDL: -914440.8409392673
LogScore ENTROPY: -898988.2669167568
LogScore AIC: -902137.2669167568




=== Stratified cross-validation ===

Correctly Classified Instances       10226               55.9012 %
Incorrectly Classified Instances      8067               44.0988 %
Kappa statistic                          0.4367
K&B Relative Info Score             795604.947  %
K&B Information Score                20552.9862 bits      1.1235 bits/instance
Class complexity | order 0           47239.1047 bits      2.5824 bits/instance
Class complexity | scheme           229372.7449 bits     12.5388 bits/instance
Complexity improvement     (Sf)    -182133.6402 bits     -9.9565 bits/instance
Mean absolute error                      0.0888
Root mean squared error                  0.2802
Relative absolute error                 56.8473 %
Root relative squared error            100.2725 %
Coverage of cases (0.95 level)          62.2643 %
Mean rel. region size (0.95 level)      13.8321 %
Total Number of Instances            18293     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,467    0,048    0,054      0,467    0,097      0,146    0,865     0,152     C
                 0,548    0,096    0,327      0,548    0,409      0,359    0,858     0,399     E
                 0,103    0,020    0,069      0,103    0,083      0,068    0,788     0,057     FV
                 0,505    0,074    0,633      0,505    0,562      0,471    0,860     0,662     AI
                 0,160    0,044    0,410      0,160    0,230      0,176    0,757     0,340     CDGKNRSYZ
                 0,383    0,041    0,434      0,383    0,407      0,363    0,832     0,389     O
                 0,905    0,133    0,800      0,905    0,849      0,755    0,954     0,930     0
                 0,052    0,023    0,079      0,052    0,063      0,035    0,736     0,075     LT
                 0,489    0,019    0,382      0,489    0,429      0,417    0,913     0,468     U
                 0,187    0,028    0,185      0,187    0,186      0,159    0,766     0,127     MBP
Weighted Avg.    0,559    0,085    0,568      0,559    0,549      0,477    0,869     0,613     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   50   33    0    9    5    1    3    1    3    2 |    a = C
  154  788   20  151   77   43   65   61   37   42 |    b = E
   21   61   27   29   27   28   45    7    3   13 |    c = FV
  237  579   76 1862  211  193  192  146   37  155 |    d = AI
  246  405  127  425  470  207  773   55   76  149 |    e = CDGKNRSYZ
   69  169    8  212   81  528  139   48  105   18 |    f = O
   30  110   73  116  167   59 6148   36   12   44 |    g = 0
   31  122   27   50   63   67  211   35   25   46 |    h = LT
   23   44    3   30    8   44   18   27  207   19 |    i = U
   58  102   29   58   37   47   88   27   37  111 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
46.73	30.84	0.0	8.41	4.67	0.93	2.8	0.93	2.8	1.87	 a = C
10.71	54.8	1.39	10.5	5.35	2.99	4.52	4.24	2.57	2.92	 b = E
8.05	23.37	10.34	11.11	10.34	10.73	17.24	2.68	1.15	4.98	 c = FV
6.43	15.7	2.06	50.49	5.72	5.23	5.21	3.96	1.0	4.2	 d = AI
8.39	13.81	4.33	14.49	16.02	7.06	26.36	1.88	2.59	5.08	 e = CDGKNRSYZ
5.01	12.27	0.58	15.4	5.88	38.34	10.09	3.49	7.63	1.31	 f = O
0.44	1.62	1.07	1.71	2.46	0.87	90.48	0.53	0.18	0.65	 g = 0
4.58	18.02	3.99	7.39	9.31	9.9	31.17	5.17	3.69	6.79	 h = LT
5.44	10.4	0.71	7.09	1.89	10.4	4.26	6.38	48.94	4.49	 i = U
9.76	17.17	4.88	9.76	6.23	7.91	14.81	4.55	6.23	18.69	 j = MBP
