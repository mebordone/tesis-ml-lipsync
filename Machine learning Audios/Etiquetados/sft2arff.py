import os

sftfiles = []
for file in os.listdir(os.getcwd()):
		if '.sft' in file:
			sftfiles.append(file)
inventario = {
	'ch':'s',
	'rr':'l',
	'0':'0',
	'hs':'a',
	'hg':'a',
	'a':'a',
	'c':'k',
	'b':'b',
	'e':'e',
	'd':'d',
	'g':'j',
	'f':'f',
	'i':'i',
	'j':'j',
	'm':'b',
	'l':'l',
	'o':'o',
	'n':'l',
	'q':'k',
	'p':'b',
	's':'s',
	'r':'l',
	'u':'u',
	't':'l',
	'v':'f',
	'y':'s'}

posiciones = set ([])
data=[]
for filename in sftfiles:
	file = open (filename,'r')
	for line in file:
		if line.startswith('\\') or line.startswith('Time'):
			continue
		lineitems = line.split('\t')
		for i in range(0,len(lineitems)):
			if lineitems [i] == '':
				lineitems [i] = '0'
		pos = lineitems[1]
		if ' ' in pos:
			poss = pos.split(' ')
			pos = poss [1]
		posiciones.add(pos)
		del lineitems[1]
		lineitems.insert(len(lineitems)-1,pos)
		data.append(','.join(lineitems))
	file.close()

arff = open ('speech.arff','w')
arff.write("""@relation speech
@attribute Time numeric
@attribute Int(dB) numeric
@attribute Pitch(Hz) numeric
@attribute RawPitch numeric
@attribute SmPitch	numeric
@attribute Melogram(st) numeric
@attribute ZCross numeric
@attribute F1(Hz) numeric
@attribute F2(Hz) numeric
@attribute F3(Hz) numeric
@attribute F4(Hz) numeric
@attribute class {'ch', 'rr', '0', 'hs', 'hg', 'a', 'c', 'b', 'e', 'd', 'g', 'f', 'i', 'j', 'm', 'l', 'o', 'n', 'q', 'p', 's', 'r', 'u', 't', 'v', 'y'}
@data
""")
for item in data:
	arff.write(item)
arff.close()
