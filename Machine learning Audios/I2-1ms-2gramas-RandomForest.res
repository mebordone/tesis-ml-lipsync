Timestamp:2013-07-22-21:25:48
Inventario:I2
Intervalo:1ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I2-1ms-2gramas
Num Instances:  183403
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1192





=== Stratified cross-validation ===

Correctly Classified Instances      164846               89.8818 %
Incorrectly Classified Instances     18557               10.1182 %
Kappa statistic                          0.8717
K&B Relative Info Score            15778471.4008 %
K&B Information Score               450796.8238 bits      2.458  bits/instance
Class complexity | order 0          523970.387  bits      2.8569 bits/instance
Class complexity | scheme          8124738.8044 bits     44.2999 bits/instance
Complexity improvement     (Sf)    -7600768.4173 bits    -41.443  bits/instance
Mean absolute error                      0.0242
Root mean squared error                  0.1132
Relative absolute error                 18.3009 %
Root relative squared error             43.9884 %
Coverage of cases (0.95 level)          95.6211 %
Mean rel. region size (0.95 level)      13.4528 %
Total Number of Instances           183403     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,939    0,010    0,948      0,939    0,943      0,933    0,979     0,963     A
                 0,719    0,010    0,782      0,719    0,749      0,738    0,917     0,780     LGJKC
                 0,942    0,004    0,948      0,942    0,945      0,940    0,982     0,961     E
                 0,715    0,003    0,800      0,715    0,755      0,753    0,923     0,782     FV
                 0,924    0,002    0,944      0,924    0,934      0,931    0,977     0,947     I
                 0,874    0,005    0,932      0,874    0,902      0,895    0,961     0,918     O
                 0,749    0,030    0,722      0,749    0,735      0,707    0,946     0,797     SNRTY
                 0,949    0,059    0,912      0,949    0,930      0,884    0,982     0,968     0
                 0,787    0,004    0,880      0,787    0,831      0,827    0,937     0,842     BMP
                 0,929    0,001    0,950      0,929    0,940      0,939    0,987     0,965     DZ
                 0,958    0,001    0,976      0,958    0,967      0,966    0,992     0,979     U
                 0,870    0,003    0,931      0,870    0,899      0,896    0,961     0,909     SNRTXY
Weighted Avg.    0,899    0,029    0,899      0,899    0,898      0,874    0,971     0,928     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 26804   177    57    33    29    56   472   805    58    13     7    46 |     a = A
   200  6350    89    46    36    83   916   958    62     7    23    57 |     b = LGJKC
    64   100 12941    18    21    26   218   256    45    15     4    32 |     c = E
    43    68    23  1830    14    22   378   132    25     6     0    20 |     d = FV
    50    45    31    15  6543     9   117   236    23     2     5     7 |     e = I
    94    71    36    20    18 11562   281  1055    28    30     7    32 |     f = O
   375   603   157   185    82   186 12934  2399   159    20    14   159 |     g = SNRTY
   435   499   186    90   125   308  1724 68435   182    24    19    89 |     h = 0
   109    99    49    21    40    51   366   474  4575     1    11    18 |     i = BMP
    24     3    17     8     4    32    44    43     2  2492     1    12 |     j = DZ
    13    25    14     0    10    19    27    45    19     1  3962     2 |     k = U
    68    82    54    21     8    53   437   200    21    11     6  6418 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
93.86	0.62	0.2	0.12	0.1	0.2	1.65	2.82	0.2	0.05	0.02	0.16	 a = A
2.27	71.94	1.01	0.52	0.41	0.94	10.38	10.85	0.7	0.08	0.26	0.65	 b = LGJKC
0.47	0.73	94.18	0.13	0.15	0.19	1.59	1.86	0.33	0.11	0.03	0.23	 c = E
1.68	2.66	0.9	71.46	0.55	0.86	14.76	5.15	0.98	0.23	0.0	0.78	 d = FV
0.71	0.64	0.44	0.21	92.38	0.13	1.65	3.33	0.32	0.03	0.07	0.1	 e = I
0.71	0.54	0.27	0.15	0.14	87.37	2.12	7.97	0.21	0.23	0.05	0.24	 f = O
2.17	3.49	0.91	1.07	0.47	1.08	74.88	13.89	0.92	0.12	0.08	0.92	 g = SNRTY
0.6	0.69	0.26	0.12	0.17	0.43	2.39	94.9	0.25	0.03	0.03	0.12	 h = 0
1.87	1.7	0.84	0.36	0.69	0.88	6.3	8.15	78.69	0.02	0.19	0.31	 i = BMP
0.89	0.11	0.63	0.3	0.15	1.19	1.64	1.6	0.07	92.92	0.04	0.45	 j = DZ
0.31	0.6	0.34	0.0	0.24	0.46	0.65	1.09	0.46	0.02	95.77	0.05	 k = U
0.92	1.11	0.73	0.28	0.11	0.72	5.92	2.71	0.28	0.15	0.08	86.98	 l = SNRTXY
