Timestamp:2013-07-23-02:58:28
Inventario:I2
Intervalo:6ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I2-6ms-1gramas
Num Instances:  30453
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.2339





=== Stratified cross-validation ===

Correctly Classified Instances       24589               80.7441 %
Incorrectly Classified Instances      5864               19.2559 %
Kappa statistic                          0.757 
K&B Relative Info Score            2204913.2249 %
K&B Information Score                63647.6139 bits      2.09   bits/instance
Class complexity | order 0           87889.7675 bits      2.8861 bits/instance
Class complexity | scheme          1707113.6118 bits     56.0573 bits/instance
Complexity improvement     (Sf)    -1619223.8443 bits    -53.1712 bits/instance
Mean absolute error                      0.0493
Root mean squared error                  0.1548
Relative absolute error                 36.88   %
Root relative squared error             59.8971 %
Coverage of cases (0.95 level)          94.3749 %
Mean rel. region size (0.95 level)      21.6254 %
Total Number of Instances            30453     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,864    0,033    0,833      0,864    0,849      0,820    0,962     0,905     A
                 0,522    0,015    0,640      0,522    0,575      0,559    0,887     0,554     LGJKC
                 0,814    0,021    0,763      0,814    0,788      0,770    0,958     0,832     E
                 0,428    0,003    0,689      0,428    0,528      0,538    0,882     0,494     FV
                 0,796    0,008    0,805      0,796    0,800      0,792    0,963     0,830     I
                 0,756    0,015    0,805      0,756    0,780      0,764    0,941     0,813     O
                 0,613    0,046    0,586      0,613    0,599      0,556    0,921     0,639     SNRTY
                 0,933    0,079    0,880      0,933    0,905      0,845    0,973     0,963     0
                 0,576    0,005    0,794      0,576    0,667      0,667    0,900     0,653     BMP
                 0,574    0,003    0,758      0,574    0,653      0,655    0,945     0,660     DZ
                 0,813    0,002    0,896      0,813    0,853      0,850    0,967     0,880     U
                 0,705    0,008    0,789      0,705    0,744      0,735    0,929     0,738     SNRTXY
Weighted Avg.    0,807    0,044    0,804      0,807    0,804      0,768    0,952     0,844     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  4171    54    73     7    22    73   180   178    14     9     8    36 |     a = A
   114   781    57     7    24    40   211   218    10     6    12    15 |     b = LGJKC
    87    48  1910    13    32    36    84    62     6    16     7    46 |     c = E
    21    20    33   184    10    19    93    38     8     1     1     2 |     d = FV
    22    19    74     5   960     3    38    56    15     2     1    11 |     e = I
   105    32    44     9     5  1702    65   216    22    19    10    22 |     f = O
   211   124    87    17    48    60  1784   511    20     5     4    41 |     g = SNRTY
   123    70    76    11    50    49   335 10828    22    13     6    28 |     h = 0
    56    15    37     8    28    37    98   114   564     2    12     9 |     i = BMP
    38     9    33     3     2    47    17    19     6   260     2    17 |     j = DZ
    14    14    24     0     3    24    17    12    12     2   567     8 |     k = U
    43    35    55     3     9    23   120    58    11     8     3   878 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
86.45	1.12	1.51	0.15	0.46	1.51	3.73	3.69	0.29	0.19	0.17	0.75	 a = A
7.63	52.24	3.81	0.47	1.61	2.68	14.11	14.58	0.67	0.4	0.8	1.0	 b = LGJKC
3.71	2.05	81.38	0.55	1.36	1.53	3.58	2.64	0.26	0.68	0.3	1.96	 c = E
4.88	4.65	7.67	42.79	2.33	4.42	21.63	8.84	1.86	0.23	0.23	0.47	 d = FV
1.82	1.58	6.14	0.41	79.6	0.25	3.15	4.64	1.24	0.17	0.08	0.91	 e = I
4.66	1.42	1.95	0.4	0.22	75.61	2.89	9.6	0.98	0.84	0.44	0.98	 f = O
7.25	4.26	2.99	0.58	1.65	2.06	61.26	17.55	0.69	0.17	0.14	1.41	 g = SNRTY
1.06	0.6	0.65	0.09	0.43	0.42	2.89	93.26	0.19	0.11	0.05	0.24	 h = 0
5.71	1.53	3.78	0.82	2.86	3.78	10.0	11.63	57.55	0.2	1.22	0.92	 i = BMP
8.39	1.99	7.28	0.66	0.44	10.38	3.75	4.19	1.32	57.4	0.44	3.75	 j = DZ
2.01	2.01	3.44	0.0	0.43	3.44	2.44	1.72	1.72	0.29	81.35	1.15	 k = U
3.45	2.81	4.41	0.24	0.72	1.85	9.63	4.65	0.88	0.64	0.24	70.47	 l = SNRTXY
