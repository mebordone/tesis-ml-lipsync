Timestamp:2013-07-22-22:20:28
Inventario:I3
Intervalo:2ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I3-2ms-4gramas
Num Instances:  91700
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1384





=== Stratified cross-validation ===

Correctly Classified Instances       82400               89.8582 %
Incorrectly Classified Instances      9300               10.1418 %
Kappa statistic                          0.8685
K&B Relative Info Score            7695787.5926 %
K&B Information Score               199682.9499 bits      2.1776 bits/instance
Class complexity | order 0          237912.3107 bits      2.5945 bits/instance
Class complexity | scheme          2832782.4681 bits     30.8918 bits/instance
Complexity improvement     (Sf)    -2594870.1575 bits    -28.2974 bits/instance
Mean absolute error                      0.0314
Root mean squared error                  0.1211
Relative absolute error                 22.2823 %
Root relative squared error             45.642  %
Coverage of cases (0.95 level)          97.1505 %
Mean rel. region size (0.95 level)      16.7349 %
Total Number of Instances            91700     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,928    0,013    0,931      0,928    0,929      0,916    0,978     0,956     A
                 0,915    0,007    0,918      0,915    0,917      0,910    0,980     0,946     E
                 0,855    0,049    0,797      0,855    0,825      0,784    0,965     0,887     CGJLNQSRT
                 0,689    0,002    0,854      0,689    0,762      0,764    0,926     0,743     FV
                 0,887    0,003    0,925      0,887    0,905      0,902    0,973     0,926     I
                 0,853    0,005    0,930      0,853    0,890      0,883    0,957     0,900     O
                 0,934    0,051    0,921      0,934    0,928      0,881    0,983     0,966     0
                 0,736    0,003    0,887      0,736    0,804      0,802    0,936     0,810     BMP
                 0,927    0,001    0,970      0,927    0,948      0,947    0,986     0,961     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,544     0,002     DZ
                 0,890    0,001    0,959      0,890    0,923      0,923    0,995     0,962     D
Weighted Avg.    0,899    0,032    0,900      0,899    0,898      0,869    0,974     0,934     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 13294    44   533     4    15    30   376    28     3     0     6 |     a = A
    49  6323   313    15    29    19   121    25     8     0     8 |     b = E
   413   183 14355    72    56   124  1451   102    20     6    11 |     c = CGJLNQSRT
    22    20   272   886    12     9    55     7     2     0     1 |     d = FV
    26    47   199    11  3157     7    92    15     6     0     1 |     e = I
    89    38   271    14    11  5672   514    23     2     0    12 |     f = O
   271   155  1546    33   100   174 33471    63     6     1    10 |     g = 0
    76    44   382     2    23    26   208  2144    10     0     0 |     h = BMP
    13     9    82     0     6    12    20     8  1929     0     1 |     i = U
     0     0    29     0     0     0     2     2     0     0     0 |     j = DZ
    23    24    39     1     5    29    21     0     2     0  1169 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
92.75	0.31	3.72	0.03	0.1	0.21	2.62	0.2	0.02	0.0	0.04	 a = A
0.71	91.51	4.53	0.22	0.42	0.27	1.75	0.36	0.12	0.0	0.12	 b = E
2.46	1.09	85.48	0.43	0.33	0.74	8.64	0.61	0.12	0.04	0.07	 c = CGJLNQSRT
1.71	1.56	21.15	68.9	0.93	0.7	4.28	0.54	0.16	0.0	0.08	 d = FV
0.73	1.32	5.59	0.31	88.65	0.2	2.58	0.42	0.17	0.0	0.03	 e = I
1.34	0.57	4.08	0.21	0.17	85.34	7.73	0.35	0.03	0.0	0.18	 f = O
0.76	0.43	4.31	0.09	0.28	0.49	93.42	0.18	0.02	0.0	0.03	 g = 0
2.61	1.51	13.1	0.07	0.79	0.89	7.14	73.55	0.34	0.0	0.0	 h = BMP
0.63	0.43	3.94	0.0	0.29	0.58	0.96	0.38	92.74	0.0	0.05	 i = U
0.0	0.0	87.88	0.0	0.0	0.0	6.06	6.06	0.0	0.0	0.0	 j = DZ
1.75	1.83	2.97	0.08	0.38	2.21	1.6	0.0	0.15	0.0	89.03	 k = D
