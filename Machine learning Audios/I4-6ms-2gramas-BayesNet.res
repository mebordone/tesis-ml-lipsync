Timestamp:2013-08-30-05:27:15
Inventario:I4
Intervalo:6ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I4-6ms-2gramas
Num Instances:  30452
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(14): class 
0Pitch(Hz)(10): class 
0RawPitch(10): class 
0SmPitch(10): class 
0Melogram(st)(11): class 
0ZCross(13): class 
0F1(Hz)(20): class 
0F2(Hz)(17): class 
0F3(Hz)(14): class 
0F4(Hz)(16): class 
1Int(dB)(16): class 
1Pitch(Hz)(11): class 
1RawPitch(12): class 
1SmPitch(11): class 
1Melogram(st)(14): class 
1ZCross(13): class 
1F1(Hz)(23): class 
1F2(Hz)(18): class 
1F3(Hz)(13): class 
1F4(Hz)(12): class 
class(10): 
LogScore Bayes: -952828.6030646149
LogScore BDeu: -965933.3391498649
LogScore MDL: -964483.6276869659
LogScore ENTROPY: -951119.33013705
LogScore AIC: -953708.33013705




=== Stratified cross-validation ===

Correctly Classified Instances       17885               58.7318 %
Incorrectly Classified Instances     12567               41.2682 %
Kappa statistic                          0.4639
K&B Relative Info Score            1434903.0347 %
K&B Information Score                36842.9235 bits      1.2099 bits/instance
Class complexity | order 0           78169.2083 bits      2.567  bits/instance
Class complexity | scheme           212235.3033 bits      6.9695 bits/instance
Complexity improvement     (Sf)    -134066.095  bits     -4.4025 bits/instance
Mean absolute error                      0.0842
Root mean squared error                  0.2628
Relative absolute error                 54.2259 %
Root relative squared error             94.3555 %
Coverage of cases (0.95 level)          69.0037 %
Mean rel. region size (0.95 level)      16.4725 %
Total Number of Instances            30452     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,400    0,027    0,083      0,400    0,138      0,172    0,892     0,118     C
                 0,644    0,098    0,355      0,644    0,458      0,420    0,884     0,459     E
                 0,142    0,012    0,144      0,142    0,143      0,131    0,803     0,082     FV
                 0,584    0,077    0,651      0,584    0,616      0,528    0,881     0,703     AI
                 0,140    0,041    0,392      0,140    0,206      0,157    0,798     0,367     CDGKNRSYZ
                 0,404    0,040    0,443      0,404    0,423      0,379    0,842     0,425     O
                 0,907    0,153    0,785      0,907    0,842      0,738    0,960     0,954     0
                 0,038    0,012    0,106      0,038    0,055      0,042    0,773     0,095     LT
                 0,562    0,026    0,336      0,562    0,421      0,418    0,932     0,495     U
                 0,168    0,019    0,228      0,168    0,194      0,173    0,795     0,150     MBP
Weighted Avg.    0,587    0,092    0,572      0,587    0,563      0,491    0,889     0,649     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
    74    58     0    24     7     4    11     0     3     4 |     a = C
   154  1512    11   253   111    69    97    52    44    44 |     b = E
    21    92    61    45    29    34   105    11    12    20 |     c = FV
   207   933    65  3524   304   311   327    68   123   169 |     d = AI
   212   733   134   706   673   320  1564   124   185   151 |     e = CDGKNRSYZ
    84   263    14   391   108   909   241    21   202    18 |     f = O
    22   225    65   229   258   145 10533    40    39    54 |     g = 0
    24   212    28    89   143   107   347    42    59    68 |     h = LT
    27    66     9    42    19    89    14     7   392    32 |     i = U
    62   168    37   111    67    63   171    30   106   165 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
40.0	31.35	0.0	12.97	3.78	2.16	5.95	0.0	1.62	2.16	 a = C
6.56	64.42	0.47	10.78	4.73	2.94	4.13	2.22	1.87	1.87	 b = E
4.88	21.4	14.19	10.47	6.74	7.91	24.42	2.56	2.79	4.65	 c = FV
3.43	15.47	1.08	58.43	5.04	5.16	5.42	1.13	2.04	2.8	 d = AI
4.41	15.26	2.79	14.7	14.01	6.66	32.57	2.58	3.85	3.14	 e = CDGKNRSYZ
3.73	11.68	0.62	17.37	4.8	40.38	10.71	0.93	8.97	0.8	 f = O
0.19	1.94	0.56	1.97	2.22	1.25	90.72	0.34	0.34	0.47	 g = 0
2.14	18.95	2.5	7.95	12.78	9.56	31.01	3.75	5.27	6.08	 h = LT
3.87	9.47	1.29	6.03	2.73	12.77	2.01	1.0	56.24	4.59	 i = U
6.33	17.14	3.78	11.33	6.84	6.43	17.45	3.06	10.82	16.84	 j = MBP
