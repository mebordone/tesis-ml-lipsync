Timestamp:2013-07-22-23:05:35
Inventario:I4
Intervalo:10ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I4-10ms-5gramas
Num Instances:  18292
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  43 4RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  44 4SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  45 4Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  47 4F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  49 4F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  50 4F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.2825





=== Stratified cross-validation ===

Correctly Classified Instances       14314               78.2528 %
Incorrectly Classified Instances      3978               21.7472 %
Kappa statistic                          0.7185
K&B Relative Info Score            1224088.1426 %
K&B Information Score                31622.7658 bits      1.7288 bits/instance
Class complexity | order 0           47237.6759 bits      2.5824 bits/instance
Class complexity | scheme           795178.0908 bits     43.4714 bits/instance
Complexity improvement     (Sf)    -747940.4148 bits    -40.8889 bits/instance
Mean absolute error                      0.0673
Root mean squared error                  0.1787
Relative absolute error                 43.1199 %
Root relative squared error             63.9707 %
Coverage of cases (0.95 level)          96.0201 %
Mean rel. region size (0.95 level)      27.5077 %
Total Number of Instances            18292     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,533    0,001    0,731      0,533    0,616      0,622    0,943     0,597     C
                 0,752    0,031    0,672      0,752    0,710      0,685    0,952     0,745     E
                 0,299    0,002    0,639      0,299    0,407      0,432    0,893     0,360     FV
                 0,841    0,068    0,759      0,841    0,797      0,745    0,951     0,870     AI
                 0,727    0,081    0,631      0,727    0,676      0,611    0,919     0,709     CDGKNRSYZ
                 0,650    0,017    0,753      0,650    0,698      0,677    0,928     0,722     O
                 0,918    0,053    0,911      0,918    0,915      0,864    0,975     0,958     0
                 0,323    0,007    0,654      0,323    0,433      0,446    0,876     0,405     LT
                 0,638    0,003    0,849      0,638    0,729      0,731    0,958     0,780     U
                 0,409    0,005    0,743      0,409    0,528      0,541    0,910     0,543     MBP
Weighted Avg.    0,783    0,051    0,783      0,783    0,777      0,734    0,948     0,817     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   57   12    1   21    9    1    4    0    1    1 |    a = C
    9 1081    3  158  129   15   28    7    2    6 |    b = E
    0   24   78   35   91   12   11    3    4    3 |    c = FV
    3  154    6 3100  236   47   90   21   10   21 |    d = AI
    2  132   13  311 2132   75  208   35    6   19 |    e = CDGKNRSYZ
    3   68    3  134  141  895  104    8    8   13 |    f = O
    2   47    9  113  306   43 6239   22    6    7 |    g = 0
    0   43    3   96  182   19   99  219    6   10 |    h = LT
    0   15    0   35   35   48   10    6  270    4 |    i = U
    2   33    6   84  116   34   57   14    5  243 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
53.27	11.21	0.93	19.63	8.41	0.93	3.74	0.0	0.93	0.93	 a = C
0.63	75.17	0.21	10.99	8.97	1.04	1.95	0.49	0.14	0.42	 b = E
0.0	9.2	29.89	13.41	34.87	4.6	4.21	1.15	1.53	1.15	 c = FV
0.08	4.18	0.16	84.06	6.4	1.27	2.44	0.57	0.27	0.57	 d = AI
0.07	4.5	0.44	10.6	72.69	2.56	7.09	1.19	0.2	0.65	 e = CDGKNRSYZ
0.22	4.94	0.22	9.73	10.24	65.0	7.55	0.58	0.58	0.94	 f = O
0.03	0.69	0.13	1.66	4.5	0.63	91.83	0.32	0.09	0.1	 g = 0
0.0	6.35	0.44	14.18	26.88	2.81	14.62	32.35	0.89	1.48	 h = LT
0.0	3.55	0.0	8.27	8.27	11.35	2.36	1.42	63.83	0.95	 i = U
0.34	5.56	1.01	14.14	19.53	5.72	9.6	2.36	0.84	40.91	 j = MBP
