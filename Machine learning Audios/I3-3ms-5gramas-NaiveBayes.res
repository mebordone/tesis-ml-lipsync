Timestamp:2014-12-07-11:08:48
Inventario:I3
Intervalo:3ms
Ngramas:5
Clasificador:NaiveBayes
Relation Name:  I3-3ms-5gramas
Num Instances:  61131
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Naive Bayes Classifier

                    Class
Attribute               A         E CGJLNQSRT        FV         I         O         0       BMP         U        DZ         D
                   (0.16)    (0.08)    (0.18)    (0.01)    (0.04)    (0.07)    (0.39)    (0.03)    (0.02)       (0)    (0.01)
==============================================================================================================================
0Int(dB)
  mean             -4.4631   -5.7241  -16.6532  -14.6126   -9.4261   -8.4226    -48.04  -13.1152   -6.2108  -27.1406   -7.8499
  std. dev.         9.9613    8.6737   11.0529    9.2915    8.7661   12.1005    16.606   11.3516    9.0317    2.1747    6.0046
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044

0Pitch(Hz)
  mean            214.0682  185.9586  117.1424  137.4946  168.4152  178.4602   17.8444  158.8832  224.0701         0  202.5863
  std. dev.       109.2702   90.1831  112.7497  108.1126   80.7025  106.9431   58.0865  112.4924   92.7797    0.0217   83.2977
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303

0RawPitch
  mean            203.5855  181.3123   99.7687  111.9267   159.707  171.3618   14.7579  146.1472  215.2186         0  196.6253
  std. dev.       115.1157    96.693  112.3365  105.4368   86.3426  113.5595   52.6837  116.9224   99.6003    0.0218     93.42
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision          0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131

0SmPitch
  mean            211.8753  183.6469  110.9268  127.3698  165.7532  176.0047   16.2046  155.5195  223.7517         0  201.5589
  std. dev.        111.039   92.0569  112.9458  105.9307   82.6657  109.2519    54.355   114.372   92.6385    0.0219   85.3024
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312

0Melogram(st)
  mean             50.8757   49.4669     30.91   38.0803   47.1058   46.3882    5.1991   41.4956   52.2659         0   52.9371
  std. dev.        17.6121   16.7812   27.2705     24.59   17.9805   20.7426   15.9975   24.3973   17.2831    0.0042    11.592
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252

0ZCross
  mean              6.9462    4.9169    2.9198    2.0505    3.5381    4.0022    0.2872    2.3289     3.713         0    3.6509
  std. dev.         4.6226    3.3248    4.7301    2.1188    3.3145     3.118    1.2752    2.1747    2.8833    0.1939    2.1087
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633

0F1(Hz)
  mean            659.2239  448.4125  331.9864  411.1787  346.5478  467.7058   58.0026  378.8006  478.6311         0  515.0145
  std. dev.       287.4054   166.292  281.7826  274.3057  164.1875  236.8053  173.4388  239.9247    325.23    0.0607  150.5296
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642

0F2(Hz)
  mean           1416.0808  1711.214  1061.285  1217.474 1845.7313 1125.8145  180.0747 1326.7322 1005.5905         0  1487.742
  std. dev.       499.3086  608.9906  858.2314  757.4226   773.683  570.3838  532.2925  756.1536   487.338    0.0738  305.0599
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431

0F3(Hz)
  mean           2601.1736 2531.3885 1804.2203  2080.908 2511.4671 2433.4936  307.8348 2230.2399 2536.8144         0 2876.0182
  std. dev.       886.6702   899.016 1433.0367 1279.4794 1065.3826 1084.1339  893.0846 1210.9903  969.7241      0.11  451.3023
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602

0F4(Hz)
  mean           3020.7731 3059.3156 2063.3024 2472.1615 2996.5825 2805.3088  369.3315 2653.8723 2908.3713         0 3339.2088
  std. dev.      1016.6813 1056.7202 1628.6158 1487.4598  1233.239 1235.5521 1069.3144 1411.1213 1040.3379    0.1129   485.379
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775

1Int(dB)
  mean             -4.3451   -5.4866  -16.7066   -14.804   -9.1799   -8.2804  -48.1807  -13.0129   -5.8577  -27.1225   -7.9081
  std. dev.         9.7988    8.4392   10.8166    9.1123    8.6146   12.0927   16.4087   11.1545    8.6173    2.1763    6.0541
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044

1Pitch(Hz)
  mean            214.8348  187.5772  116.0918  135.8817  170.3677  179.1099   17.3506  158.7755  226.2021         0  202.3699
  std. dev.       108.6313   89.3537  112.7788  107.8471   79.1235  106.1835   57.3369  112.1053   89.9667    0.0217   83.7255
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303

1RawPitch
  mean            204.4046  183.2465   98.2635  109.9703  162.0978  172.4006   14.2573  146.1731  217.6541         0   196.583
  std. dev.       114.5571   95.6687  112.0897  104.6772   85.0721  112.8726   51.6702  116.9415   96.8389    0.0218   93.9793
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision          0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131

1SmPitch
  mean            212.6964  185.6219  109.7432  125.7035  167.7906  176.7887   15.6887  154.8711  225.9095         0  201.4721
  std. dev.       110.3732    90.965  112.9104  105.4709   81.2541  108.4257   53.4945  114.2773   89.8265    0.0219   85.4827
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312

1Melogram(st)
  mean             51.1094   49.8961   30.5385   37.8284   47.7099   46.6075    5.0779   41.3347   52.9335         0   52.9623
  std. dev.        17.2871   16.2028   27.3266   24.6828   17.2872   20.5184   15.8345   24.4629   16.2835    0.0042   11.4775
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252

1ZCross
  mean              6.9827    4.9388    2.9176    2.0007    3.5626    4.0249    0.2622    2.3235     3.755         0    3.6535
  std. dev.         4.6044    3.2469    4.8034    2.0822    3.2866    3.0126    1.1593     2.129    2.8542    0.1939    2.1151
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633

1F1(Hz)
  mean            666.7136  453.7041  324.2888  403.0001  351.6224  473.8855   56.1656  374.9867  484.7897         0  515.3225
  std. dev.       281.5906  159.4091  281.0242  276.2591  157.9743  232.8436  170.5763  239.5169  320.1911    0.0607  151.6572
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642

1F2(Hz)
  mean           1426.5335 1736.7393  1042.159 1201.2387 1888.1428 1133.1742  174.9481 1317.4527 1019.5348         0 1485.0866
  std. dev.       483.8135  582.7337  860.8642  764.2508  742.2294  558.0272   525.318  757.5471  469.8361    0.0738  311.0098
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431

1F3(Hz)
  mean            2621.437 2562.5981 1772.5428 2053.3107 2559.2594  2458.752  299.0333 2214.3535 2579.3441         0 2871.6844
  std. dev.       860.1173   862.118 1439.4847 1292.4335 1021.4692 1061.5374  881.6893 1214.8583  919.5861      0.11  470.7591
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602

1F4(Hz)
  mean           3043.6864 3093.8052  2027.709  2442.436 3050.9117 2833.3702  358.9847 2641.6647 2956.7717         0 3333.3219
  std. dev.       985.2717 1009.5134 1636.9375 1504.0848 1178.4462 1207.9571 1056.2377 1419.2169   976.944    0.1129  510.4315
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775

2Int(dB)
  mean              -4.275   -5.3093  -16.7074   -14.929   -8.9731    -8.186  -48.3098  -12.8391   -5.5524  -27.1225   -7.9123
  std. dev.         9.6881    8.2373    10.603    8.9669    8.4929   12.1008   16.2247   10.9656    8.2298    2.1763    6.1046
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044

2Pitch(Hz)
  mean            215.2871  188.9058  115.6717  134.5242  172.0827  179.2936   16.8275  158.9902  228.1408         0  202.1732
  std. dev.       108.2446   88.6428  112.8573  107.5224   77.7161  105.6064   56.5405  111.5792   87.2319    0.0217   84.1187
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303

2RawPitch
  mean            204.9344  184.5166   97.4718  108.9501   164.127  173.0126   13.8002  146.3863  219.2103         0  196.0755
  std. dev.       114.2089   95.0659  111.9292  104.2181    83.993  112.4306   50.8241  116.5641   94.8961    0.0218    93.992
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision          0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131

2SmPitch
  mean              213.21  187.1933  109.1153  124.4425  169.4288  177.1572   15.1843  154.8208  227.7542         0   201.397
  std. dev.       109.9155   90.0322  112.9484  105.0137   80.1407  107.7873    52.656  113.9964   87.2767    0.0219   85.6399
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312

2Melogram(st)
  mean             51.2709   50.2427     30.34   37.5734   48.1463   46.6963    4.9554   41.2774   53.5545         0   52.9912
  std. dev.        17.0469   15.7114   27.3686   24.7697    16.741    20.402   15.6692   24.4581    15.256    0.0042     11.36
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252

2ZCross
  mean              7.0076    4.9486    2.9169    1.9576    3.5738    4.0411    0.2433    2.3403    3.7945         0    3.6786
  std. dev.         4.5938    3.2071    4.8518    2.0438    3.2163    2.9298    1.0804    2.1047    2.8295    0.1939    2.1134
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633

2F1(Hz)
  mean            673.1337  458.5411  317.4455  395.5357  356.2706  479.3315   54.3918  373.5242  491.2079         0  515.5991
  std. dev.       276.5723  152.8966  280.1108  277.2043   151.878  229.0643  167.8415  238.3426  314.7115    0.0607  152.8143
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642

2F2(Hz)
  mean           1434.8492 1759.9732 1025.2119 1189.2807 1927.6297  1139.398  169.8362 1314.2158 1035.0423         0 1483.1122
  std. dev.       470.4865  557.1365  862.5901  769.1361  710.1468  546.4539  518.3893  754.2105  450.7523    0.0738  317.3199
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431

2F3(Hz)
  mean           2637.8989 2590.6505 1744.6737 2031.3736 2603.7866 2481.3348  290.0758 2210.1004 2624.5033         0 2867.7838
  std. dev.       837.7663   826.473 1444.4359 1301.0849   977.692 1040.3144  869.7877 1211.2692  861.9955      0.11  489.8996
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602

2F4(Hz)
  mean           3061.8157 3124.7953 1996.7914 2420.8547 3100.6951 2858.4583  348.5398 2643.1324 3006.9754         0 3327.0352
  std. dev.       958.7356  964.1286 1643.8216 1516.4414 1123.6536 1181.9837 1042.8301 1417.9475  902.9144    0.1129  534.3496
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775

3Int(dB)
  mean             -4.2584   -5.2147  -16.6403  -14.9846    -8.825   -8.1561  -48.4237  -12.5947   -5.2953  -27.1089   -7.8485
  std. dev.         9.6363    8.1108   10.4295    8.8593    8.4293   12.1343   16.0588   10.7736    7.8625    2.1562    6.1433
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044    0.1044

3Pitch(Hz)
  mean             215.424  190.0809  115.9907  133.2458  173.0172    179.07   16.2557  159.7556   229.415         0  202.0027
  std. dev.       108.1379   87.8538  113.0136  107.2688   76.7882  105.2101   55.6504  110.8403   85.2349    0.0217   84.4676
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303

3RawPitch
  mean             205.191  185.5537   97.2843  108.1776  165.5748  172.9408    13.331  147.2848  220.5485         0   195.986
  std. dev.       114.0304   94.6689  111.8701  103.4633   83.1744  112.1655   50.0238  115.9925   93.0837    0.0218   94.0584
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision          0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131

3SmPitch
  mean            213.3805  188.4788  109.2749  123.2631  170.4827   176.912   14.6616   155.285  229.0404         0  201.3343
  std. dev.        109.759   89.1689  113.0944  104.6396   79.2893  107.4207   51.7924  113.4942   85.2781    0.0219   85.7678
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312

3Melogram(st)
  mean             51.3557   50.4808   30.3377   37.3343    48.407    46.671    4.8142   41.4703   54.1306         0   52.9769
  std. dev.        16.9062   15.3557   27.3968   24.8638   16.3656   20.3864   15.4731   24.3086   14.2012    0.0042    11.374
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252

3ZCross
  mean              7.0158    4.9388     2.927    1.9347    3.5777    4.0484    0.2298    2.3719    3.8272         0    3.7314
  std. dev.         4.5915    3.1975     4.869    2.0238    3.1538    2.8985    1.0465    2.0915    2.8121    0.1939     2.116
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633

3F1(Hz)
  mean            678.6538  462.8688  311.9398  388.2589  360.2165   483.672   52.7265  372.4682  496.5514         0  516.4354
  std. dev.       272.3175  146.9719  279.0217  277.0199    146.32  226.1449  165.2548  237.0685  309.2602    0.0607  152.9405
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642

3F2(Hz)
  mean           1441.2512 1781.0424 1011.7706 1180.6948 1961.8033 1142.8991  165.0589  1312.066 1050.0402         0 1482.7607
  std. dev.       459.1186  532.5896  863.2353  774.0059  680.1225  536.7971  511.9666  750.4882  432.2873    0.0738  319.5763
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431

3F3(Hz)
  mean           2651.2499 2615.4812  1722.968 2012.5832 2642.4014 2498.5172  281.5968  2207.248 2665.1247         0 2867.2974
  std. dev.       819.3519  792.6591 1447.4972  1307.812   937.499 1024.1918  858.2952 1206.6787  803.6971      0.11  499.4354
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602

3F4(Hz)
  mean           3075.9587 3152.5297 1973.3713 2403.3745 3143.1187 2876.7808  338.6077 2646.3634 3052.6473         0  3324.975
  std. dev.       936.7556  921.2174 1648.9047 1526.7311 1073.3319 1161.8705 1029.7278 1415.4829  827.1813    0.1129  545.8289
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775

4Int(dB)
  mean             -4.2995   -5.2154  -16.4957  -14.9663    -8.745    -8.205  -48.5189  -12.2908   -5.0964   -27.151   -7.7207
  std. dev.         9.6425     8.061   10.3102    8.8198    8.4284   12.2015   15.9252   10.5869    7.5403    2.2431    6.1865
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043    0.1043

4Pitch(Hz)
  mean            215.2171  190.6854  116.9511  132.7243  173.6379  178.3171   15.7183  161.0025   230.256         0  201.8505
  std. dev.       108.2936   87.4198  113.2594  107.1636   76.0641  105.1185   54.8033  109.8138   83.5916    0.0217   84.7618
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303    0.1303

4RawPitch
  mean             205.011  186.0468   98.0304  107.6938  166.4491  172.3446   12.8482  148.5952   221.377         0  195.7565
  std. dev.       114.2273   94.2836  112.1092  102.8608   82.7406   111.886   49.2604  115.2511   91.8259    0.0218    93.879
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision          0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131     0.131

4SmPitch
  mean             213.279  189.2421  110.0284  123.0119  171.0176  176.2127   14.1449  156.3345   229.966         0  201.2862
  std. dev.       109.7994   88.6477  113.3485  104.4081   78.8205  107.2818   50.9173  112.6809   83.6261    0.0219   85.8449
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312    0.1312

4Melogram(st)
  mean             51.3572   50.6147   30.4901   37.2513   48.6212   46.5702    4.6716   41.7583   54.4863         0   52.9664
  std. dev.        16.8758   15.1287   27.4186    24.928   16.0378    20.439   15.2691   24.1126   13.4335    0.0042   11.3858
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252    0.0252

4ZCross
  mean              6.9994    4.9156    2.9472    1.9401    3.5967    4.0424    0.2221    2.4227    3.8515         0    3.8027
  std. dev.         4.6018    3.2034     4.867    2.0364    3.1513    2.9091    1.0405    2.0849    2.7932    0.1939    2.1182
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633    1.1633

4F1(Hz)
  mean             683.313  466.9426  307.8306  381.3156  363.5983  486.8876   50.9791  372.4935  501.4313         0  517.5707
  std. dev.       268.9093  141.0764  277.5772  276.5899  141.2908  224.2029  162.6227  235.5015  304.2361    0.0607   152.918
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642    0.3642

4F2(Hz)
  mean           1445.9618 1800.7234 1003.2724 1172.6362 1991.3969 1143.6165  159.7147 1312.5688 1063.4653         0 1482.7798
  std. dev.       449.7706  507.5736  863.2089  779.2046  651.9928  529.3781  504.5122  745.4148  414.3404    0.0738    322.46
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431    0.4431

4F3(Hz)
  mean           2661.7097  2638.722 1709.2151 1994.0357 2676.1136 2509.8227  272.3289 2208.9124  2701.015         0 2866.4836
  std. dev.       804.8708  758.2975 1448.9435 1314.2962  900.3451 1013.4676  845.5437 1199.4463  745.1651      0.11  508.9155
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602    0.6602

4F4(Hz)
  mean           3086.6146  3179.034 1959.3865 2385.6763 3179.5458  2888.156   327.666 2654.3032 3093.8479         0 3322.1873
  std. dev.       919.3099  877.8457 1652.3781 1536.6667  1026.953 1148.0627  1014.973 1409.2849  750.1771    0.1129  556.6252
  weight sum          9585      4621     11256       864      2382      4463     23722      1948      1386        23       881
  precision         0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775    0.6775





=== Stratified cross-validation ===

Correctly Classified Instances       19859               32.486  %
Incorrectly Classified Instances     41272               67.514  %
Kappa statistic                          0.2383
K&B Relative Info Score            1268216.6298 %
K&B Information Score                32976.5128 bits      0.5394 bits/instance
Class complexity | order 0          158931.7841 bits      2.5999 bits/instance
Class complexity | scheme          5562533.2129 bits     90.9937 bits/instance
Complexity improvement     (Sf)    -5403601.4288 bits    -88.3938 bits/instance
Mean absolute error                      0.1227
Root mean squared error                  0.3411
Relative absolute error                 86.9072 %
Root relative squared error            128.3807 %
Coverage of cases (0.95 level)          36.9845 %
Mean rel. region size (0.95 level)      11.3243 %
Total Number of Instances            61131     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,392    0,036    0,670      0,392    0,495      0,449    0,852     0,569     A
                 0,270    0,028    0,442      0,270    0,335      0,305    0,837     0,339     E
                 0,137    0,027    0,535      0,137    0,218      0,201    0,643     0,375     CGJLNQSRT
                 0,068    0,033    0,029      0,068    0,041      0,023    0,651     0,027     FV
                 0,314    0,025    0,334      0,314    0,324      0,298    0,855     0,264     I
                 0,005    0,003    0,124      0,005    0,010      0,011    0,724     0,156     O
                 0,479    0,034    0,898      0,479    0,624      0,535    0,845     0,810     0
                 0,010    0,006    0,055      0,010    0,017      0,010    0,668     0,053     BMP
                 0,307    0,010    0,424      0,307    0,356      0,349    0,859     0,317     U
                 1,000    0,225    0,002      1,000    0,003      0,036    0,891     0,002     DZ
                 0,753    0,281    0,038      0,753    0,072      0,124    0,830     0,051     D
Weighted Avg.    0,325    0,032    0,620      0,325    0,409      0,361    0,792     0,530     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  3757   337   293   269   143    60   144    34    98   350  4100 |     a = A
   393  1247   170   123   225     5    37    43    55   146  2177 |     b = E
   498   333  1539   737   505    66   868   105    92  2429  4084 |     c = CGJLNQSRT
    57    27    58    59    38     2    45     5     2   161   410 |     d = FV
   121   583   128    77   749     0    43    38     6    93   544 |     e = I
   413   116   153   236    50    24    47    39   232   426  2727 |     f = O
    57   111   357   351   275    28 11353    43    32  9830  1285 |     g = 0
   114    35   103   103   199     3    80    19    39   275   978 |     h = BMP
   107     8    62    45    36     6    13    17   426    18   648 |     i = U
     0     0     0     0     0     0     0     0     0    23     0 |     j = DZ
    90    22    15    35    20     0     8     4    22     2   663 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
39.2	3.52	3.06	2.81	1.49	0.63	1.5	0.35	1.02	3.65	42.78	 a = A
8.5	26.99	3.68	2.66	4.87	0.11	0.8	0.93	1.19	3.16	47.11	 b = E
4.42	2.96	13.67	6.55	4.49	0.59	7.71	0.93	0.82	21.58	36.28	 c = CGJLNQSRT
6.6	3.13	6.71	6.83	4.4	0.23	5.21	0.58	0.23	18.63	47.45	 d = FV
5.08	24.48	5.37	3.23	31.44	0.0	1.81	1.6	0.25	3.9	22.84	 e = I
9.25	2.6	3.43	5.29	1.12	0.54	1.05	0.87	5.2	9.55	61.1	 f = O
0.24	0.47	1.5	1.48	1.16	0.12	47.86	0.18	0.13	41.44	5.42	 g = 0
5.85	1.8	5.29	5.29	10.22	0.15	4.11	0.98	2.0	14.12	50.21	 h = BMP
7.72	0.58	4.47	3.25	2.6	0.43	0.94	1.23	30.74	1.3	46.75	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	 j = DZ
10.22	2.5	1.7	3.97	2.27	0.0	0.91	0.45	2.5	0.23	75.26	 k = D
