Timestamp:2014-12-07-11:06:16
Inventario:I3
Intervalo:2ms
Ngramas:3
Clasificador:NaiveBayes
Relation Name:  I3-2ms-3gramas
Num Instances:  91701
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Naive Bayes Classifier

                    Class
Attribute               A         E CGJLNQSRT        FV         I         O         0       BMP         U        DZ         D
                   (0.16)    (0.08)    (0.18)    (0.01)    (0.04)    (0.07)    (0.39)    (0.03)    (0.02)       (0)    (0.01)
==============================================================================================================================
0Int(dB)
  mean             -4.2251    -5.205  -16.6744  -15.0221   -8.7895   -8.1324  -48.1799  -12.6603   -5.3672   -26.973   -7.8714
  std. dev.         9.6253    8.1176   10.4482    8.9257    8.3703   12.1307   16.3014   10.8312     8.007    2.1072    6.1238
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision          0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104     0.104

0Pitch(Hz)
  mean            215.3388  189.4672  114.7461   131.712  172.4288  178.9677   17.0609  158.9952  228.8154         0  202.3729
  std. dev.       108.3673   88.3909  113.0138  107.4401   77.1572  105.4237    56.853   111.641   85.6072    0.0207   84.3095
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242

0RawPitch
  mean            204.8262  185.2835   96.3608  106.7449  164.8572  173.0225   13.9619  146.6041  219.9054         0  196.0699
  std. dev.       114.8351   95.4466  112.1859  103.7582   83.8605  112.8916   51.2541  116.7477   93.8544    0.0207   94.5325
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243

0SmPitch
  mean            213.2942  187.8876  108.1541  121.4041  170.0743  176.8295   15.4228  154.5794  228.2489         0  201.7462
  std. dev.       110.0116   89.6855  113.0596  104.6191   79.5741  107.6308    53.067  114.1868   85.8949    0.0208   85.5321
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248

0Melogram(st)
  mean             51.2738    50.296   30.0127   37.1801   48.3177   46.5862    5.0216   41.2827   53.7993         0   53.0238
  std. dev.        17.0546   15.6299   27.4226   24.9375   16.4721   20.4797   15.7545   24.4294     14.79    0.0041   11.3416
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246

0ZCross
  mean              7.1301    5.2767    3.0165     2.069    3.7235    4.4336    0.2613    2.4852    4.0842         0    4.0832
  std. dev.         4.5874     3.209    4.9351    2.2383    3.2289    3.0623    1.1465    2.2611    2.9396    0.1792    2.3026
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755

0F1(Hz)
  mean            677.0705  461.0472  310.8017  388.0168  359.1654  481.7752    54.983  372.2266  494.6223         0  516.9777
  std. dev.       274.1678  149.9473  279.3941   278.164  147.6819  227.7162  168.4324  238.0257   311.451    0.0608  153.3857
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648

0F2(Hz)
  mean           1438.0676 1772.0574 1007.7614 1173.9193 1952.7441 1139.4043  171.9857 1309.0897 1043.9884         0 1484.0198
  std. dev.       463.2855  544.5249   864.368  775.1169  688.7429  541.1664  521.2356  752.5784  438.5623    0.0738  319.4464
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429

0F3(Hz)
  mean           2645.3531 2603.6899 1716.1767 2003.0171 2631.3817 2490.7242  293.4267 2203.0948 2648.1475         0 2869.5564
  std. dev.       826.9081  809.0505 1449.4623 1310.3914  948.5385 1033.2776  874.0473 1210.0929  826.0863    0.1103  497.3337
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision          0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662

0F4(Hz)
  mean           3069.6694 3139.0513 1964.4989 2393.9651 3132.5813 2866.8671  352.5404 2641.4508 3035.0863         0 3323.5678
  std. dev.        945.882  942.3082 1650.1559 1531.3561 1087.3648 1172.3377 1047.8436 1419.3848  857.2082    0.1138  543.3417
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829

1Int(dB)
  mean             -4.2367   -5.1801  -16.5949  -15.0289   -8.7172    -8.146  -48.2527  -12.4737   -5.2139  -26.9195   -7.7997
  std. dev.         9.6141     8.069   10.3539    8.8787    8.3524   12.1698   16.2023   10.7103    7.7793    2.0302    6.1523
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039    0.1039

1Pitch(Hz)
  mean            215.3054  190.0868  115.2128  131.0502  173.0539  178.5997   16.6926  159.5378  229.3808         0  202.2726
  std. dev.       108.4186   87.9634  113.1551  107.2576   76.5412   105.265   56.2804  111.1102    84.526    0.0207   84.5208
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242

1RawPitch
  mean            204.9204  185.5725   96.5081  106.2993  165.7052  172.7286   13.6787   147.555  220.4839         0  195.9629
  std. dev.        114.845   95.2181  112.2628  103.3674   83.3185  112.6688   50.8131  116.3293   93.0053    0.0207    94.473
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243

1SmPitch
  mean            213.3064  188.5362  108.5296  120.8089  170.6841  176.5062   15.0751  154.9178  228.8712         0  201.7143
  std. dev.       109.9963   89.2414  113.2143  104.3726   79.0929  107.4391   52.4896  113.8128   84.8121    0.0208   85.5991
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248

1Melogram(st)
  mean             51.3027   50.4037   30.0627   37.0227   48.4912   46.5516    4.9295    41.476   54.0704         0   53.0165
  std. dev.        16.9973   15.4597   27.4399   25.0004   16.2162    20.484   15.6282   24.3018   14.2652    0.0041   11.3502
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246

1ZCross
  mean              7.1259    5.2659    3.0292    2.0682    3.7314    4.4315    0.2535     2.518    4.0992         0    4.1291
  std. dev.         4.5903    3.2124    4.9373    2.2445    3.2172    3.0586    1.1335    2.2548    2.9273    0.1792    2.2988
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755

1F1(Hz)
  mean            680.5164   463.877  307.6156  383.2741  361.6146  484.2054   53.8376  371.8273  498.0573         0  517.6211
  std. dev.       271.5055   146.053   278.497  277.9272  144.1442  226.2125  166.7243   237.112   307.944    0.0608  153.3903
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648

1F2(Hz)
  mean           1442.0523  1785.616 1000.5225 1168.3225 1974.0792 1140.4987   168.567 1308.1584  1053.554         0 1484.0387
  std. dev.       456.0769  528.1297  864.4378  778.4294  669.1502  535.6978   516.644   749.768  426.6176    0.0738  321.3149
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429

1F3(Hz)
  mean           2653.6177 2619.8245 1704.5582 1990.5358 2655.6632  2499.568  287.4269 2202.5227  2673.852         0 2868.9125
  std. dev.       815.2889  786.5019 1450.8094 1314.6764  922.4738 1024.8703  866.0225 1206.5921  787.5699    0.1103  503.7764
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision          0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662

1F4(Hz)
  mean           3078.5421 3157.1809 1952.1789 2382.1202 3159.0187 2876.1426  345.4959 2644.3886  3063.919         0  3321.865
  std. dev.       932.0325  913.7543 1652.7631 1537.9929 1054.7924 1161.7476 1038.6519 1416.7903  806.6532    0.1138  550.7347
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829

2Int(dB)
  mean             -4.2733    -5.199  -16.4813  -15.0003    -8.676   -8.1936   -48.307  -12.2641   -5.0927  -26.9132   -7.7016
  std. dev.         9.6287    8.0531   10.2875    8.8607    8.3577   12.2208   16.1116   10.5968    7.5769    1.9983     6.187
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037    0.1037

2Pitch(Hz)
  mean            215.1446  190.3791  115.9271  130.8258  173.4856  178.0537   16.3387  160.3717  229.8416         0  202.2426
  std. dev.       108.5552   87.7367  113.3329  107.1381   76.0683  105.2226   55.7285  110.4325   83.5753    0.0207   84.5819
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242    0.1242

2RawPitch
  mean            204.7828   185.837   97.0368  105.9763  166.2787  172.2852   13.3731  148.5095  220.9278         0  195.8324
  std. dev.       114.9802   95.0002  112.4475  102.9581   83.0617  112.5036   50.3492  115.8451   92.1417    0.0207    94.326
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243    0.1243

2SmPitch
  mean            213.2239  188.9642  109.1119  120.6473  171.0525  175.9682   14.7411  155.6124  229.2536         0  201.7614
  std. dev.       110.0447   88.9263  113.4077  104.2291   78.7886  107.3853   51.9302  113.2741   84.0032    0.0208   85.5097
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248    0.1248

2Melogram(st)
  mean             51.2983   50.4842   30.1853   37.0069   48.6341   46.4749    4.8255   41.6893   54.3072         0   53.0765
  std. dev.        16.9875   15.3213   27.4518   25.0249   15.9951   20.5262   15.4833   24.1614   13.7628    0.0041   11.1742
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246    0.0246

2ZCross
  mean               7.111    5.2438    3.0478    2.0698    3.7483    4.4237    0.2484    2.5583    4.1111         0    4.1913
  std. dev.         4.6004    3.2168     4.934    2.2498    3.2275    3.0692    1.1311    2.2519    2.9164    0.1792    2.3027
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755    1.0755

2F1(Hz)
  mean            683.4195  466.4894  305.1041  378.5922    363.75  486.2046   52.7343   372.086  500.6341         0  518.4309
  std. dev.        269.455  142.2388   277.473  277.5826  140.8431  225.0865   165.102  236.0116  305.2064    0.0608  153.3851
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648    0.3648

2F2(Hz)
  mean           1444.7727 1798.4442  995.4145 1163.3139  1993.047 1140.7371  165.1204 1309.1173 1061.2779         0 1484.1075
  std. dev.       450.4532  511.8666  864.2114   782.141  650.7685  531.1147   511.871    746.06  416.4942    0.0738  323.4684
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429    0.4429

2F3(Hz)
  mean           2659.8447 2634.9785 1696.3721 1978.2255 2677.3565 2506.4348  281.5324 2204.7048 2694.0925         0 2868.2959
  std. dev.       806.7783  764.1593 1451.3307 1318.9077  898.3116 1018.6433  858.0649 1201.2074  755.1652    0.1103  510.0445
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision          0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662     0.662

2F4(Hz)
  mean           3084.7647   3174.33 1944.3398 2370.2376 3182.3609 2882.7851  338.5265 2650.5175 3086.3027         0 3319.7801
  std. dev.       921.7526  885.5429 1654.8525 1544.5093 1024.5862 1153.5603 1029.4118 1411.8243   763.546    0.1138  557.8761
  weight sum         14333      6910     16793      1286      3561      6646     35831      2915      2080        33      1313
  precision         0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829    0.6829





=== Stratified cross-validation ===

Correctly Classified Instances       31687               34.5547 %
Incorrectly Classified Instances     60014               65.4453 %
Kappa statistic                          0.2593
K&B Relative Info Score            2228312.293  %
K&B Information Score                57817.7161 bits      0.6305 bits/instance
Class complexity | order 0          237913.6665 bits      2.5945 bits/instance
Class complexity | scheme          4982974.8857 bits     54.3394 bits/instance
Complexity improvement     (Sf)    -4745061.2192 bits    -51.7449 bits/instance
Mean absolute error                      0.1196
Root mean squared error                  0.3272
Relative absolute error                 84.8932 %
Root relative squared error            123.2663 %
Coverage of cases (0.95 level)          42.8872 %
Mean rel. region size (0.95 level)      14.0029 %
Total Number of Instances            91701     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,394    0,037    0,663      0,394    0,494      0,447    0,858     0,578     A
                 0,388    0,037    0,460      0,388    0,421      0,380    0,853     0,388     E
                 0,149    0,027    0,550      0,149    0,235      0,217    0,630     0,378     CGJLNQSRT
                 0,058    0,022    0,035      0,058    0,044      0,027    0,632     0,027     FV
                 0,360    0,034    0,297      0,360    0,325      0,297    0,857     0,270     I
                 0,010    0,004    0,163      0,010    0,019      0,024    0,738     0,170     O
                 0,493    0,025    0,927      0,493    0,644      0,563    0,869     0,847     0
                 0,008    0,005    0,047      0,008    0,014      0,006    0,665     0,054     BMP
                 0,361    0,010    0,447      0,361    0,399      0,389    0,868     0,358     U
                 1,000    0,229    0,002      1,000    0,003      0,035    0,897     0,002     DZ
                 0,734    0,251    0,041      0,734    0,077      0,131    0,824     0,047     D
Weighted Avg.    0,346    0,029    0,637      0,346    0,429      0,383    0,802     0,553     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  5648   607   391   310   267    89   152    52    81   615  6121 |     a = A
   577  2681   256   103   404    13    33    68    35   245  2495 |     b = E
   752   607  2506   727  1117   139   991   209   152  4148  5445 |     c = CGJLNQSRT
    87    65    95    74   127     3    33     5     5   288   504 |     d = FV
   168  1153   192    78  1281     2    43    26     1   154   463 |     e = I
   659   355   248   267    60    68    34    33   490   688  3744 |     f = O
   108   198   576   381   539    77 17658    68    57 14389  1780 |     g = 0
   177   106   193    81   405    16    71    24    72   470  1300 |     h = BMP
   201    18    83    48    67    11    13    19   750    27   843 |     i = U
     0     0     0     0     0     0     0     0     0    33     0 |     j = DZ
   140    39    19    39    51     0    11     8    35     7   964 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
39.41	4.23	2.73	2.16	1.86	0.62	1.06	0.36	0.57	4.29	42.71	 a = A
8.35	38.8	3.7	1.49	5.85	0.19	0.48	0.98	0.51	3.55	36.11	 b = E
4.48	3.61	14.92	4.33	6.65	0.83	5.9	1.24	0.91	24.7	32.42	 c = CGJLNQSRT
6.77	5.05	7.39	5.75	9.88	0.23	2.57	0.39	0.39	22.4	39.19	 d = FV
4.72	32.38	5.39	2.19	35.97	0.06	1.21	0.73	0.03	4.32	13.0	 e = I
9.92	5.34	3.73	4.02	0.9	1.02	0.51	0.5	7.37	10.35	56.33	 f = O
0.3	0.55	1.61	1.06	1.5	0.21	49.28	0.19	0.16	40.16	4.97	 g = 0
6.07	3.64	6.62	2.78	13.89	0.55	2.44	0.82	2.47	16.12	44.6	 h = BMP
9.66	0.87	3.99	2.31	3.22	0.53	0.63	0.91	36.06	1.3	40.53	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	 j = DZ
10.66	2.97	1.45	2.97	3.88	0.0	0.84	0.61	2.67	0.53	73.42	 k = D
