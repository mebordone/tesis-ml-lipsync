Timestamp:2013-07-23-03:01:35
Inventario:I3
Intervalo:6ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I3-6ms-1gramas
Num Instances:  30453
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.2061





=== Stratified cross-validation ===

Correctly Classified Instances       25203               82.7603 %
Incorrectly Classified Instances      5250               17.2397 %
Kappa statistic                          0.7771
K&B Relative Info Score            2257135.8019 %
K&B Information Score                59014.8773 bits      1.9379 bits/instance
Class complexity | order 0           79596.05   bits      2.6137 bits/instance
Class complexity | scheme          1291930.3919 bits     42.4237 bits/instance
Complexity improvement     (Sf)    -1212334.3418 bits    -39.81   bits/instance
Mean absolute error                      0.0489
Root mean squared error                  0.1537
Relative absolute error                 34.492  %
Root relative squared error             57.7327 %
Coverage of cases (0.95 level)          95.6819 %
Mean rel. region size (0.95 level)      21.2554 %
Total Number of Instances            30453     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,859    0,029    0,850      0,859    0,854      0,827    0,962     0,907     A
                 0,804    0,020    0,773      0,804    0,788      0,770    0,960     0,826     E
                 0,768    0,078    0,691      0,768    0,728      0,663    0,936     0,792     CGJLNQSRT
                 0,426    0,002    0,766      0,426    0,547      0,567    0,884     0,515     FV
                 0,781    0,007    0,831      0,781    0,805      0,798    0,960     0,827     I
                 0,740    0,011    0,841      0,740    0,787      0,773    0,941     0,814     O
                 0,920    0,065    0,898      0,920    0,909      0,852    0,973     0,962     0
                 0,536    0,004    0,833      0,536    0,652      0,660    0,902     0,650     BMP
                 0,824    0,002    0,894      0,824    0,857      0,855    0,973     0,888     U
                 0,000    0,000    0,000      0,000    0,000      0,000    0,581     0,010     DZ
                 0,571    0,002    0,778      0,571    0,659      0,663    0,958     0,677     D
Weighted Avg.    0,828    0,046    0,829      0,828    0,826      0,785    0,957     0,873     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  4146    70   360     5    18    54   149     9     6     0     8 |     a = A
    83  1886   233    12    34    15    53    13     5     0    13 |     b = E
   297   170  4342    16    47    86   636    25    21     0    13 |     c = CGJLNQSRT
    25    29   147   183     4    14    21     2     3     0     2 |     d = FV
    25    75    97     2   942     4    44    13     3     0     1 |     e = I
    87    47   198     6     6  1666   192    19    10     0    20 |     f = O
   115    82   600     6    39    46 10687    17     9     0    10 |     g = 0
    54    36   185     7    33    30    99   525     8     0     3 |     h = BMP
    14    12    48     1     4    24    12     6   574     0     2 |     i = U
     0     0     9     0     0     0     3     0     0     0     0 |     j = DZ
    34    32    62     1     6    43     7     1     3     0   252 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
85.93	1.45	7.46	0.1	0.37	1.12	3.09	0.19	0.12	0.0	0.17	 a = A
3.54	80.36	9.93	0.51	1.45	0.64	2.26	0.55	0.21	0.0	0.55	 b = E
5.25	3.01	76.81	0.28	0.83	1.52	11.25	0.44	0.37	0.0	0.23	 c = CGJLNQSRT
5.81	6.74	34.19	42.56	0.93	3.26	4.88	0.47	0.7	0.0	0.47	 d = FV
2.07	6.22	8.04	0.17	78.11	0.33	3.65	1.08	0.25	0.0	0.08	 e = I
3.86	2.09	8.8	0.27	0.27	74.01	8.53	0.84	0.44	0.0	0.89	 f = O
0.99	0.71	5.17	0.05	0.34	0.4	92.04	0.15	0.08	0.0	0.09	 g = 0
5.51	3.67	18.88	0.71	3.37	3.06	10.1	53.57	0.82	0.0	0.31	 h = BMP
2.01	1.72	6.89	0.14	0.57	3.44	1.72	0.86	82.35	0.0	0.29	 i = U
0.0	0.0	75.0	0.0	0.0	0.0	25.0	0.0	0.0	0.0	0.0	 j = DZ
7.71	7.26	14.06	0.23	1.36	9.75	1.59	0.23	0.68	0.0	57.14	 k = D
