Timestamp:2013-08-30-05:27:09
Inventario:I4
Intervalo:6ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I4-6ms-1gramas
Num Instances:  30453
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(16): class 
0Pitch(Hz)(11): class 
0RawPitch(12): class 
0SmPitch(11): class 
0Melogram(st)(14): class 
0ZCross(13): class 
0F1(Hz)(23): class 
0F2(Hz)(18): class 
0F3(Hz)(13): class 
0F4(Hz)(12): class 
class(10): 
LogScore Bayes: -513131.8255772009
LogScore BDeu: -519948.5204638345
LogScore MDL: -519151.1184195264
LogScore ENTROPY: -512239.24072981835
LogScore AIC: -513578.24072981835




=== Stratified cross-validation ===

Correctly Classified Instances       18258               59.9547 %
Incorrectly Classified Instances     12195               40.0453 %
Kappa statistic                          0.4737
K&B Relative Info Score            1467012.3111 %
K&B Information Score                37664.6804 bits      1.2368 bits/instance
Class complexity | order 0           78170.5996 bits      2.5669 bits/instance
Class complexity | scheme           116467.2144 bits      3.8245 bits/instance
Complexity improvement     (Sf)     -38296.6148 bits     -1.2576 bits/instance
Mean absolute error                      0.0849
Root mean squared error                  0.2476
Relative absolute error                 54.7238 %
Root relative squared error             88.8986 %
Coverage of cases (0.95 level)          77.8051 %
Mean rel. region size (0.95 level)      22.8818 %
Total Number of Instances            30453     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,227    0,016    0,081      0,227    0,119      0,127    0,883     0,125     C
                 0,634    0,096    0,356      0,634    0,456      0,417    0,889     0,474     E
                 0,040    0,003    0,179      0,040    0,065      0,078    0,801     0,073     FV
                 0,659    0,111    0,593      0,659    0,624      0,527    0,884     0,707     AI
                 0,154    0,048    0,375      0,154    0,218      0,157    0,798     0,365     CDGKNRSYZ
                 0,415    0,037    0,470      0,415    0,441      0,400    0,847     0,450     O
                 0,909    0,154    0,785      0,909    0,842      0,739    0,961     0,954     0
                 0,020    0,007    0,103      0,020    0,033      0,030    0,768     0,099     LT
                 0,522    0,017    0,414      0,522    0,462      0,451    0,935     0,507     U
                 0,122    0,011    0,264      0,122    0,167      0,162    0,799     0,161     MBP
Weighted Avg.    0,600    0,100    0,563      0,600    0,566      0,491    0,890     0,653     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
    42    66     0    44    11    10    11     0     0     1 |     a = C
   110  1489     2   376   121    57    92    36    31    33 |     b = E
    14    99    17    63    57    38   102     7    12    21 |     c = FV
    86   879     8  3972   329   234   321    25    85    92 |     d = AI
   142   732    31   973   738   310  1617    59   111    89 |     e = CDGKNRSYZ
    48   224     5   510   116   935   244    17   138    14 |     f = O
    12   222    17   301   305   117 10559    21    29    28 |     g = 0
    11   232     1   175   185   102   332    22    23    36 |     h = LT
    13    63     3    85    20   110    10     8   364    21 |     i = U
    42   177    11   194    87    75   169    19    86   120 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
22.7	35.68	0.0	23.78	5.95	5.41	5.95	0.0	0.0	0.54	 a = C
4.69	63.44	0.09	16.02	5.16	2.43	3.92	1.53	1.32	1.41	 b = E
3.26	23.02	3.95	14.65	13.26	8.84	23.72	1.63	2.79	4.88	 c = FV
1.43	14.57	0.13	65.86	5.46	3.88	5.32	0.41	1.41	1.53	 d = AI
2.96	15.24	0.65	20.26	15.37	6.46	33.67	1.23	2.31	1.85	 e = CDGKNRSYZ
2.13	9.95	0.22	22.66	5.15	41.54	10.84	0.76	6.13	0.62	 f = O
0.1	1.91	0.15	2.59	2.63	1.01	90.94	0.18	0.25	0.24	 g = 0
0.98	20.73	0.09	15.64	16.53	9.12	29.67	1.97	2.06	3.22	 h = LT
1.87	9.04	0.43	12.2	2.87	15.78	1.43	1.15	52.22	3.01	 i = U
4.29	18.06	1.12	19.8	8.88	7.65	17.24	1.94	8.78	12.24	 j = MBP
