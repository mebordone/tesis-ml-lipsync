Timestamp:2013-07-22-23:05:13
Inventario:I4
Intervalo:10ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I4-10ms-4gramas
Num Instances:  18293
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.2859





=== Stratified cross-validation ===

Correctly Classified Instances       14129               77.2372 %
Incorrectly Classified Instances      4164               22.7628 %
Kappa statistic                          0.7055
K&B Relative Info Score            1207464.8435 %
K&B Information Score                31192.6269 bits      1.7052 bits/instance
Class complexity | order 0           47239.1047 bits      2.5824 bits/instance
Class complexity | scheme           931527.026  bits     50.9226 bits/instance
Complexity improvement     (Sf)    -884287.9213 bits    -48.3402 bits/instance
Mean absolute error                      0.0679
Root mean squared error                  0.1814
Relative absolute error                 43.4882 %
Root relative squared error             64.94   %
Coverage of cases (0.95 level)          95.3261 %
Mean rel. region size (0.95 level)      27.2181 %
Total Number of Instances            18293     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,551    0,001    0,728      0,551    0,628      0,632    0,934     0,630     C
                 0,739    0,032    0,663      0,739    0,698      0,672    0,944     0,725     E
                 0,307    0,003    0,602      0,307    0,406      0,424    0,849     0,316     FV
                 0,827    0,069    0,751      0,827    0,787      0,732    0,949     0,864     AI
                 0,719    0,085    0,618      0,719    0,665      0,597    0,909     0,694     CDGKNRSYZ
                 0,656    0,020    0,731      0,656    0,692      0,669    0,927     0,712     O
                 0,912    0,055    0,907      0,912    0,910      0,856    0,973     0,955     0
                 0,264    0,007    0,583      0,264    0,364      0,378    0,852     0,347     LT
                 0,619    0,003    0,816      0,619    0,704      0,705    0,958     0,744     U
                 0,382    0,005    0,728      0,382    0,501      0,517    0,897     0,519     MBP
Weighted Avg.    0,772    0,052    0,772      0,772    0,766      0,721    0,943     0,806     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   59   12    0   22   10    2    0    1    1    0 |    a = C
    6 1062    7  157  139   21   30    5    3    8 |    b = E
    0   27   80   32   94   13    5    5    1    4 |    c = FV
    3  181    5 3050  247   41  111   26    8   16 |    d = AI
    4  118   19  308 2109   79  232   33   14   17 |    e = CDGKNRSYZ
    3   58    5  130  143  904  102   10   11   11 |    f = O
    2   50   10  114  319   63 6197   27    2   11 |    g = 0
    0   43    4  100  204   29  105  179    8    5 |    h = LT
    3   19    0   32   37   45    7    5  262   13 |    i = U
    1   33    3  114  110   39   40   16   11  227 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
55.14	11.21	0.0	20.56	9.35	1.87	0.0	0.93	0.93	0.0	 a = C
0.42	73.85	0.49	10.92	9.67	1.46	2.09	0.35	0.21	0.56	 b = E
0.0	10.34	30.65	12.26	36.02	4.98	1.92	1.92	0.38	1.53	 c = FV
0.08	4.91	0.14	82.7	6.7	1.11	3.01	0.7	0.22	0.43	 d = AI
0.14	4.02	0.65	10.5	71.91	2.69	7.91	1.13	0.48	0.58	 e = CDGKNRSYZ
0.22	4.21	0.36	9.44	10.38	65.65	7.41	0.73	0.8	0.8	 f = O
0.03	0.74	0.15	1.68	4.69	0.93	91.2	0.4	0.03	0.16	 g = 0
0.0	6.35	0.59	14.77	30.13	4.28	15.51	26.44	1.18	0.74	 h = LT
0.71	4.49	0.0	7.57	8.75	10.64	1.65	1.18	61.94	3.07	 i = U
0.17	5.56	0.51	19.19	18.52	6.57	6.73	2.69	1.85	38.22	 j = MBP
