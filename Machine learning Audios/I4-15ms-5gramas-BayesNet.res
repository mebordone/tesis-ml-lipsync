Timestamp:2013-08-30-05:29:33
Inventario:I4
Intervalo:15ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I4-15ms-5gramas
Num Instances:  12185
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  42 4Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  43 4RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  44 4SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  45 4Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  47 4F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  48 4F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  49 4F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  50 4F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(5): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(6): class 
0F1(Hz)(8): class 
0F2(Hz)(8): class 
0F3(Hz)(4): class 
0F4(Hz)(6): class 
1Int(dB)(8): class 
1Pitch(Hz)(5): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(5): class 
1F1(Hz)(9): class 
1F2(Hz)(6): class 
1F3(Hz)(4): class 
1F4(Hz)(7): class 
2Int(dB)(9): class 
2Pitch(Hz)(5): class 
2RawPitch(4): class 
2SmPitch(5): class 
2Melogram(st)(9): class 
2ZCross(4): class 
2F1(Hz)(9): class 
2F2(Hz)(9): class 
2F3(Hz)(6): class 
2F4(Hz)(7): class 
3Int(dB)(9): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(7): class 
3F1(Hz)(10): class 
3F2(Hz)(8): class 
3F3(Hz)(7): class 
3F4(Hz)(6): class 
4Int(dB)(9): class 
4Pitch(Hz)(4): class 
4RawPitch(4): class 
4SmPitch(4): class 
4Melogram(st)(8): class 
4ZCross(8): class 
4F1(Hz)(11): class 
4F2(Hz)(10): class 
4F3(Hz)(9): class 
4F4(Hz)(5): class 
class(10): 
LogScore Bayes: -645399.9412383141
LogScore BDeu: -656453.9321085643
LogScore MDL: -655686.594070067
LogScore ENTROPY: -642943.5109415398
LogScore AIC: -645652.5109415397




=== Stratified cross-validation ===

Correctly Classified Instances        6450               52.9339 %
Incorrectly Classified Instances      5735               47.0661 %
Kappa statistic                          0.4123
K&B Relative Info Score             497088.2921 %
K&B Information Score                12927.2307 bits      1.0609 bits/instance
Class complexity | order 0           31669.4139 bits      2.599  bits/instance
Class complexity | scheme           165465.0229 bits     13.5794 bits/instance
Complexity improvement     (Sf)    -133795.609  bits    -10.9804 bits/instance
Mean absolute error                      0.0946
Root mean squared error                  0.2879
Relative absolute error                 60.2621 %
Root relative squared error            102.758  %
Coverage of cases (0.95 level)          60.1888 %
Mean rel. region size (0.95 level)      14.4661 %
Total Number of Instances            12185     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,514    0,088    0,033      0,514    0,063      0,114    0,855     0,035     C
                 0,490    0,092    0,316      0,490    0,385      0,328    0,840     0,359     E
                 0,119    0,034    0,049      0,119    0,069      0,055    0,775     0,041     FV
                 0,397    0,056    0,645      0,397    0,492      0,414    0,843     0,619     AI
                 0,191    0,043    0,463      0,191    0,270      0,218    0,756     0,374     CDGKNRSYZ
                 0,333    0,038    0,421      0,333    0,372      0,329    0,814     0,357     O
                 0,901    0,111    0,821      0,901    0,859      0,775    0,941     0,878     0
                 0,103    0,026    0,132      0,103    0,116      0,086    0,734     0,088     LT
                 0,460    0,023    0,328      0,460    0,382      0,371    0,905     0,444     U
                 0,208    0,032    0,181      0,208    0,193      0,165    0,763     0,118     MBP
Weighted Avg.    0,529    0,073    0,582      0,529    0,536      0,471    0,856     0,580     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   37   20    0    4    4    0    1    1    2    3 |    a = C
  154  476   21   83   36   34   59   27   50   31 |    b = E
   27   41   21   15   27   10   20    4    4    8 |    c = FV
  307  417   89  990  104  171  161   87   61  105 |    d = AI
  291  248  149  214  377   92  362   76   38  131 |    e = CDGKNRSYZ
   95  108   23  113   42  313  110   26   77   33 |    f = O
   31   52   63   42  137   23 3975   59    5   27 |    g = 0
   61   71   42   27   50   25   97   47    9   28 |    h = LT
   33   19    2   14    4   45   19    8  131   10 |    i = U
   69   52   18   34   34   30   35   21   23   83 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
51.39	27.78	0.0	5.56	5.56	0.0	1.39	1.39	2.78	4.17	 a = C
15.86	49.02	2.16	8.55	3.71	3.5	6.08	2.78	5.15	3.19	 b = E
15.25	23.16	11.86	8.47	15.25	5.65	11.3	2.26	2.26	4.52	 c = FV
12.32	16.73	3.57	39.73	4.17	6.86	6.46	3.49	2.45	4.21	 d = AI
14.71	12.54	7.53	10.82	19.06	4.65	18.3	3.84	1.92	6.62	 e = CDGKNRSYZ
10.11	11.49	2.45	12.02	4.47	33.3	11.7	2.77	8.19	3.51	 f = O
0.7	1.18	1.43	0.95	3.1	0.52	90.05	1.34	0.11	0.61	 g = 0
13.35	15.54	9.19	5.91	10.94	5.47	21.23	10.28	1.97	6.13	 h = LT
11.58	6.67	0.7	4.91	1.4	15.79	6.67	2.81	45.96	3.51	 i = U
17.29	13.03	4.51	8.52	8.52	7.52	8.77	5.26	5.76	20.8	 j = MBP
