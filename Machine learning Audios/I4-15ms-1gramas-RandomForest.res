Timestamp:2013-07-22-23:06:00
Inventario:I4
Intervalo:15ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I4-15ms-1gramas
Num Instances:  12189
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.3237





=== Stratified cross-validation ===

Correctly Classified Instances        8768               71.9337 %
Incorrectly Classified Instances      3421               28.0663 %
Kappa statistic                          0.6383
K&B Relative Info Score             736303.0143 %
K&B Information Score                19145.1811 bits      1.5707 bits/instance
Class complexity | order 0           31675.272  bits      2.5987 bits/instance
Class complexity | scheme          1106585.2775 bits     90.7856 bits/instance
Complexity improvement     (Sf)    -1074910.0056 bits    -88.1869 bits/instance
Mean absolute error                      0.0736
Root mean squared error                  0.1982
Relative absolute error                 46.8864 %
Root relative squared error             70.7481 %
Coverage of cases (0.95 level)          91.3939 %
Mean rel. region size (0.95 level)      26.3229 %
Total Number of Instances            12189     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,333    0,002    0,444      0,333    0,381      0,382    0,871     0,243     C
                 0,628    0,039    0,583      0,628    0,605      0,569    0,909     0,603     E
                 0,175    0,005    0,333      0,175    0,230      0,234    0,725     0,114     FV
                 0,776    0,078    0,719      0,776    0,746      0,678    0,919     0,809     AI
                 0,614    0,100    0,544      0,614    0,577      0,491    0,864     0,595     CDGKNRSYZ
                 0,576    0,029    0,627      0,576    0,600      0,569    0,877     0,607     O
                 0,918    0,062    0,893      0,918    0,906      0,851    0,971     0,952     0
                 0,175    0,012    0,357      0,175    0,235      0,230    0,772     0,193     LT
                 0,579    0,005    0,747      0,579    0,652      0,650    0,925     0,672     U
                 0,281    0,009    0,500      0,281    0,360      0,359    0,804     0,328     MBP
Weighted Avg.    0,719    0,061    0,709      0,719    0,710      0,657    0,913     0,739     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   24   11    0   19    5    8    1    1    2    1 |    a = C
    8  610    6  134  132   31   27    8    3   12 |    b = E
    2   23   31   16   72   11    8    8    3    3 |    c = FV
    8  146    4 1933  208   59   88   23    6   17 |    d = AI
    4  124   21  257 1215   80  186   48   14   29 |    e = CDGKNRSYZ
    2   43    7  104  142  541   64   15   10   12 |    f = O
    0   20    7   68  190   33 4057   25    1   17 |    g = 0
    0   34    4   78  135   32   75   80    3   16 |    h = LT
    3   16    1   13   38   38    1    5  165    5 |    i = U
    3   20   12   68   95   30   34   11   14  112 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
33.33	15.28	0.0	26.39	6.94	11.11	1.39	1.39	2.78	1.39	 a = C
0.82	62.82	0.62	13.8	13.59	3.19	2.78	0.82	0.31	1.24	 b = E
1.13	12.99	17.51	9.04	40.68	6.21	4.52	4.52	1.69	1.69	 c = FV
0.32	5.86	0.16	77.57	8.35	2.37	3.53	0.92	0.24	0.68	 d = AI
0.2	6.27	1.06	12.99	61.43	4.04	9.4	2.43	0.71	1.47	 e = CDGKNRSYZ
0.21	4.57	0.74	11.06	15.11	57.55	6.81	1.6	1.06	1.28	 f = O
0.0	0.45	0.16	1.54	4.3	0.75	91.83	0.57	0.02	0.38	 g = 0
0.0	7.44	0.88	17.07	29.54	7.0	16.41	17.51	0.66	3.5	 h = LT
1.05	5.61	0.35	4.56	13.33	13.33	0.35	1.75	57.89	1.75	 i = U
0.75	5.01	3.01	17.04	23.81	7.52	8.52	2.76	3.51	28.07	 j = MBP
