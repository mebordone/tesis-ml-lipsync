Timestamp:2013-07-22-22:16:07
Inventario:I3
Intervalo:2ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I3-2ms-1gramas
Num Instances:  91703
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1231





=== Stratified cross-validation ===

Correctly Classified Instances       82485               89.948  %
Incorrectly Classified Instances      9218               10.052  %
Kappa statistic                          0.8692
K&B Relative Info Score            7715955.1318 %
K&B Information Score               200209.903  bits      2.1832 bits/instance
Class complexity | order 0          237916.3779 bits      2.5944 bits/instance
Class complexity | scheme          1234313.7851 bits     13.4599 bits/instance
Complexity improvement     (Sf)    -996397.4072 bits    -10.8655 bits/instance
Mean absolute error                      0.0305
Root mean squared error                  0.1183
Relative absolute error                 21.658  %
Root relative squared error             44.5604 %
Coverage of cases (0.95 level)          97.9783 %
Mean rel. region size (0.95 level)      17.6639 %
Total Number of Instances            91703     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,922    0,010    0,947      0,922    0,935      0,923    0,985     0,963     A
                 0,912    0,006    0,922      0,912    0,917      0,911    0,985     0,948     E
                 0,849    0,049    0,795      0,849    0,821      0,780    0,974     0,909     CGJLNQSRT
                 0,651    0,001    0,927      0,651    0,765      0,774    0,965     0,767     FV
                 0,890    0,002    0,936      0,890    0,912      0,909    0,978     0,929     I
                 0,841    0,004    0,947      0,841    0,891      0,885    0,977     0,914     O
                 0,946    0,061    0,909      0,946    0,927      0,879    0,987     0,980     0
                 0,730    0,001    0,943      0,730    0,823      0,825    0,961     0,832     BMP
                 0,925    0,001    0,965      0,925    0,945      0,944    0,988     0,962     U
                 0,000    0,000    0,000      0,000    0,000      0,000    0,766     0,007     DZ
                 0,890    0,001    0,937      0,890    0,913      0,912    0,989     0,945     D
Weighted Avg.    0,899    0,035    0,901      0,899    0,899      0,870    0,982     0,946     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 13222    46   555     6    20    37   413    17     6     0    11 |     a = A
    42  6304   324     9    25    14   150    20    10     0    12 |     b = E
   339   205 14251    16    41   103  1789    15    26     0     8 |     c = CGJLNQSRT
    21    19   341   837     6    15    45     2     0     0     0 |     d = FV
    20    41   159     6  3168    11   139    12     4     0     1 |     e = I
    72    25   331    12     9  5592   561    17     1     0    26 |     f = O
   154   116  1461    14    71    62 33891    33    12     0    19 |     g = 0
    62    36   380     0    31    31   242  2127     6     0     0 |     h = BMP
     4    15    64     2    10    15    31    12  1925     0     2 |     i = U
     0     0    29     0     0     0     4     0     0     0     0 |     j = DZ
    26    27    41     1     4    26    15     1     4     0  1168 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
92.25	0.32	3.87	0.04	0.14	0.26	2.88	0.12	0.04	0.0	0.08	 a = A
0.61	91.23	4.69	0.13	0.36	0.2	2.17	0.29	0.14	0.0	0.17	 b = E
2.02	1.22	84.86	0.1	0.24	0.61	10.65	0.09	0.15	0.0	0.05	 c = CGJLNQSRT
1.63	1.48	26.52	65.09	0.47	1.17	3.5	0.16	0.0	0.0	0.0	 d = FV
0.56	1.15	4.47	0.17	88.96	0.31	3.9	0.34	0.11	0.0	0.03	 e = I
1.08	0.38	4.98	0.18	0.14	84.14	8.44	0.26	0.02	0.0	0.39	 f = O
0.43	0.32	4.08	0.04	0.2	0.17	94.58	0.09	0.03	0.0	0.05	 g = 0
2.13	1.23	13.04	0.0	1.06	1.06	8.3	72.97	0.21	0.0	0.0	 h = BMP
0.19	0.72	3.08	0.1	0.48	0.72	1.49	0.58	92.55	0.0	0.1	 i = U
0.0	0.0	87.88	0.0	0.0	0.0	12.12	0.0	0.0	0.0	0.0	 j = DZ
1.98	2.06	3.12	0.08	0.3	1.98	1.14	0.08	0.3	0.0	88.96	 k = D
