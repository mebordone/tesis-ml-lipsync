Timestamp:2013-08-30-04:41:12
Inventario:I2
Intervalo:6ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I2-6ms-5gramas
Num Instances:  30449
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  45 4Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(15): class 
0Pitch(Hz)(12): class 
0RawPitch(12): class 
0SmPitch(11): class 
0Melogram(st)(16): class 
0ZCross(10): class 
0F1(Hz)(17): class 
0F2(Hz)(18): class 
0F3(Hz)(17): class 
0F4(Hz)(17): class 
1Int(dB)(15): class 
1Pitch(Hz)(11): class 
1RawPitch(10): class 
1SmPitch(9): class 
1Melogram(st)(16): class 
1ZCross(12): class 
1F1(Hz)(17): class 
1F2(Hz)(20): class 
1F3(Hz)(18): class 
1F4(Hz)(14): class 
2Int(dB)(15): class 
2Pitch(Hz)(11): class 
2RawPitch(11): class 
2SmPitch(10): class 
2Melogram(st)(17): class 
2ZCross(12): class 
2F1(Hz)(21): class 
2F2(Hz)(18): class 
2F3(Hz)(17): class 
2F4(Hz)(14): class 
3Int(dB)(15): class 
3Pitch(Hz)(11): class 
3RawPitch(11): class 
3SmPitch(11): class 
3Melogram(st)(19): class 
3ZCross(12): class 
3F1(Hz)(20): class 
3F2(Hz)(19): class 
3F3(Hz)(16): class 
3F4(Hz)(18): class 
4Int(dB)(14): class 
4Pitch(Hz)(12): class 
4RawPitch(13): class 
4SmPitch(11): class 
4Melogram(st)(18): class 
4ZCross(12): class 
4F1(Hz)(20): class 
4F2(Hz)(19): class 
4F3(Hz)(17): class 
4F4(Hz)(15): class 
class(12): 
LogScore Bayes: -2270396.905560255
LogScore BDeu: -2314272.056344831
LogScore MDL: -2308602.5865558977
LogScore ENTROPY: -2266053.010103919
LogScore AIC: -2274296.0101039186




=== Stratified cross-validation ===

Correctly Classified Instances       17559               57.6669 %
Incorrectly Classified Instances     12890               42.3331 %
Kappa statistic                          0.4685
K&B Relative Info Score            1377936.6702 %
K&B Information Score                39781.0906 bits      1.3065 bits/instance
Class complexity | order 0           87884.2027 bits      2.8863 bits/instance
Class complexity | scheme           512670.9626 bits     16.837  bits/instance
Complexity improvement     (Sf)    -424786.7599 bits    -13.9508 bits/instance
Mean absolute error                      0.0708
Root mean squared error                  0.2537
Relative absolute error                 52.9774 %
Root relative squared error             98.1727 %
Coverage of cases (0.95 level)          62.3863 %
Mean rel. region size (0.95 level)      10.6183 %
Total Number of Instances            30449     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,579    0,046    0,702      0,579    0,635      0,577    0,899     0,708     A
                 0,025    0,011    0,107      0,025    0,040      0,029    0,726     0,097     LGJKC
                 0,549    0,057    0,447      0,549    0,493      0,449    0,883     0,471     E
                 0,128    0,020    0,084      0,128    0,102      0,088    0,811     0,075     FV
                 0,647    0,038    0,413      0,647    0,504      0,492    0,918     0,467     I
                 0,295    0,018    0,562      0,295    0,387      0,375    0,837     0,427     O
                 0,218    0,042    0,355      0,218    0,270      0,220    0,845     0,332     SNRTY
                 0,892    0,128    0,811      0,892    0,849      0,752    0,952     0,935     0
                 0,049    0,005    0,264      0,049    0,083      0,102    0,790     0,128     BMP
                 0,570    0,071    0,107      0,570    0,181      0,224    0,884     0,157     DZ
                 0,498    0,023    0,337      0,498    0,402      0,393    0,921     0,500     U
                 0,242    0,036    0,224      0,242    0,233      0,199    0,849     0,183     SNRTXY
Weighted Avg.    0,577    0,071    0,580      0,577    0,564      0,507    0,894     0,618     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  2794    77   379   111   120   187   195   236     9   456    89   172 |     a = A
   137    37   150    51   165    34   160   408     9   181    43   120 |     b = LGJKC
   191    52  1289    44   166    30   102   102    18   181    66   106 |     c = E
    27     2    59    55    24    20    50    76     6    75    11    25 |     d = FV
    23    27   146    10   780     0    69    68    11    20    19    33 |     e = I
   280    52   180    18    58   663   103   222    10   361   210    94 |     f = O
   173    31   179   135   106    39   636  1037    25   278    45   228 |     g = SNRTY
   111    32   161   133   141    75   277 10350    27   141    28   131 |     h = 0
    71    12    89    54   176    27    69   149    48   141    82    62 |     i = BMP
    45     0    22    12    20    14    14    11     2   258    19    36 |     j = DZ
    30    22    51    12    11    63    27    20     5    68   347    41 |     k = U
    98     2   176    18   123    27    91    83    12   242    72   302 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
57.91	1.6	7.85	2.3	2.49	3.88	4.04	4.89	0.19	9.45	1.84	3.56	 a = A
9.16	2.47	10.03	3.41	11.04	2.27	10.7	27.29	0.6	12.11	2.88	8.03	 b = LGJKC
8.14	2.22	54.92	1.87	7.07	1.28	4.35	4.35	0.77	7.71	2.81	4.52	 c = E
6.28	0.47	13.72	12.79	5.58	4.65	11.63	17.67	1.4	17.44	2.56	5.81	 d = FV
1.91	2.24	12.11	0.83	64.68	0.0	5.72	5.64	0.91	1.66	1.58	2.74	 e = I
12.44	2.31	8.0	0.8	2.58	29.45	4.58	9.86	0.44	16.04	9.33	4.18	 f = O
5.94	1.06	6.15	4.64	3.64	1.34	21.84	35.61	0.86	9.55	1.55	7.83	 g = SNRTY
0.96	0.28	1.39	1.15	1.21	0.65	2.39	89.17	0.23	1.21	0.24	1.13	 h = 0
7.24	1.22	9.08	5.51	17.96	2.76	7.04	15.2	4.9	14.39	8.37	6.33	 i = BMP
9.93	0.0	4.86	2.65	4.42	3.09	3.09	2.43	0.44	56.95	4.19	7.95	 j = DZ
4.3	3.16	7.32	1.72	1.58	9.04	3.87	2.87	0.72	9.76	49.78	5.88	 k = U
7.87	0.16	14.13	1.44	9.87	2.17	7.3	6.66	0.96	19.42	5.78	24.24	 l = SNRTXY
