Timestamp:2013-07-22-20:50:10
Inventario:I1
Intervalo:1ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I1-1ms-2gramas
Num Instances:  183403
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.121





=== Stratified cross-validation ===

Correctly Classified Instances      164573               89.733  %
Incorrectly Classified Instances     18830               10.267  %
Kappa statistic                          0.8698
K&B Relative Info Score            15673259.8666 %
K&B Information Score               451458.8857 bits      2.4616 bits/instance
Class complexity | order 0          528259.3617 bits      2.8803 bits/instance
Class complexity | scheme          8796289.131  bits     47.9615 bits/instance
Complexity improvement     (Sf)    -8268029.7693 bits    -45.0812 bits/instance
Mean absolute error                      0.0225
Root mean squared error                  0.1091
Relative absolute error                 18.3818 %
Root relative squared error             44.1456 %
Coverage of cases (0.95 level)          95.2836 %
Mean rel. region size (0.95 level)      12.5183 %
Total Number of Instances           183403     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,939    0,009    0,949      0,939    0,944      0,934    0,980     0,964     a
                 0,793    0,004    0,853      0,793    0,822      0,817    0,936     0,844     b
                 0,941    0,005    0,943      0,941    0,942      0,937    0,982     0,962     e
                 0,931    0,001    0,949      0,931    0,940      0,939    0,985     0,963     d
                 0,723    0,003    0,789      0,723    0,754      0,752    0,920     0,782     f
                 0,922    0,002    0,944      0,922    0,933      0,930    0,976     0,946     i
                 0,435    0,007    0,535      0,435    0,480      0,473    0,825     0,443     k
                 0,804    0,001    0,872      0,804    0,836      0,835    0,933     0,836     j
                 0,826    0,014    0,857      0,826    0,841      0,825    0,953     0,887     l
                 0,872    0,004    0,939      0,872    0,905      0,898    0,961     0,918     o
                 0,950    0,061    0,909      0,950    0,929      0,883    0,983     0,968     0
                 0,737    0,018    0,713      0,737    0,725      0,708    0,943     0,786     s
                 0,960    0,001    0,975      0,960    0,967      0,967    0,992     0,981     u
Weighted Avg.    0,897    0,029    0,896      0,897    0,896      0,872    0,970     0,926     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 26828    75    52    13    38    30   118    16   256    54   837   230    10 |     a = a
    99  4613    52     0    20    33    90     6   160    43   501   183    14 |     b = b
    75    50 12928    16    14    28    60    10   159    21   279    96     4 |     c = e
    31     4    21  2497     7     3     4     1    18    28    38    30     0 |     d = d
    43    24    32     3  1851    13    44    11    75    21   141   302     1 |     e = f
    37    28    41     3    14  6530    25     1    87    10   238    62     7 |     f = i
   105    84    50     4    48    22  1547    22   258    52   903   443    17 |     g = k
    32     8    19     1    16     7    32  1657    37     5    89   157     2 |     h = j
   310   123   178    23    59    61   235    34 14279   116  1354   506    14 |     i = l
    92    40    43    29    30    10    48     6   161 11545  1064   159     7 |     j = o
   427   212   229    27    93   122   396    51   746   294 68543   959    17 |     k = 0
   183   132    57    13   156    45   277    83   392    87  1352  7785     8 |     l = s
    16    15     8     1     1    14    18     3    34    17    33     7  3970 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
93.95	0.26	0.18	0.05	0.13	0.11	0.41	0.06	0.9	0.19	2.93	0.81	0.04	 a = a
1.7	79.34	0.89	0.0	0.34	0.57	1.55	0.1	2.75	0.74	8.62	3.15	0.24	 b = b
0.55	0.36	94.09	0.12	0.1	0.2	0.44	0.07	1.16	0.15	2.03	0.7	0.03	 c = e
1.16	0.15	0.78	93.1	0.26	0.11	0.15	0.04	0.67	1.04	1.42	1.12	0.0	 d = d
1.68	0.94	1.25	0.12	72.28	0.51	1.72	0.43	2.93	0.82	5.51	11.79	0.04	 e = f
0.52	0.4	0.58	0.04	0.2	92.19	0.35	0.01	1.23	0.14	3.36	0.88	0.1	 f = i
2.95	2.36	1.41	0.11	1.35	0.62	43.52	0.62	7.26	1.46	25.4	12.46	0.48	 g = k
1.55	0.39	0.92	0.05	0.78	0.34	1.55	80.36	1.79	0.24	4.32	7.61	0.1	 h = j
1.79	0.71	1.03	0.13	0.34	0.35	1.36	0.2	82.58	0.67	7.83	2.93	0.08	 i = l
0.7	0.3	0.32	0.22	0.23	0.08	0.36	0.05	1.22	87.24	8.04	1.2	0.05	 j = o
0.59	0.29	0.32	0.04	0.13	0.17	0.55	0.07	1.03	0.41	95.05	1.33	0.02	 k = 0
1.73	1.25	0.54	0.12	1.48	0.43	2.62	0.79	3.71	0.82	12.79	73.65	0.08	 l = s
0.39	0.36	0.19	0.02	0.02	0.34	0.44	0.07	0.82	0.41	0.8	0.17	95.96	 m = u
