Timestamp:2013-07-22-21:58:31
Inventario:I2
Intervalo:20ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I2-20ms-2gramas
Num Instances:  9128
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3826





=== Stratified cross-validation ===

Correctly Classified Instances        6069               66.4877 %
Incorrectly Classified Instances      3059               33.5123 %
Kappa statistic                          0.5866
K&B Relative Info Score             521249.2342 %
K&B Information Score                15371.5535 bits      1.684  bits/instance
Class complexity | order 0           26901.0412 bits      2.9471 bits/instance
Class complexity | scheme          1005567.356  bits    110.1629 bits/instance
Complexity improvement     (Sf)    -978666.3148 bits   -107.2159 bits/instance
Mean absolute error                      0.07  
Root mean squared error                  0.1927
Relative absolute error                 51.4755 %
Root relative squared error             73.8777 %
Coverage of cases (0.95 level)          89.8116 %
Mean rel. region size (0.95 level)      25.9695 %
Total Number of Instances             9128     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,786    0,083    0,653      0,786    0,713      0,654    0,922     0,795     A
                 0,218    0,032    0,263      0,218    0,238      0,203    0,759     0,176     LGJKC
                 0,625    0,051    0,522      0,625    0,569      0,529    0,905     0,567     E
                 0,123    0,006    0,227      0,123    0,160      0,158    0,716     0,082     FV
                 0,592    0,021    0,556      0,592    0,574      0,554    0,907     0,561     I
                 0,544    0,042    0,521      0,544    0,532      0,492    0,868     0,526     O
                 0,484    0,060    0,474      0,484    0,479      0,420    0,842     0,452     SNRTY
                 0,905    0,051    0,906      0,905    0,906      0,854    0,973     0,949     0
                 0,241    0,012    0,404      0,241    0,302      0,295    0,798     0,227     BMP
                 0,072    0,005    0,192      0,072    0,105      0,110    0,746     0,111     DZ
                 0,472    0,006    0,669      0,472    0,554      0,553    0,879     0,553     U
                 0,344    0,016    0,489      0,344    0,404      0,388    0,882     0,385     SNRTXY
Weighted Avg.    0,665    0,049    0,652      0,665    0,655      0,611    0,905     0,672     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1183   30   75    4   10   65   63   44    6    1    7   17 |    a = A
   81   98   47    7   22   37   76   45   14    4    5   14 |    b = LGJKC
   85   39  466    8   36   25   36   12   13    4    4   18 |    c = E
   21    9   20   17    5   15   34    7    5    2    2    1 |    d = FV
   12   14   66    2  228    4   25   13   10    0    1   10 |    e = I
  113   28   40    4   11  387   34   49   12    8   11   15 |    f = O
  139   57   54   19   28   31  442   98    9   12    3   22 |    g = SNRTY
   49   29   16    6   21   47  117 2930   12    1    1    7 |    h = 0
   37   19   29    7   26   27   36   23   72    1    9   13 |    i = BMP
   27   12   15    1    3   36   13    1    2   10    3   15 |    j = DZ
   17    9   10    0    5   42   10    1   10    4  103    7 |    k = U
   49   29   55    0   15   27   46   10   13    5    5  133 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
78.6	1.99	4.98	0.27	0.66	4.32	4.19	2.92	0.4	0.07	0.47	1.13	 a = A
18.0	21.78	10.44	1.56	4.89	8.22	16.89	10.0	3.11	0.89	1.11	3.11	 b = LGJKC
11.39	5.23	62.47	1.07	4.83	3.35	4.83	1.61	1.74	0.54	0.54	2.41	 c = E
15.22	6.52	14.49	12.32	3.62	10.87	24.64	5.07	3.62	1.45	1.45	0.72	 d = FV
3.12	3.64	17.14	0.52	59.22	1.04	6.49	3.38	2.6	0.0	0.26	2.6	 e = I
15.87	3.93	5.62	0.56	1.54	54.35	4.78	6.88	1.69	1.12	1.54	2.11	 f = O
15.21	6.24	5.91	2.08	3.06	3.39	48.36	10.72	0.98	1.31	0.33	2.41	 g = SNRTY
1.51	0.9	0.49	0.19	0.65	1.45	3.62	90.54	0.37	0.03	0.03	0.22	 h = 0
12.37	6.35	9.7	2.34	8.7	9.03	12.04	7.69	24.08	0.33	3.01	4.35	 i = BMP
19.57	8.7	10.87	0.72	2.17	26.09	9.42	0.72	1.45	7.25	2.17	10.87	 j = DZ
7.8	4.13	4.59	0.0	2.29	19.27	4.59	0.46	4.59	1.83	47.25	3.21	 k = U
12.66	7.49	14.21	0.0	3.88	6.98	11.89	2.58	3.36	1.29	1.29	34.37	 l = SNRTXY
