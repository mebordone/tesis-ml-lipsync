Timestamp:2013-08-30-05:25:31
Inventario:I4
Intervalo:3ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I4-3ms-4gramas
Num Instances:  61132
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(19): class 
0Pitch(Hz)(29): class 
0RawPitch(21): class 
0SmPitch(22): class 
0Melogram(st)(27): class 
0ZCross(15): class 
0F1(Hz)(248): class 
0F2(Hz)(168): class 
0F3(Hz)(421): class 
0F4(Hz)(217): class 
1Int(dB)(21): class 
1Pitch(Hz)(28): class 
1RawPitch(19): class 
1SmPitch(24): class 
1Melogram(st)(27): class 
1ZCross(15): class 
1F1(Hz)(221): class 
1F2(Hz)(270): class 
1F3(Hz)(243): class 
1F4(Hz)(293): class 
2Int(dB)(20): class 
2Pitch(Hz)(33): class 
2RawPitch(21): class 
2SmPitch(24): class 
2Melogram(st)(29): class 
2ZCross(15): class 
2F1(Hz)(187): class 
2F2(Hz)(156): class 
2F3(Hz)(274): class 
2F4(Hz)(275): class 
3Int(dB)(20): class 
3Pitch(Hz)(30): class 
3RawPitch(22): class 
3SmPitch(23): class 
3Melogram(st)(30): class 
3ZCross(15): class 
3F1(Hz)(194): class 
3F2(Hz)(160): class 
3F3(Hz)(233): class 
3F4(Hz)(302): class 
class(10): 
LogScore Bayes: -4954785.542223235
LogScore BDeu: -5338356.105681096
LogScore MDL: -5277172.095029933
LogScore ENTROPY: -5036263.119856602
LogScore AIC: -5079982.119856602




=== Stratified cross-validation ===

Correctly Classified Instances       38256               62.5793 %
Incorrectly Classified Instances     22876               37.4207 %
Kappa statistic                          0.5131
K&B Relative Info Score            3197162.5732 %
K&B Information Score                81649.9228 bits      1.3356 bits/instance
Class complexity | order 0          156104.8787 bits      2.5536 bits/instance
Class complexity | scheme           796222.4445 bits     13.0246 bits/instance
Complexity improvement     (Sf)    -640117.5659 bits    -10.4711 bits/instance
Mean absolute error                      0.0753
Root mean squared error                  0.262 
Relative absolute error                 48.7394 %
Root relative squared error             94.2875 %
Coverage of cases (0.95 level)          67.2234 %
Mean rel. region size (0.95 level)      12.2828 %
Total Number of Instances            61132     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,689    0,020    0,169      0,689    0,272      0,335    0,933     0,326     C
                 0,691    0,086    0,396      0,691    0,503      0,472    0,903     0,541     E
                 0,331    0,015    0,241      0,331    0,279      0,270    0,859     0,263     FV
                 0,608    0,059    0,715      0,608    0,657      0,585    0,898     0,743     AI
                 0,170    0,033    0,486      0,170    0,251      0,218    0,832     0,433     CDGKNRSYZ
                 0,525    0,034    0,548      0,525    0,537      0,501    0,875     0,560     O
                 0,897    0,156    0,785      0,897    0,837      0,727    0,954     0,938     0
                 0,136    0,012    0,297      0,136    0,186      0,181    0,818     0,163     LT
                 0,715    0,026    0,388      0,715    0,503      0,512    0,950     0,627     U
                 0,360    0,020    0,369      0,360    0,364      0,344    0,840     0,332     MBP
Weighted Avg.    0,626    0,088    0,626      0,626    0,605      0,538    0,904     0,695     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   250    59     0    11    10     5    23     3     0     2 |     a = C
   185  3191    79   354   201   114   195    92    92   118 |     b = E
    20   127   286    65    26    56   211    13    26    34 |     c = FV
   365  1646   159  7275   517   559   663   169   281   333 |     d = AI
   306  1463   292  1117  1624   546  3158   175   530   365 |     e = CDGKNRSYZ
   144   417    34   512   177  2345   480    52   252    50 |     f = O
    65   497   198   479   482   311 21291   125   116   159 |     g = 0
    49   344    62   167   194   166   723   301   106   109 |     h = LT
    23    88    16    50    32    92    32    30   991    32 |     i = U
    68   233    61   144    81    84   363    52   160   702 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
68.87	16.25	0.0	3.03	2.75	1.38	6.34	0.83	0.0	0.55	 a = C
4.0	69.05	1.71	7.66	4.35	2.47	4.22	1.99	1.99	2.55	 b = E
2.31	14.7	33.1	7.52	3.01	6.48	24.42	1.5	3.01	3.94	 c = FV
3.05	13.75	1.33	60.79	4.32	4.67	5.54	1.41	2.35	2.78	 d = AI
3.2	15.28	3.05	11.66	16.96	5.7	32.98	1.83	5.53	3.81	 e = CDGKNRSYZ
3.23	9.34	0.76	11.47	3.97	52.54	10.76	1.17	5.65	1.12	 f = O
0.27	2.1	0.83	2.02	2.03	1.31	89.75	0.53	0.49	0.67	 g = 0
2.21	15.49	2.79	7.52	8.73	7.47	32.55	13.55	4.77	4.91	 h = LT
1.66	6.35	1.15	3.61	2.31	6.64	2.31	2.16	71.5	2.31	 i = U
3.49	11.96	3.13	7.39	4.16	4.31	18.63	2.67	8.21	36.04	 j = MBP
