Timestamp:2013-08-30-04:18:21
Inventario:I1
Intervalo:15ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I1-15ms-2gramas
Num Instances:  12188
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(8): class 
0F1(Hz)(9): class 
0F2(Hz)(9): class 
0F3(Hz)(7): class 
0F4(Hz)(6): class 
1Int(dB)(9): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(9): class 
1F1(Hz)(11): class 
1F2(Hz)(10): class 
1F3(Hz)(7): class 
1F4(Hz)(7): class 
class(13): 
LogScore Bayes: -268125.8380201764
LogScore BDeu: -275053.75510962185
LogScore MDL: -274294.5247362437
LogScore ENTROPY: -266960.8272705313
LogScore AIC: -268519.8272705313




=== Stratified cross-validation ===

Correctly Classified Instances        7119               58.4099 %
Incorrectly Classified Instances      5069               41.5901 %
Kappa statistic                          0.4843
K&B Relative Info Score             585783.6116 %
K&B Information Score                17294.2625 bits      1.419  bits/instance
Class complexity | order 0           35957.1789 bits      2.9502 bits/instance
Class complexity | scheme            77251.0574 bits      6.3383 bits/instance
Complexity improvement     (Sf)     -41293.8786 bits     -3.3881 bits/instance
Mean absolute error                      0.0658
Root mean squared error                  0.2229
Relative absolute error                 52.6346 %
Root relative squared error             89.2002 %
Coverage of cases (0.95 level)          73.1047 %
Mean rel. region size (0.95 level)      15.6043 %
Total Number of Instances            12188     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,642    0,070    0,642      0,642    0,642      0,572    0,897     0,719     a
                 0,018    0,005    0,106      0,018    0,030      0,030    0,770     0,095     b
                 0,564    0,067    0,423      0,564    0,484      0,438    0,883     0,465     e
                 0,352    0,048    0,100      0,352    0,156      0,166    0,853     0,104     d
                 0,051    0,013    0,053      0,051    0,052      0,038    0,765     0,044     f
                 0,640    0,041    0,403      0,640    0,495      0,482    0,919     0,463     i
                 0,177    0,027    0,115      0,177    0,140      0,122    0,855     0,102     k
                 0,094    0,007    0,138      0,094    0,112      0,106    0,824     0,071     j
                 0,146    0,040    0,290      0,146    0,194      0,146    0,783     0,248     l
                 0,304    0,020    0,555      0,304    0,393      0,377    0,830     0,414     o
                 0,919    0,094    0,847      0,919    0,882      0,812    0,971     0,961     0
                 0,233    0,029    0,337      0,233    0,275      0,242    0,892     0,344     s
                 0,498    0,017    0,408      0,498    0,449      0,436    0,919     0,478     u
Weighted Avg.    0,584    0,062    0,573      0,584    0,568      0,518    0,899     0,618     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1277    3  168  103   34   46   43    7  104   71   77   45   11 |    a = a
   39    7   59   43   12   68   18   12   37   12   42   22   28 |    b = b
  112    7  548   50   10   74   21   12   44   15   31   22   25 |    c = e
   35    6   24   64    1    8    1    0   16   11    5    1   10 |    d = d
   18    3   27   26    9    9    7    3   19   11   22   21    2 |    e = f
    5    1   92    5    2  322   20    8   13    0   15   17    3 |    f = i
    4    2    5    0    6   15   42    2    9    5  110   35    2 |    g = k
   31    2   31    6    3   17    3   13    6    1    8   13    4 |    h = j
  180   18  162  168   27  127   54   11  177   42  134   57   59 |    i = l
  178    5   85  107   12   26   18    6   55  286   85   22   55 |    j = o
   45    6   36   16   31   39   30    3   48   21 4061   78    3 |    k = 0
   44    4   33   28   22   45   97   17   61    8  200  171    4 |    l = s
   20    2   24   21    1    3   10    0   22   32    4    4  142 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
64.2	0.15	8.45	5.18	1.71	2.31	2.16	0.35	5.23	3.57	3.87	2.26	0.55	 a = a
9.77	1.75	14.79	10.78	3.01	17.04	4.51	3.01	9.27	3.01	10.53	5.51	7.02	 b = b
11.53	0.72	56.44	5.15	1.03	7.62	2.16	1.24	4.53	1.54	3.19	2.27	2.57	 c = e
19.23	3.3	13.19	35.16	0.55	4.4	0.55	0.0	8.79	6.04	2.75	0.55	5.49	 d = d
10.17	1.69	15.25	14.69	5.08	5.08	3.95	1.69	10.73	6.21	12.43	11.86	1.13	 e = f
0.99	0.2	18.29	0.99	0.4	64.02	3.98	1.59	2.58	0.0	2.98	3.38	0.6	 f = i
1.69	0.84	2.11	0.0	2.53	6.33	17.72	0.84	3.8	2.11	46.41	14.77	0.84	 g = k
22.46	1.45	22.46	4.35	2.17	12.32	2.17	9.42	4.35	0.72	5.8	9.42	2.9	 h = j
14.8	1.48	13.32	13.82	2.22	10.44	4.44	0.9	14.56	3.45	11.02	4.69	4.85	 i = l
18.94	0.53	9.04	11.38	1.28	2.77	1.91	0.64	5.85	30.43	9.04	2.34	5.85	 j = o
1.02	0.14	0.82	0.36	0.7	0.88	0.68	0.07	1.09	0.48	91.94	1.77	0.07	 k = 0
5.99	0.54	4.5	3.81	3.0	6.13	13.22	2.32	8.31	1.09	27.25	23.3	0.54	 l = s
7.02	0.7	8.42	7.37	0.35	1.05	3.51	0.0	7.72	11.23	1.4	1.4	49.82	 m = u
