Timestamp:2013-08-30-04:55:49
Inventario:I3
Intervalo:2ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I3-2ms-2gramas
Num Instances:  91702
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(22): class 
0Pitch(Hz)(46): class 
0RawPitch(34): class 
0SmPitch(35): class 
0Melogram(st)(45): class 
0ZCross(17): class 
0F1(Hz)(1379): class 
0F2(Hz)(1939): class 
0F3(Hz)(2284): class 
0F4(Hz)(2125): class 
1Int(dB)(25): class 
1Pitch(Hz)(48): class 
1RawPitch(32): class 
1SmPitch(39): class 
1Melogram(st)(45): class 
1ZCross(17): class 
1F1(Hz)(1291): class 
1F2(Hz)(1920): class 
1F3(Hz)(2248): class 
1F4(Hz)(2101): class 
class(11): 
LogScore Bayes: -5059016.879274635
LogScore BDeu: -7161416.248732211
LogScore MDL: -6628120.336085925
LogScore ENTROPY: -5643161.895622657
LogScore AIC: -5815563.895622658




=== Stratified cross-validation ===

Correctly Classified Instances       68988               75.2306 %
Incorrectly Classified Instances     22714               24.7694 %
Kappa statistic                          0.6786
K&B Relative Info Score            6524057.049  %
K&B Information Score               169279.1832 bits      1.846  bits/instance
Class complexity | order 0          237915.0222 bits      2.5944 bits/instance
Class complexity | scheme           495154.6424 bits      5.3996 bits/instance
Complexity improvement     (Sf)    -257239.6202 bits     -2.8052 bits/instance
Mean absolute error                      0.0457
Root mean squared error                  0.2018
Relative absolute error                 32.4439 %
Root relative squared error             76.0426 %
Coverage of cases (0.95 level)          79.7267 %
Mean rel. region size (0.95 level)      10.9092 %
Total Number of Instances            91702     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,815    0,025    0,857      0,815    0,836      0,806    0,955     0,896     A
                 0,826    0,032    0,675      0,826    0,743      0,724    0,957     0,825     E
                 0,339    0,021    0,784      0,339    0,473      0,455    0,931     0,724     CGJLNQSRT
                 0,574    0,008    0,511      0,574    0,541      0,535    0,898     0,560     FV
                 0,847    0,017    0,670      0,847    0,748      0,742    0,958     0,831     I
                 0,743    0,015    0,792      0,743    0,767      0,750    0,921     0,803     O
                 0,904    0,158    0,786      0,904    0,841      0,732    0,967     0,958     0
                 0,634    0,008    0,714      0,634    0,672      0,663    0,896     0,680     BMP
                 0,887    0,010    0,674      0,887    0,766      0,767    0,977     0,894     U
                 0,000    0,003    0,000      0,000    0,000      -0,001   0,844     0,001     DZ
                 0,862    0,024    0,341      0,862    0,488      0,533    0,978     0,719     D
Weighted Avg.    0,752    0,075    0,769      0,752    0,740      0,687    0,951     0,860     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 11686   382   361   142   154   253   804    91    78    68   314 |     a = A
   149  5709   265    59   114    41   284    69    54    22   144 |     b = E
   956  1357  5692   118   695   451  5855   259   497    17   896 |     c = CGJLNQSRT
    37    56    37   738    29    16   322    13     9     2    27 |     d = FV
    25    84   121    22  3016    10   190    23    11    32    27 |     e = I
   245   163   121    17    37  4937   735    60    51    21   259 |     f = O
   427   532   494   321   353   410 32384   203   125   136   447 |     g = 0
    70    96    93    10    86    55   540  1849    63     1    52 |     h = BMP
    14    29    65     8     7    23    40    21  1845     2    26 |     i = U
     0     0     0     0     0     0    33     0     0     0     0 |     j = DZ
    30    46    14     9    12    37    15     2     6    10  1132 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
81.53	2.67	2.52	0.99	1.07	1.77	5.61	0.63	0.54	0.47	2.19	 a = A
2.16	82.62	3.84	0.85	1.65	0.59	4.11	1.0	0.78	0.32	2.08	 b = E
5.69	8.08	33.9	0.7	4.14	2.69	34.87	1.54	2.96	0.1	5.34	 c = CGJLNQSRT
2.88	4.35	2.88	57.39	2.26	1.24	25.04	1.01	0.7	0.16	2.1	 d = FV
0.7	2.36	3.4	0.62	84.7	0.28	5.34	0.65	0.31	0.9	0.76	 e = I
3.69	2.45	1.82	0.26	0.56	74.29	11.06	0.9	0.77	0.32	3.9	 f = O
1.19	1.48	1.38	0.9	0.99	1.14	90.38	0.57	0.35	0.38	1.25	 g = 0
2.4	3.29	3.19	0.34	2.95	1.89	18.52	63.43	2.16	0.03	1.78	 h = BMP
0.67	1.39	3.13	0.38	0.34	1.11	1.92	1.01	88.7	0.1	1.25	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
2.28	3.5	1.07	0.69	0.91	2.82	1.14	0.15	0.46	0.76	86.21	 k = D
