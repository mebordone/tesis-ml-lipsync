Timestamp:2013-07-22-22:32:46
Inventario:I3
Intervalo:20ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I3-20ms-2gramas
Num Instances:  9128
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3468





=== Stratified cross-validation ===

Correctly Classified Instances        6416               70.2892 %
Incorrectly Classified Instances      2712               29.7108 %
Kappa statistic                          0.6222
K&B Relative Info Score             543407.5425 %
K&B Information Score                14512.2622 bits      1.5899 bits/instance
Class complexity | order 0           24350.7576 bits      2.6677 bits/instance
Class complexity | scheme           751619.5492 bits     82.3422 bits/instance
Complexity improvement     (Sf)    -727268.7916 bits    -79.6745 bits/instance
Mean absolute error                      0.0703
Root mean squared error                  0.1926
Relative absolute error                 48.719  %
Root relative squared error             71.7151 %
Coverage of cases (0.95 level)          92.408  %
Mean rel. region size (0.95 level)      25.2968 %
Total Number of Instances             9128     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,757    0,069    0,683      0,757    0,718      0,661    0,921     0,792     A
                 0,602    0,044    0,550      0,602    0,575      0,536    0,906     0,550     E
                 0,656    0,132    0,542      0,656    0,593      0,488    0,863     0,591     CGJLNQSRT
                 0,094    0,002    0,371      0,094    0,150      0,181    0,724     0,103     FV
                 0,525    0,014    0,622      0,525    0,569      0,554    0,904     0,551     I
                 0,514    0,034    0,558      0,514    0,535      0,498    0,865     0,517     O
                 0,900    0,045    0,916      0,900    0,908      0,858    0,973     0,949     0
                 0,241    0,008    0,497      0,241    0,324      0,331    0,803     0,260     BMP
                 0,450    0,005    0,705      0,450    0,549      0,555    0,904     0,549     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,499     0,001     DZ
                 0,120    0,003    0,372      0,120    0,182      0,205    0,810     0,151     D
Weighted Avg.    0,703    0,060    0,700      0,703    0,695      0,643    0,913     0,714     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1140   64  180    0   10   57   38    7    4    1    4 |    a = A
   76  449  140    3   34   28    7    4    3    0    2 |    b = E
  194  123 1148    9   39   64  134   20   13    0    7 |    c = CGJLNQSRT
   19   19   62   13    2    9    7    4    1    0    2 |    d = FV
   17   67   72    2  202    3   14    6    2    0    0 |    e = I
   98   37  128    2    4  366   44   12   10    1   10 |    f = O
   44   10  199    5   10   44 2912   10    1    0    1 |    g = 0
   39   23   91    1   22   27   18   72    6    0    0 |    h = BMP
   18    8   47    0    1   33    3    9   98    0    1 |    i = U
    0    0    5    0    0    0    0    0    0    0    0 |    j = DZ
   24   16   48    0    1   25    1    1    1    0   16 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
75.75	4.25	11.96	0.0	0.66	3.79	2.52	0.47	0.27	0.07	0.27	 a = A
10.19	60.19	18.77	0.4	4.56	3.75	0.94	0.54	0.4	0.0	0.27	 b = E
11.08	7.02	65.56	0.51	2.23	3.66	7.65	1.14	0.74	0.0	0.4	 c = CGJLNQSRT
13.77	13.77	44.93	9.42	1.45	6.52	5.07	2.9	0.72	0.0	1.45	 d = FV
4.42	17.4	18.7	0.52	52.47	0.78	3.64	1.56	0.52	0.0	0.0	 e = I
13.76	5.2	17.98	0.28	0.56	51.4	6.18	1.69	1.4	0.14	1.4	 f = O
1.36	0.31	6.15	0.15	0.31	1.36	89.99	0.31	0.03	0.0	0.03	 g = 0
13.04	7.69	30.43	0.33	7.36	9.03	6.02	24.08	2.01	0.0	0.0	 h = BMP
8.26	3.67	21.56	0.0	0.46	15.14	1.38	4.13	44.95	0.0	0.46	 i = U
0.0	0.0	100.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 j = DZ
18.05	12.03	36.09	0.0	0.75	18.8	0.75	0.75	0.75	0.0	12.03	 k = D
