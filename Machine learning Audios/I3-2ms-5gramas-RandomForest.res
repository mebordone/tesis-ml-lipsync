Timestamp:2013-07-22-22:22:20
Inventario:I3
Intervalo:2ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I3-2ms-5gramas
Num Instances:  91699
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1386





=== Stratified cross-validation ===

Correctly Classified Instances       82603               90.0806 %
Incorrectly Classified Instances      9096                9.9194 %
Kappa statistic                          0.8713
K&B Relative Info Score            7706957.0249 %
K&B Information Score               199981.0047 bits      2.1808 bits/instance
Class complexity | order 0          237910.9549 bits      2.5945 bits/instance
Class complexity | scheme          2643009.3066 bits     28.8227 bits/instance
Complexity improvement     (Sf)    -2405098.3517 bits    -26.2282 bits/instance
Mean absolute error                      0.0315
Root mean squared error                  0.12  
Relative absolute error                 22.3892 %
Root relative squared error             45.2119 %
Coverage of cases (0.95 level)          97.3468 %
Mean rel. region size (0.95 level)      16.9997 %
Total Number of Instances            91699     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,927    0,012    0,935      0,927    0,931      0,918    0,979     0,958     A
                 0,918    0,007    0,915      0,918    0,916      0,910    0,980     0,946     E
                 0,859    0,049    0,799      0,859    0,828      0,788    0,967     0,894     CGJLNQSRT
                 0,680    0,001    0,870      0,680    0,764      0,766    0,939     0,765     FV
                 0,887    0,002    0,936      0,887    0,911      0,908    0,974     0,930     I
                 0,851    0,005    0,932      0,851    0,890      0,883    0,959     0,902     O
                 0,936    0,051    0,921      0,936    0,929      0,882    0,984     0,967     0
                 0,753    0,002    0,916      0,753    0,827      0,826    0,942     0,824     BMP
                 0,925    0,001    0,975      0,925    0,949      0,948    0,986     0,961     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,605     0,009     DZ
                 0,906    0,001    0,960      0,906    0,933      0,932    0,995     0,965     D
Weighted Avg.    0,901    0,032    0,902      0,901    0,901      0,872    0,976     0,937     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 13287    46   540     7    19    29   373    23     4     0     5 |     a = A
    51  6342   311    12     9    24   126    21     4     0    10 |     b = E
   392   194 14425    57    59   111  1461    61    23     2     8 |     c = CGJLNQSRT
    22    31   277   875    13    19    43     3     1     1     1 |     d = FV
    33    51   177    13  3160     9   102    15     0     0     1 |     e = I
    75    29   293     8    13  5656   530    22     3     0    17 |     f = O
   264   165  1528    27    72   161 33548    49     7     3     5 |     g = 0
    55    31   373     4    21    26   201  2196     8     0     0 |     h = BMP
     9    19    83     2     7     8    19     7  1924     0     2 |     i = U
     0     0    30     1     0     0     2     0     0     0     0 |     j = DZ
    26    24    28     0     2    26    17     0     0     0  1190 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
92.7	0.32	3.77	0.05	0.13	0.2	2.6	0.16	0.03	0.0	0.03	 a = A
0.74	91.78	4.5	0.17	0.13	0.35	1.82	0.3	0.06	0.0	0.14	 b = E
2.33	1.16	85.9	0.34	0.35	0.66	8.7	0.36	0.14	0.01	0.05	 c = CGJLNQSRT
1.71	2.41	21.54	68.04	1.01	1.48	3.34	0.23	0.08	0.08	0.08	 d = FV
0.93	1.43	4.97	0.37	88.74	0.25	2.86	0.42	0.0	0.0	0.03	 e = I
1.13	0.44	4.41	0.12	0.2	85.1	7.97	0.33	0.05	0.0	0.26	 f = O
0.74	0.46	4.26	0.08	0.2	0.45	93.63	0.14	0.02	0.01	0.01	 g = 0
1.89	1.06	12.8	0.14	0.72	0.89	6.9	75.33	0.27	0.0	0.0	 h = BMP
0.43	0.91	3.99	0.1	0.34	0.38	0.91	0.34	92.5	0.0	0.1	 i = U
0.0	0.0	90.91	3.03	0.0	0.0	6.06	0.0	0.0	0.0	0.0	 j = DZ
1.98	1.83	2.13	0.0	0.15	1.98	1.29	0.0	0.0	0.0	90.63	 k = D
