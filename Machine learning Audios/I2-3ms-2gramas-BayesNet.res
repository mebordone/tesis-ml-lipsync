Timestamp:2013-08-30-04:37:41
Inventario:I2
Intervalo:3ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I2-3ms-2gramas
Num Instances:  61134
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(21): class 
0Pitch(Hz)(28): class 
0RawPitch(19): class 
0SmPitch(23): class 
0Melogram(st)(35): class 
0ZCross(15): class 
0F1(Hz)(199): class 
0F2(Hz)(282): class 
0F3(Hz)(351): class 
0F4(Hz)(217): class 
1Int(dB)(19): class 
1Pitch(Hz)(27): class 
1RawPitch(21): class 
1SmPitch(24): class 
1Melogram(st)(36): class 
1ZCross(16): class 
1F1(Hz)(208): class 
1F2(Hz)(219): class 
1F3(Hz)(297): class 
1F4(Hz)(205): class 
class(12): 
LogScore Bayes: -2498702.424639364
LogScore BDeu: -2740191.634096108
LogScore MDL: -2699099.248054277
LogScore ENTROPY: -2550786.5164058916
LogScore AIC: -2577701.5164058916




=== Stratified cross-validation ===

Correctly Classified Instances       38643               63.2103 %
Incorrectly Classified Instances     22491               36.7897 %
Kappa statistic                          0.5319
K&B Relative Info Score            3285697.6094 %
K&B Information Score                94305.4951 bits      1.5426 bits/instance
Class complexity | order 0          175446.5243 bits      2.8699 bits/instance
Class complexity | scheme           420384.6187 bits      6.8764 bits/instance
Complexity improvement     (Sf)    -244938.0944 bits     -4.0066 bits/instance
Mean absolute error                      0.0624
Root mean squared error                  0.2268
Relative absolute error                 46.996  %
Root relative squared error             87.9708 %
Coverage of cases (0.95 level)          72.382  %
Mean rel. region size (0.95 level)      12.9053 %
Total Number of Instances            61134     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,667    0,048    0,721      0,667    0,693      0,639    0,920     0,767     A
                 0,081    0,012    0,259      0,081    0,123      0,121    0,788     0,170     LGJKC
                 0,643    0,062    0,459      0,643    0,536      0,499    0,910     0,572     E
                 0,215    0,010    0,243      0,215    0,228      0,218    0,848     0,175     FV
                 0,683    0,030    0,483      0,683    0,566      0,554    0,934     0,591     I
                 0,456    0,026    0,583      0,456    0,512      0,482    0,870     0,542     O
                 0,224    0,026    0,473      0,224    0,304      0,280    0,875     0,415     SNRTY
                 0,899    0,139    0,804      0,899    0,849      0,748    0,959     0,953     0
                 0,201    0,009    0,428      0,201    0,274      0,278    0,835     0,282     BMP
                 0,438    0,035    0,157      0,438    0,231      0,244    0,901     0,236     DZ
                 0,627    0,018    0,448      0,627    0,523      0,517    0,953     0,611     U
                 0,368    0,029    0,349      0,368    0,358      0,330    0,877     0,325     SNRTXY
Weighted Avg.    0,632    0,075    0,623      0,632    0,615      0,558    0,916     0,686     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  6389   122   713   103   159   474   211   495    50   469   121   279 |     a = A
   255   239   423    39   295   177   310   788    39   137    99   162 |     b = LGJKC
   353    73  2970    47   287    84   119   170    81   170    88   179 |     c = E
    54     3   113   186    58    36    57   202    26    78    23    28 |     d = FV
    22    34   307    17  1628     6    76   121    37    38    29    67 |     e = I
   616    46   347    29    74  2034    72   480    31   379   232   123 |     f = O
   366   170   386   117   193   142  1302  2334    92   260   116   330 |     g = SNRTY
   329   142   385   119   246   227   342 21324    83   212    55   261 |     h = 0
   131    42   218    59   211    67    84   339   392   120   158   127 |     i = BMP
   100     3   117    17    46    44    15    30    17   396    38    81 |     j = DZ
    51    23    87    10     7   131    22    22    31    66   869    67 |     k = U
   190    25   399    23   170    67   145   203    37   201   111   914 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
66.66	1.27	7.44	1.07	1.66	4.95	2.2	5.16	0.52	4.89	1.26	2.91	 a = A
8.61	8.07	14.28	1.32	9.96	5.97	10.46	26.59	1.32	4.62	3.34	5.47	 b = LGJKC
7.64	1.58	64.27	1.02	6.21	1.82	2.58	3.68	1.75	3.68	1.9	3.87	 c = E
6.25	0.35	13.08	21.53	6.71	4.17	6.6	23.38	3.01	9.03	2.66	3.24	 d = FV
0.92	1.43	12.89	0.71	68.35	0.25	3.19	5.08	1.55	1.6	1.22	2.81	 e = I
13.8	1.03	7.78	0.65	1.66	45.57	1.61	10.76	0.69	8.49	5.2	2.76	 f = O
6.3	2.93	6.65	2.01	3.32	2.44	22.42	40.19	1.58	4.48	2.0	5.68	 g = SNRTY
1.39	0.6	1.62	0.5	1.04	0.96	1.44	89.88	0.35	0.89	0.23	1.1	 h = 0
6.72	2.16	11.19	3.03	10.83	3.44	4.31	17.4	20.12	6.16	8.11	6.52	 i = BMP
11.06	0.33	12.94	1.88	5.09	4.87	1.66	3.32	1.88	43.81	4.2	8.96	 j = DZ
3.68	1.66	6.28	0.72	0.51	9.45	1.59	1.59	2.24	4.76	62.7	4.83	 k = U
7.65	1.01	16.06	0.93	6.84	2.7	5.84	8.17	1.49	8.09	4.47	36.78	 l = SNRTXY
