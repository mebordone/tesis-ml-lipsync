Timestamp:2013-07-22-22:28:20
Inventario:I3
Intervalo:3ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I3-3ms-5gramas
Num Instances:  61131
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.158





=== Stratified cross-validation ===

Correctly Classified Instances       54092               88.4854 %
Incorrectly Classified Instances      7039               11.5146 %
Kappa statistic                          0.8509
K&B Relative Info Score            4963534.948  %
K&B Information Score               129063.1819 bits      2.1113 bits/instance
Class complexity | order 0          158931.7841 bits      2.5999 bits/instance
Class complexity | scheme          1712139.0867 bits     28.0077 bits/instance
Complexity improvement     (Sf)    -1553207.3026 bits    -25.4079 bits/instance
Mean absolute error                      0.0377
Root mean squared error                  0.1296
Relative absolute error                 26.7279 %
Root relative squared error             48.7976 %
Coverage of cases (0.95 level)          97.4301 %
Mean rel. region size (0.95 level)      18.8825 %
Total Number of Instances            61131     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,917    0,018    0,906      0,917    0,911      0,895    0,978     0,951     A
                 0,894    0,010    0,881      0,894    0,887      0,878    0,978     0,926     E
                 0,842    0,055    0,774      0,842    0,807      0,762    0,960     0,870     CGJLNQSRT
                 0,634    0,002    0,855      0,634    0,728      0,733    0,940     0,727     FV
                 0,858    0,004    0,906      0,858    0,881      0,877    0,972     0,908     I
                 0,829    0,006    0,916      0,829    0,870      0,862    0,957     0,887     O
                 0,929    0,051    0,920      0,929    0,924      0,876    0,983     0,966     0
                 0,709    0,002    0,914      0,709    0,798      0,800    0,944     0,805     BMP
                 0,901    0,001    0,956      0,901    0,928      0,927    0,985     0,947     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,607     0,005     DZ
                 0,837    0,001    0,946      0,837    0,888      0,888    0,995     0,935     D
Weighted Avg.    0,885    0,034    0,887      0,885    0,885      0,854    0,974     0,925     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  8793    48   427     6    15    40   240     9     2     0     5 |     a = A
    76  4131   245     5    27    14    94    15     6     0     8 |     b = E
   363   187  9478    44    54    80   970    49    19     2    10 |     c = CGJLNQSRT
    23    25   213   548     9    17    26     3     0     0     0 |     d = FV
    30    62   151     8  2043     2    73    12     0     0     1 |     e = I
   102    35   239     9    10  3700   340    10     7     0    11 |     f = O
   214   129  1113    15    65   113 22032    23    11     0     7 |     g = 0
    62    38   264     6    24    27   135  1381    11     0     0 |     h = BMP
    14    12    57     0     6    17    23     8  1249     0     0 |     i = U
     1     0    22     0     0     0     0     0     0     0     0 |     j = DZ
    32    23    34     0     3    29    21     1     1     0   737 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
91.74	0.5	4.45	0.06	0.16	0.42	2.5	0.09	0.02	0.0	0.05	 a = A
1.64	89.4	5.3	0.11	0.58	0.3	2.03	0.32	0.13	0.0	0.17	 b = E
3.22	1.66	84.2	0.39	0.48	0.71	8.62	0.44	0.17	0.02	0.09	 c = CGJLNQSRT
2.66	2.89	24.65	63.43	1.04	1.97	3.01	0.35	0.0	0.0	0.0	 d = FV
1.26	2.6	6.34	0.34	85.77	0.08	3.06	0.5	0.0	0.0	0.04	 e = I
2.29	0.78	5.36	0.2	0.22	82.9	7.62	0.22	0.16	0.0	0.25	 f = O
0.9	0.54	4.69	0.06	0.27	0.48	92.88	0.1	0.05	0.0	0.03	 g = 0
3.18	1.95	13.55	0.31	1.23	1.39	6.93	70.89	0.56	0.0	0.0	 h = BMP
1.01	0.87	4.11	0.0	0.43	1.23	1.66	0.58	90.12	0.0	0.0	 i = U
4.35	0.0	95.65	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 j = DZ
3.63	2.61	3.86	0.0	0.34	3.29	2.38	0.11	0.11	0.0	83.65	 k = D
