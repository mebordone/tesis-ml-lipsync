Timestamp:2013-07-22-21:55:10
Inventario:I2
Intervalo:10ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I2-10ms-1gramas
Num Instances:  18296
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.2874





=== Stratified cross-validation ===

Correctly Classified Instances       13729               75.0383 %
Incorrectly Classified Instances      4567               24.9617 %
Kappa statistic                          0.6876
K&B Relative Info Score            1207246.0256 %
K&B Information Score                35120.9266 bits      1.9196 bits/instance
Class complexity | order 0           53208.3385 bits      2.9082 bits/instance
Class complexity | scheme          1537864.3606 bits     84.0547 bits/instance
Complexity improvement     (Sf)    -1484656.0221 bits    -81.1465 bits/instance
Mean absolute error                      0.0584
Root mean squared error                  0.1729
Relative absolute error                 43.421  %
Root relative squared error             66.6794 %
Coverage of cases (0.95 level)          92.0365 %
Mean rel. region size (0.95 level)      22.8916 %
Total Number of Instances            18296     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,843    0,052    0,755      0,843    0,797      0,757    0,948     0,866     A
                 0,396    0,022    0,480      0,396    0,434      0,410    0,824     0,376     LGJKC
                 0,746    0,032    0,664      0,746    0,703      0,677    0,937     0,730     E
                 0,245    0,005    0,427      0,245    0,311      0,316    0,801     0,248     FV
                 0,725    0,011    0,733      0,725    0,729      0,718    0,938     0,742     I
                 0,662    0,023    0,702      0,662    0,681      0,657    0,912     0,708     O
                 0,518    0,053    0,517      0,518    0,518      0,465    0,876     0,517     SNRTY
                 0,921    0,071    0,885      0,921    0,902      0,843    0,969     0,955     0
                 0,451    0,009    0,632      0,451    0,527      0,521    0,865     0,484     BMP
                 0,405    0,003    0,653      0,405    0,500      0,509    0,907     0,456     DZ
                 0,714    0,003    0,851      0,714    0,776      0,775    0,944     0,786     U
                 0,577    0,012    0,669      0,577    0,620      0,606    0,910     0,588     SNRTXY
Weighted Avg.    0,750    0,047    0,743      0,750    0,744      0,705    0,932     0,772     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 2478   48   83    2   15   54  115   98    6    9    5   27 |    a = A
  115  354   67    7   26   40  138  102   13    4    6   22 |    b = LGJKC
   92   51 1073   10   34   26   54   38   18    4    3   35 |    c = E
   22   22   28   64    7   18   63   16    9    2    2    8 |    d = FV
   18   23   72    4  542    6   23   31   20    0    2    7 |    e = I
  110   37   54    6    5  912   54  128   16   18   12   25 |    f = O
  213   88   67   26   37   54  928  298   20    6    9   44 |    g = SNRTY
   82   48   43   11   27   44  243 6259   18    6    3   14 |    h = 0
   49   24   35    7   27   35   65   62  268    4    5   13 |    i = BMP
   35    9   28    2    4   39   13    7    5  111    4   17 |    j = DZ
   14    9   13    2    3   41   16    6   10    2  302    5 |    k = U
   53   24   52    9   12   31   83   30   21    4    2  438 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
84.29	1.63	2.82	0.07	0.51	1.84	3.91	3.33	0.2	0.31	0.17	0.92	 a = A
12.86	39.6	7.49	0.78	2.91	4.47	15.44	11.41	1.45	0.45	0.67	2.46	 b = LGJKC
6.4	3.55	74.62	0.7	2.36	1.81	3.76	2.64	1.25	0.28	0.21	2.43	 c = E
8.43	8.43	10.73	24.52	2.68	6.9	24.14	6.13	3.45	0.77	0.77	3.07	 d = FV
2.41	3.07	9.63	0.53	72.46	0.8	3.07	4.14	2.67	0.0	0.27	0.94	 e = I
7.99	2.69	3.92	0.44	0.36	66.23	3.92	9.3	1.16	1.31	0.87	1.82	 f = O
11.9	4.92	3.74	1.45	2.07	3.02	51.84	16.65	1.12	0.34	0.5	2.46	 g = SNRTY
1.21	0.71	0.63	0.16	0.4	0.65	3.57	92.07	0.26	0.09	0.04	0.21	 h = 0
8.25	4.04	5.89	1.18	4.55	5.89	10.94	10.44	45.12	0.67	0.84	2.19	 i = BMP
12.77	3.28	10.22	0.73	1.46	14.23	4.74	2.55	1.82	40.51	1.46	6.2	 j = DZ
3.31	2.13	3.07	0.47	0.71	9.69	3.78	1.42	2.36	0.47	71.39	1.18	 k = U
6.98	3.16	6.85	1.19	1.58	4.08	10.94	3.95	2.77	0.53	0.26	57.71	 l = SNRTXY
