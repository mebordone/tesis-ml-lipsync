Timestamp:2013-08-30-04:17:50
Inventario:I1
Intervalo:10ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I1-10ms-4gramas
Num Instances:  18293
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(10): class 
0Pitch(Hz)(6): class 
0RawPitch(4): class 
0SmPitch(6): class 
0Melogram(st)(7): class 
0ZCross(10): class 
0F1(Hz)(10): class 
0F2(Hz)(10): class 
0F3(Hz)(7): class 
0F4(Hz)(10): class 
1Int(dB)(12): class 
1Pitch(Hz)(5): class 
1RawPitch(4): class 
1SmPitch(5): class 
1Melogram(st)(10): class 
1ZCross(9): class 
1F1(Hz)(11): class 
1F2(Hz)(10): class 
1F3(Hz)(13): class 
1F4(Hz)(9): class 
2Int(dB)(10): class 
2Pitch(Hz)(6): class 
2RawPitch(6): class 
2SmPitch(5): class 
2Melogram(st)(9): class 
2ZCross(11): class 
2F1(Hz)(13): class 
2F2(Hz)(11): class 
2F3(Hz)(10): class 
2F4(Hz)(9): class 
3Int(dB)(11): class 
3Pitch(Hz)(5): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(8): class 
3ZCross(11): class 
3F1(Hz)(12): class 
3F2(Hz)(10): class 
3F3(Hz)(9): class 
3F4(Hz)(8): class 
class(13): 
LogScore Bayes: -868852.1662309216
LogScore BDeu: -887272.8054261393
LogScore MDL: -885136.2857186495
LogScore ENTROPY: -865939.566259754
LogScore AIC: -869851.5662597542




=== Stratified cross-validation ===

Correctly Classified Instances       10267               56.1253 %
Incorrectly Classified Instances      8026               43.8747 %
Kappa statistic                          0.4609
K&B Relative Info Score             814176.1957 %
K&B Information Score                23880.4954 bits      1.3054 bits/instance
Class complexity | order 0           53631.5533 bits      2.9318 bits/instance
Class complexity | scheme           229148.5338 bits     12.5266 bits/instance
Complexity improvement     (Sf)    -175516.9805 bits     -9.5948 bits/instance
Mean absolute error                      0.0677
Root mean squared error                  0.2426
Relative absolute error                 54.5272 %
Root relative squared error             97.3644 %
Coverage of cases (0.95 level)          63.7129 %
Mean rel. region size (0.95 level)      11.122  %
Total Number of Instances            18293     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,585    0,058    0,658      0,585    0,619      0,553    0,892     0,704     a
                 0,027    0,004    0,176      0,027    0,047      0,057    0,761     0,095     b
                 0,508    0,057    0,433      0,508    0,468      0,420    0,877     0,440     e
                 0,547    0,087    0,088      0,547    0,151      0,192    0,870     0,129     d
                 0,115    0,023    0,069      0,115    0,086      0,072    0,773     0,049     f
                 0,627    0,041    0,394      0,627    0,484      0,470    0,911     0,440     i
                 0,281    0,046    0,108      0,281    0,156      0,148    0,843     0,102     k
                 0,183    0,015    0,126      0,183    0,149      0,140    0,845     0,083     j
                 0,059    0,018    0,262      0,059    0,097      0,083    0,776     0,230     l
                 0,281    0,018    0,565      0,281    0,375      0,366    0,831     0,421     o
                 0,892    0,082    0,865      0,892    0,878      0,805    0,960     0,953     0
                 0,237    0,028    0,349      0,237    0,282      0,251    0,887     0,330     s
                 0,480    0,016    0,412      0,480    0,443      0,430    0,914     0,477     u
Weighted Avg.    0,561    0,054    0,587      0,561    0,557      0,511    0,894     0,613     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1719   10  231  311   78   77  124   29   71  123   91   62   14 |    a = a
   53   16   74   84   36  114   46   26   19   18   55   23   30 |    b = b
  134    2  731  149   23  115   67   53   31   17   46   32   38 |    c = e
   40    3   13  150    3   12    2    6   14   12    6    4    9 |    d = d
   23    3   31   48   30   15   22   12   13   11   26   23    4 |    e = f
    8    9  104   24    5  469   56   15    7    1   25   22    3 |    f = i
    6    3    6    3   15   17  101    4    9    4  159   30    2 |    g = k
   41    4   46   15    9   13    7   38    2    0   11   19    3 |    h = j
  229   15  206  450   84  169  126   52  106   33  166   84   68 |    i = l
  209    4  108  244   19   42   57   17   37  387  118   33  102 |    j = o
   72    9   71   89   87   76   99   10   38   29 6059  143   13 |    k = 0
   45   12   35   89   48   65  200   39   48   10  235  258    4 |    l = s
   32    1   32   56    0    6   27    1   10   40    8    7  203 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
58.47	0.34	7.86	10.58	2.65	2.62	4.22	0.99	2.41	4.18	3.1	2.11	0.48	 a = a
8.92	2.69	12.46	14.14	6.06	19.19	7.74	4.38	3.2	3.03	9.26	3.87	5.05	 b = b
9.32	0.14	50.83	10.36	1.6	8.0	4.66	3.69	2.16	1.18	3.2	2.23	2.64	 c = e
14.6	1.09	4.74	54.74	1.09	4.38	0.73	2.19	5.11	4.38	2.19	1.46	3.28	 d = d
8.81	1.15	11.88	18.39	11.49	5.75	8.43	4.6	4.98	4.21	9.96	8.81	1.53	 e = f
1.07	1.2	13.9	3.21	0.67	62.7	7.49	2.01	0.94	0.13	3.34	2.94	0.4	 f = i
1.67	0.84	1.67	0.84	4.18	4.74	28.13	1.11	2.51	1.11	44.29	8.36	0.56	 g = k
19.71	1.92	22.12	7.21	4.33	6.25	3.37	18.27	0.96	0.0	5.29	9.13	1.44	 h = j
12.81	0.84	11.52	25.17	4.7	9.45	7.05	2.91	5.93	1.85	9.28	4.7	3.8	 i = l
15.18	0.29	7.84	17.72	1.38	3.05	4.14	1.23	2.69	28.1	8.57	2.4	7.41	 j = o
1.06	0.13	1.04	1.31	1.28	1.12	1.46	0.15	0.56	0.43	89.17	2.1	0.19	 k = 0
4.14	1.1	3.22	8.18	4.41	5.97	18.38	3.58	4.41	0.92	21.6	23.71	0.37	 l = s
7.57	0.24	7.57	13.24	0.0	1.42	6.38	0.24	2.36	9.46	1.89	1.65	47.99	 m = u
