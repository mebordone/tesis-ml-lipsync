Timestamp:2013-08-30-03:55:40
Inventario:I1
Intervalo:1ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I1-1ms-1gramas
Num Instances:  183404
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(33): class 
0Pitch(Hz)(95): class 
0RawPitch(63): class 
0SmPitch(93): class 
0Melogram(st)(85): class 
0ZCross(21): class 
0F1(Hz)(2711): class 
0F2(Hz)(3422): class 
0F3(Hz)(3617): class 
0F4(Hz)(3544): class 
class(13): 
LogScore Bayes: -5764935.599293342
LogScore BDeu: -8104551.999159808
LogScore MDL: -7499272.704156041
LogScore ENTROPY: -6422011.449882601
LogScore AIC: -6599785.449882601




=== Stratified cross-validation ===

Correctly Classified Instances      150434               82.0233 %
Incorrectly Classified Instances     32970               17.9767 %
Kappa statistic                          0.7692
K&B Relative Info Score            14187028.2807 %
K&B Information Score               408650.4872 bits      2.2281 bits/instance
Class complexity | order 0          528260.7083 bits      2.8803 bits/instance
Class complexity | scheme           467266.7347 bits      2.5477 bits/instance
Complexity improvement     (Sf)      60993.9737 bits      0.3326 bits/instance
Mean absolute error                      0.0287
Root mean squared error                  0.1548
Relative absolute error                 23.4643 %
Root relative squared error             62.6473 %
Coverage of cases (0.95 level)          86.7282 %
Mean rel. region size (0.95 level)       9.6356 %
Total Number of Instances           183404     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,864    0,013    0,924      0,864    0,893      0,875    0,965     0,935     a
                 0,700    0,006    0,790      0,700    0,742      0,736    0,926     0,752     b
                 0,853    0,012    0,848      0,853    0,850      0,838    0,970     0,901     e
                 0,889    0,009    0,584      0,889    0,705      0,716    0,976     0,837     d
                 0,642    0,005    0,657      0,642    0,650      0,645    0,930     0,655     f
                 0,859    0,007    0,832      0,859    0,845      0,839    0,965     0,881     i
                 0,212    0,004    0,527      0,212    0,302      0,326    0,933     0,344     k
                 0,743    0,004    0,681      0,743    0,711      0,708    0,932     0,750     j
                 0,695    0,017    0,807      0,695    0,747      0,725    0,942     0,803     l
                 0,795    0,009    0,871      0,795    0,831      0,820    0,932     0,859     o
                 0,913    0,138    0,811      0,913    0,859      0,762    0,973     0,964     0
                 0,515    0,011    0,737      0,515    0,606      0,597    0,970     0,717     s
                 0,902    0,005    0,816      0,902    0,857      0,855    0,984     0,930     u
Weighted Avg.    0,820    0,061    0,819      0,820    0,815      0,769    0,962     0,887     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 24680   160   253   242    76   108   277   141   559   244  1566   177    74 |     a = a
    86  4071    56    19    14   142    15    14   104    88  1070    85    50 |     b = b
   157   119 11717   156   110    91    76    68   417    84   576    88    81 |     c = e
    36     3    44  2383     6    10     0     3    51    31    98     4    13 |     d = d
    27     5    46    10  1645    40    11     4    30    19   639    75    10 |     e = f
    22    77    85    26    47  6085    26    21   170    49   357    97    21 |     f = i
    15    19    73     3     3    12   752     6    99    60  1933   530    50 |     g = k
    34     3    33     5     1    12    21  1532    11    11   306    84     9 |     h = j
   513   116   483   188    44   205    58    59 12026   289  2781   417   113 |     i = l
   177   140    85   318    45    64    18    17   268 10518  1493    32    59 |     j = o
   803   389   854   638   491   473   123   348   931   580 65849   322   316 |     k = 0
   130    24    67    66    14    63    27    10   137    67  4476  5445    44 |     l = s
    25    28    22    23     8     8    23    26   100    34    76    33  3731 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
86.42	0.56	0.89	0.85	0.27	0.38	0.97	0.49	1.96	0.85	5.48	0.62	0.26	 a = a
1.48	70.02	0.96	0.33	0.24	2.44	0.26	0.24	1.79	1.51	18.4	1.46	0.86	 b = b
1.14	0.87	85.28	1.14	0.8	0.66	0.55	0.49	3.03	0.61	4.19	0.64	0.59	 c = e
1.34	0.11	1.64	88.85	0.22	0.37	0.0	0.11	1.9	1.16	3.65	0.15	0.48	 d = d
1.05	0.2	1.8	0.39	64.23	1.56	0.43	0.16	1.17	0.74	24.95	2.93	0.39	 e = f
0.31	1.09	1.2	0.37	0.66	85.91	0.37	0.3	2.4	0.69	5.04	1.37	0.3	 f = i
0.42	0.53	2.05	0.08	0.08	0.34	21.15	0.17	2.78	1.69	54.37	14.91	1.41	 g = k
1.65	0.15	1.6	0.24	0.05	0.58	1.02	74.3	0.53	0.53	14.84	4.07	0.44	 h = j
2.97	0.67	2.79	1.09	0.25	1.19	0.34	0.34	69.55	1.67	16.08	2.41	0.65	 i = l
1.34	1.06	0.64	2.4	0.34	0.48	0.14	0.13	2.03	79.48	11.28	0.24	0.45	 j = o
1.11	0.54	1.18	0.88	0.68	0.66	0.17	0.48	1.29	0.8	91.31	0.45	0.44	 k = 0
1.23	0.23	0.63	0.62	0.13	0.6	0.26	0.09	1.3	0.63	42.35	51.51	0.42	 l = s
0.6	0.68	0.53	0.56	0.19	0.19	0.56	0.63	2.42	0.82	1.84	0.8	90.19	 m = u
