Timestamp:2013-07-23-03:02:00
Inventario:I3
Intervalo:6ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I3-6ms-2gramas
Num Instances:  30452
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.2292





=== Stratified cross-validation ===

Correctly Classified Instances       24908               81.7943 %
Incorrectly Classified Instances      5544               18.2057 %
Kappa statistic                          0.766 
K&B Relative Info Score            2243646.3205 %
K&B Information Score                58662.6425 bits      1.9264 bits/instance
Class complexity | order 0           79594.6588 bits      2.6138 bits/instance
Class complexity | scheme          1581198.268  bits     51.9243 bits/instance
Complexity improvement     (Sf)    -1501603.6092 bits    -49.3105 bits/instance
Mean absolute error                      0.0499
Root mean squared error                  0.1578
Relative absolute error                 35.1672 %
Root relative squared error             59.2615 %
Coverage of cases (0.95 level)          95.1399 %
Mean rel. region size (0.95 level)      20.5922 %
Total Number of Instances            30452     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,862    0,035    0,821      0,862    0,841      0,811    0,959     0,900     A
                 0,807    0,022    0,754      0,807    0,780      0,761    0,959     0,825     E
                 0,767    0,078    0,693      0,767    0,728      0,664    0,926     0,760     CGJLNQSRT
                 0,395    0,003    0,632      0,395    0,486      0,494    0,871     0,419     FV
                 0,762    0,008    0,803      0,762    0,782      0,773    0,957     0,812     I
                 0,734    0,016    0,788      0,734    0,760      0,742    0,936     0,794     O
                 0,896    0,054    0,910      0,896    0,903      0,844    0,968     0,946     0
                 0,561    0,007    0,738      0,561    0,638      0,634    0,898     0,614     BMP
                 0,791    0,002    0,917      0,791    0,849      0,848    0,974     0,883     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,541     0,009     DZ
                 0,615    0,002    0,844      0,615    0,711      0,717    0,962     0,734     D
Weighted Avg.    0,818    0,044    0,820      0,818    0,817      0,775    0,951     0,855     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  4160    89   318    16    18    57   139    16     5     1     6 |     a = A
   108  1895   202    10    30    21    47    20     4     0    10 |     b = E
   327   204  4338    23    55   111   510    62    13     2     8 |     c = CGJLNQSRT
    36    29   132   170    13    19    17     9     3     1     1 |     d = FV
    20    81   114     9   919     3    41    16     2     0     1 |     e = I
   127    41   206     9    10  1652   165    14    11     0    16 |     f = O
   184    87   670    21    60   131 10402    43     4     2     6 |     g = 0
    52    42   169     7    34    33    85   550     7     0     1 |     h = BMP
    20    13    52     3     2    31    11    13   551     0     1 |     i = U
     1     0     8     1     0     0     2     0     0     0     0 |     j = DZ
    30    31    54     0     4    39     9     2     1     0   271 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
86.22	1.84	6.59	0.33	0.37	1.18	2.88	0.33	0.1	0.02	0.12	 a = A
4.6	80.74	8.61	0.43	1.28	0.89	2.0	0.85	0.17	0.0	0.43	 b = E
5.78	3.61	76.74	0.41	0.97	1.96	9.02	1.1	0.23	0.04	0.14	 c = CGJLNQSRT
8.37	6.74	30.7	39.53	3.02	4.42	3.95	2.09	0.7	0.23	0.23	 d = FV
1.66	6.72	9.45	0.75	76.2	0.25	3.4	1.33	0.17	0.0	0.08	 e = I
5.64	1.82	9.15	0.4	0.44	73.39	7.33	0.62	0.49	0.0	0.71	 f = O
1.58	0.75	5.77	0.18	0.52	1.13	89.6	0.37	0.03	0.02	0.05	 g = 0
5.31	4.29	17.24	0.71	3.47	3.37	8.67	56.12	0.71	0.0	0.1	 h = BMP
2.87	1.87	7.46	0.43	0.29	4.45	1.58	1.87	79.05	0.0	0.14	 i = U
8.33	0.0	66.67	8.33	0.0	0.0	16.67	0.0	0.0	0.0	0.0	 j = DZ
6.8	7.03	12.24	0.0	0.91	8.84	2.04	0.45	0.23	0.0	61.45	 k = D
