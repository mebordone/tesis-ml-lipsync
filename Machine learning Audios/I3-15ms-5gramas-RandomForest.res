Timestamp:2013-07-22-22:32:20
Inventario:I3
Intervalo:15ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I3-15ms-5gramas
Num Instances:  12185
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  42 4Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  43 4RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  44 4SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  45 4Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  47 4F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  48 4F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  49 4F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  50 4F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3087





=== Stratified cross-validation ===

Correctly Classified Instances        9090               74.5999 %
Incorrectly Classified Instances      3095               25.4001 %
Kappa statistic                          0.675 
K&B Relative Info Score             777844.6163 %
K&B Information Score                20627.0124 bits      1.6928 bits/instance
Class complexity | order 0           32285.6179 bits      2.6496 bits/instance
Class complexity | scheme           586529.2108 bits     48.1353 bits/instance
Complexity improvement     (Sf)    -554243.5929 bits    -45.4857 bits/instance
Mean absolute error                      0.0662
Root mean squared error                  0.1797
Relative absolute error                 46.0719 %
Root relative squared error             67.0581 %
Coverage of cases (0.95 level)          95.5929 %
Mean rel. region size (0.95 level)      25.9559 %
Total Number of Instances            12185     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,802    0,057    0,734      0,802    0,767      0,720    0,947     0,838     A
                 0,658    0,036    0,613      0,658    0,635      0,602    0,927     0,649     E
                 0,725    0,121    0,585      0,725    0,648      0,559    0,899     0,668     CGJLNQSRT
                 0,102    0,002    0,391      0,102    0,161      0,194    0,837     0,183     FV
                 0,610    0,012    0,679      0,610    0,643      0,629    0,936     0,651     I
                 0,579    0,026    0,648      0,579    0,611      0,582    0,913     0,624     O
                 0,911    0,043    0,922      0,911    0,917      0,870    0,979     0,960     0
                 0,261    0,005    0,642      0,261    0,371      0,397    0,869     0,373     BMP
                 0,495    0,003    0,797      0,495    0,610      0,621    0,932     0,659     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,698     0,108     DZ
                 0,181    0,001    0,681      0,181    0,286      0,347    0,876     0,326     D
Weighted Avg.    0,746    0,054    0,748      0,746    0,738      0,694    0,939     0,774     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1596   79  196    3   15   49   46    2    1    0    2 |    a = A
   89  639  161    2   31   23   10    9    3    1    3 |    b = E
  209  114 1686    8   43   63  171   15   12    0    4 |    c = CGJLNQSRT
   18   21   99   18    3   11    3    2    1    0    1 |    d = FV
   18   62   85    1  307    9   13    6    2    0    0 |    e = I
  111   42  148    2    8  544   62   10    9    0    4 |    f = O
   52   21  257    5   17   31 4023    7    1    0    0 |    g = 0
   31   32  147    6   23   22   28  104    5    0    1 |    h = BMP
   18   14   42    1    2   60    2    5  141    0    0 |    i = U
    0    0    4    0    0    0    1    0    0    0    0 |    j = DZ
   33   19   56    0    3   28    2    2    2    0   32 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
80.24	3.97	9.85	0.15	0.75	2.46	2.31	0.1	0.05	0.0	0.1	 a = A
9.17	65.81	16.58	0.21	3.19	2.37	1.03	0.93	0.31	0.1	0.31	 b = E
8.99	4.9	72.52	0.34	1.85	2.71	7.35	0.65	0.52	0.0	0.17	 c = CGJLNQSRT
10.17	11.86	55.93	10.17	1.69	6.21	1.69	1.13	0.56	0.0	0.56	 d = FV
3.58	12.33	16.9	0.2	61.03	1.79	2.58	1.19	0.4	0.0	0.0	 e = I
11.81	4.47	15.74	0.21	0.85	57.87	6.6	1.06	0.96	0.0	0.43	 f = O
1.18	0.48	5.82	0.11	0.39	0.7	91.14	0.16	0.02	0.0	0.0	 g = 0
7.77	8.02	36.84	1.5	5.76	5.51	7.02	26.07	1.25	0.0	0.25	 h = BMP
6.32	4.91	14.74	0.35	0.7	21.05	0.7	1.75	49.47	0.0	0.0	 i = U
0.0	0.0	80.0	0.0	0.0	0.0	20.0	0.0	0.0	0.0	0.0	 j = DZ
18.64	10.73	31.64	0.0	1.69	15.82	1.13	1.13	1.13	0.0	18.08	 k = D
