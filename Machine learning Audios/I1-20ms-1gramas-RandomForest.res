Timestamp:2013-07-22-21:22:29
Inventario:I1
Intervalo:20ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I1-20ms-1gramas
Num Instances:  9129
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.3735





=== Stratified cross-validation ===

Correctly Classified Instances        6062               66.4038 %
Incorrectly Classified Instances      3067               33.5962 %
Kappa statistic                          0.5846
K&B Relative Info Score             515778.0853 %
K&B Information Score                15338.5848 bits      1.6802 bits/instance
Class complexity | order 0           27128.0259 bits      2.9716 bits/instance
Class complexity | scheme          1221856.6698 bits    133.8434 bits/instance
Complexity improvement     (Sf)    -1194728.6439 bits   -130.8718 bits/instance
Mean absolute error                      0.0641
Root mean squared error                  0.1878
Relative absolute error                 51.0466 %
Root relative squared error             74.9527 %
Coverage of cases (0.95 level)          87.4466 %
Mean rel. region size (0.95 level)      22.2427 %
Total Number of Instances             9129     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,781    0,078    0,663      0,781    0,717      0,659    0,909     0,782     a
                 0,314    0,022    0,329      0,314    0,321      0,299    0,773     0,224     b
                 0,606    0,045    0,547      0,606    0,575      0,536    0,895     0,551     e
                 0,116    0,006    0,229      0,116    0,154      0,154    0,779     0,086     d
                 0,080    0,006    0,169      0,080    0,108      0,107    0,690     0,053     f
                 0,561    0,021    0,544      0,561    0,552      0,532    0,892     0,545     i
                 0,149    0,011    0,211      0,149    0,175      0,163    0,681     0,087     k
                 0,220    0,003    0,444      0,220    0,294      0,307    0,782     0,209     j
                 0,395    0,062    0,412      0,395    0,403      0,339    0,800     0,338     l
                 0,514    0,035    0,551      0,514    0,532      0,494    0,867     0,543     o
                 0,919    0,060    0,893      0,919    0,906      0,853    0,969     0,945     0
                 0,448    0,031    0,479      0,448    0,463      0,430    0,857     0,466     s
                 0,459    0,006    0,662      0,459    0,542      0,542    0,902     0,554     u
Weighted Avg.    0,664    0,051    0,649      0,664    0,654      0,609    0,895     0,663     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1175   10   72    3    2   15    6    4   74   61   48   30    5 |    a = a
   34   94   15    4    6   22    4    2   47   21   28    9   13 |    b = b
   90   23  452    5    5   40    9    6   60   24   14   16    2 |    c = e
   22    7   13   16    3    2    0    1   32   30    2    6    4 |    d = d
   20   13   24    2   11    4    5    1   26   10    6   16    0 |    e = f
   16   19   57    0    0  216    0    1   39    3   19   14    1 |    f = i
   22    5   10    0    5    3   27    3   20    7   40   39    0 |    g = k
   23    4   16    1    4   10    3   24    7    7    4    6    0 |    h = j
  154   41   86   20    6   41   18    2  359   63   64   47    9 |    i = l
   99   24   40   13    5    7    3    4   77  366   51    9   14 |    j = o
   45   20   10    1    3   15   15    0   48   29 2975   75    1 |    k = 0
   60    6   18    2   14   21   37    3   51   11   79  247    2 |    l = s
   11   20   13    3    1    1    1    3   31   32    0    2  100 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
78.07	0.66	4.78	0.2	0.13	1.0	0.4	0.27	4.92	4.05	3.19	1.99	0.33	 a = a
11.37	31.44	5.02	1.34	2.01	7.36	1.34	0.67	15.72	7.02	9.36	3.01	4.35	 b = b
12.06	3.08	60.59	0.67	0.67	5.36	1.21	0.8	8.04	3.22	1.88	2.14	0.27	 c = e
15.94	5.07	9.42	11.59	2.17	1.45	0.0	0.72	23.19	21.74	1.45	4.35	2.9	 d = d
14.49	9.42	17.39	1.45	7.97	2.9	3.62	0.72	18.84	7.25	4.35	11.59	0.0	 e = f
4.16	4.94	14.81	0.0	0.0	56.1	0.0	0.26	10.13	0.78	4.94	3.64	0.26	 f = i
12.15	2.76	5.52	0.0	2.76	1.66	14.92	1.66	11.05	3.87	22.1	21.55	0.0	 g = k
21.1	3.67	14.68	0.92	3.67	9.17	2.75	22.02	6.42	6.42	3.67	5.5	0.0	 h = j
16.92	4.51	9.45	2.2	0.66	4.51	1.98	0.22	39.45	6.92	7.03	5.16	0.99	 i = l
13.9	3.37	5.62	1.83	0.7	0.98	0.42	0.56	10.81	51.4	7.16	1.26	1.97	 j = o
1.39	0.62	0.31	0.03	0.09	0.46	0.46	0.0	1.48	0.9	91.91	2.32	0.03	 k = 0
10.89	1.09	3.27	0.36	2.54	3.81	6.72	0.54	9.26	2.0	14.34	44.83	0.36	 l = s
5.05	9.17	5.96	1.38	0.46	0.46	0.46	1.38	14.22	14.68	0.0	0.92	45.87	 m = u
