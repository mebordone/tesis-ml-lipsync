Timestamp:2013-08-30-04:08:44
Inventario:I1
Intervalo:2ms
Ngramas:3
Clasificador:BayesNet
Relation Name:  I1-2ms-3gramas
Num Instances:  91701
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=31 #classindex=30
Network structure (nodes followed by parents)
0Int(dB)(24): class 
0Pitch(Hz)(43): class 
0RawPitch(29): class 
0SmPitch(37): class 
0Melogram(st)(46): class 
0ZCross(17): class 
0F1(Hz)(1647): class 
0F2(Hz)(2312): class 
0F3(Hz)(2304): class 
0F4(Hz)(2524): class 
1Int(dB)(22): class 
1Pitch(Hz)(46): class 
1RawPitch(29): class 
1SmPitch(36): class 
1Melogram(st)(45): class 
1ZCross(16): class 
1F1(Hz)(1612): class 
1F2(Hz)(2243): class 
1F3(Hz)(2379): class 
1F4(Hz)(2445): class 
2Int(dB)(25): class 
2Pitch(Hz)(45): class 
2RawPitch(31): class 
2SmPitch(39): class 
2Melogram(st)(44): class 
2ZCross(17): class 
2F1(Hz)(1439): class 
2F2(Hz)(2328): class 
2F3(Hz)(2303): class 
2F4(Hz)(2522): class 
class(13): 
LogScore Bayes: -7556507.326163681
LogScore BDeu: -1.1904543578382665E7
LogScore MDL: -1.0749215162258627E7
LogScore ENTROPY: -8772130.165293287
LogScore AIC: -9118189.165293286




=== Stratified cross-validation ===

Correctly Classified Instances       73180               79.8028 %
Incorrectly Classified Instances     18521               20.1972 %
Kappa statistic                          0.743 
K&B Relative Info Score            6840624.0335 %
K&B Information Score               197473.557  bits      2.1535 bits/instance
Class complexity | order 0          264696.1546 bits      2.8865 bits/instance
Class complexity | scheme           680851.0185 bits      7.4247 bits/instance
Complexity improvement     (Sf)    -416154.8639 bits     -4.5382 bits/instance
Mean absolute error                      0.0315
Root mean squared error                  0.1693
Relative absolute error                 25.7029 %
Root relative squared error             68.4358 %
Coverage of cases (0.95 level)          82.4538 %
Mean rel. region size (0.95 level)       8.6445 %
Total Number of Instances            91701     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,819    0,015    0,910      0,819    0,862      0,839    0,960     0,917     a
                 0,677    0,006    0,774      0,677    0,722      0,715    0,904     0,722     b
                 0,844    0,018    0,790      0,844    0,816      0,801    0,963     0,870     e
                 0,868    0,015    0,458      0,868    0,600      0,624    0,972     0,789     d
                 0,617    0,007    0,546      0,617    0,580      0,574    0,907     0,606     f
                 0,856    0,011    0,767      0,856    0,809      0,802    0,960     0,855     i
                 0,209    0,007    0,380      0,209    0,270      0,271    0,921     0,295     k
                 0,721    0,006    0,559      0,721    0,630      0,630    0,917     0,704     j
                 0,633    0,016    0,806      0,633    0,709      0,689    0,910     0,749     l
                 0,765    0,010    0,853      0,765    0,806      0,794    0,926     0,835     o
                 0,902    0,125    0,823      0,902    0,861      0,766    0,969     0,960     0
                 0,514    0,016    0,670      0,514    0,582      0,565    0,961     0,668     s
                 0,905    0,006    0,770      0,905    0,832      0,831    0,979     0,917     u
Weighted Avg.    0,798    0,057    0,800      0,798    0,794      0,749    0,953     0,865     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 11735   111   256   262   133   117   127   103   333   184   715   180    77 |     a = a
    42  1972    68    18    10    67    27    11    35    49   510    60    46 |     b = b
    86    61  5833   122    46    68    62    46   169    34   270    78    35 |     c = e
    28     1    37  1168     8     6    11     5    13    17    40     9     3 |     d = d
    23     6    40    18   794    22    15     1    11    13   299    42     2 |     e = f
    11    32    51    25    21  3049    46    16    60    11   164    64    11 |     f = i
    11     8    32    11     5    17   372    16    41    22   898   320    27 |     g = k
    13     0    43    12     1    10    16   746     3     1   134    52     4 |     h = j
   330    72   394   188    24   148    77    64  5498   138  1318   286   143 |     i = l
   139    60   100   219    18    27    56    48   103  5082   712    36    46 |     j = o
   394   178   454   377   325   327   124   233   428   355 32324   196   116 |     k = 0
    82    30    60   111    59   115    24    36    79    46  1881  2724    51 |     l = s
     4    18    11    18    10     3    22     9    47     5    31    19  1883 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
81.87	0.77	1.79	1.83	0.93	0.82	0.89	0.72	2.32	1.28	4.99	1.26	0.54	 a = a
1.44	67.65	2.33	0.62	0.34	2.3	0.93	0.38	1.2	1.68	17.5	2.06	1.58	 b = b
1.24	0.88	84.41	1.77	0.67	0.98	0.9	0.67	2.45	0.49	3.91	1.13	0.51	 c = e
2.08	0.07	2.75	86.78	0.59	0.45	0.82	0.37	0.97	1.26	2.97	0.67	0.22	 d = d
1.79	0.47	3.11	1.4	61.74	1.71	1.17	0.08	0.86	1.01	23.25	3.27	0.16	 e = f
0.31	0.9	1.43	0.7	0.59	85.62	1.29	0.45	1.68	0.31	4.61	1.8	0.31	 f = i
0.62	0.45	1.8	0.62	0.28	0.96	20.9	0.9	2.3	1.24	50.45	17.98	1.52	 g = k
1.26	0.0	4.15	1.16	0.1	0.97	1.55	72.08	0.29	0.1	12.95	5.02	0.39	 h = j
3.8	0.83	4.54	2.17	0.28	1.71	0.89	0.74	63.34	1.59	15.18	3.29	1.65	 i = l
2.09	0.9	1.5	3.3	0.27	0.41	0.84	0.72	1.55	76.47	10.71	0.54	0.69	 j = o
1.1	0.5	1.27	1.05	0.91	0.91	0.35	0.65	1.19	0.99	90.21	0.55	0.32	 k = 0
1.55	0.57	1.13	2.1	1.11	2.17	0.45	0.68	1.49	0.87	35.5	51.42	0.96	 l = s
0.19	0.87	0.53	0.87	0.48	0.14	1.06	0.43	2.26	0.24	1.49	0.91	90.53	 m = u
