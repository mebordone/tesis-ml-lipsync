Timestamp:2013-08-30-04:42:29
Inventario:I2
Intervalo:15ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I2-15ms-2gramas
Num Instances:  12188
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(9): class 
0F1(Hz)(9): class 
0F2(Hz)(9): class 
0F3(Hz)(7): class 
0F4(Hz)(6): class 
1Int(dB)(10): class 
1Pitch(Hz)(6): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(10): class 
1F1(Hz)(10): class 
1F2(Hz)(10): class 
1F3(Hz)(7): class 
1F4(Hz)(7): class 
class(12): 
LogScore Bayes: -269955.35113628773
LogScore BDeu: -276419.918404983
LogScore MDL: -275674.70497597696
LogScore ENTROPY: -268736.1522101491
LogScore AIC: -270211.1522101491




=== Stratified cross-validation ===

Correctly Classified Instances        7094               58.2048 %
Incorrectly Classified Instances      5094               41.7952 %
Kappa statistic                          0.4778
K&B Relative Info Score             576141.9331 %
K&B Information Score                16875.8531 bits      1.3846 bits/instance
Class complexity | order 0           35679.1394 bits      2.9274 bits/instance
Class complexity | scheme            80067.6297 bits      6.5694 bits/instance
Complexity improvement     (Sf)     -44388.4903 bits     -3.642  bits/instance
Mean absolute error                      0.0716
Root mean squared error                  0.2344
Relative absolute error                 52.945  %
Root relative squared error             90.1203 %
Coverage of cases (0.95 level)          71.2258 %
Mean rel. region size (0.95 level)      16.8561 %
Total Number of Instances            12188     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,652    0,070    0,646      0,652    0,649      0,580    0,898     0,713     A
                 0,030    0,009    0,146      0,030    0,050      0,045    0,716     0,096     LGJKC
                 0,571    0,067    0,426      0,571    0,488      0,442    0,882     0,468     E
                 0,040    0,016    0,036      0,040    0,037      0,023    0,771     0,041     FV
                 0,640    0,040    0,408      0,640    0,498      0,485    0,918     0,463     I
                 0,301    0,025    0,506      0,301    0,378      0,353    0,829     0,408     O
                 0,209    0,046    0,335      0,209    0,258      0,202    0,832     0,322     SNRTY
                 0,924    0,115    0,821      0,924    0,869      0,792    0,971     0,962     0
                 0,008    0,004    0,054      0,008    0,013      0,008    0,768     0,093     BMP
                 0,275    0,045    0,085      0,275    0,130      0,130    0,841     0,075     DZ
                 0,477    0,015    0,440      0,477    0,458      0,445    0,914     0,487     U
                 0,173    0,037    0,168      0,173    0,170      0,134    0,826     0,149     SNRTXY
Weighted Avg.    0,582    0,070    0,554      0,582    0,558      0,505    0,894     0,612     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1296   25  166   31   45   79   92   90    0   99    5   61 |    a = A
   67   18   94   18   61   22   79  137    3   37    9   53 |    b = LGJKC
  106   19  554    8   71   18   46   35    5   39   16   54 |    c = E
   20    1   31    7    8   11   29   24    2   23    3   18 |    d = FV
   10    7   81    1  322    1   32   22    1    6    3   17 |    e = I
  172   18   87   13   26  283   43   86    7  109   56   40 |    f = O
  114   16   75   46   68   28  255  404   12   84   21   95 |    g = SNRTY
   52    3   35   40   40   21   94 4082    6   15    3   26 |    h = 0
   40    5   59   19   75   13   40   46    3   43   24   32 |    i = BMP
   30    0   27    3    5   18    6    5    9   50   10   19 |    j = DZ
   22    9   20    0    3   37   11    6    2   18  136   21 |    k = U
   77    2   72   11   65   28   34   37    6   66   23   88 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
65.16	1.26	8.35	1.56	2.26	3.97	4.63	4.52	0.0	4.98	0.25	3.07	 a = A
11.2	3.01	15.72	3.01	10.2	3.68	13.21	22.91	0.5	6.19	1.51	8.86	 b = LGJKC
10.92	1.96	57.05	0.82	7.31	1.85	4.74	3.6	0.51	4.02	1.65	5.56	 c = E
11.3	0.56	17.51	3.95	4.52	6.21	16.38	13.56	1.13	12.99	1.69	10.17	 d = FV
1.99	1.39	16.1	0.2	64.02	0.2	6.36	4.37	0.2	1.19	0.6	3.38	 e = I
18.3	1.91	9.26	1.38	2.77	30.11	4.57	9.15	0.74	11.6	5.96	4.26	 f = O
9.36	1.31	6.16	3.78	5.58	2.3	20.94	33.17	0.99	6.9	1.72	7.8	 g = SNRTY
1.18	0.07	0.79	0.91	0.91	0.48	2.13	92.42	0.14	0.34	0.07	0.59	 h = 0
10.03	1.25	14.79	4.76	18.8	3.26	10.03	11.53	0.75	10.78	6.02	8.02	 i = BMP
16.48	0.0	14.84	1.65	2.75	9.89	3.3	2.75	4.95	27.47	5.49	10.44	 j = DZ
7.72	3.16	7.02	0.0	1.05	12.98	3.86	2.11	0.7	6.32	47.72	7.37	 k = U
15.13	0.39	14.15	2.16	12.77	5.5	6.68	7.27	1.18	12.97	4.52	17.29	 l = SNRTXY
