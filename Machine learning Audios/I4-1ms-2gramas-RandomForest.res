Timestamp:2013-07-22-22:35:45
Inventario:I4
Intervalo:1ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I4-1ms-2gramas
Num Instances:  183403
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1146





=== Stratified cross-validation ===

Correctly Classified Instances      166072               90.5503 %
Incorrectly Classified Instances     17331                9.4497 %
Kappa statistic                          0.8764
K&B Relative Info Score            15751341.7277 %
K&B Information Score               400669.0391 bits      2.1846 bits/instance
Class complexity | order 0          466506.5985 bits      2.5436 bits/instance
Class complexity | scheme          7147485.5212 bits     38.9715 bits/instance
Complexity improvement     (Sf)    -6680978.9226 bits    -36.4279 bits/instance
Mean absolute error                      0.0274
Root mean squared error                  0.1199
Relative absolute error                 17.8072 %
Root relative squared error             43.2144 %
Coverage of cases (0.95 level)          96.1353 %
Mean rel. region size (0.95 level)      15.7016 %
Total Number of Instances           183403     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,908    0,000    0,951      0,908    0,929      0,929    0,969     0,928     C
                 0,941    0,005    0,944      0,941    0,943      0,938    0,982     0,961     E
                 0,721    0,002    0,825      0,721    0,770      0,768    0,922     0,782     FV
                 0,939    0,012    0,949      0,939    0,944      0,931    0,980     0,967     AI
                 0,850    0,034    0,823      0,850    0,836      0,805    0,966     0,903     CDGKNRSYZ
                 0,873    0,004    0,942      0,873    0,906      0,900    0,962     0,918     O
                 0,947    0,058    0,914      0,947    0,930      0,884    0,983     0,969     0
                 0,652    0,007    0,774      0,652    0,708      0,700    0,900     0,727     LT
                 0,960    0,001    0,978      0,960    0,969      0,968    0,993     0,981     U
                 0,787    0,004    0,880      0,787    0,831      0,827    0,936     0,843     MBP
Weighted Avg.    0,906    0,031    0,905      0,906    0,905      0,878    0,973     0,939     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   983    16     2    16    27     3    29     4     1     2 |     a = C
     4 12931    13    97   282    25   276    75     3    34 |     b = E
     2    26  1847    55   441    19   133    18     0    20 |     c = FV
     8   109    37 33452   708    61  1007   163    15    80 |     d = AI
    14   236   192   516 24177   198  2479   417    28   202 |     e = CDGKNRSYZ
     2    40    18    95   393 11553  1040    64     5    24 |     f = O
    14   208    75   583  2073   271 68270   417    18   187 |     g = 0
     5    76    31   232   828    79   981  4314     6    67 |     h = LT
     1     9     1    27    57    10    32    21  3970     9 |     i = U
     1    48    22   163   404    41   464    82    14  4575 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
90.77	1.48	0.18	1.48	2.49	0.28	2.68	0.37	0.09	0.18	 a = C
0.03	94.11	0.09	0.71	2.05	0.18	2.01	0.55	0.02	0.25	 b = E
0.08	1.02	72.12	2.15	17.22	0.74	5.19	0.7	0.0	0.78	 c = FV
0.02	0.31	0.1	93.86	1.99	0.17	2.83	0.46	0.04	0.22	 d = AI
0.05	0.83	0.67	1.81	84.95	0.7	8.71	1.47	0.1	0.71	 e = CDGKNRSYZ
0.02	0.3	0.14	0.72	2.97	87.3	7.86	0.48	0.04	0.18	 f = O
0.02	0.29	0.1	0.81	2.87	0.38	94.67	0.58	0.02	0.26	 g = 0
0.08	1.15	0.47	3.51	12.51	1.19	14.82	65.18	0.09	1.01	 h = LT
0.02	0.22	0.02	0.65	1.38	0.24	0.77	0.51	95.96	0.22	 i = U
0.02	0.83	0.38	2.8	6.95	0.71	7.98	1.41	0.24	78.69	 j = MBP
