Timestamp:2013-07-22-21:52:34
Inventario:I2
Intervalo:3ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I2-3ms-4gramas
Num Instances:  61132
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1811





=== Stratified cross-validation ===

Correctly Classified Instances       52975               86.6567 %
Incorrectly Classified Instances      8157               13.3433 %
Kappa statistic                          0.8316
K&B Relative Info Score            4884284.7673 %
K&B Information Score               140190.0056 bits      2.2932 bits/instance
Class complexity | order 0          175443.7931 bits      2.8699 bits/instance
Class complexity | scheme          2500292.086  bits     40.8999 bits/instance
Complexity improvement     (Sf)    -2324848.2929 bits    -38.03   bits/instance
Mean absolute error                      0.0379
Root mean squared error                  0.1319
Relative absolute error                 28.5429 %
Root relative squared error             51.1871 %
Coverage of cases (0.95 level)          96.2327 %
Mean rel. region size (0.95 level)      18.506  %
Total Number of Instances            61132     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,920    0,021    0,890      0,920    0,905      0,887    0,976     0,947     A
                 0,661    0,013    0,715      0,661    0,687      0,673    0,922     0,714     LGJKC
                 0,896    0,011    0,872      0,896    0,883      0,874    0,979     0,924     E
                 0,634    0,003    0,759      0,634    0,691      0,690    0,932     0,696     FV
                 0,868    0,004    0,887      0,868    0,877      0,872    0,968     0,904     I
                 0,832    0,009    0,879      0,832    0,855      0,844    0,951     0,877     O
                 0,717    0,036    0,679      0,717    0,698      0,665    0,944     0,743     SNRTY
                 0,934    0,060    0,908      0,934    0,921      0,870    0,980     0,962     0
                 0,716    0,004    0,862      0,716    0,782      0,779    0,936     0,790     BMP
                 0,844    0,001    0,932      0,844    0,886      0,885    0,982     0,910     DZ
                 0,893    0,001    0,952      0,893    0,921      0,920    0,984     0,945     U
                 0,812    0,003    0,921      0,812    0,863      0,859    0,961     0,878     SNRTXY
Weighted Avg.    0,867    0,033    0,867      0,867    0,866      0,837    0,968     0,902     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  8814    97    48    10    23    44   222   269    29     6     4    19 |     a = A
   163  1960    64    15    25    45   329   325    15     0     9    13 |     b = LGJKC
    88    52  4139    16    27    18   119   108    18    11     5    20 |     c = E
    30    26    25   548    11    26   144    39     8     0     2     5 |     d = FV
    29    28    50     9  2067     9    69    95    17     1     2     6 |     e = I
    86    48    40    12    13  3714   101   372    31    15    11    20 |     f = O
   271   212   114    60    44    98  4164   747    46     5     4    43 |     g = SNRTY
   254   203   146    29    74   155   614 22156    39    12     8    33 |     h = 0
    76    37    37    15    31    28   152   164  1395     1    10     2 |     i = BMP
    26     5    23     2     3    32    27    13     2   763     0     8 |     j = DZ
    17    25    13     2     8    20    23    24    12     0  1238     4 |     k = U
    49    48    50     4     4    36   167    91     6     5     8  2017 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
91.96	1.01	0.5	0.1	0.24	0.46	2.32	2.81	0.3	0.06	0.04	0.2	 a = A
5.5	66.15	2.16	0.51	0.84	1.52	11.1	10.97	0.51	0.0	0.3	0.44	 b = LGJKC
1.9	1.13	89.57	0.35	0.58	0.39	2.58	2.34	0.39	0.24	0.11	0.43	 c = E
3.47	3.01	2.89	63.43	1.27	3.01	16.67	4.51	0.93	0.0	0.23	0.58	 d = FV
1.22	1.18	2.1	0.38	86.78	0.38	2.9	3.99	0.71	0.04	0.08	0.25	 e = I
1.93	1.08	0.9	0.27	0.29	83.22	2.26	8.34	0.69	0.34	0.25	0.45	 f = O
4.67	3.65	1.96	1.03	0.76	1.69	71.69	12.86	0.79	0.09	0.07	0.74	 g = SNRTY
1.07	0.86	0.62	0.12	0.31	0.65	2.59	93.39	0.16	0.05	0.03	0.14	 h = 0
3.9	1.9	1.9	0.77	1.59	1.44	7.8	8.42	71.61	0.05	0.51	0.1	 i = BMP
2.88	0.55	2.54	0.22	0.33	3.54	2.99	1.44	0.22	84.4	0.0	0.88	 j = DZ
1.23	1.8	0.94	0.14	0.58	1.44	1.66	1.73	0.87	0.0	89.32	0.29	 k = U
1.97	1.93	2.01	0.16	0.16	1.45	6.72	3.66	0.24	0.2	0.32	81.17	 l = SNRTXY
