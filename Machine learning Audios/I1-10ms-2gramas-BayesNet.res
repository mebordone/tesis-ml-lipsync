Timestamp:2013-08-30-04:17:34
Inventario:I1
Intervalo:10ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I1-10ms-2gramas
Num Instances:  18295
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(10): class 
0Pitch(Hz)(6): class 
0RawPitch(6): class 
0SmPitch(5): class 
0Melogram(st)(9): class 
0ZCross(11): class 
0F1(Hz)(13): class 
0F2(Hz)(11): class 
0F3(Hz)(10): class 
0F4(Hz)(9): class 
1Int(dB)(11): class 
1Pitch(Hz)(5): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(8): class 
1ZCross(11): class 
1F1(Hz)(12): class 
1F2(Hz)(10): class 
1F3(Hz)(9): class 
1F4(Hz)(8): class 
class(13): 
LogScore Bayes: -443132.91453701706
LogScore BDeu: -452512.1403040706
LogScore MDL: -451452.11063089984
LogScore ENTROPY: -441696.61385191145
LogScore AIC: -443684.61385191145




=== Stratified cross-validation ===

Correctly Classified Instances       10935               59.7704 %
Incorrectly Classified Instances      7360               40.2296 %
Kappa statistic                          0.4968
K&B Relative Info Score             896957.2009 %
K&B Information Score                26306.4762 bits      1.4379 bits/instance
Class complexity | order 0           53634.4104 bits      2.9316 bits/instance
Class complexity | scheme           120350.8726 bits      6.5783 bits/instance
Complexity improvement     (Sf)     -66716.4621 bits     -3.6467 bits/instance
Mean absolute error                      0.0639
Root mean squared error                  0.2217
Relative absolute error                 51.4317 %
Root relative squared error             88.9871 %
Coverage of cases (0.95 level)          73.02   %
Mean rel. region size (0.95 level)      14.5723 %
Total Number of Instances            18295     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,653    0,065    0,659      0,653    0,656      0,590    0,906     0,738     a
                 0,019    0,004    0,124      0,019    0,032      0,036    0,769     0,095     b
                 0,604    0,066    0,439      0,604    0,508      0,466    0,893     0,494     e
                 0,380    0,047    0,110      0,380    0,171      0,183    0,861     0,112     d
                 0,080    0,017    0,064      0,080    0,071      0,056    0,780     0,047     f
                 0,647    0,037    0,429      0,647    0,516      0,502    0,921     0,495     i
                 0,148    0,014    0,174      0,148    0,160      0,145    0,861     0,115     k
                 0,096    0,006    0,154      0,096    0,118      0,114    0,821     0,078     j
                 0,144    0,040    0,282      0,144    0,190      0,142    0,786     0,245     l
                 0,333    0,021    0,562      0,333    0,418      0,399    0,839     0,442     o
                 0,912    0,107    0,835      0,912    0,872      0,793    0,965     0,957     0
                 0,305    0,027    0,419      0,305    0,353      0,323    0,897     0,345     s
                 0,485    0,018    0,388      0,485    0,431      0,419    0,925     0,489     u
Weighted Avg.    0,598    0,066    0,583      0,598    0,580      0,528    0,902     0,631     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1919    3  244  155   58   55   33   13  146  112  113   71   18 |    a = a
   59   11   83   48   16  103   20   12   62   26   80   25   49 |    b = b
  146   11  868   63   17   93   16   14   73   25   50   30   32 |    c = e
   46    7   36  104    5   10    1    0   21   20    8    2   14 |    d = d
   25    5   36   39   21   12   10    3   21   19   44   19    7 |    e = f
   10    7  131    4    6  484   18    6   15    1   29   29    8 |    f = i
    8    1    5    2   12   16   53    8   16    6  170   59    3 |    g = k
   46    4   50    7    7   20    0   20    8    3   20   18    5 |    h = j
  228   21  250  265   59  177   51   15  257   58  234   79   94 |    i = l
  260    5  125  142   26   28   12    8   81  459  131   24   76 |    j = o
   80    2   72   39   69   72   23    8   89   28 6202   99   14 |    k = 0
   52    8   38   44   32   57   53   23   92   10  344  332    3 |    l = s
   35    4   38   31    2    2   14    0   30   50    6    6  205 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
65.27	0.1	8.3	5.27	1.97	1.87	1.12	0.44	4.97	3.81	3.84	2.41	0.61	 a = a
9.93	1.85	13.97	8.08	2.69	17.34	3.37	2.02	10.44	4.38	13.47	4.21	8.25	 b = b
10.15	0.76	60.36	4.38	1.18	6.47	1.11	0.97	5.08	1.74	3.48	2.09	2.23	 c = e
16.79	2.55	13.14	37.96	1.82	3.65	0.36	0.0	7.66	7.3	2.92	0.73	5.11	 d = d
9.58	1.92	13.79	14.94	8.05	4.6	3.83	1.15	8.05	7.28	16.86	7.28	2.68	 e = f
1.34	0.94	17.51	0.53	0.8	64.71	2.41	0.8	2.01	0.13	3.88	3.88	1.07	 f = i
2.23	0.28	1.39	0.56	3.34	4.46	14.76	2.23	4.46	1.67	47.35	16.43	0.84	 g = k
22.12	1.92	24.04	3.37	3.37	9.62	0.0	9.62	3.85	1.44	9.62	8.65	2.4	 h = j
12.75	1.17	13.98	14.82	3.3	9.9	2.85	0.84	14.37	3.24	13.09	4.42	5.26	 i = l
18.88	0.36	9.08	10.31	1.89	2.03	0.87	0.58	5.88	33.33	9.51	1.74	5.52	 j = o
1.18	0.03	1.06	0.57	1.02	1.06	0.34	0.12	1.31	0.41	91.25	1.46	0.21	 k = 0
4.78	0.74	3.49	4.04	2.94	5.24	4.87	2.11	8.46	0.92	31.62	30.51	0.28	 l = s
8.27	0.95	8.98	7.33	0.47	0.47	3.31	0.0	7.09	11.82	1.42	1.42	48.46	 m = u
