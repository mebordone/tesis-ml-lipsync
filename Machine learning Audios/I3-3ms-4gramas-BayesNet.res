Timestamp:2013-08-30-05:02:05
Inventario:I3
Intervalo:3ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I3-3ms-4gramas
Num Instances:  61132
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(21): class 
0Pitch(Hz)(30): class 
0RawPitch(25): class 
0SmPitch(23): class 
0Melogram(st)(33): class 
0ZCross(17): class 
0F1(Hz)(221): class 
0F2(Hz)(199): class 
0F3(Hz)(391): class 
0F4(Hz)(321): class 
1Int(dB)(22): class 
1Pitch(Hz)(31): class 
1RawPitch(22): class 
1SmPitch(25): class 
1Melogram(st)(33): class 
1ZCross(17): class 
1F1(Hz)(275): class 
1F2(Hz)(217): class 
1F3(Hz)(382): class 
1F4(Hz)(338): class 
2Int(dB)(21): class 
2Pitch(Hz)(29): class 
2RawPitch(22): class 
2SmPitch(24): class 
2Melogram(st)(32): class 
2ZCross(15): class 
2F1(Hz)(210): class 
2F2(Hz)(238): class 
2F3(Hz)(385): class 
2F4(Hz)(357): class 
3Int(dB)(20): class 
3Pitch(Hz)(35): class 
3RawPitch(22): class 
3SmPitch(25): class 
3Melogram(st)(32): class 
3ZCross(16): class 
3F1(Hz)(242): class 
3F2(Hz)(240): class 
3F3(Hz)(298): class 
3F4(Hz)(262): class 
class(11): 
LogScore Bayes: -5112945.002732823
LogScore BDeu: -5627790.327344228
LogScore MDL: -5539116.054060606
LogScore ENTROPY: -5228230.568085887
LogScore AIC: -5284648.568085886




=== Stratified cross-validation ===

Correctly Classified Instances       38526               63.021  %
Incorrectly Classified Instances     22606               36.979  %
Kappa statistic                          0.5251
K&B Relative Info Score            3330115.6779 %
K&B Information Score                86589.8194 bits      1.4164 bits/instance
Class complexity | order 0          158933.1498 bits      2.5998 bits/instance
Class complexity | scheme           816156.7963 bits     13.3507 bits/instance
Complexity improvement     (Sf)    -657223.6465 bits    -10.7509 bits/instance
Mean absolute error                      0.0675
Root mean squared error                  0.2496
Relative absolute error                 47.8453 %
Root relative squared error             93.942  %
Coverage of cases (0.95 level)          66.9633 %
Mean rel. region size (0.95 level)      10.8671 %
Total Number of Instances            61132     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,655    0,044    0,736      0,655    0,693      0,641    0,920     0,767     A
                 0,656    0,061    0,466      0,656    0,545      0,510    0,911     0,576     E
                 0,172    0,032    0,549      0,172    0,262      0,233    0,854     0,497     CGJLNQSRT
                 0,270    0,012    0,243      0,270    0,256      0,245    0,860     0,220     FV
                 0,707    0,033    0,462      0,707    0,559      0,550    0,935     0,604     I
                 0,467    0,026    0,584      0,467    0,519      0,489    0,873     0,545     O
                 0,896    0,154    0,787      0,896    0,838      0,728    0,955     0,939     0
                 0,271    0,011    0,440      0,271    0,335      0,328    0,841     0,313     BMP
                 0,678    0,021    0,429      0,678    0,526      0,526    0,953     0,643     U
                 0,000    0,002    0,000      0,000    0,000      -0,001   0,952     0,004     DZ
                 0,616    0,055    0,141      0,616    0,230      0,276    0,927     0,283     D
Weighted Avg.    0,630    0,082    0,647      0,630    0,613      0,552    0,915     0,715     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  6278   712   371   122   224   478   512    72   156    23   637 |     a = A
   289  3031   229    58   299    84   192    89    76     6   268 |     b = E
   807  1311  1939   219   791   443  3822   237   471    12  1204 |     c = CGJLNQSRT
    58   101    36   233    53    29   208    24    17     3   102 |     d = FV
    29   262   116    23  1684     2   131    31    34    12    58 |     e = I
   532   318   158    35    73  2084   480    53   255     9   466 |     f = O
   313   395   478   179   290   251 21267   117    70    58   305 |     g = 0
   109   195   124    56   187    54   363   527   147     3   183 |     h = BMP
    46    87    46    13     7   102    32    35   940     0    78 |     i = U
     0     0     0     0     0     0    23     0     0     0     0 |     j = DZ
    73    86    32    19    37    43     4    14    24     6   543 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
65.5	7.43	3.87	1.27	2.34	4.99	5.34	0.75	1.63	0.24	6.65	 a = A
6.25	65.59	4.96	1.26	6.47	1.82	4.15	1.93	1.64	0.13	5.8	 b = E
7.17	11.65	17.23	1.95	7.03	3.94	33.96	2.11	4.18	0.11	10.7	 c = CGJLNQSRT
6.71	11.69	4.17	26.97	6.13	3.36	24.07	2.78	1.97	0.35	11.81	 d = FV
1.22	11.0	4.87	0.97	70.7	0.08	5.5	1.3	1.43	0.5	2.43	 e = I
11.92	7.13	3.54	0.78	1.64	46.7	10.76	1.19	5.71	0.2	10.44	 f = O
1.32	1.67	2.01	0.75	1.22	1.06	89.65	0.49	0.3	0.24	1.29	 g = 0
5.6	10.01	6.37	2.87	9.6	2.77	18.63	27.05	7.55	0.15	9.39	 h = BMP
3.32	6.28	3.32	0.94	0.51	7.36	2.31	2.53	67.82	0.0	5.63	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
8.29	9.76	3.63	2.16	4.2	4.88	0.45	1.59	2.72	0.68	61.63	 k = D
