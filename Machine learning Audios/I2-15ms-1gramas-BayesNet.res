Timestamp:2013-08-30-04:42:26
Inventario:I2
Intervalo:15ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I2-15ms-1gramas
Num Instances:  12189
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(10): class 
0Pitch(Hz)(6): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(10): class 
0F1(Hz)(10): class 
0F2(Hz)(10): class 
0F3(Hz)(7): class 
0F4(Hz)(7): class 
class(12): 
LogScore Bayes: -148737.5920160263
LogScore BDeu: -152249.16072347862
LogScore MDL: -151832.34965638726
LogScore ENTROPY: -148111.37128385587
LogScore AIC: -148902.37128385587




=== Stratified cross-validation ===

Correctly Classified Instances        7336               60.1854 %
Incorrectly Classified Instances      4853               39.8146 %
Kappa statistic                          0.4969
K&B Relative Info Score             597814.268  %
K&B Information Score                17508.3407 bits      1.4364 bits/instance
Class complexity | order 0           35680.6035 bits      2.9273 bits/instance
Class complexity | scheme            46207.5291 bits      3.7909 bits/instance
Complexity improvement     (Sf)     -10526.9256 bits     -0.8636 bits/instance
Mean absolute error                      0.0725
Root mean squared error                  0.2213
Relative absolute error                 53.5569 %
Root relative squared error             85.0745 %
Coverage of cases (0.95 level)          78.981  %
Mean rel. region size (0.95 level)      24.2493 %
Total Number of Instances            12189     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,725    0,091    0,608      0,725    0,662      0,592    0,905     0,728     A
                 0,012    0,006    0,091      0,012    0,021      0,015    0,726     0,099     LGJKC
                 0,627    0,080    0,404      0,627    0,491      0,450    0,893     0,497     E
                 0,017    0,006    0,042      0,017    0,024      0,017    0,769     0,039     FV
                 0,636    0,036    0,430      0,636    0,513      0,498    0,925     0,516     I
                 0,373    0,034    0,481      0,373    0,421      0,382    0,834     0,416     O
                 0,184    0,038    0,347      0,184    0,240      0,195    0,851     0,316     SNRTY
                 0,936    0,125    0,809      0,936    0,868      0,790    0,974     0,965     0
                 0,018    0,005    0,104      0,018    0,030      0,030    0,781     0,109     BMP
                 0,082    0,012    0,096      0,082    0,089      0,076    0,843     0,089     DZ
                 0,456    0,013    0,458      0,456    0,457      0,444    0,923     0,495     U
                 0,183    0,028    0,219      0,183    0,199      0,168    0,829     0,171     SNRTXY
Weighted Avg.    0,602    0,077    0,544      0,602    0,562      0,509    0,901     0,622     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1443   10  170    7   36   92   73   96    2   13    5   42 |    a = A
   95    7  110    4   63   27   84  139    3   14   11   41 |    b = LGJKC
  121   10  609    3   72   22   31   35    4   13   11   40 |    c = E
   26    3   43    3    7   17   19   32    3    8    4   12 |    d = FV
   15    5   94    0  320    0   26   20    1    3    2   17 |    e = I
  219    4  112    3   15  351   35   95    8   21   51   26 |    f = O
  168   22  108   19   58   43  224  452   17   24   12   71 |    g = SNRTY
   69    2   41   18   38   24   72 4134    4    3    3   10 |    h = 0
   47    4   68    8   66   33   32   55    7   13   29   37 |    i = BMP
   45    1   41    1    7   33    5    6    3   15    6   19 |    j = DZ
   29    2   27    1    2   54    6    5    5    7  130   17 |    k = U
   95    7   86    5   61   33   39   38   10   22   20   93 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
72.55	0.5	8.55	0.35	1.81	4.63	3.67	4.83	0.1	0.65	0.25	2.11	 a = A
15.89	1.17	18.39	0.67	10.54	4.52	14.05	23.24	0.5	2.34	1.84	6.86	 b = LGJKC
12.46	1.03	62.72	0.31	7.42	2.27	3.19	3.6	0.41	1.34	1.13	4.12	 c = E
14.69	1.69	24.29	1.69	3.95	9.6	10.73	18.08	1.69	4.52	2.26	6.78	 d = FV
2.98	0.99	18.69	0.0	63.62	0.0	5.17	3.98	0.2	0.6	0.4	3.38	 e = I
23.3	0.43	11.91	0.32	1.6	37.34	3.72	10.11	0.85	2.23	5.43	2.77	 f = O
13.79	1.81	8.87	1.56	4.76	3.53	18.39	37.11	1.4	1.97	0.99	5.83	 g = SNRTY
1.56	0.05	0.93	0.41	0.86	0.54	1.63	93.57	0.09	0.07	0.07	0.23	 h = 0
11.78	1.0	17.04	2.01	16.54	8.27	8.02	13.78	1.75	3.26	7.27	9.27	 i = BMP
24.73	0.55	22.53	0.55	3.85	18.13	2.75	3.3	1.65	8.24	3.3	10.44	 j = DZ
10.18	0.7	9.47	0.35	0.7	18.95	2.11	1.75	1.75	2.46	45.61	5.96	 k = U
18.66	1.38	16.9	0.98	11.98	6.48	7.66	7.47	1.96	4.32	3.93	18.27	 l = SNRTXY
