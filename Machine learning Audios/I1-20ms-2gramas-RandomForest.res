Timestamp:2013-07-22-21:22:40
Inventario:I1
Intervalo:20ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I1-20ms-2gramas
Num Instances:  9128
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3778





=== Stratified cross-validation ===

Correctly Classified Instances        6112               66.9588 %
Incorrectly Classified Instances      3016               33.0412 %
Kappa statistic                          0.592 
K&B Relative Info Score             524563.7323 %
K&B Information Score                15601.2169 bits      1.7092 bits/instance
Class complexity | order 0           27126.5299 bits      2.9718 bits/instance
Class complexity | scheme           968000.5196 bits    106.0474 bits/instance
Complexity improvement     (Sf)    -940873.9897 bits   -103.0756 bits/instance
Mean absolute error                      0.0643
Root mean squared error                  0.1844
Relative absolute error                 51.1923 %
Root relative squared error             73.5973 %
Coverage of cases (0.95 level)          90.184  %
Mean rel. region size (0.95 level)      23.7444 %
Total Number of Instances             9128     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,774    0,084    0,646      0,774    0,704      0,643    0,922     0,794     a
                 0,268    0,020    0,308      0,268    0,286      0,264    0,791     0,233     b
                 0,613    0,047    0,538      0,613    0,573      0,533    0,900     0,553     e
                 0,101    0,005    0,222      0,101    0,139      0,141    0,756     0,109     d
                 0,109    0,006    0,231      0,109    0,148      0,150    0,709     0,073     f
                 0,574    0,020    0,559      0,574    0,567      0,547    0,907     0,545     i
                 0,221    0,010    0,320      0,221    0,261      0,254    0,773     0,180     k
                 0,147    0,002    0,457      0,147    0,222      0,254    0,825     0,264     j
                 0,414    0,065    0,413      0,414    0,414      0,349    0,823     0,346     l
                 0,548    0,036    0,563      0,548    0,555      0,518    0,874     0,530     o
                 0,908    0,057    0,897      0,908    0,902      0,848    0,974     0,949     0
                 0,543    0,024    0,594      0,543    0,567      0,542    0,889     0,560     s
                 0,463    0,005    0,673      0,463    0,549      0,550    0,897     0,556     u
Weighted Avg.    0,670    0,051    0,658      0,670    0,660      0,616    0,908     0,676     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
 1165   13   73    5    4   18    6    5   90   53   47   21    5 |    a = a
   32   80   22    2    2   30    4    1   53   29   26   10    8 |    b = b
   97   19  457    5    5   24    5    3   76   26   14   13    2 |    c = e
   33    5   15   14    2    2    0    0   27   28    5    1    6 |    d = d
   23    6   21    0   15    5    2    0   25   13   13   14    1 |    e = f
   17   17   64    0    2  221    2    0   38    1   15    7    1 |    f = i
   14    7    8    1    3    5   40    1   21   11   40   29    1 |    g = k
   30    5   25    2    0    5    1   16    8    6    3    8    0 |    h = j
  169   48   86   12    9   42   15    3  377   42   60   35   12 |    i = l
  100   23   35   17    2    3    7    3   58  390   54   12    8 |    j = o
   57   20   17    0    8   18   22    1   55   49 2937   50    2 |    k = 0
   55    7   18    2   12   19   18    2   55    5   56  299    3 |    l = s
   11   10    9    3    1    3    3    0   29   40    4    4  101 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
77.41	0.86	4.85	0.33	0.27	1.2	0.4	0.33	5.98	3.52	3.12	1.4	0.33	 a = a
10.7	26.76	7.36	0.67	0.67	10.03	1.34	0.33	17.73	9.7	8.7	3.34	2.68	 b = b
13.0	2.55	61.26	0.67	0.67	3.22	0.67	0.4	10.19	3.49	1.88	1.74	0.27	 c = e
23.91	3.62	10.87	10.14	1.45	1.45	0.0	0.0	19.57	20.29	3.62	0.72	4.35	 d = d
16.67	4.35	15.22	0.0	10.87	3.62	1.45	0.0	18.12	9.42	9.42	10.14	0.72	 e = f
4.42	4.42	16.62	0.0	0.52	57.4	0.52	0.0	9.87	0.26	3.9	1.82	0.26	 f = i
7.73	3.87	4.42	0.55	1.66	2.76	22.1	0.55	11.6	6.08	22.1	16.02	0.55	 g = k
27.52	4.59	22.94	1.83	0.0	4.59	0.92	14.68	7.34	5.5	2.75	7.34	0.0	 h = j
18.57	5.27	9.45	1.32	0.99	4.62	1.65	0.33	41.43	4.62	6.59	3.85	1.32	 i = l
14.04	3.23	4.92	2.39	0.28	0.42	0.98	0.42	8.15	54.78	7.58	1.69	1.12	 j = o
1.76	0.62	0.53	0.0	0.25	0.56	0.68	0.03	1.7	1.51	90.76	1.55	0.06	 k = 0
9.98	1.27	3.27	0.36	2.18	3.45	3.27	0.36	9.98	0.91	10.16	54.26	0.54	 l = s
5.05	4.59	4.13	1.38	0.46	1.38	1.38	0.0	13.3	18.35	1.83	1.83	46.33	 m = u
