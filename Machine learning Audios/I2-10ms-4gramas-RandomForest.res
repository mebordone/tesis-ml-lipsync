Timestamp:2013-07-22-21:56:12
Inventario:I2
Intervalo:10ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I2-10ms-4gramas
Num Instances:  18293
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.3032





=== Stratified cross-validation ===

Correctly Classified Instances       14002               76.5429 %
Incorrectly Classified Instances      4291               23.4571 %
Kappa statistic                          0.7073
K&B Relative Info Score            1226496.2694 %
K&B Information Score                35684.7015 bits      1.9507 bits/instance
Class complexity | order 0           53204.0531 bits      2.9084 bits/instance
Class complexity | scheme          1054660.8432 bits     57.6538 bits/instance
Complexity improvement     (Sf)    -1001456.7901 bits    -54.7454 bits/instance
Mean absolute error                      0.0593
Root mean squared error                  0.1687
Relative absolute error                 44.0928 %
Root relative squared error             65.0545 %
Coverage of cases (0.95 level)          94.7029 %
Mean rel. region size (0.95 level)      24.693  %
Total Number of Instances            18293     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,859    0,054    0,753      0,859    0,802      0,764    0,957     0,881     A
                 0,456    0,022    0,518      0,456    0,485      0,461    0,864     0,419     LGJKC
                 0,757    0,033    0,659      0,757    0,704      0,679    0,950     0,736     E
                 0,268    0,004    0,479      0,268    0,344      0,352    0,871     0,288     FV
                 0,715    0,013    0,696      0,715    0,705      0,693    0,952     0,737     I
                 0,678    0,022    0,718      0,678    0,697      0,673    0,926     0,720     O
                 0,603    0,049    0,572      0,603    0,587      0,541    0,908     0,593     SNRTY
                 0,918    0,057    0,905      0,918    0,912      0,859    0,975     0,956     0
                 0,456    0,008    0,659      0,456    0,539      0,536    0,888     0,503     BMP
                 0,401    0,002    0,748      0,401    0,523      0,543    0,932     0,529     DZ
                 0,664    0,003    0,857      0,664    0,748      0,749    0,948     0,764     U
                 0,613    0,009    0,744      0,613    0,672      0,663    0,933     0,668     SNRTXY
Weighted Avg.    0,765    0,041    0,763      0,765    0,761      0,725    0,946     0,791     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 2525   43   77    7   18   42   95   87   16    3    2   25 |    a = A
  110  408   45   12   26   38  114   96   15    2    7   21 |    b = LGJKC
  105   53 1088    4   44   22   50   30   15    7    4   16 |    c = E
   29   19   29   70    4   15   69   14    5    0    2    5 |    d = FV
   15   21   90    6  535    6   28   26   11    0    1    9 |    e = I
  121   30   52    4   15  933   56  100   22   11    9   24 |    f = O
  172   86   83   19   37   54 1079  203   23    5    7   22 |    g = SNRTY
  106   47   46    8   30   63  224 6237   13    1    5   15 |    h = 0
   62   24   40    9   31   32   55   56  271    1    8    5 |    i = BMP
   41    9   29    0    7   37   18    5    4  110    0   14 |    j = DZ
   21   24   18    2    6   37   13    6    9    2  281    4 |    k = U
   46   24   54    5   16   21   85   29    7    5    2  465 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
85.88	1.46	2.62	0.24	0.61	1.43	3.23	2.96	0.54	0.1	0.07	0.85	 a = A
12.3	45.64	5.03	1.34	2.91	4.25	12.75	10.74	1.68	0.22	0.78	2.35	 b = LGJKC
7.3	3.69	75.66	0.28	3.06	1.53	3.48	2.09	1.04	0.49	0.28	1.11	 c = E
11.11	7.28	11.11	26.82	1.53	5.75	26.44	5.36	1.92	0.0	0.77	1.92	 d = FV
2.01	2.81	12.03	0.8	71.52	0.8	3.74	3.48	1.47	0.0	0.13	1.2	 e = I
8.79	2.18	3.78	0.29	1.09	67.76	4.07	7.26	1.6	0.8	0.65	1.74	 f = O
9.61	4.8	4.64	1.06	2.07	3.02	60.28	11.34	1.28	0.28	0.39	1.23	 g = SNRTY
1.56	0.69	0.68	0.12	0.44	0.93	3.3	91.79	0.19	0.01	0.07	0.22	 h = 0
10.44	4.04	6.73	1.52	5.22	5.39	9.26	9.43	45.62	0.17	1.35	0.84	 i = BMP
14.96	3.28	10.58	0.0	2.55	13.5	6.57	1.82	1.46	40.15	0.0	5.11	 j = DZ
4.96	5.67	4.26	0.47	1.42	8.75	3.07	1.42	2.13	0.47	66.43	0.95	 k = U
6.06	3.16	7.11	0.66	2.11	2.77	11.2	3.82	0.92	0.66	0.26	61.26	 l = SNRTXY
