Timestamp:2013-08-30-04:16:44
Inventario:I1
Intervalo:6ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I1-6ms-4gramas
Num Instances:  30450
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(14): class 
0Pitch(Hz)(10): class 
0RawPitch(12): class 
0SmPitch(10): class 
0Melogram(st)(14): class 
0ZCross(12): class 
0F1(Hz)(16): class 
0F2(Hz)(18): class 
0F3(Hz)(20): class 
0F4(Hz)(16): class 
1Int(dB)(15): class 
1Pitch(Hz)(12): class 
1RawPitch(11): class 
1SmPitch(10): class 
1Melogram(st)(15): class 
1ZCross(12): class 
1F1(Hz)(22): class 
1F2(Hz)(15): class 
1F3(Hz)(19): class 
1F4(Hz)(16): class 
2Int(dB)(16): class 
2Pitch(Hz)(12): class 
2RawPitch(11): class 
2SmPitch(11): class 
2Melogram(st)(14): class 
2ZCross(13): class 
2F1(Hz)(18): class 
2F2(Hz)(19): class 
2F3(Hz)(20): class 
2F4(Hz)(17): class 
3Int(dB)(14): class 
3Pitch(Hz)(12): class 
3RawPitch(13): class 
3SmPitch(10): class 
3Melogram(st)(14): class 
3ZCross(13): class 
3F1(Hz)(22): class 
3F2(Hz)(20): class 
3F3(Hz)(18): class 
3F4(Hz)(23): class 
class(13): 
LogScore Bayes: -1811168.1952215277
LogScore BDeu: -1850700.4493799417
LogScore MDL: -1846029.2606947096
LogScore ENTROPY: -1808455.640381125
LogScore AIC: -1815734.6403811246




=== Stratified cross-validation ===

Correctly Classified Instances       17724               58.2069 %
Incorrectly Classified Instances     12726               41.7931 %
Kappa statistic                          0.4811
K&B Relative Info Score            1435395.6327 %
K&B Information Score                41771.668  bits      1.3718 bits/instance
Class complexity | order 0           88595.3932 bits      2.9095 bits/instance
Class complexity | scheme           400718.7436 bits     13.1599 bits/instance
Complexity improvement     (Sf)    -312123.3503 bits    -10.2504 bits/instance
Mean absolute error                      0.0644
Root mean squared error                  0.2371
Relative absolute error                 52.2137 %
Root relative squared error             95.5045 %
Coverage of cases (0.95 level)          65.2479 %
Mean rel. region size (0.95 level)      10.6907 %
Total Number of Instances            30450     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,595    0,044    0,717      0,595    0,650      0,595    0,905     0,730     a
                 0,041    0,006    0,195      0,041    0,068      0,076    0,787     0,113     b
                 0,565    0,059    0,443      0,565    0,497      0,453    0,888     0,492     e
                 0,543    0,072    0,102      0,543    0,171      0,211    0,879     0,163     d
                 0,142    0,021    0,090      0,142    0,110      0,097    0,807     0,077     f
                 0,667    0,040    0,407      0,667    0,506      0,497    0,919     0,477     i
                 0,261    0,036    0,127      0,261    0,171      0,159    0,864     0,110     k
                 0,251    0,009    0,245      0,251    0,248      0,239    0,851     0,167     j
                 0,104    0,027    0,293      0,104    0,153      0,125    0,790     0,247     l
                 0,323    0,020    0,562      0,323    0,410      0,393    0,839     0,440     o
                 0,889    0,095    0,852      0,889    0,870      0,788    0,957     0,952     0
                 0,237    0,023    0,391      0,237    0,295      0,272    0,896     0,360     s
                 0,516    0,024    0,335      0,516    0,406      0,399    0,926     0,500     u
Weighted Avg.    0,582    0,058    0,603      0,582    0,577      0,528    0,901     0,635     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  2871    14   385   465   109   124   127    35   158   192   182    85    78 |     a = a
    66    40    95   146    63   175    55    29    46    27   123    32    83 |     b = b
   170    25  1326   170    54   176    60    53    81    42    76    41    73 |     c = e
    46     7    33   246     9    25     3     2    22    18    11     5    26 |     d = d
    23     7    59    76    61    27    30    11    12    17    64    33    10 |     e = f
    13    14   153    19    16   805    54    18    23     1    42    32    16 |     f = i
    11     5    11     5    18    32   155     3    13     9   274    56     1 |     g = k
    52     3    57    21    15    28    20    88     6     4    25    22    10 |     h = j
   250    30   396   616   105   288   149    34   303    92   345   123   196 |     i = l
   301    13   190   343    29    55    49    23    83   726   211    46   182 |     j = o
   104    29   169   144   132   143   128    27   114    85 10321   176    36 |     k = 0
    67     9    63    92    50    92   363    36   138    14   431   422     5 |     l = s
    32     9    56    75    17     7    27     0    34    65    10     5   360 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
59.5	0.29	7.98	9.64	2.26	2.57	2.63	0.73	3.27	3.98	3.77	1.76	1.62	 a = a
6.73	4.08	9.69	14.9	6.43	17.86	5.61	2.96	4.69	2.76	12.55	3.27	8.47	 b = b
7.24	1.07	56.5	7.24	2.3	7.5	2.56	2.26	3.45	1.79	3.24	1.75	3.11	 c = e
10.15	1.55	7.28	54.3	1.99	5.52	0.66	0.44	4.86	3.97	2.43	1.1	5.74	 d = d
5.35	1.63	13.72	17.67	14.19	6.28	6.98	2.56	2.79	3.95	14.88	7.67	2.33	 e = f
1.08	1.16	12.69	1.58	1.33	66.75	4.48	1.49	1.91	0.08	3.48	2.65	1.33	 f = i
1.85	0.84	1.85	0.84	3.04	5.4	26.14	0.51	2.19	1.52	46.21	9.44	0.17	 g = k
14.81	0.85	16.24	5.98	4.27	7.98	5.7	25.07	1.71	1.14	7.12	6.27	2.85	 h = j
8.54	1.02	13.53	21.05	3.59	9.84	5.09	1.16	10.35	3.14	11.79	4.2	6.7	 i = l
13.37	0.58	8.44	15.24	1.29	2.44	2.18	1.02	3.69	32.25	9.37	2.04	8.09	 j = o
0.9	0.25	1.46	1.24	1.14	1.23	1.1	0.23	0.98	0.73	88.91	1.52	0.31	 k = 0
3.76	0.51	3.54	5.16	2.81	5.16	20.37	2.02	7.74	0.79	24.19	23.68	0.28	 l = s
4.59	1.29	8.03	10.76	2.44	1.0	3.87	0.0	4.88	9.33	1.43	0.72	51.65	 m = u
