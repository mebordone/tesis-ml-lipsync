Timestamp:2013-07-22-22:50:30
Inventario:I4
Intervalo:2ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I4-2ms-1gramas
Num Instances:  91703
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1308





=== Stratified cross-validation ===

Correctly Classified Instances       81877               89.285  %
Incorrectly Classified Instances      9826               10.715  %
Kappa statistic                          0.8592
K&B Relative Info Score            7541392.2023 %
K&B Information Score               192217.2739 bits      2.0961 bits/instance
Class complexity | order 0          233717.1164 bits      2.5486 bits/instance
Class complexity | scheme          1299353.1077 bits     14.1691 bits/instance
Complexity improvement     (Sf)    -1065635.9913 bits    -11.6205 bits/instance
Mean absolute error                      0.0358
Root mean squared error                  0.1281
Relative absolute error                 23.2053 %
Root relative squared error             46.1469 %
Coverage of cases (0.95 level)          97.8212 %
Mean rel. region size (0.95 level)      20.559  %
Total Number of Instances            91703     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,873    0,000    0,933      0,873    0,902      0,902    0,972     0,915     C
                 0,914    0,007    0,918      0,914    0,916      0,910    0,985     0,947     E
                 0,659    0,001    0,916      0,659    0,767      0,775    0,962     0,759     FV
                 0,921    0,014    0,942      0,921    0,931      0,915    0,984     0,964     AI
                 0,838    0,043    0,784      0,838    0,810      0,774    0,974     0,900     CDGKNRSYZ
                 0,839    0,004    0,947      0,839    0,890      0,884    0,977     0,912     O
                 0,956    0,072    0,895      0,956    0,924      0,875    0,987     0,980     0
                 0,571    0,003    0,861      0,571    0,687      0,693    0,940     0,700     LT
                 0,925    0,001    0,967      0,925    0,946      0,945    0,987     0,961     U
                 0,735    0,002    0,938      0,735    0,824      0,826    0,961     0,831     MBP
Weighted Avg.    0,893    0,038    0,895      0,893    0,891      0,862    0,981     0,938     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   475    12     0    15    23     2    15     0     2     0 |     a = C
     8  6318    11    70   237    17   201    25     8    15 |     b = E
     0    26   848    23   314    15    54     4     0     2 |     c = FV
    10    90    11 16481   513    36   644    64    10    35 |     d = AI
     5   181    21   322 11957    98  1551    99    13    25 |     e = CDGKNRSYZ
     1    34    10    94   289  5579   592    24     5    18 |     f = O
     8   125    17   229  1049    59 34254    54    12    26 |     g = 0
     0    49     5   150   539    42   623  1897     7    11 |     h = LT
     2    12     0    23    48    14    33    14  1925     9 |     i = U
     0    33     3    93   279    28   307    21     8  2143 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
87.32	2.21	0.0	2.76	4.23	0.37	2.76	0.0	0.37	0.0	 a = C
0.12	91.43	0.16	1.01	3.43	0.25	2.91	0.36	0.12	0.22	 b = E
0.0	2.02	65.94	1.79	24.42	1.17	4.2	0.31	0.0	0.16	 c = FV
0.06	0.5	0.06	92.1	2.87	0.2	3.6	0.36	0.06	0.2	 d = AI
0.04	1.27	0.15	2.26	83.78	0.69	10.87	0.69	0.09	0.18	 e = CDGKNRSYZ
0.02	0.51	0.15	1.41	4.35	83.95	8.91	0.36	0.08	0.27	 f = O
0.02	0.35	0.05	0.64	2.93	0.16	95.59	0.15	0.03	0.07	 g = 0
0.0	1.47	0.15	4.51	16.22	1.26	18.75	57.09	0.21	0.33	 h = LT
0.1	0.58	0.0	1.11	2.31	0.67	1.59	0.67	92.55	0.43	 i = U
0.0	1.13	0.1	3.19	9.57	0.96	10.53	0.72	0.27	73.52	 j = MBP
