Timestamp:2013-08-30-05:05:56
Inventario:I3
Intervalo:15ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I3-15ms-4gramas
Num Instances:  12186
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(5): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(5): class 
0F1(Hz)(9): class 
0F2(Hz)(9): class 
0F3(Hz)(5): class 
0F4(Hz)(7): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(8): class 
1ZCross(7): class 
1F1(Hz)(10): class 
1F2(Hz)(9): class 
1F3(Hz)(7): class 
1F4(Hz)(7): class 
2Int(dB)(10): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(9): class 
2ZCross(9): class 
2F1(Hz)(10): class 
2F2(Hz)(11): class 
2F3(Hz)(8): class 
2F4(Hz)(7): class 
3Int(dB)(9): class 
3Pitch(Hz)(6): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(10): class 
3F1(Hz)(11): class 
3F2(Hz)(9): class 
3F3(Hz)(8): class 
3F4(Hz)(7): class 
class(11): 
LogScore Bayes: -534355.1281789689
LogScore BDeu: -545794.9522192585
LogScore MDL: -545022.4942574335
LogScore ENTROPY: -532505.0930049431
LogScore AIC: -535166.0930049431




=== Stratified cross-validation ===

Correctly Classified Instances        6691               54.9073 %
Incorrectly Classified Instances      5495               45.0927 %
Kappa statistic                          0.4392
K&B Relative Info Score             537161.275  %
K&B Information Score                14245.0311 bits      1.169  bits/instance
Class complexity | order 0           32287.0828 bits      2.6495 bits/instance
Class complexity | scheme           143497.7207 bits     11.7756 bits/instance
Complexity improvement     (Sf)    -111210.6379 bits     -9.1261 bits/instance
Mean absolute error                      0.0826
Root mean squared error                  0.2702
Relative absolute error                 57.5189 %
Root relative squared error            100.8387 %
Coverage of cases (0.95 level)          61.2178 %
Mean rel. region size (0.95 level)      12.6046 %
Total Number of Instances            12186     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,526    0,062    0,622      0,526    0,570      0,497    0,877     0,649     A
                 0,425    0,054    0,407      0,425    0,416      0,364    0,862     0,387     E
                 0,188    0,052    0,459      0,188    0,267      0,199    0,766     0,392     CGJLNQSRT
                 0,090    0,017    0,074      0,090    0,082      0,067    0,788     0,048     FV
                 0,614    0,042    0,389      0,614    0,476      0,462    0,913     0,422     I
                 0,224    0,015    0,552      0,224    0,319      0,320    0,815     0,367     O
                 0,905    0,107    0,828      0,905    0,865      0,785    0,962     0,949     0
                 0,033    0,008    0,121      0,033    0,051      0,047    0,770     0,097     BMP
                 0,449    0,015    0,416      0,449    0,432      0,418    0,907     0,463     U
                 0,200    0,014    0,006      0,200    0,011      0,032    0,913     0,004     DZ
                 0,672    0,134    0,069      0,672    0,125      0,185    0,869     0,097     D
Weighted Avg.    0,549    0,069    0,596      0,549    0,550      0,491    0,878     0,617     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1047  157  132   30   51   77   95    7   16   22  355 |    a = A
  115  413   63   11   95   10   46    7   31   14  166 |    b = E
  229  185  438   74  174   22  491   55   39   66  552 |    c = CGJLNQSRT
   23   23   25   16    2    8   20    2    3    5   50 |    d = FV
   13   57   33    4  309    0   26    8    4    9   40 |    e = I
  136   70   70   10   33  211   95    3   59    9  244 |    f = O
   33   37  141   43   42   15 3996   11    5   31   61 |    g = 0
   42   47   32   23   72    6   41   13   18   10   95 |    h = BMP
   19   20   12    1    7   33   12    0  128    9   44 |    i = U
    0    0    1    0    0    0    3    0    0    1    0 |    j = DZ
   27    5    8    3    9    0    0    1    5    0  119 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
52.64	7.89	6.64	1.51	2.56	3.87	4.78	0.35	0.8	1.11	17.85	 a = A
11.84	42.53	6.49	1.13	9.78	1.03	4.74	0.72	3.19	1.44	17.1	 b = E
9.85	7.96	18.84	3.18	7.48	0.95	21.12	2.37	1.68	2.84	23.74	 c = CGJLNQSRT
12.99	12.99	14.12	9.04	1.13	4.52	11.3	1.13	1.69	2.82	28.25	 d = FV
2.58	11.33	6.56	0.8	61.43	0.0	5.17	1.59	0.8	1.79	7.95	 e = I
14.47	7.45	7.45	1.06	3.51	22.45	10.11	0.32	6.28	0.96	25.96	 f = O
0.75	0.84	3.19	0.97	0.95	0.34	90.51	0.25	0.11	0.7	1.38	 g = 0
10.53	11.78	8.02	5.76	18.05	1.5	10.28	3.26	4.51	2.51	23.81	 h = BMP
6.67	7.02	4.21	0.35	2.46	11.58	4.21	0.0	44.91	3.16	15.44	 i = U
0.0	0.0	20.0	0.0	0.0	0.0	60.0	0.0	0.0	20.0	0.0	 j = DZ
15.25	2.82	4.52	1.69	5.08	0.0	0.0	0.56	2.82	0.0	67.23	 k = D
