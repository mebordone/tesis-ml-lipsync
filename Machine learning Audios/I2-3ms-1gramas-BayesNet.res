Timestamp:2013-08-30-04:37:29
Inventario:I2
Intervalo:3ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I2-3ms-1gramas
Num Instances:  61135
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(19): class 
0Pitch(Hz)(27): class 
0RawPitch(21): class 
0SmPitch(24): class 
0Melogram(st)(36): class 
0ZCross(16): class 
0F1(Hz)(208): class 
0F2(Hz)(219): class 
0F3(Hz)(297): class 
0F4(Hz)(205): class 
class(12): 
LogScore Bayes: -1305873.0930048616
LogScore BDeu: -1418784.3780177224
LogScore MDL: -1399869.7674942014
LogScore ENTROPY: -1329584.3615894832
LogScore AIC: -1342339.3615894832




=== Stratified cross-validation ===

Correctly Classified Instances       38760               63.4007 %
Incorrectly Classified Instances     22375               36.5993 %
Kappa statistic                          0.5312
K&B Relative Info Score            3315543.4875 %
K&B Information Score                95160.9391 bits      1.5566 bits/instance
Class complexity | order 0          175447.8898 bits      2.8698 bits/instance
Class complexity | scheme           231023.087  bits      3.7789 bits/instance
Complexity improvement     (Sf)     -55575.1971 bits     -0.9091 bits/instance
Mean absolute error                      0.0651
Root mean squared error                  0.2157
Relative absolute error                 49.0183 %
Root relative squared error             83.6805 %
Coverage of cases (0.95 level)          78.4166 %
Mean rel. region size (0.95 level)      18.5577 %
Total Number of Instances            61135     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,710    0,060    0,689      0,710    0,700      0,643    0,921     0,766     A
                 0,085    0,011    0,275      0,085    0,130      0,130    0,783     0,173     LGJKC
                 0,659    0,069    0,438      0,659    0,526      0,491    0,910     0,572     E
                 0,161    0,005    0,301      0,161    0,210      0,212    0,839     0,167     FV
                 0,662    0,028    0,494      0,662    0,566      0,552    0,935     0,603     I
                 0,464    0,030    0,551      0,464    0,504      0,470    0,868     0,538     O
                 0,192    0,026    0,440      0,192    0,267      0,245    0,875     0,405     SNRTY
                 0,902    0,149    0,794      0,902    0,844      0,739    0,959     0,953     0
                 0,187    0,008    0,448      0,187    0,264      0,275    0,835     0,287     BMP
                 0,352    0,021    0,201      0,352    0,256      0,251    0,898     0,232     DZ
                 0,574    0,014    0,482      0,574    0,524      0,514    0,953     0,602     U
                 0,355    0,027    0,362      0,355    0,358      0,331    0,875     0,328     SNRTXY
Weighted Avg.    0,634    0,081    0,611      0,634    0,609      0,551    0,915     0,685     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  6810   111   718    52   146   454   204   516    31   231    87   225 |     a = A
   301   252   496    20   256   201   271   824    40    77    78   147 |     b = LGJKC
   421    68  3045    28   269    80   118   176    71   101    68   176 |     c = E
    76     5   139   139    58    45    62   215    23    61    16    25 |     d = FV
    27    39   365     7  1578     9    87   121    27    34    28    60 |     e = I
   743    51   370    18    67  2069    70   491    31   252   174   127 |     f = O
   463   160   431    60   189   199  1114  2589    72   139    90   302 |     g = SNRTY
   418   141   407    73   233   242   343 21392    60   136    56   225 |     h = 0
   164    49   266    29   196   100    87   342   365    90   128   132 |     i = BMP
   127     6   161    16    36    68    13    33    13   318    38    75 |     j = DZ
    69    17   100     4    15   192    29    22    28    53   796    61 |     k = U
   265    19   453    16   151    94   134   230    54    93    94   882 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
71.05	1.16	7.49	0.54	1.52	4.74	2.13	5.38	0.32	2.41	0.91	2.35	 a = A
10.16	8.5	16.74	0.67	8.64	6.78	9.15	27.81	1.35	2.6	2.63	4.96	 b = LGJKC
9.11	1.47	65.89	0.61	5.82	1.73	2.55	3.81	1.54	2.19	1.47	3.81	 c = E
8.8	0.58	16.09	16.09	6.71	5.21	7.18	24.88	2.66	7.06	1.85	2.89	 d = FV
1.13	1.64	15.32	0.29	66.25	0.38	3.65	5.08	1.13	1.43	1.18	2.52	 e = I
16.65	1.14	8.29	0.4	1.5	46.36	1.57	11.0	0.69	5.65	3.9	2.85	 f = O
7.97	2.75	7.42	1.03	3.25	3.43	19.18	44.58	1.24	2.39	1.55	5.2	 g = SNRTY
1.76	0.59	1.72	0.31	0.98	1.02	1.45	90.16	0.25	0.57	0.24	0.95	 h = 0
8.42	2.52	13.66	1.49	10.06	5.13	4.47	17.56	18.74	4.62	6.57	6.78	 i = BMP
14.05	0.66	17.81	1.77	3.98	7.52	1.44	3.65	1.44	35.18	4.2	8.3	 j = DZ
4.98	1.23	7.22	0.29	1.08	13.85	2.09	1.59	2.02	3.82	57.43	4.4	 k = U
10.66	0.76	18.23	0.64	6.08	3.78	5.39	9.26	2.17	3.74	3.78	35.49	 l = SNRTXY
