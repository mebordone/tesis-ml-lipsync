Timestamp:2013-08-30-04:17:04
Inventario:I1
Intervalo:6ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I1-6ms-5gramas
Num Instances:  30449
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  45 4Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(15): class 
0Pitch(Hz)(12): class 
0RawPitch(12): class 
0SmPitch(11): class 
0Melogram(st)(17): class 
0ZCross(11): class 
0F1(Hz)(17): class 
0F2(Hz)(22): class 
0F3(Hz)(20): class 
0F4(Hz)(18): class 
1Int(dB)(14): class 
1Pitch(Hz)(10): class 
1RawPitch(12): class 
1SmPitch(10): class 
1Melogram(st)(14): class 
1ZCross(12): class 
1F1(Hz)(16): class 
1F2(Hz)(18): class 
1F3(Hz)(20): class 
1F4(Hz)(16): class 
2Int(dB)(15): class 
2Pitch(Hz)(12): class 
2RawPitch(11): class 
2SmPitch(10): class 
2Melogram(st)(15): class 
2ZCross(12): class 
2F1(Hz)(22): class 
2F2(Hz)(15): class 
2F3(Hz)(19): class 
2F4(Hz)(16): class 
3Int(dB)(16): class 
3Pitch(Hz)(12): class 
3RawPitch(11): class 
3SmPitch(11): class 
3Melogram(st)(14): class 
3ZCross(13): class 
3F1(Hz)(18): class 
3F2(Hz)(19): class 
3F3(Hz)(20): class 
3F4(Hz)(17): class 
4Int(dB)(14): class 
4Pitch(Hz)(12): class 
4RawPitch(13): class 
4SmPitch(10): class 
4Melogram(st)(14): class 
4ZCross(13): class 
4F1(Hz)(22): class 
4F2(Hz)(20): class 
4F3(Hz)(18): class 
4F4(Hz)(23): class 
class(13): 
LogScore Bayes: -2271113.1096601333
LogScore BDeu: -2320981.372146771
LogScore MDL: -2315001.552061784
LogScore ENTROPY: -2267697.8618269265
LogScore AIC: -2276861.8618269265




=== Stratified cross-validation ===

Correctly Classified Instances       17483               57.4173 %
Incorrectly Classified Instances     12966               42.5827 %
Kappa statistic                          0.4738
K&B Relative Info Score            1390746.9865 %
K&B Information Score                40475.9556 bits      1.3293 bits/instance
Class complexity | order 0           88594.0019 bits      2.9096 bits/instance
Class complexity | scheme           498334.1625 bits     16.3662 bits/instance
Complexity improvement     (Sf)    -409740.1607 bits    -13.4566 bits/instance
Mean absolute error                      0.0657
Root mean squared error                  0.2429
Relative absolute error                 53.2687 %
Root relative squared error             97.8472 %
Coverage of cases (0.95 level)          62.8526 %
Mean rel. region size (0.95 level)      10.091  %
Total Number of Instances            30449     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,581    0,043    0,718      0,581    0,643      0,588    0,901     0,720     a
                 0,046    0,005    0,221      0,046    0,076      0,088    0,785     0,112     b
                 0,548    0,058    0,440      0,548    0,488      0,443    0,882     0,470     e
                 0,589    0,080    0,100      0,589    0,171      0,218    0,884     0,170     d
                 0,128    0,022    0,076      0,128    0,095      0,082    0,807     0,079     f
                 0,664    0,040    0,405      0,664    0,503      0,494    0,916     0,467     i
                 0,283    0,044    0,114      0,283    0,163      0,154    0,858     0,106     k
                 0,256    0,010    0,223      0,256    0,239      0,230    0,857     0,166     j
                 0,087    0,023    0,290      0,087    0,134      0,113    0,786     0,242     l
                 0,313    0,019    0,567      0,313    0,403      0,388    0,837     0,435     o
                 0,882    0,088    0,861      0,882    0,871      0,791    0,955     0,950     0
                 0,235    0,024    0,374      0,235    0,288      0,262    0,893     0,352     s
                 0,508    0,024    0,336      0,508    0,404      0,396    0,921     0,492     u
Weighted Avg.    0,574    0,055    0,606      0,574    0,573      0,525    0,898     0,629     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  2804    13   381   503   120   131   162    43   145   190   171    80    82 |     a = a
    70    45    97   159    65   173    64    29    26    25   111    37    79 |     b = b
   167    26  1286   198    61   173    86    68    52    42    72    43    73 |     c = e
    42     7    33   267     9    22     3     3    18    15    10     5    19 |     d = d
    22     6    60    78    55    28    39    12    16    17    54    33    10 |     e = f
    15    10   147    25    14   801    72    17    15     1    39    35    15 |     f = i
    11     2     7     5    21    35   168     2    11    11   270    49     1 |     g = k
    48     2    59    22    14    29    19    90     4     7    21    29     7 |     h = j
   252    26   390   690   119   271   179    36   254    78   314   133   185 |     i = l
   277    11   185   367    34    59    71    27    82   704   201    48   185 |     j = o
   105    30   159   166   130   153   176    32   103    78 10237   203    35 |     k = 0
    58     9    62   110    63    96   398    43   121    14   380   418    10 |     l = s
    32    17    56    75    17     8    34     1    28    60    11     4   354 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
58.11	0.27	7.9	10.42	2.49	2.72	3.36	0.89	3.01	3.94	3.54	1.66	1.7	 a = a
7.14	4.59	9.9	16.22	6.63	17.65	6.53	2.96	2.65	2.55	11.33	3.78	8.06	 b = b
7.12	1.11	54.79	8.44	2.6	7.37	3.66	2.9	2.22	1.79	3.07	1.83	3.11	 c = e
9.27	1.55	7.28	58.94	1.99	4.86	0.66	0.66	3.97	3.31	2.21	1.1	4.19	 d = d
5.12	1.4	13.95	18.14	12.79	6.51	9.07	2.79	3.72	3.95	12.56	7.67	2.33	 e = f
1.24	0.83	12.19	2.07	1.16	66.42	5.97	1.41	1.24	0.08	3.23	2.9	1.24	 f = i
1.85	0.34	1.18	0.84	3.54	5.9	28.33	0.34	1.85	1.85	45.53	8.26	0.17	 g = k
13.68	0.57	16.81	6.27	3.99	8.26	5.41	25.64	1.14	1.99	5.98	8.26	1.99	 h = j
8.61	0.89	13.32	23.57	4.07	9.26	6.12	1.23	8.68	2.66	10.73	4.54	6.32	 i = l
12.31	0.49	8.22	16.3	1.51	2.62	3.15	1.2	3.64	31.27	8.93	2.13	8.22	 j = o
0.9	0.26	1.37	1.43	1.12	1.32	1.52	0.28	0.89	0.67	88.2	1.75	0.3	 k = 0
3.25	0.51	3.48	6.17	3.54	5.39	22.33	2.41	6.79	0.79	21.32	23.46	0.56	 l = s
4.59	2.44	8.03	10.76	2.44	1.15	4.88	0.14	4.02	8.61	1.58	0.57	50.79	 m = u
