Timestamp:2013-07-22-22:30:37
Inventario:I3
Intervalo:10ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I3-10ms-4gramas
Num Instances:  18293
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.2691





=== Stratified cross-validation ===

Correctly Classified Instances       14377               78.5929 %
Incorrectly Classified Instances      3916               21.4071 %
Kappa statistic                          0.7253
K&B Relative Info Score            1250555.0635 %
K&B Information Score                32946.8187 bits      1.8011 bits/instance
Class complexity | order 0           48172.4848 bits      2.6334 bits/instance
Class complexity | scheme           804383.209  bits     43.9722 bits/instance
Complexity improvement     (Sf)    -756210.7241 bits    -41.3388 bits/instance
Mean absolute error                      0.0597
Root mean squared error                  0.1692
Relative absolute error                 41.7785 %
Root relative squared error             63.346  %
Coverage of cases (0.95 level)          95.9711 %
Mean rel. region size (0.95 level)      24.199  %
Total Number of Instances            18293     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,838    0,045    0,781      0,838    0,808      0,771    0,957     0,876     A
                 0,724    0,028    0,687      0,724    0,705      0,679    0,947     0,737     E
                 0,753    0,101    0,634      0,753    0,688      0,612    0,917     0,717     CGJLNQSRT
                 0,215    0,002    0,629      0,215    0,320      0,363    0,860     0,301     FV
                 0,686    0,009    0,760      0,686    0,721      0,711    0,946     0,734     I
                 0,654    0,019    0,733      0,654    0,691      0,669    0,925     0,717     O
                 0,910    0,048    0,918      0,910    0,914      0,864    0,975     0,957     0
                 0,429    0,006    0,706      0,429    0,534      0,539    0,901     0,516     BMP
                 0,641    0,003    0,847      0,641    0,729      0,731    0,955     0,764     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,623     0,008     DZ
                 0,383    0,001    0,797      0,383    0,518      0,549    0,946     0,536     D
Weighted Avg.    0,786    0,048    0,789      0,786    0,782      0,740    0,949     0,820     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 2463   74  228    3   16   57   76   17    3    0    3 |    a = A
   99 1041  185    4   37   24   27   11    6    0    4 |    b = E
  269  141 2591   12   46   80  271   17   12    2    2 |    c = CGJLNQSRT
   17   22  124   56    9   15    8    4    5    0    1 |    d = FV
   15   76  103    4  513    4   24    7    2    0    0 |    e = I
  113   40  182    2    6  901   96   19    7    0   11 |    f = O
   79   43  386    3   21   55 6184   20    3    0    1 |    g = 0
   43   32  160    4   23   24   43  255    9    0    1 |    h = BMP
   18   18   56    1    1   41    4   10  271    0    3 |    i = U
    0    1    7    0    0    0    0    0    0    0    0 |    j = DZ
   39   27   62    0    3   28    2    1    2    0  102 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
83.78	2.52	7.76	0.1	0.54	1.94	2.59	0.58	0.1	0.0	0.1	 a = A
6.88	72.39	12.87	0.28	2.57	1.67	1.88	0.76	0.42	0.0	0.28	 b = E
7.81	4.1	75.25	0.35	1.34	2.32	7.87	0.49	0.35	0.06	0.06	 c = CGJLNQSRT
6.51	8.43	47.51	21.46	3.45	5.75	3.07	1.53	1.92	0.0	0.38	 d = FV
2.01	10.16	13.77	0.53	68.58	0.53	3.21	0.94	0.27	0.0	0.0	 e = I
8.21	2.9	13.22	0.15	0.44	65.43	6.97	1.38	0.51	0.0	0.8	 f = O
1.16	0.63	5.68	0.04	0.31	0.81	91.01	0.29	0.04	0.0	0.01	 g = 0
7.24	5.39	26.94	0.67	3.87	4.04	7.24	42.93	1.52	0.0	0.17	 h = BMP
4.26	4.26	13.24	0.24	0.24	9.69	0.95	2.36	64.07	0.0	0.71	 i = U
0.0	12.5	87.5	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	 j = DZ
14.66	10.15	23.31	0.0	1.13	10.53	0.75	0.38	0.75	0.0	38.35	 k = D
