Timestamp:2013-08-30-04:13:12
Inventario:I1
Intervalo:3ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I1-3ms-1gramas
Num Instances:  61135
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(21): class 
0Pitch(Hz)(29): class 
0RawPitch(20): class 
0SmPitch(28): class 
0Melogram(st)(37): class 
0ZCross(15): class 
0F1(Hz)(301): class 
0F2(Hz)(250): class 
0F3(Hz)(382): class 
0F4(Hz)(354): class 
class(13): 
LogScore Bayes: -1338881.0652392344
LogScore BDeu: -1514632.3940818987
LogScore MDL: -1482007.245521397
LogScore ENTROPY: -1379717.3208009521
LogScore AIC: -1398280.3208009517




=== Stratified cross-validation ===

Correctly Classified Instances       40358               66.0146 %
Incorrectly Classified Instances     20777               33.9854 %
Kappa statistic                          0.5661
K&B Relative Info Score            3472212.046  %
K&B Information Score               100473.4071 bits      1.6435 bits/instance
Class complexity | order 0          176880.0177 bits      2.8933 bits/instance
Class complexity | scheme           219249.2029 bits      3.5863 bits/instance
Complexity improvement     (Sf)     -42369.1852 bits     -0.693  bits/instance
Mean absolute error                      0.0573
Root mean squared error                  0.1995
Relative absolute error                 46.7175 %
Root relative squared error             80.5517 %
Coverage of cases (0.95 level)          82.0954 %
Mean rel. region size (0.95 level)      16.8325 %
Total Number of Instances            61135     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,710    0,053    0,712      0,710    0,711      0,658    0,924     0,782     a
                 0,198    0,008    0,460      0,198    0,276      0,287    0,836     0,290     b
                 0,661    0,064    0,460      0,661    0,542      0,507    0,916     0,597     e
                 0,409    0,018    0,259      0,409    0,317      0,313    0,909     0,301     d
                 0,166    0,006    0,300      0,166    0,213      0,215    0,841     0,180     f
                 0,664    0,024    0,525      0,664    0,586      0,572    0,938     0,616     i
                 0,105    0,004    0,326      0,105    0,159      0,176    0,882     0,194     k
                 0,280    0,005    0,398      0,280    0,329      0,328    0,876     0,302     j
                 0,343    0,048    0,429      0,343    0,381      0,327    0,832     0,385     l
                 0,482    0,024    0,610      0,482    0,538      0,510    0,876     0,571     o
                 0,902    0,130    0,814      0,902    0,856      0,760    0,960     0,953     0
                 0,370    0,017    0,570      0,370    0,449      0,433    0,922     0,483     s
                 0,608    0,012    0,539      0,608    0,571      0,562    0,954     0,634     u
Weighted Avg.    0,660    0,073    0,647      0,660    0,645      0,589    0,920     0,706     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  6809    49   637   198    62   128    41    43   563   366   507   109    73 |     a = a
   157   385   237    61    33   188    18    24   249   102   345    41   108 |     b = b
   393    70  3054    94    35   225    27    41   328    80   175    48    51 |     c = e
   111     9   127   370     6    24     1     7   123    59    30     6    31 |     d = d
    59    24   132    38   143    53     9     2   103    44   211    33    13 |     e = f
    23    24   361    28    12  1582    19    17   112    10   118    47    29 |     f = i
    24    12    46     1     9    37   125    10    67    18   615   210    18 |     g = k
   100     4   110     9     9    52    12   194    44    19    95    37     8 |     h = j
   584    99   963   198    40   343    33    46  1994   252   899   201   167 |     i = l
   730    36   339   216    21    49    20    26   194  2150   491    28   163 |     j = o
   361    69   409   133    77   217    40    56   476   229 21396   220    43 |     k = 0
   139    32   130    51    26   116    30    20   279    33  1366  1314    16 |     l = s
    68    24    98    30     4     2     8     1   111   165    21    12   842 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
71.04	0.51	6.65	2.07	0.65	1.34	0.43	0.45	5.87	3.82	5.29	1.14	0.76	 a = a
8.06	19.76	12.17	3.13	1.69	9.65	0.92	1.23	12.78	5.24	17.71	2.1	5.54	 b = b
8.5	1.51	66.09	2.03	0.76	4.87	0.58	0.89	7.1	1.73	3.79	1.04	1.1	 c = e
12.28	1.0	14.05	40.93	0.66	2.65	0.11	0.77	13.61	6.53	3.32	0.66	3.43	 d = d
6.83	2.78	15.28	4.4	16.55	6.13	1.04	0.23	11.92	5.09	24.42	3.82	1.5	 e = f
0.97	1.01	15.16	1.18	0.5	66.41	0.8	0.71	4.7	0.42	4.95	1.97	1.22	 f = i
2.01	1.01	3.86	0.08	0.76	3.1	10.49	0.84	5.62	1.51	51.59	17.62	1.51	 g = k
14.43	0.58	15.87	1.3	1.3	7.5	1.73	27.99	6.35	2.74	13.71	5.34	1.15	 h = j
10.04	1.7	16.55	3.4	0.69	5.89	0.57	0.79	34.27	4.33	15.45	3.45	2.87	 i = l
16.36	0.81	7.6	4.84	0.47	1.1	0.45	0.58	4.35	48.17	11.0	0.63	3.65	 j = o
1.52	0.29	1.72	0.56	0.32	0.91	0.17	0.24	2.01	0.97	90.18	0.93	0.18	 k = 0
3.91	0.9	3.66	1.44	0.73	3.27	0.84	0.56	7.85	0.93	38.46	36.99	0.45	 l = s
4.91	1.73	7.07	2.16	0.29	0.14	0.58	0.07	8.01	11.9	1.52	0.87	60.75	 m = u
