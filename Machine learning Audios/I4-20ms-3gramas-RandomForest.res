Timestamp:2013-07-22-23:07:36
Inventario:I4
Intervalo:20ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I4-20ms-3gramas
Num Instances:  9127
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.3586





=== Stratified cross-validation ===

Correctly Classified Instances        6345               69.519  %
Incorrectly Classified Instances      2782               30.481  %
Kappa statistic                          0.6082
K&B Relative Info Score             523964.2396 %
K&B Information Score                13713.5997 bits      1.5025 bits/instance
Class complexity | order 0           23870.9714 bits      2.6154 bits/instance
Class complexity | scheme           689889.6774 bits     75.5878 bits/instance
Complexity improvement     (Sf)    -666018.7059 bits    -72.9724 bits/instance
Mean absolute error                      0.0802
Root mean squared error                  0.203 
Relative absolute error                 50.8174 %
Root relative squared error             72.278  %
Coverage of cases (0.95 level)          93.0426 %
Mean rel. region size (0.95 level)      29.5891 %
Total Number of Instances             9127     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,283    0,001    0,567      0,283    0,378      0,398    0,877     0,284     C
                 0,609    0,047    0,535      0,609    0,569      0,530    0,912     0,570     E
                 0,043    0,004    0,130      0,043    0,065      0,067    0,753     0,067     FV
                 0,769    0,105    0,657      0,769    0,708      0,628    0,913     0,784     AI
                 0,605    0,112    0,514      0,605    0,555      0,462    0,863     0,567     CDGKNRSYZ
                 0,486    0,031    0,573      0,486    0,526      0,491    0,878     0,529     O
                 0,915    0,049    0,911      0,915    0,913      0,865    0,974     0,951     0
                 0,125    0,010    0,331      0,125    0,181      0,185    0,784     0,167     LT
                 0,445    0,004    0,740      0,445    0,556      0,566    0,889     0,560     U
                 0,227    0,006    0,548      0,227    0,322      0,340    0,807     0,260     MBP
Weighted Avg.    0,695    0,064    0,685      0,695    0,683      0,629    0,912     0,711     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   17   13    0   19    8    3    0    0    0    0 |    a = C
    3  454    4  126  104   27   10    9    1    8 |    b = E
    0   20    6   29   57   14    5    3    1    3 |    c = FV
    3  128    5 1453  174   43   51   21    2   10 |    d = AI
    4  111   15  241  902   60  105   30    9   15 |    e = CDGKNRSYZ
    1   44    3  115  127  346   52    7   13    4 |    f = O
    0   12    2   52  154   36 2960   10    1    8 |    g = 0
    1   27    2   77  126   13   42   42    2    5 |    h = LT
    1   11    2   33   29   40    2    0   97    3 |    i = U
    0   29    7   67   75   22   21    5    5   68 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
28.33	21.67	0.0	31.67	13.33	5.0	0.0	0.0	0.0	0.0	 a = C
0.4	60.86	0.54	16.89	13.94	3.62	1.34	1.21	0.13	1.07	 b = E
0.0	14.49	4.35	21.01	41.3	10.14	3.62	2.17	0.72	2.17	 c = FV
0.16	6.77	0.26	76.88	9.21	2.28	2.7	1.11	0.11	0.53	 d = AI
0.27	7.44	1.01	16.15	60.46	4.02	7.04	2.01	0.6	1.01	 e = CDGKNRSYZ
0.14	6.18	0.42	16.15	17.84	48.6	7.3	0.98	1.83	0.56	 f = O
0.0	0.37	0.06	1.61	4.76	1.11	91.5	0.31	0.03	0.25	 g = 0
0.3	8.01	0.59	22.85	37.39	3.86	12.46	12.46	0.59	1.48	 h = LT
0.46	5.05	0.92	15.14	13.3	18.35	0.92	0.0	44.5	1.38	 i = U
0.0	9.7	2.34	22.41	25.08	7.36	7.02	1.67	1.67	22.74	 j = MBP
