Timestamp:2013-07-23-03:04:51
Inventario:I4
Intervalo:6ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I4-6ms-2gramas
Num Instances:  30452
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.2417





=== Stratified cross-validation ===

Correctly Classified Instances       24648               80.9405 %
Incorrectly Classified Instances      5804               19.0595 %
Kappa statistic                          0.7535
K&B Relative Info Score            2171222.5533 %
K&B Information Score                55748.8448 bits      1.8307 bits/instance
Class complexity | order 0           78169.2083 bits      2.567  bits/instance
Class complexity | scheme          1858763.6544 bits     61.0391 bits/instance
Complexity improvement     (Sf)    -1780594.4461 bits    -58.4722 bits/instance
Mean absolute error                      0.0575
Root mean squared error                  0.1699
Relative absolute error                 37.0216 %
Root relative squared error             61.0004 %
Coverage of cases (0.95 level)          94.3189 %
Mean rel. region size (0.95 level)      23.4152 %
Total Number of Instances            30452     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,724    0,001    0,827      0,724    0,772      0,773    0,954     0,765     C
                 0,820    0,024    0,744      0,820    0,780      0,762    0,957     0,824     E
                 0,463    0,004    0,598      0,463    0,522      0,520    0,860     0,408     FV
                 0,860    0,051    0,805      0,860    0,832      0,789    0,956     0,894     AI
                 0,742    0,062    0,690      0,742    0,715      0,660    0,923     0,744     CDGKNRSYZ
                 0,741    0,017    0,776      0,741    0,758      0,740    0,929     0,779     O
                 0,898    0,058    0,905      0,898    0,901      0,841    0,966     0,944     0
                 0,410    0,011    0,595      0,410    0,485      0,478    0,851     0,416     LT
                 0,792    0,002    0,923      0,792    0,853      0,852    0,971     0,878     U
                 0,549    0,007    0,735      0,549    0,629      0,625    0,891     0,596     MBP
Weighted Avg.    0,809    0,046    0,808      0,809    0,807      0,764    0,946     0,840     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   134    18     0    14    11     0     4     1     2     1 |     a = C
     5  1924    14   142   145    28    45    23     1    20 |     b = E
     1    38   199    52    87    14    29     3     0     7 |     c = FV
     8   178    24  5189   287    71   184    56     3    31 |     d = AI
     3   171    45   379  3561   108   390   100     7    38 |     e = CDGKNRSYZ
     2    47    10   126   162  1668   182    24     6    24 |     f = O
     3    96    28   282   505   148 10424    70     9    45 |     g = 0
     1    53     3   131   241    38   163   459    11    19 |     h = LT
     4    21     0    17    33    39     9    13   552     9 |     i = U
     1    39    10   112   127    35    88    23     7   538 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
72.43	9.73	0.0	7.57	5.95	0.0	2.16	0.54	1.08	0.54	 a = C
0.21	81.98	0.6	6.05	6.18	1.19	1.92	0.98	0.04	0.85	 b = E
0.23	8.84	46.28	12.09	20.23	3.26	6.74	0.7	0.0	1.63	 c = FV
0.13	2.95	0.4	86.04	4.76	1.18	3.05	0.93	0.05	0.51	 d = AI
0.06	3.56	0.94	7.89	74.16	2.25	8.12	2.08	0.15	0.79	 e = CDGKNRSYZ
0.09	2.09	0.44	5.6	7.2	74.1	8.09	1.07	0.27	1.07	 f = O
0.03	0.83	0.24	2.43	4.35	1.27	89.78	0.6	0.08	0.39	 g = 0
0.09	4.74	0.27	11.71	21.54	3.4	14.57	41.02	0.98	1.7	 h = LT
0.57	3.01	0.0	2.44	4.73	5.6	1.29	1.87	79.2	1.29	 i = U
0.1	3.98	1.02	11.43	12.96	3.57	8.98	2.35	0.71	54.9	 j = MBP
