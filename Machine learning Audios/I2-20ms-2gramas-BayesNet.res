Timestamp:2013-08-30-04:43:01
Inventario:I2
Intervalo:20ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I2-20ms-2gramas
Num Instances:  9128
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(4): class 
0F1(Hz)(9): class 
0F2(Hz)(8): class 
0F3(Hz)(2): class 
0F4(Hz)(3): class 
1Int(dB)(9): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(7): class 
1ZCross(8): class 
1F1(Hz)(9): class 
1F2(Hz)(9): class 
1F3(Hz)(5): class 
1F4(Hz)(5): class 
class(12): 
LogScore Bayes: -183078.45162825432
LogScore BDeu: -188000.06381895914
LogScore MDL: -187500.0984883236
LogScore ENTROPY: -182142.6261270427
LogScore AIC: -183317.6261270427




=== Stratified cross-validation ===

Correctly Classified Instances        5212               57.099  %
Incorrectly Classified Instances      3916               42.901  %
Kappa statistic                          0.4678
K&B Relative Info Score             422037.8513 %
K&B Information Score                12445.8263 bits      1.3635 bits/instance
Class complexity | order 0           26901.0412 bits      2.9471 bits/instance
Class complexity | scheme            58104.0459 bits      6.3655 bits/instance
Complexity improvement     (Sf)     -31203.0048 bits     -3.4184 bits/instance
Mean absolute error                      0.074 
Root mean squared error                  0.2353
Relative absolute error                 54.3873 %
Root relative squared error             90.202  %
Coverage of cases (0.95 level)          71.1109 %
Mean rel. region size (0.95 level)      18.1803 %
Total Number of Instances             9128     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,642    0,076    0,625      0,642    0,633      0,560    0,888     0,694     A
                 0,011    0,008    0,068      0,011    0,019      0,008    0,690     0,082     LGJKC
                 0,540    0,065    0,423      0,540    0,475      0,425    0,878     0,446     E
                 0,043    0,016    0,041      0,043    0,042      0,027    0,757     0,041     FV
                 0,582    0,044    0,370      0,582    0,452      0,434    0,904     0,411     I
                 0,249    0,021    0,496      0,249    0,331      0,314    0,817     0,366     O
                 0,244    0,052    0,344      0,244    0,286      0,225    0,805     0,307     SNRTY
                 0,927    0,104    0,831      0,927    0,876      0,806    0,972     0,962     0
                 0,023    0,005    0,132      0,023    0,040      0,043    0,756     0,087     BMP
                 0,333    0,043    0,106      0,333    0,160      0,166    0,833     0,084     DZ
                 0,454    0,016    0,402      0,454    0,427      0,413    0,903     0,431     U
                 0,145    0,046    0,123      0,145    0,133      0,092    0,813     0,132     SNRTXY
Weighted Avg.    0,571    0,067    0,546      0,571    0,549      0,497    0,884     0,593     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
  966   19  114   27   42   56   79   63    2   59    8   70 |    a = A
   53    5   67    9   49   16   62  103    3   23    5   55 |    b = LGJKC
   86   21  403    5   59   11   35   24    4   42   11   45 |    c = E
   20    0   24    6    5    7   22   15    2   13    1   23 |    d = FV
    6    2   71    2  224    1   35   18    2    6    5   13 |    e = I
  133   12   63    8   20  177   40   65    6   90   57   41 |    f = O
  105    4   51   32   60   16  223  265   14   49   17   78 |    g = SNRTY
   33    2   19   32   28   17   74 3000    6    6    4   15 |    h = 0
   35    2   41   11   61    8   30   30    7   30   16   28 |    i = BMP
   26    0   13    3    6    9    7    4    2   46    6   16 |    j = DZ
   16    4   18    1    4   27   11    4    1   17   99   16 |    k = U
   66    2   68   10   48   12   30   19    4   55   17   56 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
64.19	1.26	7.57	1.79	2.79	3.72	5.25	4.19	0.13	3.92	0.53	4.65	 a = A
11.78	1.11	14.89	2.0	10.89	3.56	13.78	22.89	0.67	5.11	1.11	12.22	 b = LGJKC
11.53	2.82	54.02	0.67	7.91	1.47	4.69	3.22	0.54	5.63	1.47	6.03	 c = E
14.49	0.0	17.39	4.35	3.62	5.07	15.94	10.87	1.45	9.42	0.72	16.67	 d = FV
1.56	0.52	18.44	0.52	58.18	0.26	9.09	4.68	0.52	1.56	1.3	3.38	 e = I
18.68	1.69	8.85	1.12	2.81	24.86	5.62	9.13	0.84	12.64	8.01	5.76	 f = O
11.49	0.44	5.58	3.5	6.56	1.75	24.4	28.99	1.53	5.36	1.86	8.53	 g = SNRTY
1.02	0.06	0.59	0.99	0.87	0.53	2.29	92.71	0.19	0.19	0.12	0.46	 h = 0
11.71	0.67	13.71	3.68	20.4	2.68	10.03	10.03	2.34	10.03	5.35	9.36	 i = BMP
18.84	0.0	9.42	2.17	4.35	6.52	5.07	2.9	1.45	33.33	4.35	11.59	 j = DZ
7.34	1.83	8.26	0.46	1.83	12.39	5.05	1.83	0.46	7.8	45.41	7.34	 k = U
17.05	0.52	17.57	2.58	12.4	3.1	7.75	4.91	1.03	14.21	4.39	14.47	 l = SNRTXY
