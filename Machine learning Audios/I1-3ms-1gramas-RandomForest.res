Timestamp:2013-07-22-21:13:21
Inventario:I1
Intervalo:3ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I1-3ms-1gramas
Num Instances:  61135
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1655





=== Stratified cross-validation ===

Correctly Classified Instances       52724               86.2419 %
Incorrectly Classified Instances      8411               13.7581 %
Kappa statistic                          0.8247
K&B Relative Info Score            4834395.803  %
K&B Information Score               139890.1366 bits      2.2882 bits/instance
Class complexity | order 0          176880.0177 bits      2.8933 bits/instance
Class complexity | scheme          1813581.532  bits     29.6652 bits/instance
Complexity improvement     (Sf)    -1636701.5144 bits    -26.7719 bits/instance
Mean absolute error                      0.0343
Root mean squared error                  0.1266
Relative absolute error                 27.9623 %
Root relative squared error             51.1032 %
Coverage of cases (0.95 level)          96.5061 %
Mean rel. region size (0.95 level)      18.1823 %
Total Number of Instances            61135     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,908    0,016    0,914      0,908    0,911      0,894    0,979     0,948     a
                 0,711    0,003    0,879      0,711    0,786      0,785    0,944     0,783     b
                 0,889    0,010    0,876      0,889    0,883      0,873    0,977     0,922     e
                 0,803    0,002    0,878      0,803    0,839      0,837    0,971     0,862     d
                 0,606    0,002    0,832      0,606    0,701      0,707    0,941     0,706     f
                 0,868    0,004    0,894      0,868    0,881      0,876    0,971     0,902     i
                 0,291    0,004    0,583      0,291    0,388      0,404    0,867     0,364     k
                 0,706    0,001    0,901      0,706    0,791      0,795    0,928     0,785     j
                 0,736    0,018    0,812      0,736    0,772      0,751    0,952     0,824     l
                 0,820    0,006    0,915      0,820    0,864      0,856    0,967     0,890     o
                 0,955    0,089    0,871      0,955    0,911      0,853    0,983     0,975     0
                 0,714    0,023    0,658      0,714    0,685      0,665    0,965     0,763     s
                 0,900    0,001    0,950      0,900    0,925      0,923    0,984     0,948     u
Weighted Avg.    0,862    0,042    0,861      0,862    0,859      0,829    0,972     0,905     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  8704    20    67    13    14    23    23     8   152    39   416   102     4 |     a = a
    51  1385    28     1     3    27    12     0    58    24   259    89    11 |     b = b
    69    23  4107    12    13    20    15     7   113    22   167    43    10 |     c = e
    34     3    27   726     0     1     0     0    32    36    33     9     3 |     d = d
    26     9    26     3   524     9     5     3    37    16    75   131     0 |     e = f
    24    14    48     0     7  2067     8     2    51     7   127    25     2 |     f = i
    32    10    35     1    13    14   347     5    72    19   416   222     6 |     g = k
    32     3    23     2     4     4     6   489    17     4    41    66     2 |     h = j
   240    37   139    12     7    56    42     7  4285    67   682   238     7 |     i = l
    90    18    31    25     9     6    13     1    99  3658   450    56     7 |     j = o
   159    28   120    26    19    62    39    12   209    73 22647   323     9 |     k = 0
    55    13    19     3    17    17    77     7   130    14   659  2537     4 |     l = s
    11    12    16     3     0     5     8     2    24    21    23    13  1248 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
90.81	0.21	0.7	0.14	0.15	0.24	0.24	0.08	1.59	0.41	4.34	1.06	0.04	 a = a
2.62	71.1	1.44	0.05	0.15	1.39	0.62	0.0	2.98	1.23	13.3	4.57	0.56	 b = b
1.49	0.5	88.88	0.26	0.28	0.43	0.32	0.15	2.45	0.48	3.61	0.93	0.22	 c = e
3.76	0.33	2.99	80.31	0.0	0.11	0.0	0.0	3.54	3.98	3.65	1.0	0.33	 d = d
3.01	1.04	3.01	0.35	60.65	1.04	0.58	0.35	4.28	1.85	8.68	15.16	0.0	 e = f
1.01	0.59	2.02	0.0	0.29	86.78	0.34	0.08	2.14	0.29	5.33	1.05	0.08	 f = i
2.68	0.84	2.94	0.08	1.09	1.17	29.11	0.42	6.04	1.59	34.9	18.62	0.5	 g = k
4.62	0.43	3.32	0.29	0.58	0.58	0.87	70.56	2.45	0.58	5.92	9.52	0.29	 h = j
4.12	0.64	2.39	0.21	0.12	0.96	0.72	0.12	73.64	1.15	11.72	4.09	0.12	 i = l
2.02	0.4	0.69	0.56	0.2	0.13	0.29	0.02	2.22	81.96	10.08	1.25	0.16	 j = o
0.67	0.12	0.51	0.11	0.08	0.26	0.16	0.05	0.88	0.31	95.45	1.36	0.04	 k = 0
1.55	0.37	0.53	0.08	0.48	0.48	2.17	0.2	3.66	0.39	18.55	71.42	0.11	 l = s
0.79	0.87	1.15	0.22	0.0	0.36	0.58	0.14	1.73	1.52	1.66	0.94	90.04	 m = u
