Timestamp:2013-08-30-05:19:23
Inventario:I4
Intervalo:2ms
Ngramas:2
Clasificador:BayesNet
Relation Name:  I4-2ms-2gramas
Num Instances:  91702
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=21 #classindex=20
Network structure (nodes followed by parents)
0Int(dB)(24): class 
0Pitch(Hz)(45): class 
0RawPitch(33): class 
0SmPitch(38): class 
0Melogram(st)(47): class 
0ZCross(18): class 
0F1(Hz)(1457): class 
0F2(Hz)(1987): class 
0F3(Hz)(1986): class 
0F4(Hz)(2005): class 
1Int(dB)(22): class 
1Pitch(Hz)(47): class 
1RawPitch(34): class 
1SmPitch(39): class 
1Melogram(st)(45): class 
1ZCross(18): class 
1F1(Hz)(1497): class 
1F2(Hz)(1915): class 
1F3(Hz)(1983): class 
1F4(Hz)(2163): class 
class(10): 
LogScore Bayes: -5134738.698473111
LogScore BDeu: -6988876.644438072
LogScore MDL: -6526161.31541684
LogScore ENTROPY: -5647256.073468205
LogScore AIC: -5801095.073468205




=== Stratified cross-validation ===

Correctly Classified Instances       69290               75.56   %
Incorrectly Classified Instances     22412               24.44   %
Kappa statistic                          0.6789
K&B Relative Info Score            6395816.5225 %
K&B Information Score               163022.1567 bits      1.7777 bits/instance
Class complexity | order 0          233715.7607 bits      2.5486 bits/instance
Class complexity | scheme           496683.8282 bits      5.4163 bits/instance
Complexity improvement     (Sf)    -262968.0675 bits     -2.8676 bits/instance
Mean absolute error                      0.0498
Root mean squared error                  0.2102
Relative absolute error                 32.2911 %
Root relative squared error             75.7115 %
Coverage of cases (0.95 level)          80.0975 %
Mean rel. region size (0.95 level)      12.0056 %
Total Number of Instances            91702     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,816    0,016    0,236      0,816    0,366      0,434    0,947     0,688     C
                 0,838    0,037    0,649      0,838    0,731      0,713    0,955     0,820     E
                 0,584    0,008    0,495      0,584    0,536      0,531    0,897     0,571     FV
                 0,803    0,033    0,854      0,803    0,828      0,788    0,952     0,894     AI
                 0,360    0,022    0,750      0,360    0,486      0,466    0,925     0,695     CDGKNRSYZ
                 0,764    0,016    0,784      0,764    0,774      0,756    0,921     0,807     O
                 0,905    0,159    0,785      0,905    0,841      0,732    0,967     0,958     0
                 0,457    0,009    0,661      0,457    0,540      0,536    0,892     0,532     LT
                 0,897    0,011    0,650      0,897    0,754      0,757    0,976     0,893     U
                 0,651    0,010    0,690      0,651    0,670      0,660    0,896     0,688     MBP
Weighted Avg.    0,756    0,077    0,765      0,756    0,745      0,688    0,947     0,851     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   444    28     0    17     8     2    37     1     7     0 |     a = C
   137  5788    64   172   229    30   285    85    52    68 |     b = E
    21    63   751    42    29    22   324     9    11    14 |     c = FV
   295   747   190 14371   503   268  1000   180   128   212 |     d = AI
   514  1203   107  1132  5131   495  4863   149   454   224 |     e = CDGKNRSYZ
   151   173    22   225    94  5077   738    44    69    53 |     f = O
   153   624   359   652   489   433 32445   267   181   229 |     g = 0
    88   169     4   119   226    73  1037  1519    55    33 |     h = LT
    19    17    13    19    49    20    41    18  1866    18 |     i = U
    60   104     7    87    83    56   546    26    48  1898 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
81.62	5.15	0.0	3.13	1.47	0.37	6.8	0.18	1.29	0.0	 a = C
1.98	83.76	0.93	2.49	3.31	0.43	4.12	1.23	0.75	0.98	 b = E
1.63	4.9	58.4	3.27	2.26	1.71	25.19	0.7	0.86	1.09	 c = FV
1.65	4.17	1.06	80.31	2.81	1.5	5.59	1.01	0.72	1.18	 d = AI
3.6	8.43	0.75	7.93	35.95	3.47	34.07	1.04	3.18	1.57	 e = CDGKNRSYZ
2.27	2.6	0.33	3.39	1.41	76.39	11.1	0.66	1.04	0.8	 f = O
0.43	1.74	1.0	1.82	1.36	1.21	90.55	0.75	0.51	0.64	 g = 0
2.65	5.09	0.12	3.58	6.8	2.2	31.21	45.71	1.66	0.99	 h = LT
0.91	0.82	0.63	0.91	2.36	0.96	1.97	0.87	89.71	0.87	 i = U
2.06	3.57	0.24	2.98	2.85	1.92	18.73	0.89	1.65	65.11	 j = MBP
