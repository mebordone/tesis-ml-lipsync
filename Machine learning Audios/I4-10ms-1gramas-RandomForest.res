Timestamp:2013-07-22-23:04:14
Inventario:I4
Intervalo:10ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I4-10ms-1gramas
Num Instances:  18296
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.2766





=== Stratified cross-validation ===

Correctly Classified Instances       14006               76.5523 %
Incorrectly Classified Instances      4290               23.4477 %
Kappa statistic                          0.6964
K&B Relative Info Score            1200305.34   %
K&B Information Score                31005.0039 bits      1.6946 bits/instance
Class complexity | order 0           47243.3901 bits      2.5822 bits/instance
Class complexity | scheme          1324494.2733 bits     72.3926 bits/instance
Complexity improvement     (Sf)    -1277250.8832 bits    -69.8104 bits/instance
Mean absolute error                      0.0663
Root mean squared error                  0.1838
Relative absolute error                 42.4564 %
Root relative squared error             65.7752 %
Coverage of cases (0.95 level)          93.1242 %
Mean rel. region size (0.95 level)      25.558  %
Total Number of Instances            18296     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,449    0,002    0,608      0,449    0,516      0,520    0,886     0,469     C
                 0,737    0,033    0,659      0,737    0,696      0,670    0,938     0,710     E
                 0,280    0,004    0,487      0,280    0,355      0,362    0,783     0,281     FV
                 0,808    0,061    0,769      0,808    0,788      0,733    0,938     0,858     AI
                 0,672    0,084    0,605      0,672    0,637      0,564    0,897     0,673     CDGKNRSYZ
                 0,648    0,018    0,750      0,648    0,695      0,674    0,914     0,714     O
                 0,918    0,068    0,888      0,918    0,903      0,845    0,970     0,956     0
                 0,264    0,011    0,488      0,264    0,343      0,342    0,818     0,327     LT
                 0,726    0,003    0,839      0,726    0,778      0,775    0,943     0,797     U
                 0,429    0,007    0,669      0,429    0,523      0,524    0,852     0,496     MBP
Weighted Avg.    0,766    0,056    0,760      0,766    0,759      0,711    0,932     0,799     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   48   12    1   23   10    4    5    2    2    0 |    a = C
   10 1060    9  126  130   29   46   12    4   12 |    b = E
    1   23   73   25   94   13   16    6    4    6 |    c = FV
    6  193   10 2980  262   60  118   33    6   20 |    d = AI
    2  150   25  317 1970   73  291   62   15   28 |    e = CDGKNRSYZ
    6   53    5   92  156  892  125   21   10   17 |    f = O
    2   44   11  112  291   37 6242   31    3   25 |    g = 0
    0   27    9  109  191   29  122  179    5    6 |    h = LT
    3   16    1   14   32   26    7    5  307   12 |    i = U
    1   30    6   75  120   27   54   16   10  255 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
44.86	11.21	0.93	21.5	9.35	3.74	4.67	1.87	1.87	0.0	 a = C
0.7	73.71	0.63	8.76	9.04	2.02	3.2	0.83	0.28	0.83	 b = E
0.38	8.81	27.97	9.58	36.02	4.98	6.13	2.3	1.53	2.3	 c = FV
0.16	5.23	0.27	80.8	7.1	1.63	3.2	0.89	0.16	0.54	 d = AI
0.07	5.11	0.85	10.81	67.17	2.49	9.92	2.11	0.51	0.95	 e = CDGKNRSYZ
0.44	3.85	0.36	6.68	11.33	64.78	9.08	1.53	0.73	1.23	 f = O
0.03	0.65	0.16	1.65	4.28	0.54	91.82	0.46	0.04	0.37	 g = 0
0.0	3.99	1.33	16.1	28.21	4.28	18.02	26.44	0.74	0.89	 h = LT
0.71	3.78	0.24	3.31	7.57	6.15	1.65	1.18	72.58	2.84	 i = U
0.17	5.05	1.01	12.63	20.2	4.55	9.09	2.69	1.68	42.93	 j = MBP
