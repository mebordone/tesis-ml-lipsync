Timestamp:2014-12-07-11:00:16
Inventario:I2
Intervalo:6ms
Ngramas:5
Clasificador:NaiveBayes
Relation Name:  I2-6ms-5gramas
Num Instances:  30449
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  35 3Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  45 4Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Naive Bayes Classifier

                    Class
Attribute               A     LGJKC         E        FV         I         O     SNRTY         0       BMP        DZ         U    SNRTXY
                   (0.16)    (0.05)    (0.08)    (0.01)    (0.04)    (0.07)     (0.1)    (0.38)    (0.03)    (0.01)    (0.02)    (0.04)
========================================================================================================================================
0Int(dB)
  mean             -5.4118  -16.8634   -7.2577  -13.3109   -10.716   -9.5072  -18.3324  -47.9711  -12.6386   -7.6936   -8.0314   -9.2073
  std. dev.        11.0068   14.0542    9.8253    10.453    9.4418   12.3595   11.2176   16.8891   11.8743    6.5466   10.7199    8.5069
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001

0Pitch(Hz)
  mean            209.0402  128.0818   177.001   146.571  157.8868  172.4379  103.6075   18.4166  166.0364   199.245  213.6025  185.9212
  std. dev.       112.8416  111.2595   95.1194  108.6374   87.2331  110.9435  109.0665   58.7916   110.778   85.6459  105.4867   97.2902
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457

0RawPitch
  mean            198.7329  113.4028  171.0375  124.7505  148.2543  163.5133   85.3037   15.6513  153.2783   193.425  202.7883  173.3215
  std. dev.       117.5529  109.6481  100.5737  107.8857   90.5894  116.7271  106.6164    54.151   115.415   94.0949  111.3103  106.0915
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486

0SmPitch
  mean            206.6612  122.0708  173.7645  136.7277  155.3294  169.3568    96.811    16.968  163.4877   197.374  210.7198  183.7195
  std. dev.       114.6666  111.6075   97.6867  107.2216   88.5745  112.9699  108.2139   55.5321  112.7218   88.7078  107.1445   99.0575
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475

0Melogram(st)
  mean             49.6051   33.4811   47.0369   41.3271    44.285   44.7923   28.5655     5.373   43.4903   52.3915   49.1013   48.2841
  std. dev.        19.1651   26.5814   19.4744   23.0186   20.6362   22.1672   27.2576   16.2254   23.1301   12.5877   21.0803    19.035
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271

0ZCross
  mean              6.6693    2.6674    4.7142    2.4161     3.439    3.8153    2.8658    0.3431    2.6114    3.6254    3.5554     3.502
  std. dev.         4.7465    2.9822    3.6571    2.3921    3.6778    3.4988    4.7679    1.6017    2.5157    2.1222     3.083    4.0281
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739

0F1(Hz)
  mean            624.4955  358.9817   424.749  455.8423  328.6897  439.0009  332.8065   62.8556  404.5692  502.2986  448.2668  490.1587
  std. dev.       309.4436  276.5377  194.8652  260.5602  186.0771  251.6708   298.055  181.6101  237.8241  160.3655  344.2631  196.8731
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628

0F2(Hz)
  mean           1371.8281   1188.62 1589.4535 1316.0818 1672.1769 1089.9744 1000.1119  192.4366 1389.0091 1466.0281  946.1299 1546.6182
  std. dev.       560.4474  869.0979  707.3303  718.3454  865.5474   620.861  855.9919  549.1729  730.9012  360.7816  558.2464  557.0315
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452

0F3(Hz)
  mean           2512.8034 1945.9505 2387.6492 2237.6313  2323.906 2319.4528 1724.5492  328.4058 2338.8226 2821.6202  2338.906 2654.8791
  std. dev.       989.7196 1395.1696 1045.4602 1197.6039  1204.488 1176.4849 1453.2587  918.0696 1159.0283  571.6295 1145.7887   869.342
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587

0F4(Hz)
  mean           2919.6988  2221.746 2894.6517 2637.6754 2785.0724 2674.6565 1970.9286  394.5822 2753.4563 3279.4353  2695.275 3034.6537
  std. dev.      1138.3963 1581.9111 1242.3286 1381.4899 1408.5564 1346.3444  1646.432 1100.9601 1335.5172  632.5802 1265.2932  985.9775
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801

1Int(dB)
  mean             -4.9579  -16.7031   -6.5177  -13.9952  -10.1042   -8.9796  -19.0573  -48.2639  -12.9282   -8.0066   -7.2074   -9.5653
  std. dev.        10.4863    13.296    9.2729    9.9098    9.0807   12.1898   10.7165   16.5014   11.6685    6.6028    10.005    8.4491
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001

1Pitch(Hz)
  mean            211.8007  126.3445  181.8248  141.9695  163.9943  175.9605   96.2956   17.3103  163.7762  198.2064  220.3105  183.3401
  std. dev.       110.5354  111.2828   92.6133  108.6529   83.5367  108.5713  108.1055   57.1184  111.3227   87.0365   97.9407   99.0785
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457

1RawPitch
  mean            201.4532  109.6625  176.4078   119.205  154.7055  167.3967   78.1954   14.6449  150.2849  192.8254  209.3446  170.7056
  std. dev.       115.6497  109.1198   98.3173  106.8748   87.7211  114.5172  104.4827   52.4106  115.4886   95.2246  105.1448  107.6975
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486

1SmPitch
  mean            209.3893  119.3079  178.7991  132.2744  161.4192  173.1852   89.7759   15.8556  160.7347  196.7754  217.8306  180.9221
  std. dev.       112.5358  111.8128   95.0401  106.6852    85.075  110.6103  106.8917   53.6517  113.5476   89.4547   99.6499  100.8584
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475

1Melogram(st)
  mean             50.3727   32.7095   48.4575   40.1595   45.9796   45.7682   26.5062    5.0779   42.8393   52.1022   51.0893   47.6281
  std. dev.        18.2348   26.7353   17.9973   23.6269   19.1192   21.3497    27.263   15.8217   23.5694   13.0915   18.8712   19.6691
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271

1ZCross
  mean              6.7846    2.6988    4.8042     2.184     3.476    3.8899    2.8312    0.2937    2.4616     3.558    3.6026    3.4143
  std. dev.         4.6549    3.1551    3.4983     2.217    3.4985    3.3516    4.9886    1.3781     2.346    2.1216    2.9524    4.3863
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739

1F1(Hz)
  mean             641.598  348.0585  436.6695  439.5501   338.547  453.8732  311.0479   58.3302  395.6915  500.8635   463.808  475.6155
  std. dev.       298.3129  276.3117  181.6351  266.9062  174.0301  244.1481  298.4515  175.0393  237.6905  164.1639  334.8008  198.9303
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628

1F2(Hz)
  mean            1395.894 1163.6918 1648.9094 1279.0165 1765.1714 1110.1712  938.1094  179.1355 1369.4578 1456.8096  978.1609 1527.2592
  std. dev.       530.2954  875.8164  663.0113  732.7963   818.714   595.382  862.5477  531.0655  735.8848  368.4906  525.2413  573.2143
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452

1F3(Hz)
  mean           2559.4613 1907.5727 2459.3827 2180.6268 2427.5668 2381.0703 1615.5154  306.6629 2305.6138 2811.4308 2438.5289 2624.6117
  std. dev.       937.7438 1410.0957   978.513  1230.956 1132.3873 1130.0518 1465.7643  891.1097 1172.8483   599.856 1064.3754  904.5056
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587

1F4(Hz)
  mean           2973.1331 2176.6756 2976.4834 2575.9403 2903.1799 2744.0896 1848.6293  368.3769 2726.3768 3270.9669 2803.9205 3002.7625
  std. dev.      1076.9898 1597.8874 1157.0182 1422.3377 1317.4154 1290.3365 1663.6578 1068.4378 1356.7863  668.3634 1162.6311 1029.8321
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801

2Int(dB)
  mean             -4.5987  -16.3618   -5.8995  -14.5611   -9.5588   -8.5713  -19.5944  -48.5576  -12.9966   -8.2731    -6.419   -9.9112
  std. dev.        10.0572   12.4852     8.786    9.4214    8.7617   12.1108    10.125   16.0892   11.3571    6.6541    9.2206    8.3856
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001

2Pitch(Hz)
  mean            213.9858  126.3582  185.9675  138.3865   168.922  178.5044   90.3578   16.3327  162.1914  197.3077  225.2933  180.6989
  std. dev.       108.6894  111.2021   90.3606  108.2488   80.1793  106.4846   106.959    55.605  111.4338    88.379   91.5415   100.486
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457

2RawPitch
  mean            203.4289  107.9608  181.6948  114.6567   160.411  171.1249   72.1285   13.5156  148.0422  192.1779  215.3066  167.8129
  std. dev.       114.2033  108.7309   96.2139  105.4998   84.8193  112.4932  102.1251   50.0964  115.6256   96.4293   98.6174  109.0519
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486

2SmPitch
  mean            211.6839  118.4424  183.5596  128.8353  166.5988  175.9059   83.5013   14.8087  158.6549  196.3238  223.7844  178.2525
  std. dev.       110.6731  112.0733   92.3488  105.8213   81.8868  108.6981  105.2338   51.7844   113.938   90.1643   92.4136  102.1592
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475

2Melogram(st)
  mean             51.0457   32.1702   49.6417   39.2686   47.3894   46.4994   24.7586    4.8253   42.2976   51.8281   52.9038   47.0089
  std. dev.        17.3442   26.8419   16.5413   24.0506   17.6115   20.6431   27.1749   15.4719   23.9194    13.567    16.369     20.22
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271

2ZCross
  mean              6.8934    2.7946    4.8687    2.0038    3.5256    3.9353    2.7856    0.2426    2.3634     3.514    3.6666    3.3286
  std. dev.         4.5993    3.3599    3.3612    2.0839    3.3498     3.141    5.1116    1.1446    2.1945    2.1397    2.8883     4.662
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739

2F1(Hz)
  mean            658.1625  339.5717  448.1303  421.3901  348.8664  467.9136  289.9277   53.9414  387.5882  500.2076  478.0605   459.862
  std. dev.       286.2026   275.456  167.3815  272.3544  162.2662  235.6843  297.1682  168.2233  237.6824  169.1352   324.529  200.5848
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628

2F2(Hz)
  mean           1419.2622 1144.2017 1706.5198 1239.1028 1853.8785 1129.6245  878.9046  166.3602 1348.6957 1446.9631 1009.2259 1504.3129
  std. dev.       497.7941  878.3276  612.0386  748.4208  761.7664  569.4748  865.0862  512.7699  740.2835  383.2018  490.7397  591.2449
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452

2F3(Hz)
  mean           2605.2622 1880.2332 2528.8267 2115.5779 2527.1222  2440.143 1511.1536  285.4685  2271.328 2795.3227 2534.3382 2589.1837
  std. dev.       881.9027 1420.2481   904.012 1263.4416 1049.8927 1080.4541 1470.6773  863.1988 1184.8692  639.5707  971.1663  942.4543
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587

2F4(Hz)
  mean           3024.9811 2145.5262  3055.702 2508.5338 3015.9411 2810.7638 1730.2026  343.1326 2699.0169 3253.5631 2907.9441 2965.4012
  std. dev.      1010.9519 1608.9438 1062.5316 1464.9089 1214.2293 1230.5926 1671.0286 1035.5503 1377.3282  718.6748 1043.9573 1077.2017
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801

3Int(dB)
  mean             -4.3897  -15.8985   -5.4538  -14.9101   -9.0975     -8.32  -19.8827  -48.8227   -12.808   -8.3777   -5.7217  -10.2662
  std. dev.         9.7701   11.6832    8.3541     9.056    8.4757   12.1084    9.6412   15.7047   10.9801    6.7523    8.3788    8.3511
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001    0.1001

3Pitch(Hz)
  mean            215.1858  129.3649  188.9309  135.5009  172.4627  179.6002   86.8917   15.2665  162.0524  196.8696  229.1165  178.6153
  std. dev.       107.7136  110.8074   88.8484  107.5102   77.3033  105.0982  106.2567   53.8964  110.5525   89.1614    86.113  101.4837
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457

3RawPitch
  mean            204.8304  108.6374  185.2813   111.142  164.6635  173.0353   68.0383   12.5274  147.7901  192.0542  219.1052   165.347
  std. dev.       113.2593  108.4392   94.3657   103.983   82.3462  111.2194  100.1913   48.0554  115.4056   96.8511   94.0431  110.4673
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486

3SmPitch
  mean            213.0105  120.4571  187.4191  126.3464   169.864  177.2873   79.8684   13.7665  157.4726   196.136  227.6752  175.6511
  std. dev.       109.5872  112.0198   90.1622  104.6501   79.4786  107.1683  104.3365    49.939  113.6823   90.5134   87.0875  103.3145
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475

3Melogram(st)
  mean             51.4062   32.7705    50.443    38.768   48.4372   46.7991   23.4771    4.5879   42.0662   51.7615   54.1423     46.72
  std. dev.        16.8175   26.7824   15.4134   24.2654   16.3134   20.2997   27.0502    15.144   23.9804      13.6    14.241   20.4641
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271

3ZCross
  mean              6.9598    2.8967    4.9057    1.9165    3.5597    3.9697    2.7562    0.2049    2.3311    3.5269    3.7441    3.2118
  std. dev.         4.5644    3.5454    3.2087    2.0253    3.2377    2.9459    5.1961    0.9759    2.0986    2.1532    2.8259    4.8566
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739

3F1(Hz)
  mean            671.8022  335.0167  458.4701  405.4793  358.3341  479.3066  270.0199   50.4118  382.1121  501.7228  490.8774  444.7698
  std. dev.       275.7695  274.1106  153.5049  275.9084  149.9822  228.1739  294.3178  162.5836  236.5551  169.8665  313.8193  200.7989
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628

3F2(Hz)
  mean           1436.7855 1131.8837 1756.5682  1210.404 1934.2548 1142.3274  822.7494  156.3977 1334.4278  1445.933 1039.7614 1484.6813
  std. dev.       469.7671    875.83  558.7593  760.5497    698.11  545.8877  863.1665  498.6702  740.0834  387.8544  453.9752  605.2985
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452

3F3(Hz)
  mean           2640.8558 1869.8757 2589.9599 2065.6755  2617.135   2486.42 1412.3534  267.8992 2248.5273 2792.7197 2624.1187 2558.3474
  std. dev.       835.2949 1424.9403  828.9033 1286.9009  961.8845 1037.8263 1467.8279  838.7997 1187.9068  652.3593   862.036  972.3374
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587

3F4(Hz)
  mean           3063.4806 2134.6394 3123.7667 2456.6943 3116.8532 2862.0839 1619.7766  322.5881 2685.0292 3250.4045 3008.5041 2934.0257
  std. dev.       955.4282 1614.2076   967.012 1495.5033 1104.1679 1178.6146 1671.5497 1007.9294 1387.1463  735.0539  904.6428 1115.4508
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801

4Int(dB)
  mean             -4.3746  -15.1801   -5.3083  -14.9621   -8.8383   -8.2927  -19.7992  -49.0439  -12.3369   -8.2512   -5.2124   -10.604
  std. dev.         9.6808   10.9367    8.1563    8.8347    8.4087   12.1905    9.4462   15.4157    10.602    6.8533    7.6533    8.3777
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1027    0.1027    0.1027    0.1027    0.1027    0.1027    0.1027    0.1027    0.1027    0.1027    0.1027    0.1027

4Pitch(Hz)
  mean            215.2971  135.4825  191.0581  133.3692  173.8339  178.5747    86.664   14.1348  163.7124  196.5351  230.7901  177.0522
  std. dev.       107.7031  110.3961   87.4612  106.8281   75.6506  104.7438   106.604   52.0172  108.8774   89.7942   82.8574  102.1951
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457    0.1457

4RawPitch
  mean            204.9722  112.5458  186.7023  109.9149  166.6698  172.5426   67.6665   11.6322  150.0446  191.2433  221.3767  163.2624
  std. dev.       113.3158  108.6812   93.4491  102.8897   81.1132  110.6448  100.0083   46.5665   114.114   96.8178   91.1707    111.26
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486    0.1486

4SmPitch
  mean            213.2402   125.785  189.6338   124.407  171.4037  176.5059   79.3932   12.7092  158.6198  196.0253  229.7721  173.7136
  std. dev.       109.3851  112.2615   88.7774   103.769   78.1033   106.636  104.4918   48.0626  112.4568   90.7394   83.4441  104.1861
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475    0.1475

4Melogram(st)
  mean              51.533   34.0249   50.8127   38.4205   48.9436   46.6912   23.2972    4.2927    42.399   51.7323   54.8522   46.3742
  std. dev.        16.5965    26.633   14.8245   24.4262   15.5254   20.3122   27.0944    14.704   23.6994   13.6209   12.6632   20.7876
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271    0.0271

4ZCross
  mean              6.9593    2.9949    4.8852    1.9001    3.5568    3.9781    2.7735    0.1869    2.4041    3.6047    3.8114    3.1317
  std. dev.         4.5717    3.5547    3.1792    2.0075    3.1167    2.8849    5.2227    0.9421    2.0753    2.1402    2.7838    4.9422
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739    1.1739

4F1(Hz)
  mean            682.1071  338.0269   466.462  391.2913  366.1781  486.7294  255.3971   46.8725  381.3506  503.7105  499.9719  432.0277
  std. dev.       268.0102  271.8139  141.9729  276.1702  138.5016  223.3891  290.5344   156.957   233.551  170.1733  304.4022  199.9502
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628    0.3628

4F2(Hz)
  mean           1448.2374 1139.7769 1796.4609 1193.1191 2002.2125  1145.619  783.6108  145.6977 1332.6509 1445.2293 1063.4579 1469.5062
  std. dev.       448.6029  866.5606  511.4411  771.0706  633.8216   528.768  859.5135  482.7575  730.2324    391.85  420.6329  614.4609
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452    0.4452

4F3(Hz)
  mean           2665.0519 1894.4352 2636.0758 2027.9477 2693.6179  2513.778 1343.1362  249.4435 2248.6819 2791.4444 2692.0225 2533.9315
  std. dev.       801.5039 1417.7185  763.9923 1301.1049  876.6153 1011.4708 1461.9806  812.1745 1174.9913  665.7853   759.659  992.8328
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587    0.6587

4F4(Hz)
  mean            3089.112    2167.4 3175.4825 2421.4135 3200.0296 2890.9482 1542.3552  300.7336 2697.4569 3246.0224 3085.5469 2910.8191
  std. dev.       915.0775 1608.5329  884.6328 1516.9857  997.3182 1145.4649 1668.0224  976.9899 1376.7352  750.8035  770.8999 1142.9278
  weight sum          4825      1495      2347       430      1206      2251      2912     11607       980       453       697      1246
  precision         0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801    0.6801





=== Stratified cross-validation ===

Correctly Classified Instances       14480               47.5549 %
Incorrectly Classified Instances     15969               52.4451 %
Kappa statistic                          0.3523
K&B Relative Info Score             959990.6456 %
K&B Information Score                27714.971  bits      0.9102 bits/instance
Class complexity | order 0           87884.2027 bits      2.8863 bits/instance
Class complexity | scheme           845141.6052 bits     27.756  bits/instance
Complexity improvement     (Sf)    -757257.4025 bits    -24.8697 bits/instance
Mean absolute error                      0.0874
Root mean squared error                  0.2838
Relative absolute error                 65.464  %
Root relative squared error            109.8116 %
Coverage of cases (0.95 level)          52.2809 %
Mean rel. region size (0.95 level)      10.9366 %
Total Number of Instances            30449     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,418    0,043    0,645      0,418    0,507      0,451    0,862     0,583     A
                 0,018    0,012    0,070      0,018    0,029      0,011    0,681     0,078     LGJKC
                 0,236    0,025    0,442      0,236    0,308      0,284    0,835     0,325     E
                 0,072    0,026    0,038      0,072    0,050      0,034    0,733     0,030     FV
                 0,294    0,023    0,341      0,294    0,315      0,290    0,856     0,265     I
                 0,005    0,001    0,212      0,005    0,010      0,022    0,749     0,162     O
                 0,205    0,040    0,352      0,205    0,259      0,212    0,768     0,278     SNRTY
                 0,892    0,120    0,821      0,892    0,855      0,761    0,919     0,840     0
                 0,005    0,002    0,079      0,005    0,010      0,012    0,714     0,057     BMP
                 0,770    0,278    0,040      0,770    0,076      0,132    0,826     0,054     DZ
                 0,231    0,009    0,375      0,231    0,286      0,282    0,850     0,226     U
                 0,012    0,009    0,055      0,012    0,020      0,007    0,691     0,064     SNRTXY
Weighted Avg.    0,476    0,065    0,530      0,476    0,479      0,427    0,841     0,501     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  2017   106   211   101    88    18   161   223     1  1803    70    26 |     a = A
    81    27    51    52    60     8   194   362     8   610     6    36 |     b = LGJKC
   245    70   555    47   133     6    81    92     4  1060    38    16 |     c = E
    24     5    12    31     9     0    40    79     1   218     2     9 |     d = FV
    66    41   220    31   354     0    71    64     4   344     7     4 |     e = I
   277    58    67    95    42    11    91   228     3  1270    93    16 |     f = O
   115    30    43   190    85     3   598   965    21   757    15    90 |     g = SNRTY
    29    11    41   139    97     1   238 10357    14   616    13    51 |     h = 0
    63    12     9    41    87     1    69   153     5   513    18     9 |     i = BMP
    49     0     8    11     6     0    11    13     0   349     3     3 |     j = DZ
    73    27     9    14    24     4    33    17     1   334   161     0 |     k = U
    89     1    30    56    54     0   111    65     1   821     3    15 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
41.8	2.2	4.37	2.09	1.82	0.37	3.34	4.62	0.02	37.37	1.45	0.54	 a = A
5.42	1.81	3.41	3.48	4.01	0.54	12.98	24.21	0.54	40.8	0.4	2.41	 b = LGJKC
10.44	2.98	23.65	2.0	5.67	0.26	3.45	3.92	0.17	45.16	1.62	0.68	 c = E
5.58	1.16	2.79	7.21	2.09	0.0	9.3	18.37	0.23	50.7	0.47	2.09	 d = FV
5.47	3.4	18.24	2.57	29.35	0.0	5.89	5.31	0.33	28.52	0.58	0.33	 e = I
12.31	2.58	2.98	4.22	1.87	0.49	4.04	10.13	0.13	56.42	4.13	0.71	 f = O
3.95	1.03	1.48	6.52	2.92	0.1	20.54	33.14	0.72	26.0	0.52	3.09	 g = SNRTY
0.25	0.09	0.35	1.2	0.84	0.01	2.05	89.23	0.12	5.31	0.11	0.44	 h = 0
6.43	1.22	0.92	4.18	8.88	0.1	7.04	15.61	0.51	52.35	1.84	0.92	 i = BMP
10.82	0.0	1.77	2.43	1.32	0.0	2.43	2.87	0.0	77.04	0.66	0.66	 j = DZ
10.47	3.87	1.29	2.01	3.44	0.57	4.73	2.44	0.14	47.92	23.1	0.0	 k = U
7.14	0.08	2.41	4.49	4.33	0.0	8.91	5.22	0.08	65.89	0.24	1.2	 l = SNRTXY
