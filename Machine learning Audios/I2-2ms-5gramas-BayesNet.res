Timestamp:2013-08-30-04:35:26
Inventario:I2
Intervalo:2ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I2-2ms-5gramas
Num Instances:  91699
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(24): class 
0Pitch(Hz)(44): class 
0RawPitch(31): class 
0SmPitch(39): class 
0Melogram(st)(45): class 
0ZCross(16): class 
0F1(Hz)(1402): class 
0F2(Hz)(2387): class 
0F3(Hz)(2541): class 
0F4(Hz)(2438): class 
1Int(dB)(24): class 
1Pitch(Hz)(43): class 
1RawPitch(32): class 
1SmPitch(42): class 
1Melogram(st)(45): class 
1ZCross(16): class 
1F1(Hz)(1338): class 
1F2(Hz)(2203): class 
1F3(Hz)(2483): class 
1F4(Hz)(2484): class 
2Int(dB)(23): class 
2Pitch(Hz)(44): class 
2RawPitch(34): class 
2SmPitch(39): class 
2Melogram(st)(46): class 
2ZCross(16): class 
2F1(Hz)(1715): class 
2F2(Hz)(2221): class 
2F3(Hz)(2405): class 
2F4(Hz)(2304): class 
3Int(dB)(25): class 
3Pitch(Hz)(46): class 
3RawPitch(33): class 
3SmPitch(36): class 
3Melogram(st)(46): class 
3ZCross(16): class 
3F1(Hz)(1679): class 
3F2(Hz)(2163): class 
3F3(Hz)(2662): class 
3F4(Hz)(2583): class 
4Int(dB)(23): class 
4Pitch(Hz)(46): class 
4RawPitch(35): class 
4SmPitch(39): class 
4Melogram(st)(47): class 
4ZCross(16): class 
4F1(Hz)(1632): class 
4F2(Hz)(2133): class 
4F3(Hz)(2518): class 
4F4(Hz)(2413): class 
class(12): 
LogScore Bayes: -1.2416190177201701E7
LogScore BDeu: -1.9113910440459527E7
LogScore MDL: -1.7352016319083914E7
LogScore ENTROPY: -1.4289828247464305E7
LogScore AIC: -1.4825819247464305E7




=== Stratified cross-validation ===

Correctly Classified Instances       72128               78.6573 %
Incorrectly Classified Instances     19571               21.3427 %
Kappa statistic                          0.7281
K&B Relative Info Score            6779301.2155 %
K&B Information Score               194118.6375 bits      2.1169 bits/instance
Class complexity | order 0          262551.22   bits      2.8632 bits/instance
Class complexity | scheme          1130143.035  bits     12.3245 bits/instance
Complexity improvement     (Sf)    -867591.815  bits     -9.4613 bits/instance
Mean absolute error                      0.0357
Root mean squared error                  0.1835
Relative absolute error                 26.9033 %
Root relative squared error             71.2808 %
Coverage of cases (0.95 level)          80.5407 %
Mean rel. region size (0.95 level)       8.9704 %
Total Number of Instances            91699     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,813    0,015    0,912      0,813    0,859      0,837    0,960     0,915     A
                 0,467    0,008    0,737      0,467    0,572      0,571    0,910     0,600     LGJKC
                 0,848    0,021    0,764      0,848    0,804      0,789    0,962     0,870     E
                 0,632    0,008    0,516      0,632    0,568      0,565    0,907     0,625     FV
                 0,860    0,012    0,749      0,860    0,801      0,794    0,959     0,854     I
                 0,774    0,009    0,865      0,774    0,817      0,805    0,927     0,844     O
                 0,367    0,020    0,655      0,367    0,471      0,453    0,941     0,629     SNRTY
                 0,900    0,138    0,808      0,900    0,851      0,750    0,966     0,949     0
                 0,690    0,007    0,768      0,690    0,727      0,719    0,904     0,727     BMP
                 0,889    0,020    0,394      0,889    0,546      0,584    0,972     0,800     DZ
                 0,914    0,007    0,761      0,914    0,830      0,830    0,977     0,920     U
                 0,806    0,010    0,770      0,806    0,788      0,779    0,938     0,826     SNRTXY
Weighted Avg.    0,787    0,062    0,791      0,787    0,780      0,733    0,953     0,860     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 11649   133   309   164   134   181   316   740   115   343    60   189 |     a = A
    95  2070   225    15   150    51   343  1205    20   120    88    46 |     b = LGJKC
    77    91  5863    65    90    18   158   273    57   115    17    86 |     c = E
    22     5    28   813    30    14    42   304     9    14     5     0 |     d = FV
     9    37    62    15  3064     7   111   181    34    16     3    22 |     e = I
   121    45   124    18    31  5144    98   706    56   217    41    45 |     f = O
   337   108   411    77   185   119  3182  3363   117   435   176   153 |     g = SNRTY
   368   273   481   381   323   345   296 32249   175   481   141   316 |     h = 0
    38    18    53     6    58    32    88   523  2012    35    49     3 |     i = BMP
    18     0    30     9     0    14    12    41     1  1197     4    20 |     j = DZ
     4    28     3    10     3     4    46    40    19    11  1901    11 |     k = U
    40     2    83     2    21    20   167   308     6    56    13  2984 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
81.27	0.93	2.16	1.14	0.93	1.26	2.2	5.16	0.8	2.39	0.42	1.32	 a = A
2.15	46.75	5.08	0.34	3.39	1.15	7.75	27.21	0.45	2.71	1.99	1.04	 b = LGJKC
1.11	1.32	84.85	0.94	1.3	0.26	2.29	3.95	0.82	1.66	0.25	1.24	 c = E
1.71	0.39	2.18	63.22	2.33	1.09	3.27	23.64	0.7	1.09	0.39	0.0	 d = FV
0.25	1.04	1.74	0.42	86.04	0.2	3.12	5.08	0.95	0.45	0.08	0.62	 e = I
1.82	0.68	1.87	0.27	0.47	77.4	1.47	10.62	0.84	3.27	0.62	0.68	 f = O
3.89	1.25	4.74	0.89	2.14	1.37	36.73	38.82	1.35	5.02	2.03	1.77	 g = SNRTY
1.03	0.76	1.34	1.06	0.9	0.96	0.83	90.01	0.49	1.34	0.39	0.88	 h = 0
1.3	0.62	1.82	0.21	1.99	1.1	3.02	17.94	69.02	1.2	1.68	0.1	 i = BMP
1.34	0.0	2.23	0.67	0.0	1.04	0.89	3.05	0.07	88.93	0.3	1.49	 j = DZ
0.19	1.35	0.14	0.48	0.14	0.19	2.21	1.92	0.91	0.53	91.39	0.53	 k = U
1.08	0.05	2.24	0.05	0.57	0.54	4.51	8.32	0.16	1.51	0.35	80.61	 l = SNRTXY
