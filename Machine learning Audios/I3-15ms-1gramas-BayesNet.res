Timestamp:2013-08-30-05:05:41
Inventario:I3
Intervalo:15ms
Ngramas:1
Clasificador:BayesNet
Relation Name:  I3-15ms-1gramas
Num Instances:  12189
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
0Int(dB)(9): class 
0Pitch(Hz)(6): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(7): class 
0ZCross(10): class 
0F1(Hz)(11): class 
0F2(Hz)(9): class 
0F3(Hz)(8): class 
0F4(Hz)(7): class 
class(11): 
LogScore Bayes: -147091.11230517627
LogScore BDeu: -150243.95690823937
LogScore MDL: -150027.89669135844
LogScore ENTROPY: -146617.3918619207
LogScore AIC: -147342.3918619207




=== Stratified cross-validation ===

Correctly Classified Instances        7447               61.0961 %
Incorrectly Classified Instances      4742               38.9039 %
Kappa statistic                          0.5026
K&B Relative Info Score             628169.6847 %
K&B Information Score                16656.0656 bits      1.3665 bits/instance
Class complexity | order 0           32291.476  bits      2.6492 bits/instance
Class complexity | scheme            43875.6514 bits      3.5996 bits/instance
Complexity improvement     (Sf)     -11584.1754 bits     -0.9504 bits/instance
Mean absolute error                      0.0765
Root mean squared error                  0.2317
Relative absolute error                 53.2908 %
Root relative squared error             86.4991 %
Coverage of cases (0.95 level)          79.7768 %
Mean rel. region size (0.95 level)      23.2087 %
Total Number of Instances            12189     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,724    0,091    0,608      0,724    0,661      0,591    0,904     0,727     A
                 0,625    0,083    0,393      0,625    0,483      0,441    0,892     0,497     E
                 0,178    0,043    0,493      0,178    0,262      0,209    0,800     0,433     CGJLNQSRT
                 0,023    0,003    0,098      0,023    0,037      0,040    0,770     0,043     FV
                 0,626    0,039    0,410      0,626    0,495      0,481    0,923     0,515     I
                 0,373    0,035    0,472      0,373    0,417      0,378    0,831     0,418     O
                 0,936    0,136    0,796      0,936    0,861      0,778    0,974     0,965     0
                 0,030    0,005    0,164      0,030    0,051      0,057    0,789     0,123     BMP
                 0,463    0,015    0,418      0,463    0,439      0,426    0,924     0,485     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,910     0,003     DZ
                 0,198    0,022    0,118      0,198    0,148      0,136    0,862     0,113     D
Weighted Avg.    0,611    0,084    0,585      0,611    0,575      0,517    0,898     0,662     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1441  175  101    5   41   93   96    0   10    2   25 |    a = A
  138  607   51    0   72   24   36    2   24    0   17 |    b = E
  347  320  414    8  189  118  709   30   64    1  125 |    c = CGJLNQSRT
   25   40   26    4   12   17   33    5    4    1   10 |    d = FV
   13  104   36    1  315    1   20    4    6    0    3 |    e = I
  228  109   49    3   20  351   95    3   38    0   44 |    f = O
   67   38   94   17   34   22 4136    1    4    1    4 |    g = 0
   47   76   47    2   72   34   57   12   28    0   24 |    h = BMP
   29   29   10    0    5   56    5    9  132    0   10 |    i = U
    0    0    0    0    0    0    5    0    0    0    0 |    j = DZ
   35   45   11    1    9   27    1    7    6    0   35 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
72.45	8.8	5.08	0.25	2.06	4.68	4.83	0.0	0.5	0.1	1.26	 a = A
14.21	62.51	5.25	0.0	7.42	2.47	3.71	0.21	2.47	0.0	1.75	 b = E
14.92	13.76	17.81	0.34	8.13	5.08	30.49	1.29	2.75	0.04	5.38	 c = CGJLNQSRT
14.12	22.6	14.69	2.26	6.78	9.6	18.64	2.82	2.26	0.56	5.65	 d = FV
2.58	20.68	7.16	0.2	62.62	0.2	3.98	0.8	1.19	0.0	0.6	 e = I
24.26	11.6	5.21	0.32	2.13	37.34	10.11	0.32	4.04	0.0	4.68	 f = O
1.52	0.86	2.13	0.38	0.77	0.5	93.62	0.02	0.09	0.02	0.09	 g = 0
11.78	19.05	11.78	0.5	18.05	8.52	14.29	3.01	7.02	0.0	6.02	 h = BMP
10.18	10.18	3.51	0.0	1.75	19.65	1.75	3.16	46.32	0.0	3.51	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
19.77	25.42	6.21	0.56	5.08	15.25	0.56	3.95	3.39	0.0	19.77	 k = D
