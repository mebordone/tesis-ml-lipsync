Timestamp:2013-07-23-03:05:25
Inventario:I4
Intervalo:6ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I4-6ms-3gramas
Num Instances:  30451
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  15 1Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
  25 2Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.242





=== Stratified cross-validation ===

Correctly Classified Instances       24766               81.3307 %
Incorrectly Classified Instances      5685               18.6693 %
Kappa statistic                          0.7578
K&B Relative Info Score            2174342.1168 %
K&B Information Score                55827.6698 bits      1.8334 bits/instance
Class complexity | order 0           78167.8173 bits      2.567  bits/instance
Class complexity | scheme          1445524.2099 bits     47.4705 bits/instance
Complexity improvement     (Sf)    -1367356.3926 bits    -44.9035 bits/instance
Mean absolute error                      0.0585
Root mean squared error                  0.167 
Relative absolute error                 37.6782 %
Root relative squared error             59.9335 %
Coverage of cases (0.95 level)          95.6356 %
Mean rel. region size (0.95 level)      24.8005 %
Total Number of Instances            30451     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,697    0,001    0,816      0,697    0,752      0,753    0,951     0,766     C
                 0,813    0,024    0,738      0,813    0,773      0,754    0,961     0,822     E
                 0,458    0,004    0,644      0,458    0,535      0,538    0,916     0,485     FV
                 0,862    0,051    0,806      0,862    0,833      0,790    0,960     0,902     AI
                 0,755    0,065    0,684      0,755    0,718      0,663    0,932     0,770     CDGKNRSYZ
                 0,719    0,013    0,811      0,719    0,762      0,746    0,937     0,794     O
                 0,909    0,060    0,903      0,909    0,906      0,848    0,972     0,953     0
                 0,404    0,008    0,654      0,404    0,499      0,500    0,872     0,458     LT
                 0,786    0,002    0,903      0,786    0,840      0,839    0,971     0,873     U
                 0,554    0,005    0,801      0,554    0,655      0,657    0,907     0,646     MBP
Weighted Avg.    0,813    0,047    0,813      0,813    0,810      0,768    0,953     0,855     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   129    12     1    19    10     2     8     0     2     2 |     a = C
     6  1907     9   151   162    28    47    23     3    11 |     b = E
     1    35   197    42   107    17    22     4     0     5 |     c = FV
     8   199    10  5196   293    57   197    45     6    20 |     d = AI
     4   167    34   367  3626    95   394    68    14    33 |     e = CDGKNRSYZ
     3    50     9   150   179  1618   189    25    13    15 |     f = O
     1   106    24   246   499    99 10550    47     7    30 |     g = 0
     3    43     6   156   253    21   173   452     5     7 |     h = LT
     2    17     3    26    34    32    15     8   548    12 |     i = U
     1    49    13    96   139    27    84    19     9   543 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
69.73	6.49	0.54	10.27	5.41	1.08	4.32	0.0	1.08	1.08	 a = C
0.26	81.25	0.38	6.43	6.9	1.19	2.0	0.98	0.13	0.47	 b = E
0.23	8.14	45.81	9.77	24.88	3.95	5.12	0.93	0.0	1.16	 c = FV
0.13	3.3	0.17	86.15	4.86	0.95	3.27	0.75	0.1	0.33	 d = AI
0.08	3.48	0.71	7.64	75.51	1.98	8.2	1.42	0.29	0.69	 e = CDGKNRSYZ
0.13	2.22	0.4	6.66	7.95	71.88	8.4	1.11	0.58	0.67	 f = O
0.01	0.91	0.21	2.12	4.3	0.85	90.88	0.4	0.06	0.26	 g = 0
0.27	3.84	0.54	13.94	22.61	1.88	15.46	40.39	0.45	0.63	 h = LT
0.29	2.44	0.43	3.73	4.88	4.59	2.15	1.15	78.62	1.72	 i = U
0.1	5.0	1.33	9.8	14.18	2.76	8.57	1.94	0.92	55.41	 j = MBP
