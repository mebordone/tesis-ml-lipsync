Timestamp:2013-07-22-22:27:09
Inventario:I3
Intervalo:3ms
Ngramas:4
Clasificador:RandomForest
Relation Name:  I3-3ms-4gramas
Num Instances:  61132
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1615





=== Stratified cross-validation ===

Correctly Classified Instances       53884               88.1437 %
Incorrectly Classified Instances      7248               11.8563 %
Kappa statistic                          0.8465
K&B Relative Info Score            4949507.7185 %
K&B Information Score               128697.325  bits      2.1052 bits/instance
Class complexity | order 0          158933.1498 bits      2.5998 bits/instance
Class complexity | scheme          1876269.0125 bits     30.6921 bits/instance
Complexity improvement     (Sf)    -1717335.8627 bits    -28.0923 bits/instance
Mean absolute error                      0.0377
Root mean squared error                  0.131 
Relative absolute error                 26.6945 %
Root relative squared error             49.3098 %
Coverage of cases (0.95 level)          97.1799 %
Mean rel. region size (0.95 level)      18.6394 %
Total Number of Instances            61132     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,912    0,018    0,905      0,912    0,909      0,892    0,976     0,946     A
                 0,888    0,010    0,874      0,888    0,881      0,871    0,977     0,925     E
                 0,838    0,056    0,773      0,838    0,804      0,759    0,959     0,864     CGJLNQSRT
                 0,606    0,002    0,837      0,606    0,703      0,709    0,928     0,693     FV
                 0,857    0,004    0,903      0,857    0,879      0,875    0,971     0,905     I
                 0,828    0,006    0,912      0,828    0,868      0,859    0,954     0,883     O
                 0,928    0,053    0,917      0,928    0,922      0,872    0,982     0,964     0
                 0,704    0,003    0,886      0,704    0,785      0,784    0,935     0,791     BMP
                 0,889    0,001    0,959      0,889    0,923      0,921    0,984     0,945     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,498     0,000     DZ
                 0,834    0,001    0,919      0,834    0,874      0,874    0,995     0,925     D
Weighted Avg.    0,881    0,035    0,883      0,881    0,881      0,849    0,972     0,921     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
  8742    58   433     5    15    34   267    16     7     0     8 |     a = A
    71  4104   263     9    23    12   103    21     2     0    13 |     b = E
   365   200  9433    38    51    92   985    56    21     4    11 |     c = CGJLNQSRT
    27    25   217   524    13    21    32     4     0     0     1 |     d = FV
    26    48   149    12  2041     5    84    15     1     0     1 |     e = I
   117    27   229     6     8  3696   339    22     2     0    17 |     f = O
   206   148  1093    23    68   121 22006    33    12     1    12 |     g = 0
    54    42   265     7    32    29   142  1371     6     0     0 |     h = BMP
    16    16    66     0     6    14    25     9  1232     0     2 |     i = U
     2     0    20     0     0     0     1     0     0     0     0 |     j = DZ
    30    28    34     2     3    30    17     0     2     0   735 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
91.21	0.61	4.52	0.05	0.16	0.35	2.79	0.17	0.07	0.0	0.08	 a = A
1.54	88.81	5.69	0.19	0.5	0.26	2.23	0.45	0.04	0.0	0.28	 b = E
3.24	1.78	83.8	0.34	0.45	0.82	8.75	0.5	0.19	0.04	0.1	 c = CGJLNQSRT
3.13	2.89	25.12	60.65	1.5	2.43	3.7	0.46	0.0	0.0	0.12	 d = FV
1.09	2.02	6.26	0.5	85.68	0.21	3.53	0.63	0.04	0.0	0.04	 e = I
2.62	0.6	5.13	0.13	0.18	82.81	7.6	0.49	0.04	0.0	0.38	 f = O
0.87	0.62	4.61	0.1	0.29	0.51	92.76	0.14	0.05	0.0	0.05	 g = 0
2.77	2.16	13.6	0.36	1.64	1.49	7.29	70.38	0.31	0.0	0.0	 h = BMP
1.15	1.15	4.76	0.0	0.43	1.01	1.8	0.65	88.89	0.0	0.14	 i = U
8.7	0.0	86.96	0.0	0.0	0.0	4.35	0.0	0.0	0.0	0.0	 j = DZ
3.41	3.18	3.86	0.23	0.34	3.41	1.93	0.0	0.23	0.0	83.43	 k = D
