Timestamp:2014-12-07-11:18:57
Inventario:I4
Intervalo:10ms
Ngramas:5
Clasificador:NaiveBayes
Relation Name:  I4-10ms-5gramas
Num Instances:  18292
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
   3 0RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
   4 0SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
   5 0Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
   7 0F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
   9 0F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  10 0F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  13 1RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  14 1SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  15 1Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  17 1F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  18 1F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  19 1F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  20 1F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  23 2RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  24 2SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  25 2Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  27 2F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  28 2F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  29 2F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  30 2F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  32 3Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   816 /  4%  3063 
  33 3RawPitch                  Num   0%  54%  46%     0 /  0%   853 /  5%  2955 
  34 3SmPitch                   Num   0%  52%  48%     0 /  0%   827 /  5%  3024 
  35 3Melogram(st)              Num   0%  47%  53%     0 /  0%   581 /  3%  2807 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  37 3F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  38 3F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  39 3F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  40 3F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%     7 /  0%   772 
  42 4Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   817 /  4%  3064 
  43 4RawPitch                  Num   0%  54%  46%     0 /  0%   854 /  5%  2956 
  44 4SmPitch                   Num   0%  52%  48%     0 /  0%   828 /  5%  3025 
  45 4Melogram(st)              Num   0%  47%  53%     0 /  0%   582 /  3%  2808 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    42 
  47 4F1(Hz)                    Num   0%  51%  49%     0 /  0%  2131 / 12%  4724 
  48 4F2(Hz)                    Num   0%  51%  49%     0 /  0%  4210 / 23%  6615 
  49 4F3(Hz)                    Num   0%  51%  49%     0 /  0%  3688 / 20%  6145 
  50 4F4(Hz)                    Num   0%  51%  49%     0 /  0%  3689 / 20%  6054 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Naive Bayes Classifier

                    Class
Attribute               C         E        FV        AI CDGKNRSYZ         O         0        LT         U       MBP
                   (0.01)    (0.08)    (0.01)     (0.2)    (0.16)    (0.08)    (0.37)    (0.04)    (0.02)    (0.03)
====================================================================================================================
0Int(dB)
  mean             -6.4529   -9.6111   -10.982   -8.0595  -13.4847  -11.2977  -48.0267  -16.0821  -10.2969  -11.3972
  std. dev.         7.3443   11.4443   11.4347   12.2769   12.3775     13.03   17.1401    13.394   12.4298   11.9964
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005

0Pitch(Hz)
  mean            218.0652  162.8814  162.8376  188.9807  148.9134  162.3445   19.6403  139.3106  196.4288  176.5923
  std. dev.        89.3448  100.9376  106.0306  116.1003  107.8563  115.4514   60.4023  109.9958  121.8438  106.6658
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626

0RawPitch
  mean            208.5068  157.1635  143.5452  178.5736  134.6309   154.051    16.296  122.7151  185.1858  167.2471
  std. dev.        99.7777   104.273  109.1897  119.0135  112.2226  119.4964   54.9452  109.6034   124.139  111.4688
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679

0SmPitch
  mean            216.2366  160.1278  154.4791   186.874  144.3739  160.0958   17.9599  133.4932  191.6402  173.7853
  std. dev.        91.7601  103.2014   106.176  116.9778  109.6288  116.5286   56.9887  110.1176  124.0826  108.2015
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647

0Melogram(st)
  mean             52.7437   43.9709   45.1576   46.3475   39.8194   42.2639    5.6482   38.6717    43.858   45.9865
  std. dev.        15.2862    21.953   20.6297   21.6151   24.6247   23.8573    16.628   25.0621   25.2949   21.1991
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294

0ZCross
  mean              5.1828    4.2718    3.4224    5.6022    3.3091     3.572    0.3724    2.2665    3.5052    3.0467
  std. dev.         3.0277    3.6952    3.1109     5.072    3.7441    3.7147    1.6643    2.4399    3.5492    3.0136
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927

0F1(Hz)
  mean            533.8202  397.3557  499.4316   527.655    440.04  404.7558   67.3207  381.3129  421.2912   435.447
  std. dev.       175.5754  220.5354  230.5154  327.9384   262.745  263.5378  189.0983  259.8712  363.4485  227.9619
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688

0F2(Hz)
  mean           1720.6808 1452.2339 1440.7522 1349.9804  1319.111 1058.6386   201.594 1233.5552  898.5293 1470.3392
  std. dev.        488.576  782.1386  648.3082  696.9298  745.7677  680.2641  559.6456  787.6783  631.6566  691.4076
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504

0F3(Hz)
  mean           3021.9249 2228.1348 2441.7175  2346.106 2272.7361 2179.4174  344.8935 2109.5279 2149.9078 2460.7556
  std. dev.       761.4372 1168.5798 1052.2514 1148.1158 1231.3065 1259.3843   936.734 1305.2621 1279.6717 1067.6148
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705

0F4(Hz)
  mean           2978.9753 2704.2382 2859.7375 2742.4349 2621.1316 2526.1338  415.0202 2403.8574 2470.7103 2886.9193
  std. dev.       761.7583 1397.5563 1204.9182  1328.849 1403.1602 1451.9381  1124.632 1481.9803 1423.5708 1222.9136
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851

1Int(dB)
  mean             -6.7085   -8.2832   -12.229   -7.1833  -14.2874  -10.1884  -48.5012  -17.3068    -9.141  -12.0773
  std. dev.          7.364   10.5574   10.7764    11.503   11.9271   12.4847   16.5463   13.4707    11.517   11.9572
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005

1Pitch(Hz)
  mean            218.5288  171.3761  155.4117  194.9304  141.3715  169.4942   17.6176  128.4001  206.5505  171.2937
  std. dev.         90.751   97.3583  107.1208  112.0096  110.3402  112.1681     57.63  108.7944  112.6088  108.4803
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626

1RawPitch
  mean             208.433  165.9268  134.3425  184.2271  126.6001  160.7417   14.7182   112.071  196.9115  160.9589
  std. dev.       101.0409  101.4248  108.4897  115.7899  113.6728  117.1537   52.3175  107.3734  116.0658  112.6321
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679

1SmPitch
  mean            216.7046  168.9406  147.5141  192.6957  136.5611  166.8657   15.9871   122.639  203.2317  168.8457
  std. dev.        93.1388   99.8036  106.2865  113.2299  111.7065  113.6276   53.9948  108.6587  114.3476  109.5157
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647

1Melogram(st)
  mean             52.7475   46.0304   43.7744   47.8712   37.6591   44.1418    5.1053   36.4493   46.9326   45.0143
  std. dev.        15.3301   20.3297   21.5454   20.1804   25.5957   22.6348   15.8999    25.713   23.1329   21.9175
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294

1ZCross
  mean              4.9291    4.5442    2.8924    5.7585    3.2095    3.7485    0.3145    2.0374    3.4685    2.7268
  std. dev.         3.0103    3.6764    2.7663    4.8394    4.0613    3.6397    1.5734    2.3667    3.3113    2.7447
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927

1F1(Hz)
  mean            529.3915  414.4876  481.1684  551.2791  416.5574  427.1986   60.6401  349.1112  438.9512  420.5783
  std. dev.       179.1174  204.0813  244.4663  316.0641  271.3028  255.5101  179.8321  263.7445  351.0171   233.997
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688

1F2(Hz)
  mean           1736.0184 1540.0099 1382.0687 1408.5939  1259.428   1087.12  182.9445 1147.8219  933.7491 1431.5618
  std. dev.       498.0707  736.2298  677.1876  660.0555  778.3079  643.6083  536.0273  817.2674  589.8158  712.6322
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504

1F3(Hz)
  mean           3017.1308 2334.9624 2354.9866 2438.1888 2167.0875 2276.2654  313.3268 1963.7106 2280.9865 2400.4431
  std. dev.       768.3313 1092.4038 1117.9617 1071.3908 1291.6694 1203.3602  898.7423 1367.3241 1204.2554 1114.3931
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705

1F4(Hz)
  mean           2987.0366 2827.1616 2769.7197 2850.5395 2500.9426 2631.6643  377.1315 2239.6191 2611.7192 2825.1944
  std. dev.       768.7934 1299.7201 1285.7643 1238.3054 1476.1834 1382.1236 1079.4377 1551.8968 1326.9291 1281.0444
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851

2Int(dB)
  mean             -7.0016   -6.9781   -13.453   -6.3329  -15.0396   -9.2289  -48.9843   -18.344    -7.821  -12.6892
  std. dev.         7.3095     9.587    9.9848   10.6492   11.2462   12.1503   15.8913   13.3214   10.4376   11.7406
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005

2Pitch(Hz)
  mean            219.2614  179.7817  147.5196  200.9328  133.3928  175.9738    15.546  119.7295  217.8099  166.9471
  std. dev.        91.8205   93.2543  107.4902  107.3968  112.2156  108.3955   54.3414  107.3728   101.251  109.8658
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626

2RawPitch
  mean             208.703  175.0704  125.4184  190.3913  117.8787  167.5572   12.9606  102.8483  208.6619  153.9342
  std. dev.       102.9477    97.669  107.1313  111.9338  114.2722  114.1306   49.1247  104.6379  106.1879  113.8843
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679

2SmPitch
  mean            217.4467  177.2466  139.8605  198.6154  128.3973  173.3248   13.9244  115.2132  215.6213  163.7838
  std. dev.        94.1938   95.7152  105.8427   108.961  113.3215   110.039   50.3099  106.8974  102.5619  111.3474
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647

2Melogram(st)
  mean              52.783   48.2348   41.8303   49.4355   35.4275   45.7502    4.6149    34.284   50.2186   43.8398
  std. dev.        15.3641   18.1696   22.7672   18.4609   26.3801   21.3432   15.1945    26.197   19.9908   22.8015
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294

2ZCross
  mean              4.6512     4.777    2.4071    5.9107    3.1442    3.9062    0.2453    1.8216    3.5022    2.4526
  std. dev.         2.9353    3.5473    2.4072    4.6692    4.4336    3.5254    1.2668    2.2569    3.0347    2.4563
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927

2F1(Hz)
  mean            521.4647  432.7945  457.1842  576.7681   389.737   450.362    53.593  323.1748  460.9396  403.2101
  std. dev.       179.3598  184.6991   256.028  301.8607     277.4  244.3368  169.3736  261.8126  335.9081  236.5419
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688

2F2(Hz)
  mean           1746.4904 1632.6744 1322.7123 1470.1943 1190.2888 1117.1452  162.6833 1086.0644  979.7948  1390.089
  std. dev.       504.7605   673.554  699.8891  615.8943  806.2202  603.6027  507.4891   832.925  538.5145    729.41
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504

2F3(Hz)
  mean           3010.0494 2447.1049 2268.6436  2533.848 2049.2265 2373.7752   279.465 1854.2511 2430.7754 2335.5553
  std. dev.       772.6826  996.0136  1175.068  979.1218 1345.9914 1134.1201  854.4639  1399.832  1089.578  1153.522
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705

2F4(Hz)
  mean           2997.2301  2953.752 2681.0905 2960.0494 2367.1259 2740.1433  336.5585 2123.7254 2777.4425  2763.331
  std. dev.       775.7465 1176.1247 1356.4028   1128.24 1542.8598 1297.7013 1026.8796  1593.511 1184.8746  1333.663
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851

3Int(dB)
  mean             -7.3322   -5.9386   -14.457   -5.6784   -15.618    -8.538  -49.4538  -18.4889    -6.523  -12.9285
  std. dev.         7.6223    8.7524    9.1174    9.9846     10.45   12.0283   15.1932   12.6608    9.2325   11.2858
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005

3Pitch(Hz)
  mean            218.1275  187.1289  141.0758  205.2387  127.0197   180.298   13.6938  118.1534  226.0414  164.0251
  std. dev.        95.0512   89.1848  107.2369   103.862  113.3109  104.9788   51.1571  105.4677   90.8899  110.3131
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626

3RawPitch
  mean            206.6453  182.7671  118.1042   194.723  111.0724  173.1119    11.117   99.3424  217.7899  151.3042
  std. dev.       107.5275   93.8486  105.1012  108.9604  114.3242   111.182   45.2445  102.4698   96.3272  114.0339
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679

3SmPitch
  mean            216.3151  185.0532  133.6333  203.0017  121.5226  177.9648   12.1462  112.8848  225.0495  160.0578
  std. dev.        97.3174    91.154  104.9032  105.5608  114.1845  106.8233   46.8965  104.8411   91.3186  112.2555
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647

3Melogram(st)
  mean             52.3042   50.1673   40.1599   50.6646   33.5543   46.7989    4.1839   33.5997   52.9506   42.9332
  std. dev.        16.2008    15.729   23.6891   16.8735   26.8996   20.3195   14.5516   26.2392   16.4082   23.4206
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294

3ZCross
  mean              4.4338    4.8777    2.0356    6.0537    3.1165    4.0104     0.176    1.7089    3.6091    2.2633
  std. dev.         2.8448    3.2885    2.1197    4.5907    4.7317    3.2415    0.9114    2.0853     2.929    2.1902
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927

3F1(Hz)
  mean            508.9472  452.4055  427.4933  601.6725  361.7945  474.0843     46.37  304.5369  482.6221  387.8062
  std. dev.        182.517  159.6131  266.6231   285.672  280.8176  229.5879  157.5981   257.521  319.3509  236.7462
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688

3F2(Hz)
  mean           1730.5088 1730.4304 1255.6134 1529.4088 1117.0915 1149.4791  141.7401 1045.9701 1027.4385 1355.9289
  std. dev.       532.3332  583.9406  728.5852   564.757  826.5133  557.6755  475.9872  838.9015  482.4236   740.105
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504

3F3(Hz)
  mean           2976.1527 2566.2209 2161.8658 2625.3755  1927.844 2474.5963  243.9108 1787.0178 2578.9648 2275.3297
  std. dev.       827.9906  864.4824 1236.4745  873.1643 1389.7671 1047.0894  803.9046 1417.9226   937.242 1178.2882
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705

3F4(Hz)
  mean           2973.4111 3090.2109 2569.1202 3063.7655 2228.6958 2853.8724  294.0316 2052.3429 2942.8419  2712.388
  std. dev.       831.3383 1010.4123  1435.181  1001.764 1597.0415 1192.6404  966.9098 1617.2317  994.8814 1372.7244
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851

4Int(dB)
  mean              -7.615   -5.4216  -14.8768   -5.4273   -15.907   -8.3525  -49.8229   -17.257   -5.5461  -12.4295
  std. dev.         7.8549    8.2134    8.5952    9.7157    9.8974   12.1594   14.6176   11.8848    7.9501   10.5978
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005    0.1005

4Pitch(Hz)
  mean            217.1294  190.3918  138.4937  206.5475  125.7212  179.1301   11.8171  127.2198  230.9859  166.0775
  std. dev.          97.99   87.4257  106.1938  102.6023  113.8857   103.979   47.9085  104.5917   83.0165  107.8218
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626    0.1626

4RawPitch
  mean             203.988  186.9416  114.3432  196.5165  108.2636  172.8881    9.4639  108.5129  221.8351  152.1597
  std. dev.       111.7049   92.1579  102.4081  107.8174  114.0645  110.0663   41.8811  102.4682   90.5835   112.921
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679    0.1679

4SmPitch
  mean            215.3063  188.8901  131.3637  204.4857  119.6298  177.1958    10.227  122.4141  230.1655   161.354
  std. dev.       100.1796   88.8533  103.6517  104.2821  114.6634  105.5718   43.0626  104.5656   83.5323   110.475
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647    0.1647

4Melogram(st)
  mean             51.8536    50.757   39.8199   51.1396   32.9778   46.9889    3.6614   35.8749   54.7081   43.3262
  std. dev.        16.9829   14.7786   23.9008   16.1076   27.1031   20.0024   13.7186   25.7164      13.2   23.0622
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294    0.0294

4ZCross
  mean              4.3613    4.8831     1.892    6.1024    3.1024    4.0329    0.1408    1.8063     3.713    2.2589
  std. dev.         2.7409    3.1607    1.9842    4.5486    4.8997    2.9611    0.7915    2.0083    2.8728    2.0707
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927    1.2927

4F1(Hz)
  mean            500.5551  467.7497  399.7197  620.2592  340.6224  488.3137   40.0819  301.5955  499.4556  383.8168
  std. dev.       184.0787  137.6849  273.4014  272.7775  280.0634  220.1308  146.5535  251.2771   302.477  232.2972
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688    0.3688

4F2(Hz)
  mean            1706.467 1804.3312 1203.5318 1571.1457 1064.7521 1157.6298   123.524 1056.1009 1072.0404 1349.6905
  std. dev.       559.5245  496.1344  754.0967  523.0015   835.656  525.4581  447.0477  834.8657  424.8655  728.3086
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504    0.4504

4F3(Hz)
  mean           2943.5343 2653.2835 2069.2644  2688.852 1843.0025 2526.8298  212.1309 1805.2244 2703.6172 2268.8241
  std. dev.       876.6084  742.6344 1282.9054  785.4546 1412.8812  995.0347  754.3882 1419.6008  758.4179 1165.0514
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705    0.6705

4F4(Hz)
  mean           2953.1522 3189.0151 2471.0333 3135.5866 2133.8449 2910.5036   256.116 2071.6081 3086.4504 2720.5333
  std. dev.       883.5322  856.4692 1495.7988  897.0935 1628.0142 1127.9374  908.5444 1617.3773  766.0819 1363.5495
  weight sum           107      1438       261      3688      2933      1377      6794       677       423       594
  precision         0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851    0.6851





=== Stratified cross-validation ===

Correctly Classified Instances        8209               44.8775 %
Incorrectly Classified Instances     10083               55.1225 %
Kappa statistic                          0.326 
K&B Relative Info Score             546790.3827 %
K&B Information Score                14125.6365 bits      0.7722 bits/instance
Class complexity | order 0           47237.6759 bits      2.5824 bits/instance
Class complexity | scheme           448205.5844 bits     24.5028 bits/instance
Complexity improvement     (Sf)    -400967.9085 bits    -21.9204 bits/instance
Mean absolute error                      0.11  
Root mean squared error                  0.3118
Relative absolute error                 70.4771 %
Root relative squared error            111.5814 %
Coverage of cases (0.95 level)          52.1485 %
Mean rel. region size (0.95 level)      14.8868 %
Total Number of Instances            18292     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,542    0,225    0,014      0,542    0,027      0,058    0,793     0,023     C
                 0,403    0,122    0,220      0,403    0,285      0,216    0,783     0,217     E
                 0,123    0,041    0,041      0,123    0,062      0,048    0,739     0,033     FV
                 0,246    0,025    0,712      0,246    0,366      0,349    0,816     0,546     AI
                 0,097    0,009    0,681      0,097    0,170      0,217    0,721     0,360     CDGKNRSYZ
                 0,018    0,005    0,238      0,018    0,034      0,047    0,756     0,181     O
                 0,897    0,103    0,838      0,897    0,866      0,784    0,928     0,853     0
                 0,127    0,064    0,071      0,127    0,091      0,048    0,696     0,065     LT
                 0,314    0,012    0,384      0,314    0,346      0,334    0,852     0,235     U
                 0,015    0,006    0,080      0,015    0,025      0,021    0,718     0,062     MBP
Weighted Avg.    0,449    0,059    0,614      0,449    0,461      0,428    0,827     0,526     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   58   34    2    6    0    0    3    4    0    0 |    a = C
  515  580   26   80   22   11   53  119   24    8 |    b = E
   92   56   32    5    4    0   38   31    2    1 |    c = FV
 1238  774  148  909   51   51  156  274   72   15 |    d = AI
  993  408  255  105  284    6  534  302   14   32 |    e = CDGKNRSYZ
  471  352   93   95    8   25  127  124   79    3 |    f = O
  230   96  123   19   29    5 6093  167    5   27 |    g = 0
  192  137   51    6    5    2  182   86    5   11 |    h = LT
  116   62    8   32    5    5   12   44  133    6 |    i = U
  236  134   40   19    9    0   76   59   12    9 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
54.21	31.78	1.87	5.61	0.0	0.0	2.8	3.74	0.0	0.0	 a = C
35.81	40.33	1.81	5.56	1.53	0.76	3.69	8.28	1.67	0.56	 b = E
35.25	21.46	12.26	1.92	1.53	0.0	14.56	11.88	0.77	0.38	 c = FV
33.57	20.99	4.01	24.65	1.38	1.38	4.23	7.43	1.95	0.41	 d = AI
33.86	13.91	8.69	3.58	9.68	0.2	18.21	10.3	0.48	1.09	 e = CDGKNRSYZ
34.2	25.56	6.75	6.9	0.58	1.82	9.22	9.01	5.74	0.22	 f = O
3.39	1.41	1.81	0.28	0.43	0.07	89.68	2.46	0.07	0.4	 g = 0
28.36	20.24	7.53	0.89	0.74	0.3	26.88	12.7	0.74	1.62	 h = LT
27.42	14.66	1.89	7.57	1.18	1.18	2.84	10.4	31.44	1.42	 i = U
39.73	22.56	6.73	3.2	1.52	0.0	12.79	9.93	2.02	1.52	 j = MBP
