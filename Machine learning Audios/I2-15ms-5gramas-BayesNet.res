Timestamp:2013-08-30-04:42:49
Inventario:I2
Intervalo:15ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I2-15ms-5gramas
Num Instances:  12185
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  42 4Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  43 4RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  44 4SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  45 4Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  47 4F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  48 4F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  49 4F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  50 4F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(6): class 
0ZCross(4): class 
0F1(Hz)(8): class 
0F2(Hz)(8): class 
0F3(Hz)(3): class 
0F4(Hz)(7): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(4): class 
1F1(Hz)(9): class 
1F2(Hz)(8): class 
1F3(Hz)(3): class 
1F4(Hz)(7): class 
2Int(dB)(8): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(4): class 
2F1(Hz)(9): class 
2F2(Hz)(8): class 
2F3(Hz)(6): class 
2F4(Hz)(6): class 
3Int(dB)(8): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(9): class 
3F1(Hz)(9): class 
3F2(Hz)(9): class 
3F3(Hz)(7): class 
3F4(Hz)(6): class 
4Int(dB)(10): class 
4Pitch(Hz)(6): class 
4RawPitch(4): class 
4SmPitch(4): class 
4Melogram(st)(7): class 
4ZCross(10): class 
4F1(Hz)(10): class 
4F2(Hz)(10): class 
4F3(Hz)(7): class 
4F4(Hz)(7): class 
class(12): 
LogScore Bayes: -615679.1638666502
LogScore BDeu: -629196.7238161303
LogScore MDL: -627789.6771012935
LogScore ENTROPY: -612835.7231457424
LogScore AIC: -616014.7231457426




=== Stratified cross-validation ===

Correctly Classified Instances        6533               53.6151 %
Incorrectly Classified Instances      5652               46.3849 %
Kappa statistic                          0.4301
K&B Relative Info Score             506763.7787 %
K&B Information Score                14844.108  bits      1.2182 bits/instance
Class complexity | order 0           35674.7455 bits      2.9278 bits/instance
Class complexity | scheme           172158.9167 bits     14.1288 bits/instance
Complexity improvement     (Sf)    -136484.1712 bits    -11.201  bits/instance
Mean absolute error                      0.0777
Root mean squared error                  0.2605
Relative absolute error                 57.4358 %
Root relative squared error            100.1399 %
Coverage of cases (0.95 level)          60.2462 %
Mean rel. region size (0.95 level)      12.142  %
Total Number of Instances            12185     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,493    0,054    0,641      0,493    0,557      0,490    0,872     0,638     A
                 0,010    0,006    0,086      0,010    0,018      0,013    0,691     0,081     LGJKC
                 0,347    0,037    0,447      0,347    0,391      0,348    0,859     0,382     E
                 0,079    0,018    0,061      0,079    0,069      0,054    0,784     0,043     FV
                 0,567    0,037    0,395      0,567    0,465      0,446    0,906     0,427     I
                 0,232    0,019    0,509      0,232    0,319      0,309    0,810     0,355     O
                 0,296    0,058    0,361      0,296    0,325      0,260    0,783     0,292     SNRTY
                 0,897    0,101    0,834      0,897    0,864      0,784    0,946     0,892     0
                 0,020    0,004    0,143      0,020    0,035      0,042    0,761     0,091     BMP
                 0,489    0,108    0,064      0,489    0,113      0,145    0,838     0,076     DZ
                 0,446    0,016    0,404      0,446    0,424      0,410    0,906     0,457     U
                 0,295    0,069    0,158      0,295    0,205      0,169    0,823     0,142     SNRTXY
Weighted Avg.    0,536    0,063    0,561      0,536    0,535      0,482    0,870     0,557     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
  980   21  142   35   47  103   96  112    3  318   27  105 |    a = A
   55    6   33   21   48   11   87  127    8   89    7  106 |    b = LGJKC
   92   16  337    5  108   12   39   57    6  158   31  110 |    c = E
   16    1   19   14    1    6   30   17    2   39    3   29 |    d = FV
    8    4   35    5  285    0   28   38    1   34    7   58 |    e = I
  134   10   53    9   37  218   47  106    4  211   54   57 |    f = O
   78    5   36   58   47   12  360  262   16  167   18  159 |    g = SNRTY
   23    3   24   49   34   15  191 3959    4   44    5   63 |    h = 0
   37    0   15   20   69    9   50   30    8   68   17   76 |    i = BMP
   28    0    9    3    6    3   11    3    0   89    8   22 |    j = DZ
   22    3   15    1   10   32   10   17    0   32  127   16 |    k = U
   56    1   36    9   30    7   49   18    4  139   10  150 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
49.27	1.06	7.14	1.76	2.36	5.18	4.83	5.63	0.15	15.99	1.36	5.28	 a = A
9.2	1.0	5.52	3.51	8.03	1.84	14.55	21.24	1.34	14.88	1.17	17.73	 b = LGJKC
9.47	1.65	34.71	0.51	11.12	1.24	4.02	5.87	0.62	16.27	3.19	11.33	 c = E
9.04	0.56	10.73	7.91	0.56	3.39	16.95	9.6	1.13	22.03	1.69	16.38	 d = FV
1.59	0.8	6.96	0.99	56.66	0.0	5.57	7.55	0.2	6.76	1.39	11.53	 e = I
14.26	1.06	5.64	0.96	3.94	23.19	5.0	11.28	0.43	22.45	5.74	6.06	 f = O
6.4	0.41	2.96	4.76	3.86	0.99	29.56	21.51	1.31	13.71	1.48	13.05	 g = SNRTY
0.52	0.07	0.54	1.11	0.77	0.34	4.33	89.69	0.09	1.0	0.11	1.43	 h = 0
9.27	0.0	3.76	5.01	17.29	2.26	12.53	7.52	2.01	17.04	4.26	19.05	 i = BMP
15.38	0.0	4.95	1.65	3.3	1.65	6.04	1.65	0.0	48.9	4.4	12.09	 j = DZ
7.72	1.05	5.26	0.35	3.51	11.23	3.51	5.96	0.0	11.23	44.56	5.61	 k = U
11.0	0.2	7.07	1.77	5.89	1.38	9.63	3.54	0.79	27.31	1.96	29.47	 l = SNRTXY
