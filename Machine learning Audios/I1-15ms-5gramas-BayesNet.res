Timestamp:2013-08-30-04:18:41
Inventario:I1
Intervalo:15ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I1-15ms-5gramas
Num Instances:  12185
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  22 2Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  23 2RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  24 2SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  25 2Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  27 2F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  28 2F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  29 2F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  30 2F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  32 3Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
  33 3RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
  34 3SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  35 3Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  37 3F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  38 3F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  39 3F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  40 3F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  41 4Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  42 4Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  43 4RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  44 4SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  45 4Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  47 4F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  48 4F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  49 4F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  50 4F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(6): class 
0ZCross(4): class 
0F1(Hz)(8): class 
0F2(Hz)(7): class 
0F3(Hz)(3): class 
0F4(Hz)(7): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(5): class 
1F1(Hz)(9): class 
1F2(Hz)(8): class 
1F3(Hz)(3): class 
1F4(Hz)(7): class 
2Int(dB)(9): class 
2Pitch(Hz)(4): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(7): class 
2ZCross(5): class 
2F1(Hz)(9): class 
2F2(Hz)(8): class 
2F3(Hz)(7): class 
2F4(Hz)(6): class 
3Int(dB)(9): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(8): class 
3F1(Hz)(9): class 
3F2(Hz)(9): class 
3F3(Hz)(7): class 
3F4(Hz)(6): class 
4Int(dB)(9): class 
4Pitch(Hz)(4): class 
4RawPitch(4): class 
4SmPitch(4): class 
4Melogram(st)(7): class 
4ZCross(9): class 
4F1(Hz)(11): class 
4F2(Hz)(10): class 
4F3(Hz)(7): class 
4F4(Hz)(7): class 
class(13): 
LogScore Bayes: -617772.2926592996
LogScore BDeu: -632685.5927283983
LogScore MDL: -631189.6371480945
LogScore ENTROPY: -614989.1283645404
LogScore AIC: -618433.1283645404




=== Stratified cross-validation ===

Correctly Classified Instances        6364               52.2281 %
Incorrectly Classified Instances      5821               47.7719 %
Kappa statistic                          0.4232
K&B Relative Info Score             494940.3359 %
K&B Information Score                14612.8552 bits      1.1992 bits/instance
Class complexity | order 0           35952.7849 bits      2.9506 bits/instance
Class complexity | scheme           172785.8578 bits     14.1802 bits/instance
Complexity improvement     (Sf)    -136833.0729 bits    -11.2296 bits/instance
Mean absolute error                      0.0738
Root mean squared error                  0.2546
Relative absolute error                 59.0496 %
Root relative squared error            101.8619 %
Coverage of cases (0.95 level)          59.9343 %
Mean rel. region size (0.95 level)      11.2692 %
Total Number of Instances            12185     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,489    0,053    0,641      0,489    0,555      0,487    0,870     0,645     a
                 0,038    0,006    0,174      0,038    0,062      0,067    0,760     0,096     b
                 0,357    0,045    0,410      0,357    0,382      0,333    0,852     0,370     e
                 0,560    0,119    0,066      0,560    0,119      0,161    0,848     0,092     d
                 0,096    0,019    0,069      0,096    0,081      0,066    0,782     0,048     f
                 0,563    0,041    0,370      0,563    0,447      0,428    0,900     0,421     i
                 0,304    0,057    0,095      0,304    0,145      0,141    0,825     0,083     k
                 0,290    0,033    0,092      0,290    0,140      0,147    0,852     0,064     j
                 0,056    0,017    0,273      0,056    0,093      0,084    0,782     0,236     l
                 0,228    0,018    0,507      0,228    0,314      0,305    0,807     0,345     o
                 0,883    0,065    0,885      0,883    0,884      0,818    0,961     0,953     0
                 0,283    0,034    0,348      0,283    0,312      0,275    0,840     0,308     s
                 0,449    0,016    0,396      0,449    0,421      0,407    0,900     0,453     u
Weighted Avg.    0,522    0,047    0,580      0,522    0,531      0,488    0,882     0,584     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l    m   <-- classified as
  972    9  151  335   40   57  113   43   31   99   65   44   30 |    a = a
   35   15   29   74   19   69   28   36   18    8   22   27   19 |    b = b
   85    6  347  169    6  106   57   80   17   16   32   23   27 |    c = e
   25    1    8  102    1    9    4    7   11    2    2    2    8 |    d = d
   11    2   24   49   17    1   20   14    5    5    8   19    2 |    e = f
    4    5   49   42    3  283   49   35    4    0   13   13    3 |    f = i
    4    2    3    5   10    8   72   10    7    3   84   29    0 |    g = k
   24    3   19   11    6    9    8   40    1    0    1   10    6 |    h = j
  152   12   94  373   34  103   97   79   68   22   68   87   27 |    i = l
  128    4   64  216    9   40   64   20   27  214   78   16   60 |    j = o
   29   13   23   50   54   38  123   14   30   18 3898  119    5 |    k = 0
   27   14   18   75   45   33   99   48   28    2  129  208    8 |    l = s
   20    0   18   35    1    8   24    8    2   33    7    1  128 |    m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
48.87	0.45	7.59	16.84	2.01	2.87	5.68	2.16	1.56	4.98	3.27	2.21	1.51	 a = a
8.77	3.76	7.27	18.55	4.76	17.29	7.02	9.02	4.51	2.01	5.51	6.77	4.76	 b = b
8.75	0.62	35.74	17.4	0.62	10.92	5.87	8.24	1.75	1.65	3.3	2.37	2.78	 c = e
13.74	0.55	4.4	56.04	0.55	4.95	2.2	3.85	6.04	1.1	1.1	1.1	4.4	 d = d
6.21	1.13	13.56	27.68	9.6	0.56	11.3	7.91	2.82	2.82	4.52	10.73	1.13	 e = f
0.8	0.99	9.74	8.35	0.6	56.26	9.74	6.96	0.8	0.0	2.58	2.58	0.6	 f = i
1.69	0.84	1.27	2.11	4.22	3.38	30.38	4.22	2.95	1.27	35.44	12.24	0.0	 g = k
17.39	2.17	13.77	7.97	4.35	6.52	5.8	28.99	0.72	0.0	0.72	7.25	4.35	 h = j
12.5	0.99	7.73	30.67	2.8	8.47	7.98	6.5	5.59	1.81	5.59	7.15	2.22	 i = l
13.62	0.43	6.81	22.98	0.96	4.26	6.81	2.13	2.87	22.77	8.3	1.7	6.38	 j = o
0.66	0.29	0.52	1.13	1.22	0.86	2.79	0.32	0.68	0.41	88.31	2.7	0.11	 k = 0
3.68	1.91	2.45	10.22	6.13	4.5	13.49	6.54	3.81	0.27	17.57	28.34	1.09	 l = s
7.02	0.0	6.32	12.28	0.35	2.81	8.42	2.81	0.7	11.58	2.46	0.35	44.91	 m = u
