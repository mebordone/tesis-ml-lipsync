Timestamp:2013-07-22-21:29:04
Inventario:I2
Intervalo:1ms
Ngramas:3
Clasificador:RandomForest
Relation Name:  I2-1ms-3gramas
Num Instances:  183402
Num Attributes: 31

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   780 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
   9 0F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   781 
  12 1Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  19 1F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     3 /  0%   781 
  22 2Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   256 /  0%  4213 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   417 /  0%  4119 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   263 /  0%  4193 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   133 /  0%  3454 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     4 /  0%    58 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1154 /  1%  4824 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2223 /  1%  6697 
  29 2F3(Hz)                    Num   0%  53%  47%     0 /  0%  1930 /  1%  6200 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1933 /  1%  6096 
  31 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.1265





=== Stratified cross-validation ===

Correctly Classified Instances      164367               89.6212 %
Incorrectly Classified Instances     19035               10.3788 %
Kappa statistic                          0.8688
K&B Relative Info Score            15763644.6079 %
K&B Information Score               450372.7857 bits      2.4557 bits/instance
Class complexity | order 0          523969.0404 bits      2.8569 bits/instance
Class complexity | scheme          8304882.145  bits     45.2824 bits/instance
Complexity improvement     (Sf)    -7780913.1046 bits    -42.4255 bits/instance
Mean absolute error                      0.0244
Root mean squared error                  0.1145
Relative absolute error                 18.4482 %
Root relative squared error             44.4976 %
Coverage of cases (0.95 level)          95.6118 %
Mean rel. region size (0.95 level)      13.4243 %
Total Number of Instances           183402     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,941    0,011    0,940      0,941    0,941      0,930    0,980     0,963     A
                 0,730    0,012    0,761      0,730    0,745      0,733    0,921     0,777     LGJKC
                 0,940    0,005    0,942      0,940    0,941      0,936    0,982     0,960     E
                 0,739    0,003    0,781      0,739    0,759      0,756    0,919     0,768     FV
                 0,922    0,002    0,939      0,922    0,931      0,928    0,976     0,945     I
                 0,878    0,006    0,916      0,878    0,897      0,889    0,956     0,909     O
                 0,750    0,030    0,720      0,750    0,735      0,707    0,944     0,787     SNRTY
                 0,938    0,054    0,918      0,938    0,928      0,881    0,981     0,964     0
                 0,793    0,004    0,863      0,793    0,826      0,822    0,939     0,838     BMP
                 0,931    0,001    0,955      0,931    0,943      0,942    0,985     0,959     DZ
                 0,956    0,000    0,981      0,956    0,969      0,968    0,992     0,980     U
                 0,874    0,003    0,927      0,874    0,900      0,896    0,962     0,910     SNRTXY
Weighted Avg.    0,896    0,028    0,897      0,896    0,896      0,871    0,970     0,925     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 26865   181    56    31    33    60   446   746    77    11     7    44 |     a = A
   183  6446    82    61    36    74   893   912    70     5    12    53 |     b = LGJKC
    74    97 12910    21    20    28   226   265    49    16     4    30 |     c = E
    47    76    23  1892    12    32   339    97    20     4     0    19 |     d = FV
    53    48    38    11  6534    18   135   209    24     0     5     8 |     e = I
    96    81    33    28    21 11624   255   998    41    21     4    32 |     f = O
   429   674   179   228    87   214 12954  2120   179    25    12   172 |     g = SNRTY
   592   620   235   100   153   512  1909 67630   221    20    14   109 |     h = 0
   109   125    66    25    37    48   341   419  4608     4    12    20 |     i = BMP
    31     7    20     6     2    30    44    25     7  2497     1    12 |     j = DZ
    19    26    13     0    12    18    25    37    22     1  3957     7 |     k = U
    74    95    54    20    12    30   413   193    21    12     5  6450 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
94.08	0.63	0.2	0.11	0.12	0.21	1.56	2.61	0.27	0.04	0.02	0.15	 a = A
2.07	73.03	0.93	0.69	0.41	0.84	10.12	10.33	0.79	0.06	0.14	0.6	 b = LGJKC
0.54	0.71	93.96	0.15	0.15	0.2	1.64	1.93	0.36	0.12	0.03	0.22	 c = E
1.84	2.97	0.9	73.88	0.47	1.25	13.24	3.79	0.78	0.16	0.0	0.74	 d = FV
0.75	0.68	0.54	0.16	92.25	0.25	1.91	2.95	0.34	0.0	0.07	0.11	 e = I
0.73	0.61	0.25	0.21	0.16	87.83	1.93	7.54	0.31	0.16	0.03	0.24	 f = O
2.48	3.9	1.04	1.32	0.5	1.24	75.0	12.27	1.04	0.14	0.07	1.0	 g = SNRTY
0.82	0.86	0.33	0.14	0.21	0.71	2.65	93.78	0.31	0.03	0.02	0.15	 h = 0
1.87	2.15	1.14	0.43	0.64	0.83	5.87	7.21	79.26	0.07	0.21	0.34	 i = BMP
1.16	0.26	0.75	0.22	0.07	1.12	1.64	0.93	0.26	93.1	0.04	0.45	 j = DZ
0.46	0.63	0.31	0.0	0.29	0.44	0.6	0.89	0.53	0.02	95.65	0.17	 k = U
1.0	1.29	0.73	0.27	0.16	0.41	5.6	2.62	0.28	0.16	0.07	87.41	 l = SNRTXY
