#!/usr/bin/python
# -*- coding: utf8 -*-
import os,sys
from Inventarios import inventarios

inv_op = sys.argv[1]
msop = sys.argv[2]
ngramas = int(sys.argv[3])
#~ msop = 3
#~ inv_op='I2'
#~ ngramas=1

sftfiles = []
for file in os.listdir("{}/{}".format(os.getcwd(),'Sft{}ms'.format(msop))):
		if '.sft' in file:
			sftfiles.append("{}/{}/{}".format(os.getcwd(),'Sft{}ms'.format(msop),file))
inventario = inventarios['I{}'.format(inv_op)]
data=[]
for filename in sftfiles:
	file = open (filename,'r')
	for line in file:
		if line.startswith('\\') or line.startswith('Time'):
			continue
		lineitems = line.split('\t')
		for i in range(0,len(lineitems)):
			if lineitems [i] == '':
				lineitems [i] = '0'
		pos = lineitems[1]
		if ' ' in pos:
			poss = pos.split(' ')
			pos = poss [1]
		del lineitems[1]
		lineitems.insert(len(lineitems)-1,inventario[pos])
		lineitems[len(lineitems)-1] = '\n'
		data.append(lineitems)
	file.close()
datasec = []
for i in (range(len(data)-(ngramas-1))):
#~ for i in range(120):
	sec = ''
	for j in range (ngramas-1):
		sec1 = data [i+j][:]
		del sec1[-1]
		del sec1[-1]
		del sec1[0]
		sec = '{},{}'.format(sec,','.join(sec1))
	sec3 = data [i+(ngramas-1)][:]
	pos = sec3[-2]
	del sec3[-1]
	del sec3[-1]
	del sec3[0]
	sec = sec [1:]
	sec = "{},{},{}\n".format(sec, ','.join(sec3),pos)
	datasec.append(sec)
#~ print datasec[:1]

arff = open ('I{}-{}ms-{}gramas.arff'.format(inv_op,msop,ngramas),'w')

arff.write("""@relation I{}-{}ms-{}gramas
""".format(inv_op,msop,ngramas))
for i in range (ngramas):
	arff.write("""@attribute {0}Int(dB) numeric
@attribute {0}Pitch(Hz) numeric
@attribute {0}RawPitch numeric
@attribute {0}SmPitch numeric
@attribute {0}Melogram(st) numeric
@attribute {0}ZCross numeric
@attribute {0}F1(Hz) numeric
@attribute {0}F2(Hz) numeric
@attribute {0}F3(Hz) numeric
@attribute {0}F4(Hz) numeric
""".format(i))
arff.write("""@attribute class {}{}{}
@data
""".format('{',','.join(list(set(inventario.values()))),'}'))
for item in datasec:
	arff.write(item)
arff.close()
