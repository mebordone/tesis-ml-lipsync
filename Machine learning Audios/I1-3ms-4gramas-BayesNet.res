Timestamp:2013-08-30-04:14:25
Inventario:I1
Intervalo:3ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I1-3ms-4gramas
Num Instances:  61132
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  21 2Int(dB)                   Num   0%  10%  90%     0 /  0%     2 /  0%   776 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  31 3Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(20): class 
0Pitch(Hz)(32): class 
0RawPitch(20): class 
0SmPitch(25): class 
0Melogram(st)(35): class 
0ZCross(15): class 
0F1(Hz)(264): class 
0F2(Hz)(269): class 
0F3(Hz)(506): class 
0F4(Hz)(345): class 
1Int(dB)(21): class 
1Pitch(Hz)(33): class 
1RawPitch(20): class 
1SmPitch(26): class 
1Melogram(st)(34): class 
1ZCross(15): class 
1F1(Hz)(300): class 
1F2(Hz)(408): class 
1F3(Hz)(511): class 
1F4(Hz)(389): class 
2Int(dB)(21): class 
2Pitch(Hz)(28): class 
2RawPitch(19): class 
2SmPitch(27): class 
2Melogram(st)(31): class 
2ZCross(15): class 
2F1(Hz)(280): class 
2F2(Hz)(355): class 
2F3(Hz)(427): class 
2F4(Hz)(299): class 
3Int(dB)(21): class 
3Pitch(Hz)(29): class 
3RawPitch(20): class 
3SmPitch(28): class 
3Melogram(st)(37): class 
3ZCross(15): class 
3F1(Hz)(301): class 
3F2(Hz)(250): class 
3F3(Hz)(382): class 
3F4(Hz)(354): class 
class(13): 
LogScore Bayes: -5070200.801399613
LogScore BDeu: -5845932.850428323
LogScore MDL: -5697443.520777645
LogScore ENTROPY: -5254170.786042903
LogScore AIC: -5334613.786042903




=== Stratified cross-validation ===

Correctly Classified Instances       40007               65.4436 %
Incorrectly Classified Instances     21125               34.5564 %
Kappa statistic                          0.5647
K&B Relative Info Score            3429171.0869 %
K&B Information Score                99229.9169 bits      1.6232 bits/instance
Class complexity | order 0          176875.9209 bits      2.8933 bits/instance
Class complexity | scheme           758311.8934 bits     12.4045 bits/instance
Complexity improvement     (Sf)    -581435.9724 bits     -9.5112 bits/instance
Mean absolute error                      0.0537
Root mean squared error                  0.2188
Relative absolute error                 43.7344 %
Root relative squared error             88.3272 %
Coverage of cases (0.95 level)          70.8172 %
Mean rel. region size (0.95 level)       9.7251 %
Total Number of Instances            61132     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,650    0,037    0,763      0,650    0,702      0,655    0,923     0,783     a
                 0,251    0,009    0,469      0,251    0,327      0,327    0,843     0,320     b
                 0,649    0,056    0,487      0,649    0,557      0,521    0,914     0,594     e
                 0,570    0,040    0,176      0,570    0,269      0,300    0,923     0,327     d
                 0,275    0,014    0,226      0,275    0,248      0,238    0,858     0,212     f
                 0,705    0,029    0,497      0,705    0,583      0,573    0,936     0,617     i
                 0,232    0,014    0,249      0,232    0,240      0,226    0,892     0,198     k
                 0,398    0,008    0,372      0,398    0,385      0,378    0,892     0,341     j
                 0,270    0,031    0,478      0,270    0,345      0,311    0,833     0,388     l
                 0,477    0,022    0,628      0,477    0,543      0,517    0,879     0,576     o
                 0,892    0,113    0,834      0,892    0,862      0,771    0,958     0,953     0
                 0,420    0,018    0,588      0,420    0,490      0,471    0,923     0,482     s
                 0,682    0,017    0,482      0,682    0,565      0,562    0,955     0,668     u
Weighted Avg.    0,654    0,062    0,666      0,654    0,650      0,598    0,921     0,709     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  6232    75   680   508   129   183   130    66   486   410   417   133   136 |     a = a
    98   488   180   166    75   189    46    24   108    60   316    52   146 |     b = b
   273    77  3000   202    74   254    85    81   223    71   159    36    86 |     c = e
    74    18    79   515    22    35     3     1    50    35    27    11    34 |     d = d
    43    26   105    79   238    47    27     7    26    33   178    41    14 |     e = f
    21    34   256    31    29  1679    56    22    72     5   104    50    23 |     f = i
    18    16    22     8    19    41   277    12    34    13   571   151    10 |     g = k
    66     4    97    11    15    53    10   276    16    15    72    53     5 |     h = j
   422   107   864   588   114   402   166    73  1571   226   813   191   282 |     i = l
   529    43   314   405    44    66    64    54   133  2131   449    38   193 |     j = o
   244    93   384   238   208   273   125    85   311   254 21163   282    63 |     k = 0
    98    38   110   114    66   147    94    38   199    37  1095  1492    24 |     l = s
    47    22    66    56    21     8    30     2    60   101    20     8   945 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
65.02	0.78	7.09	5.3	1.35	1.91	1.36	0.69	5.07	4.28	4.35	1.39	1.42	 a = a
5.03	25.05	9.24	8.52	3.85	9.7	2.36	1.23	5.54	3.08	16.22	2.67	7.49	 b = b
5.91	1.67	64.92	4.37	1.6	5.5	1.84	1.75	4.83	1.54	3.44	0.78	1.86	 c = e
8.19	1.99	8.74	56.97	2.43	3.87	0.33	0.11	5.53	3.87	2.99	1.22	3.76	 d = d
4.98	3.01	12.15	9.14	27.55	5.44	3.13	0.81	3.01	3.82	20.6	4.75	1.62	 e = f
0.88	1.43	10.75	1.3	1.22	70.49	2.35	0.92	3.02	0.21	4.37	2.1	0.97	 f = i
1.51	1.34	1.85	0.67	1.59	3.44	23.24	1.01	2.85	1.09	47.9	12.67	0.84	 g = k
9.52	0.58	14.0	1.59	2.16	7.65	1.44	39.83	2.31	2.16	10.39	7.65	0.72	 h = j
7.25	1.84	14.85	10.1	1.96	6.91	2.85	1.25	27.0	3.88	13.97	3.28	4.85	 i = l
11.85	0.96	7.04	9.07	0.99	1.48	1.43	1.21	2.98	47.75	10.06	0.85	4.32	 j = o
1.03	0.39	1.62	1.0	0.88	1.15	0.53	0.36	1.31	1.07	89.21	1.19	0.27	 k = 0
2.76	1.07	3.1	3.21	1.86	4.14	2.65	1.07	5.6	1.04	30.83	42.0	0.68	 l = s
3.39	1.59	4.76	4.04	1.52	0.58	2.16	0.14	4.33	7.29	1.44	0.58	68.18	 m = u
