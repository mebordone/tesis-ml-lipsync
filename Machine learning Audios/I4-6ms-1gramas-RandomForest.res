Timestamp:2013-07-23-03:04:24
Inventario:I4
Intervalo:6ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I4-6ms-1gramas
Num Instances:  30453
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.2216





=== Stratified cross-validation ===

Correctly Classified Instances       24898               81.7588 %
Incorrectly Classified Instances      5555               18.2412 %
Kappa statistic                          0.7625
K&B Relative Info Score            2183203.2646 %
K&B Information Score                56052.4629 bits      1.8406 bits/instance
Class complexity | order 0           78170.5996 bits      2.5669 bits/instance
Class complexity | scheme          1447582.4284 bits     47.535  bits/instance
Complexity improvement     (Sf)    -1369411.8289 bits    -44.968  bits/instance
Mean absolute error                      0.0565
Root mean squared error                  0.1652
Relative absolute error                 36.417  %
Root relative squared error             59.2911 %
Coverage of cases (0.95 level)          95.2386 %
Mean rel. region size (0.95 level)      24.5536 %
Total Number of Instances            30453     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,686    0,001    0,770      0,686    0,726      0,725    0,949     0,737     C
                 0,814    0,022    0,757      0,814    0,785      0,767    0,958     0,829     E
                 0,423    0,003    0,655      0,423    0,514      0,521    0,877     0,491     FV
                 0,856    0,040    0,841      0,856    0,849      0,811    0,959     0,906     AI
                 0,733    0,067    0,671      0,733    0,701      0,643    0,933     0,769     CDGKNRSYZ
                 0,746    0,012    0,830      0,746    0,786      0,771    0,941     0,817     O
                 0,926    0,072    0,888      0,926    0,906      0,847    0,973     0,962     0
                 0,411    0,007    0,676      0,411    0,511      0,514    0,859     0,476     LT
                 0,818    0,002    0,898      0,818    0,856      0,854    0,967     0,880     U
                 0,546    0,004    0,806      0,546    0,651      0,654    0,906     0,645     MBP
Weighted Avg.    0,818    0,049    0,816      0,818    0,814      0,772    0,953     0,862     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j   <-- classified as
   127    14     0    18     9     2    10     1     4     0 |     a = C
     7  1911    11    99   189    29    62    21     6    12 |     b = E
     1    28   182    35   128    13    28     5     4     6 |     c = FV
    10   167     9  5165   321    61   217    49     6    26 |     d = AI
     3   175    32   332  3521    93   532    77    10    27 |     e = CDGKNRSYZ
     2    42    10   110   158  1680   203    17    11    18 |     f = O
     6    80    16   170   478    52 10747    32     6    24 |     g = 0
     3    53     7   102   258    33   185   460     9     9 |     h = LT
     3    13     1    20    32    32    12     7   570     7 |     i = U
     3    41    10    87   150    29   105    11     9   535 |     j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
68.65	7.57	0.0	9.73	4.86	1.08	5.41	0.54	2.16	0.0	 a = C
0.3	81.42	0.47	4.22	8.05	1.24	2.64	0.89	0.26	0.51	 b = E
0.23	6.51	42.33	8.14	29.77	3.02	6.51	1.16	0.93	1.4	 c = FV
0.17	2.77	0.15	85.64	5.32	1.01	3.6	0.81	0.1	0.43	 d = AI
0.06	3.64	0.67	6.91	73.32	1.94	11.08	1.6	0.21	0.56	 e = CDGKNRSYZ
0.09	1.87	0.44	4.89	7.02	74.63	9.02	0.76	0.49	0.8	 f = O
0.05	0.69	0.14	1.46	4.12	0.45	92.56	0.28	0.05	0.21	 g = 0
0.27	4.74	0.63	9.12	23.06	2.95	16.53	41.11	0.8	0.8	 h = LT
0.43	1.87	0.14	2.87	4.59	4.59	1.72	1.0	81.78	1.0	 i = U
0.31	4.18	1.02	8.88	15.31	2.96	10.71	1.12	0.92	54.59	 j = MBP
