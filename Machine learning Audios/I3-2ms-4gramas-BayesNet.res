Timestamp:2013-08-30-04:57:36
Inventario:I3
Intervalo:2ms
Ngramas:4
Clasificador:BayesNet
Relation Name:  I3-2ms-4gramas
Num Instances:  91700
Num Attributes: 41

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=41 #classindex=40
Network structure (nodes followed by parents)
0Int(dB)(23): class 
0Pitch(Hz)(47): class 
0RawPitch(33): class 
0SmPitch(38): class 
0Melogram(st)(41): class 
0ZCross(17): class 
0F1(Hz)(1340): class 
0F2(Hz)(1805): class 
0F3(Hz)(2117): class 
0F4(Hz)(2165): class 
1Int(dB)(24): class 
1Pitch(Hz)(48): class 
1RawPitch(35): class 
1SmPitch(40): class 
1Melogram(st)(41): class 
1ZCross(17): class 
1F1(Hz)(1307): class 
1F2(Hz)(1893): class 
1F3(Hz)(2235): class 
1F4(Hz)(2060): class 
2Int(dB)(22): class 
2Pitch(Hz)(46): class 
2RawPitch(34): class 
2SmPitch(35): class 
2Melogram(st)(45): class 
2ZCross(17): class 
2F1(Hz)(1379): class 
2F2(Hz)(1939): class 
2F3(Hz)(2284): class 
2F4(Hz)(2125): class 
3Int(dB)(25): class 
3Pitch(Hz)(48): class 
3RawPitch(32): class 
3SmPitch(39): class 
3Melogram(st)(45): class 
3ZCross(17): class 
3F1(Hz)(1291): class 
3F2(Hz)(1920): class 
3F3(Hz)(2248): class 
3F4(Hz)(2101): class 
class(11): 
LogScore Bayes: -9962398.64132768
LogScore BDeu: -1.4111373202626195E7
LogScore MDL: -1.3061005554069413E7
LogScore ENTROPY: -1.111415066154707E7
LogScore AIC: -1.145491866154707E7




=== Stratified cross-validation ===

Correctly Classified Instances       69114               75.3697 %
Incorrectly Classified Instances     22586               24.6303 %
Kappa statistic                          0.6813
K&B Relative Info Score            6550833.3323 %
K&B Information Score               169974.7698 bits      1.8536 bits/instance
Class complexity | order 0          237912.3107 bits      2.5945 bits/instance
Class complexity | scheme           952061.4399 bits     10.3823 bits/instance
Complexity improvement     (Sf)    -714149.1292 bits     -7.7879 bits/instance
Mean absolute error                      0.0449
Root mean squared error                  0.2058
Relative absolute error                 31.884  %
Root relative squared error             77.5229 %
Coverage of cases (0.95 level)          77.7317 %
Mean rel. region size (0.95 level)       9.941  %
Total Number of Instances            91700     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,813    0,022    0,874      0,813    0,842      0,815    0,957     0,901     A
                 0,845    0,031    0,693      0,845    0,761      0,744    0,959     0,844     E
                 0,326    0,019    0,793      0,326    0,462      0,450    0,936     0,737     CGJLNQSRT
                 0,621    0,009    0,508      0,621    0,559      0,555    0,907     0,600     FV
                 0,852    0,017    0,669      0,852    0,749      0,744    0,958     0,840     I
                 0,758    0,013    0,821      0,758    0,789      0,773    0,924     0,822     O
                 0,900    0,156    0,787      0,900    0,840      0,730    0,964     0,946     0
                 0,669    0,008    0,736      0,669    0,701      0,692    0,901     0,711     BMP
                 0,911    0,010    0,669      0,911    0,771      0,775    0,977     0,907     U
                 0,000    0,005    0,000      0,000    0,000      -0,001   0,855     0,001     DZ
                 0,903    0,030    0,307      0,903    0,459      0,517    0,981     0,777     D
Weighted Avg.    0,754    0,073    0,777      0,754    0,742      0,691    0,952     0,864     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k   <-- classified as
 11654   362   353   165   157   222   795    85    92    82   366 |     a = A
   100  5842   228    61   101    22   283    54    34    37   148 |     b = E
   856  1338  5480   113   744   371  5774   272   565    32  1248 |     c = CGJLNQSRT
    20    33    30   799    26    14   318    10     7     0    29 |     d = FV
    22    78   121    23  3033     5   194    20     3    39    23 |     e = I
   169   145   108    17    33  5039   724    54    42    42   273 |     f = O
   437   503   434   363   375   398 32237   186   141   235   521 |     g = 0
    48    93    79     9    57    38   546  1950    52     1    42 |     h = BMP
     3    17    65    12     1     5    44    17  1895     2    19 |     i = U
     0     0     0     0     0     0    33     0     0     0     0 |     j = DZ
    24    24    12    11     6    20    15     2     3    11  1185 |     k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
81.31	2.53	2.46	1.15	1.1	1.55	5.55	0.59	0.64	0.57	2.55	 a = A
1.45	84.54	3.3	0.88	1.46	0.32	4.1	0.78	0.49	0.54	2.14	 b = E
5.1	7.97	32.63	0.67	4.43	2.21	34.38	1.62	3.36	0.19	7.43	 c = CGJLNQSRT
1.56	2.57	2.33	62.13	2.02	1.09	24.73	0.78	0.54	0.0	2.26	 d = FV
0.62	2.19	3.4	0.65	85.17	0.14	5.45	0.56	0.08	1.1	0.65	 e = I
2.54	2.18	1.63	0.26	0.5	75.82	10.89	0.81	0.63	0.63	4.11	 f = O
1.22	1.4	1.21	1.01	1.05	1.11	89.97	0.52	0.39	0.66	1.45	 g = 0
1.65	3.19	2.71	0.31	1.96	1.3	18.73	66.9	1.78	0.03	1.44	 h = BMP
0.14	0.82	3.13	0.58	0.05	0.24	2.12	0.82	91.11	0.1	0.91	 i = U
0.0	0.0	0.0	0.0	0.0	0.0	100.0	0.0	0.0	0.0	0.0	 j = DZ
1.83	1.83	0.91	0.84	0.46	1.52	1.14	0.15	0.23	0.84	90.25	 k = D
