Timestamp:2013-07-22-21:49:29
Inventario:I2
Intervalo:3ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I2-3ms-1gramas
Num Instances:  61135
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   559 /  1%  3828 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   613 /  1%  3808 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   575 /  1%  3802 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   269 /  0%  3285 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     3 /  0%    50 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1075 /  2%  4783 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2270 /  4%  6724 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1945 /  3%  6241 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1922 /  3%  6122 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.1659





=== Stratified cross-validation ===

Correctly Classified Instances       52758               86.2975 %
Incorrectly Classified Instances      8377               13.7025 %
Kappa statistic                          0.8259
K&B Relative Info Score            4869801.1443 %
K&B Information Score               139770.4032 bits      2.2863 bits/instance
Class complexity | order 0          175447.8898 bits      2.8698 bits/instance
Class complexity | scheme          1712093.0202 bits     28.0051 bits/instance
Complexity improvement     (Sf)    -1536645.1304 bits    -25.1353 bits/instance
Mean absolute error                      0.037 
Root mean squared error                  0.1314
Relative absolute error                 27.8755 %
Root relative squared error             50.9723 %
Coverage of cases (0.95 level)          96.6828 %
Mean rel. region size (0.95 level)      19.2713 %
Total Number of Instances            61135     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,912    0,016    0,913      0,912    0,912      0,896    0,980     0,950     A
                 0,618    0,009    0,785      0,618    0,691      0,683    0,930     0,705     LGJKC
                 0,883    0,010    0,879      0,883    0,881      0,871    0,978     0,923     E
                 0,609    0,001    0,855      0,609    0,711      0,718    0,946     0,709     FV
                 0,866    0,004    0,889      0,866    0,877      0,873    0,971     0,902     I
                 0,824    0,007    0,907      0,824    0,863      0,854    0,967     0,889     O
                 0,695    0,040    0,644      0,695    0,669      0,633    0,952     0,749     SNRTY
                 0,948    0,079    0,883      0,948    0,915      0,859    0,983     0,974     0
                 0,690    0,003    0,888      0,690    0,776      0,777    0,944     0,781     BMP
                 0,799    0,001    0,891      0,799    0,842      0,842    0,973     0,869     DZ
                 0,895    0,001    0,948      0,895    0,921      0,919    0,985     0,949     U
                 0,803    0,004    0,897      0,803    0,848      0,843    0,960     0,862     SNRTXY
Weighted Avg.    0,863    0,039    0,864      0,863    0,861      0,832    0,972     0,907     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
  8737    54    57     4    21    46   242   364    20    15     1    24 |     a = A
   112  1830    62     5    19    35   428   430    15     2     8    17 |     b = LGJKC
    63    50  4081    13    30    20   130   153    20    12    12    37 |     c = E
    27     8    31   526     8    16   191    50     1     0     1     5 |     d = FV
    27    29    46     7  2063     7    68   111    17     0     3     4 |     e = I
    90    26    40     9    13  3678   114   420    19    18     9    27 |     f = O
   224   172    87    19    46    68  4037  1047    29     7    12    60 |     g = SNRTY
   150    83   109    17    67    71   639 22504    30    20     6    30 |     h = 0
    49    29    38     9    39    31   175   224  1344     0     8     2 |     i = BMP
    29     3    24     1     2    41    30    29     1   722     3    19 |     j = DZ
    10    17    15     2     5    27    23    26    14     4  1240     3 |     k = U
    52    29    55     3     7    16   191   117     4    10     5  1996 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
91.15	0.56	0.59	0.04	0.22	0.48	2.52	3.8	0.21	0.16	0.01	0.25	 a = A
3.78	61.76	2.09	0.17	0.64	1.18	14.44	14.51	0.51	0.07	0.27	0.57	 b = LGJKC
1.36	1.08	88.31	0.28	0.65	0.43	2.81	3.31	0.43	0.26	0.26	0.8	 c = E
3.13	0.93	3.59	60.88	0.93	1.85	22.11	5.79	0.12	0.0	0.12	0.58	 d = FV
1.13	1.22	1.93	0.29	86.61	0.29	2.85	4.66	0.71	0.0	0.13	0.17	 e = I
2.02	0.58	0.9	0.2	0.29	82.41	2.55	9.41	0.43	0.4	0.2	0.6	 f = O
3.86	2.96	1.5	0.33	0.79	1.17	69.51	18.03	0.5	0.12	0.21	1.03	 g = SNRTY
0.63	0.35	0.46	0.07	0.28	0.3	2.69	94.85	0.13	0.08	0.03	0.13	 h = 0
2.52	1.49	1.95	0.46	2.0	1.59	8.98	11.5	68.99	0.0	0.41	0.1	 i = BMP
3.21	0.33	2.65	0.11	0.22	4.54	3.32	3.21	0.11	79.87	0.33	2.1	 j = DZ
0.72	1.23	1.08	0.14	0.36	1.95	1.66	1.88	1.01	0.29	89.47	0.22	 k = U
2.09	1.17	2.21	0.12	0.28	0.64	7.69	4.71	0.16	0.4	0.2	80.32	 l = SNRTXY
