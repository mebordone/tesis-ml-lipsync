Timestamp:2013-08-30-04:11:15
Inventario:I1
Intervalo:2ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I1-2ms-5gramas
Num Instances:  91699
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(24): class 
0Pitch(Hz)(45): class 
0RawPitch(27): class 
0SmPitch(36): class 
0Melogram(st)(43): class 
0ZCross(17): class 
0F1(Hz)(1635): class 
0F2(Hz)(2240): class 
0F3(Hz)(2307): class 
0F4(Hz)(2382): class 
1Int(dB)(25): class 
1Pitch(Hz)(41): class 
1RawPitch(30): class 
1SmPitch(35): class 
1Melogram(st)(47): class 
1ZCross(17): class 
1F1(Hz)(1587): class 
1F2(Hz)(2331): class 
1F3(Hz)(2217): class 
1F4(Hz)(2488): class 
2Int(dB)(24): class 
2Pitch(Hz)(43): class 
2RawPitch(29): class 
2SmPitch(37): class 
2Melogram(st)(46): class 
2ZCross(17): class 
2F1(Hz)(1647): class 
2F2(Hz)(2312): class 
2F3(Hz)(2304): class 
2F4(Hz)(2524): class 
3Int(dB)(22): class 
3Pitch(Hz)(46): class 
3RawPitch(29): class 
3SmPitch(36): class 
3Melogram(st)(45): class 
3ZCross(16): class 
3F1(Hz)(1612): class 
3F2(Hz)(2243): class 
3F3(Hz)(2379): class 
3F4(Hz)(2445): class 
4Int(dB)(25): class 
4Pitch(Hz)(45): class 
4RawPitch(31): class 
4SmPitch(39): class 
4Melogram(st)(44): class 
4ZCross(17): class 
4F1(Hz)(1439): class 
4F2(Hz)(2328): class 
4F3(Hz)(2303): class 
4F4(Hz)(2522): class 
class(13): 
LogScore Bayes: -1.2466569044163613E7
LogScore BDeu: -1.967734973760472E7
LogScore MDL: -1.7763044868515156E7
LogScore ENTROPY: -1.4482215182575753E7
LogScore AIC: -1.5056476182575753E7




=== Stratified cross-validation ===

Correctly Classified Instances       73659               80.3269 %
Incorrectly Classified Instances     18040               19.6731 %
Kappa statistic                          0.7502
K&B Relative Info Score            6901991.1759 %
K&B Information Score               199246.1123 bits      2.1728 bits/instance
Class complexity | order 0          264693.4431 bits      2.8865 bits/instance
Class complexity | scheme          1095073.1735 bits     11.942  bits/instance
Complexity improvement     (Sf)    -830379.7304 bits     -9.0555 bits/instance
Mean absolute error                      0.0304
Root mean squared error                  0.1692
Relative absolute error                 24.8406 %
Root relative squared error             68.3885 %
Coverage of cases (0.95 level)          82.0227 %
Mean rel. region size (0.95 level)       8.2675 %
Total Number of Instances            91699     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,818    0,013    0,923      0,818    0,867      0,847    0,961     0,920     a
                 0,690    0,006    0,777      0,690    0,731      0,724    0,906     0,736     b
                 0,847    0,017    0,805      0,847    0,826      0,811    0,963     0,880     e
                 0,897    0,017    0,438      0,897    0,589      0,620    0,974     0,830     d
                 0,641    0,007    0,551      0,641    0,592      0,588    0,912     0,643     f
                 0,860    0,010    0,775      0,860    0,815      0,809    0,959     0,860     i
                 0,232    0,009    0,327      0,232    0,271      0,263    0,924     0,298     k
                 0,741    0,006    0,571      0,741    0,645      0,646    0,921     0,723     j
                 0,648    0,014    0,831      0,648    0,728      0,710    0,912     0,768     l
                 0,783    0,009    0,869      0,783    0,824      0,812    0,928     0,847     o
                 0,899    0,121    0,826      0,899    0,861      0,767    0,968     0,959     0
                 0,542    0,015    0,682      0,542    0,604      0,587    0,962     0,684     s
                 0,909    0,005    0,796      0,909    0,849      0,847    0,978     0,922     u
Weighted Avg.    0,803    0,055    0,809      0,803    0,801      0,757    0,954     0,871     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
 11725   113   235   332   143   113   167    87   298   164   677   205    74 |     a = a
    30  2012    60    22    14    50    42    10    24    37   506    66    42 |     b = b
    60    70  5854   126    44    78    89    57   141    27   271    70    23 |     c = e
    19     1    24  1207     6     0     8     6     4    15    42     9     5 |     d = d
    20    11    26    15   824    17    33     2     5    10   285    35     3 |     e = f
     7    25    58    18    15  3062    71    19    49     7   158    69     3 |     f = i
    11     5    32    11     6    17   413     6    28    22   921   286    22 |     g = k
     7     0    38    10     0     9    17   767     4     0   123    55     5 |     h = j
   272    66   345   216    19   144   131    54  5628   113  1315   263   114 |     i = l
   114    50    87   203    26    23    79    40    68  5206   686    38    26 |     j = o
   357   187   451   443   334   331   157   246   433   347 32201   221   121 |     k = 0
    80    32    58   141    56   101    28    43    51    36  1755  2869    48 |     l = s
     3    17     3    11     9     4    29     7    41     6    37    22  1891 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
81.8	0.79	1.64	2.32	1.0	0.79	1.17	0.61	2.08	1.14	4.72	1.43	0.52	 a = a
1.03	69.02	2.06	0.75	0.48	1.72	1.44	0.34	0.82	1.27	17.36	2.26	1.44	 b = b
0.87	1.01	84.72	1.82	0.64	1.13	1.29	0.82	2.04	0.39	3.92	1.01	0.33	 c = e
1.41	0.07	1.78	89.67	0.45	0.0	0.59	0.45	0.3	1.11	3.12	0.67	0.37	 d = d
1.56	0.86	2.02	1.17	64.07	1.32	2.57	0.16	0.39	0.78	22.16	2.72	0.23	 e = f
0.2	0.7	1.63	0.51	0.42	85.99	1.99	0.53	1.38	0.2	4.44	1.94	0.08	 f = i
0.62	0.28	1.8	0.62	0.34	0.96	23.2	0.34	1.57	1.24	51.74	16.07	1.24	 g = k
0.68	0.0	3.67	0.97	0.0	0.87	1.64	74.11	0.39	0.0	11.88	5.31	0.48	 h = j
3.13	0.76	3.97	2.49	0.22	1.66	1.51	0.62	64.84	1.3	15.15	3.03	1.31	 i = l
1.72	0.75	1.31	3.05	0.39	0.35	1.19	0.6	1.02	78.33	10.32	0.57	0.39	 j = o
1.0	0.52	1.26	1.24	0.93	0.92	0.44	0.69	1.21	0.97	89.87	0.62	0.34	 k = 0
1.51	0.6	1.09	2.66	1.06	1.91	0.53	0.81	0.96	0.68	33.13	54.15	0.91	 l = s
0.14	0.82	0.14	0.53	0.43	0.19	1.39	0.34	1.97	0.29	1.78	1.06	90.91	 m = u
