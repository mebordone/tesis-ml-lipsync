#!/usr/bin/python
# -*- coding: utf8 -*-
import sys, os
import re
from pprint import pprint


expfile = open (sys.argv[1])
textmatrix=expfile.read().split('=== Confusion Matrix ===')[1].split('\n')
matrix = []
lista = re.sub("\s+"," ",textmatrix[2]).split('<--')[0].split(' ')[1:]
matrix.append(lista)
for i in range (3,len(textmatrix)-2):
	filanum = re.sub("\s+"," ",textmatrix[i]).split('|')[0].split(' ')
	filaclase = re.sub("\s+"," ",textmatrix[i]).split('|')[1]
	filanum = filanum [1:-1]
	suma=0
	for i in filanum:
		suma=suma+int(i)
	for i in range(len(filanum)):
		filanum [i] = round(float (filanum[i]) *100 / suma,2)
	filanum.append(filaclase)
	matrix.append(filanum)
expfile.close()
print "=== Percent Confusion Matrix ==="
for fila in matrix:
	print "\t".join([str(i) for i in fila])
