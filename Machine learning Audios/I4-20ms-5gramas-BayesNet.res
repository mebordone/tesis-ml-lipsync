Timestamp:2013-08-30-05:29:59
Inventario:I4
Intervalo:20ms
Ngramas:5
Clasificador:BayesNet
Relation Name:  I4-20ms-5gramas
Num Instances:  9125
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 1Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  12 1Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  13 1RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  14 1SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  15 1Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  17 1F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  18 1F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  19 1F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  20 1F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  21 2Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  22 2Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  23 2RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  24 2SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  25 2Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  27 2F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  28 2F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  29 2F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  30 2F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  31 3Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  32 3Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  33 3RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  34 3SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  35 3Melogram(st)              Num   0%  45%  55%     0 /  0%   937 / 10%  2341 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  37 3F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  38 3F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  39 3F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  40 3F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  41 4Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
  42 4Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
  43 4RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
  44 4SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
  45 4Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
  47 4F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
  48 4F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
  49 4F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  50 4F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    10 


Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=51 #classindex=50
Network structure (nodes followed by parents)
0Int(dB)(8): class 
0Pitch(Hz)(4): class 
0RawPitch(4): class 
0SmPitch(4): class 
0Melogram(st)(5): class 
0ZCross(4): class 
0F1(Hz)(6): class 
0F2(Hz)(6): class 
0F3(Hz)(2): class 
0F4(Hz)(4): class 
1Int(dB)(8): class 
1Pitch(Hz)(4): class 
1RawPitch(4): class 
1SmPitch(4): class 
1Melogram(st)(5): class 
1ZCross(4): class 
1F1(Hz)(8): class 
1F2(Hz)(6): class 
1F3(Hz)(3): class 
1F4(Hz)(4): class 
2Int(dB)(8): class 
2Pitch(Hz)(5): class 
2RawPitch(4): class 
2SmPitch(4): class 
2Melogram(st)(5): class 
2ZCross(3): class 
2F1(Hz)(8): class 
2F2(Hz)(6): class 
2F3(Hz)(2): class 
2F4(Hz)(6): class 
3Int(dB)(9): class 
3Pitch(Hz)(4): class 
3RawPitch(4): class 
3SmPitch(4): class 
3Melogram(st)(7): class 
3ZCross(4): class 
3F1(Hz)(8): class 
3F2(Hz)(6): class 
3F3(Hz)(2): class 
3F4(Hz)(4): class 
4Int(dB)(9): class 
4Pitch(Hz)(5): class 
4RawPitch(4): class 
4SmPitch(4): class 
4Melogram(st)(7): class 
4ZCross(6): class 
4F1(Hz)(9): class 
4F2(Hz)(9): class 
4F3(Hz)(6): class 
4F4(Hz)(4): class 
class(10): 
LogScore Bayes: -437535.4589325439
LogScore BDeu: -445789.0443902508
LogScore MDL: -445262.0307349787
LogScore ENTROPY: -435509.5028206256
LogScore AIC: -437648.5028206256




=== Stratified cross-validation ===

Correctly Classified Instances        4635               50.7945 %
Incorrectly Classified Instances      4490               49.2055 %
Kappa statistic                          0.3954
K&B Relative Info Score             357212.025  %
K&B Information Score                 9350.3711 bits      1.0247 bits/instance
Class complexity | order 0           23867.9778 bits      2.6157 bits/instance
Class complexity | scheme           112028.6406 bits     12.2771 bits/instance
Complexity improvement     (Sf)     -88160.6628 bits     -9.6614 bits/instance
Mean absolute error                      0.0991
Root mean squared error                  0.289 
Relative absolute error                 62.7865 %
Root relative squared error            102.9054 %
Coverage of cases (0.95 level)          60.8877 %
Mean rel. region size (0.95 level)      16.3321 %
Total Number of Instances             9125     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,500    0,106    0,030      0,500    0,057      0,102    0,843     0,030     C
                 0,430    0,081    0,321      0,430    0,367      0,306    0,832     0,347     E
                 0,210    0,052    0,058      0,210    0,091      0,085    0,767     0,040     FV
                 0,353    0,052    0,639      0,353    0,455      0,383    0,832     0,596     AI
                 0,202    0,046    0,464      0,202    0,282      0,225    0,764     0,383     CDGKNRSYZ
                 0,279    0,039    0,375      0,279    0,320      0,275    0,796     0,300     O
                 0,895    0,088    0,849      0,895    0,871      0,798    0,942     0,878     0
                 0,136    0,036    0,126      0,136    0,131      0,097    0,749     0,097     LT
                 0,431    0,026    0,291      0,431    0,348      0,335    0,892     0,409     U
                 0,181    0,029    0,174      0,181    0,177      0,149    0,744     0,116     MBP
Weighted Avg.    0,508    0,063    0,583      0,508    0,525      0,464    0,852     0,567     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j   <-- classified as
   30   16    2    3    2    0    0    1    3    3 |    a = C
  144  321   35   60   24   40   41   25   38   18 |    b = E
   26   22   29    9   22    6    9    7    2    6 |    c = FV
  285  294  101  668   89  148  105   73   55   72 |    d = AI
  255  153  160  149  302   57  199   93   37   87 |    e = CDGKNRSYZ
   90   78   35   86   31  199   82   18   67   26 |    f = O
   21   27   65   15  105   14 2892   74    7   13 |    g = 0
   52   42   38   17   48   17   48   46    6   23 |    h = LT
   27   13    2   18    3   31   13    8   94    9 |    i = U
   65   35   29   21   25   18   19   19   14   54 |    j = MBP

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	
50.0	26.67	3.33	5.0	3.33	0.0	0.0	1.67	5.0	5.0	 a = C
19.3	43.03	4.69	8.04	3.22	5.36	5.5	3.35	5.09	2.41	 b = E
18.84	15.94	21.01	6.52	15.94	4.35	6.52	5.07	1.45	4.35	 c = FV
15.08	15.56	5.34	35.34	4.71	7.83	5.56	3.86	2.91	3.81	 d = AI
17.09	10.25	10.72	9.99	20.24	3.82	13.34	6.23	2.48	5.83	 e = CDGKNRSYZ
12.64	10.96	4.92	12.08	4.35	27.95	11.52	2.53	9.41	3.65	 f = O
0.65	0.84	2.01	0.46	3.25	0.43	89.45	2.29	0.22	0.4	 g = 0
15.43	12.46	11.28	5.04	14.24	5.04	14.24	13.65	1.78	6.82	 h = LT
12.39	5.96	0.92	8.26	1.38	14.22	5.96	3.67	43.12	4.13	 i = U
21.74	11.71	9.7	7.02	8.36	6.02	6.35	6.35	4.68	18.06	 j = MBP
