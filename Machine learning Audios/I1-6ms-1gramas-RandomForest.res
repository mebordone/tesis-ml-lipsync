Timestamp:2013-07-23-02:55:26
Inventario:I1
Intervalo:6ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I1-6ms-1gramas
Num Instances:  30453
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%     3 /  0%   777 
   2 0Pitch(Hz)                 Num   0%  51%  49%     0 /  0%   750 /  2%  3423 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   699 /  2%  3337 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   748 /  2%  3382 
   5 0Melogram(st)              Num   0%  48%  52%     0 /  0%   413 /  1%  3048 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     8 /  0%    47 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1130 /  4%  4802 
   8 0F2(Hz)                    Num   0%  51%  49%     0 /  0%  2251 /  7%  6691 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1969 /  6%  6255 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1938 /  6%  6099 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    13 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.2333





=== Stratified cross-validation ===

Correctly Classified Instances       24563               80.6587 %
Incorrectly Classified Instances      5890               19.3413 %
Kappa statistic                          0.7557
K&B Relative Info Score            2193282.0647 %
K&B Information Score                63827.7468 bits      2.0959 bits/instance
Class complexity | order 0           88599.5666 bits      2.9094 bits/instance
Class complexity | scheme          1747986.4263 bits     57.3995 bits/instance
Complexity improvement     (Sf)    -1659386.8597 bits    -54.4901 bits/instance
Mean absolute error                      0.0453
Root mean squared error                  0.1487
Relative absolute error                 36.7573 %
Root relative squared error             59.8712 %
Coverage of cases (0.95 level)          94.2699 %
Mean rel. region size (0.95 level)      20.023  %
Total Number of Instances            30453     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,865    0,033    0,834      0,865    0,849      0,820    0,963     0,908     a
                 0,600    0,007    0,745      0,600    0,665      0,659    0,901     0,657     b
                 0,813    0,022    0,759      0,813    0,785      0,767    0,959     0,825     e
                 0,603    0,004    0,689      0,603    0,643      0,640    0,944     0,639     d
                 0,442    0,004    0,623      0,442    0,517      0,519    0,885     0,502     f
                 0,788    0,007    0,815      0,788    0,801      0,793    0,958     0,827     i
                 0,216    0,007    0,375      0,216    0,274      0,274    0,803     0,218     k
                 0,558    0,002    0,803      0,558    0,659      0,667    0,889     0,618     j
                 0,644    0,033    0,678      0,644    0,661      0,626    0,916     0,693     l
                 0,742    0,012    0,830      0,742    0,784      0,769    0,944     0,814     o
                 0,935    0,081    0,877      0,935    0,905      0,844    0,974     0,963     0
                 0,663    0,025    0,625      0,663    0,643      0,621    0,938     0,696     s
                 0,813    0,002    0,897      0,813    0,853      0,851    0,972     0,887     u
Weighted Avg.    0,807    0,044    0,800      0,807    0,802      0,766    0,952     0,844     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l     m   <-- classified as
  4174    13    89    13     9    21    26     5   156    53   191    70     5 |     a = a
    59   588    37     4     8    25    14     2    72    22   113    31     5 |     b = b
    89    24  1907    14    14    28     7     4   123    26    72    29    10 |     c = e
    35     8    31   273     3     3     1     1    38    37    14     7     2 |     d = d
    24     8    34     3   190     4     8     1    32    18    41    63     4 |     e = f
    22    25    74     3     3   950     4     3    39     4    60    18     1 |     f = i
    31     7    15     2    12     9   128     7    56    18   180   126     2 |     g = k
    35     1    28     1     4     6     6   196    13     4    28    26     3 |     h = j
   230    39   138    23    19    44    40     1  1885    56   304   138    10 |     i = l
    99    25    43    29     9    10    12     2   100  1670   214    27    11 |     j = o
   122    37    81    23    18    38    39    11   155    56 10854   168     9 |     k = 0
    78     7    21     5    14    21    50     8    83    14   297  1181     3 |     l = s
     9     7    15     3     2     6     6     3    28    33    13     5   567 |     m = u

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	m	
86.51	0.27	1.84	0.27	0.19	0.44	0.54	0.1	3.23	1.1	3.96	1.45	0.1	 a = a
6.02	60.0	3.78	0.41	0.82	2.55	1.43	0.2	7.35	2.24	11.53	3.16	0.51	 b = b
3.79	1.02	81.25	0.6	0.6	1.19	0.3	0.17	5.24	1.11	3.07	1.24	0.43	 c = e
7.73	1.77	6.84	60.26	0.66	0.66	0.22	0.22	8.39	8.17	3.09	1.55	0.44	 d = d
5.58	1.86	7.91	0.7	44.19	0.93	1.86	0.23	7.44	4.19	9.53	14.65	0.93	 e = f
1.82	2.07	6.14	0.25	0.25	78.77	0.33	0.25	3.23	0.33	4.98	1.49	0.08	 f = i
5.23	1.18	2.53	0.34	2.02	1.52	21.59	1.18	9.44	3.04	30.35	21.25	0.34	 g = k
9.97	0.28	7.98	0.28	1.14	1.71	1.71	55.84	3.7	1.14	7.98	7.41	0.85	 h = j
7.86	1.33	4.71	0.79	0.65	1.5	1.37	0.03	64.4	1.91	10.39	4.71	0.34	 i = l
4.4	1.11	1.91	1.29	0.4	0.44	0.53	0.09	4.44	74.19	9.51	1.2	0.49	 j = o
1.05	0.32	0.7	0.2	0.16	0.33	0.34	0.09	1.33	0.48	93.48	1.45	0.08	 k = 0
4.38	0.39	1.18	0.28	0.79	1.18	2.81	0.45	4.66	0.79	16.67	66.27	0.17	 l = s
1.29	1.0	2.15	0.43	0.29	0.86	0.86	0.43	4.02	4.73	1.87	0.72	81.35	 m = u
