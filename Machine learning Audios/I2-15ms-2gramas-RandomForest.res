Timestamp:2013-07-22-21:57:14
Inventario:I2
Intervalo:15ms
Ngramas:2
Clasificador:RandomForest
Relation Name:  I2-15ms-2gramas
Num Instances:  12188
Num Attributes: 21

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
   2 0Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   940 /  8%  2714 
   3 0RawPitch                  Num   0%  53%  47%     0 /  0%   980 /  8%  2609 
   4 0SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
   5 0Melogram(st)              Num   0%  46%  54%     0 /  0%   837 /  7%  2573 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
   7 0F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
   8 0F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
   9 0F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  10 0F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  11 1Int(dB)                   Num   0%  10%  90%     0 /  0%    10 /  0%   768 
  12 1Pitch(Hz)                 Num   0%  50%  50%     0 /  0%   941 /  8%  2715 
  13 1RawPitch                  Num   0%  53%  47%     0 /  0%   979 /  8%  2609 
  14 1SmPitch                   Num   0%  51%  49%     0 /  0%   957 /  8%  2691 
  15 1Melogram(st)              Num   0%  46%  54%     0 /  0%   838 /  7%  2574 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%    10 /  0%    40 
  17 1F1(Hz)                    Num   0%  50%  50%     0 /  0%  2281 / 19%  3977 
  18 1F2(Hz)                    Num   0%  50%  50%     0 /  0%  3848 / 32%  5158 
  19 1F3(Hz)                    Num   0%  50%  50%     0 /  0%  3419 / 28%  4850 
  20 1F4(Hz)                    Num   0%  50%  50%     0 /  0%  3319 / 27%  4764 
  21 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 5 random features.
Out of bag error: 0.352





=== Stratified cross-validation ===

Correctly Classified Instances        8511               69.831  %
Incorrectly Classified Instances      3677               30.169  %
Kappa statistic                          0.6263
K&B Relative Info Score             739255.0435 %
K&B Information Score                21653.6217 bits      1.7766 bits/instance
Class complexity | order 0           35679.1394 bits      2.9274 bits/instance
Class complexity | scheme          1182610.1912 bits     97.0307 bits/instance
Complexity improvement     (Sf)    -1146931.0518 bits    -94.1033 bits/instance
Mean absolute error                      0.0658
Root mean squared error                  0.1855
Relative absolute error                 48.6371 %
Root relative squared error             71.3361 %
Coverage of cases (0.95 level)          91.0322 %
Mean rel. region size (0.95 level)      24.8927 %
Total Number of Instances            12188     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,820    0,071    0,693      0,820    0,751      0,702    0,934     0,828     A
                 0,299    0,032    0,327      0,299    0,313      0,279    0,779     0,218     LGJKC
                 0,663    0,042    0,576      0,663    0,617      0,583    0,918     0,627     E
                 0,158    0,006    0,292      0,158    0,205      0,206    0,769     0,113     FV
                 0,618    0,018    0,591      0,618    0,604      0,587    0,925     0,619     I
                 0,601    0,037    0,574      0,601    0,587      0,552    0,889     0,599     O
                 0,496    0,056    0,496      0,496    0,496      0,440    0,855     0,465     SNRTY
                 0,903    0,054    0,905      0,903    0,904      0,850    0,973     0,950     0
                 0,321    0,012    0,476      0,321    0,383      0,374    0,828     0,315     BMP
                 0,192    0,003    0,473      0,192    0,273      0,295    0,855     0,234     DZ
                 0,568    0,004    0,783      0,568    0,659      0,660    0,924     0,665     U
                 0,460    0,013    0,602      0,460    0,521      0,508    0,880     0,476     SNRTXY
Weighted Avg.    0,698    0,046    0,692      0,698    0,691      0,650    0,918     0,709     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k    l   <-- classified as
 1631   35   82    3   17   54   75   60    6    4    4   18 |    a = A
   97  179   56   11   30   31   97   58   15    1    2   21 |    b = LGJKC
  104   33  644    4   38   47   45   19   16    5    3   13 |    c = E
   23   15   20   28    6   24   37   10    4    1    0    9 |    d = FV
   20   27   74    3  311    6   23   17   12    0    2    8 |    e = I
  106   35   39    9   10  565   43   65   25   14    9   20 |    f = O
  173  101   57   17   38   58  604  133   15    3    3   16 |    g = SNRTY
   66   54   29   11   23   59  164 3990    9    0    2   10 |    h = 0
   38   24   31    3   34   33   48   35  128    2   12   11 |    i = BMP
   32    7   18    2    5   39   14    2    4   35    4   20 |    j = DZ
   13   12   14    0    3   41   11    2   16    2  162    9 |    k = U
   49   25   54    5   11   27   56   18   19    7    4  234 |    l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
82.0	1.76	4.12	0.15	0.85	2.71	3.77	3.02	0.3	0.2	0.2	0.9	 a = A
16.22	29.93	9.36	1.84	5.02	5.18	16.22	9.7	2.51	0.17	0.33	3.51	 b = LGJKC
10.71	3.4	66.32	0.41	3.91	4.84	4.63	1.96	1.65	0.51	0.31	1.34	 c = E
12.99	8.47	11.3	15.82	3.39	13.56	20.9	5.65	2.26	0.56	0.0	5.08	 d = FV
3.98	5.37	14.71	0.6	61.83	1.19	4.57	3.38	2.39	0.0	0.4	1.59	 e = I
11.28	3.72	4.15	0.96	1.06	60.11	4.57	6.91	2.66	1.49	0.96	2.13	 f = O
14.2	8.29	4.68	1.4	3.12	4.76	49.59	10.92	1.23	0.25	0.25	1.31	 g = SNRTY
1.49	1.22	0.66	0.25	0.52	1.34	3.71	90.33	0.2	0.0	0.05	0.23	 h = 0
9.52	6.02	7.77	0.75	8.52	8.27	12.03	8.77	32.08	0.5	3.01	2.76	 i = BMP
17.58	3.85	9.89	1.1	2.75	21.43	7.69	1.1	2.2	19.23	2.2	10.99	 j = DZ
4.56	4.21	4.91	0.0	1.05	14.39	3.86	0.7	5.61	0.7	56.84	3.16	 k = U
9.63	4.91	10.61	0.98	2.16	5.3	11.0	3.54	3.73	1.38	0.79	45.97	 l = SNRTXY
