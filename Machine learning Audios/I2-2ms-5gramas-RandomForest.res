Timestamp:2013-07-22-21:47:27
Inventario:I2
Intervalo:2ms
Ngramas:5
Clasificador:RandomForest
Relation Name:  I2-2ms-5gramas
Num Instances:  91699
Num Attributes: 51

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
   2 0Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
   3 0RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
   4 0SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
   5 0Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
   7 0F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
   8 0F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
   9 0F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  10 0F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  11 1Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  12 1Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  13 1RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  14 1SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  15 1Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  16 1ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  17 1F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  18 1F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  19 1F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  20 1F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  21 2Int(dB)                   Num   0%   9%  91%     0 /  0%     4 /  0%   779 
  22 2Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  23 2RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  24 2SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  25 2Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  26 2ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  27 2F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  28 2F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  29 2F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  30 2F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  31 3Int(dB)                   Num   0%   9%  91%     0 /  0%     5 /  0%   780 
  32 3Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  33 3RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  34 3SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  35 3Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  36 3ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  37 3F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  38 3F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  39 3F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  40 3F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  41 4Int(dB)                   Num   0%   9%  91%     0 /  0%     6 /  0%   781 
  42 4Pitch(Hz)                 Num   0%  52%  48%     0 /  0%   471 /  1%  4016 
  43 4RawPitch                  Num   0%  55%  45%     0 /  0%   550 /  1%  4014 
  44 4SmPitch                   Num   0%  53%  47%     0 /  0%   491 /  1%  3998 
  45 4Melogram(st)              Num   0%  49%  51%     0 /  0%   212 /  0%  3363 
  46 4ZCross                    Num   0% 100%   0%     0 /  0%     6 /  0%    54 
  47 4F1(Hz)                    Num   0%  52%  48%     0 /  0%  1126 /  1%  4776 
  48 4F2(Hz)                    Num   0%  52%  48%     0 /  0%  2304 /  3%  6727 
  49 4F3(Hz)                    Num   0%  52%  48%     0 /  0%  1967 /  2%  6224 
  50 4F4(Hz)                    Num   0%  52%  48%     0 /  0%  1884 /  2%  6074 
  51 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    12 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 6 random features.
Out of bag error: 0.1509





=== Stratified cross-validation ===

Correctly Classified Instances       81467               88.8418 %
Incorrectly Classified Instances     10232               11.1582 %
Kappa statistic                          0.8588
K&B Relative Info Score            7612142.3149 %
K&B Information Score               217966.2251 bits      2.377  bits/instance
Class complexity | order 0          262551.22   bits      2.8632 bits/instance
Class complexity | scheme          3414879.6162 bits     37.2401 bits/instance
Complexity improvement     (Sf)    -3152328.3962 bits    -34.3769 bits/instance
Mean absolute error                      0.0319
Root mean squared error                  0.1208
Relative absolute error                 24.0215 %
Root relative squared error             46.9157 %
Coverage of cases (0.95 level)          96.5648 %
Mean rel. region size (0.95 level)      16.8086 %
Total Number of Instances            91699     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,933    0,014    0,923      0,933    0,928      0,915    0,980     0,958     A
                 0,708    0,011    0,763      0,708    0,735      0,722    0,928     0,762     LGJKC
                 0,921    0,007    0,912      0,921    0,916      0,909    0,980     0,947     E
                 0,706    0,002    0,805      0,706    0,752      0,751    0,932     0,754     FV
                 0,898    0,003    0,922      0,898    0,910      0,906    0,974     0,929     I
                 0,859    0,006    0,916      0,859    0,887      0,879    0,958     0,902     O
                 0,752    0,032    0,707      0,752    0,729      0,700    0,953     0,789     SNRTY
                 0,944    0,058    0,912      0,944    0,928      0,881    0,984     0,968     0
                 0,753    0,003    0,893      0,753    0,817      0,814    0,940     0,822     BMP
                 0,879    0,001    0,946      0,879    0,911      0,910    0,986     0,943     DZ
                 0,920    0,001    0,976      0,920    0,947      0,946    0,987     0,963     U
                 0,852    0,002    0,940      0,852    0,894      0,891    0,966     0,905     SNRTXY
Weighted Avg.    0,888    0,030    0,889      0,888    0,888      0,863    0,972     0,921     


=== Confusion Matrix ===

     a     b     c     d     e     f     g     h     i     j     k     l   <-- classified as
 13372   124    38    12    16    43   257   400    29    12     4    26 |     a = A
   141  3136    51    26    23    42   444   500    25     0    12    28 |     b = LGJKC
    58    61  6361    12    21    26   153   154    19    12     6    27 |     c = E
    36    27    21   908    13    30   179    59     6     1     0     6 |     d = FV
    34    34    45     7  3199     9    91   123    15     0     0     4 |     e = I
   102    48    40    17    12  5709   129   541    14    15     3    16 |     f = O
   277   289   128    86    55    98  6514  1096    62     6     3    49 |     g = SNRTY
   285   250   156    46    85   173   892 33822    68    13     7    32 |     h = 0
    84    58    44     7    33    25   231   226  2194     2     9     2 |     i = BMP
    26     2    28     2     1    37    33    23     2  1183     0     9 |     j = DZ
    17    27    14     1    12    17    34    24    17     1  1914     2 |     k = U
    49    54    49     4     1    23   252    99     6     6     4  3155 |     l = SNRTXY

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	l	
93.3	0.87	0.27	0.08	0.11	0.3	1.79	2.79	0.2	0.08	0.03	0.18	 a = A
3.18	70.82	1.15	0.59	0.52	0.95	10.03	11.29	0.56	0.0	0.27	0.63	 b = LGJKC
0.84	0.88	92.05	0.17	0.3	0.38	2.21	2.23	0.27	0.17	0.09	0.39	 c = E
2.8	2.1	1.63	70.61	1.01	2.33	13.92	4.59	0.47	0.08	0.0	0.47	 d = FV
0.95	0.95	1.26	0.2	89.83	0.25	2.56	3.45	0.42	0.0	0.0	0.11	 e = I
1.53	0.72	0.6	0.26	0.18	85.9	1.94	8.14	0.21	0.23	0.05	0.24	 f = O
3.2	3.34	1.48	0.99	0.63	1.13	75.19	12.65	0.72	0.07	0.03	0.57	 g = SNRTY
0.8	0.7	0.44	0.13	0.24	0.48	2.49	94.4	0.19	0.04	0.02	0.09	 h = 0
2.88	1.99	1.51	0.24	1.13	0.86	7.92	7.75	75.27	0.07	0.31	0.07	 i = BMP
1.93	0.15	2.08	0.15	0.07	2.75	2.45	1.71	0.15	87.89	0.0	0.67	 j = DZ
0.82	1.3	0.67	0.05	0.58	0.82	1.63	1.15	0.82	0.05	92.02	0.1	 k = U
1.32	1.46	1.32	0.11	0.03	0.62	6.81	2.67	0.16	0.16	0.11	85.22	 l = SNRTXY
