Timestamp:2013-07-22-22:32:37
Inventario:I3
Intervalo:20ms
Ngramas:1
Clasificador:RandomForest
Relation Name:  I3-20ms-1gramas
Num Instances:  9129
Num Attributes: 11

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
   1 0Int(dB)                   Num   0%  11%  89%     0 /  0%    14 /  0%   764 
   2 0Pitch(Hz)                 Num   0%  49%  51%     0 /  0%  1033 / 11%  2440 
   3 0RawPitch                  Num   0%  52%  48%     0 /  0%  1062 / 12%  2369 
   4 0SmPitch                   Num   0%  50%  50%     0 /  0%  1056 / 12%  2419 
   5 0Melogram(st)              Num   0%  45%  55%     0 /  0%   938 / 10%  2342 
   6 0ZCross                    Num   0% 100%   0%     0 /  0%     7 /  0%    34 
   7 0F1(Hz)                    Num   0%  49%  51%     0 /  0%  2180 / 24%  3398 
   8 0F2(Hz)                    Num   0%  49%  51%     0 /  0%  3463 / 38%  4259 
   9 0F3(Hz)                    Num   0%  49%  51%     0 /  0%  3209 / 35%  4087 
  10 0F4(Hz)                    Num   0%  49%  51%     0 /  0%  3134 / 34%  4027 
  11 class                      Nom 100%   0%   0%     0 /  0%     0 /  0%    11 


Options: -I 10 -K 0 -S 1 -num-slots 4 

Random forest of 10 trees, each constructed while considering 4 random features.
Out of bag error: 0.3432





=== Stratified cross-validation ===

Correctly Classified Instances        6341               69.46   %
Incorrectly Classified Instances      2788               30.54   %
Kappa statistic                          0.6111
K&B Relative Info Score             542301.4853 %
K&B Information Score                14482.6784 bits      1.5864 bits/instance
Class complexity | order 0           24352.2535 bits      2.6676 bits/instance
Class complexity | scheme           897260.6486 bits     98.2868 bits/instance
Complexity improvement     (Sf)    -872908.3951 bits    -95.6193 bits/instance
Mean absolute error                      0.0698
Root mean squared error                  0.1952
Relative absolute error                 48.3367 %
Root relative squared error             72.6583 %
Coverage of cases (0.95 level)          90.7657 %
Mean rel. region size (0.95 level)      23.7943 %
Total Number of Instances             9129     


=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,744    0,066    0,691      0,744    0,716      0,658    0,911     0,782     A
                 0,598    0,044    0,546      0,598    0,571      0,531    0,900     0,551     E
                 0,595    0,133    0,515      0,595    0,552      0,438    0,842     0,561     CGJLNQSRT
                 0,065    0,003    0,243      0,065    0,103      0,119    0,703     0,078     FV
                 0,514    0,016    0,589      0,514    0,549      0,532    0,899     0,530     I
                 0,535    0,034    0,572      0,535    0,553      0,517    0,862     0,562     O
                 0,914    0,055    0,901      0,914    0,907      0,856    0,970     0,945     0
                 0,234    0,011    0,412      0,234    0,299      0,293    0,789     0,246     BMP
                 0,491    0,003    0,805      0,491    0,610      0,622    0,913     0,618     U
                 0,000    0,000    0,000      0,000    0,000      -0,000   0,499     0,001     DZ
                 0,098    0,004    0,289      0,098    0,146      0,161    0,783     0,120     D
Weighted Avg.    0,695    0,063    0,687      0,695    0,687      0,631    0,904     0,709     


=== Confusion Matrix ===

    a    b    c    d    e    f    g    h    i    j    k   <-- classified as
 1119   64  189    5   13   52   48    6    1    0    8 |    a = A
   78  446  147    4   21   23   13   10    0    0    4 |    b = E
  209  138 1041    6   60   85  172   27    7    2    4 |    c = CGJLNQSRT
   25   18   58    9    4   12    7    3    1    0    1 |    d = FV
   12   56   88    1  198    2   12   15    1    0    0 |    e = I
   81   36  129    3    5  381   46   16    5    0   10 |    f = O
   31   10  183    4   11   33 2957    7    1    0    0 |    g = 0
   25   30   98    2   20   23   21   70    6    1    3 |    h = BMP
   14    6   44    0    2   27    2   14  107    0    2 |    i = U
    1    0    2    0    0    0    2    0    0    0    0 |    j = DZ
   25   13   42    3    2   28    1    2    4    0   13 |    k = D

=== Percent Confusion Matrix ===
a	b	c	d	e	f	g	h	i	j	k	
74.35	4.25	12.56	0.33	0.86	3.46	3.19	0.4	0.07	0.0	0.53	 a = A
10.46	59.79	19.71	0.54	2.82	3.08	1.74	1.34	0.0	0.0	0.54	 b = E
11.94	7.88	59.45	0.34	3.43	4.85	9.82	1.54	0.4	0.11	0.23	 c = CGJLNQSRT
18.12	13.04	42.03	6.52	2.9	8.7	5.07	2.17	0.72	0.0	0.72	 d = FV
3.12	14.55	22.86	0.26	51.43	0.52	3.12	3.9	0.26	0.0	0.0	 e = I
11.38	5.06	18.12	0.42	0.7	53.51	6.46	2.25	0.7	0.0	1.4	 f = O
0.96	0.31	5.65	0.12	0.34	1.02	91.35	0.22	0.03	0.0	0.0	 g = 0
8.36	10.03	32.78	0.67	6.69	7.69	7.02	23.41	2.01	0.33	1.0	 h = BMP
6.42	2.75	20.18	0.0	0.92	12.39	0.92	6.42	49.08	0.0	0.92	 i = U
20.0	0.0	40.0	0.0	0.0	0.0	40.0	0.0	0.0	0.0	0.0	 j = DZ
18.8	9.77	31.58	2.26	1.5	21.05	0.75	1.5	3.01	0.0	9.77	 k = D
