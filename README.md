# Tesis ML-Lipsync

Sincronización automática de movimientos labiales para programas de animación mediante análisis de audio en español
Matías Eduardo Bordone Carranza
Famaf - UNC
Licenciatura en Ciencias de la computación

Si bien se hicieron otras pruebas el pipeline final de trbajo fue el siguiente:

Grabación de los diálogos en español
Etiquetadod e bloques de audio con posiciones bucales con speech analizer.
Se escribió un script de python apra generar el corpus de dato a partir de los archivos etiquetados.
Procesod e aprendizaje y evaluacion con weka (libreria de java)
+ bloques temporales de 1,2,5,7,10,20 ms
+ vectores de datos con 1-gramas a 5-gramas
+ metodos de machine learning utilizados, nahive bayes, bayes net y random tree
+ A su vez se generaron multiples conjuntos de clasificacion (5 diccionarios distintos) generando distintos conjuntos de posiciones bucales